<?php

namespace app\Tasks;

use app\Services\Common\SmsService;
use app\Services\Company\CompanyService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use app\Wechat\WxPay;
use Server\CoreBase\Task;
use app\Services\Common\ConfigService;
use app\Services\Common\NotifiyService;
use app\Library\SLog;

/**
 * 定时任务
 */
class TimeTask extends Task
{

    protected $GetIPAddressHttpClient;
    protected $coupon_model;
    protected $coupon_cate_model;

    public function initialization($task_id, $from_id, $worker_pid, $task_name, $method_name, $context)
    {
        parent::initialization($task_id, $from_id, $worker_pid, $task_name, $method_name, $context);
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
        $this->coupon_model = $this->loader->model('CouponModel', $this);
        $this->coupon_cate_model = $this->loader->model('CouponCateModel', $this);


        //启用备用redis连接池
        $this->redis = $this->loader->redis("redisPoolSpare", $this);
    }

    /**
     * 合同到期前一天,若有续费记录,再次下发套餐【暂时没用】
     */
    /*
    public function deliveryPackage() {
        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        //查询今日即将到期的合同
        $time = time();
        $start = strtotime(date('Y-m-d 0:0:0', $time));
        $end = strtotime(date('Y-m-d 23:59:59', $time));
        $where['real_exire_date'] = ['BETWEEN', [$start, $end]];
        $where['status'] = 4;
        $data = $this->db->select('contract_id,real_exire_date')
                ->from($this->prefix . 'contract')
                ->TPWhere($where)
                ->order(['exire_date' => 'DESC'])
                ->query()
                ->result_array();
        if (empty($data)) {
            return false;
        }

        $add_status = false;
        // $this->db->begin(function () use (&$add_status, $data) {
        //查询该合同有待下发套餐的续费记录
        foreach ($data as $k => $v) {
            $renew_map['contract_id'] = $v['contract_id'];
            $renew_map['pay_status'] = 2;
            $renew_map['status'] = 2;
            $order = $this->db
                    ->select('renew_order_id,package_mode,package_value,contract_id,package_cycle')
                    ->from($this->prefix . 'renew_order')
                    ->TPWhere($renew_map)
                    ->query()
                    ->row();
            if (empty($order)) {
                continue;
            }
            //查询该合同下所有设备
            $join = [
                ['equipment_lists as el', $this->prefix . 'contract_equipment.equipment_id = el.equipment_id', 'inner']
            ];
            $eq_map[$this->prefix . 'contract_equipment.contract_id'] = $v['contract_id'];
            $eq_map['state'] = 1;
            $eq_map[$this->prefix . 'contract_equipment.is_delete'] = 0;
            $eq_lists = $this->db->select('el.device_no,el.remaining_traffic,el.remaining_days,el.used_traffic,el.used_days')
                    ->from($this->prefix . 'contract_equipment')
                    ->TPWhere($eq_map)
                    ->TPJoin($join)
                    ->query()
                    ->result_array();
            if (empty($eq_lists)) {
                continue;
            }
            //下发指令--数据同步(下发套餐)
            foreach ($eq_lists as $kk => $vv) {
                $data_sync_params['sn'] = $vv['device_no'];
                $data_sync_params['used_days'] = $vv['used_days'];
                $data_sync_params['used_traffic'] = $vv['used_traffic'];
                $data_sync_params['remaining_days'] = $vv['remaining_days'];
                $data_sync_params['remaining_traffic'] = $vv['remaining_traffic'];
                if ($order['package_mode'] == 2) {
                    $data_sync_params['remaining_traffic'] = $order['package_value'] + $vv['remaining_traffic'];
                } else if ($order['package_mode'] == 1) {
                    $data_sync_params['remaining_days'] = $order['package_value'] + $vv['remaining_days'];
                }
                $data_sync_path = '/House/Issue/data_sync';
                HttpService::Thrash($data_sync_params, $data_sync_path);
            }
            //修改续费订单状态
            $edit_renew_data['status'] = 3;
            $edit_renew_data['renew_time'] = time();
            $edit_renew_where['renew_order_id'] = $order['renew_order_id'];
            $this->db->update($this->prefix . 'renew_order')
                    ->set($edit_renew_data)
                    ->TPwhere($edit_renew_where)
                    ->query()
                    ->affected_rows();
            //修改合同到期时间
            $edit_contract_data['real_exire_date'] = $v['real_exire_date'] + $order['package_cycle'] * 86400;
            $edit_contract_where['contract_id'] = $v['contract_id'];
            $this->db->update($this->prefix . 'contract')
                    ->set($edit_contract_data)
                    ->TPwhere($edit_contract_where)
                    ->query()
                    ->affected_rows();
        }
        $add_status = true;
        //  });

        if ($add_status) {
            ump(date('Y-m-d H:i:s', time()) . '下发套餐成功');
            return false;
        }
        var_dump(date('Y-m-d H:i:s', time()) . '下发套餐失败');
    }
    */

    /**
     * 续费提醒
     */
    public function renewalWarn()
    {
        //每天15点整通知
        if (date('H') != 15 || date('i') != 30 || date('s') != 0) {
            return false;
        }
        //获取后台配置
        $config = ConfigService::getConfig('contract_expire_time');
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', "合同到期提醒" . $config,true);
        $arr = explode(',', $config);
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', "合同到期提醒" . json_encode($arr),true);
        if (!empty($arr)) {
            foreach ($arr as $k => $v) {
                $this->warn($v);
            }
        }

    }

    /**
     * 续费提醒
     * @param type $day 时间
     */
    public function warn($day)
    {
        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $time = time() + 86400 * $day;
        $start = strtotime(date('Y-m-d 0:0:0', $time));
        $end = strtotime(date('Y-m-d 23:59:59', $time));
        $where['exire_date'] = ['BETWEEN', [$start, $end]];
        $where['status'] = 4;
        $where['is_old_contract'] = 0;//2023/9/6老系统先不发短信
        $join = [
            ['customer as c', $this->prefix . 'contract.user_id = c.user_id', 'inner']
        ];
        $data = $this->db->select('contract_id,contract_no,real_exire_date,exire_date,c.openid,c.telphone,renew_money,company_id')
            ->from($this->prefix . 'contract')
            ->TPWhere($where)
            ->TPJoin($join)
            ->order(['exire_date' => 'DESC'])
            ->query()
            ->result_array();
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', "合同到期提醒" . json_encode($data),true);
        if (!empty($data)) {
            $redis = get_instance()->loader->redis("redisPool", get_instance());
            //发送模板消息--续费提醒
            //通知到用户
            foreach ($data as $k => $v) {
                $user_notice_config = ConfigService::getTemplateConfig('renew_warn', 'user', $v['company_id']);
                if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                    $template_id = $user_notice_config['template']['wx_template_id'];
                    $mp_template_id = $user_notice_config['template']['mp_template_id'];
                    $str = '尊敬的用户,您好!您的净水器租用合同(合同编号:' . $v['contract_no'] . ')将于' . $day . '天后到期。为了保证您的正常使用，';
                    if ($day == 0) {
                        $str = '尊敬的用户,您好!您的净水器租用合同将于今天到期。为了保证您的正常使用,';
                    }
                    $template_data = [
                        'keyword1' => ['value' => '合同编号:' . $v['contract_no']],
                        'keyword2' => ['value' => '合同续费'],
                        'keyword3' => ['value' => $str . '请及时进入微信小程序进行续费,如已缴费,请忽略此信息。感谢您一直以来对我们的支持!点击卡片即可进入续费'],
                    ];
                    $content = $str . '请及时进行续费,如已缴费,请忽略此信息。感谢您一直以来对我们的支持!';
                    $url = 'pages/user/renew/renew?contract_id=' . $v['contract_id'];
                    $wx_url = '';
                    //如果是水行家特殊处理
                    if ($v['company_id'] == 4) {
                        $wx_url = ConfigService::getConfig('renew_web_url');
                    }
                    $weapp_template_keyword = '';
                    $exire_date = date('Y年m月d日 H:s', $v['exire_date']);
                    $mp_template_data = [
                        'first' => ['value' => '尊敬的用户,您好!您有一份合同即将到期,合同编号:' . $v['contract_no'], 'color' => '#4e4747'],
                        'keyword1' => ['value' => $exire_date, 'color' => '#4e4747'],
                        'keyword2' => ['value' => $v['renew_money'] . '元', 'color' => '#4e4747'],
                        'remark' => ['value' => '为了保证您的正常使用,请及时进入进行续费,如已缴费,请忽略此信息。感谢您一直以来对我们的支持!!若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $v['company_id']), 'color' => '#173177'],
                    ];
                    // FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', $v['telphone'] . '-' . $content);
                    // $company_config = CompanyService::getCompanyConfig($v['company_id']);
                    $company_config = $redis->hGet('cloud_company_config_data', $v['company_id']);
                    FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', "合同到期提醒" .json_encode(['contract_no'=>$v['contract_no'],'exire_date'=>$exire_date]),true);
                    if (!empty($company_config)) {
                        $company_config = json_decode($company_config, true);
                       NotifiyService::sendNotifiy($v['openid'], $template_data, $template_id, $v['telphone'], $content, $url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $company_config, $wx_url);
                    }

                }
            }

        }
    }

    /**
     * 合同到期修改合同状态
     */
    public function editContractStatus()
    {
        echo "合同到期修改合同状态开始" . date('Y-m-d H:i:s');
        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $time = time();
        $start = strtotime('1970-01-01');
        $end = strtotime(date('Y-m-d 23:59:59', $time));
        $where['exire_date'] = ['BETWEEN', [$start, $end]];
        $where['status'] = 4;
        $data = $this->db->select('contract_id')
            ->from($this->prefix . 'contract')
            ->TPWhere($where)
            ->query()
            ->result_array();
        if (!empty($data)) {
            $add_status = false;
            $this->db->begin(function () use (&$add_status, $where, $data) {
                //修改合同状态
                $edit_contract_data['status'] = 5;
                $this->db->update($this->prefix . 'contract')
                    ->set($edit_contract_data)
                    ->TPwhere($where)
                    ->query()
                    ->affected_rows();
                //添加合同日志
                foreach ($data as $k => $v) {
                    $add_data['contract_id'] = $v['contract_id'];
                    $add_data['do_id'] = 0;
                    $add_data['do_type'] = 3;
                    $add_data['terminal_type'] = 4;
                    $add_data['do_time'] = time();
                    $add_data['old_value'] = 4;
                    $add_data['new_value'] = 5;
                    $add_data['remark'] = '合同已到期,系统自动将修改合同状态为已到期';
                    $this->db->insert($this->prefix . 'contract_log')
                        ->set($add_data)
                        ->query()
                        ->insert_id();
                }
                $add_status = true;
            });
            if ($add_status) {
                var_dump(date('Y-m-d H:i:s', time()) . '修改合同状态为已到期成功');
                return false;
            }
            var_dump(date('Y-m-d H:i:s', time()) . '修改合同状态为已到期失败');
        }
        var_dump(date('Y-m-d H:i:s', time()) . '没有即将到期的合同');
    }

    /**
     * @desc  滤芯到期--发送模板消息，新建工单
     * @param
     * @date   2018-09-03
     * @return [type]     [description]
     * @author lcx
     */
    public function filterExpires()
    {
        //查询所有即将过期的滤芯
        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $time = time();
        $start = strtotime(date('Y-m-d 00:00:00', $time));
        $end = strtotime(date('Y-m-d 23:59:59', $time));
        $where['is_filter'] = 1;
        $where['expire_time'] = ['BETWEEN', [$start, $end]];
        $data = $this->db->select('contract_id,equipment_id,parts_id')
            ->from($this->prefix . 'equipments_parts')
            ->TPWhere($where)
            ->query()
            ->result_array();
        $add_status = false;
        $this->db->begin(function () use (&$add_status, $data) {
            if (!empty($data)) {
                $redis = get_instance()->loader->redis("redisPool", get_instance());
                foreach ($data as $k => $v) {
                    $where2['contract_id'] = $v['contract_id'];
                    $contract = $this->db->select('user_id,contract_id,company_id')
                        ->from($this->prefix . 'contract')
                        ->TPWhere($where2)
                        ->query()
                        ->row();
                    if (empty($contract)) {
                        continue;
                    }
                    //配件信息
                    $parts_info = $this->db->select('parts_name')
                        ->from($this->prefix . 'parts')
                        ->TPWhere(['parts_id' => $v['parts_id']])
                        ->query()
                        ->row();
                    if (empty($parts_info)) {
                        continue;
                    }
                    //查询用户信息
                    $where3['user_id'] = $contract['user_id'];
                    $user_info = $this->db->select('openid,telphone,user_id')
                        ->from($this->prefix . 'customer')
                        ->TPWhere($where3)
                        ->query()
                        ->row();
                    if (!empty($user_info)) {
                        //设备信息
                        $where4['equipment_id'] = $v['equipment_id'];
                        $eq_info = $this->db->select('equipments_id,device_no,province,city,area,province_code,city_code,area_code,address,lat,lng,contact_number,contact')
                            ->from($this->prefix . 'equipment_lists')
                            ->TPWhere($where4)
                            ->query()
                            ->row();

                        //所属区域
                        $map['province_id'] = $eq_info['province_code'];
                        $map['city_id'] = $eq_info['city_code'];
                        $map['area_id'] = $eq_info['area_code'];
                        $sql = "select * from " . $this->prefix . "administrative_info where locate('," . $eq_info['area_code'] . ",',area_id) AND city_id=" . $eq_info['city_code'] . " AND province_id=" . $eq_info['province_code'];
                        $area_info = $this->db->query($sql)->row();
                        //新建换芯工单
                        $order['order_number'] = pay_sn('SW');
                        $order['equipments_id'] = $eq_info['equipments_id'];
                        $order['user_id'] = $user_info['user_id'];
                        $order['contract_id'] = $contract['contract_id'];
                        $order['administrative_id'] = $area_info['a_id'];
                        $order['operation_id'] = $area_info['operation'];
                        $order['reception_user'] = 2;
                        $order['work_order_status'] = 1;
                        $order['order_time'] = time();
                        $order['province'] = $eq_info['province'];
                        $order['city'] = $eq_info['city'];
                        $order['area'] = $eq_info['area'];
                        $order['province_code'] = $eq_info['province_code'];
                        $order['city_code'] = $eq_info['city_code'];
                        $order['area_code'] = $eq_info['area_code'];
                        $order['service_address'] = $eq_info['address'];
                        $order['lat'] = $eq_info['lat'];
                        $order['lng'] = $eq_info['lng'];
                        $order['contact_number'] = $eq_info['contact_number'];
                        $order['contacts'] = $eq_info['contact'];
                        $work_order_id = $this->db->insert($this->prefix . 'work_order')
                            ->set($order)
                            ->query()
                            ->insert_id();
                        //添加工单设备信息
                        $work_eq_data['work_order_id'] = $work_order_id;
                        $work_eq_data['equipment_id'] = $v['equipment_id'];
                        $work_eq_data['create_time'] = time();
                        $this->db->insert($this->prefix . 'work_equipment')
                            ->set($work_eq_data)
                            ->query()
                            ->insert_id();
                        //添加工单业务类型
                        $work_business_data['work_order_id'] = $work_order_id;
                        $work_business_data['business_id'] = 2;
                        $this->db->insert($this->prefix . 'work_order_business')
                            ->set($work_business_data)
                            ->query()
                            ->insert_id();
                        //添加工单日志
                        $log['work_order_id'] = $work_order_id;
                        $log['create_work_time'] = $order['order_time'];
                        $log['operating_time'] = time();
                        $log['do_id'] = 0;
                        $log['operating_type'] = 4;
                        $log['operating_status'] = 1;
                        $log['remarks'] = '【系统】自动发起换芯工单';
                        $this->db->insert($this->prefix . 'work_order_log')
                            ->set($log)
                            ->query()
                            ->insert_id();
                        $template_data = [];
                        $url = '';
                        $content = '尊敬的用户，您好！您的智能净水器滤芯已将到期!为了不影响您的正常使用,请尽快打开微信进入小程序提交换芯工单哟';
                        $user_notice_config = ConfigService::getTemplateConfig('core_expire', 'user', $v['company_id']);
                        if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                            $template_id = $user_notice_config['template']['wx_template_id'];
                            $mp_template_id = $user_notice_config['template']['mp_template_id'];
                            $weapp_template_keyword = '';
                            $mp_template_data = [
                                'first' => ['value' => '尊敬的用户，您好！您的智能净水器滤芯已将到期!', 'color' => '#4e4747'],
                                'keyword1' => ['value' => $eq_info['device_no'], 'color' => '#4e4747'],
                                'keyword2' => ['value' => $parts_info['parts_name'] . '已经到期', 'color' => '#4e4747'],
                                'remark' => ['value' => '为了不影响您的正常使用,请尽快提交换芯工单哟!感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾!若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $contract['company_id']), 'color' => '#173177'],
                            ];
                            $company_config = $redis->hGet('cloud_company_config_data', $v['company_id']);
                            if (!empty($company_config)) {
                                $company_config = json_decode($company_config, true);
                                NotifiyService::sendNotifiy($user_info['openid'], $template_data, $template_id, $user_info['telphone'], $content, $url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $company_config);
                            }
                        }
                    }
                }
            }
            $add_status = true;
        });
    }

    /**
     * 续费当天发送续费信息-批量发送
    */
    public function sendRenewMsgByDay()
    {
        // 短信内容
        $sendMsg = '您好，您家净水器合同已到期，为不影响您正常用水，可登陆《重庆润泉》小程序，我的合同续费；如已续费，请忽略此信息！';
        // 获取当日开始和结束的时间戳
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        // 查询今日续费到期的合同下的用户id
        $where = [
            'exire_date'=>['BETWEEN', [$beginToday, $endToday]],
        ];

        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $data = $this->db->select('contract_id,user_id')
            ->from($this->prefix . 'contract')
            ->TPWhere($where)
            ->query()
            ->result_array();
        dump($data);
        $add_status = false;

    }

    /**
     * 发送续费短信 每月首日-批量发送
    */
    public function sendRenewMsgByMonthStart()
    {

        $mysql = get_instance()->config['mysql'];
        $prefix = $mysql[$mysql['active']]['prefix'] ?? '';

        // 判断是否发送短信
        $date = date('d',time());
        if($date!= '01'){
            return false;
        }
        $sendMsg = '尊敬的润泉用户：您好，您家净水器这个月该换芯续约，本月之内会有工作人员联系您更换滤芯，上门前请检查工作服及工作牌；您也可微信登陆《重庆润泉》小程序，我的合同自主续费；如您有特殊情况需提前安排更换滤芯，请致电公司服务热线：023-89883333）感谢您的信任与支持！(如已续费，请忽略此信息)';
        $month = date('m');
        // 获取这个月开始和结束时间戳
        // TODO 查询范围正负3天  之前合同时间都是按照365天来计算 闰年平年会有误差
        $startMonth = strtotime(date('Y-m-01 00:00:00')) - (3*86400);
        $endMonth  = strtotime(date('Y-m-t 23:59:59')) + (3*86400);

        //查询当月需要续费的所有合同
        $where['exire_date'] = ['BETWEEN', [$startMonth, $endMonth]];
        $where['status'] = 4;
        $join = [
            ['customer as c', $prefix . 'contract.user_id = c.user_id', 'inner']
        ];
        // 查询所有这个月过期的合同
        $data = $this->db->select('contract_id,contract_no,real_exire_date,exire_date,c.openid,c.telphone,renew_money,company_id,installed_time')
            ->from($prefix . 'contract')
            ->TPWhere($where)
            ->TPJoin($join)
            ->order(['exire_date' => 'DESC'])
            ->query()
            ->result_array();
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', ['data'=>"合同到期提醒" . json_encode($data)],true);
        if (!empty($data)) {
            $redis = get_instance()->loader->redis("redisPool", get_instance());
            //发送模板消息--续费提醒
            //通知到用户
            foreach ($data as $k => $v) {
                // TODO 因为查询到期时间正负3天  根据合同安装月份 来判断(是否本月安装) 是否发送短信
                if(date('m',$v['installed_time']) == $month && $v['renew_money'] > 0){
                    $company_config = $redis->hGet('cloud_company_config_data', $v['company_id']);
                    $company_config = json_decode($company_config,true);
                    $dev_mode = ConfigService::getConfig('develop_mode', false, $company_config['company_id']);
                    if (!$dev_mode) {
                        FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', ['data'=>"发送短信" .json_encode(['template_tel'=>$v['telphone'],'template_content'=>$sendMsg])],true);
                        //$contract[] = $v['contract_no'];
                        SmsService::sendBaMiSms($v['telphone'], $sendMsg, $company_config['sms_config']);
                    }
                }
            }
            // TODO 验证发送情况
            SmsService::sendBaMiSms(18223373494, $sendMsg, $company_config['sms_config']);
        }
    }



    /**
     * 发送续费短信 每月首日-批量发送
     */
    public function sendNewYearMessage()
    {

        $mysql = get_instance()->config['mysql'];
        $prefix = $mysql[$mysql['active']]['prefix'] ?? '';

        $set_date = '2025-01-24';
        $set_date_time_start = strtotime($set_date);
        $set_date_time_end = $set_date_time_start + 86399;
        // 判断是否发送短信
        if(time() < $set_date_time_start || time() > $set_date_time_end){
            return false;
        }
        $sendMsg = '亲爱的用户：新春佳节到，润泉祝您新春快乐，阖家幸福！温馨提醒：春节假期外出游玩请您关闭净水器总阀';

        # 待签、生效中、已过期
        $where['status'] = ['IN',[3,4,5]];
        $join = [
            ['customer as c', $prefix . 'contract.user_id = c.user_id', 'inner']
        ];
        # 查询满足条件的合同
        $data = $this->db->select('contract_id,c.telphone')
            ->from($prefix . 'contract')
            ->TPWhere($where)
            ->TPJoin($join)
            ->order(['exire_date' => 'DESC'])
            ->query()
            ->result_array();
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', ['data'=>"春节提醒通知" . json_encode($data)],true);
        if (!empty($data)) {
            # 去重
            $totalPhoneArray = array_column($data,'telphone');
            $uniqueTotalPhoneArray = array_values(array_filter(array_unique($totalPhoneArray)));
            $redis = get_instance()->loader->redis("redisPool", get_instance());
            # 执行发送
            foreach ($uniqueTotalPhoneArray as $k => $v) {
                if(isPhone($v)){
                    $company_config = $redis->hGet('cloud_company_config_data', 1);
                    $company_config = json_decode($company_config,true);
                    $dev_mode = ConfigService::getConfig('develop_mode', false, $company_config['company_id']);
                    if (!$dev_mode) {
                        FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', ['data'=>"发送短信" .json_encode(['template_tel'=>$v,'template_content'=>$sendMsg])],true);
                        //$contract[] = $v['contract_no'];
                        SmsService::sendBaMiSms($v, $sendMsg, $company_config['sms_config']);
                    }
                }
            }
            // TODO 验证发送情况
            SmsService::sendBaMiSms(15213444495, $sendMsg, $company_config['sms_config']);
        }
    }


//        // 发送短信的内容
//        $sendMsg = '测试:尊敬的润泉用户：您好，您家净水器合同这个月到期，如果您对我们的服务认可，可登陆《重庆润泉》小程序，我的合同续费；如已续费，请忽略此信息！）本月会有工作人员联系您上门更换滤芯。上门前请检查其工作服及工作牌，如您有特殊情况需提前安排更换，请致电公司服务热线：023-89883333）感谢您的信任与支持！';
//        $redis = get_instance()->loader->redis("redisPool", get_instance());
//        $company_config = $redis->hGet('cloud_company_config_data', 1);
//        $company_config = json_decode($company_config,true);
//
//        //$dev_mode = ConfigService::getConfig('develop_mode', false, $company_config['company_id']);
//        dump('测试发送短信');
//        //if (!$dev_mode) {//如果是非开发模式且模板消息发送失败时发送短信
//            FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', ['data'=>"发送短信" .json_encode(['template_tel'=>18223373494,'template_content'=>$sendMsg])],true);
//            SmsService::sendBaMiSms(18223373494, $sendMsg, $company_config['sms_config']);
//        //}


    // 优惠券定时任务
    public function couponTask()
    {
        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';

        // 1-- 获取所有今天已失效的优惠券
        $where['uid'] = ['>', 0]; // 表示领取了的
        $where['status'] = ['=',1];  // 待使用的
        $where['valid_time_end'] = ['<',time()];
        $coupon_data = $this->db->select('*')
            ->from($this->prefix . 'coupon')
            ->TPWhere($where)
            ->query()
            ->result_array();
        unset($where);

        // 2-- 修改已失效的优惠券状态
        if ($coupon_data) {
            foreach ($coupon_data as $k => $v) {
                $where['coupon_id'] = $v['coupon_id'];
                $data['status'] = 3;
                $this->db->update($this->prefix . 'coupon')
                    ->set($data)
                    ->TPWhere($where)
                    ->query()
                    ->affected_rows();
                unset($where);
            }
        }
    }


    //查询银行打款信息
    public function inquiryBank()
    {
        //仅凌晨0点和1点整执行
        if (!in_array(date('H'), [1,10,14]) || date('i') != 0 || date('s') != 0) {
           // var_dump(date('H'));
            return false;
        }
        var_dump(date('Ymd H').'执行inquiryBank');
        //查询审核通过提现单子
        $where['cash_order_state'] = 2;//审核通过
        $where['play_money_type'] = 1;//线上打款
        $where['play_money_state'] = 2;//打款中

        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $data = $this->db->select('cash_order_id,cash_order_number,cash_order_state,play_money_type,play_money_state')
            ->from($this->prefix . 'withdraw')
            ->TPWhere($where)
            ->query()
            ->result_array();
        if (empty($data)) {
            var_dump(date('Y-m-d') . '暂无待查询的线上提现订单');
            return false;
        }
        //查询打款信息
        $presentationLog = '[' . date('Y-m-d H:i:s') . '] ';
        foreach ($data as $k => $v) {
            $result = $this->queryBank($v['cash_order_number']);
            if ($result == false) {
                var_dump('订单' . $v['cash_order_number'] . '查询失败');
            }
            if ($result['return_code'] != 'SUCCESS') {
                var_dump('订单' . $v['cash_order_number'] . '查询失败,失败原因：' . $result['return_msg']);
            }
            if ($result['result_code'] != 'SUCCESS') {
                var_dump('订单' . $v['cash_order_number'] . '查询失败,错误代码：' . $result['err_code'] . ';错误代码描述：' . $result['err_code_des']);
            }
            $update = [];
            if (isset($result['status']) && $result['status'] == 'PROCESSING') {
                //（处理中，如有明确失败，则返回额外失败原因；否则没有错误原因）
                $update['error_msg'] = '打款中...自动查询';
                $update['play_money_state'] = 2;
            } elseif (isset($result['status']) && $result['status'] == 'SUCCESS') {
                //（付款成功）
                $update['error_msg'] = '打款付款成功--自动';
                $update['play_money_state'] = 3;
            } elseif (isset($result['status']) && $result['status'] == 'FAILED') {
                //（付款失败,需要替换付款单号重新发起付款）
                $update['error_msg'] = '付款失败，请重新审核--自动';
                $update['cash_order_number'] = pay_sn('DWD');
                $update['cash_order_state'] = 1;
                $update['play_money_state'] = 1;
            } elseif (isset($result['status']) && $result['status'] == 'BANK_FAIL') {
                //（银行退票，订单状态由付款成功流转至退票,退票时付款金额和手续费会自动退还）
                $update['error_msg'] = '银行退票--自动';
                $update['cash_order_state'] = 1;
                $update['play_money_state'] = 1;
            } else {
                $update['error_msg'] = $result['return_msg'].'--自动';
                var_dump('订单' . $v['cash_order_number'] . '查询成功,状态异常：' . $result['return_msg']);
            }
            //修改查询信息
            if (!empty($update)) {
                $this->db->update($this->prefix . 'withdraw')
                    ->set($update)
                    ->TPwhere(['cash_order_id' => $v['cash_order_id']])
                    ->query()
                    ->affected_rows();
            }

        }

    }


    /**
     * 查询打款银行卡信息
     * @param string $order_number
     * @return mixed
     * @throws \Exception
     * @date 2019/7/2 11:45
     * @author ligang
     */
    protected function queryBank(string $order_number)
    {
        $config = $this->config->get('wx_pay');
        //$wx = new WxPay($config);
        $wx = WxPay::getInstance($config);
        if ($wx->isSetConfig) $wx->setConfig($config);
        $param = [
            'mch_id' => $config['mchid'],
            'partner_trade_no' => $order_number,
            'nonce_str' => $wx->createNoncestr(),
        ];
        $param['sign'] = $wx->getSign($param);
        $xml = $wx->arrayToXml($param);
        $wx->url = $wx::GET_APY_BANK;
        $result = $wx->postXmlSSLCurl($xml, $wx->url);
        if ($result === false) {
            return false;
        }
        return $wx->xmlToArray($result);
    }


    // 批量发送短信
    public function sendMsgBatch()
    {
        $mysql = get_instance()->config['mysql'];
        $prefix = $mysql[$mysql['active']]['prefix'] ?? '';

        // 判断是否发送短信
        $date = date('Y-m-d',time());
        if($date!= '2024-08-26'){
            return false;
        }
        $sendMsg = '亲爱的客户：夏日炎炎，外出时请您一定关闭净水器总阀，注意防暑降温，记得多喝水哦！愿润泉的温馨提醒化作一抹清风，为您带来丝丝凉意；润泉祝您身体健康，悠然度夏。';
        // 只查询生效中 过期的
        $where['status'] = ['in',[4,5]];
        //$where['is_old_contract'] = 0;//2023/9/6老系统先不发短信
        $join = [
            ['customer as c', $prefix . 'contract.user_id = c.user_id', 'inner']
        ];
        // 查询所有这个月过期的合同
        $data = $this->db->select('contract_id,contract_no,c.openid,c.telphone,company_id')
            ->from($prefix . 'contract')
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->result_array();
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', ['data'=>"合同到期提醒" . json_encode($data)],true);
        $count = 0;
        $start = date('Y-m-d H:i:s',time());
        if (!empty($data)) {
            $redis = get_instance()->loader->redis("redisPool", get_instance());
            //发送模板消息--续费提醒
            //通知到用户
            //$contract = [];
            foreach ($data as $k => $v) {
                // TODO 因为查询到期时间正负3天  根据合同安装月份 来判断(是否本月安装) 是否发送短信
                if(isMobile($v['telphone'])){
                    $company_config = $redis->hGet('cloud_company_config_data', $v['company_id']);
                    $company_config = json_decode($company_config,true);
                    $dev_mode = ConfigService::getConfig('develop_mode', false, $company_config['company_id']);
                    if (!$dev_mode) {
                        FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', ['data'=>"发送短信" .json_encode(['template_tel'=>$v['telphone'],'template_content'=>$sendMsg])],true);
                        //$contract[] = $v['contract_no'];
                        SmsService::sendBaMiSms($v['telphone'], $sendMsg, $company_config['sms_config']);
                        $count++;
                    }
                }
            }
            $end = date('Y-m-d H:i:s',time());
            $sendMsgCheck = '总计发送短信:['.$count.']条 开始执行时间:['.$start.'] 结束执行时间:['.$end.']';
            // TODO 验证发送情况
            SmsService::sendBaMiSms(18223373494, $sendMsgCheck, $company_config['sms_config']);
        }
    }

}
