<?php

namespace app\index\controller;

use cmiot\OneNetApi;
use think\Controller;
use think\Request;
use cmiot\Util;

class Demo extends Controller {

    /**
     * 设备推送数据流API
     * @return mixed
     */
    public function index() {
        /**
         * 设备推送数据流
         */
        $raw_input = file_get_contents('php://input');
        $resolved_body = Util::resolveBody($raw_input);

        /**
         * 最后得到的$resolved_body就是推送过后的数据
         * At last, var $resolved_body is the data that is pushed
         */
        Util::l($resolved_body);
        echo $resolved_body;

        /**
         * *************************************
         * 一个简单的示例结束
         * A simple example ends
         ***************************************/
        exit;
    }

    public function callDevice(){
        //秘钥
        $apiKey = 'V1I7+XB3pbNNIcERNXhn5vemOX3ihusH3zq4tQ7yE6IcbgRGH8xI7zDkVfA3wx2VYXCBIOf+z1MpsCTBAB1ipg==';
        //链接地址
        $apiUrl = 'https://iot-api.heclouds.com';
        //产品ID
        $productId = '5TPD49eaP2';

        //创建api对象
        $sm = new OneNetApi($apiKey, $apiUrl,$productId);

        $device_id = '866179063289487';//设备ID
        //调用设备服务器设置数据
        $device = $sm->remainDays($device_id,0);
        $error_code = 0;
        $error = '';
        if ($sm->error_no()!=0) {
            //处理错误信息
            $error_code = $sm->error_no();
            $error = $sm->error();
            var_dump('错误码：'.$error_code);
            var_dump('错误消息：'.$error);
        }else{
            var_dump('调用成功');exit;
        }

        //展现设备
        debugDump($device);
        exit;
    }

}
