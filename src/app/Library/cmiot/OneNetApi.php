<?php

namespace cmiot;

use cmiot\SafetyAuth;

/**
 * ChinaMobile OneNet PHP API SDK
 * @author zhangzhan - goalzz85@gmail.com
 * @Editor LiMao - limao777@126.com
 * @license MIT - https://opensource.org/licenses/MIT
 */
class OneNetApi
{

    protected $_key = NULL;

    protected $_base_url = 'https://iot-api.heclouds.com';

    protected $_raw_response = ''; // 服务端返回的原始数据

    protected $_http_code = 200;

    protected $_error_no = 0;

    protected $_error = '';

    protected $_token = '';

    protected $_product_id = '';

    protected $_res = '';

    protected $_expiration = 0;


    protected static $_ALLOW_METHODS = array(
        'GET',
        'PUT',
        'POST',
        'DELETE'
    );

    public function __construct($key = NULL, $base_url = NULL, $product_id = NULL, $res = null)
    {
        $this->_key = $key;
        $this->_product_id = $product_id;
        //用户资源参数
        //   $this->_res = 'userid/154611'; // 测试环境
        $this->_res = 'userid/357025'; // 正式环境
        $this->_expiration = time() + 24 + 3600;

        if (!empty($base_url)) {
            $this->_base_url = $base_url;
        }
        //获取TOKEN签名
        require_once APP_DIR . '/Library/cmiot/SafetyAuth.php';
        $safetyAuth = new SafetyAuth($this->_key, $this->_expiration, $this->_res);
        $this->_token = $safetyAuth->makeToken();
    }

    public function raw_response()
    {
        return $this->_raw_response;
    }

    public function error()
    {
        return $this->_error;
    }

    public function error_no()
    {
        return $this->_error_no;
    }

    public function http_code()
    {
        return $this->_http_code;
    }

    public function curApiKey()
    {
        return $this->_key;
    }

    //TDS值设置   目前并不更新数据需要与舒斌对接
    public function TDSSet($deviceId, $TDS_switch = 1, $TDS_types = 2, $TDS_default = 109)
    {
        $identifier = 'TDS_set';
        $parm['aacmd14'] = "1";
        $parm['TDS_switch'] = $TDS_switch;//0显示实时值,1显示默认值
        $parm['TDS_types'] = $TDS_types;//1设置纯水,2设置原水
        $parm['TDS_default'] = $TDS_default;//TDS默认值
        return $this->callService($deviceId, $identifier, $parm);
    }

    //已用天数
    public function daysUsedSet($deviceId, $used_days = 0)
    {
        $identifier = 'days_used_set';
        $parm['aacmdBA'] = "1";
        $parm['used_days'] = $used_days;//已用天数
        return $this->callService($deviceId, $identifier, $parm);
    }

    //恢复出厂设置
    public function factoryReset($deviceId, $resetType = 1)
    {
        $identifier = 'factory_reset';
        $parm['aacmd10'] = "1";
        $parm['reset'] = $resetType;//0无效,1恢复出厂设置
        return $this->callService($deviceId, $identifier, $parm);
    }

    //滤芯值设置修改  这个设置没有响应需要联调
    public function filterSet($deviceId, $filter_cmd = 7, $filter_number = 1000)
    {
        $identifier = 'filter_set';
        $parm['aacmd0E'] = "1";
        $parm['filter_cmd'] = $filter_cmd;//enum(枚举)  看产品设计文档
        $parm['filter_number'] = $filter_number;//滤芯值
        return $this->callService($deviceId, $identifier, $parm);
    }

    //已用流量设置 这个设置没有响应需要联调
    public function flowSet($deviceId, $used_flow = 1000, $remain_flow = 1000)
    {
        $identifier = 'flow_set';
        $parm['aacmdAA'] = "1";
        $parm['used_flow'] = $used_flow;//已用流量
        $parm['remain_flow'] = $remain_flow;//剩余流量
        return $this->callService($deviceId, $identifier, $parm);
    }

    //获取滤芯状态
    public function getFilterState($deviceId, $get_filter = 1)
    {
        $identifier = 'get_filter_state';
        $parm['aacmd19'] = "1";
        $parm['get_filter'] = $get_filter;
        return $this->callService($deviceId, $identifier, $parm);
    }

    //获取状态
    public function getState($deviceId, $get_wstate = 1)
    {
        $identifier = 'get_state';
//        $parm['aacmd19'] = "1";
        $parm['aacmd0C'] = "1";
        $parm['get_wstate'] = $get_wstate;
        return $this->callService($deviceId, $identifier, $parm);
    }

    //心跳间隔时间设置
    public function heartbeat_set($deviceId, $heartbeat_time = 180)
    {
        $identifier = 'heartbeat_set';
        $parm['aacmd15'] = "1";
        $parm['heartbeat_time'] = $heartbeat_time;//心跳间隔时间单位秒
        return $this->callService($deviceId, $identifier, $parm);
    }

    //进入检修时间
    public function maintenanceTime($deviceId, $maintenance_time = 3600)
    {
        $identifier = 'maintenance_time';
        $parm['aacmd13'] = "1";
        $parm['maintenance_time'] = $maintenance_time;//进入检修时间单位秒
        return $this->callService($deviceId, $identifier, $parm);
    }

    //开启制水功能
    public function openWaterProduction($deviceId, $water_production_switch = 1)
    {
        $identifier = 'open_water_production';
        $parm['aacmd05'] = "1";
//        $parm['water_production_switch'] = intval($water_production_switch);//0开机,1关机
        $parm['water_production_switch'] = $water_production_switch;//0开机,1关机
        return $this->callService($deviceId, $identifier, $parm);
    }

    //检修测试模式开关
    public function overhaulTestSet($deviceId, $overhaul_test_switch = 0)
    {
        $identifier = 'overhaul_test_set';
        $parm['aacmd16'] = "1";
        $parm['overhaul_test_switch'] = $overhaul_test_switch;//0无效,1进入测试模式,2退出测试模式,3进入检修模式,4退出检修模式
        return $this->callService($deviceId, $identifier, $parm);
    }

    /**
     * 下发套餐
     * @param $deviceId
     * @param $work_mode
     * @param $filter_cfg_total
     * @return bool|mixed
     */
    public function packageSet($deviceId, $work_mode = 1, $filter_cfg_total = [])
    {
        dump('下发套餐2025-2-6>>>>>');
        $identifier = 'package_set';
        $parm['aacmd02'] = "1";
        $parm['work_mode'] = intval($work_mode);//0流量模式,1时长模式
        $parm['filter_cfg1_total'] = intval($filter_cfg_total[0] ?? 10000);//滤芯值
        $parm['filter_cfg2_total'] = intval($filter_cfg_total[1] ?? 10000);//滤芯值;
        $parm['filter_cfg3_total'] = intval($filter_cfg_total[2] ?? 10000);//滤芯值;
        $parm['filter_cfg4_total'] = intval($filter_cfg_total[3] ?? 10000);//滤芯值;
        $parm['filter_cfg5_total'] = intval($filter_cfg_total[4] ?? 10000);//滤芯值;
        return $this->callService($deviceId, $identifier, $parm);
    }

    //剩余天数设置
    public function remainDays($deviceId, $remain_days_set = 365)
    {
        $identifier = 'remain_days';
        $parm['aacmdBB'] = "1";
        $parm['remain_days_set'] = $remain_days_set;//剩余天数
        return $this->callService($deviceId, $identifier, $parm);
    }

    //剩余流量设置
    public function remainFlowSet($deviceId, $remain_flow_cnt = 1000000)
    {
        $identifier = 'remain_flow_set';
        $parm['aacmdAB'] = "1";
        $parm['remain_flow_cnt'] = $remain_flow_cnt;//剩余流量
        return $this->callService($deviceId, $identifier, $parm);
    }

    //远程重启
    public function remoteReset($deviceId, $reset_t = 1)
    {
        $identifier = 'remote_reset';
        $parm['aacmd7A'] = "1";
        $parm['reset_t'] = $reset_t;//0无效,1复位重启
        return $this->callService($deviceId, $identifier, $parm);
    }

    //屏幕开关
    public function screenSwitch($deviceId, $screen_open = 1)
    {
        $identifier = 'screen_switch';
        $parm['aacmd03'] = "1";
        $parm['screen_open'] = $screen_open;//0打开,1关闭
        return $this->callService($deviceId, $identifier, $parm);
    }

    //屏幕显示内容设置
    public function showContentsSet($deviceId, $show_contents_day = 1, $show_flow = 3)
    {
        $identifier = 'show_contents_set';
        $parm['aacmd5A'] = "1";
        $parm['show_contents_day'] = $show_contents_day;//0显示已用天数,1显示剩余天数
        $parm['show_flow'] = $show_flow;//2显示已用流量,3显示剩余流量
        return $this->callService($deviceId, $identifier, $parm);
    }

    //时间戳同步
    public function timeSet($deviceId, $time_stamp_set = 0)
    {
        $identifier = 'time_set';
        $parm['aacmd17'] = "1";
        $parm['time_stamp_set'] = $time_stamp_set ?? time();
        return $this->callService($deviceId, $identifier, $parm);
    }

    //数据同步
    public function usedTimeSet($deviceId, $used_days = 1, $remain_days = 365, $used_traffic = 0, $remain_flow = 100000000)
    {
        $identifier = 'used_time_set';
        $parm['aacmd08'] = "1";
        $parm['used_days'] = $used_days;
        $parm['remain_days'] = $remain_days;
        $parm['used_traffic'] = $used_traffic;
        $parm['remain_flow'] = $remain_flow;
        return $this->callService($deviceId, $identifier, $parm);
    }

    //强冲
    public function washSwitch($deviceId, $wash_open = 1)
    {
        $identifier = 'wash_switch';
        $parm['aacmd07'] = "1";
        $parm['wash_open'] = $wash_open;
        return $this->callService($deviceId, $identifier, $parm);
    }

    //强冲时间设置
    public function washTimeSet($deviceId, $wash_time = 20)
    {
        $identifier = 'wash_time_set';
        $parm['aacmd11'] = "1";
        $parm['wash_time'] = $wash_time;
        return $this->callService($deviceId, $identifier, $parm);
    }

    //工作模式设置
    public function workModeSet($deviceId, $mode_set = 1)
    {
        $identifier = 'work_mode_set';
        $parm['aacmd0F'] = "1";
        $parm['mode_set'] = $mode_set;//0流量模式,1时长模式
        return $this->callService($deviceId, $identifier, $parm);
    }
    //检修时长设置
    public function setMaintenanceTime($deviceId, $time)
    {
        $identifier = 'maintenance_time';
        $parm['aacmd13'] = "1";
        $parm['maintenance_time'] = $time;//进入检修时间,单位秒
        return $this->callService($deviceId, $identifier, $parm);
    }

    /**
     * 激活设备
     * @return void
     */
    public function activation($deviceId,$remain_days,$used_days,$remain_flow=999999,$used_traffic=0,$work_mode=1)
    {
        $identifier = 'activation';
        $parm['aacmd04'] = "1";
        $parm['remain_days'] = intval($remain_days);//剩余天数
        $parm['used_days'] = intval($used_days);//剩余天数
        $parm['remain_flow'] = intval($remain_flow);//剩余天数
        $parm['used_traffic'] = intval($used_traffic);//剩余天数
        $parm['work_mode'] = $work_mode;//工作模式 0流量模式 1时长模式
        return $this->callService($deviceId, $identifier, $parm);
    }

    //获取设备信息
    public function device($id)
    {
        if (empty($id)) {
            return FALSE;
        }

        $api = "device/detail";
        $data = [
            'product_id' => $this->_product_id,
            'device_name' => $id
        ];
        return $this->_call($api, 'GET', $data);
    }

    //获取设备属性详情
//    public function queryDevicePropertyDetail($id,$params)
    public function queryDevicePropertyDetail($id)
    {
        if (empty($id)) {
            return FALSE;
        }
        $api = "/thingmodel/query-device-property-detail";
        $data = [
            'product_id' => $this->_product_id,
            'device_name' => $id,
//            'params'=> $params
            'params' => [
                "days_used", "device_state"
            ]
        ];
        dump('获取设备属性');
        return $this->_call($api, 'POST', $data);
    }

    //设置设备属性
    public function setDeviceProperty($id, $params)
    {
        if (empty($id)) {
            return FALSE;
        }

        $api = "/thingmodel/set-device-property";
        $data = [
            'product_id' => $this->_product_id,
            'device_name' => $id,
            'params' => $params
        ];
        return $this->_call($api, 'POST', $data);
    }

    //设备属性最新数据查询
    public function queryDeviceProperty($id)
    {
        if (empty($id)) {
            return FALSE;
        }

        $api = "/thingmodel/query-device-property";
        $data = [
            'product_id' => $this->_product_id,
            'device_name' => $id,
        ];
        return $this->_call($api, 'GET', $data);
    }

    //设备服务调用
    public function callService($id, $identifier, $params)
    {
        if (empty($id) || empty($identifier) || empty($params)) {
            return FALSE;
        }

        $api = "/thingmodel/call-service";
        $data = [
            'product_id' => $this->_product_id,
            'device_name' => $id,
            'identifier' => $identifier,
            'params' => $params
        ];
        return $this->_call($api, 'POST', $data);
    }


    /**
     * 下面是下发指令开始
     *
     */

    public function send_data_to_edp($device_id, $qos, $timeout, $sms, $type = 0)
    {
        if (empty($device_id)) {
            return FALSE;
        }

        $params = array();
        $params['device_id'] = $device_id;

        if ($qos === 0 || $qos === 1) {
            $params['qos'] = $qos;
        }
        if (!empty($timeout)) {
            $params['timeout'] = $timeout;
        }
        if ($type === 0 || $type === 1) {
            $params['type'] = $type;
        }

        $params = http_build_query($params);

        $api = "/cmds?{$params}";
        return $this->_call($api, 'POST', $sms);
    }

    public function send_data_to_mqtt($topic, $sms)
    {
        $api = "/mqtt?topic={$topic}";
        return $this->_call($api, 'POST', $sms);
    }

    public function send_data_to_edp_mqtt_use_device_id($device_id, $sms, $params = array())
    {
        if (empty($device_id)) {
            return FALSE;
        }

        $params['device_id'] = $device_id;

        $params = http_build_query($params);

        $api = "/cmds?{$params}";
        return $this->_call($api, 'POST', $sms);
    }

    /*
     * $params可以包含以下
     *
     * qos=0 | 1 //是否需要响应，默认为0。
     *
     * 0：不需要响应，即最多发送一次，不关心设备是否响应；
     *
     * 1：需要响应，如果设备收到命令后没有响应，则会在下一次设备登陆时若命令在有效期内(有效期定义参见timeout参数）则会继续发送。
     *
     * 对响应时间无限制，多次响应以最后一次为准。
     *
     * 本参数仅当type=0时有效；
     *
     * timeout=300 //命令有效时间，默认0。
     *
     * 0：在线命令，若设备在线,下发给设备，若设备离线，直接丢弃；
     *
     * >0： 离线命令，若设备在线，下发给设备，若设备离线，在当前时间加timeout时间内为有效期，有效期内，若设备上线，则下发给设备。单位：秒，有效围：0~2678400。
     */
    public function send_data_to_mqtt_use_device_id($device_id, $sms, $params = array())
    {
        if (empty($device_id)) {
            return FALSE;
        }

        $params['device_id'] = $device_id;

        $params = http_build_query($params);

        $api = "/cmds?{$params}";
        return $this->_call($api, 'POST', $sms);
    }

    public function send_data_to_tcp_touchuan($device_id, $sms)
    {
        if (empty($device_id)) {
            return FALSE;
        }

        $params = array();

        $params['device_id'] = $device_id;

        $params = http_build_query($params);

        $api = "/cmds?{$params}";
        return $this->_call($api, 'POST', $sms);
    }

    public function send_data_to_modbus($device_id, $sms)
    {
        if (empty($device_id)) {
            return FALSE;
        }

        $params = array();

        $params['device_id'] = $device_id;

        $params = http_build_query($params);

        $api = "/cmds?{$params}";
        return $this->_call($api, 'POST', array('cmd' => $sms));
    }

    public function get_dev_status($cmd_uuid)
    {
        if (empty($cmd_uuid)) {
            return FALSE;
        }

        $api = "/cmds/{$cmd_uuid}";
        $res = $this->_call($api, 'GET');
        return $res;
    }

    public function get_dev_status_resp($cmd_uuid)
    {
        if (empty($cmd_uuid)) {
            return FALSE;
        }

        $api = "/cmds/{$cmd_uuid}/resp";
        $res = $this->_rawcall($api, 'GET');
        return $res;
    }

    // 开始时间和结束时间转换为接口形式
    protected function _startendtime_to_startduration($start_time, $end_time)
    {
        $duration = 3600;
        $start = 0;
        if (!empty($start_time)) {
            $start = strtotime($start_time) + 1; // 不返回这个时间的点
            $start_time = date('Y-m-d\TH:i:s', $start);
        }

        if (!empty($end_time)) {
            $duration = $end_time - $start_time;
        }
        return array(
            $start_time,
            $duration
        );
    }

    // 权限key操作，具体还需要修改
    protected function _paddingUrl($url, $method = 'GET', $data = array())
    {
        if (empty($url)) {
            return $url;
        }

        if ($url[0] != '/') {
            $url = '/' . $url;
        }
        $url = $this->_base_url . $url;
        if ($method == 'GET' && $data) {
            $url = $url . '?' . http_build_query($data);
        }

        return $url;
    }

    // 返回直接的ret数据
    protected function _rawcall($url, $method = 'GET', $data = array(), $headers = array())
    {
        $url = $this->_paddingUrl($url, $method, $data);

        $this->_error_no = 0;
        $this->_error = NULL;
        $default_headers = array(
            "Authorization: {$this->_token}"
        );

        if (empty($this->_key)) {
            $default_headers = array();
        }

        if (empty($url)) {
            $this->_http_code = 500;
            return FALSE;
        }

        if (!in_array($method, self::$_ALLOW_METHODS)) {
            $this->_http_code = 500;
            return FALSE;
        }

        // 如果data不是想要的，直接设置为NULL
        if (is_null($data) || (is_array($data) && count($data) == 0) || $data === FALSE) {
            $data = NULL;
        } else {
            if (is_array($data)) {
                $data = json_encode($data);
            }
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        if ($method != 'GET') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        // header set
        if (!empty($headers)) {
            $headers = array_merge($default_headers, $headers);
        } else {
            $headers = $default_headers;
        }
        // 有可能default_header为空
        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $this->_beforeCall($ch, $url, $method, $data);
        $ret = curl_exec($ch);
        $this->_afterCall($ch, $url, $method, $data, $ret);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (!empty($http_code)) {
            $this->_http_code = $http_code;
        }

        curl_close($ch);
        $this->_raw_response = $ret;
        return $ret;
    }

    protected function _call($url, $method = 'GET', $data = array(), $headers = array())
    {
        $ret = $this->_rawcall($url, $method, $data, $headers);
        $ori_ret = $ret;
        $ret = @json_decode($ret, TRUE);
        if (empty($ret)) {
            $ret = FALSE;
        } else {
            if (empty($ret['code'])) {
                if (isset($ret['data'])) {
                    $ret = $ret['data'];
                } else {
                    $ret = TRUE;
                }
            } else {
                // 产生了错误了
                /**
                 * {
                 * errno:0 //0表示成功
                 * error:''//错误信息
                 * data:{} //如果有数据放在这里边
                 * }
                 */
                $this->_error_no = $ret['code'];
                if (!empty($ret['code'])) {
                    $this->_error = $ret['msg'];
                }

                $ret = FALSE;
            }
        }
        $this->_afterDecode($url, $method, $data, $ori_ret, $ret);
        return $ret;
    }

    protected function _beforeCall($ch, $url, $method, $data)
    {
    }

    protected function _afterCall($ch, $url, $method, $data, $ret)
    {
    }

    protected function _afterDecode($url, $method, $data, $ori_ret, $ret)
    {
    }
}

class SmartDataApi extends OneNetApi
{
}
