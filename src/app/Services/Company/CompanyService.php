<?php
/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: xg
 * @Date: 2021-03-15 17:33:35
 * @LastEditors: xg
 * @LastEditTime: 2022-09-24 20:15:44
 */

namespace app\Services\Company;

use app\Services\Common\HttpService;

/**
 * 配置服务
 * Class HttpService
 * @package app\Services\Common\HttpService
 */
class CompanyService
{
    /**
     * 获取公司基本信息
     * @param $company_id
     * @return bool|mixed
     */
    public static function getCompanyInfo($company_id)
    {
        if (empty($company_id)) {
            return false;
        }
        //请求boss后台接口获取入驻商信息
        $redis = get_instance()->loader->redis("redisPool", get_instance());
//        $redis_data = $redis->hGet('cloud_company_info_data',$company_id);
//        if (!empty($redis_data)) {
//            return json_decode($redis_data, true);
//        }
        $company_info = HttpService::requestBossApi(['company_id' => $company_id], '/api/Company/getCompany');
        if (!empty($company_info) && $company_info['code'] == 1000) {
            $redis->hSet('cloud_company_info_data',$company_id, json_encode($company_info['data']));
            return $company_info['data'];
        }
        return false;
    }

    /**
     * 获取公司配置
     * @param $company_id
     * @return bool|mixed
     */
    public static function getCompanyConfig($company_id)
    {
        if (empty($company_id)) {
            return false;
        }
        //请求boss后台接口获取入驻商信息
        $redis = get_instance()->loader->redis("redisPool", get_instance());
        $redis_data = $redis->hGet('cloud_company_config_data' ,$company_id);
//        if (!empty($redis_data)) {
//            return json_decode($redis_data, true);
//        }
        $company_info = HttpService::requestBossApi(['company_id' => $company_id], '/api/Company/getCompayConfig');
      
        if (!empty($company_info) && $company_info['code'] == 1) {
            $redis->hSet('cloud_company_config_data',$company_id, json_encode($company_info['data']));
            $config = $company_info['data'];
            $config['engineer_wx_config']['adapay_app_id'] = get_instance()->config->get('adapay_app_id')['engineer'];
            $config['wx_config']['adapay_app_id'] = get_instance()->config->get('adapay_app_id')['user'];
            return $config;
        }

        return false;
    }

}
