<?php

/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2019-04-24
 * Time: 下午 2:26
 */

namespace app\Services\Device;

use app\Services\Common\HttpService;
use app\Services\Log\AsyncFile;
use Server\CoreBase\SwooleException;

/**
 * 设备操作逻辑
 * Class DeviceService
 * @package app\Services\
 */

class DeviceService
{

    /**
     * 激活设备
     * @desc 1.下发套餐,2.数据数据
     * @param $device_no 必选  string  设备编号
     * @param $package_mode 必选 int 工作模式
     * @param $eq_filter_element_num 必选 array 滤芯值
     * @param $sync_data 必选 array 下发数据(已用天数,剩余天数,剩余天数,剩余流量)
     * @param $contract_id 可选 int 合同ID
     * @return bool
     * @date 2019-04-25
     * @author lcx
     */
    public static function activeDevice($device_no, $package_mode, $eq_filter_element_num, $sync_data, $contract_id = 0)
    {
        $equipmentWaterRecordModel = get_instance()->loader->model('EquipmentWaterRecordModel', get_instance());
        $equipmentListModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $contractPackageRecordModel = get_instance()->loader->model('ContractPackageRecordModel', get_instance());
        $redis = get_instance()->loader->redis("redisPool", get_instance());

        if (empty($device_no) || empty($eq_filter_element_num) || empty($sync_data)) {
            throw new SwooleException("参数错误");
        }
        //查询设备信息
        $eq_info = $equipmentListModel->findEquipmentLists(['device_no' => $device_no], 'equipment_id');
        if (empty($eq_info)) {
            throw new SwooleException("设备信息错误");
        }
        try {

            //下发-绑定套餐
            $params = ['sn' => $device_no, 'working_mode' => $package_mode, 'filter_element_max' => $eq_filter_element_num];
            $path = '/House/Issue/bindingPackage';
            HttpService::Thrash($params, $path);
            sleepCoroutine(2000);
            //下发--数据同步
            $data_sync_params = $sync_data;
            $data_sync_params['sn'] = $device_no;
            //如果是时长模式并且设置已用流量为0(大于0表示强制设置),统一计算已用流量值
            if ($package_mode == 1 && empty($data_sync_params['used_traffic'])) {
                $equipment_water_record_sum = $equipmentWaterRecordModel->getOne(['equipment_id' => $eq_info['equipment_id']], 'sum(water) total_water');  // 总用水量
                $data_sync_params['used_traffic'] = $equipment_water_record_sum['total_water'] ?? 0; //已用流量
            }
            $data_sync_path = '/House/Issue/data_sync';
            HttpService::Thrash($data_sync_params, $data_sync_path);

            sleepCoroutine(2000);


            //请求心跳
            $state_params['sn'] = $device_no;
            $state_path = '/House/Issue/heartbeat';
            HttpService::Thrash($state_params, $state_path);

            //保存设备数据
            $equipmentListModel->updateEquipmentLists(array('remaining_traffic' => $data_sync_params['remaining_traffic'], 'remaining_days' => $data_sync_params['remaining_days'], 'used_traffic' => $data_sync_params['used_traffic'], 'used_days' => $data_sync_params['used_days']), array('equipment_id' => $eq_info['equipment_id']));

            //保存套餐信息
            $package_record['contract_id'] = $contract_id;
            $package_record['equipment_id'] = $eq_info['equipment_id'];
            $package_record['used_days'] = $data_sync_params['used_days'];
            $package_record['remaining_days'] = $data_sync_params['remaining_days'];
            $package_record['used_traffic'] = $data_sync_params['used_traffic'];
            $package_record['remaining_traffic'] = $data_sync_params['remaining_traffic'];
            $package_record['working_mode'] = $package_mode;
            $package_record['filter_element_max'] = json_encode($eq_filter_element_num);
            $package_record['hand_time'] = time();
            $package_record['last_hand_time'] = time();
            $package_record['status'] = 1;
            $contractPackageRecordModel->add($package_record);

            //将套餐信息放入Redis中，实时监测套餐是否下发成功
            $PlanToMonitor = $redis->get('PlanToMonitor');
            if ($PlanToMonitor) {
                $PlanToMonitor = json_decode($PlanToMonitor, 1);
            } else {
                $PlanToMonitor = [];
            }
            array_push($PlanToMonitor, $package_record);
            $redis->set('PlanToMonitor', json_encode($PlanToMonitor));
        } catch (SwooleException $ex) {
            dump($ex->getMessage());
        }
    }

    //激活购买设备--保存设备配件滤芯信息并下发套餐
    public static function activeBuyDevice($device_no, $equipments_id)
    {
        $equipmentListModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $equipmentModel = get_instance()->loader->model('EquipmentModel', get_instance());
        $eqPartModel = get_instance()->loader->model('EquipmentsPartsModel', get_instance());
        $bindModel = get_instance()->loader->model('CustomerBindEquipmentModel', get_instance());
        $eq_info = $equipmentModel->getDetail(['equipments_id' => $equipments_id], 'original_parts,filter_element');
        $eq_list_info = $equipmentListModel->findEquipmentLists(['device_no' => $device_no], 'equipment_id');
        $eq_part_lists = array();
        $eq_filter_element = array();
        $eq_filter_element_num = array();
        $db_eq_filter_element_num = []; //设备表滤芯值
        $original_parts = array();
        if (!empty($eq_info)) {
            //原始配件
            if (!empty($eq_info['original_parts'])) {
                $original_parts = json_decode($eq_info['original_parts'], true);
                if (!empty($original_parts)) {
                    foreach ($original_parts as $kk => $vv) {
                        $arr['equipment_id'] = $eq_list_info['equipment_id'];
                        $arr['parts_id'] = $vv['parts_id'];
                        $arr['is_original'] = 1;
                        $arr['create_time'] = time();
                        $arr['cycle'] = $vv['cycle'];
                        $arr['last_replace_time'] = time();
                        $arr['expire_time'] = $arr['last_replace_time'] + $arr['cycle'] * 86400;
                        $arr['parts_number'] = 1;
                        $eq_part_lists[] = $arr;
                    }
                }
            }
            //滤芯
            if (!empty($eq_info['filter_element'])) {
                $filter_element = json_decode($eq_info['filter_element'], true);
                if (!empty($filter_element)) {
                    foreach ($filter_element as $kk => $vv) {
                        $arr2['equipment_id'] = $eq_list_info['equipment_id'];
                        $arr2['parts_id'] = $vv['parts_id'];
                        $arr2['is_filter'] = 1;
                        $arr2['create_time'] = time();
                        $arr2['cycle'] = $vv['cycle'];
                        $arr2['last_replace_time'] = time();
                        $arr2['expire_time'] = $arr2['last_replace_time'] + $arr2['cycle'] * 86400;
                        $arr2['parts_number'] = 1;
                        $arr2['filter_level'] = $kk + 1;
                        $eq_filter_element[] = $arr2;
                        array_push($eq_filter_element_num, $vv['cycle']);
                        $db_arr['residual_value'] = $vv['cycle'];
                        $db_arr['max'] = $vv['cycle'];
                        array_push($db_eq_filter_element_num, $db_arr);
                    }
                }
                //处理滤芯5级
                $all_len = count($eq_filter_element_num);
                if ($all_len < 5) {
                    $len = 5 - $all_len;
                    for ($i = 0; $i < $len; $i++) {
                        array_push($eq_filter_element_num, 1000);
                        $db_arr['residual_value'] = 1000;
                        $db_arr['max'] = 1000;
                        array_push($db_eq_filter_element_num, $db_arr);
                    }
                } else if ($all_len > 5) {
                    $eq_filter_element_num = array_slice($eq_filter_element_num, 0, 5);
                    $db_eq_filter_element_num = array_slice($db_eq_filter_element_num, 0, 5);
                }
            }
        }
        //设备配件表
        $eqPartModel->del(['equipment_id' => $eq_list_info['equipment_id']]); //删除当前设备配件信息
        if (!empty($eq_part_lists)) {
            foreach ($eq_part_lists as $k => $v) {
                $eqPartModel->add($v);
            }
        }
        if (!empty($eq_filter_element)) {
            foreach ($eq_filter_element as $k => $v) {
                $eqPartModel->add($v);
            }
        }
        $equipmentListModel->updateEquipmentLists(['start_time' => time(), 'end_time' => time() + 100 * 86400, 'working_mode' => 1, 'filter_element' => json_encode($db_eq_filter_element_num)], ['equipment_id' => $eq_list_info['equipment_id']]);
        /*		 * ****************************修改设备租赁状态****************** */
        $bindModel->save(['equipment_id' => $eq_list_info['equipment_id'], 'is_owner' => 2], ['status' => 1]);
        $sync_data['used_days'] = 0;
        $sync_data['used_traffic'] = 0;
        $sync_data['remaining_days'] = 999999999;
        $sync_data['remaining_traffic'] = 999999999;

        DeviceService::activeDevice($device_no, 1, $eq_filter_element_num, $sync_data);
    }

    public static function syncFilterData($device_no)
    {
        $redis = get_instance()->loader->redis("redisPool", get_instance());
        if (empty($device_no)) {
            return false;
        }
        //如果确实到期直接返回
        $residual_value = $redis->hget('cloud_device_filter_element_true', $device_no);
        if ($residual_value < 0) {
            return false;
        }
        unset($residual_value);
        //避免每次查询数据库,首选判断redis是否存在;如果欠费和滤芯待复位同时存在,将过期的滤芯值设为固定值2
        $redis_data = $redis->hget('cloud_device_filter_element', $device_no);
        if (!empty($redis_data)) {
            $redis_data = json_decode($redis_data, true);
            if ($redis_data[0]['date'] == date('Y-m-d')) {//每天只计算一次
//                if (date('i') % 5 != 0) {//每5分钟执行一次
//                    return false;
//                }
                $redis_arr = [];
                foreach ($redis_data as $k => $v) {
                    $path = '/House/Issue/filter_element_reset_modification';
                    $params = $v;
                    if ($params['is_exipre'] == 1 && $params['value'] < 0) {
                        $params['value'] = 30;
                    }
                    unset($params['date']);
                    unset($params['is_exipre']);
                    HttpService::Thrash($params, $path);
                    sleepCoroutine(600);
                    //请求心跳
                    $redis->del('device_heartbeat:' . $device_no);//删除redis心跳
                    $state_params['sn'] = $device_no;
                    $state_path = '/House/Issue/heartbeat';
                    HttpService::Thrash($state_params, $state_path);
                    sleepCoroutine(2000);
                    $redis_arr[] = $params;
                }
                unset($redis_arr);
                return true;
            }
        }
        //查询设备是否到期
        $achievementModel = get_instance()->loader->model('AchievementModel', get_instance());
        $where = [
            'device_no' => $device_no
        ];
        $join = [
            ['contract', 'rq_equipment_lists.contract_id = rq_contract.contract_id', 'LEFT']
        ];
        $achievementModel->table = 'equipment_lists';
        $result = $achievementModel->findJoinData($where, 'exire_date,effect_time', $join);
        $is_exipre = 0;
        if (!empty($result) && $result['exire_date'] < time()) {//已经到期
            $is_exipre = 1;
        }
        unset($where);
        $map['device_no'] = $device_no;
        $equipmentListModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $partsModel = get_instance()->loader->model('PartsModel', get_instance());
        $device_info = $equipmentListModel->findEquipmentLists($map, 'equipment_id,device_no');
        if (empty($device_info)) {
            return false;
        }
        $join = [
            ['equipments_parts ep', 'ep.parts_id = rq_parts.parts_id', 'left'],  // 设备配件表 -- 配件表
        ];
        $field = 'ep.id,rq_parts.parts_id,rq_parts.parts_name,ep.cycle,ep.expire_time';
        // 获取设备滤芯
        $where['ep.equipment_id'] = $device_info['equipment_id'];
        $where['ep.is_filter'] = 1;  // 1表示滤芯
        $where['ep.is_delete'] = 0;  // 0表示未删除
        $equipments_parts = $partsModel->getJoinAll($where, $field, $join);
        unset($where);
        if ($equipments_parts) {
            $redis_arr = [];
            foreach ($equipments_parts as $kk => &$vv) {
                $residual_value = floor(($vv['expire_time'] - time()) / 86400); // 剩余天数，当前仅根据时间计算，算法待定
                $params = ['sn' => $device_no, 'type' => 2, 'key' => $kk + 1, 'value' => $residual_value + 1];
                if ($residual_value < 0) {
                    if ($is_exipre == 1) {
                        $params['value'] =30;
                    } else {
                        $redis->hset('cloud_device_filter_element_true', $device_no, $residual_value);//保存确实到期了的
                        $redis->expire('cloud_device_filter_element_true', 86400);
                        break;
                    }
                }
                $path = '/House/Issue/filter_element_reset_modification';
                HttpService::Thrash($params, $path);
                sleepCoroutine(600);
                //请求心跳
                $redis->del('device_heartbeat:' . $device_no);//删除redis心跳
                $state_params['sn'] = $device_no;
                $state_path = '/House/Issue/heartbeat';
                HttpService::Thrash($state_params, $state_path);
                sleepCoroutine(2000);
                $params['date'] = date('Y-m-d');
                $params['is_exipre'] = $is_exipre;
                $redis_arr[] = $params;
            }
            //存入redis下一次直接读取
            if (!empty($redis_arr)) {
                AsyncFile::write('machineStateSyncFilter', date('Y-m-d H:i:s', time()) . '-' . $device_no . ':同步内容db' . json_encode($redis_arr));
                $redis->hset('cloud_device_filter_element', $device_no, json_encode($redis_arr));
                $redis->expire('cloud_device_filter_element', 86400);
            }
            unset($redis_arr);
        }


    }


    //生成非智能设备，设备编号
    public static function createNonEqNo()
    {
        $equipmentListModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $str = date('YmdHis').rand(10,99);
        $device_no = 'yho' . $str;
        $checkNumber = $equipmentListModel->findEquipmentLists(['device_no' => $device_no]);
        if (!$checkNumber) {
            return $device_no;
        }
        DeviceService::createNonEqNo();
    }


    //非智能设备新装工单生成时绑定
    public static function bindNonEquipment($work_order_id,$user_id,$equipments_id)
    {
        if (empty($work_order_id)) {
            return false;
        }
        $workOrderModel = get_instance()->loader->model('WorkOrderModel', get_instance());
        $workOrderBusinessModel = get_instance()->loader->model('WorkOrderBusinessModel', get_instance());
        $equipmentModel = get_instance()->loader->model('EquipmentModel', get_instance());
        $orderModel = get_instance()->loader->model('OrderModel', get_instance());
        $equipmentListsModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $customerBindEquipmentModel = get_instance()->loader->model('CustomerBindEquipmentModel', get_instance());
        $contractEquipmentModel = get_instance()->loader->model('ContractEquipmentModel', get_instance());
        $contractModel = get_instance()->loader->model('ContractModel', get_instance());
        $workEquipmentModel = get_instance()->loader->model('WorkEquipmentModel', get_instance());
        $contractRankModel = get_instance()->loader->model('ContractRankModel', get_instance());

        //查询工单信息
        $wo_map['work_order_id'] = $work_order_id;
        $work_order_info = $workOrderModel->getWorkDetail($wo_map, array(), '*');
        if (empty($work_order_info)) {
            return false;
        }
        //如果非新装不进行绑定设备
        $business = $workOrderBusinessModel->getOne(['work_order_id' => $work_order_info['work_order_id'], 'business_id' => 3], 'work_order_business_id');
        if (empty($business)) {
            return true;
        }
        $warranty_time['start'] = '';
        $warranty_time['end'] = '';
        //查询新产品信息
        $eq_info = $equipmentModel->getDetail(array('equipments_id' => $equipments_id), 'type,equipments_name,model,equipments_type,warranty_time,mainboard_id');
        //如果新产品不是非智能设备,不走绑定流程
        if ($eq_info['type'] != 2) {
            return true;
        }
        $device_no = DeviceService::createNonEqNo(); //生成设备编号

        /*		 * * *****设备保修时间，若从走下单流程，保修时间从确认收货时开始计算，若从不走下单流程，直接发起新装工单，保修时间从设备绑定时开始计算****** */
        if ($eq_info['equipments_type'] == 2) {
            $warranty_time['start'] = time();
            $warranty_time['end'] = $warranty_time['start'] + 86400 * $eq_info['warranty_time'];
        }

        //如果是后台添加的工单,没有订单数据
        if (!empty($work_order_info['order_id'])) {
            //判断该用户是否租赁该类型设备
            $or_map['equipments_id'] = $work_order_info['equipments_id'];
            $or_map['user_id'] = $work_order_info['user_id'];
            $or_map['order_id'] = $work_order_info['order_id'];
            $order = $orderModel->getOne($or_map, 'warranty_start_time,warranty_end_time,opertaor,pay_status');
            if (!empty($order) && $order['opertaor'] == 1 && $order['pay_status'] != 2) {
                //return $this->jsonend(-1103, "该用户订单未完成支付,不能绑定");
                return false;
            }
            if ($eq_info['equipments_type'] == 2) {
                $warranty_time['start'] = $order['warranty_start_time'];
                $warranty_time['end'] = $order['warranty_end_time'];
            }
        }

        //判断设备是否已经存在
        $equipment_info = $equipmentListsModel->findEquipmentLists(array('device_no' => $device_no), 'equipment_id');

        if (!empty($equipment_info)) {
            //判断用户是否重复绑定设备
            $bd_map['equipment_id'] = $equipment_info['equipment_id'];
            $bd_map['is_owner'] = 2;
            $bind_info = $customerBindEquipmentModel->getOne($bd_map, '*');
            if (!empty($bind_info)) {
                //return $this->jsonend(-1104, "该设备已被绑定过，不能重复绑定");
                return false;
            }
        }
        //判断用户是否已经绑定完
        $bind_map['equipments_id'] = $work_order_info['equipments_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['user_id'] = $work_order_info['user_id'];
        $bind_map['work_order_id'] = $work_order_id;
        $bind_list = $customerBindEquipmentModel->getAll($bind_map, '*');
        if (!empty($bind_list) && count($bind_list) >= $work_order_info['equipment_num']) {
            //return $this->jsonend(-1105, '该用户租赁该类型设备' . $work_order_info['equipment_num'] . '台，已绑定' . count($bind_list) . '台');
            return false;
        }

        //非智能设备所有状态正常
        $eq_data['status'] = 2;
        $eq_data['device_status'] = 0;
        $eq_data['working_mode'] = 1;

        $eq_data['province'] = $work_order_info['province'];
        $eq_data['city'] = $work_order_info['city'];
        $eq_data['area'] = $work_order_info['area'];
        $eq_data['province_code'] = $work_order_info['province_code'];
        $eq_data['city_code'] = $work_order_info['city_code'];
        $eq_data['area_code'] = $work_order_info['area_code'];
        $eq_data['address'] = $work_order_info['service_address'];
        $eq_data['lng'] = $work_order_info['lng'];
        $eq_data['lat'] = $work_order_info['lat'];
        $eq_data['contact_number'] = $work_order_info['contact_number'];
        $eq_data['contact'] = $work_order_info['contacts'];
        $eq_data['alias_name'] = $eq_info['equipments_name'];
        $eq_data['equipment_type'] = $eq_info['equipments_type'];
        if ($eq_info['equipments_type'] == 2) {
            $eq_data['start_time'] = $warranty_time['start'];
            $eq_data['end_time'] = $warranty_time['end'];
        }
        $eq_data['company_id'] = $work_order_info['company_id'];
        if (empty($equipment_info)) {
            //新增设备记录
            $eq_data['equipments_id'] = $work_order_info['equipments_id'];
            $eq_data['device_no'] = $device_no;
            $eq_data['create_time'] = time();
            $eq_data['status'] = 1;
            $eq_data['contract_id'] = $work_order_info['contract_id'];
            $equipment_id = $equipmentListsModel->insertEquipmentLists($eq_data);
        } else {
            //修改设备信息
            $eq_data['contract_id'] = $work_order_info['contract_id'];
            $equipment_id = $equipment_info['equipment_id'];
            $eq_data['equipments_id'] = $work_order_info['equipments_id'];
            $equipmentListsModel->updateEquipmentLists($eq_data, array('equipment_id' => $equipment_id));
        }
        //添加绑定记录
        $bind_data['equipments_id'] = $work_order_info['equipments_id'];
        $bind_data['work_order_id'] = $work_order_id;
        $bind_data['equipment_id'] = $equipment_id;
        $bind_data['user_id'] = $work_order_info['user_id'];
        $bind_data['create_time'] = time();
        $bind_data['is_owner'] = 2;
        if ($eq_info['equipments_type'] == 2) {//如果是售卖
            $bind_data['status'] = 1;
        }
        $customerBindEquipmentModel->addCustomerBindEquipment($bind_data);

        //添加合同设备关联记录
        if (!empty($work_order_info['contract_id'])) {
            $contract_eq_data['contract_id'] = $work_order_info['contract_id'];
            $contract_eq_data['equipments_id'] = $work_order_info['equipments_id'];
            $contract_eq_data['equipment_id'] = $equipment_id;
            $contract_eq_data['equipments_address'] = $work_order_info['province'] . $work_order_info['city'] . $work_order_info['area'] . $work_order_info['service_address'];
            $contract_eq_data['addtime'] = time();
            $contractEquipmentModel->add($contract_eq_data);
        }

        //添加工单设备关联表
        $work_eq_data['equipment_id'] = $equipment_id;
        $work_eq_data['work_order_id'] = $work_order_id;
        $work_eq_data['create_time'] = time();
        $workEquipmentModel->add($work_eq_data);

        //已判定完修改相应状态
        $bind_map['equipments_id'] = $work_order_info['equipments_id'];
        $bind_map['user_id'] = $work_order_info['user_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['work_order_id'] = $work_order_id;
        $bind_lists = $customerBindEquipmentModel->getAll($bind_map, '*');

        if (count($bind_lists) >= $work_order_info['equipment_num']) {
            $workOrderModel->editWork(array('work_order_id' => $work_order_id), array('is_bind_equipment' => 1));
            //合同状态
            if ($eq_info['equipments_type'] == 2) {//如果是售卖
                //修改合同状态
                $contractModel->save(['contract_id' => $work_order_info['contract_id']], ['status' => 4, 'is_effect' => 2, 'effect_time' => time(), 'sign_date' => time(), 'installed_time' => time(), 'exire_date' => time() + 86400 * 20 * 365, 'real_exire_date' => time() + 86400 * 20 * 365]);
                //添加合同日志
                DeviceService::addContractLog($work_order_info['contract_id'], $user_id,4, "售卖非物联网产品已上门,合同状态修改为生效中");
            } else {
                //添加合同日志
                DeviceService::addContractLog($work_order_info['contract_id'], $user_id,3, "设备绑定成功,待签署合同");
                $contractModel->save(array('contract_id' => $work_order_info['contract_id']), array('status' => 3));
            }

            //新增----------2019年1月22日11:00:46----------------ligang
            //绑定设备完成后，更改合同关系表设备数
            $contract = $contractModel->getOne(['contract_id' => $work_order_info['contract_id']], 'contract_no');
            $contract_rank = $contractRankModel->getOne(['contract_no' => $contract['contract_no']], 'id');
            $contractRankModel->save(['id' => $contract_rank['id']], ['device_number' => $work_order_info['equipment_num']]);
        }
        return true;

    }


    /**
     * @desc   添加合同操作日志
     * @param $contract_id
     * @param $user_id
     * @param int $status 1增2删3改4解绑'
     * @param string $remark
     * @return mixed [type]     [description]
     * @throws SwooleException
     * @date   2018-07-19
     * @author lcx
     */
    public static function addContractLog($contract_id, $user_id,$status = 1, $remark='')
    {
        $contractLogModel = get_instance()->loader->model('ContractLogModel', get_instance());
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res =$contractLogModel->add($data);
        return $res;
    }


    /**
     * @ 工程端半年、一年换芯
     * @param array $data 数据包
     * @return array
     * @throws SwooleException
     * @throws \RedisException
     */
    public function changeFilterElement(array $data): array
    {
        $equipmentId = $data['equipment_id'] ?? 0;//设备id
        $level = $data['filter_package_id'] ?? 0;// 换芯类型 1 半年换芯 2 一年换芯
        if(!$equipmentId)               return [false,'缺少参数equipment_id'];
        if(!in_array($level,[1,2]))      return [false,'未知换芯类型'];

        $map['equipment_id'] = $equipmentId;
        $join = [
            ['contract c', 'c.contract_id = rq_equipment_lists.contract_id', 'left'],  // 合同表
            ['equipments e', 'e.equipments_id = rq_equipment_lists.equipments_id', 'left'],  // 产品表
            ['equipment_rental_package p', 'p.package_id = c.package_id', 'left'],  // 套餐表
        ];
        //  设备模型 TODO 产品套餐升级后 查询产品套餐下面设置的滤芯
        $equipmentListsModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $filed = 'e.type,p.half_year_levels,p.one_year_levels,p.cycle,p.is_old,rq_equipment_lists.equipments_id,c.user_id,c.installed_time,
        c.package_id,rq_equipment_lists.contract_id,shutdown_state,equipment_id,device_no,device_status,remaining_days';
        $info = $equipmentListsModel->findEquipmentLists($map, $filed, 'AND', $join);
        if (empty($info)) {
            return [false,'设备不存在'];
        }
        // 查询产品套餐 换芯级数
        $half_year_levels = array_values(array_unique(array_filter(explode(',',$info['half_year_levels'])))); // 半年换芯级数
        $one_year_levels = array_values(array_unique(array_filter(explode(',',$info['one_year_levels']))));   // 一年换芯级数
        if($level==1 && empty($half_year_levels)){
            // 滤芯套餐
            $half_year_levels = [1,2,3];
        }
        if($level==2 && empty($one_year_levels)){
            // 滤芯套餐
            $one_year_levels = [1,2,3,4,5];
        }
        // 查询设备配件
        $equipmentsPartsModel = get_instance()->loader->model('EquipmentsPartsModel', get_instance());
        $map1['contract_id'] = $info['contract_id'];
        $map1['equipment_id'] = $equipmentId;
        $map1['is_filter'] = 1;
        $map1['filter_level'] = ['in',$one_year_levels];// 默认一年换芯
        if($level==1){
            $map1['filter_level'] = ['in',$half_year_levels];
        }
        $parts = $equipmentsPartsModel->getAll($map1);
        if(empty($parts)){
            return [false,'没有需要更换的滤芯'];
        }
        // 换芯日志模型
        $forCoreRecordModel = get_instance()->loader->model('ForCoreRecordModel', get_instance());
        // 处理滤芯数据
        foreach ($parts as $k=>$v){
            // 老套餐把天转换为月
            $cycle = getMonthByDay($v['cycle']);
            // 需要修改的滤芯数据 更新内容（过期时间 最近换芯时间 换芯次数）
            $partsUp = [
                'expire_time'=>strtotime('+'.$cycle.' month',time()),// TODO 每次换芯 直接按周期更新到期时间
                'last_replace_time'=>time(),
                'replace_num'=>$v['replace_num'] + 1
            ];
            $partsWhere = ['parts_id'=>$v['parts_id'], 'equipment_id'=>$info['equipment_id']];
            $equipmentsPartsModel->save($partsWhere,$partsUp);

            // 滤芯换芯记录
            $log = [
                'user_id'=>$info['user_id'],
                'contract_id'=>$info['contract_id'],
                'parts_id'=>$v['parts_id'],
                'cycle'=>$v['cycle'],
                'equipment_id'=>$info['equipment_id'],
                'for_core_record_time'=>time(),
                'order_type'=>2,// 换芯类型 1 工单换芯 2 快速换芯,
                'filter_package_id'=>$info['package_id'] // TODO 2024-7月前保存的为 rq_filter_package 表里的id  现变为rq_equipments_package 表id
            ];
            $forCoreRecordModel->add($log);
        }
        // 修改是否提示换芯状态
        $equipmentListsModel->save(['equipment_id'=>$equipmentId],['is_core'=>2]);
        if ($info['type'] == 1) {
            // 智能设备 需要下发套餐
            $redis = get_instance()->loader->redis("redisPool", get_instance());
            foreach ($parts as $kk => &$vv) {
                $residual_value = floor(($vv['expire_time'] - time()) / 86400); // 剩余天数，当前仅根据时间计算，算法待定
                $params = [
                    'sn' => $info['device_no'],
                    'type' => 2,
                    'key' => $vv['filter_level'] + 1,
                    'value' => $residual_value + 1
                ];
                $path = '/House/Issue/filter_element_reset_modification';
                sleepCoroutine(2000);
                $result = HttpService::Thrash($params, $path);
            }
            //删除redis数据，用于同步
            $redis->del('device_heartbeat:' . $info['device_no']);
            $redis->del('work_state:' . $info['device_no']);

            //请求心跳
            sleepCoroutine(2000);
            $state_params['sn'] = $info['device_no'];
            $state_path = '/House/Issue/heartbeat';
            HttpService::Thrash($state_params, $state_path);

        }
        return [true,'处理完成,请在设备信息里确认是否同步成功'];
    }

    /**
     * @ 系统后台crm 批量 半年 一年 换芯
     * @param array $data 数据包
     * @return array
     * @throws SwooleException
     * @throws \RedisException
     */
    public function changeFilterElementAll(array $data): array
    {
        $equipmentIdArr = $data['equipment_id'] ?? [];
        $level = $data['filter_package_id'] ?? 0;
        if(empty($equipmentIdArr) || !is_array($equipmentIdArr)){
            return [false,'必要参数[equipment_id]错误'];
        }
        if(!in_array($level,[1,2])){
            return [false,'未知换芯类型'];
        }
        $flag_num = 0; // 记录批量操作的记录数，成功则记录，失败不记录
        $equipmentsPartsModel = get_instance()->loader->model('EquipmentsPartsModel', get_instance());
        $equipmentListsModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $forCoreRecordModel = get_instance()->loader->model('ForCoreRecordModel', get_instance());
        $redis = get_instance()->loader->redis("redisPool", get_instance());
        foreach ($equipmentIdArr as $value) {
            $equipment_id = (int)trim($value);
            $map['equipment_id'] = $equipment_id;
            $join = [
                ['contract c', 'c.contract_id = rq_equipment_lists.contract_id', 'left'],  // 设备配件表 -- 配件表
                ['equipments e', 'e.equipments_id = rq_equipment_lists.equipments_id', 'left'],  // 设备配件表 -- 配件表
                ['equipment_rental_package p', 'p.package_id = c.package_id', 'left'],  // 套餐表
            ];
            //  TODO 产品套餐升级后 查询产品套餐下面设置的滤芯
            $filed = 'e.type,
            p.half_year_levels,p.one_year_levels,p.cycle,p.is_old,
            rq_equipment_lists.equipments_id,
            c.user_id,c.installed_time,c.package_id,
            rq_equipment_lists.contract_id,shutdown_state,equipment_id,device_no,device_status,remaining_days';
            $info = $equipmentListsModel->findEquipmentLists($map, $filed, 'AND', $join);
            if (empty($info)) {
                continue;
            }
            $half_year_levels = $info['half_year_levels'] ?  array_values(array_unique((explode(',',$info['half_year_levels'])))) : [1,2,3]; // 半年换芯级数
            $one_year_levels = $info['one_year_levels'] ? array_values(array_unique((explode(',',$info['one_year_levels'])))) : [1,2,3,4,5];   // 一年换芯级数

            // 查询设备配件
            $map1['equipment_id'] = $equipment_id;
            $map1['contract_id'] = $info['contract_id'];
            $map1['is_filter'] = 1;
            $map1['filter_level'] = ['in',$one_year_levels];// 默认一年换芯
            if($level==1){
                $map1['filter_level'] = ['in',$half_year_levels];
            }
            $parts = $equipmentsPartsModel->getAll($map1);
            if(empty($parts)){
               continue;
            }
            // 处理滤芯数据
            foreach ($parts as $k=>$v){
                $cycle = getMonthByDay($v['cycle']);
                // 需要修改的滤芯数据 更新内容（过期时间 最近换芯时间 换芯次数）
                $partsUp = [
                    'expire_time'=>strtotime('+'.$cycle.' month',time()),// TODO 每次换芯 直接按周期更新到期时间
                    'last_replace_time'=>time(),
                    'replace_num'=>$v['replace_num'] + 1
                ];
                $partsWhere = ['parts_id'=>$v['parts_id'], 'equipment_id'=>$info['equipment_id']];
                $equipmentsPartsModel->save($partsWhere,$partsUp);

                // 滤芯换芯记录
                $log = [
                    'user_id'=>$info['user_id'],
                    'contract_id'=>$info['contract_id'],
                    'parts_id'=>$v['parts_id'],
                    'cycle'=>$v['cycle'],
                    'equipment_id'=>$info['equipment_id'],
                    'for_core_record_time'=>time(),
                    'order_type'=>2,// 换芯类型 1 工单换芯 2 快速换芯,
                    'filter_package_id'=>$info['package_id'] // TODO 2024-7月前保存的为 rq_filter_package 表里的id  现变为rq_equipments_package 表id
                ];
                $forCoreRecordModel->add($log);
            }
            // 修改是否提示换芯状态
            $equipmentListsModel->save(['equipment_id'=>$equipment_id],['is_core'=>2]);
            if ($info['type'] == 1) {
                // 智能设备 需要下发套餐
                foreach ($parts as $kk => &$vv) {
                    $residual_value = floor(($vv['expire_time'] - time()) / 86400); // 剩余天数，当前仅根据时间计算，算法待定
                    $params = [
                        'sn' => $info['device_no'],
                        'type' => 2,
                        'key' => $vv['filter_level'] + 1,
                        'value' => $residual_value + 1
                    ];
                    $path = '/House/Issue/filter_element_reset_modification';
                    sleepCoroutine(2000);
                    HttpService::Thrash($params, $path);
                }
                //删除redis数据，用于同步
                $redis->del('device_heartbeat:' . $info['device_no']);
                $redis->del('work_state:' . $info['device_no']);

                //请求心跳
                sleepCoroutine(2000);
                $state_params['sn'] = $info['device_no'];
                $state_path = '/House/Issue/heartbeat';
                HttpService::Thrash($state_params, $state_path);

            }
            $flag_num++;
        }
        if ($flag_num > 0) {
            return [true,'处理完成,请在设备信息里确认是否同步成功'];
        } else {
            return [false,'没有设备滤芯更新'];
        }
    }

    # 其他配置
    public function otherParts($contract_id): string
    {
        $equipmentsPartsModel = get_instance()->loader->model('EquipmentsPartsModel', get_instance());
        $partsModel = get_instance()->loader->model('PartsModel', get_instance());
        $join = [
            ['parts p', 'p.parts_id=rq_equipments_parts.parts_id', 'left']
        ];
        $where['rq_equipments_parts.contract_id'] = $contract_id;
        $where['rq_equipments_parts.is_original'] = 2;
        $where['rq_equipments_parts.is_filter'] = 2;
        $where['rq_equipments_parts.is_delete'] = 0;
        $parts = $equipmentsPartsModel->getAll($where, 'parts_name', $join);//其他配件
        if(!empty($parts)){
            $partsNames = array_column($parts,'parts_name');
            return implode("、", $partsNames);
        }
        return '--';
    }


}
