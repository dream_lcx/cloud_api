<?php

namespace app\Services\Common;

use AdaPaySdk\AdaPayCommon;
use AdaPaySdk\Drawcash;
use AdaPaySdk\Member;
use AdaPaySdk\SettleAccount;
use app\Library\SLog;
use app\Services\Log\FileLogService;

class AdaPayService
{
    /**
     * 用户取现
     */
    public static function drawcash(array $drawcash_param)
    {
        if (empty($drawcash_param)) {
            return false;
        }

        $drawcash = new Drawcash();
        $drawcash_param = array(
            'order_no' => $drawcash_param['order_no'],
            'app_id' => $drawcash_param['app_id'],
            'cash_type' => 'DM', // DM-可用额度取现（已入账金额如未取现，则转为可用额度，取现后实时到账）
            'cash_amt' => $drawcash_param['cash_amt'],
            'member_id' => $drawcash_param['member_id'],
            'notify_url' => $drawcash_param['notify_url']
        );

        $drawcash->create($drawcash_param);
        if ($drawcash->isError()) {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_drawcash_error', json_encode($drawcash->result) . PHP_EOL, true);
            return $drawcash->result;
        } else {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_drawcash_success', json_encode($drawcash->result) . PHP_EOL, true);
            return $drawcash->result;
        }
    }

    /**
     * 创建用户对象
     */
    public static function member(array $params)
    {
        // 创建用户对象
        $member_param = array(
            'app_id' => $params['app_id'],
            'member_id' => $params['member_id'],
            'nickname' => $params['nickname']
        );
        $member = new Member();
        $member->create($member_param);

        if ($member->isError()) {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_member_error', json_encode($member->result) . PHP_EOL, true);
            // 失败处理
            return false;
        } else {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_member_success', json_encode($member->result) . PHP_EOL, true);
            // 成功处理
            return true;
        }
    }

    /**
     * 创建余额支付请求
     */
    public static function requestAdapay($params)
    {
        // 创建实名用户对象
        $adaPayCommon_param = array(
            'adapay_func_code' => 'settle_accounts.balancePay',
            'app_id' => $params['app_id'],
            'order_no' => $params['order_no'],
            'out_member_id' => '0',
            'in_member_id' => $params['in_member_id'],
            'trans_amt' => $params['trans_amt'],
            'goods_title' => $params['goods_title'],
            'goods_desc' => $params['goods_desc'],
        );
        $adaPayCommon = new AdapayCommon();
        $adaPayCommon->requestAdapay($adaPayCommon_param);
        if ($adaPayCommon->isError()) {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_requestAdapay_error', json_encode($adaPayCommon->result) . PHP_EOL, true);
            // 失败处理
            return false;
        } else {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_requestAdapay_success', json_encode($adaPayCommon->result) . PHP_EOL, true);
            // 成功处理
            return true;
        }
    }

    /**
     * 创建结算账户对象
     */
    public static function settleAccount($params)
    {
        $settleAccount = new SettleAccount();
        $settleAccount_param = array(
            'app_id' => $params['app_id'],
            'member_id' => $params['member_id'],
            'channel' => 'bank_account',
            'account_info' => [
                'card_id' => $params['card_id'],
                'card_name' => $params['card_name'],
                'cert_id' => $params['cert_id'],
                'cert_type' => '00',
                'tel_no' => $params['tel_no'],
                'bank_acct_type' => '2'
            ],
        );
        $settleAccount->create($settleAccount_param);
        if ($settleAccount->isError()) {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_settleAccount_error', json_encode($settleAccount->result) . PHP_EOL, true);
            return false;
        } else {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_settleAccount_success', json_encode($settleAccount->result) . PHP_EOL, true);
            return true;
        }
    }


    public static function codePay($pay_data, $notify_url = '', $user_info = '')
    {
        $payment_params = array(
            "adapay_func_code" => 'qrPrePay.qrPreOrder',
            "order_no" => $pay_data['order_sn'],
            "app_id" => $pay_data['app_id'],
            "pay_amt" => $pay_data['money'] . "",
            "goods_title" => "顺哥哥二维码title",
            "goods_desc" => "顺哥哥二维码desc",
        );
        $adaPayCommon = new AdaPayCommon();
        $s = $adaPayCommon->requestAdapayUits($payment_params);
        dump($s);
    }

    /**
     * 汇付支付
     * @param $pay_data
     * @param $notify_url
     * @param $user_info
     * @return array
     */
    public static function pay($pay_data, $notify_url, $user_info)
    {
        # 初始化支付类
        $adaPaySdkPayment = new \AdaPaySdk\Payment();
        # 支付设置
        $point = 2;//保留2为小数
        $money = (string)sprintf('%.' . $point . 'f', $pay_data['money']);
        $payment_params = array(
            "app_id" => $pay_data['app_id'],
            "order_no" => $pay_data['order_sn'],
            "pay_channel" => "wx_lite",
            "pay_amt" => $money,
            "goods_title" => "重庆润泉",
            "goods_desc" => "设备服务费",
            "notify_url" => $notify_url,
            "expend" => [
                "open_id" => $user_info['openid']
            ]
        );
        # 发起支付
        $adaPaySdkPayment->create($payment_params);
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_order', $pay_data['order_sn'] . PHP_EOL, true);
        # 对支付结果进行处理
        if ($adaPaySdkPayment->isError()) {
            $msg = [
                'title' => '汇付支付失败>>>order_no:' . $payment_params['order_no'],
                'msg' => $adaPaySdkPayment->result
            ];
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_service_order_error', json_encode($msg), true);
            //失败处理
            return [
                'code' => -1000,
                'data' => []
            ];
        } else {
            $result = $adaPaySdkPayment->result;
            //成功处理
            $pay_info = $result['expend']['pay_info'];
            return [
                'code' => 1000,
                'data' => json_decode($pay_info, true)
            ];
        }
    }


    public static function refund($refund_data)
    {
        $payment_params = array(
            "payment_id" => $refund_data['payment_id'],
            "refund_order_no" => $refund_data['refund_order_no'],
            "refund_amt" => $refund_data['refund_amt'],
            "reason" => "顺哥哥退款"
        );

        $refund = new \AdaPaySdk\Refund();
        $refund->create($payment_params);

        if ($refund->isError()) {
            //失败处理
            var_dump($refund->result);
        } else {
            //成功处理
            var_dump($refund->result);
        }
    }

    public function account()
    {
        $arr = [
            ['2023112916295248101535', '87354']

            ['002212023112916005310575846807760384000'],
            ['002212023112915555410575845553902956544'],
            ['']
        ];
    }
}
