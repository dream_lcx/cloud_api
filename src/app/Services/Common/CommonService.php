<?php

namespace app\Services\Common;

/**
 * 公共服务
 * Class HttpService
 * @package app\Services\Common\HttpService
 */
class CommonService {

	/**
	 * 生成唯一编号
	 * @param type $key 
	 * @param type $prefix
	 * @param type $type 1用户账号 2订单合同编号
	 * @return type
	 */
	public static function createSn($key = '', $prefix = '', $type = 2) {
		$redis = get_instance()->loader->redis("redisPool", get_instance());
		if ($type == 1) {//用户账号
			$sn = rand(100000, 999999);
		} else {//订单编号，合同编号
			$sn = pay_sn($prefix);
		}
		if ($redis->hExists($key, $sn)) {
			echo $sn . $key . "已存在";
			CommonService::createSn($key, $prefix, $type);
		} else {
			$redis->hSet($key, $sn, $sn);
			return $sn;
		}
	}

}
