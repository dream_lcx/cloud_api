<?php
/*
 * @Descripttion:
 * @version: 1.0.0
 * @Author: xg
 * @Date: 2021-03-15 17:33:35
 * @LastEditors: xg
 * @LastEditTime: 2022-05-09 17:08:10
 */

namespace app\Services\Contract;

use app\Services\Common\HttpService;
use Server\CoreBase\SwooleException;
use Exception;

/**
 * 合同服务
 * Class ContractService
 * @package app\Services\Common\ContractService
 */
class ContractService
{
    /**
     * 获取合同模板-
     */
    public static function getTemplate($company_id, $business_id, $contrct_class, $o_id = 0, $contract_equipments_line = 1, $equipments_id = 0)
    {
        $contractTemplateModel = get_instance()->loader->model('ContractTemplateModel', get_instance());
        //1同公司同服务同运营中心
        $temp_map['company_id'] = $company_id;
        $temp_map['business_id'] = $business_id;
        $temp_map['contract_class'] = $contrct_class;
        $temp_map['o_id'] = $o_id;
        $temp_map['contract_equipments_line'] = $contract_equipments_line;
        $temp_map['equipments_id'] = $equipments_id;
        $temp_info = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info)) {
            return $temp_info;
        }
        //1同公司同服务运营中心通用
        $temp_map['o_id'] = 0;
        $temp_info = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info)) {
            return $temp_info;
        }
        //2同公司所有服务通用
        $temp_map['business_id'] = 0;
        $temp_info2 = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info2)) {
            return $temp_info2;
        }
        // 3 所有公司同服务
        $temp_map['company_id'] = 0;
        $temp_map['business_id'] = $business_id;
        $temp_info3 = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info3)) {
            return $temp_info3;
        }
        // 4 所有公司所有服务
        $temp_map['company_id'] = 0;
        $temp_map['business_id'] = 0;
        $temp_info4 = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info4)) {
            return $temp_info4;
        }
        return false;
    }


    /**
     * 获取合同模版
    */
    public  function getTemplate2($data): array
    {
        $template =  $this->getWebContractTemplate($data);
        $contract = $data['contract_info'];
        switch ($contract['business_id']){
            case 5:$businessName = '移机';break;
            case 8:$businessName = '报停拆机';break;
            case 9:$businessName = '报停重装';break;
            default:$businessName = '续费';break;
        }
        //$version = '202403/';
        $version = '202409/';
        switch ($template){
            case 'new':
                $template = $version.'new_contract_temp.pdf';
                $templateName = '家用机新装合同';
                break;
            case 'buy':
                $template = $version.'buy_contract_temp.pdf';
                $templateName = '家用机购买合同';
                break;
            case 'renew':
                $template = $version.'renew_contract_temp.pdf';
                $templateName = '家用机'.$businessName.'合同';
                break;
            case 'commercial':
                $template = $version.'commercial_contract_temp.pdf';
                $templateName = '商用机合同';
                break;
            default:
                $template = $version.'new_contract_temp.pdf';
                $templateName = '默认合同';
                break;
        }
        dump('签署合同模版:'.$template);
        return [
            'company_id'=>$contract['company_id'],
            'business_id'=>$contract['business_id'],
            'oid'=>0,
            'contract_class'=>$contract['contract_class'],
            'template_name'=>$templateName==''? '家用机新装合同':$templateName,
            'network_path'=>'',
            'local_path'=>$template,
            'company_sign_postion_config'=>null,
            'user_sign_postion_config'=>null,
            'view_layout'=>1,
            'remark'=>'',
            'status'=>1,
            'contract_equipments_line'=>0,
            'equipments_id'=>0
        ];
    }

    /**
     * 获取 前端显示模版
     * @param array $data
     * @return string
     */

    public function getWebContractTemplate(array $data): string
    {
        $template = 'new';
        $contractInfo = $data['contract_info'];// 合同详情
        $business_id = $contractInfo['business_id']; // business_id 业务类型
        $is_buy_out = $contractInfo['is_buy_out'] ?? 2;// 是否买断 1 是 2 否
        $contract_equipments_line = $contractInfo['contract_equipments_line'];// 产品线 1 家用机 2 商用机
        //$contract_class = $contractInfo['contract_class'];//合同分类 1租赁合同 2购买合同
        $pid = $contractInfo['pid'] ?? 0;//合同id 续费时更新
        $esign_contract_no = $contractInfo['esign_contract_no'];
        $renew_order_id = (int)$data['renew_order_id'] ?? 0;//续费订单id
        $renew_contract_no = $data['esign_contract_no'] ?? '';
        // TODO  商用机 续费 新装 移机 重装 使用同一模版
        if($contract_equipments_line == 2){
            $template = 'commercial';
        }else{
            // TODO 家用机 合同
            if($contract_equipments_line != 1){
                return 'new';
            }
            if($business_id == 3){
                if($is_buy_out == 1){
                    $template = 'buy';
                }
                if($is_buy_out == 2){
                    $template = 'new';
                }
                if($renew_order_id || $esign_contract_no){
                    $template = 'renew';
                }
            }
            if($business_id == 5 && $pid){
                $template = 'renew';
            }
            if(in_array($business_id,[8,9,10])){
                $template = 'renew';
            }
            if(!empty($renew_contract_no)){
                $template = 'renew';
            }
        }
        return $template;
    }

    // 查询资金记录里面的真实金额
    public function getFinanceRecord($contract,int $type,int $renewOrderId = 0): array
    {
        // 查询新装工单
        $workOrderModel = get_instance()->loader->model('WorkOrderModel', get_instance());// 工单
        $financeRecordModel = get_instance()->loader->model('FinanceRecordModel', get_instance()); // 资金记录
        if($type==1){
            dump('新装结算资金查询');
            $work_join = [
                ['work_order_business b','b.work_order_id = rq_work_order.work_order_id','left']
            ];
            $work_where = [
                'rq_work_order.contract_id'=>$contract['contract_id'],
                'rq_work_order.user_id'=>$contract['user_id'],
                'b.business_id'=>3
            ];
            $work_filed = 'rq_work_order.order_id,rq_work_order.work_order_id';
            $work_order = $workOrderModel->getOne($work_where,$work_filed,$work_join);
            dump($work_order);
            $finance_where = [
                'order_id'=>$work_order['order_id'],
                'user_id'=>$contract['user_id']
            ];
            $finance = $financeRecordModel->getOne($finance_where,'*');
            dump('订单支付资金记录');
            dump($finance);
            if(empty($finance)){
                $finance_where = [
                    'work_order_id'=>$work_order['work_order_id'],
                    'user_id'=>$contract['user_id']
                ];
                $finance = $financeRecordModel->getOne($finance_where,'*');
                dump('工单支付资金记录');
                dump($finance);
            }
            $money = $finance['total_money'] ? bcdiv($finance['total_money'],100,2) : 0;
            $financeRecordId = $finance['id'] ?? 0;
        }else{
            dump('续费资金查询');
            // 续费资金记录查询
            $where = [
                'renew_order_id'=>$renewOrderId,
                'user_id'=>$contract['user_id']
            ];
            $finance = $financeRecordModel->getOne($where);
            $financeRecordId = $finance['id'];
            $money = $finance['total_money'] ? bcdiv($finance['total_money'],100,2) : 0;
        }
        if(!empty($finance)){
            $financeRecordId = $finance['id'] ?? 0;
            $money = $finance['total_money'] ? bcdiv($finance['total_money'],100,2) : 0;
        }
        dump('资金记录ID:'.$financeRecordId);
        dump('系统资金记录金额:'.$money);
        return [
            'financeRecordId'=>$financeRecordId,
            'money'=>$money
        ];
    }






    /**
     * @throws SwooleException
     * @throws Exception
     */
    public static function getMoveContractId(int $contractId)
    {
        $contractModel = get_instance()->loader->model('ContractModel', get_instance());
        $equModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        // 实列 合同模型
        $where = [
            'contract_id'=>$contractId
        ];
        $filed = 'contract_id,pid';
        $contract = $contractModel->getOne($where,$filed);
        if(!$contract){
            throw new Exception('合同不存在', -1000);
        }
        // 存在移机合同
        if($contract['pid'] != 0){
            dump('存在移机合同ID:'.$contract['pid']);
            $isTrue = true;
            $whereContractId = $contract['pid'];
            while ($isTrue){
                $selectWhere = [
                    'contract_id'=>$whereContractId
                ];
                $moveContract = $contractModel->getOne($selectWhere,$filed);
                if(empty($moveContract)){
                    $isTrue = false;
                    continue;
                }
                $equipmentLists = $equModel->getOne(['contract_id'=>$moveContract['contract_id']]);
                if(empty($equipmentLists)){
                    dump('续费移机合同设备不存在,合同ID:'.$moveContract['contract_id']);
                    $isTrue = false;
                    continue;
                }
                if($moveContract['pid']){
                    $whereContractId = $whereContractId['pid'];
                }else{
                    $isTrue = false;
                    $contractId = $moveContract['contract_id'];
                }
            }
        }
        return $contractId;
    }


    /**
     * 查询合同 通过用户 或地址
     * @param array $data 请求参数
     * @param int $user_id 当前工程人员
     * @return array
     * @throws SwooleException
     */
    public function searchContractByUser(array $data,int $user_id): array
    {
        if(!$user_id){
            return [false,'工程人员异常'];
        }
        $keyword = $data['keywordMsg'] ?? '';
        $address = $data['keywordAddress'] ?? '';
        if(!$keyword && !$address){
            return [false,'请输入用户信息或地址'];
        }
        try {
            // 实例化模型
            $engineersModel = get_instance()->loader->model('EngineersModel', get_instance());
            $engineersAdministrativeCenterModel = get_instance()->loader->model('EngineersAdministrativeCenterModel', get_instance());
            // 工程人员信息
            $engineers = $engineersModel->getOne(['engineers_id' => $user_id], 'operation_id, administrative_id');
            // 所属区域
            $engineers_administrative_center = $engineersAdministrativeCenterModel->getAll(['engineers_id' => $user_id], 'engineers_id, administrative_id');
            // 工程人员所有行政区
            $administrative_ids[] = $engineers['administrative_id'];
            foreach ($engineers_administrative_center as $v) {
                $administrative_ids[] = (int)$v['administrative_id'];
            }
            // 去重 排序
            $administrative_ids = array_values(array_unique($administrative_ids));
            if ($administrative_ids[0]) {
                $where['rq_contract.administrative_id'] = ['in', $administrative_ids];
            }
            // 输入了用户信息
            if($keyword){
                $where['b.realname|b.telphone'] = ['LIKE', '%' . $keyword . '%'];
            }
            // 输入了地址
            if($address){
                $where['rq_contract.province|rq_contract.city|rq_contract.area|rq_contract.address'] = ['like', '%'.$address.'%'];
            }
            $where['b.is_delete'] = 0;

            $join = [
                ['customer b', 'b.user_id = rq_contract.user_id', 'left']
            ];
            $field = 'distinct(b.user_id),b.telphone,b.account,b.realname,b.nickname,b.gender,b.avatar_img,b.country,b.province,b.city,b.area,b.user_address,b.id_card';

            // 合同
            $contractModel = get_instance()->loader->model('ContractModel', get_instance());
            $user_list = $contractModel->getAll($where, $field, ['rq_contract.add_time' => 'DESC'], 1, -1, $join);
            unset($where);
            unset($join);
            if (empty($user_list)) {
                return [false,'暂无符合条件的用户'];
            }

            $join = [
                ['contract', 'rq_contract.contract_id=rq_work_order.contract_id', 'left'],
                ['work_order_business business', 'business.work_order_id=rq_work_order.work_order_id', 'left']
            ];
            $data = [];

            $workOrderModel = get_instance()->loader->model('WorkOrderModel', get_instance());
            $contractStatus = get_instance()->config['contract_status'];
            foreach ($user_list as $k => $v) {
                unset($where);
                if($address){
                    $where['rq_contract.province|rq_contract.city|rq_contract.area|rq_contract.address'] = ['like','%'.$address.'%'];
                }
                //查询当前工程安装的用户以及合同信息
                $where['rq_contract.user_id'] = $v['user_id'];
                $where['business.business_id'] = ['IN', [3, 5, 9,10]]; //新装
                $where['rq_contract.status'] = ['IN', [4, 5]];//生效中或者已过期的合同
                $thisFiled = 'rq_work_order.work_order_id,rq_contract.contract_id,rq_contract.effect_time,rq_contract.contract_no,rq_contract.province,rq_contract.city,rq_contract.area,rq_contract.address,rq_contract.phone,rq_contract.contact_person,rq_contract.exire_date,rq_contract.status';
                $contract = $workOrderModel->getAll($where, $join, 1, -1, $thisFiled, ['effect_time' => 'DESC'], 'rq_work_order.contract_id');
                if (empty($contract)) {
                    continue;
                }
                foreach ($contract as $kk => $vv) {
                    $contract[$kk]['status_desc'] = $contractStatus[$vv['status']];
                    $contract[$kk]['effect_time'] = empty($vv['effect_time']) ? '' : date('Y/m/d', $vv['effect_time']);
                    $contract[$kk]['exire_date'] = empty($vv['exire_date']) ? '' : date('Y/m/d', $vv['exire_date']);
                }
                $v['avatar_img'] = uploadImgPath($v['avatar_img']);
                $v['username'] = empty($v['realname']) ? urldecode($v['nickname']) : $v['realname'];
                unset($v['nickname']);
                unset($v['realname']);
                $item['user_info'] = $v;
                $item['contract'] = $contract;
                $data[] = $item;
            }
            if (empty($data)) {
                return [false,'暂无符合条件的用户'];
            }
        }catch (SwooleException $exception){
            return [false,$exception->getMessage()];
        }
        return [true,$data];
    }
    // 获取安装过期时间
    public function contractExpireTime($cycle,$self)
    {
        $cycle = $cycle ?:12;
        $cycle = getMonthByDay($cycle);
        if($self){
            return $self;
            //$expireTime = strtotime('+1 year',$self);
        }else{
            $expireTime = strtotime('+'.$cycle.' month',time());
        }
        return $expireTime;
    }

    // 查询更新 续费金额
    // contractId 合同 id  packageId 套餐id
    public static function findRenewMoneyAndUpdate($contractId,$contractRenewMoney,$packageId)
    {
        $model = get_instance()->loader->model('AchievementModel', get_instance());
        $model->table = 'equipment_rental_package';
        $package = $model->findData(['package_id'=>$packageId]);
        if(empty($package)){
            return $contractRenewMoney;
        }
        if($package['renew_money'] != $contractRenewMoney){
            // 更新合同的续费金额
            dump('合同续费金额跟新:'.$contractId);
            $model->table = 'contract';
            $model->updateData(['contract_id'=>$contractId],['renew_money'=>$package['renew_money']]);
        }
        return $package['renew_money'];
    }

    /**
     * @ 配件过期时间更正 旧版 存储为天数 现为月数     旧数据与新数据冲突   //TODO 合同到期时间 这些 通用
     * @param int $cycle 配件周期   旧版为 天数 新版为月数
     * @param int $start 开始时间
    */
    public static function getPartsExpireTime(int $cycle,int $start)
    {
        // 周期
        $partsCycle = $cycle;
        //单位
        $unit = 'year';
        if($cycle <= 12){
            $unit = 'month';
        }else{
            switch ($cycle){
                case 180:$partsCycle = 6;$unit = 'month';break;
                case 365:$partsCycle = 1;break;
                case 999:$partsCycle = 20;break;
            }
            if($cycle > 365 && $cycle != 999){
                // 考虑特殊情况不为整年数
                if($cycle % 365 != 0){
                    $yearNum = floor($cycle / 365);//向下取整年
                    $sy = $cycle % 365;
                    $syMonth = ceil($sy / 30);// 向上取整月
                    $time = strtotime('+'.$yearNum.' year',$start);
                    return strtotime('+'.$syMonth.' month',$time);
                }
                $partsCycle = $cycle / 365;
            }
        }
        return strtotime('+'.$partsCycle.' '.$unit,$start);
    }

    /**
     * 配件剩余天数
     * @param int $cycle 周期
     * @param int $installed 安装时间
    */
    public static function partsWillExpireDay(int $cycle, int $installed)
    {
        if(!$installed || !$cycle){
            return 0;
        }
        // 获取过期时间
        $expire = 0;
        if($cycle<12){
            $expire = strtotime('+'.$cycle.' month',$installed);
        }else{
            switch ($cycle){
                case 180:$expire = strtotime('+6 month',$installed);break;
                case 365:$expire = strtotime('+1 year',$installed);break;
                case 999:$expire = strtotime('+20 year',$installed);break;
            }
            if($cycle > 365 && $cycle != 999){
                // 考虑特殊情况不为整年数
                if($cycle % 365 != 0){
                    $yearNum = floor($cycle / 365);//向下取整年
                    $sy = $cycle % 365;
                    $syMonth = ceil($sy / 30);// 向上取整月
                    $time = strtotime('+'.$yearNum.' year',$installed);
                    $expire =  strtotime('+'.$syMonth.' month',$time);
                }else{
                    $year = $cycle / 365;
                    $expire = strtotime('+'.$year.' year',$installed);
                }
            }
        }
        $willDay = ($expire - time()) / 86400;
        return  $willDay > 0 ? ceil($willDay) : 0;
    }


    // 获取百分比
    public static function proportion($cycle,$day,$installed)
    {
        if($cycle <= 12 && $cycle > 0){
            $expire = strtotime('+'.$cycle.' month',$installed) - $installed;
            $expireDay = ceil($expire / 86400);
        }else if($cycle > 12 && !in_array($cycle,[240,999])){
            $expire = strtotime('+20 year',$installed) - $installed;
            $expireDay = ceil($expire / 86400);
        }else{
            $expireDay = $cycle;
        }
        $sy = $expireDay - $day;
        if($sy == 0){
            return 1;
        }
        return  $sy / $day;
    }




    /**-----------------------------------------用户端合同签署通用方法----------------------------------------------------**/

    // TODO 判断合同是否绑定设备
    public static function isBindingEquipments(int $contractId): bool
    {
        $equipmentListsModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $equipment_lists_info = $equipmentListsModel->getOne(['contract_id' => $contractId], 'equipment_id');
        if (empty($equipment_lists_info)) {
            return true;
        }
        return false;
    }


    /**
     * TODO 换算合同周期天数
     * @param int $sign 合同签署时间
     * @param int $cycle 合同周期
    */
    public static function getContractCycleDays(int $sign,int $cycle = 12)
    {
        if(!$sign){
            $sign = time();
        }
        $unit = 'year';
        $num = 0;
        switch ($cycle){
            case 12||365:$num = 1;break;
            case 6:$num = 6;$unit = 'month';break;
            case 999:$num = 20;break;
        }
        if($cycle < 12){
            $unit = 'month';
        }
        // 合同过期时间 TODO 考虑到有闰年的情况 需获取到期真实时间戳来计算
        $expire = strtotime('+'.$num.' '.$unit,$sign);
        return  ($expire - $sign) / 86400;
    }

    /**
     * @ 用户设备绑定关系 验证  是否存在相同记录
     * @param array $work_order
     * @return bool
     * @throws SwooleException
     * @author
     */
    public static function checkCustomerBindEquipment(array $work_order): bool
    {
        if(empty($work_order)){
            return true;
        }
        $where['equipments_id'] = $work_order['equipments_id'];
        $where['work_order_id'] = $work_order['work_order_id'];
        $where['equipment_id'] = $work_order['equipment_id'];
        $where['user_id'] = $work_order['user_id'];
        $where['is_owner'] = 2;
        $where['status'] = ['in',[0,1]];
        $customerBindEquipment = get_instance()->loader->model('CustomerBindEquipmentModel', get_instance());
        $bind = $customerBindEquipment->getOne($where,'*');
        // 不存在绑定
        if(empty($bind)){
            return true;
        }
        return false;
    }

    // 根据工单获取合同状态  报停重装 必须先签合同 再回单
    public  function getContractStatus(int $work_order_id): bool
    {

        $workOrderModel = get_instance()->loader->model('WorkOrderModel', get_instance());
        $work = $workOrderModel->getOne(['work_order_id'=>$work_order_id],'contract_id');
        $contractModel = get_instance()->loader->model('ContractModel', get_instance());
        if(!empty($work)){
            $contract = $contractModel->getOne(['contract_id'=>$work['contract_id'],'contract_id,status']);
            if($contract['status'] == 3){
                return false;
            }
        }
        return true;
    }

}
