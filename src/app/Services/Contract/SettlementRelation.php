<?php

namespace app\Services\Contract;

use app\Controllers\Achievement\Config;
use app\Controllers\User\Base;

class SettlementRelation
{
    protected $Achievement;
    protected $ContractEquipmentModel;

    /**
     * 新 更新关系
     * // TODO 2024-6-1 开始使用该方式
    */

    // 更新合同关系
    public function newUpdateRelation($contract_no,$huaStatus): array
    {
        if (!$contract_no) {
            return [false,"缺少必要参数：contract_no"];
        }
        //合同信息
        $contract_where = [
            'contract_no' => trim($contract_no)
        ];
        $contract_field = 'rq_contract.contract_no,
        rq_contract.contract_id,
        rq_contract.pid,
        rq_contract.eq_count,
        rq_contract.user_id,
        rq_contract.source_user_id,
        rq_contract.source_contract_id,
        rq_contract.user_explainer,
        rq_contract.dismantle_user_explainer,
        rq_contract.dismantle_user_explain_settle_mode,
        rq_customer.is_dealer,
        rq_customer.source,
        rq_customer.dismantle_user_explainer as user_dismantle_user_explainer,
        rq_customer.dismantle_user_explain_settle_mode as user_dismantle_user_explain_settle_mode,
        rq_customer.is_old_client,
        rq_customer.is_agent_dealer,
        rq_customer.agent_pid
        ';
        $contract_join = [
            ['customer', 'rq_contract.user_id = rq_customer.user_id']
        ];
        $this->Achievement = get_instance()->loader->model('AchievementModel', get_instance());
        $this->Achievement->table = 'contract';
        $contract = $this->Achievement->findJoinData($contract_where, $contract_field, $contract_join);
        if (empty($contract)) {
            return [false,"合同或用户不存在"];
        }
        $dismantle_user_explainer = $contract['dismantle_user_explainer'];
        $dismantle_user_explain_settle_mode= $contract['dismantle_user_explain_settle_mode'];
        //若合同未设置,继续取客户管理设置的拆机讲解人
        if(empty($dismantle_user_explainer)){
            $dismantle_user_explainer = $contract['user_dismantle_user_explainer'];
            $dismantle_user_explain_settle_mode = $contract['user_dismantle_user_explain_settle_mode'];
        }
        $this->Achievement->table = 'contract_rank';
        $contract_rank = $this->Achievement->findData(['contract_no' => $contract_no], 'id,can_update,remarks,device_number,is_old_rank');
        if (!empty($contract_rank)) {
            //
            if ($contract_rank['can_update'] == 2 && empty($dismantle_user_explainer)) {
                return [false,"不允许更新关系1!" . $contract_rank['remarks']];
            }
        }
        //寻找该合同的新装工单
        $work_order_where = [
            'contract_id' => $contract['contract_id'],
            'rq_work_order_business.business_id' => ['IN', [3,5,9,10]],
        ];
        $work_order_field = 'rq_work_order_business.business_id,
        rq_work_order.explainer,
        rq_work_order.work_order_status,
        rq_work_order.user_id,
        rq_work_order.equipments_id,
        rq_work_order.administrative_id';
        $work_order_join = [
            ['work_order_business', 'rq_work_order.work_order_id = rq_work_order_business.work_order_id']
        ];
        $this->Achievement->table = 'work_order';
        $work_order = $this->Achievement->findJoinData($work_order_where, $work_order_field, $work_order_join);
        if (empty($work_order)) {
            return [false,'该合同不存在新装工单'];
        }
        //层级等级关系
        $field = '
            rq_customer.user_id,
            rq_customer.source,
            rq_customer.account,
            rq_customer.realname,
            rq_customer.username,
            rq_customer.nickname,
            rq_customer.is_dealer,
            rq_oa_workers.check_out,
            rq_oa_workers.agent_level,
            rq_customer.agent_pid,
            rq_customer.is_agent_dealer
        ';
        $order = [
            'source' => 'DESC'
        ];
        $join = [
            ['oa_workers', 'rq_customer.account = rq_oa_workers.workers_number', 'LEFT']
        ];
        $this->Achievement->table = 'customer';
        $all_user = $this->Achievement->selectData([], $field, $join, $order);
        $start = $contract['source'];
        // TODO 如果设置了代理经销商 那么用户的推荐人从代理经销商去找
        //XG 如果用户推荐人是普通用户,直接从代理经销商找则用户推荐人无法获得优惠券
//        if($contract['agent_pid']){// 设置了代理经销商
//            $this->Achievement->table = 'oa_workers';
//            $agentOaWorkers = $this->Achievement->findData(['workers_id'=>$contract['agent_pid']], 'user_id,is_agent_dealer');
//            if($agentOaWorkers['is_agent_dealer']){// 必须是当前状态为代理经销商
//                $start = $agentOaWorkers['user_id'];
//            }
//        }
        // TODO 用户的推荐人 若存在合同推荐人优先取合同推荐人
        if(!empty($contract['source_user_id'])){
            $start = $contract['source_user_id'];
        }
        //查询代理经销商用户ID
        $this->Achievement->table = 'oa_workers';
        $agent_user_id = 0;
        if(!empty($contract['agent_pid'])){
            $agent_user = $this->Achievement->findData(['workers_id'=>$contract['agent_pid']], 'user_id');
            $agent_user_id = $agent_user['user_id']??0;
        }
        $HigherUp = $this->findHigherUp($all_user, $start,$agent_user_id);
        if ($HigherUp === false) {
            return [false,'该关系存在闭环，请检查关系是否正确'];
        }
        //将本人压入数组第一位
        $self_user = [];
        foreach ($all_user as $key=>$val){
            if($val['user_id']==$contract['user_id']){
                array_push($self_user,$val);
            }
        }
        foreach ($HigherUp as $kk=>$vv){
            array_push($self_user,$vv);
        }
        // TODO 定义分佣金额 固定经销商分佣100 推荐人10 其余依次上次按极差分佣 分完为止
        // TODO 代理商等级分佣 一级110 二级120 三级130 四级 140
       // $commissionResult = $this->getOaWorkersCommission($HigherUp);
        $Relation = $this->findRelation($self_user, $all_user);
        $data = [];
        $data['is_check'] = 0;
        if($contract_rank['is_old_rank']==0) {
            $data = [
                'contract_user_id' => $contract['user_id'],
                'source_contract_id' => $contract['source_contract_id'],
                'source_user_id' => $contract['source_user_id'],
                'user_source' => empty($contract['source']) ? 0 : $contract['source'],
                'contract_user_identity' => $this->contractUserIdentity($contract),
                'contract_no' => $contract_no,
                'explainer_type' => 1,
                'explainer_id' => 0,
                'explainer_name' => '',
                'push_apply_id' => 0,
                'push_apply_name' => '',
                'manage_id' => 0,
                'manage_name' => '',
                'manage_money' => '0.00',
                'star_manage_id' => 0,
                'star_manage_name' => '',
                'star_manage_money' => '0.00',
                'two_manage_id' => 0,
                'two_manage_name' => '',
                'two_manage_money' => '0.00',
                'five_manage_id' => 0,
                'five_manage_name' => '',
                'five_manage_money' => '0.00',
                'ten_manage_id' => 0,
                'ten_manage_name' => '',
                'ten_manage_money' => '0.00',
                'twenty_manage_id' => 0,
                'twenty_manage_name' => '',
                'twenty_manage_money' => '0.00',
                'add_time' => time(),
                'star_explainer_type' => 1,
                'star_explainer_id' => 0,
                'dismantle_explainer_type' => 0,
                'dismantle_explainer_id' => 0,
                'dismantle_explainer_name' => '',
                'dismantle_explainer_money' => '0.00',
                'dismantle_explainer_settle_mode' => 0,
                'standard_push_id' => 0,
                'standard_push_name' => '',
                'standard_push_money' => '0.00',
                'device_number' => $contract['eq_count']
            ];
            // 循环给数据赋值
//            $data = $this->writeCommission($commissionResult,$data);
//            // 推荐人的10块钱 推荐人一定是经销商
//            $data = $this->recommendCommission($commissionResult,$data);
        }
        //老系统客户得合同仅更新拆机讲解人
        if($contract_rank['is_old_rank']==0){
            $data['push_apply_is_old'] = $contract['is_old_client'];
            /**********************新装讲解人******************/
            $user_explainer = $contract['user_explainer'];
            //若合同未设置新装讲解人,继续取工单的讲解人
            if(empty($user_explainer)){
                $user_explainer = $work_order['explainer'];
            }
            if (!empty($user_explainer)) {
                $explainer_id = substr($user_explainer, 2);
                if (strtolower(substr($user_explainer, 0, 1)) == 'd') {
                    //经销商
                    $this->Achievement->table = 'customer';
                    $dealer = $this->Achievement->findData(['user_id' => $explainer_id, 'is_dealer' => 1, 'is_delete' => 0], 'user_id,realname,username,nickname');
                    if (empty($dealer)) {
                        return [false,'讲解人不存在或不是经销商'];
                    }
                    $data['explainer_type'] = 1;
                    $data['explainer_id'] = $dealer['user_id'];
                    $data['explainer_name'] = empty($dealer['realname']) ? (empty($dealer['nickname']) ? $dealer['username'] : urldecode($dealer['nickname'])) : $dealer['realname'];
                } else {
                    //工程人员
                    $this->Achievement->table = 'engineers';
                    $engineers = $this->Achievement->findData(['engineers_id' => $explainer_id, 'is_delete' => 0], 'engineers_id,engineers_name');
                    if (empty($engineers)) {
                        return [false,'讲解人不存在或不是工程人员'];
                    }
                    $data['explainer_type'] = 2;
                    $data['explainer_id'] = $engineers['engineers_id'];
                    $data['explainer_name'] = $engineers['engineers_name'];
                }
            }
            /**********************星级讲解人:经销商的讲解人（讲解经销商成为区域代理的人）-2022/4/1******************/
            //查询推荐人是否是经销商并且存在讲解人
//            if (!empty($contract['source'])) {
//                //查询推荐人得讲解人
//                $this->Achievement->table = 'oa_workers';
//                $result = $this->Achievement->findJoinData(['user_id' => $contract['source']], 'workers_explainer');
//                if (!empty($result) && !empty($result['workers_explainer'])) {
//                    $explainer = explode('-', $result['workers_explainer']);
//                    $explainer_id = $explainer[1];
//                    $explainer_type = $explainer[0];
//                    if ($explainer_type == 'D') {
//                        //经销商
//                        $this->Achievement->table = 'customer';
//                        $dealer = $this->Achievement->findData(['user_id' => $explainer_id, 'is_dealer' => 1, 'is_delete' => 0], 'user_id,realname,username,nickname');
//                        if (empty($dealer)) {
//                            return [false,'星级讲解人不存在或不是经销商'];
//                        }
//                        $data['star_explainer_type'] = 1;
//                        $data['star_explainer_id'] = $dealer['user_id'];
//                        $data['star_explainer_name'] = empty($dealer['realname']) ? (empty($dealer['nickname']) ? $dealer['username'] : urldecode($dealer['nickname'])) : $dealer['realname'];
//                    } else {
//                        //工程人员
//                        $this->Achievement->table = 'engineers';
//                        $engineers = $this->Achievement->findData(['engineers_id' => $explainer_id, 'is_delete' => 0], 'engineers_id,engineers_name');
//                        if (empty($engineers)) {
//                            return [false,'星级讲解人不存在或不是工程人员'];
//                        }
//                        $data['star_explainer_type'] = 2;
//                        $data['star_explainer_id'] = $engineers['engineers_id'];
//                        $data['star_explainer_name'] = $engineers['engineers_name'];
//                    }
//                }
//            }

            foreach ($Relation as $key => $value) {
                $name = empty($value['realname']) ? (empty($value['nickname']) ? $value['username'] : urldecode($value['nickname'])) : $value['realname'];
                switch ($value['position']) {
                    case 0:
                        //用户
                        break;
                    case 1:
                        //推荐人(返券)
                        if ($value['is_dealer'] == 0) {
                            $data['push_apply_id'] = $value['user_id'];
                            $data['push_apply_name'] = $name;
                            $data['push_apply_money_type'] = 1;
                            $data['push_apply_type'] = $this->pushApplyType($value['user_id']);
                        }
                        break;
                    case 2:
                        //经销商
                        $data['manage_id'] = $value['user_id'];
                        $data['manage_name'] = $name;
                        $data['manage_state'] = 1;
                        $data['manage_level'] = $value['agent_level'];
                        $data['manage_money'] = $value['money']??0;
                        break;
                    case 3:
                        //一级代理经销商
                        $data['star_manage_id'] = $value['user_id'];
                        $data['star_manage_name'] = $name;
                        $data['star_manage_state'] = 1;
                        $data['star_manage_level'] = $value['agent_level'];
                        $data['star_manage_money'] = $value['money']??0;
                        break;
                    case 4:
                        //间推 一级代理经销商 推荐人（必须为代理经销商 切等级比一级代理经销商高）
                        $data['two_manage_id'] = $value['user_id'];
                        $data['two_manage_name'] = $name;
                        $data['two_manage_state'] = 1;
                        $data['two_manage_level'] = $value['agent_level'];
                        $data['two_manage_money'] = $value['money'];
                        break;
                    case 5:
                        // 二级代理经销商
                        $data['five_manage_id'] =  $value['user_id'];
                        $data['five_manage_name'] = $name;
                        $data['five_manage_state'] = 1;
                        $data['five_manage_level'] = $value['agent_level'];
                        $data['five_manage_money'] = $value['money'];

                        break;
                    case 6:
                        // 三级代理经销商
                        $data['ten_manage_id'] =  $value['user_id'];
                        $data['ten_manage_name'] = $name;
                        $data['ten_manage_state'] = 1;
                        $data['ten_manage_level'] = $value['agent_level'];
                        $data['ten_manage_money'] = $value['money'];
                        break;
                    case 7:
                        // 四级代理经销商
                        $data['twenty_manage_id'] =  $value['user_id'];
                        $data['twenty_manage_name'] = $name;
                        $data['twenty_manage_state'] = 1;
                        $data['twenty_manage_level'] = $value['agent_level'];
                        $data['twenty_manage_money'] = $value['money'];
                        //20星经销商
                        break;
                    default:
                        return [false,'等级关系异常'];
                }
            }
            if ($huaStatus) {
                //运营中心和城市合伙人结算关系
                $this->Achievement->table = 'customer_administrative_center';
                $customer_administrative_center_info = $this->Achievement->findData(['user_id' => $work_order['user_id']], 'administrative_id,operation_id');
                if (empty($customer_administrative_center_info) || empty($customer_administrative_center_info['administrative_id']) || empty($customer_administrative_center_info['operation_id'])) {
                    return [false,'用户不属于任何运营中心或者城市合伙人'];
                }
                //城市合伙人分成配置
                $this->Achievement->table = 'administrative_commission_config';
                $administrative_commission_config = $this->Achievement->findData(['a_id' => $customer_administrative_center_info['administrative_id'], 'equipments_id' => $work_order['equipments_id'], 'is_deleted' => 2], 'money,a_id');
                if (empty($administrative_commission_config)) {
                    $administrative_commission_config = $this->Achievement->findData(['a_id' => -1, 'equipments_id' => $work_order['equipments_id'], 'is_deleted' => 2], 'money,a_id');
                }
                $administrative_money = $administrative_commission_config['money'] ?? 0;
                //运营中心分成配置
                $this->Achievement->table = 'operation_commission_config';
                $operation_commission_config = $this->Achievement->findData(['o_id' => $customer_administrative_center_info['operation_id'], 'equipments_id' => $work_order['equipments_id'], 'is_deleted' => 2], 'money,o_id');
                if (empty($operation_commission_config)) {
                    $operation_commission_config = $this->Achievement->findData(['o_id' => -1, 'equipments_id' => $work_order['equipments_id'], 'is_deleted' => 2], 'money,o_id');
                }
                $operation_money = $operation_commission_config['money'] ?? 0;
                $operation_money = $operation_money;

                $data['operation_id'] = $customer_administrative_center_info['operation_id'];
                $data['operation_money'] = formatMoney($operation_money, 2);
                $data['administrative_id'] = $customer_administrative_center_info['administrative_id'];
                $data['administrative_money'] = formatMoney($administrative_money, 2);
            }


            //渠道结算关系
            $this->Achievement->table = 'customer';
            $channel_user_info = $this->Achievement->findData(['user_id' => $work_order['user_id']], 'user_belong_to_promoter,user_belong_to_channel');
            //团长和团员
            if (!empty($channel_user_info['user_belong_to_promoter'])) {
                $this->Achievement->table = 'channel_promoters';
                $channel_promoters_info = $this->Achievement->findData(['channel_promoter_id' => $channel_user_info['user_belong_to_promoter'], 'is_deleted' => 2, 'status' => 1], 'channel_promoter_id,pid,level');
                $this->Achievement->table = 'channel_promoters_commission_config';
                if (!empty($channel_promoters_info)) {
                    $channel_promoters_leader_config = [];
                    if (!empty($channel_promoters_info['pid'])) {
                        unset($where);
                        $this->Achievement->table = 'channel_promoters';
                        $channel_promoters_leader_info = $this->Achievement->findData(['user_id' => $channel_promoters_info['pid'], 'is_deleted' => 2, 'status' => 1], 'channel_promoter_id');
                        $where['channel_promoter_id'] = $channel_promoters_leader_info['channel_promoter_id'];
                        $where['equipments_id'] = $work_order['equipments_id'];
                        $where['is_deleted'] = 2;
                        $this->Achievement->table = 'channel_promoters_commission_config';
                        $channel_promoters_leader_config = $this->Achievement->findData($where, 'money'); //分成配置
                    }
                    unset($where);
                    $channel_promoters_id = $channel_user_info['user_belong_to_promoter'];
                    $where['channel_promoter_id'] = $channel_promoters_id;
                    $where['equipments_id'] = $work_order['equipments_id'];
                    $where['is_deleted'] = 2;
                    $channel_promoters_config = $this->Achievement->findData($where, 'money'); //分成配置
                    $data['channel_promoter_leader_id'] = $channel_promoters_info['channel_promoter_id'];
                    $data['channel_promoter_leader_money'] = formatMoney($channel_promoters_config['money'], 2);
                    if (!empty($channel_promoters_leader_config)) {
                        $data['channel_promoter_leader_id'] = $channel_promoters_leader_info['channel_promoter_id'];
                        $data['channel_promoter_leader_money'] = formatMoney($channel_promoters_leader_config['money'], 2);
                        $data['channel_promoter_id'] = $channel_promoters_info['channel_promoter_id'];
                        $data['channel_promoter_money'] = formatMoney($channel_promoters_leader_config['money'], 2);
                    }
                }
            }
            //渠道
            unset($where);
            if (!empty($channel_user_info['user_belong_to_channel'])) {
                $where['channel_id'] = $channel_user_info['user_belong_to_channel'];
                $where['equipments_id'] = $work_order['equipments_id'];
                $where['is_deleted'] = 2;
                $this->Achievement->table = 'channel_commission_config';
                $channel_config = $this->Achievement->findData($where, 'money'); //分成配置
                if (empty($channel_config)) { //如果未单独设置，取默认设置
                    $where['channel_id'] = -1;
                    $channel_config = $this->Achievement->findData($where, 'money'); //分成配置
                }
                $data['channel_id'] = $channel_user_info['user_belong_to_channel'];
                $data['channel_money'] = formatMoney($channel_config['money'], 2);
            }

        }

        /**********************拆机讲解人20230617******************/

        if (!empty($dismantle_user_explainer)) {
            $dismantle_explainer_id = substr($dismantle_user_explainer, 2);
            if (strtolower(substr($dismantle_user_explainer, 0, 1)) == 'd') {
                //经销商
                $this->Achievement->table = 'customer';
                $dealer = $this->Achievement->findData(['user_id' => $dismantle_explainer_id, 'is_dealer' => 1, 'is_delete' => 0], 'user_id,realname,username,nickname');
                if (empty($dealer)) {
                    return [false,'讲解人不存在或不是经销商'];
                }
                $data['dismantle_explainer_settle_mode'] = $dismantle_user_explain_settle_mode;
                $data['dismantle_explainer_type'] = 1;
                $data['dismantle_explainer_id'] = $dealer['user_id'];
                $data['dismantle_explainer_name'] = empty($dealer['realname']) ? (empty($dealer['nickname']) ? $dealer['username'] : urldecode($dealer['nickname'])) : $dealer['realname'];
            } else {
                //工程人员
                $this->Achievement->table = 'engineers';
                $engineers = $this->Achievement->findData(['engineers_id' => $dismantle_explainer_id, 'is_delete' => 0], 'engineers_id,engineers_name');
                if (empty($engineers)) {
                    return [false,'讲解人不存在或不是工程人员'];
                }
                $data['dismantle_explainer_settle_mode'] = $dismantle_user_explain_settle_mode;
                $data['dismantle_explainer_type'] = 2;
                $data['dismantle_explainer_id'] = $engineers['engineers_id'];
                $data['dismantle_explainer_name'] = $engineers['engineers_name'];
            }
        }
        $this->Achievement->table = 'contract_rank';
        if (!empty($contract_rank)) {
            $data['device_number'] = $contract_rank['device_number'];
            if ($contract_rank['can_update'] == 2 && empty($dismantle_user_explainer)) {
                return [false,'不允许更新关系2!'.$contract_rank['remarks']];
            }
            //如何是非新装合同,取父级折算台数
            if(empty($contract_rank['device_number']) && !empty($contract['pid'])){
                $this->Achievement->table = 'contract';
                $parent_contract =  $this->Achievement->findData(['contract_id' => $contract['pid']], 'contract_no');
                $this->Achievement->table = 'contract_rank';
                $parent_contract_rank = $this->Achievement->findData(['contract_no' => $parent_contract['contract_no']], 'id,can_update,remarks,device_number');
                $data['device_number'] = $parent_contract_rank['device_number'];
            }
            $this->Achievement->updateData(['id' => $contract_rank['id']], $data);
            //更新客户表更新状态
            $this->Achievement->table = 'customer';
            $this->Achievement->updateData(['user_id' => $contract['user_id']], ['is_update_relation' => 1]);
            return [true,'更新关系成功'];
        }
        $this->Achievement->insertData($data);
        return [true,'创建关系成功'];
    }

    /**
     * 获取等级
     * @param array $line
     * @param array $data
     * @param array $result
     * @return array
     * @date 2019/1/18 10:12
     * @author ligang
     */
    protected function findRelation(array $line, array $data, array &$result = [])
    {
        //先到先得
        //字段        用户   推荐人   经销商      1星   2星    5星  10星   20星
        //position      0      1        2     3     4      5    6      7
        //position      0      1    代理经销商   代理经销商额外    代理经销商推荐人      5    6      7
        $star = false;
        $one_star = false;
        $two_star = false;
        //处理代理经销商关系
        $agent_workers = [];
        $this->Achievement->table = 'oa_workers';
        // 所有代理经销商
        $agent_dealer = $this->Achievement->selectData(['is_agent_dealer'=>1],'agent_level,user_id');
        $agent_dealer_arr = [];
        if(!empty($agent_dealer)){
            foreach ($agent_dealer as $kk=>$vv){
                $agent_dealer_arr[$vv['user_id']] = $vv['agent_level'];
            }
            unset($kk);
            unset($vv);
        }
        foreach ($line as $k=>&$v){
            if($v['is_agent_dealer']==0 || $k==0){//排除当前用户以及非代理经销商
                continue;
            }
            //获取代理经销商级别
            $v['agent_level'] = $agent_dealer_arr[$v['user_id']];
            array_push($agent_workers,$v);
        }
        //寻找极差
        if(!empty($agent_workers)){
            $level = $agent_workers[0]['agent_level'];
            $agent_level = [];
            $unit = 10;//每个等级金额
            foreach ($agent_workers as $kk=>&$vv){
                if($vv['agent_level'] > $level){
                    $vv['money'] = bcmul(bcsub($vv['agent_level'],$level,0),$unit,0);
                    $level = $vv['agent_level'];
                    array_push($agent_level,$vv);
                }
                if($vv['agent_level']==4){
                    break;
                }
            }
        }
        unset($kk);
        unset($vv);
        //$dealerSource = 0;// 经销商 推荐人
        $totalCommission = 150;// 佣金总额
        $dealerUnit = 100; // 经销商 金额
        $unit = 10; // 等级差 单位 一级10块

        $agent_level_ids = [];
        foreach ($line as $key => $value) {
            if ($key == 0) {
                //第一个一定是用户
                $value['position'] = 0; //用户标识
                array_push($result, $value);
                continue;
            }
            //第二个 必定是 推荐人 且有可能是经销商、(代理经销商和经销商)、(普通用户)  三种可能
            if($key==1){
                // 推荐人必须是普通用户才返券
                if($value['is_dealer']==0 && $value['is_agent_dealer']==0){
                    //推荐人是普通用户
                    $value['position'] = 1; //推荐人标识
                    array_push($result, $value);
                }
                // 推荐人是经销商
                if($value['is_dealer']==1  && $value['is_agent_dealer']==0){
                    //推荐人是经销商
                    $value['position'] = 2; //经销商
                    $value['money'] = $dealerUnit;
                    $totalCommission = bcsub($totalCommission,$value['money'],0);
                    $star = true;
                    array_push($result, $value);
                }
                // 推荐是代理经销商 又是 经销商
                if( $value['is_agent_dealer']==1){
                    //推荐人是代理经销商
                    $value['position'] = 2; //经销商
                    $value['money'] = $dealerUnit;
                    $totalCommission = bcsub($totalCommission,$value['money'],0);
                    $star = true;
                    array_push($result, $value);
                    //推荐人是代理经销商
                    $value['position'] = 3; //代理经销商
                    $value['money'] = bcmul($value['agent_level'],$unit,0);
                    $totalCommission = bcsub($totalCommission,$value['money'],0);
                    if($value['user_id'] != 2){
                        $agent_level_ids[] = $value['user_id'];
                    }
                    $one_star = true;
                    array_push($result, $value);
                }
                continue;
            }
            if(!empty($agent_workers)) {
                //寻找经销商
                if (!$star && $value['is_dealer'] == 1) {
                    //推荐人是代理经销商
                    $value['position'] = 2; //经销商
                    $value['money'] = $dealerUnit;
                    $totalCommission = bcsub($totalCommission,$value['money'],0);
                    $star = true;
                    array_push($result, $value);
                }

                //寻找第一个代理经销商
                if (!$one_star && $value['is_agent_dealer'] == 1) {
                    $value['position'] = 3; //代理经销商
                    $value['money'] = bcmul($value['agent_level'], $unit, 0);
                    $totalCommission = bcsub($totalCommission,$value['money'],0);
                    $one_star = true;
                    if($value['user_id'] != 2){
                        $agent_level_ids[] = $value['user_id'];
                    }
                    array_push($result, $value);
                }
//                //获取代理经销商的推荐人 // 经销商的推荐人
//                if(!$two_star && $commendUser == $value['user_id']){
//                    $value['position'] = 4; //代理经销商推荐人
//                    $value['money'] = $unit;
//                    $totalCommission = bcsub($totalCommission,$value['money'],0);
//                    $two_star = true;
//                    array_push($result, $value);
//                }
            }
        }
        //获取代理经销商极差
        $position = 5;
        if(!empty($agent_level)){
            foreach ($agent_level as $kk=>&$vv){
                $vv['position'] = $position; //代理经销商推荐人
                if($totalCommission < $vv['money']){
                    $vv['money'] = $totalCommission;
                    if($vv['user_id'] != 2){ // 把参与极差分成的代理经销商 保存起来 周建华除外
                        $agent_level_ids[] = $vv['user_id'];
                    }
                    array_push($result, $vv);
                    break;
                }else{
                    $totalCommission = bcsub($totalCommission,$vv['money'],0);
                    if($vv['user_id'] != 2){
                        $agent_level_ids[] = $vv['user_id'];
                    }
                    array_push($result, $vv);
                    $position++;
                }

            }
        }
        // TODO 2024-06-05 17:32 娟姐电话沟通 推荐人没有上一级 钱返给周建华
        if(empty($agent_workers)){
            // 如果用户直接没得推荐人 分100 有推荐人 分 150
            $result[] = $this->getZhou(2,100);// 经销商
            $result[] = $this->getZhou(3,40);// 一级代理经销商
        }

        // 用户代理经销商推荐人10块
        $result[] = $this->findAgentRecommendUserId($line,$agent_level_ids,$result);
//        dump($result);
//        // 推荐人
//        $result[] = $this->findRecommendUser($line,$commendUser,$agent_level_ids);
        return $result;
    }

    // 获取周建华的账号
    public function getZhou($position,$money): array
    {
        $workersIdZhou = 2;
        $workers = $this->Achievement->findData(['workers_id'=>$workersIdZhou]);
        return [
            "user_id" => $workers['user_id'],
            "source" =>0,
            "account" =>  $workers['workers_number'],
            "realname" =>  $workers['workers_name'],
            "username" => $workers['workers_name'].".".$workers['workers_phone'],
            "nickname" => "%E5%91%A8%E5%BB%BA%E5%8D%8E%C2%B713320256975",
            "is_dealer" => 1,
            "check_out" => $workers['check_out'],
            "agent_level" =>$workers['agent_level'],
            "agent_pid" => 0,
            "is_agent_dealer" =>$workers['is_agent_dealer'],
            'position'=>$position,
            'money'=>$money
        ];
    }

    /**
     * 寻找推荐人上级  必须为代理经销商
     * @param array $allUser 用户关系组
     * @param array $agent_level_ids 参与极差的代理经销商
     * @param array $result 分佣关系
    */
    public function findAgentRecommendUserId(array $allUser,array $agent_level_ids,array $result)
    {
        $is_ok = false;
        foreach ($result as $k=>$v){
            // 周建华存在经销商身份 推荐奖给总账户
            if($v['user_id'] == 2 && $v['position'] == 2){
                $is_ok = true;
                break;
            }
        }
        if($is_ok){
            dump('周建华经销商身份,推荐奖给总账户');
            return [
                "user_id" => 1109,
                "source" =>0,
                "account" =>  891539,
                "realname" =>  '总账户',
                "username" => '总账户',
                "nickname" => "",
                "is_dealer" => 1,
                "check_out" => 1,
                "agent_level" =>0,
                "agent_pid" => 0,
                "is_agent_dealer" =>0,
                'position'=>4, // 推荐奖标识
                'money'=>10
            ];
        }
        // TODO 寻找推荐人 推荐人必须为代理经销商  且为第一个代理经销商的上级 且等级比第一个代理经销商 等级高
        if(count($allUser) <= 1){
            return $this->getZhou(4,10);
        }

        $first = [];
        // 找出推荐人的第一个代理经销商
        foreach ($allUser as $k=>$v){
            if($v['is_agent_dealer'] == 1){
                $first = $v;
                break;
            }
        }
        if(empty($first)){
            return $this->getZhou(4,10);
        }
        $commend = [];
        foreach ($allUser as $k=>$v){
            if($v['is_agent_dealer'] == 1 && !in_array($v['user_id'],$agent_level_ids)){
                $v['position'] = 4;
                $v['money'] = 10;
                $commend = $v;
                break;
            }
        }
        if(empty($commend)){
            $commend = $this->getZhou(4,10);
        }
        return $commend;
    }


    /**
     * 获取上级(从用户本身开始)
     * @param array $data 原始数据
     * @param string $start 开始人-之前用户的上一级
     * @param array $result 返回结果
     * @param array $group 已存在组，一维数组，ID
     * @return array|bool
     * @date 2019/2/28 16:40
     * @author ligang
     */

    protected function findHigherUp(array $data, string $start,string $agent_pid, array &$result = [], array &$group = [], bool &$ring = false,int &$index=0)
    {

        foreach ($data as $key => $value) {
            if ($value['user_id'] == $start) {
                if (in_array($value['source'], $group)) {
                    //该用户的推荐人已经在组里面，说明该层级关系已经闭环
                    $ring = true;
                    return false;
                }
                //用户直接推荐人
                if($index==0) {
                    array_push($group, $value['user_id']);
                    array_push($result, $value);
                    //若用户指定了代理经销商,从代理经销商开始查
                    $index++;
                    if (!empty($agent_pid)) {
                        $this->findHigherUp($data, $agent_pid,0, $result, $group, $ring,$index);
                    }else{
                        if ($value['source'] != 0) {
                             $this->findHigherUp($data, $value['source'],0, $result, $group, $ring,$index);
                        }
                        continue;
                    }
                }
                array_push($group, $value['user_id']);
                array_push($result, $value);
                if ($value['source'] != 0) {
                    $index++;
                    $this->findHigherUp($data, $value['source'],0, $result, $group, $ring,$index);
                }
            }
        }
        return $ring ? false : $result;
    }

    /**
     * @查询用户的经销商或代理经销商
     * @param int $type 查询类型 经销商  1 经销商 2 代理经销商
     * @param array $allUser 所有用户
     * @param string $startUserId 开始查询的用户Id
     * @param string $agent_pid 设置的代理经销商
     * @param array $result
     * @param array $group
     * @param bool $ring
     * @param int $index
     * @return array|bool
     */
    protected function findUserDealerOrAgentDealer(int $type,array $allUser, string $startUserId,string $agent_pid, array &$result = [], array &$group = [], bool &$ring = false,int &$index=0)
    {
        foreach ($allUser as $key => $value) {
            if ($value['user_id'] == $startUserId) {
                if (in_array($value['source'], $group)) {
                    //该用户的推荐人已经在组里面，说明该层级关系已经闭环
                    $ring = true;
                    return false;
                }
                // 上级存在上级
                if($type == 1){
                    if($value['source'] != 0){
                        if($value['is_dealer'] == 1){
                            array_push($group, $value['user_id']);
                            array_push($result, $value);
                        }
                        if($agent_pid){
                            $this->findUserDealerOrAgentDealer(2,$allUser, $agent_pid,0, $result, $group, $ring,$index);
                        }else{
                            $this->findUserDealerOrAgentDealer($type,$allUser, $value['source'],0, $result, $group, $ring,$index);
                        }

                    }

                }
                if($type == 2){
                    if($value['source'] != 0){
                        if($value['is_agent_dealer']){
                            array_push($group, $value['user_id']);
                            array_push($result, $value);
                        }
                        $start = $value['source'];
                        if($value['agent_pid']){
                            $start = $value['agent_pid'];
                        }
                        $this->findUserDealerOrAgentDealer($type,$allUser, $start,0, $result, $group, $ring,$index);
                    }
                }
            }
        }
        return $ring ? false : $result;
    }
    /**
     * 获取经销商(代理经销商分佣情况)
     */
    public function getOaWorkersCommission(array $data): array
    {
        //TODO 关系      用户(设置了代理经销商)   推荐人（设置的代理经销商）  （代理经销商的上级）一级   二级         三级       四级
        //TODO 关系      用户                  推荐人                     推荐人的上级）一级      二级         三级       四级
        //TODO 字段(150)        用户     推荐人(10)    一级代理经销商(110)    二级(120)    三级(130)    四级(140)
        //TODO 字段(150)        用户     推荐人(10)    经销商(100)           1星           2星        5星        10星       20星
        //TODO position         0         1              2               3              4         5           6          7

        // 读取配置
        $settlementConfig = get_instance()->config['settlement_commission'];
        // TODO tag 1 经销商 2 推荐人
        $money = $settlementConfig['total_commission'];
        $syMoney = $money;
        $result = [];
        $tag = 1;
        $is_dealer = true;
        foreach ($data as $k=>$value){
            if(!$k){
                // 获取推荐人 (当前合同用户的推荐人的推荐人)
                $recommendUser = $this->getRecommendUser((int)$value['source'],$settlementConfig['is_recommend']);
                if(!empty($recommendUser)){
                    $result[] = $recommendUser;
                    // 推荐人的上级 减去推荐人的佣金
                    $syMoney = $money - $settlementConfig['is_recommend'];// 剩余佣金
                }
                // 用户推荐人是经销商
                if($value['is_dealer']){
                    $thisArr = $value;
                    $thisArr['money'] = $settlementConfig['dealer_commission'];
                    $thisArr['tag'] = $tag;// 经销商
                    $result[] = $thisArr;
                    $syMoney = $syMoney - $thisArr['money'];
                    $tag += 1;
                    $is_dealer = false;
                }
                // 用户推荐人是代理经销商
                if($value['is_agent_dealer']){
                    $thisArr = $value;
                    $commission = $this->getCommissionMoney($value['agent_level'],$settlementConfig);
                    $thisArr['money'] = $commission;
                    $thisArr['tag'] = $tag;// 代理经销商
                    $result[] = $thisArr;
                    $tag += 1;
                    $syMoney = $syMoney - $thisArr['money'];
                }
            }else{
                if($value['is_agent_dealer']){
                    $thisArr = $value;
                    // 把层级的第一位经销商写入到经销商中
                    if($value['is_dealer'] && $is_dealer){
                        $thisArr['money'] = $settlementConfig['dealer_commission'];
                        $thisArr['tag'] = 1;// 经销商
                        $result[] = $thisArr;
                        $tag += 1;
                        $syMoney = $syMoney - $thisArr['money'];
                        $is_dealer = false;
                    }
                    // 级差
                    $levelCha = $this->getAgentLevelCha($data,$k,$value['agent_level']);// 获取下级的等级
                    if(!$levelCha){
                        // 没有级差 不分佣
                        $thisArr['money'] = 0;
                        $thisArr['tag'] = $tag;
                        $tag += 1;
                        $result[] = $thisArr;
                        continue;
                    }
                    // 级差所得佣金
                    $commission = $levelCha * 10;
                    // 剩余金额小于级差所得金额 全部划分给当前代理经销商
                    if($syMoney <= $commission){
                        $thisArr['money'] = $commission;
                        $thisArr['tag'] = $tag;
                        $result[] = $thisArr;
                        break;// 结束循环 佣金已分完
                    }
                    $syMoney = $syMoney - $commission;
                    $thisArr['money'] = $commission;
                    $thisArr['tag'] = $tag;
                    $tag += 1;
                    $result[] = $thisArr;
                }else{
                    // 把层级的第一位经销商写入到经销商中
                    if($value['is_dealer'] && $is_dealer){
                        $thisArr = $value;
                        $thisArr['money'] = $settlementConfig['dealer_commission'];
                        $thisArr['tag'] = 1;// 经销商
                        $result[] = $thisArr;
                        $tag += 1;
                        $syMoney = $syMoney - $thisArr['money'];
                        $is_dealer = false;
                    }
                }

            }
        }
        return $result;
    }

    public function getCommissionMoney(int $level,array $settlementConfig): int
    {
        $money = 0;
        switch ($level){
            case 1:$money = $settlementConfig['agent_level_first'];break;
            case 2:$money = $settlementConfig['agent_level_two'];break;
            case 3:$money = $settlementConfig['agent_level_three'];break;
            case 4:$money = $settlementConfig['agent_level_four'];break;
        }
        return $money;
    }

    public function getAgentLevelCha(array $data,int $k,int $level)
    {
        $user = $data[$k-1];
        // 只获取代理经销商的等级
        $levelCha = $level - $user['agent_level'];
        if($levelCha <= 0){
            return 0;
        }
        return $levelCha;
    }

    public function getRecommendUser(int $userId,int $recommendMoney): array
    {

        $field = '
            rq_customer.user_id,
            rq_customer.source,
            rq_customer.account,
            rq_customer.realname,
            rq_customer.username,
            rq_customer.nickname,
            rq_customer.is_dealer,
            rq_oa_workers.check_out,
            rq_oa_workers.agent_level,
            rq_oa_workers.workers_name,
            rq_oa_workers.is_agent_dealer as workers_is_agent_dealer,
            rq_customer.agent_pid,
            rq_customer.is_agent_dealer
        ';
        $join = [
            ['oa_workers', 'rq_oa_workers.user_id = rq_customer.user_id', 'LEFT']
        ];
        $this->Achievement = get_instance()->loader->model('AchievementModel', get_instance());
        $this->Achievement->table = 'customer';
        $user = $this->Achievement->findJoinData(['rq_customer.user_id'=>$userId], $field,$join);
        return    [
            "user_id" => $user['user_id'],
            "source" =>$user['source'],
            "account" =>  $user['account'],
            "realname" =>  $user['realname'],
            "username" => $user['username'],
            "nickname" => $user['nickname'],
            "is_dealer" =>$user['is_dealer'],
            "check_out" => $user['check_out'],
            "agent_level" =>$user['agent_level'],
            "agent_pid" => $user['agent_pid'],
            "is_agent_dealer" =>$user['is_agent_dealer'],
            "money"=>$recommendMoney,//推荐人佣金
            'tag'=>999,// 推荐人
        ];
    }

    protected function contractUserIdentity($contract): string
    {
        if($contract['is_agent_dealer']){
            $desc = '代理经销商';
        }else{
            if($contract['is_dealer']){
                $desc = '经销商';
            }else{
                $desc = '用户';
            }
        }
        return $desc;

    }

    public function pushApplyType($user_id): int
    {
        //如果只有商用合同视为商用客户
        $this->Achievement->table = 'contract';
        $contract_info = $this->Achievement->selectData(['user_id'=>$user_id,'status'=>['IN',[4,5,9,10]]],'contract_equipments_line');
        $contract_equipments_line = array_column($contract_info,'contract_equipments_line');
        if(empty($contract_equipments_line)){
            return 1;
        }
        if(in_array(1,$contract_equipments_line)){
            return 1;
        }
        return 2;
    }

    public function recommendCommission($commissionResult,$data)
    {
        foreach ($commissionResult as $k=>$value){
            // 推荐人的10块
            if($value['tag'] == 999 && $value['is_dealer']){
                // 推荐人是经销商
                if($data['manage_id'] == $value['user_id']){
                    $data['manage_origanl_money'] = $data['manage_origanl_money'] + $value['money'];
                    $data['manage_money'] = $data['manage_money'] + $value['money'];
                }
                // 第一级代理经销商
                else  if($data['star_manage_id'] == $value['user_id']){
                    $data['star_manage_money'] = $data['star_manage_money'] + $value['money'];
                }
                // 第二级代理经销商
                else if($data['two_manage_id'] == $value['user_id']){
                    $data['two_manage_money'] = $data['two_manage_money'] + $value['money'];
                }
                // 第三级代理经销商
                else if($data['five_manage_id'] == $value['user_id']){
                    $data['five_manage_money'] = $data['five_manage_money'] + $value['money'];
                }
                // 第四级代理经销商
                else if($data['ten_manage_id'] == $value['user_id']){
                    $data['ten_manage_money'] = $data['ten_manage_money'] + $value['money'];
                }
                // 第五级代理经销商
                else if($data['twenty_manage_id'] == $value['user_id']){
                    $data['twenty_manage_money'] = $data['twenty_manage_money'] + $value['money'];
                }
                break;
            }
        }
        return $data;
    }

    // 分佣情况 写入关系表
    public function writeCommission($commissionResult,$data)
    {
        foreach ($commissionResult as $value){
            $name = empty($value['realname']) ? (empty($value['nickname']) ? $value['username'] : urldecode($value['nickname'])) : $value['realname'];
            // 经销商分佣
            if($value['tag'] == 1 && $value['is_dealer']){
                $data['manage_id'] = $value['user_id'];
                $data['manage_name'] = $name;
                $data['manage_origanl_money'] = $value['money'];
                $data['manage_money'] = $value['money'];
                $data['manage_state'] = ($value['check_out'] == 1) ? 1 : 2;
            }
            if($value['tag'] == 2){
                $data['star_manage_id'] = $value['user_id'];
                $data['star_manage_name'] = $name;
                $data['star_manage_money'] = $value['money'];
                $data['star_manage_state'] = ($value['check_out'] == 1) ? 1 : 2;
            }
            if($value['tag'] == 3){
                $data['two_manage_id'] = $value['user_id'];
                $data['two_manage_name'] = $name;
                $data['two_manage_money'] = $value['money'];
                $data['two_manage_state'] = ($value['check_out'] == 1) ? 1 : 2;
            }
            if($value['tag'] == 4){
                $data['five_manage_id'] = $value['user_id'];
                $data['five_manage_name'] = $name;
                $data['five_manage_money'] = $value['money'];
                $data['five_manage_state'] = ($value['check_out'] == 1) ? 1 : 2;
            }
            if($value['tag'] == 5){
                $data['ten_manage_id'] = $value['user_id'];
                $data['ten_manage_name'] = $name;
                $data['ten_manage_money'] = $value['money'];
                $data['ten_manage_state'] = ($value['check_out'] == 1) ? 1 : 2;
            }
            if($value['tag'] == 6){
                $data['twenty_manage_id'] = $value['user_id'];
                $data['twenty_manage_name'] = $name;
                $data['twenty_manage_money'] = $value['money'];
                $data['twenty_manage_state'] = ($value['check_out'] == 1) ? 1 : 2;
            }
            // 推荐人返券
            if($value['tag'] == 999 && $value['is_dealer'] == 0 && $value['is_agent_dealer'] == 0){
                $data['push_apply_id'] = $value['user_id'];
                $data['push_apply_name'] = $name;
                $data['push_apply_money_type'] = 1;
                $data['push_apply_type'] = $this->pushApplyType($value['user_id']);
            }
        }
        return $data;
    }

    // 获取更新关系通道
    public function relationPassageway($contract): bool
    {
        $time = strtotime('2024-06-01');
        //$time = strtotime(get_instance()->config['newBalanceWayTime']);
        // 如果自定义了 安装时间 去安装时间 否则取装机时间
        // 如果都为空 取当前时间
        $install = $contract['sign_date'] ?: $contract['installed_time'];
        $install = $install ?: time();
        return  $install > $time;
    }

    public static function contractInstalledTime(int $install,int $sign): int
    {
        // 阶段2 2023-06-01 00:00:00 - 2024-05-31 23:59:59
        $installed_time_start_20230601 = 1685548800; // 2023-06-01 00:00:00
        $installed_time_end_20240531 = 1717171199; // 2024-05-31 23:59:59

        // 阶段1 2019-01-01 00:00:00 - 2023-5-31 23:59:59
        $installed_time_start_20190101 = 1546272000; // 2019-01-01 00:00:00
        $installed_time_end_20230531 = 1685548799;// 2023-5-31 23:59:59
        $installed = $sign ?: $install;
        $installed = $installed ?: time();
        // 通道1 安装时间在 2019-01-01 00:00:00 - 2023-5-31 23:59:59
        if($installed >= $installed_time_start_20190101 && $installed <= $installed_time_end_20230531){
            return 1;
        }
        // 通道2 安装时间在 2023-06-01 00:00:00 - 2024-05-31 23:59:59
        if($installed >= $installed_time_start_20230601 && $installed <= $installed_time_end_20240531){
            return 2;
        }

        return 0;
    }

}