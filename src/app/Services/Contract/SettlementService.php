<?php

namespace app\Services\Contract;

use app\Controllers\Achievement\Base;
use app\Library\SLog;
use app\Services\Common\ConfigService;
use app\Services\Common\ReturnCodeService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use app\Services\User\UserService;
use Endroid\QrCode\ErrorCorrectionLevel;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Table;
use Server\CoreBase\SwooleException;
use think\image\Exception;

class SettlementService
{

    protected $Achievement;
    protected $ContractModel;
    protected $CustomerModel;
    protected $EquipmentModel;
    protected $ChannelPromotersModel;
    protected $ChannelCommissionConfigModel;
    protected $userService;
    protected $ChannelModel;
    protected $ChannelPromotersCommissionConfigModel;
    protected $WorkOrderModel;
    protected $isContractRankMoney;
    protected $renewBuyMinMoney; // 续费买断最低结算金额
    protected $renewMinMoney; // 单纯续费最低结算金额
    protected $is_dk;
    protected $dkMoney;
    public function __construct()
    {
        $this->renewBuyMinMoney = 980;// 续费买断最低结算金额  不得低于980
        $this->renewMinMoney = 428; // 单纯续费最低结算金额 不得低于 428
        $this->dkMoney = 575; // 机器收款为576(含优惠券)以下的经销商提成如果是100元的都改为60元
        //$this->Achievement = get_instance()->loader->model('AchievementModel', get_instance());
        $this->Achievement = get_instance()->loader->model('AchievementModel', get_instance());
        $this->is_dk = false;
//        $this->ContractModel = $this->loader->model('ContractModel', $this);
//        $this->EquipmentModel = $this->loader->model('EquipmentModel', $this);
//        $this->CustomerModel = $this->loader->model('CustomerModel', $this);
//        $this->ChannelPromotersModel = $this->loader->model('ChannelPromotersModel', $this);
//        $this->ChannelModel = $this->loader->model('ChannelModel', $this);
//        $this->ChannelCommissionConfigModel = $this->loader->model('ChannelCommissionConfigModel', $this);
//        $this->ChannelPromotersCommissionConfigModel = $this->loader->model('ChannelPromotersCommissionConfigModel', $this);
//        $this->ChannelPromotersModel = $this->loader->model('ChannelPromotersModel', $this);
//        $this->WorkOrderModel = $this->loader->model('WorkOrderModel', $this);
//        $this->WorkOrderReturnVisitModel = $this->loader->model('WorkOrderReturnVisitModel', $this);
//        $this->OaAchievementModel = $this->loader->model('OaAchievementModel', $this);
//        $this->userService = new UserService();
//        $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
        $this->isContractRankMoney = true;//是否去合同关系表的金额,由于目前合同关系金额有BUG，暂时不取
    }

//    /**
//     * 验证是否可以进行结算
//     * @param int $is_admin 是否手动结算 0：自动结算，1：手动结算 默认自动结算
//     * @param int $type 结算类型 1：新装，2：续费，默认为1：新装
//     * @param $contract_no 合同编号
//     */
    public function balanceCheck($from_renew_order_id,$type,$contract_no,$is_admin,$is_settle=true): array
    {
        if (!in_array($type,[1,2]) || !$contract_no) {
            return [false,'参数错误'];
        }
        //合同有效性
        $field = 'rq_contract.contract_equipments_line,
        rq_contract.renew_money,
        rq_contract.contract_money,
        rq_contract.contract_class,
        rq_customer.source,
        rq_customer.source_is_default,
        rq_contract.is_special,
        rq_contract.contract_id,
        rq_contract.installed_time,
        rq_contract.effect_time,
        rq_contract.now_date,
        rq_contract.contract_no,
        rq_contract.company_id,
        rq_contract.is_buy_out,
        rq_contract.user_id,
        rq_contract.administrative_id,
        rq_contract.operation_id,
        id_card,
        realname,
        is_old_client,
        rq_equipments.grade_money,
        rq_equipments.ticket_money,
        rq_equipments.coupon_valid_time,
        is_old_contract';
        $join = [
            ['customer', 'rq_contract.user_id = rq_customer.user_id', 'LEFT'],
            ['contract_equipment', 'rq_contract.contract_id = rq_contract_equipment.contract_id', 'LEFT'],
            ['equipments', 'rq_contract_equipment.equipments_id = rq_equipments.equipments_id', 'LEFT'],
        ];
        //$this->Achievement = get_instance()->loader->model('AchievementModel', get_instance());
        $this->Achievement->table = 'contract';
        $contract = $this->Achievement->findJoinData(['contract_no' => $contract_no], $field, $join);
        if (empty($contract)) {
            return [false,'合同不存在,编号:'.$contract_no];
        }
        // TODO 自动结算验证金额
        if(!$is_admin && $is_settle){

            // 查询资金记录
            $contractService = new ContractService();
            $financeRecordData = $contractService->getFinanceRecord($contract,$type,$from_renew_order_id);
            if($financeRecordData['money'] < $this->renewMinMoney){
                return [false,'查询资金记录:合同金额小于'.$this->renewMinMoney.'不进行结算,合同:'.$contract_no];
            }
            // 租赁合同 低于428 不结算
            if ($contract['contract_class'] == 1  && $contract['renew_money'] < $this->renewMinMoney) {
                return [false,'合同金额小于'.$this->renewMinMoney.'不进行结算,合同:'.$contract_no];
            }
            if ($contract['is_buy_out'] == 1 && $contract['contract_class'] = 2 && $contract['contract_money'] < $this->renewBuyMinMoney) {
                return [false,'买断合同金额小于'.$this->renewBuyMinMoney.'不进行结算,合同:'.$contract_no];
            }
            if($contract['contract_class'] == 1 && $contract['contract_money'] < $this->renewMinMoney){
                return [false,'合同金额小于'.$this->renewMinMoney.'不进行结算,合同:'.$contract_no];
            }
        }
        // TODO 2022-10-24如果是商用机,首次续费不自动结算（只能后台财务结算）
        if($is_settle && $contract['contract_equipments_line']==2 && $is_admin==0 && $type==2){
            //判断是否是首次续费
            $this->Achievement->table = 'renew_order';
            $renew_order = $this->Achievement->findData(['contract_id' => $contract['contract_id']]);
            if(empty($renew_order)){
                return [false,'商用机首次续费不自动结算!合同编号:'.$contract_no];
            }
        }
        if (empty($contract['company_id'])) {
            return [false,'合同信息错误,请检查公司ID，合同编号：' . $contract_no];
        }
        //特殊合同,不进行结算
        if ($contract['is_special'] == 1 && !$is_admin) {
            return [false,'此合同为特殊合同,不进行结算。合同编号：' . $contract_no];
        }
        //2022-10-24 续费无推荐人直接不进行结算
        //2024-4-22 新装 续费无推荐人不能进行结算
        if (empty($contract['source'])  && $contract['is_old_contract'] != 1) {
            return [false,'此客户不存在推荐人,不能进行结算,请先设置推荐人并更新关系!编号:'.$contract_no];
        }
        //如果金额不进行结算
        if (($type == 1 && empty($contract['contract_money'])) || ($type == 2 && empty($contract['renew_money']))) {
            return [false,'此合同金额为0,不进行结算。合同编号：' . $contract_no];
        }

        //是否开启结算
        $server_maintain = ConfigService::getBalanceConfig('', 'server_maintain', $contract['company_id']);
        if (!empty($server_maintain) && $server_maintain['switch'] == 1) {
            return [false,$server_maintain['msg']];
        }

        //验证是否已经结算
        $this->Achievement->table = 'oa_achievement';
        $achievement = $this->Achievement->findData(['contract_number' => $contract_no, 'achievement_work_order_type' => 3], 'achievement_id');
        if ($is_settle && $type == 1 && !empty($achievement)) {
            return [false,'合同新装已经结算，不能重复结算，合同编号：' . $contract_no];
        }

        //获取合同层级关系
        $this->Achievement->table = 'contract_rank';
        $contract_rank = $this->Achievement->findData(['contract_no' => $contract_no]);
        if (empty($contract_rank)) {
            //应该在后台审核工单时（前台已处理），建立关系
            return [false,'该合同还不存在结算关系，无法结算1，合同编号：' . $contract_no];
        }
        if (empty($contract_rank['device_number'])) {
            //在工程人员回单时，写入的台数
            return [false,'该合同还不存在结算关系，无法结算2，合同编号：' . $contract_no];
        }
        //处理是否是老系统客户介绍，根据结算关系的用户查询
        $this->Achievement->table = 'customer';
        $contract_rank_user = $this->Achievement->findData(['user_id' => $contract_rank['contract_user_id']]);
        $contract['is_old_client'] = empty($contract_rank_user['is_old_client']) ? 0 : $contract_rank_user['is_old_client'];

        //验证特殊工单-新装工单:工程人员回单后才可以进行结算
        if ($type == 1) {
            //工单
            $order_where = [
                'contract_id' => $contract['contract_id'],
                'rq_work_order_business . business_id' => ['IN',[3,9]],
            ];
            $order_field = '
                rq_work_order_business . business_id,
                rq_work_order . explainer,
                rq_work_order . work_order_status
            ';
            $order_join = [
                ['work_order_business', 'rq_work_order . work_order_id = rq_work_order_business . work_order_id']
            ];
            $this->Achievement->table = 'work_order';
            $order = $this->Achievement->findJoinData($order_where, $order_field, $order_join);
            if (empty($order)) {
                return [false,'合同不存在新装工单，合同编号：' . $contract_no];
            }
            //工单状态最后一步为11
            if (!in_array($order['work_order_status'], [11, 12, 15, 16, 17])) {
                return [false,'请确认工单流程已完成，合同编号：' . $contract_no];
            }
        }
        return [true,['contract_rank' => $contract_rank, 'contract' => $contract]];
    }

    /**
     * 获取结算金额分配配置，账号配置(若产品管理时进行了配置则取该配置，反之取默认值（配置文件）)
     * @param array $equipments_config 产品配置信息,包含信息:grade_money ,ticket_money,coupon_valid_time(均为配置ID,产品管理时设置)
     * @param int $company_id 公司ID
     * @return array
     * @throws SwooleException
     */
    public function balanceConf(array $equipments_config, int $company_id): array
    {
        $config = [];
        // TODO 2024-06-01 起 等级金额 采用极差 总佣金 150
        // TODO 等级金额 2024-06-01 以前的作废
        $config['grade_money'] = get_instance()->config['grade_money'];
        if (!empty($equipments_config['grade_money'])) {
            $grade_money = ConfigService::getBalanceConfig($equipments_config['grade_money'], 'grade_money', $company_id);
            $config['grade_money'] = $grade_money;
        }

        // 结算佣金配置
        $config['settlement_commission'] = get_instance()->config['settlement_commission'];

        //续费券抵用金额
        $config['ticket_money'] = get_instance()->config['ticket_money'];
        if (!empty($equipments_config['ticket_money'])) {
            $grade_money = ConfigService::getBalanceConfig($equipments_config['ticket_money'], 'ticket_money', $company_id);
            $config['ticket_money'] = $grade_money;
        }

        //续费券有效期
        $config['coupon_valid_time'] = get_instance()->config['coupon_valid_time'];
        if (!empty($equipments_config['coupon_valid_time'])) {
            $grade_money = ConfigService::getBalanceConfig($equipments_config['coupon_valid_time'], 'coupon_valid_time', $company_id);
            $config['coupon_valid_time'] = $grade_money;
        }
        //账号
        $account = ConfigService::getBalanceConfig('', 'account', $company_id);
        $config['account'] = $account;
        return [true,$config];
    }

    /**
     * 验证是否进行分钱
     * @param array $contract 合同信息 is_old_client:是否是老系统客户介绍,0:否 1:是
     * @param array $contract_rank 合同关系 push_apply_id:推荐人ID
     * @param int $is_settle 是否结算 0 否 1 是
     * @return array
     */
    public function balanceCheckCommission(array $contract, array $contract_rank, int $is_settle): array
    {
        if(!$is_settle && $contract_rank['is_old_rank']==1 && empty($contract_rank['dismantle_explainer_id'])){
            return [false,'老系统导入的合同不允许重新计算金额'];
        }
        //老系统客户介绍和推荐人（返券）同时存在时
        if ($contract['is_old_client'] == 1 && !empty($contract_rank['push_apply_id']) && $contract_rank['push_apply_money_type']==1) {
            return [false,'老系统客户介绍和推荐人（返券）同时存在'];
        }
        return [true,'验证通过'];
    }

    /**
     * 老系统客户介绍的分佣金,注意:老系统客户账号必须是员工账号
     * 福利:返续费券抵用金额
     * @param array $contract_rank 合同关系
     * @param array $config 配置信息
     * @param int $achievement_id 业绩记录ID
     * @param int $add_time 结算时间
     * @param int $is_settle 是否结算 0 否 1 是
     * @param array $calculateMoneyData 经销商金额
     * @return array
     */
    public function oldSystemCustomerIntroductionCommission(array $contract_rank, array $config, int $achievement_id, int $add_time, int $is_settle, array $calculateMoneyData): array
    {
        //获取老系统客户账号
        $where = [
            'workers_number' => $config['account']['old_client_number']
        ];
        $field = 'workers_id, user_id,temp_money,total_money,cash_available,commissions';
        $this->Achievement->table = 'oa_workers';
        $old_client_number = $this->Achievement->findData($where, $field);
        $workers_id = $old_client_number['workers_id']??0;
        if (empty($workers_id)) {
            //生成员工总账号
            $add_data['user_id'] = 0;
            $add_data['workers_name'] = '老系统客户';
            $add_data['workers_number'] = $config['account']['old_client_number'];
            $add_data['temp_money'] = 0;
            $add_data['total_money'] = 0;
            $add_data['cash_available'] = 0;
            $add_data['commissions'] = 0;
            $add_data['add_time'] = $add_time;
            $workers_id = $this->Achievement->insertData($add_data);
        }
        $unit_money = $calculateMoneyData['ticket_deduction_unit_money'];
        $money = $contract_rank['device_number'] * $unit_money;//佣金金额
        //添加经销商业绩数据
        $dealer = [
            'workers_id' => $workers_id,
            'workers_position' => '13',
            'money' => $money,
            'sort' => '13',
            'add_time' => $add_time,
            'is_available' => 2,
            'achievement_id' => $achievement_id
        ];
        if($is_settle){
            $this->Achievement->table = 'oa_achievement_workers';
            $this->Achievement->insertData($dealer);
        }
        return [true,['achievement_workers' => $dealer,  'workers_id' => $workers_id, 'money' => $money,'unit_money'=>$unit_money]];
    }

    /**
     * @param int $user_id
     * @return int
     */
    public function pushApplyType(int $user_id): int
    {
        //如果只有商用合同视为商用客户
        $this->Achievement->table = 'contract';
        $contract_info = $this->Achievement->selectData(['user_id'=>$user_id,'status'=>['IN',[4,5,9,10]]],'contract_equipments_line');
        $contract_equipments_line = array_column($contract_info,'contract_equipments_line');
        if(empty($contract_equipments_line)){
            return 1;
        }
        if(in_array(1,$contract_equipments_line)){
            return 1;
        }
        return 2;
    }
    /**
     * 获取优惠券分类ID
     * 1.若已有优惠券ID直接取优惠券ID
     * 若用户续费金额大于576并且其推荐人续费金额也大于576则返券60，否则返48
     */
    public function getCouponId($contract_rank){
        $contract_no = $contract_rank['contract_no'];
        if(!empty($contract_rank['push_apply_coupon_id'])){
            return $contract_rank['push_apply_coupon_id'];
        }
        $coupon_money = $contract_rank['push_apply_money'];
        $coupon_cate_id = 0;//优惠券模板ID
        //老系统导入客户若无返券金额,直接返回0
        if($contract_rank['is_old_rank']==1 && $coupon_money==0){
            return $coupon_cate_id;
        }
        //优惠券金额,根据推荐人合同的续费金额计算
        if(empty($coupon_money)){
            $this->Achievement->table = 'contract';
            $contract_info = $this->Achievement->findData(['contract_no'=>$contract_no],'user_id,renew_money,source_user_id,source_contract_id');
            if(empty($contract_info)){
                return $coupon_cate_id;
            }
            //若存在推荐合同优先取推荐合同，否则取推荐人第一份合同
            if(!empty($contract_info['source_contract_id'])){
                $p_contract_info = $this->Achievement->findData(['contract_id'=>$contract_info['source_contract_id']],'user_id,renew_money');
            }else{
                $this->Achievement->table = 'customer';
                $user_info = $this->Achievement->findData(['user_id'=>$contract_info['user_id']],'source');
                if(!empty($user_info) && !empty($user_info['source'])) {
                    $this->Achievement->table = 'contract';
                    $p_contract_info = $this->Achievement->findData(['user_id' => $user_info['source'], 'status' => ['IN', [4, 5, 9]]], 'user_id,renew_money');
                }
            }
            $push_apply_type = 1;
            $coupon_money = 48;
            if(!empty($p_contract_info)){
                $push_apply_type = $this->pushApplyType($p_contract_info['user_id']);
                //如果推荐人为家用客户并且续费金额小于576
                if($push_apply_type==1 && $p_contract_info['renew_money']<576){
                    $coupon_money =  ceil(bcdiv($p_contract_info['renew_money'],12,2));
                }
            }
        }
        if(empty($coupon_money)){
            return $coupon_cate_id;
        }
        //查询优惠券面额是否存在,不存在新增
        $this->Achievement->table = 'coupon_cate';
        $coupon_cate_info = $this->Achievement->findData(['put_type'=>5,'money'=>$coupon_money],'id');
        if(empty($coupon_cate_info)){
            $cate_data['name'] = $coupon_money.'元抵扣券';
            $cate_data['coupon_type'] = 1;
            $cate_data['put_type'] = 5;
            $cate_data['money'] = formatMoney($coupon_money,1);
            $cate_data['deduction_money'] = 5000;
            $cate_data['condition'] = 0;
            $cate_data['time_type'] = 1;
            $cate_data['use_days'] = 732;
            $cate_data['add_time'] = time();
            $cate_data['status'] = 1;
            $coupon_cate_id = $this->Achievement->insertData($cate_data);
        }else{
            $coupon_cate_id = $coupon_cate_info['id'];
        }
        return $coupon_cate_id;

    }

    /**
     * 推荐人分佣金
     *福利:每台设备返续费抵用券一张
     * @param array $contract_rank 合同关系
     * @param array $contract 合同信息
     * @param $add_time
     * @param string $achievement_id
     * @param $is_settle
     * @param $calculateMoneyData
     * @param $from_renew_order_id
     * @return array
     */
    public function referenceCommission($contract_rank, $contract, $add_time, $achievement_id = '',$is_settle,$calculateMoneyData,$from_renew_order_id): array
    {
        if(empty($contract_rank['push_apply_money']) && $contract_rank['is_old_rank']==1){
            return [true,[
                'ticket_money'=>0,
                'money'=>0,
                'push_apply_money_type'=>$contract_rank['push_apply_money_type'],
                'workers_id'=>0,
                'unit_money'=>0,
                'unit_ticket_money'=>0
            ]];
        }
        //推荐人是经销商
        if($contract_rank['push_apply_money_type']==2){
            $where = [
                'user_id' => $contract_rank['push_apply_id'],
                'is_delete'=>0
            ];
            $field = 'workers_id, user_id,temp_money,total_money,cash_available,commissions';
            $this->Achievement->table = 'oa_workers';
            $workers_info = $this->Achievement->findData($where, $field);
            if(empty($workers_info)){
                return [false,'经销商信息不存在'];
            }
            //添加经销商业绩数据
            if(empty($contract_rank['push_apply_money'])){
                return [true,[
                    'ticket_money'=>0,
                    'money'=>0,
                    'push_apply_money_type'=>$contract_rank['push_apply_money_type'],
                    'workers_id'=>0,
                    'unit_money'=>0,
                    'unit_ticket_money'=>0
                ]];
            }
            $unit_money = $contract_rank['push_apply_money'];
            $money = $contract_rank['device_number'] * $unit_money;
            //$money = $contract_rank['manage_money'] - ($contract_rank['device_number'] * $unit_money);
            $dealer = [
                'workers_id' => $workers_info['workers_id'],
                'workers_position' => '2',
                'money' => $money,
                'sort' => '2',
                'add_time' => $add_time,
                'is_available' => 2,
                'achievement_id' => $achievement_id
            ];
            if($is_settle) {
                $this->Achievement->table = 'oa_achievement_workers';
                $this->Achievement->insertData($dealer);
            }
            return [true,[
                'ticket_money'=>$dealer['money'],
                'money'=>$dealer['money'],
                'push_apply_money_type'=>$contract_rank['push_apply_money_type'],
                'workers_id'=>$dealer['workers_id'],
                'unit_money'=>$unit_money,
                'unit_ticket_money'=>$unit_money
            ]];
        }else{
            //查询状态正常的续费抵用券,选查询是否有需要满足发放条件的，若无查询通用的
            $cid = $calculateMoneyData['ticket_cid'];
            $this->Achievement->table = 'coupon_cate';
            $coupon = $this->Achievement->findData(['put_type' => 5, 'status' => 1,'id'=>$cid], '*');
            if (empty($coupon)) {
                return [false,'续费抵用券不存在'];
            }
            if($is_settle) {
                $this->Achievement->table = 'coupon';
                //若已经提前发放
                if(!empty($from_renew_order_id)){
                    $user_coupon = $this->Achievement->findData(['from_order_type'=>2,'from_order_id'=>$from_renew_order_id],'*');
                    if(!empty($user_coupon)){
                        $this->Achievement->table = 'coupon_cate';
                        $coupon = $this->Achievement->findData(['put_type' => 5, 'status' => 1,'id'=>$user_coupon['cid']], '*');
                        $unit_money = formatMoney($coupon['money'],2);
                        return [true,[
                            'unit_ticket_money'=>formatMoney($coupon['deduction_money'],2),
                            'ticket_money'=>formatMoney($coupon['deduction_money'],2)*$contract_rank['device_number'],
                            'money'=>$unit_money*$contract_rank['device_number'],
                            'cid'=>$cid,
                            'push_apply_money_type'=>$contract_rank['push_apply_money_type'],
                            'unit_money'=>$unit_money
                        ]];
                    }
                }
                $valid_time_start = $add_time;
                $valid_time_end = $add_time + $coupon['use_days'] * 86400;
                if ($coupon['time_type'] == 2) {
                    $valid_time_start = $coupon['use_start_time'];
                    $valid_time_end = $coupon['use_end_time'];
                }
                //每台设备发一张券
                $this->Achievement->table = 'coupon';
                for ($i = 1; $i <= $contract_rank['device_number']; $i++) {
                    $code = $this->generate_promotion_code();
                    $coupon_data_tmp = [
                        'uid' => $contract_rank['push_apply_id'],
                        'cid' => $coupon['id'],
                        'code' => $code,
                        'type' => 5,
                        'status' => 1,
                        'send_time' => $add_time,
                        'valid_time_start' => $valid_time_start,
                        'valid_time_end' => $valid_time_end,
                        'qr_code' => $this->getCodeImg($code),
                        'from_contract_id' => $contract['contract_id'],
                        'from_achievement_id' => $achievement_id
                    ];
                    $this->Achievement->insertData($coupon_data_tmp);
                }
            }
            $unit_money = formatMoney($coupon['money'],2);
            return [true,[
                'unit_ticket_money'=>formatMoney($coupon['deduction_money'],2),
                'ticket_money'=>formatMoney($coupon['deduction_money'],2)*$contract_rank['device_number'],
                'money'=>$unit_money*$contract_rank['device_number'],
                'cid'=>$cid,
                'push_apply_money_type'=>$contract_rank['push_apply_money_type'],
                'unit_money'=>$unit_money
            ]];
        }
    }

    /**
     * 讲解分佣金
     * @param $contract_rank 合同关系
     * @param $achievement_id
     * @param $add_time
     * @param $is_settle
     * @param $calculateMoneyData
     * @return array
     */
    public function explainerCommission($contract_rank, $achievement_id, $add_time,$is_settle,$calculateMoneyData): array
    {
        //讲解费金额
        $unit_money = $calculateMoneyData['new_install_explainer_unit_money'];//新装讲解费;
        $money = $contract_rank['device_number'] *$unit_money;
        //新装讲解原金额
        $origin_explain_money = $calculateMoneyData['new_install_explainer_origanl_money'];//原新装讲解费用
        $where = [
            'user_id' => $contract_rank['explainer_id']
        ];
        $field = 'workers_id,inspection_results,temp_money,total_money,commissions,check_out';
        $this->Achievement->table = 'oa_workers';
        $explainer = $this->Achievement->findData($where, $field);
        if ($contract_rank['explainer_type'] == 1) {//经销商讲解
            if(empty($contract_rank['dismantle_explainer_id'])) {
                $explain_d = [
                    'achievement_id' => $achievement_id,
                    'workers_id' => $explainer['workers_id'],
                    'workers_position' => '9',
                    'money' => $money,
                    'sort' => '1',
                    'is_available' => 2,
                    'add_time' => $add_time,
                ];
                //添加经销商业绩数据
                if($is_settle) {
                    $this->Achievement->table = 'oa_achievement_workers';
                    $this->Achievement->insertData($explain_d);
                    $this->addAccountLog($explainer['workers_id'], $money, $achievement_id, '讲解费', 1, 2, 2, 2, $add_time);
                }
            }else{
                $money = 0;
            }
            // 讲解返佣成功
            return [true,[
                'workers_id' => $explainer['workers_id'],
                'money' => $money,
                'origin_explain_money'=>$origin_explain_money,
                'unit_money'=>$unit_money
            ]];
        } else {//工程讲解
            if(empty($contract_rank['dismantle_explainer_id'])){
                $this->Achievement->table = 'engineers';
                $engineers = $this->Achievement->findData(['engineers_id' => $contract_rank['explainer_id']], 'engineers_id,money,temp_money');
                $explain_e = [
                    'engineers_id' => $engineers['engineers_id'],
                    'achievement_id' => $achievement_id,
                    'money' => $money,
                    'add_time' => $add_time,
                    'is_available' => 2
                ];
                if($is_settle) {
                    //写入工程业绩表
                    $this->Achievement->table = 'oa_engineers_performance';
                    $this->Achievement->insertData($explain_e);

                    //更新工程人员账户--2020.3.10新装字段临时金额，结算时将进入临时金额，划拨后到可提现金额
                    $this->Achievement->table = 'engineers';
                    $edit_data['temp_money'] = $engineers['temp_money'] + $money;
                    $this->Achievement->updateData(['engineers_id' => $engineers['engineers_id']], $edit_data);
                    //资金记录
                    $this->addAccountLog($contract_rank['explainer_id'], $money, $achievement_id, '讲解费', 2, 2, 2, 2, $add_time);
                }
            }else{
                $money = 0;
            }
            //讲解返佣成功
            return [true,[
                'workers_id' => $contract_rank['explainer_id'],
                'money' => $money,
                'origin_explain_money'=>$origin_explain_money,
                'unit_money'=>$unit_money
            ]];
        }
        return [false,'讲解返佣失败'];
    }

    /**
     * 经销商分佣金
     * 以下情况经销商无法获得佣金:
     * 1.结算关系中无经销商
     * 2.员工信息中查询不到经销商信息
     * 3.经销商不合格（未达到考核标准）--此续费结算时会出现该情况
     * @param $type 结算类型 1新装 2续费
     * @param $contract_rank 合同关系信息
     * @param $total_account_number 总账号信息
     * @param $achievement_id 业绩ID
     * @param $auto_audit_config
     * @param $add_time
     * @param $is_settle
     * @param $calculateMoneyData
     * @return array
     */
    public function dealerCommission($type,$contract_rank, $total_account_number, $achievement_id, $auto_audit_config, $add_time,$is_settle,$calculateMoneyData): array

    {
        //若是老系统导入并且无经销商,直接不写入
//        if($contract_rank['is_old_rank']==1 && empty($contract_rank['manage_id'])){
//            return [false,'老系统导入无经销商'];
//        }
        $is_get = false;//是否获得经销商结算，false则划到总账户
        $workers_id = $total_account_number['workers_id'];//员工ID
        $workers_position = 3;//职位 3经销商 10总账号
        $device_number = 0;//业绩台数
        if (!empty($contract_rank['manage_id'])) {
            $where = [
                'user_id' => $contract_rank['manage_id']
            ];
            $field = 'inspection_total,workers_id,inspection_results,inspection_standard,check_out,temp_money,total_money,commissions,cash_available, check_out';
            $this->Achievement->table = 'oa_workers';
            $dealer_info = $this->Achievement->findJoinData($where, $field);
            if (!empty($dealer_info)) {
                $is_get = true;
                $workers_id = $dealer_info['workers_id'];
            }
        }
        $unit_money =  $calculateMoneyData['manage_unit_money'];//经销商获得佣金;
        $original_money = $contract_rank['device_number'] * $unit_money;
        $original_dealer_money = $calculateMoneyData['manage_origanl_money'];//经销商原金额
        $edit_contract_rank = [];
        $edit_contract_rank['manage_money'] = $calculateMoneyData['manage_unit_money'];
        $edit_contract_rank['manage_origanl_money'] = $calculateMoneyData['manage_origanl_money'];
        if ($type == 1) {
            /****************新装结算,如果存在讲解费和返券,从经销商账户扣掉******************/
            //添加经销商业绩数据
            $this->Achievement->table = 'oa_achievement_workers';
            // TODO 机器收款为576(含优惠券)以下的经销商提成如果是100元的都改为60元
            if($original_money == 100){
                $original_money = $this->is_dk ? 60 : $original_money;
                $edit_contract_rank['manage_money'] = $original_money;
            }
            $dealer = [
                'achievement_id' => $achievement_id,
                'workers_id' => $workers_id,
                'workers_position' => $workers_position,
                'money' => $original_money,
                'sort' => '2',
                'is_available' => 2,
                'add_time' => $add_time,
            ];
            if($is_settle) {
                $this->Achievement->insertData($dealer);
                $note = $is_get == true ? '经销商结算[新]获得' : '经销商结算[续]获得，但经销商不存在';
                $this->addAccountLog($workers_id, $original_money, $achievement_id, $note, 1, 2, 2, 2, $add_time);
            }
        } else {
            /****************续费结算,前提必须要合格,不合格将钱划到总账户（考核前提下）******************/
            //续费，需要合格才获得,反之划给总账户
//            if ($auto_audit_config['state'] == 1 && $contract_rank['manage_state'] != 1) {
//                $is_get = false;//是否获得经销商结算，false则划到总账户
//                $temp_money = $total_account_number['temp_money'];//临时金额
//                $workers_id = $total_account_number['workers_id'];//员工ID
//            }
            //添加经销商业绩数据
            $this->Achievement->table = 'oa_achievement_workers';
            // TODO 机器收款为576(含优惠券)以下的经销商提成如果是100元的都改为60元
            if($original_money == 100){
                $original_money = $this->is_dk ? 60 : $original_money;
                $edit_contract_rank['manage_money'] = $original_money;
            }
            $dealer = [
                'achievement_id' => $achievement_id,
                'workers_id' => $workers_id,
                'workers_position' => $workers_position,
                'money' => $original_money,
                'sort' => '2',
                'is_available' => 2,
                'add_time' => $add_time,
            ];
            if($is_settle) {
                $this->Achievement->insertData($dealer);
                //添加资金记录---此处待优化
                $note = $is_get == true ? '经销商结算[续费]获得' : '经销商结算[续]获得，但经销商不合格';
                $this->addAccountLog($workers_id, $original_money, $achievement_id, $note, 1, 2, 2, 2, $add_time);
            }
        }
        return [true,[
            'workers_id' => $workers_id,
            'money' => $original_money,
            'device_number' => $device_number,
            'edit_contract_rank' => $edit_contract_rank,
            'original_dealer_money'=>$original_dealer_money,
            'unit_money'=>$unit_money
        ]];
    }

    /**
     * 计算经销商获得金额
     * 若已有金额,直接取
     * 若无金额,经销商金额=100-讲解金额-返券金额
     * @param $contract_rank
     * @param $config
     * @return array
     */
    public function calculateMoney($contract_rank,$config): array
    {
        $ticket_deduction_unit_money = $contract_rank['push_apply_coupon_value'];
        $ticket_unit_money = $contract_rank['push_apply_money'];
        $ticket_cid = $contract_rank['push_apply_coupon_id'];
        $push_apply_money_type = $contract_rank['push_apply_money_type'];
        $new_install_explainer_origanl_money = 0;
        $manage_origanl_money = 0;
        $dismantle_explainer_unit_money = 0;
        //已验证的直接取已有金额
        if ($contract_rank['is_check']==1) {
            $manage_unit_money = $contract_rank['manage_money'];
            $dismantle_explainer_unit_money = $contract_rank['dismantle_explainer_money'];
            $new_install_explainer_unit_money = $contract_rank['explainer_money'];
            $resultData = [
                'ticket_deduction_unit_money'=>$ticket_deduction_unit_money,
                'ticket_unit_money'=>$ticket_unit_money,
                'ticket_cid'=>$ticket_cid,
                'push_apply_money_type'=>$push_apply_money_type,
                'new_install_explainer_origanl_money'=>$new_install_explainer_origanl_money,
                'manage_origanl_money'=>$manage_origanl_money,
                'manage_unit_money'=>$manage_unit_money,
                'dismantle_explainer_unit_money'=>$dismantle_explainer_unit_money,
                'new_install_explainer_unit_money'=>$new_install_explainer_unit_money
            ];
            return [true,$resultData];
        }
        if($contract_rank['is_old_rank']==1){
            $manage_unit_money =  $contract_rank['manage_money'];//经销商获得佣金;
            $new_install_explainer_unit_money =  $contract_rank['explainer_money'];//新装讲解人获得佣金;
        }else{
            $manage_unit_money =  $config['settlement_commission']['dealer_commission'];//经销商佣金;
            $new_install_explainer_unit_money = 0;//新装讲解费;
            //扣除讲解费
            if (!empty($contract_rank['explainer_id'])) {
                $new_install_explainer_unit_money =  $config['grade_money'][11];
                $manage_unit_money = bcsub($manage_unit_money,$new_install_explainer_unit_money,2);
            }
            //扣除返券
            if (!empty($contract_rank['push_apply_id']) || $contract_rank['push_apply_is_old']==1) {
                $cid = $this->getCouponId($contract_rank);
                $this->Achievement->table = 'coupon_cate';
                $coupon = $this->Achievement->findData(['put_type' => 5, 'status' => 1,'id'=>$cid], '*');
                $ticket_deduction_unit_money = formatMoney($coupon['deduction_money'],2);
                $ticket_unit_money = formatMoney($coupon['money'],2);
                $ticket_cid = $cid;
                $push_apply_money_type = $contract_rank['push_apply_is_old'] == 1?2:1;
                $manage_unit_money = bcsub($manage_unit_money,$ticket_deduction_unit_money,2);
            }
        }
        // 经销商所得佣金
        $base_money = bcadd($manage_unit_money,$new_install_explainer_unit_money,2);
        $new_install_explainer_origanl_money = $new_install_explainer_unit_money;
        $manage_origanl_money = $manage_unit_money;
        //存在拆机讲解人
        if(!empty($contract_rank['dismantle_explainer_id'])){
            if($contract_rank['is_old_rank']==1){//老系统客户拆机讲解人逻辑
                /**
                 * 模式三：老系统客户拆机讲解人模式
                 * A.经销商大于0并且新装讲解人金额为0:拆机讲解人80%,经销商20%,新装讲解人0;
                 * B.经销商大于0并且新装讲解人大于0:拆机讲解人得48,经销商得剩下金额，新装讲解人0
                 * C.经销商为0:拆机讲解人48，新装讲解人0
                 */
                if($contract_rank['manage_money'] > 0 && $new_install_explainer_unit_money == 0){
                    $dismantle_explainer_unit_money = bcmul($base_money,0.8,2);
                    $manage_unit_money = bcsub($base_money,$dismantle_explainer_unit_money,0);
                    $new_install_explainer_unit_money = 0;
                }else if($contract_rank['manage_money'] > 0 && $new_install_explainer_unit_money > 0){
                    $dismantle_explainer_unit_money = 48;
                    $manage_unit_money = bcsub($base_money,$dismantle_explainer_unit_money,0);
                    $new_install_explainer_unit_money = 0;
                }else if($contract_rank['manage_money']==0){
                    $dismantle_explainer_unit_money = 48;
                    $manage_unit_money = 0;
                    $new_install_explainer_unit_money = 0;
                }
            }else {//非老系统客户拆机讲解人逻辑
                /**
                 *模式一：经销商20元  经销商20元,剩下拆机讲解人得,新装讲解人0
                 *模式二：拆机讲解人和经销商各一半  经销商50%,拆机讲解人50%,新装讲解人0
                 */
                if ($contract_rank['dismantle_explainer_settle_mode'] == 1) {
                    $manage_unit_money = 20;
                    $dismantle_explainer_unit_money = bcsub($base_money,$manage_unit_money,2);
                    $new_install_explainer_unit_money = 0;
                } else {
                    $manage_unit_money = bcdiv($base_money,2,0);
                    $dismantle_explainer_unit_money = bcsub($base_money,$manage_unit_money,2);
                    $new_install_explainer_unit_money = 0;
                }
            }
        }
        return [true,[
            'ticket_deduction_unit_money'=>$ticket_deduction_unit_money,
            'ticket_unit_money'=>$ticket_unit_money,
            'ticket_cid'=>$ticket_cid,
            'push_apply_money_type'=>$push_apply_money_type,
            'new_install_explainer_origanl_money'=>$new_install_explainer_origanl_money,
            'manage_origanl_money'=>$manage_origanl_money,
            'manage_unit_money'=>$manage_unit_money,// 经销商的佣金
            'dismantle_explainer_unit_money'=>$dismantle_explainer_unit_money,
            'new_install_explainer_unit_money'=>$new_install_explainer_unit_money
        ]];
    }

    // 结算固定分佣
    public function otherCommission(int $achievement_id)
    {
        $this->Achievement->table = 'oa_achievement_workers';
        // 市场助理
        $workers_ids = [
            [
                //'id'=>5316, // 测试环境数据
                'id'=>25244,
                'workers_position'=>18,// 市场助理25244
            ],
            [
                //'id'=>5308,
                'id'=>25246,
                'workers_position'=>11,// 总经理 25246
            ],
            [
                //'id'=>5310,
                'id'=>25247,
                'workers_position'=>12,// 总经理助理  25247
            ],
            [
                //'id'=>5319,// 测试环境数据
                'id'=>25248,
                'workers_position'=>17,// 市场总监
            ]
        ];
        foreach ($workers_ids as $k=>$v){
            $dealer = [
                'achievement_id' => $achievement_id,
                'workers_id' => $v['id'],
                'workers_position' => $v['workers_position'],
                'money' => 5,
                'sort' => '3',
                'is_available' => 2,
                'add_time' => time(),
            ];
            $this->Achievement->insertData($dealer);
        }
    }

    /**
     * 星级经销商分佣金：
     * 以下情况星级经销商无法获得佣金:
     * 1.结算关系中无经销商
     * 2.员工信息中查询不到星级经销商信息
     * 3.星级经销商不合格（未达到考核标准）--此续费结算时会出现该情况
     * 4.员工不合格或者星级经销商不合格（未达到考核标准）--不论新装和续费，5星、10星、20星要求员工和对应星级经销商必须都合格
     * @param int $type 结算类型 1新装 2续费
     * @param array $contract_rank 合同关系信息
     * @param int $level
     * @param array $config
     * @param bool $is_settle
     * @param int $achievement_id 业绩
     * @param bool $is_have_star_explain
     * @return array
     */
    public function starDealerCommission(int $type, array $contract_rank, int $level, array $config, bool $is_settle, int $achievement_id, int $add_time,bool $is_have_star_explain): array
    {

        //$is_get = false;//是否获得代理经销商结算，false则划到总账户
        $workers_id = 0;
        $manage_id = 0 ;
        $agentDealerCommissionMoney = 0; // 代理经销商分佣的钱
        $workers_position = 0;
        switch ($level){
            case 1:
                $manage_id = $contract_rank['star_manage_id'];
                $agentDealerCommissionMoney = $contract_rank['star_manage_money'];
                $workers_position = 4;
                break;
            case 2:
                $manage_id = $contract_rank['two_manage_id'];
                $agentDealerCommissionMoney = $contract_rank['two_manage_money'];
                $workers_position = 8;
                break;
            case 3:
                $manage_id = $contract_rank['five_manage_id'];
                $agentDealerCommissionMoney = $contract_rank['five_manage_money'];
                $workers_position = 5;
                break;
            case 4:
                $manage_id = $contract_rank['ten_manage_id'];
                $agentDealerCommissionMoney = $contract_rank['ten_manage_money'];
                $workers_position = 6;
                break;
            case 5:
                $manage_id = $contract_rank['twenty_manage_id'];
                $agentDealerCommissionMoney = $contract_rank['twenty_manage_money'];
                $workers_position = 7;
                break;
        }
        if($contract_rank['is_old_rank'] == 1 && (!$manage_id || !$agentDealerCommissionMoney) || $agentDealerCommissionMoney == '0.00'){
            return [false,'老系统导入无代理经销商'];
        }
        // TODO 查询代理经销商
        if (!empty($manage_id)) {
            $where = [
                'user_id' => $manage_id
            ];
            $field = 'inspection_total,workers_id, inspection_results, temp_money,total_money, commissions, cash_available, check_out';
            $this->Achievement->table = 'oa_workers';
            $dealer_info = $this->Achievement->findJoinData($where, $field);
            if (!empty($dealer_info)) {
                $workers_id = $dealer_info['workers_id'];
            }
        }
        // TODO 所得佣金直接取合同关系表 contract_rank 里面的对应字段数据
        $unit_money = $agentDealerCommissionMoney;
        $original_money = $unit_money* $contract_rank['device_number'];// 初始金额
        $edit_contract_rank = [];
        switch ($level){
            case 1: $edit_contract_rank['star_manage_money'] = $unit_money;break;
            case 2: $edit_contract_rank['two_manage_money'] = $unit_money;break;
            case 3: $edit_contract_rank['five_manage_money'] = $unit_money;break;
            case 4: $edit_contract_rank['ten_manage_money'] = $unit_money;break;
            case 5: $edit_contract_rank['twenty_manage_money'] = $unit_money;break;
        }
        if ($type == 1) {
            /****************新装结算******************/
            //添加经销商业绩数据
            $this->Achievement->table = 'oa_achievement_workers';
            $dealer = [
                'achievement_id' => $achievement_id,
                'workers_id' => $workers_id,
                'workers_position' => $workers_position,
                'money' => $original_money,
                'sort' => '3',
                'is_available' => 2,
                'add_time' => $add_time,
            ];
            if ($is_settle){
                $this->Achievement->insertData($dealer);
                //添加资金记录---此处待优化
                $note = $level.'级代理经销商结算[新]获取';
                $this->addAccountLog($workers_id, $original_money, $achievement_id, $note, 1, 2, 2, 2, $add_time);
            }
        } else {
            /****************续费结算,必须合格才可以获得(考核前提下)******************/
//            if ($auto_audit_config['state'] == 1 && $contract_rank[$manage_state] != 1) {
//                $is_get = false;//是否获得星级经销商结算，false则划到总账户
//                $workers_id = $total_account_number['workers_id'];
//            }
            //添加经销商业绩数据
            $this->Achievement->table = 'oa_achievement_workers';
            $dealer = [
                'achievement_id' => $achievement_id,
                'workers_id' => $workers_id,
                'workers_position' => $workers_position,
                'money' => $original_money,
                'sort' => '3',
                'is_available' => 2,
                'add_time' => $add_time,
            ];
            if($is_settle) {
                $this->Achievement->insertData($dealer);
                //添加资金记录---此处待优化
                $note = $level.'级代理经销商结算[续]获取';
                $this->addAccountLog($workers_id, $original_money, $achievement_id, $note, 1, 2, 2, 2, $add_time);
            }
        }
        return [true,[
            'workers_id' => $workers_id,
            'money' => $original_money,
            'edit_contract_rank' => $edit_contract_rank
        ]];
    }
    //准经销商
    public function standardPushCommission($contract_rank,$achievement_id,$add_time,$is_settle): array
    {
        $unit_money = $contract_rank['standard_push_money'];
        $money = $contract_rank['device_number'] * $unit_money;
        $where = [
            'user_id' => $contract_rank['standard_push_id']
        ];
        $field = 'workers_id,inspection_results,temp_money,total_money,commissions,check_out';
        $this->Achievement->table = 'oa_workers';
        $workers_info = $this->Achievement->findData($where, $field);
        if(empty($workers_info)) {
            return [false,'星级讲解返佣失败'];
        }
        $explain_d = [
            'achievement_id' => $achievement_id,
            'workers_id' => $workers_info['workers_id'],
            'workers_position' => '16',
            'money' => $money,
            'sort' => '1',
            'is_available' => 2,
            'add_time' => $add_time,
        ];
        //添加经销商业绩数据
        if($is_settle && $money!=0) {
            $this->Achievement->table = 'oa_achievement_workers';
            $this->Achievement->insertData($explain_d);
            $this->addAccountLog($workers_info['workers_id'], $money, $achievement_id, '准经销商', 1, 2, 2, 2, $add_time);
        }
        return [true,[
            'workers_id' => $workers_info['workers_id'],
            'money' => $money,
            'unit_money'=>$unit_money
        ]];
    }

    //拆机讲解人
    public function dismantleExplainerCommission($contract_rank, $achievement_id, $add_time,$is_settle,$calculateMoneyData): array
    {
        $unit_money = $calculateMoneyData['dismantle_explainer_unit_money'];
        $money = $unit_money*$contract_rank['device_number'];
        if ($contract_rank['dismantle_explainer_type'] == 1) {//经销商讲解
            $where = [
                'user_id' => $contract_rank['dismantle_explainer_id']
            ];
            $field = 'workers_id,inspection_results,temp_money,total_money,commissions,check_out';
            $this->Achievement->table = 'oa_workers';
            $explainer = $this->Achievement->findData($where, $field);
            $explain_d = [
                'achievement_id' => $achievement_id,
                'workers_id' => $explainer['workers_id'],
                'workers_position' => 15,
                'money' => $money,
                'sort' => '1',
                'is_available' => 2,
                'add_time' => $add_time,
            ];

            //添加经销商业绩数据
            if($is_settle) {
                $this->Achievement->table = 'oa_achievement_workers';
                $this->Achievement->insertData($explain_d);
                $this->addAccountLog($explainer['workers_id'], $money, $achievement_id, '讲解费', 1, 2, 2, 2, $add_time);
            }
            // 讲解返佣成功
            return [true,[
                'workers_id' => $explainer['workers_id'],
                'money' => $money,
                'unit_money'=>$unit_money
            ]];
        } else {//工程讲解
            $this->Achievement->table = 'engineers';
            $engineers = $this->Achievement->findData(['engineers_id' => $contract_rank['dismantle_explainer_id']], 'engineers_id,money,temp_money');
            $explain_e = [
                'engineers_id' => $engineers['engineers_id'],
                'achievement_id' => $achievement_id,
                'money' => $money,
                'add_time' => $add_time,
                'is_available' => 2,
                'settle_type'=>3
            ];
            if($is_settle) {
                //写入工程业绩表
                $this->Achievement->table = 'oa_engineers_performance';
                $this->Achievement->insertData($explain_e);

                //更新工程人员账户--2020.3.10新装字段临时金额，结算时将进入临时金额，划拨后到可提现金额
                $this->Achievement->table = 'engineers';
                $edit_data['temp_money'] = $engineers['temp_money'] + $money;
                $this->Achievement->updateData(['engineers_id' => $engineers['engineers_id']], $edit_data);
                //资金记录
                $this->addAccountLog($engineers['engineers_id'], $money, $achievement_id, '讲解费', 2, 2, 2, 2, $add_time);
            }
            // 讲解返佣成功
            return [true,[
                'workers_id' =>$contract_rank['explainer_id'],
                'money' => $money,'unit_money'=>$unit_money
            ]];
        }
    }

//    /**
//     * 星级讲解人-经销商的讲解人（讲解经销商成为区域代理的人），此讲解人分星级经销商的5块钱（星级经销商和星级讲解各得5块）（2022-3-26周总提出新增）
//     * @param int $type 类型
//     * @param array $contract_rank
//     * @param int $achievement_id
//     * @param int $add_time
//     * @param int $is_settle
//     */
    public function starExplainerCommission($type, $contract_rank,$achievement_id,$add_time,$is_settle): array
    {
        $unit_money = get_instance()->config['star_explainer_money']; //星级讲解费金额
        if (!empty($contract_rank['star_explainer_money']) && $contract_rank['star_explainer_money']!="0.00") {
            $unit_money = $contract_rank['star_explainer_money'];
        }
        $money = $contract_rank['device_number'] * $unit_money;//星级讲解费
        if ($contract_rank['star_explainer_type'] == 1) {//经销商讲解
            $where = [
                'user_id' => $contract_rank['star_explainer_id']
            ];
            $field = 'workers_id,inspection_results,temp_money,total_money,commissions,check_out';
            $this->Achievement->table = 'oa_workers';
            $explainer = $this->Achievement->findData($where, $field);
            $explain_d = [
                'achievement_id' => $achievement_id,
                'workers_id' => $explainer['workers_id'],
                'workers_position' => '14',
                'money' => $money,
                'sort' => '1',
                'is_available' => 2,
                'add_time' => $add_time,
            ];

            //添加经销商业绩数据
            if($is_settle) {
                $this->Achievement->table = 'oa_achievement_workers';
                $this->Achievement->insertData($explain_d);
                $this->addAccountLog($explainer['workers_id'], $money, $achievement_id, '星级讲解费', 1, 2, 2, 2, $add_time);
            }
            // 星级讲解返佣成功
            return [true,[
                'workers_id' => $explainer['workers_id'],
                'money' => $money,
                'unit_money'=>$unit_money
            ]];
        } else {//工程讲解
            $this->Achievement->table = 'engineers';
            $engineers = $this->Achievement->findData(['engineers_id' => $contract_rank['star_explainer_id']], 'engineers_id,money,temp_money');
            $explain_e = [
                'engineers_id' => $engineers['engineers_id'],
                'achievement_id' => $achievement_id,
                'money' => $money,
                'settle_type'=>2,
                'add_time' => $add_time,
                'is_available' => 2
            ];
            if($is_settle) {
                //写入工程业绩表
                $this->Achievement->table = 'oa_engineers_performance';
                $this->Achievement->insertData($explain_e);

                //更新工程人员账户--2020.3.10新装字段临时金额，结算时将进入临时金额，划拨后到可提现金额
                $this->Achievement->table = 'engineers';
                $edit_data['temp_money'] = $engineers['temp_money'] + $money;
                $this->Achievement->updateData(['engineers_id' => $engineers['engineers_id']], $edit_data);
                //资金记录
                $this->addAccountLog($contract_rank['explainer_id'], $money, $achievement_id, '星级讲解费', 2, 2, 2, 2, $add_time);
            }
            //星级讲解返佣成功
            return [true,[
                'money'=>$money,
                'unit_money'=>$unit_money
            ]];
        }
        return [false,'星级讲解返佣失败'];
    }

    /**
     * 特殊账号分佣金-总经理和总经理助理
     * @param $type
     * @param $contract_rank
     * @param $config
     * @param $grade_money_index
     * @param $account_index
     * @param $position
     * @param $workers_position
     * @param $achievement_id
     * @param $add_time
     * @param $is_settle
     * @return array
     */
    public function specialAccountCommission($type, $contract_rank, $config, $grade_money_index, $account_index, $position, $workers_position, $achievement_id, $add_time,$is_settle): array
    {
        $unit_money = $config['grade_money'][$grade_money_index];
        if (!empty($contract_rank['gma_money']) && $contract_rank['gma_money']!="0.00") {
            $unit_money = $contract_rank['gma_money'];
            if ($grade_money_index == 10) {
                $unit_money = $contract_rank['gm_money'];
            }
        }
        $money = $contract_rank['device_number'] * $unit_money;
        //总经理助理
        $where = [
            'workers_number' => $config['account'][$account_index]
        ];
        $field = '
            workers_id,
            user_id,
            workers_name,
            inspection_results,
            temp_money,
            total_money,
            commissions,
            cash_available,
            check_out
        ';
        $this->Achievement->table = 'oa_workers';
        $dealer_info = $this->Achievement->findJoinData($where, $field);
        if (empty($dealer_info)) {
            //生成员工总账号
            $add_data['user_id'] = 0;
            $add_data['workers_name'] = $position;
            $add_data['workers_number'] = $config['account'][$account_index];
            $add_data['temp_money'] = 0;
            $add_data['total_money'] = 0;
            $add_data['cash_available'] = 0;
            $add_data['commissions'] = 0;
            $add_data['add_time'] = $add_time;
            $add_data['inspection_results'] = 0;
            $add_data['check_out'] = 0;
            $workers_id = $this->Achievement->insertData($add_data);
            $add_data['workers_id'] = $workers_id;
            $dealer_info = $add_data;
            unset($add_data);
            FileLogService::WriteLog(SLog::ERROR_LOG, 'settlement', '业绩ID:' . $achievement_id . ' - ' . $position . '不存在,系统自动创建');

            // return returnResult(ReturnCodeService::FAIL, '结算失败,未检测到' . $position . '账号');
        }

        //更新合同关系
        $edit_contract_rank = [];
        // $this->Achievement->table = 'contract_rank';
        if ($grade_money_index == 10) {
            $edit_contract_rank['gm_id'] = $dealer_info['user_id'];
            $edit_contract_rank['gm_name'] = $dealer_info['workers_name'];
            $edit_contract_rank['gm_money'] = $unit_money;
            $edit_contract_rank['gm_state'] = 1;
        } else {
            $edit_contract_rank['gma_id'] = $dealer_info['user_id'];
            $edit_contract_rank['gma_name'] = $dealer_info['workers_name'];
            $edit_contract_rank['gma_money'] = $unit_money;
            $edit_contract_rank['gma_state'] = 1;
        }
        // $this->Achievement->updateData(['id' => $contract_rank['id']], $edit_contract_rank);
        //添加经销商业绩数据
        $this->Achievement->table = 'oa_achievement_workers';
        $dealer = [
            'achievement_id' => $achievement_id,
            'workers_id' => $dealer_info['workers_id'],
            'workers_position' => $workers_position,
            'money' => $money,
            'sort' => '7',
            'is_available' => 2,
            'add_time' => $add_time,
        ];
        if($is_settle) {
            $this->Achievement->insertData($dealer);
            //添加资金记录---此处待优化
            $this->addAccountLog($dealer_info['user_id'], $money, $achievement_id, $position . "结算[' . (($type == 1) ? '新' : '续') . ']获得'", 1, 2, 2, 2, $add_time);
        }
        return [true,[
            'workers_id' => $dealer_info['workers_id'],
            'money' => $money,
            'edit_contract_rank' => $edit_contract_rank
        ]];
    }

//    /**
//     * 增加资金变动记录
//     * @param $user_id 用户ID（职工ID和工程人员ID）
//     * @param $money 金额,单位元
//     * @param $order_id 订单ID（包含业绩订单和提现订单）
//     * @param int $user_type 记录类型，1：经销商，2：工程人员
//     * @param int $source 来源，1：提现，2：结算，3：其他 4:开会补贴或者扣款
//     * @param int $account_type 账户类型，1：支出，2：收入
//     * @param int $is_available 是否可用 1是 2否，冻结或者临时金额表示不可用
//     * @param string $note 备注
//     */
    public function addAccountLog($user_id, $money, $order_id, $note = '', $user_type = 1, $source = 2, $account_type = 2, $is_available = 1, $add_time)
    {
        $temp_account_log = [
            'user_id' => $user_id,
            'user_type' => $user_type,
            'source' => $source,
            'account_type' => $account_type,
            'order_id' => $order_id,
            'money' => $money,
            'note' => $note,
            'add_time' => $add_time,
            'is_available' => $is_available
        ];
        $this->Achievement->table = 'funds_record';
        $this->Achievement->insertData($temp_account_log);
    }

    /**
     * 获取系统总账号
     * @param $config 配置信息
     * @return array
     */
    public function getTotalAccount($config): array
    {

        //TODO 获取  周建华 的账号
        $where = [
            'workers_number' => 855182
        ];
        $field = '
            workers_id,
            user_id,
            temp_money,
            total_money,
            cash_available,
            commissions
        ';
        $this->Achievement->table = 'oa_workers';
        $total_account_number = $this->Achievement->findData($where, $field);
        if (empty($total_account_number)) {
            //生成员工总账号
            $add_data['user_id'] = 0;
            $add_data['workers_name'] = '系统总账号';
            $add_data['workers_number'] = $config['account']['total_account_number'];
            $add_data['temp_money'] = 0;
            $add_data['total_money'] = 0;
            $add_data['cash_available'] = 0;
            $add_data['commissions'] = 0;
            $add_data['add_time'] = time();
            $workers_id = $this->Achievement->insertData($add_data);
            $add_data['workers_id'] = $workers_id;
            $total_account_number = $add_data;
            unset($add_data);
            FileLogService::WriteLog(SLog::ERROR_LOG, 'settlement', ':系统总账号不存在,系统自动创建');
            // return returnResult(ReturnCodeService::FAIL, '结算失败，员工信息里不存在总账号,请确保总账号必须是员工账号');
        }
        return [true,$total_account_number];
    }

    /**
     * 润泉业绩结算
     * @param $type
     * @param $contract
     * @param $contract_rank
     * @param $config
     */
    public function balanceRunQuanCommission($type, $contract, $contract_rank, $config, $auto_audit_config, $add_time, $from_renew_order_id,$is_settle,$operator): array
    {

        /*************验证是否进行分钱****************/
        [$ok,$resultMsg] = $this->balanceCheckCommission($contract, $contract_rank,$is_settle);
        if (!$ok) {
            return [false,$resultMsg];
        }
        unset($resultMsg);unset($ok);
        $achievement_id = 0;
        //添加结算信息
        if($is_settle){
            // TODO 查询对应的资金记录
            $contractService = new ContractService();
            $financeRecordData = $contractService->getFinanceRecord($contract,$type,$from_renew_order_id);
            if($financeRecordData['money'] < $this->dkMoney){
                dump('查询资金记录:满足代扣条件');
                $this->is_dk = true;
            }
            $this->Achievement->table = 'oa_achievement';
            $achievement = [
                'achievement_work_order_type' => ($type == 1) ? 3 : 4,
                'contract_number' => $contract['contract_no'],
                'contract_customer' => $contract['realname'] ?? '',
                'contract_customer_id_card' => $contract['id_card'] ?? '',
                'equipments_number' => $contract_rank['device_number'],
                'add_time' => $add_time,
                'from_renew_order_id' => $from_renew_order_id,
                'operator'=>$operator,
                'finance_record_id'=>$financeRecordData['financeRecordId']
            ]; //业绩数据
            $achievement_id = $this->Achievement->insertData($achievement);
        }

        /**
         * 计算经销商金额、新装讲解人、推荐人,拆机讲解人
         * $ticket_deduction_unit_money 推荐人抵扣金额
         * $ticket_unit_money 推荐人金额
         * $ticket_cid 推荐人优惠券ID
         * $push_apply_money_type 推荐人返佣类型 1优惠券 2 佣金(经销商)
         * $new_install_explainer_origanl_money 新装讲解人原始金额
         * $manage_origanl_money 经销商原始金额
         * $manage_unit_money 经销商金额
         * $dismantle_explainer_unit_money 拆机讲解人金额
         * $new_install_explainer_unit_money 新装讲解人金额
         * */
        [$ok,$result] = $this->calculateMoney($contract_rank,$config);
        $calculateMoneyData = $result;
        $edit_contract_rank_arr = [];
        unset($ok);unset($result);
        // TODO 老系统客户介绍 或者 推荐人（返券）分佣
        $workers = [];
        if ($contract['is_old_client']==1 || $contract_rank['push_apply_is_old']==1) {//老系统客户介绍的-返续费抵用券金额
            $contract_rank['push_apply_is_old'] = 1;
            [$ok,$result] = $this->oldSystemCustomerIntroductionCommission($contract_rank, $config, $achievement_id, $add_time,$is_settle,$calculateMoneyData);//分佣金
            if(!$ok){
                return [false,'老系统客户介绍 或者 推荐人（返券）分佣金错误'];
            }
            if (!empty($result) && !empty($result['workers_id'])) {
                $item['workers_id'] = $result['workers_id'];
                $item['money'] = $result['money'];
                $workers[] = $item;
            }
            //if(empty($contract_rank['push_apply_money'])){
            $edit_contract_rank_arr['push_apply_money_type'] = 2;
            $edit_contract_rank_arr['push_apply_money'] = $result['unit_money'];
            //}
            unset($result);unset($ok);

        }
        // TODO 推荐人,发放续费券
        else if (!empty($contract_rank['push_apply_id']) && !empty($contract_rank['push_apply_name'])) {
            [$ok,$result] = $this->referenceCommission($contract_rank, $contract, $add_time, $achievement_id,$is_settle,$calculateMoneyData,$from_renew_order_id);//分佣金
            if ($ok) {
                if (!empty($result) && !empty($result['workers_id']) && $result['push_apply_money_type']==2 && !empty($result['money'])) {
                    $item['workers_id'] = $result['workers_id'];
                    $item['money'] = $result['money'];
                    $workers[] = $item;
                }
                //var_dump($contract_rank['contract_no']."推荐人返券CID:".$result['data']['cid'].',金额:'. $result['data']['unit_money']);
                //if(empty($contract_rank['push_apply_coupon_id'])){
                $edit_contract_rank_arr['push_apply_coupon_id'] = $result['cid']??0;
                $edit_contract_rank_arr['push_apply_money'] = $result['unit_money'];
                $edit_contract_rank_arr['push_apply_coupon_value'] = $result['unit_ticket_money'];
                // }
                unset($result);unset($ok);
            }
        }
        // TODO 准经销商 返佣
        if(!empty($contract_rank['standard_push_id']) && !empty($contract_rank['standard_push_money'])){
            [$ok,$result] = $this->standardPushCommission($contract_rank, $achievement_id, $add_time,$is_settle);
            if (!$ok) {
                return [false,$result];
            }
            $item['workers_id'] = $result['workers_id'];
            $item['money'] = $result['money'];
            $workers[] = $item;
            unset($result);unset($ok);
        }

        // TODO 新装讲解人分佣金-讲解人单独给，判断其他人是否有重叠，若有，则找到该人，在该人基础上加，反之，更新;若存在拆机讲解人,新装讲解人不得钱
        if (!empty($contract_rank['explainer_id'])) {
            [$ok,$result] = $this->explainerCommission($contract_rank, $achievement_id, $add_time,$is_settle,$calculateMoneyData);
            if (!$ok) {
                return [false,$result];
            }
            if (!empty($result) && !empty($result['workers_id'])) {
                $item['workers_id'] = $result['workers_id'];
                $item['money'] = $result['money'];
                $item['explainer_origanl_money'] = $result['origin_explain_money'];
                //如果拆机讲解人为空,新装讲解人才能获得钱
                if(empty($contract_rank['dismantle_explainer_id'])){
                    $workers[] = $item;
                    $edit_contract_rank_arr['explainer_money'] = $result['unit_money'];
                }
            }
            unset($result);unset($ok);
        }

        // TODO 星级讲解人分佣  若存在星级讲解人，则星级经销商分5块钱出来
        // TODO 2024-7-31  2024-6-01 后新装 取消星级讲解分佣
        $is_have_star_explain = false;       //是否存在星级讲解人
//        if (!empty($contract_rank['star_explainer_id'])) {
//            $is_have_star_explain = true;
//            [$ok,$result] = $this->starExplainerCommission($type, $contract_rank, $achievement_id, $add_time,$is_settle);
//            if (!$ok) {
//                return [false,$result];
//            }
//            if (!empty($result) && !empty($result['workers_id'])) {
//                $item['workers_id'] = $result['workers_id'];
//                $item['money'] = $result['money'];
//                $workers[] = $item;
//                $edit_contract_rank['star_explainer_money'] = $result['unit_money'];
//            }
//            unset($result);unset($ok);
//        }
        // TODO 获取系统总账号，特殊情况直接将钱划到总账户
        [$ok,$result] = $this->getTotalAccount($config);
        if($ok){
            $total_account_number = $result;
        }
        unset($result);unset($ok);
        // TODO 经销商分佣
        [$ok,$result] = $this->dealerCommission($type,$contract_rank, $total_account_number, $achievement_id, $auto_audit_config, $add_time,$is_settle,$calculateMoneyData);
        if ($ok && !empty($result) && !empty($result['workers_id'])) {
            $dealer_money = $result['money'];
            $original_dealer_money = $result['original_dealer_money'];
            $item['workers_id'] = $result['workers_id'];
            $item['money'] = $dealer_money;
            $edit_contract_rank = $result['edit_contract_rank'] ?? [];
            $edit_contract_rank_arr = array_merge($edit_contract_rank, $edit_contract_rank_arr);
            $workers[] = $item;
            unset($item);
        }
        unset($result);unset($ok);
        // TODO 拆机讲解人分佣
        if (!empty($contract_rank['dismantle_explainer_id'])) {
            [$ok,$result] = $this->dismantleExplainerCommission($contract_rank, $achievement_id, $add_time,$is_settle,$calculateMoneyData);
            if (!$ok) {
                return [false,'讲解返佣失败'];
            }
            if (!empty($result) && !empty($result['workers_id'])) {
                $item['workers_id'] = $result['workers_id'];
                $item['money'] = $result['money'];
                $workers[] = $item;
            }
            $edit_contract_rank_arr['dismantle_explainer_money'] = $result['unit_money'];
            unset($result);unset($ok);
        }

        // TODO 一级代理经销商分佣
        [$ok,$result] = $this->starDealerCommission($type, $contract_rank, 1,  $config,$is_settle,$achievement_id,$add_time,$is_have_star_explain);
        if ($ok && !empty($result) && !empty($result['workers_id'])) {
            $item['workers_id'] = $result['workers_id'];
            $item['money'] = $result['money'];
            $edit_contract_rank = $result['edit_contract_rank'] ?? [];
            $edit_contract_rank_arr = array_merge($edit_contract_rank, $edit_contract_rank_arr);
            $workers[] = $item;
            unset($item);
        }
        unset($result);unset($ok);

        // TODO 二级代理经销商分佣
        [$ok,$result] = $this->starDealerCommission($type, $contract_rank, 2,  $config,$is_settle,$achievement_id,$add_time,$is_have_star_explain);
        if ($ok && !empty($result) && !empty($result['workers_id'])) {
            $item['workers_id'] = $result['workers_id'];
            $item['money'] = $result['money'];
            $edit_contract_rank = $result['edit_contract_rank'] ?? [];
            $edit_contract_rank_arr = array_merge($edit_contract_rank, $edit_contract_rank_arr);
            $workers[] = $item;
            unset($item);
        }
        unset($result);unset($ok);

        // TODO 三级代理经销商分佣
        [$ok,$result] = $this->starDealerCommission($type, $contract_rank, 3,  $config,$is_settle,$achievement_id,$add_time,$is_have_star_explain);
        if ($ok && !empty($result) && !empty($result['workers_id'])) {
            $item['workers_id'] = $result['workers_id'];
            $item['money'] = $result['money'];
            $edit_contract_rank = $result['edit_contract_rank'] ?? [];
            $edit_contract_rank_arr = array_merge($edit_contract_rank, $edit_contract_rank_arr);
            $workers[] = $item;
            unset($item);
        }
        // TODO 四级代理经销商分佣
        [$ok,$result] = $this->starDealerCommission($type, $contract_rank, 4,  $config,$is_settle,$achievement_id,$add_time,$is_have_star_explain);
        if ($ok && !empty($result) && !empty($result['workers_id'])) {
            $item['workers_id'] = $result['workers_id'];
            $item['money'] = $result['money'];
            $edit_contract_rank = $result['edit_contract_rank'] ?? [];
            $edit_contract_rank_arr = array_merge($edit_contract_rank, $edit_contract_rank_arr);
            $workers[] = $item;
            unset($item);
        }
        unset($result);unset($ok);
        // TODO 五级代理经销商分佣 理论上是分给总账户
        [$ok,$result] = $this->starDealerCommission($type, $contract_rank, 5,  $config,$is_settle,$achievement_id,$add_time,$is_have_star_explain);
        if ($ok && !empty($result) && !empty($result['workers_id'])) {
            $item['workers_id'] = $result['workers_id'];
            $item['money'] = $result['money'];
            $edit_contract_rank = $result['edit_contract_rank'] ?? [];
            $edit_contract_rank_arr = array_merge($edit_contract_rank, $edit_contract_rank_arr);
            $workers[] = $item;
            unset($item);
        }
        // TODO 2024-07-01 结算固定分佣
//        $this->otherCommission($achievement_id);
//        $item['workers_id'] = 25244;
//        $item['money'] = 5;
//        $workers[] = $item;
//        // 总经理
//        $item['workers_id'] = 25246;
//        $item['money'] = 5;
//        $workers[] = $item;
//        // 总经理助理
//        $item['workers_id'] = 25247;
//        $item['money'] = 5;
//        $workers[] = $item;
//        // 市场总监
//        $item['workers_id'] = 25248;
//        $item['money'] = 5;
//        $workers[] = $item;
//        unset($item);
        unset($result);unset($ok);
        if($is_settle) {
            //更新账户
            //计算各个账户总和
            $workers_id_arr = array_unique(array_column($workers, 'workers_id'));
            $final_workers = [];
            foreach ($workers_id_arr as $kk => $vv) {
                $money = 0;
                $device_number = 0;
                foreach ($workers as $k => $v) {
                    if ($v['workers_id'] == $vv) {
                        $money += $v['money'];
                        $device_number += $v['device_number'] ?? 0;
                    }
                }
                $item['workers_id'] = $vv;
                $item['money'] = $money;
                $item['device_number'] = $device_number;
                AsyncFile::write('kaohe', date('Y-m-d H:i:s', time()) . '-' . '合同编号:' . $contract['contract_no'] . ' - 员工ID' . $vv . '===>>>' . $device_number);
                $final_workers[] = $item;
            }
            $this->Achievement->table = 'oa_workers';
            foreach ($final_workers as $k => $v) {
                $dealer_info = $this->Achievement->findJoinData(['workers_id' => $v['workers_id']], 'temp_money,inspection_results,inspection_total,inspection_end_time,inspection_start_time');
                $edit_dealer_info['temp_money'] = $dealer_info['temp_money'] + $v['money'];
                $edit_dealer_info['inspection_total'] = $dealer_info['inspection_total'] + $v['device_number'];
                //装机时间在考核周期内才会增加完成业绩
                if ($contract['now_date'] >= $dealer_info['inspection_start_time'] && $contract['now_date'] <= $dealer_info['inspection_end_time']) {
                    $edit_dealer_info['inspection_results'] = $dealer_info['inspection_results'] + $v['device_number'];
                }
                AsyncFile::write('kaohe', date('Y-m-d H:i:s', time()) . '-' . '合同编号:' . $contract['contract_no'] . ' - 员工ID:' . $v['workers_id'] . '===>>>' . json_encode($edit_dealer_info) . '>>>考核周期:' . json_encode(['inspection_start_time' => date('Y-m-d H:i:s', $dealer_info['inspection_start_time']), 'inspection_end_time' => date('Y-m-d H:i:s', $dealer_info['inspection_end_time'])]) . '>>>签约时间:' . date('Y-m-d H:i:s', $contract['now_date']) . '>>装机台数:' . $v['device_number']);
                $this->Achievement->updateData(['workers_id' => $v['workers_id']], $edit_dealer_info);

            }
        }
        //更新合同关系
        $this->Achievement->table = 'contract_rank';
        if (!empty($edit_contract_rank_arr)) {
            $this->Achievement->updateData(['id' => $contract_rank['id']], $edit_contract_rank_arr);
        }
        /*************渠道合作伙伴分佣金****************/
        $this->channelSettlement($contract_rank, $achievement_id, $add_time);
        return [true,'结算成功'];
    }



    /**
     * 结算补单--解决结算失败的
     */
    public function http_balanceRepair()
    {
        $contract_id = $this->parm['contract_id'] ?? '';
        $type = $this->parm['type'] ?? 1;
        $is_admin = $this->parm['is_admin'] ?? 0;
        if (empty($contract_id)) {
            return $this->jsonend(ReturnCodeService::FAIL, '请选择合同');
        }
        $this->Achievement->table = 'contract';
        $contract_info = $this->Achievement->findData(['contract_id' => $contract_id, 'status' => ['IN', [4, 5, 6]]], 'contract_id,exire_date,contract_no');
        if (empty($contract_info)) {
            return $this->jsonend(ReturnCodeService::FAIL, '合同不存在或者状态非法');
        }
        $contract_no = $contract_info['contract_no'];
        $renew_order_id = 0;
        if ($type == 1) {
            $this->Achievement->table = 'oa_achievement';
            $achievement = $this->Achievement->findData(['contract_number' => $contract_no, 'achievement_work_order_type' => 3], 'achievement_id');
            if (!empty($achievement)) {
                return $this->jsonend(ReturnCodeService::FAIL, '合同新装已经结算，不能重复结算，合同编号：' . $contract_no);
            }
            //获取结算时间
            $this->Achievement->table = 'work_order';
            $join = [
                ['work_order_business wb', 'wb.work_order_id=rq_work_order.work_order_id', 'left'],
                ['work_order_return_visit wv', 'wv.work_order_id=rq_work_order.work_order_id', 'left'],
            ];
            $work_order_info = $this->Achievement->findJoinData(['contract_id' => $contract_info['contract_id'], 'wb.business_id' => 3, 'visit_status' => 2], 'wv.visit_time,order_number', $join);
            if (empty($work_order_info) || empty($work_order_info['visit_time'])) {
                return $this->jsonend(ReturnCodeService::FAIL, '工单信息错误，工单编号：' . $work_order_info['order_number']);
            }
            //$add_time = $work_order_info['visit_time'];
            $add_time = time();
        } else {
            //如果是续费
            $this->Achievement->table = 'renew_order';
            $renew_order_info = $this->Achievement->selectData(['contract_id' => $contract_info['contract_id'], 'pay_status' => 2], 'renew_order_id,pay_time', [], ['pay_time' => 'DESC']);
            if (empty($renew_order_info)) {
                return $this->jsonend(ReturnCodeService::FAIL, '续费订单信息错误，合同编号：' . $contract_no);
            }
            $this->Achievement->table = 'oa_achievement';
            $achievement = $this->Achievement->selectData(['contract_number' => $contract_no, 'achievement_work_order_type' => 4], 'achievement_id');
            if (count($achievement) >= count($renew_order_info)) {
                return $this->jsonend(ReturnCodeService::FAIL, '未存在未结算的续费订单，合同编号：' . $contract_no);
            }
            if ($contract_info['exire_date'] < strtotime(date('2020-12-30 23:59:59'))) {
                return $this->jsonend(ReturnCodeService::FAIL, '合同到期时间小于2020-12-30 23:59:59,请确认是否续费，合同编号：' . $contract_no);
            }
            // $add_time = $renew_order_info[0]['pay_time'];//最后一次续费订单支付时间
            $add_time = time();//最后一次续费订单支付时间
            $renew_order_id = $renew_order_info[0]['renew_order_id'];
        }

        return $this->balanceFun($contract_no, $type, $add_time, $renew_order_id,$is_admin);
    }


    // TODO 结算方法

    public function balance(array $data): array
    {
        $contract_no = $data['contract_no'] ?? '';
        $type = $data['type'] ?? 1; // 结算类型 1 新装 2 续费 默认新装
        $from_renew_order_id = $data['from_renew_order_id'] ?? 0; // 续费订单id
        $add_time = $data['time'] ?? time();//记录时间
        $device_number = $data['device_number']??'';//折算台数,仅商用机会传此参数
        $is_admin = $data['is_admin'] ?? 0;// 结算通道 0 定时任务自动结算  1 后台手动结算
        $is_settle = $data['is_settle'] ?? true;//是否进行结算 false:仅更新金额 true:进行结算
        $operator = $data['operator'] ?? '';//操作者
        //修改合同关系表设备台数
        if (!empty($device_number)) {
            //修改合同表折算台数
            $this->Achievement->table = 'contract_rank';
            $this->Achievement->updateData(['contract_no' => $contract_no], ['device_number'=>$device_number]);
        }
        if(!empty($from_renew_order_id)){
            $this->Achievement->table = 'renew_order';
            $renew_order_info = $this->Achievement->findData(['renew_order_id' => $from_renew_order_id], '*');
            // 自动结算验证
            if(!empty($renew_order_info) && !$is_admin){
                // TODO  订单真实金额 = 订单实际付款总金额(order_actual_money) + 优惠券抵扣的总金额(coupon_total_money) + 账户余额抵扣(deduction_account_balance) + 代付抵扣金额(paying_agent_deduction_account_balance)
                $order_final_money = $renew_order_info['order_actual_money']+$renew_order_info['coupon_total_money']+$renew_order_info['deduction_account_balance']+$renew_order_info['paying_agent_deduction_account_balance'];
                if($renew_order_info['is_buy_out'] == 1){
                    // 续费买断情况 低于980 不结算
                    if($order_final_money < $this->renewBuyMinMoney){
                        return [false,'续费买断合同实际支付金额小于'.$this->renewBuyMinMoney.'不结算1，合同编号：' . $contract_no];
                    }
                }else{
                    // 单纯续费 低于428 不结算
                    if($order_final_money < $this->renewMinMoney){
                        return [false,'合同实际支付金额小于'.$this->renewMinMoney.'不结算1，合同编号：' . $contract_no];
                    }
                    // TODO 机器收款为576(含优惠券)以下的经销商提成如果是100元的都改为60元
                    if($order_final_money < $this->dkMoney){
                        $this->is_dk = true;
                    }
                }
            }
        }
        return $this->balanceFun($contract_no, $type, $add_time, $from_renew_order_id,$is_admin,$is_settle,$operator);
    }

//    /**
//     * @结算函数
//     * @param string $contract_no 合同编号
//     * @param int $type 结算类型 1 新装 2 续费
//     * @param integer $add_time 结算时间
//     * @param int $from_renew_order_id 续费订单id
//     * @param int $is_admin 结算通道 0 定时任务结算 1 后台手动结算
//     * @param bool $is_settle 结算状态
//    */
    public function balanceFun($contract_no, $type, $add_time, $from_renew_order_id,$is_admin,$is_settle=true,$operator=''): array
    {

        /*************参数校验****************/
        if (!$contract_no) {
            return [false,'缺少必要参数:contract_no'];
        }
        if (!in_array($type, [1, 2])) {
            return [false,'未知结算类型'];
        }
        FileLogService::WriteLog(SLog::ERROR_LOG, 'settlement', "开始结算" . $contract_no);
        /*************结算验证****************/
        [$ok,$result] = $this->balanceCheck($from_renew_order_id,$type,$contract_no,$is_admin,$is_settle);
        if (!$ok) {
            return [false,$result];
        }

        $contract_rank = $result['contract_rank'];//合同结算关系
        $contract = $result['contract'];//合同信息
        $company_id = $result['contract']['company_id'];//公司ID
        unset($result);unset($ok);

        // TODO 获取结算配置
        [$ok,$result] = $this->balanceConf($contract, $company_id);
        if (!$ok) {
            return [false,$result];
        }
        $config = $result;
        unset($result);unset($ok);
        // TODO 判断是否开启经销商考核
        $this->Achievement->table = 'oa_balance_config';
        $auto_audit_config = $this->Achievement->findData(['`key`' => 'auto_audit', 'company_id' => $company_id], 'state');
        if (empty($auto_audit_config)) {
            FileLogService::WriteLog(SLog::ERROR_LOG, 'settlement', $contract_no . '-结算失败,是否考核配置不存在');
            return [false,'考核配置不存在'];
        }
        // TODO 开始结算分钱
        [$ok,$result] = $this->balanceRunquanCommission($type, $contract, $contract_rank, $config, $auto_audit_config, $add_time, $from_renew_order_id,$is_settle,$operator);//润泉结算
        if(!$ok){
            return [false,$result];
        }
        return [true,$result];
    }


    /**给渠道合作伙伴和其团队结算
     * @param $contract_rank
     * @param $acheviment_id
     * @param $add_time
     * @return false|void
     */
    public function channelSettlement($contract_rank, $acheviment_id, $add_time)
    {

        if (empty($contract_rank)) {
            return false;
        }
        if (!empty($contract_rank['channel_id'])) {
            $this->Achievement->table = 'channel';
            $channel_info = $this->Achievement->findData(['id' => $contract_rank['channel_id']], 'user_id');
            if (!empty($channel_info)) {
                $op_data['achievement_id'] = $acheviment_id;
                $op_data['role'] = 4;
                $op_data['source'] = 1;
                $op_data['workers_id'] = $contract_rank['channel_id'];
                $op_data['money'] = $contract_rank['channel_money'];
                $op_data['is_available'] = 2;
                $op_data['add_time'] = $add_time;
                $op_data['remarks'] = '结算获得';
                $this->Achievement->table = 'oa_achievement_workers';
                $this->Achievement->insertData($op_data);
                $this->userService->changeAccount($channel_info['user_id'], $contract_rank['channel_money'], 1, '', '渠道结算', 1, 2);
                unset($op_data);
            }

        }
        if (!empty($contract_rank['channel_promoter_leader_id'])) {
            $this->Achievement->table = 'channel_promoters';
            $channel_promoter_leader_info = $this->Achievement->findData(['channel_promoter_id' => $contract_rank['channel_promoter_leader_id']], 'user_id');
            if (!empty($channel_promoter_leader_info)) {
                $op_data['achievement_id'] = $acheviment_id;
                $op_data['role'] = 4;
                $op_data['source'] = 2;
                $op_data['workers_id'] = $contract_rank['channel_id'];
                $op_data['money'] = -$contract_rank['channel_promoter_leader_money'];
                $op_data['is_available'] = 2;
                $op_data['add_time'] = $add_time;
                $op_data['remarks'] = '返佣给团长';
                $this->Achievement->table = 'oa_achievement_workers';
                $this->Achievement->insertData($op_data);
                $this->userService->changeAccount($channel_info['user_id'], $contract_rank['channel_promoter_leader_money'], 5, '', '返佣给团长', 2, 2);
                unset($op_data);
                $op_data['achievement_id'] = $acheviment_id;
                $op_data['role'] = 5;
                $op_data['source'] = 1;
                $op_data['workers_id'] = $contract_rank['channel_promoter_leader_id'];
                $op_data['money'] = $contract_rank['channel_promoter_leader_money'];
                $op_data['is_available'] = 2;
                $op_data['add_time'] = $add_time;
                $op_data['remarks'] = '结算获得';
                $this->Achievement->table = 'oa_achievement_workers';
                $this->Achievement->insertData($op_data);
                $this->userService->changeAccount($channel_promoter_leader_info['user_id'], $contract_rank['channel_promoter_leader_money'], 1, '', '推广团长结算', 1, 2);
            }
        }
        if (!empty($contract_rank['channel_promoter_id'])) {
            $this->Achievement->table = 'channel_promoters';
            $channel_promoter_info = $this->Achievement->findData(['channel_promoter_id' => $contract_rank['channel_promoter_id']], 'user_id');
            if (!empty($channel_promoter_info)) {
                $op_data['achievement_id'] = $acheviment_id;
                $op_data['role'] = 5;
                $op_data['source'] = 2;
                $op_data['workers_id'] = $contract_rank['channel_promoter_leader_id'];
                $op_data['money'] = -$contract_rank['channel_promoter_money'];
                $op_data['is_available'] = 2;
                $op_data['add_time'] = $add_time;
                $op_data['remarks'] = '返佣给团员';
                $this->Achievement->table = 'oa_achievement_workers';
                $this->Achievement->insertData($op_data);
                $this->userService->changeAccount($channel_promoter_leader_info['user_id'], $contract_rank['channel_promoter_money'], 5, '', '返佣给团员', 5, 2);
                unset($op_data);
                $op_data['achievement_id'] = $acheviment_id;
                $op_data['role'] = 5;
                $op_data['source'] = 1;
                $op_data['workers_id'] = $contract_rank['channel_promoter_id'];
                $op_data['money'] = $contract_rank['channel_promoter_money'];
                $op_data['is_available'] = 2;
                $op_data['add_time'] = $add_time;
                $op_data['remarks'] = '结算获得';
                $this->Achievement->table = 'oa_achievement_workers';
                $this->Achievement->insertData($op_data);
                $this->userService->changeAccount($channel_promoter_info['user_id'], $contract_rank['channel_promoter_money'], 1, '', '推广结算', 1, 2);
            }

        }


    }

    /**
     * 合伙人结算
     * @param $contract_id
     * @param $acheviment_id
     * @param int $device_number
     * @return bool|void
     */
    public function partnerSettlement($contract_rank, $acheviment_id, $add_time = '')
    {
        //新增运营中心业绩信息
        if (!empty($contract_rank['operation_id'])) {
            $op_data['achievement_id'] = $acheviment_id;
            $op_data['role'] = 2;
            $op_data['source'] = 1;
            $op_data['workers_id'] = $contract_rank['operation_id'];
            $op_data['money'] = $contract_rank['operation_money'];
            $op_data['is_available'] = 2;
            $op_data['add_time'] = $add_time;
            $op_data['remarks'] = '结算获得';
            $this->Achievement->table = 'oa_achievement_workers';
            $this->Achievement->insertData($op_data);
            $this->partnerChangeAccount($contract_rank['operation_id'], 2, $contract_rank['operation_money'], 1, 0, '结算获得', 1, 2, $add_time);
            unset($op_data);
        }
        if (!empty($contract_rank['administrative_id'])) {
            $op_data['achievement_id'] = $acheviment_id;
            $op_data['role'] = 2;
            $op_data['source'] = 2;
            $op_data['workers_id'] = $contract_rank['operation_id'];
            $op_data['money'] = -$contract_rank['administrative_money'];
            $op_data['is_available'] = 2;
            $op_data['add_time'] = $add_time;
            $op_data['remarks'] = '返佣给城市合伙人';
            $this->Achievement->table = 'oa_achievement_workers';
            $this->Achievement->insertData($op_data);
            $this->partnerChangeAccount($contract_rank['operation_id'], 2, -$contract_rank['administrative_money'], 1, 0, '返佣给城市合伙人', 1, 2, $add_time);
            unset($op_data);
            $op_data['achievement_id'] = $acheviment_id;
            $op_data['role'] = 3;
            $op_data['source'] = 1;
            $op_data['workers_id'] = $contract_rank['administrative_id'];
            $op_data['money'] = $contract_rank['administrative_money'];
            $op_data['is_available'] = 2;
            $op_data['add_time'] = $add_time;
            $op_data['remarks'] = '结算获得';
            $this->Achievement->table = 'oa_achievement_workers';
            $this->Achievement->insertData($op_data);
            $this->partnerChangeAccount($contract_rank['administrative_id'], 3, $contract_rank['administrative_money'], 1, 0, '结算获得', 1, 2, $add_time);
        }
        return true;

    }

    public function partnerChangeAccount($user_id, $role = 1, $money, $source = 1, $order_id = 0, $remarks = '', $type = 1, $account_type = 1, $add_time = '')
    {
        if (empty($user_id)) {
            return false;
        }
        $adminAccountModel = $this->loader->model('AdminAccountModel', $this);
        $adminAccountBillModel = $this->loader->model('AdminAccountBillModel', $this);
        $money = formatMoney($money, 1);//将元转为分
        //判断是否创建了账户
        $account_info = $adminAccountModel->getOne(['aid' => $user_id, 'role' => $role], 'id,total_money,available_money,frozen_money');
        if (!empty($account_info)) {
            $id = $account_info['id'];
            $total_money = $account_info['total_money'];
            $available_money = $account_info['available_money'];
            $frozen_money = $account_info['frozen_money'];
        } else {
            //创建账户
            $account_data['aid'] = $user_id;
            $account_data['role'] = $role;
            $account_data['total_money'] = 0;
            $account_data['available_money'] = 0;
            $account_data['frozen_money'] = 0;
            $account_data['create_time'] = $add_time;
            $id = $adminAccountModel->add($account_data);

            $total_money = 0;
            $available_money = 0;
            $frozen_money = 0;
        }

        //更新账户余额
        if ($account_type == 1) {
            //可提现账户
            switch ($type) {
                case 1:
                    $edit_account_data['total_money'] = $total_money + $money;
                    $edit_account_data['available_money'] = $available_money + $money;
                    break;

                case 2:
                    $edit_account_data['available_money'] = $available_money - $money;
                    break;
            }
        } else if ($account_type == 2) {
            //冻结账户
            switch ($type) {
                case 1:
                    $edit_account_data['frozen_money'] = $frozen_money + $money;
                    break;

                case 2:
                    $edit_account_data['frozen_money'] = $frozen_money - $money;
                    break;
            }
        }
        $edit_account_result = $adminAccountModel->save(['id' => $id], $edit_account_data);
        //写入账户记录
        $bill_data['aid'] = $user_id;
        $bill_data['role'] = $role;
        $bill_data['money'] = $money;
        $bill_data['change_type'] = $type;
        $bill_data['account_type'] = $account_type;
        $bill_data['source'] = $source;
        $bill_data['work_order_id'] = $order_id;
        $bill_data['remarks'] = $remarks;
        $bill_data['create_time'] = $add_time;
        $add_bill_result = $adminAccountBillModel->add($bill_data);
        return $edit_account_result && $add_bill_result;
    }


    // 生成二维码图片
    public function getCodeImg($code = 'AbCd')
    {
        // Create a basic QR code  创建一个基本的QR码
        //$qrCode = new QrCode($code);
        $qrCode = get_instance()->QrCode;;
        $qrCode->setText($code);
        $qrCode->setSize(300);

        // Set advanced options  设置高级选项
        $qrCode->setWriterByName('png');
        $qrCode->setMargin([
            't' => 10,
            'r' => 10,
            'b' => 10,
            'l' => 10,
        ]);
        $qrCode->setEncoding('UTF-8');
        $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH);
        $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
        $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
        $qrCode->setLogoSize(150, 200);
        $qrCode->setRoundBlockSize(true);
        $qrCode->setValidateResult(false);
        $qrCode->setWriterOptions(['exclude_xml_declaration' => true]);

        // 生成二维码图片
        $date = date('Ymd') . '/';
        $path = WWW_DIR . '/face/coupon/' . $date;
        file_exists(WWW_DIR . '/face') || mkdir(WWW_DIR . '/face', 0777);
        file_exists(WWW_DIR . '/face/coupon') || mkdir(WWW_DIR . '/face/coupon', 0777);
        file_exists($path) || mkdir($path, 0777);
        $file = 'FA' . $code . 'FDE' . uniqid() . mt_rand(100000, 999999) . '.png';
        $filename = $path . $file;

        // Save it to a file  将其保存到文件中
        $qrCode->writeFile($filename);

        return $date . $file;
    }

    /**
     * 生成券码  优惠券码以C开头 提货码以D开头 邀请好友以I开头
     * @param $code_length 券码长度
     * @param $prefix 前缀
     */
    public function generate_promotion_code($code_length = 6, $prefix = 'C')
    {
        $characters = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
        $code = "";
        for ($i = 0; $i < $code_length - 1; $i++) {
            $code .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        $code = $prefix . $code;
        if ($prefix == 'C') {
            $this->Achievement->table = 'coupon';
            $coupon_num = $this->Achievement->findData(['code' => $code], 'COUNT(*) AS number');
            $coupon_num = $coupon_num['number'] ?? 0;
            if ($coupon_num > 0) {
                $this->generate_promotion_code($code_length, $prefix);
            }
        }
        //$code =microtime().$code;
        return $code;
    }

    /**
     * 结算关系
     * 参数名          类型  是否必须    说明
     * page             int     否       当前页码，默认1
     * row              int     否       每页显示数目，默认10
     * keyword          string  否       关键字
     * contract_no      string  否       合同编号
     * is_settlement    int     否       类型，1：已结算，2：未结算
     * @author ligang
     * @RequestType POST
     * @date 2019/6/4 9:26
     */
    public function http_settlementRelations()
    {
        $page = $this->parm['page'] ?? 1;
        $row = $this->parm['row'] ?? 10;
        $keyword = $this->parm['keyword'] ?? '';
        $is_settlement = $this->parm['is_settlement'] ?? '';
        $contract_no = $this->parm['contract_no'] ?? '';
        $where = [];
        if (!empty($keyword)) {
            $where['
            rq_customer.realname|
            push_apply_name|
            manage_name|
            star_manage_name|
            two_manage_name|
            five_manage_name|
            ten_manage_name|
            twenty_manage_name|
            explainer_name|star_explainer_name
            '] = ['LIKE', '%' . trim($keyword) . '%'];
        }
        if (!empty($contract_no)) {
            $where['contract_no'] = ['LIKE', '%' . trim($contract_no) . '%'];
        }
        if (!empty($is_settlement) && $is_settlement == 1) {
            $where['achievement_work_order_type'] = ['>=', 3];
        }
        if (!empty($is_settlement) && $is_settlement == 0) {
            $where['achievement_work_order_type'] = ['<', 3];
        }
        $where['rq_customer.user_belong_to_company'] = $this->company;
        $join = [
            ['oa_achievement', 'rq_contract_rank.contract_no = rq_oa_achievement.contract_number', 'LEFT'],
            ['customer', 'rq_contract_rank.contract_user_id = rq_customer.user_id', 'LEFT']
        ];
        $field = '
            rq_contract_rank.id,
            rq_customer.realname,
            rq_customer.telphone,
            rq_contract_rank.contract_no,
            rq_contract_rank.contract_user_identity,
            rq_contract_rank.push_apply_name,
            rq_contract_rank.manage_name,
            rq_contract_rank.manage_state,
            rq_contract_rank.manage_money,
            rq_contract_rank.star_manage_name,
            rq_contract_rank.star_manage_state,
            rq_contract_rank.star_manage_money,
            rq_contract_rank.two_manage_name,
            rq_contract_rank.two_manage_state,
            rq_contract_rank.two_manage_money,
            rq_contract_rank.five_manage_name,
            rq_contract_rank.five_manage_state,
            rq_contract_rank.five_manage_money,
            rq_contract_rank.ten_manage_name,
            rq_contract_rank.ten_manage_money,
            rq_contract_rank.ten_manage_state,
            rq_contract_rank.twenty_manage_name,
            rq_contract_rank.twenty_manage_state,
            rq_contract_rank.twenty_manage_money,
            rq_contract_rank.star_explainer_name,
            rq_contract_rank.gma_money,
            rq_contract_rank.gma_name,
            rq_contract_rank.gm_name,
            rq_contract_rank.gm_money,
            rq_contract_rank.explainer_name,
            rq_contract_rank.device_number,
            rq_customer.is_old_client,
            rq_customer.old_client_name
        ';
        $order = [
            'rq_contract_rank.add_time' => 'DESC'
        ];

        $this->Achievement->table = 'contract_rank';
        $result = $this->Achievement->selectPageData($where, $field, $join, $row, $page, $order);
        if (!empty($result)) {
            foreach ($result as $key => &$value) {
                $key_arr = array_keys($value);
                foreach ($key_arr as $k => $v) {
                    if (empty($value[$v]) && $v != 'is_old_client') {
                        $value[$v] = '';
                    }
                }
                if ($value['is_old_client'] == 0 && !empty($value['old_client_name'])) {
                    $value['old_client_name'] = '';
                }
            }
        }


        $count = $this->Achievement->findJoinData($where, 'COUNT(*) AS number', $join);
        $count = $count['number'] ?? '';

        $data = [
            'data' => $result,
            'page' => [
                'current_page' => $page,
                'row' => $row,
                'total' => $count,
                'total_page' => ceil($count / $row)
            ]
        ];
        return $this->jsonend(1000, '获取成功', $data);
    }

    /**
     * 导出结算明细
     * 参数名          类型      是否必须    说明
     * keyword          string      否       关键字
     * is_settlement    int         否       类型，1：已结算，2：未结算
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @date 2019/6/18 16:13
     * @author ligang
     * @RequestType POST
     */
    public function http_exportSettlementRelations()
    {
        $path = WWW_DIR . '/work_order/';
        //删除以前的文件
        AsyncFile::delFile($path, 'xlsx');

        $keyword = $this->parm['keyword'] ?? '';
        $is_settlement = $this->parm['is_settlement'] ?? '';
        $where = [];
        if (!empty($keyword)) {
            $where['
            rq_customer.realname|
            contract_no|
            push_apply_name|
            manage_name|
            star_manage_name|
            two_manage_name|
            five_manage_name|
            ten_manage_name|
            twenty_manage_name|
            explainer_name|star_explainer_name
            '] = ['LIKE', '%' . trim($keyword) . '%'];
        }
        if (!empty($is_settlement) && $is_settlement == 1) {
            $where['achievement_work_order_type'] = ['>=', 3];
        }
        if (!empty($is_settlement) && $is_settlement == 0) {
            $where['achievement_work_order_type'] = ['<', 3];
        }
        $join = [
            ['oa_achievement', 'rq_contract_rank.contract_no = rq_oa_achievement.contract_number', 'LEFT'],
            ['customer', 'rq_contract_rank.contract_user_id = rq_customer.user_id', 'LEFT'],
        ];
        $field = '
            rq_customer.realname,
            rq_customer.telphone,
            rq_contract_rank.contract_user_identity,
            rq_contract_rank.contract_no,
            rq_contract_rank.push_apply_name,
            rq_contract_rank.manage_name,
            rq_contract_rank.manage_money,
            rq_contract_rank.star_manage_name,
            rq_contract_rank.star_manage_money,
            rq_contract_rank.two_manage_name,
            rq_contract_rank.two_manage_money,
            rq_contract_rank.five_manage_name,
            rq_contract_rank.five_manage_money,
            rq_contract_rank.five_manage_state,
            rq_contract_rank.ten_manage_name,
            rq_contract_rank.ten_manage_money,
            rq_contract_rank.ten_manage_state,
            rq_contract_rank.twenty_manage_name,
            rq_contract_rank.twenty_manage_money,
            rq_contract_rank.twenty_manage_state,
            rq_contract_rank.star_explainer_name,
            rq_contract_rank.explainer_type,
            rq_contract_rank.explainer_name,
            rq_contract_rank.explainer_money,
            rq_contract_rank.device_number,
            rq_oa_achievement.achievement_work_order_type,
            rq_oa_achievement.add_time,
            rq_customer.old_client_name
        ';
        $order = [
            'rq_contract_rank.add_time' => 'DESC'
        ];
        $this->Achievement->table = 'contract_rank';
        $result = $this->Achievement->selectData($where, $field, $join, $order);
        $data = [];
        foreach ($result as $key => $value) {
            $prefixOrderType = ' ';
            if (!empty($value['achievement_work_order_type']) && $value['achievement_work_order_type'] == 3) {
                $prefixOrderType = '[新]';
            }
            if (!empty($value['achievement_work_order_type']) && $value['achievement_work_order_type'] == 4) {
                $prefixOrderType = '[续]';
            }
            $prefixEngineer = $value['explainer_type'] == 2 ? '[工]' : '';
            $tmp = [
                'contract_no' => $prefixOrderType . $value['contract_no'],
                'realname' => !empty($value['realname']) ? $value['realname'] . '[' . $value['telphone'] . ']' : '',
                'contract_user_identity' => $value['contract_user_identity'],
                'old_client_name' => $value['old_client_name'],
                'push_apply_name' => $value['push_apply_name'],
                'explainer' => $prefixEngineer . $value['explainer_name'],
                'manage_name' => $value['manage_name'],
                'manage_money' => $value['manage_money'],
                'star_manage_name' => $value['star_manage_name'],
                'star_manage_money' => $value['star_manage_money'],
                'two_manage_name' => $value['two_manage_name'],
                'two_manage_money' => $value['two_manage_money'],
                'five_manage_name' => $value['five_manage_name'],
                'five_manage_money' => $value['five_manage_state'] == 1 ? $value['five_manage_money'] : '0.00',
                'ten_manage_name' => $value['ten_manage_name'],
                'ten_manage_money' => $value['ten_manage_state'] == 1 ? $value['ten_manage_money'] : '0.00',
                'twenty_manage_name' => $value['twenty_manage_name'],
                'star_explainer_name' => $value['star_explainer_name'],
                'twenty_manage_money' => $value['twenty_manage_state'] == 1 ? $value['twenty_manage_money'] : '0.00',
                'device_number' => ' ' . $value['device_number'],
                'add_time' => $value['add_time'] ? date('Y-m-d H:i:s', $value['add_time']) : '',
            ];
            array_push($data, $tmp);
        }

        $tableTile = [
            '合同编号',
            '合同客户',
            '客户身份',
            '老系统客户介绍',
            '推荐人',
            '讲解',
            '经销商',
            '星级讲解人',
            '1星级经销商',
            '2星级经销商',
            '5星级经销商',
            '10星级经销商',
            '20星级经销商',
            '设备台数',
            '结算时间',
        ];
        $field = [
            'contract_no',
            'realname',
            'contract_user_identity',
            'old_client_name',
            'push_apply_name',
            'explainer',
            'manage_name',
            'star_explainer_name',
            'star_manage_name',
            'two_manage_name',
            'five_manage_name',
            'ten_manage_name',
            'twenty_manage_name',
            'device_number',
            'add_time',
        ];
        $sheet_title = '结算明细';
        $title = date('YmdHis') . '结算明细';
        $file_name = date('YmdHis') . '结算明细';

        file_exists($path) || mkdir($path, 0777);
        $file_path = $path;
        $suffix = 'xlsx';
        $output = false;

        $xlx = $this->export($tableTile, $field, $sheet_title, $title, $data, $file_name, $file_path, $suffix, $output);
        $data = [
            'url' => 'work_order/' . $xlx
        ];
        return $this->jsonend(1000, 'ok', $data);
    }

    /**
     * 编辑关系
     * 参数名              类型   是否必须    说明
     * id                   int     是       关系ID
     * push_apply_id        int     是       推荐人ID
     * push_apply_name      string  是       推荐人名称
     * manage_id            int     是       经销商ID
     * manage_name          string  是       经销商名称
     * manage_money         float   是       经销商金额
     * manage_state         int     是       经销商状态
     * star_manage_id       int     是       1星级经销商ID
     * star_manage_name     string  是       1星级经销商名称
     * star_manage_money    float   是       1星级经销商金额
     * star_manage_state    int     是       1星级经销商状态
     * two_manage_id        int     是       2星级经销商ID
     * two_manage_name      string  是       2星级经销商名称
     * two_manage_money     float   是       2星级经销商金额
     * two_manage_state     int     是       2星级经销商状态
     * five_manage_id       int     是       5星级经销商ID
     * five_manage_name     string  是       5星级经销商名称
     * five_manage_money    float   是       5星级经销商金额
     * five_manage_state    int     是       5星级经销商状态
     * ten_manage_id        int     是       10星级经销商ID
     * ten_manage_name      string  是       10星级经销商名称
     * ten_manage_money     float   是       10星级经销商金额
     * ten_manage_state     int     是       10星级经销商状态
     * twenty_manage_id     int     是       20星级经销商ID
     * twenty_manage_name   string  是       20星级经销商名称
     * twenty_manage_money  float   是       20星级经销商金额
     * twenty_manage_state  int     是       20星级经销商状态
     * explainer_type       int     是       讲解类型
     * explainer_id         int     是       讲解人ID
     * explainer_name       string  是       讲解人名称
     * explainer_money      float   是       讲解金额
     * device_number        int     是       设备台数
     * @author ligang
     * @RequestType POST
     * @date 2019/6/4 10:32
     */
    public function http_editSettlementRelations()
    {
        foreach ($this->parm as $key => $value) {
            $this->parm[$key] = trim($value);
        }

        $id = $this->parm['id'] ?? '';
        if (empty($id)) {
            return $this->jsonend(-1000, '数据异常');
        }

        $where = [
            'rq_contract_rank.id' => $id
        ];
        $join = [
            ['customer', 'rq_contract_rank.contract_user_id = rq_customer.user_id', 'LEFT']
        ];
        $field = '
            rq_contract_rank.id,
            rq_customer.realname,
            rq_customer.telphone,
            rq_contract_rank.contract_user_id,
            rq_contract_rank.contract_no,
            rq_contract_rank.contract_user_identity,
            rq_contract_rank.push_apply_id,
            rq_contract_rank.push_apply_name,
            rq_contract_rank.manage_id,
            rq_contract_rank.manage_name,
            rq_contract_rank.manage_money,
            rq_contract_rank.manage_state,
            rq_contract_rank.star_manage_id,
            rq_contract_rank.star_manage_name,
            rq_contract_rank.star_manage_money,
            rq_contract_rank.star_manage_state,
            rq_contract_rank.two_manage_id,
            rq_contract_rank.two_manage_name,
            rq_contract_rank.two_manage_money,
            rq_contract_rank.two_manage_state,
            rq_contract_rank.five_manage_id,
            rq_contract_rank.five_manage_name,
            rq_contract_rank.five_manage_money,
            rq_contract_rank.five_manage_state,
            rq_contract_rank.ten_manage_id,
            rq_contract_rank.ten_manage_name,
            rq_contract_rank.ten_manage_money,
            rq_contract_rank.ten_manage_state,
            rq_contract_rank.twenty_manage_id,
            rq_contract_rank.twenty_manage_name,
            rq_contract_rank.twenty_manage_money,
            rq_contract_rank.twenty_manage_state,
            rq_contract_rank.explainer_type,
            rq_contract_rank.explainer_id,
            rq_contract_rank.explainer_name,
            rq_contract_rank.explainer_money,
            rq_contract_rank.device_number,
            rq_customer.is_old_client,
            rq_customer.old_client_name,
            rq_customer.old_client_tel
        ';
        $this->Achievement->table = 'contract_rank';
        $result = $this->Achievement->findJoinData($where, $field, $join);
        if (empty($result)) {
            return $this->jsonend(-1000, '结算关系不存在');
        }
        //老系统客户介绍修改时同时修改用户的
        $out = ['realname', 'telphone', 'is_old_client', 'old_client_name', 'old_client_tel'];
        $user = [];
        $rank = [];
        foreach ($result as $key => $value) {
            foreach ($this->parm as $k => $v) {
                if ($key == $k && !in_array($key, $out) && $value != $v) {
                    //关系
                    if (stripos($v, '-') != false) {
                        //讲解
                        if (substr($v, 0, 1) == 'D') {
                            //经销商
                            $rank['explainer_type'] = 1;
                            $rank['explainer_id'] = substr($v, stripos($v, '-') + 1);
                        } elseif (substr($v, 0, 1) == 'E') {
                            //工程
                            $rank['explainer_type'] = 2;
                            $rank['explainer_id'] = substr($v, stripos($v, '-') + 1);
                        } else {
                            return $this->jsonend(-1000, '讲解信息错误', '错误代码：50005');
                        }
                        continue;
                    }
                    $rank[$key] = $v;
                }
                if ($key == $k && in_array($key, $out) && $value != $v) {
                    //用户
                    $user[$key] = $v;
                }
            }
        }
        if (empty($user) && empty($rank)) {
            return $this->jsonend(-1000, '未检测到修改数据');
        }
        $check = false;
        $this->db->begin(function () use (&$check, $user, $rank, $result) {
            if (!empty($user)) {
                $this->Achievement->table = 'customer';
                $this->Achievement->updateData(['user_id' => $result['contract_user_id']], $user);
            }
            if (!empty($rank)) {
                $this->Achievement->table = 'contract_rank';
                $this->Achievement->updateData(['id' => $result['id']], $rank);
            }
            $check = true;
        }, function ($e) {
            return $this->jsonend(-1000, '操作失败，错误代码：50003', $e->error);
        });
        if ($check) {
            return $this->jsonend(1000, '操作成功');
        }
        return $this->jsonend(-1000, '操作失败，错误代码：50004');
    }

    /**
     * 获取结算关系详情
     * 参数名  类型  是否必须    说明
     * id       int     是       关系ID
     * @author ligang
     * @RequestType POST
     * @date 2019/6/4 10:32
     */
    public function http_getSettlementRelations()
    {
        $id = $this->parm['id'] ?? '';
        if (empty($id)) {
            return $this->jsonend('-1000', '请选择关系');
        }
        $where = [
            'rq_contract_rank.id' => $id
        ];
        $join = [
            ['customer', 'rq_contract_rank.contract_user_id = rq_customer.user_id', 'LEFT']
        ];
        $field = '
            rq_contract_rank.id,
            rq_customer.realname,
            rq_customer.telphone,
            rq_contract_rank.contract_no,
            rq_contract_rank.contract_user_identity,
            rq_contract_rank.push_apply_id,
            rq_contract_rank.push_apply_name,
            rq_contract_rank.manage_id,
            rq_contract_rank.manage_name,
            rq_contract_rank.manage_money,
            rq_contract_rank.manage_state,
            rq_contract_rank.star_manage_id,
            rq_contract_rank.star_manage_name,
            rq_contract_rank.star_manage_money,
            rq_contract_rank.star_manage_state,
            rq_contract_rank.two_manage_id,
            rq_contract_rank.two_manage_name,
            rq_contract_rank.two_manage_money,
            rq_contract_rank.two_manage_state,
            rq_contract_rank.five_manage_id,
            rq_contract_rank.five_manage_name,
            rq_contract_rank.five_manage_money,
            rq_contract_rank.five_manage_state,
            rq_contract_rank.ten_manage_id,
            rq_contract_rank.ten_manage_name,
            rq_contract_rank.ten_manage_money,
            rq_contract_rank.ten_manage_state,
            rq_contract_rank.twenty_manage_id,
            rq_contract_rank.twenty_manage_name,
            rq_contract_rank.twenty_manage_money,
            rq_contract_rank.twenty_manage_state,
            rq_contract_rank.explainer_type,
            rq_contract_rank.explainer_id,
            rq_contract_rank.explainer_name,
            rq_contract_rank.explainer_money,
            rq_contract_rank.device_number,
            rq_customer.is_old_client,
            rq_customer.old_client_name,
            rq_customer.old_client_tel
        ';
        $this->Achievement->table = 'contract_rank';
        $result = $this->Achievement->findJoinData($where, $field, $join);
        return $this->jsonend(1000, '获取成功', $result);
    }

    /**
     * 获取用户
     * @author ligang
     * @RequestType POST
     * @date 2019/6/4 10:33
     */
    public function http_getUser()
    {
        $where = [
            'account_status' => 1,
            'is_delete' => 0,
        ];
        $field = '
            user_id,
            telphone,
            realname,
            username
        ';
        $order = [
            'is_dealer' => 'DESC'
        ];
        $this->Achievement->table = 'customer';
        $result = $this->Achievement->findData($where, $field, $order);
        $data = [];
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $tmp = [
                    'user_id' => $value['user_id'],
                    'telphone' => $value['telphone'],
                    'name' => $value['realname'] ? $value['realname'] : $value['username'],
                ];
                array_push($data, $tmp);
            }
        }
        return $this->jsonend(1000, '获取成功', $data);
    }

    /**
     * 获取所有经销商
     * @desc 编辑结算关系需要
     * 参数名      类型       是否必须    说明
     * user_id      int         否       用户ID
     * type         int         否       类型，1：经销商，2：用户（不含经销商）
     * is_engineer  int         否       是否包含工程人员，1：是，0：否
     * @author ligang
     * @RequestType POST
     * @date 2019/6/25 17:00
     */
    public function http_getAllUser()
    {
        $user_id = $this->parm['user_id'] ?? '';
        $type = $this->parm['type'] ?? 1;
        $is_engineer = $this->parm['is_engineer'] ?? 0;
        $where = [
            'is_dealer' => 1,
            'is_delete' => 0,
            'account_status' => 1,
        ];
        if ($type == 2) {
            $where['is_dealer'] = 0;
        }
        if (!empty($user_id)) {
            $where['user_id'] = $user_id;
        }
        $field = '
            user_id,
            telphone,
            realname
        ';
        $join = [];
        $order = [
            'account' => 'ASC',
            'telphone' => 'ASC',
        ];
        $this->Achievement->table = 'customer';
        $result = $this->Achievement->selectData($where, $field, $join, $order);
        if (!empty($result)) {
            foreach ($result as $key => &$value) {
                if ($is_engineer == 1) {
                    $value['user_id'] = 'D-' . $value['user_id'];
                }
                $key_arr = array_keys($value);
                foreach ($key_arr as $k => $v) {
                    if (empty($value[$v])) {
                        $value[$v] = '暂无';
                    }
                }
            }

        }
        if ($is_engineer == 1) {
            $e_where = [
                'engineers_status' => 2,
                'is_delete' => 0,
            ];
            $e_field = '
                engineers_id,
                engineers_name,
                engineers_phone
            ';
            $this->Achievement->table = 'engineers';
            $e_result = $this->Achievement->selectData($e_where, $e_field);
            if (!empty($e_result)) {
                foreach ($e_result as $key => &$value) {
                    $value['user_id'] = 'E-' . $value['engineers_id'];
                    $value['realname'] = $value['engineers_name'];
                    $value['telphone'] = $value['engineers_phone'];
                    unset($value['engineers_name']);
                    unset($value['engineers_phone']);
                    $key_arr = array_keys($value);
                    foreach ($key_arr as $k => $v) {
                        if (empty($value[$v])) {
                            $value[$v] = '暂无';
                        }
                    }
                }
            }
            $result = array_merge($result, $e_result);
        }
        return $this->jsonend(1000, '获取成功', $result);
    }

    /**
     * 补送续费券
     */
    public function http_sendRenewCoupon()
    {

        $contract_no = $this->parm['contract_no'] ?? '';
        if (empty($contract_no)) {
            return $this->jsonend(-1000, '缺少合同编号');
        }
        //查询所有生效的合同
        $join = [
            ['customer', 'rq_customer.user_id = rq_contract_rank.contract_user_id', 'left'],
            ['contract', 'rq_contract.contract_no = rq_contract_rank.contract_no', 'left'],
            ['work_order', 'rq_work_order.contract_id = rq_contract.contract_id', 'left'],
        ];
        $field = 'rq_work_order.work_order_id,rq_contract_rank.contract_no,push_apply_id,push_apply_name,rq_customer.is_old_client,effect_time,complete_time,rq_contract_rank.device_number,rq_contract.contract_id';
        $where['rq_contract.status'] = 4;
        $where['rq_contract.business_id'] = 3;
        $where['rq_work_order.visit_status'] = 2;
        $where['rq_contract.contract_no'] = $contract_no;
        $this->Achievement->table = 'contract_rank';
        $data = $this->Achievement->findJoinData($where, $field, $join);
        if (empty($data)) {
            return $this->jsonend(-1000, '暂无需补续费券的合同');
        }
        if ($data['is_old_client'] == 1 && !empty($data['push_apply_id'])) {
            return $this->jsonend(-1000, '老系统客户介绍和推荐人（返券）同时存在,不进行返券');
        }
        if ($data['is_old_client'] == 1) {
            return $this->jsonend(-1000, '老系统客户介绍,不进行返券');
        }
        if (empty($data['push_apply_id'])) {
            return $this->jsonend(-1000, '不存在推荐人,不进行返券');
        }
        $this->Achievement->table = 'coupon_cate';
        $coupon = $this->Achievement->findData(['put_type' => 5]);
        if (empty($coupon)) {
            return $this->jsonend(-1000, '续费抵用券不存在,不进行返券');
        }
        //查询工单回访时间
        $this->Achievement->table = 'work_order_return_visit';
        $order = [
            'visit_time' => 'DESC'
        ];
        $visit = $this->Achievement->selectData(['work_order_id' => $data['work_order_id'], 'status' => 2], 'visit_time', [], $order);
        if (empty($visit)) {
            return $this->jsonend(-1000, '不存在回访记录,不进行返券');
        }

        //判断是否已经赠送过,避免重复赠送
        $this->Achievement->table = 'coupon';
        $contract_rank = $this->Achievement->findData(['from_contract_id' => $data['contract_id']], 'from_contract_id');
        if (!empty($contract_rank)) {
            return $this->jsonend(-1000, '已经赠送过,不进行返券');
        }
        //券状态正常发放
        if ($coupon['status'] == 1) {
            //一台设备发一个
            $coupon_data = [];
            for ($i = 1; $i <= $data['device_number']; $i++) {
                $code = $this->generate_promotion_code();
                $coupon_data_tmp = [
                    'uid' => $data['push_apply_id'],
                    'cid' => $coupon['id'],
                    'code' => $code,
                    'type' => 5,
                    'status' => 1,
                    'send_time' => $visit[0]['visit_time'],
                    'valid_time_start' => $visit[0]['visit_time'],
                    'valid_time_end' => $visit[0]['visit_time'] + 365 * 86400,
                    'qr_code' => $this->getCodeImg($code),
                    'from_contract_id' => $data['contract_id']
                ];
                array_push($coupon_data, $coupon_data_tmp);
            }

            if (empty($coupon_data)) {
                return $this->jsonend(-1000, '不存在设备信息,不进行返券');
            }
            $this->Achievement->table = 'coupon';
            $this->db->begin(function () use (&$check, $coupon_data) {
                foreach ($coupon_data as $key => $value) {
                    $this->Achievement->insertData($value);
                }
                $check = true;
            }, function ($e) {
                return $this->jsonend(-1000, '补送失败', $e->error);
            });
            if ($check) {
                return $this->jsonend(1000, '补送成功' . date('Y-m-d H:i:s'));
            }

        }
        return $this->jsonend(-1000, '补送失败');


    }

    //撤销结算--解决续费重复操作问题导致重复结算问题
    public function http_removeSettlement()
    {
        $achievement_id = $this->parm['achievement_id'] ?? '';
        if (empty($achievement_id)) {
            return $this->jsonend(-1000, '缺少achievement_id');
        }
        $this->Achievement->table = 'oa_achievement';
        $oa_achievement = $this->Achievement->findData(['achievement_id' => $achievement_id], '*');
        if (empty($oa_achievement)) {
            return $this->jsonend(-1000, '业绩信息不存在');
        }
        $this->Achievement->table = 'oa_achievement_workers';
        $oa_achievement_workers = $this->Achievement->selectData(['achievement_id' => $oa_achievement['achievement_id']], '*');
        if (empty($oa_achievement_workers)) {
            return $this->jsonend(-1000, '业绩职工关联表不存在');
        }
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', date('Y-m-d H:i:s') . '开始' . PHP_EOL,true);
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', 'oa_achievement:' . json_encode($oa_achievement) . PHP_EOL,true);
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', 'oa_achievement_workers:' . json_encode($oa_achievement_workers) . PHP_EOL,true);


        $check = false;
        $this->db->begin(function () use (&$check, $oa_achievement_workers, $oa_achievement) {
            //减少经销商账户余额
            $this->Achievement->table = 'oa_workers';
            foreach ($oa_achievement_workers as $k => $v) {
                $workers = $this->Achievement->findData(['workers_id' => $v['workers_id']], 'temp_money');
                $money = $workers['temp_money'] - $v['money'];
                $this->Achievement->updateData(['workers_id' => $v['workers_id']], ['temp_money' => $money]);
                FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', '减少经销商账户:' . $v['workers_id'] . '减少' . $v['money'] . '将' . $workers['temp_money'] . '修改为' . $money . PHP_EOL,true);

            }

            //减少工程人员余额
            $this->Achievement->table = 'oa_engineers_performance';
            $oa_engineers_performance = $this->Achievement->selectData(['achievement_id' => $oa_achievement['achievement_id']], '*');
            if (!empty($oa_engineers_performance)) {
                $this->Achievement->table = 'engineers';
                foreach ($oa_engineers_performance as $k => $v) {
                    $workers = $this->Achievement->findData(['engineers_id' => $v['engineers_id']], 'temp_money');
                    $money = $workers['temp_money'] - $v['money'];
                    $this->Achievement->updateData(['engineers_id' => $v['engineers_id']], ['temp_money' => $money]);
                    FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', '减少工程账户:' . $v['engineers_id'] . '减少' . $v['money'] . '将' . $workers['temp_money'] . '修改为' . $money . PHP_EOL);

                }
            }
            //删除业绩信息
            $this->Achievement->table = 'oa_engineers_performance';
            $this->Achievement->deleteData(['achievement_id' => $oa_achievement['achievement_id']]);
            $this->Achievement->table = 'oa_achievement_workers';
            $this->Achievement->deleteData(['achievement_id' => $oa_achievement['achievement_id']]);
            $this->Achievement->table = 'oa_achievement';
            $this->Achievement->deleteData(['achievement_id' => $oa_achievement['achievement_id']]);
            /*****
             * 2021/12/7修改记录：原是直接删除资金记录，现为了保留记录便于查账不删除原记录并新增退还记录
             *
             */
            //删除资金记录
            $this->Achievement->table = 'funds_record';
            // $this->Achievement->deleteData(['order_id' => $oa_achievement['achievement_id'], 'source' => 2]);
            //新增资金记录
            $this->Achievement->table = 'funds_record';
            $record = $this->Achievement->selectData(['order_id' => $oa_achievement['achievement_id'], 'source' => 2],'user_id,user_type,order_id,money');
            foreach ($record as $k=>$v){
                $item = [];
                $item['user_id'] = $v['user_id'];
                $item['user_type'] = $v['user_type'];
                $item['source'] = 8;
                $item['account_type'] = 1;
                $item['order_id'] = $v['order_id'];
                $item['money'] = $v['money'];
                $item['note'] = '撤销结算金额退还(出现了退款),合同编号:'.$oa_achievement['contract_number'];
                $item['is_available'] = 2;
                $item['add_time'] = time();
                $this->Achievement->insertData($item);
            }
            //删除优惠券
            $this->Achievement->table = 'coupon';
            $coupon = $this->Achievement->findData(['from_achievement_id' => $oa_achievement['achievement_id'], 'type' => 5, 'status' => 1], 'coupon_id');
            if (!empty($coupon)) {
                $this->Achievement->deleteData(['coupon_id' => $coupon['coupon_id']]);
            }
            $check = true;
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', date('Y-m-d H:i:s') . '结束' . PHP_EOL);

        }, function ($e) {
            return $this->jsonend(-1000, 'FAIL' . $e->error);
        });
        if ($check) {
            return $this->jsonend(1000, 'success');
        }

        return $this->jsonend(-1000, 'FAIL');
    }
    /**
     * 重新结算
     */
    public function http_againSettlement(){
        $achievement_id = $this->parm['achievement_id'] ?? '';
        if (empty($achievement_id)) {
            return $this->jsonend(-1000, '缺少achievement_id');
        }
        $this->Achievement->table = 'oa_achievement';
        $oa_achievement = $this->Achievement->findData(['achievement_id' => $achievement_id], '*');
        if (empty($oa_achievement)) {
            return $this->jsonend(-1000, '业绩信息不存在');
        }
        $this->Achievement->table = 'oa_achievement_workers';
        $oa_achievement_workers = $this->Achievement->selectData(['achievement_id' => $oa_achievement['achievement_id']], '*');
        if (empty($oa_achievement_workers)) {
            return $this->jsonend(-1000, '业绩职工关联表不存在');
        }
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', date('Y-m-d H:i:s') . '开始' . PHP_EOL,true);
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', 'oa_achievement:' . json_encode($oa_achievement) . PHP_EOL,true);
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', 'oa_achievement_workers:' . json_encode($oa_achievement_workers) . PHP_EOL,true);


        $check = false;
        $this->db->begin(function () use (&$check, $oa_achievement_workers, $oa_achievement) {

            //减少经销商账户余额
            $this->Achievement->table = 'oa_workers';
            foreach ($oa_achievement_workers as $k => $v) {
                $workers = $this->Achievement->findData(['workers_id' => $v['workers_id']], 'temp_money');
                $money = $workers['temp_money'] - $v['money'];
                $this->Achievement->updateData(['workers_id' => $v['workers_id']], ['temp_money' => $money]);
                FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', '减少经销商账户:' . $v['workers_id'] . '减少' . $v['money'] . '将' . $workers['temp_money'] . '修改为' . $money . PHP_EOL,true);

            }

            //减少工程人员余额
            $this->Achievement->table = 'oa_engineers_performance';
            $oa_engineers_performance = $this->Achievement->selectData(['achievement_id' => $oa_achievement['achievement_id']], '*');
            if (!empty($oa_engineers_performance)) {
                $this->Achievement->table = 'engineers';
                foreach ($oa_engineers_performance as $k => $v) {
                    $workers = $this->Achievement->findData(['engineers_id' => $v['engineers_id']], 'temp_money');
                    $money = $workers['temp_money'] - $v['money'];
                    $this->Achievement->updateData(['engineers_id' => $v['engineers_id']], ['temp_money' => $money]);
                    FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', '减少工程账户:' . $v['engineers_id'] . '减少' . $v['money'] . '将' . $workers['temp_money'] . '修改为' . $money . PHP_EOL);

                }
            }
            //删除业绩信息
            $this->Achievement->table = 'oa_engineers_performance';
            $this->Achievement->deleteData(['achievement_id' => $oa_achievement['achievement_id']]);
            $this->Achievement->table = 'oa_achievement_workers';
            $this->Achievement->deleteData(['achievement_id' => $oa_achievement['achievement_id']]);
            $this->Achievement->table = 'oa_achievement';
            $this->Achievement->deleteData(['achievement_id' => $oa_achievement['achievement_id']]);
            /*****
             * 2021/12/7修改记录：原是直接删除资金记录，现为了保留记录便于查账不删除原记录并新增退还记录
             *
             */
            //删除资金记录
            $this->Achievement->table = 'funds_record';
            // $this->Achievement->deleteData(['order_id' => $oa_achievement['achievement_id'], 'source' => 2]);
            //新增资金记录
            $this->Achievement->table = 'funds_record';
            $record = $this->Achievement->selectData(['order_id' => $oa_achievement['achievement_id'], 'source' => 2],'user_id,user_type,order_id,money');
            foreach ($record as $k=>$v){
                $item = [];
                $item['user_id'] = $v['user_id'];
                $item['user_type'] = $v['user_type'];
                $item['source'] = 8;
                $item['account_type'] = 1;
                $item['order_id'] = $v['order_id'];
                $item['money'] = $v['money'];
                $item['note'] = '撤销结算金额退还(出现了退款),合同编号:'.$oa_achievement['contract_number'];
                $item['is_available'] = 2;
                $item['add_time'] = time();
                $this->Achievement->insertData($item);
            }
            //删除优惠券
            $this->Achievement->table = 'coupon';
            $coupon = $this->Achievement->findData(['from_achievement_id' => $oa_achievement['achievement_id'], 'type' => 5, 'status' => 1], 'coupon_id');
            if (!empty($coupon)) {
                $this->Achievement->deleteData(['coupon_id' => $coupon['coupon_id']]);
            }
            $check = true;


            FileLogService::WriteLog(SLog::DEBUG_LOG, 'removeSettlement', date('Y-m-d H:i:s') . '结束' . PHP_EOL);

        }, function ($e) {
            return $this->jsonend(-1000, 'FAIL' . $e->error);
        });
        if ($check) {
            //重新结算
            $type= $oa_achievement['achievement_work_order_type']==3?1:2;
            $result = $this->balanceFun($oa_achievement['contract_number'],$type, $oa_achievement['add_time'], $oa_achievement['from_renew_order_id'],0);
            return $this->jsonend(1000, 'success',['res'=>$result,'oa'=>$oa_achievement]);
        }

        return $this->jsonend(-1000, 'FAIL',$oa_achievement);
    }
    /**
     * 导出业绩
     */
    public function http_exportAchievement()
    {
        $type = $this->parm['type'] ?? 1;//1导出统计 2导出明细
        $start_time = empty($this->parm['start_time'] ?? '') ? 0 : strtotime($this->parm['start_time'] . '00:00:00');
        $end_time = empty($this->parm['end_time'] ?? '') ? strtotime(date('Y-m-d 23:59:59', time())) : strtotime($this->parm['end_time'] . '23:59:59');
        //获取2019装机的合同
        $this->Achievement->table = 'contract';
        $lists = $this->Achievement->selectData(['status' => ['IN', [4, 5, 6]], 'now_date' => ['between', [$start_time, $end_time], 'company_id' => $this->company]], 'contract_no,installed_time,status,now_date,contract_id');
        $contrac_no_arr = [];
        $contract_data = [];
        $i = 0;
        foreach ($lists as $k => $v) {
            $achievement_time = $v['now_date'];
            //如果是2019.1.1前装机的根据派单时间
            if ($v['installed_time'] < strtotime('2019-01-01')) {
                $achievement_time = '';
                $this->Achievement->table = 'work_order';
                $join = [
                    ['work_order_business wob', 'wob.work_order_id=rq_work_order.work_order_id', 'left']
                ];
                $work_order_info = $this->Achievement->findJoinData(['contract_id' => $v['contract_id'], 'wob.business_id' => 3], 'order_time', $join);
                if (!empty($work_order_info)) {
                    $achievement_time = $work_order_info['order_time'];
                }
            }
            if (empty($achievement_time)) {
                // FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', $v['contract_no'] . '-装机时间小于2019.1.1并且派单时间为空' . PHP_EOL);
                continue;
            }
            if ($achievement_time < $end_time) {
                //判断是否结算
                $this->Achievement->table = 'oa_achievement';
                $achievement = $this->Achievement->findJoinData(['contract_number' => $v['contract_no'], 'achievement_work_order_type' => 3, 'state' => 1], 'achievement_id', []);
                if (empty($achievement)) {
                    continue;
                }
                array_push($contrac_no_arr, $v['contract_no']);
                $i++;
                // FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', $v['contract_no'] . '-' . date('Y-m-d H:i:s', $achievement_time) . PHP_EOL);
            }

        }
        if ($type == 2) {
            //组装导出明细数据
            foreach ($contrac_no_arr as $k => $v) {
                $this->Achievement->table = 'contract_rank';
                $contract_rank = $this->Achievement->findData(['contract_no' => $v], 'manage_id');
                $this->Achievement->table = 'oa_workers';
                $workers = $this->Achievement->findJoinData(['rq_oa_workers.user_id' => $contract_rank['manage_id']], 'workers_name,workers_phone,workers_entry_time,inspection_standard');
                if (empty($workers['workers_phone'])) {
                    continue;
                }
                $item['workers_entry_time'] = date('Y年m月d日', $workers['workers_entry_time']);
                $item['name'] = $workers['workers_name'] . '(联系电话:' . $workers['workers_phone'] . ')';
                $item['contract_no'] = $v['contract_no'] . ' ';
                $item['installed_time'] = date('Y年m月d日', $v['installed_time']);
                $item['num'] = '1台';
                $contract_data[] = $item;
            }

            $sheet_title = '物联网业绩明细表';
            $title = '物联网业绩明细表';
            $file_name = date('YmdHis') . '-物联网业绩明细表';
            $path = WWW_DIR . '/export_achievement/';
            file_exists($path) || mkdir($path, 0777);
            $file_path = $path;
            $suffix = 'xlsx';
            $output = false;
            $tableTile = ['合同编号', '装机时间', '经销商姓名', '升级（入职）时间', '业绩'];
            $field = ['contract_no', 'installed_time', 'name', 'workers_entry_time', 'num'];
            $xlx = $this->export($tableTile, $field, $sheet_title, $title, $contract_data, $file_name, $file_path, $suffix, $output);
            $url = $this->config->get('callback_domain_name');
            if ($this->dev_mode == 1) {
                $url = $this->config->get('debug_config.callback_domain_name');
            }
            $data = [
                'url' => $url . '/export_achievement/' . $xlx
            ];
            return $this->jsonend(1000, 'ok', $data);
        } else {
            // FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', "共" . $i . '个合同' . PHP_EOL);
            $this->Achievement->table = 'contract_rank';
            $where['contract_no'] = ['IN', $contrac_no_arr];
            $contract_rank = $this->Achievement->selectData($where, 'manage_id,count(*) count', [], [], 'manage_id');
            $data = [];
            foreach ($contract_rank as $k => $v) {
                $this->Achievement->table = 'oa_workers';
                $join = [
                    ['customer c', 'c.user_id=rq_oa_workers.user_id', 'left']
                ];
                $workers = $this->Achievement->findJoinData(['rq_oa_workers.user_id' => $v['manage_id']], 'workers_name,workers_phone,workers_entry_time,inspection_standard,c.city,c.area', $join);
                if (empty($workers['workers_phone'])) {
                    continue;
                }
                $item['area'] = $workers['city'] . $workers['area'];
                $item['workers_entry_time'] = date('Y年m月d日', $workers['workers_entry_time']);
                $item['role'] = '经销商';
                $item['name'] = $workers['workers_name'] . '(联系电话:' . $workers['workers_phone'] . ')';
                $item['num'] = $v['count'] . '台';
                $data[] = $item;
                //FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', $workers['workers_name'] . '(联系电话:' . $workers['workers_phone'] . ')共' . $v['count'] . '台' . PHP_EOL);
            }
            $sheet_title = '物联网业绩表';
            $title = '物联网业绩表';
            $file_name = date('YmdHis') . '-年物联网业绩表';
            $path = WWW_DIR . '/export_achievement/';
            file_exists($path) || mkdir($path, 0777);
            $file_path = $path;
            $suffix = 'xlsx';
            $output = false;
            $tableTile = ['地区', '升级（入职）时间', '角色', '姓名', '累计业绩'];
            $field = ['area', 'workers_entry_time', 'role', 'name', 'num'];
            $xlx = $this->export($tableTile, $field, $sheet_title, $title, $data, $file_name, $file_path, $suffix, $output);
            $url = $this->config->get('callback_domain_name');
            if ($this->dev_mode == 1) {
                $url = $this->config->get('debug_config.callback_domain_name');
            }
            $data = [
                'url' => $url . '/export_achievement/' . $xlx
            ];
        }
        return $this->jsonend(1000, 'ok', $data);


    }
    /*
        * 导出考核数据
        */
    public function http_adminExamineAchievementDetail()
    {
        //接收参数
        $type = $this->parm['type'] ?? -1;//1导出统计 2导出明细
        $keywords = $this->parm['keywords'] ?? '';//关键字搜索
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $is_export = $this->parm['is_export'] ?? 0;//是否是导出
        $is_admin = $this->parm['is_admin'] ?? 0;//是否是后台查看
        if ($is_export == 1) {
            $pageSize = -1;
        }
        $c_time = $this->parm['c_time'] ?? '';
        $inspection_standard = 0;
        $inspection_results = 0;
        $start_time = empty($this->parm['start_time'] ?? '') ? 0 : strtotime($this->parm['start_time'] . '00:00:00');
        $end_time = empty($this->parm['end_time'] ?? '') ? strtotime(date('Y-m-d 23:59:59', time())) : strtotime($this->parm['end_time'] . '23:59:59');
        $user_id = empty($this->parm['user_id']) ? $this->user_id : $this->parm['user_id'];
        //如果查询某个人的业绩,仅查询考核周期内的
        if ($is_admin == 0 && (!empty($this->user_id) || !empty($this->parm['user_id']))) {
            $user_id = empty($this->parm['user_id']) ? $this->user_id : $this->parm['user_id'];
            $this->Achievement->table = 'oa_workers';
            $workers_info = $this->Achievement->findJoinData(['user_id' => $user_id, 'is_delete' => 0], 'inspection_start_time,inspection_end_time,inspection_standard,inspection_results', []);
            if (!empty($workers_info)) {
                $start_time = $workers_info['inspection_start_time'];
                $end_time = $workers_info['inspection_end_time'];
                $inspection_standard = $workers_info['inspection_standard'];
                $inspection_results = $workers_info['inspection_results'];
            }

        }


        //业绩系统传的时间格式
        if (!empty($c_time) && stripos($c_time, '~') === false) {
            return $this->jsonend(-1000, '时间格式错误');
        }
        if (!empty($c_time)) {
            $c_time = explode('~', $c_time);
            $start_time = strtotime(trim($c_time[0]) . ' 00:00:00');
            $end_time = strtotime(trim($c_time[1]) . ' 23:59:59');
        }

        //获取2019装机的合同
        $this->Achievement->table = 'oa_achievement';
        $join = [
            ['contract', 'rq_contract.contract_no=rq_oa_achievement.contract_number', 'left'],
        ];
        $lists = $this->Achievement->selectData(['rq_contract.status' => ['IN', [4, 5, 6]], 'rq_contract.now_date' => ['between', [$start_time, $end_time], 'rq_contract.company_id' => $this->company, 'achievement_work_order_type' => 3]], 'rq_contract.contract_no,rq_contract.installed_time,rq_contract.effect_time,now_date,rq_contract.status,rq_contract.contract_id', $join);
        if (empty($lists)) {
            return $this->jsonend(-1000, '暂无数据');
        }
        $contrac_no = [];
        $i = 0;
        foreach ($lists as $k => $v) {
            $achievement_time = $v['now_date'];
            //如果是2019.1.1前装机的根据派单时间
            if ($v['installed_time'] < strtotime('2019-01-01')) {
                $achievement_time = '';
                $this->Achievement->table = 'work_order';
                $join = [
                    ['work_order_business wob', 'wob.work_order_id=rq_work_order.work_order_id', 'left']
                ];
                $work_order_info = $this->Achievement->findJoinData(['contract_id' => $v['contract_id'], 'wob.business_id' => 3], 'order_time', $join);
                if (!empty($work_order_info)) {
                    $achievement_time = $work_order_info['order_time'];
                }
            }
            if (!empty($achievement_time) && $achievement_time < $end_time) {
                //判断是否结算
                $this->Achievement->table = 'oa_achievement';
                $achievement = $this->Achievement->findJoinData(['contract_number' => $v['contract_no'], 'achievement_work_order_type' => 3, 'state' => 1], 'achievement_id', []);
                if (empty($achievement)) {
                    continue;
                }
                array_push($contrac_no, $v['contract_no']);
                $i++;
                // FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', $v['contract_no'] . '-' . date('Y-m-d H:i:s', $achievement_time) . PHP_EOL);
            }
        }
        if(empty($contrac_no)){
            $inspection_info['inspection_results'] = $inspection_results;
            $inspection_info['inspection_standard'] = $inspection_standard;
            $inspection_info['inspection_date'] = date('Y年m月d日', $start_time) . ' 至 ' . date('Y年m月d日', $end_time);
            $data = [
                'inspection_info' => $inspection_info,
                'data' => [],
                'page' => [
                    'current_page' => $page,
                    'row' => $pageSize,
                    'total' => 0,
                    'total_page' => 0
                ]
            ];
            return $this->jsonend(-1000, '暂无数据', $data);
        }
        //查询数据
        $this->Achievement->table = 'oa_achievement_workers';
        $joins = [
            ['oa_achievement h', 'h.achievement_id=rq_oa_achievement_workers.achievement_id', 'left'],
            ['oa_workers w', 'w.workers_id=rq_oa_achievement_workers.workers_id', 'left'],
            ['contract c', 'c.contract_no=h.contract_number', 'left'],
            ['customer u', 'c.user_id=u.user_id', 'left'],
        ];

        $field = 'rq_oa_achievement_workers.id,
                rq_oa_achievement_workers.money,
                rq_oa_achievement_workers.is_available,
                rq_oa_achievement_workers.source,
                rq_oa_achievement_workers.workers_position,
                rq_oa_achievement_workers.add_time,
                h.contract_number,
                h.contract_customer,
                h.equipments_number,          
                h.achievement_work_order_type,          
                w.workers_id,
                w.workers_name,
                w.workers_phone,
                w.workers_number,
                w.workers_entry_time,
                w.city,
                w.area,
                w.check_out,
                w.add_time,
                w.inspection_total,
                w.inspection_results,
                w.inspection_standard,
                w.inspection_start_time,
                w.inspection_end_time,
                c.installed_time,c.now_date,u.realname,u.telphone';

        $where = [
            'rq_oa_achievement_workers.workers_position' => 3,
            'h.contract_number' => ['IN', $contrac_no],
            'c.company_id' => $this->company,
            'w.position' => 3,

        ];
        if (!empty($this->user_id) && $is_admin == 0) {
            $where['w.user_id'] = $this->user_id;
        }
        if (!empty($this->parm['user_id'])) {
            $where['w.user_id'] = $this->parm['user_id'];
        }
        if (!empty($keywords)) {
            $where['h.contract_number|w.workers_phone|w.workers_name'] = ['LIKE', '%' . trim($keywords) . '%'];
        }
        if (!empty($this->parm['inspection_end_time'])) {
            $where['w.inspection_end_time'] = ['<=', strtotime($this->parm['inspection_end_time'])+ 86400 - 1];
        }
        $order = [
            'h.add_time' => 'desc'
        ];
        $inspection_info = [];
        $workers_id = [];
        $result = $this->Achievement->selectPageData($where, $field, $joins, -1, 1, $order, 'h.contract_number,w.workers_id');
        $inspection_info['inspection_results'] = $inspection_results;
        $inspection_info['inspection_standard'] = $inspection_standard;
        $inspection_info['inspection_date'] = date('Y年m月d日', $start_time) . ' 至 ' . date('Y年m月d日', $end_time);
        if (empty($result)) {
            $data = [
                'inspection_info' => $inspection_info,
                'data' => [],
                'page' => [
                    'current_page' => $page,
                    'row' => $pageSize,
                    'total' => 0,
                    'total_page' => 0
                ]
            ];
            return $this->jsonend(-1000, '暂无数据', $data);
        }
        foreach ($result as $k => $v) {
            $result[$k]['name'] = $v['workers_name'] . '(tel:' . $v['workers_phone'] . ')';
            $result[$k]['contract_number'] = $v['contract_number'] . ' ';
            $result[$k]['installed_time'] = $v['installed_time'] ? date('Y年m月d日', $v['installed_time']) : '--';
            $result[$k]['now_date'] = $v['now_date'] ? date('Y年m月d日', $v['now_date']) : '--';
            $result[$k]['add_time'] = $v['add_time'] ? date('Y年m月d日', $v['add_time']) : '--';
            $result[$k]['workers_entry_time'] = $v['workers_entry_time'] ? date('Y年m月d日', $v['workers_entry_time']) : '--';
            $result[$k]['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);
            $result[$k]['equipments_number'] = $v['equipments_number'] . ' ' ?? '';
            $result[$k]['inspection_total'] = $v['inspection_total'] . ' ' ?? 0;
            $result[$k]['inspection_results'] = $v['inspection_results'] . ' ' ?? 0;
            $result[$k]['inspection_standard'] = $v['inspection_standard'] . ' ' ?? 0;
            $result[$k]['user'] = $v['realname'] . '(联系电话:' . $v['telphone'] . ')';
            //组装导出明细数据
            $item['name'] = $v['workers_name'] . '(tel:' . $v['workers_phone'] . ')';
            $item['contract_number'] = $v['contract_number'] . ' ';
            $item['installed_time'] = $v['installed_time'] ? date('Y年m月d日', $v['installed_time']) : '--';
            $item['now_date'] = $v['now_date'] ? date('Y年m月d日', $v['now_date']) : '--';
            $item['add_time'] = $v['add_time'] ? date('Y年m月d日', $v['add_time']) : '--';
            $item['workers_entry_time'] = $v['workers_entry_time'] ? date('Y年m月d日', $v['workers_entry_time']) : '--';
            $item['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);
            $item['equipments_number'] = $v['equipments_number'] . ' ' ?? '';
            $item['inspection_total'] = $v['inspection_total'] . ' ' ?? 0;
            $item['inspection_results'] = $v['inspection_results'] . ' ' ?? 0;
            $item['inspection_standard'] = $v['inspection_standard'] . ' ' ?? 0;
            $item['user'] = $v['realname'] . '(联系电话:' . $v['telphone'] . ')';
            $contract_data[] = $item;
            array_push($workers_id, $v['workers_id']);

        }

        if ($type == 1) {//业绩考核统计
            $this->Achievement->table = 'oa_workers';
            $wheres['workers_id'] = ['IN', $workers_id];
            $work_list = $this->Achievement->selectPageData($wheres, '*', [], -1, 1, ['inspection_total' => 'desc'], 'workers_id');
            foreach ($work_list as $k => $v) {
                $work_list[$k]['workers_entry_time'] = $v['workers_entry_time'] ? date('Y年m月d日', $v['workers_entry_time']) : '--';
                $work_list[$k]['add_time'] = $v['add_time'] ? date('Y年m月d日', $v['add_time']) : '--';
                $work_list[$k]['workers_sex'] = $v['workers_sex'] == 1 ? '男' : '女';
                $work_list[$k]['role'] = '经销商';
                $work_list[$k]['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);
                $work_list[$k]['inspection_total'] = $v['inspection_total'] . ' ' ?? 0;
                $work_list[$k]['inspection_results'] = $v['inspection_results'] . ' ' ?? 0;
                $work_list[$k]['inspection_standard'] = $v['inspection_standard'] . ' ' ?? 0;
                $work_list[$k]['workers_position'] = $v['position'];
                $work_list[$k]['check_out'] = $v['check_out'] == 1 ? '是' : '否';
                $work_list[$k]['workers_name'] = $v['workers_name'] . '(工号:' . $v['workers_number'] . ',联系电话:' . $v['workers_phone'] . ')';
            }

            if ($is_export == 1) {
                //导出统计
                $sheet_title = '经销商业绩考核统计表';
                $title = '经销商业绩考核统计表';
                $file_name = date('YmdHis') . '-经销商业绩考核统计表';
                $path = WWW_DIR . '/export_achievement/';
                file_exists($path) || mkdir($path, 0777);
                $file_path = $path;
                $suffix = 'xlsx';
                $output = false;
                $tableTile = ['角色', '员工', '区域', '升级（入职）时间','添加时间', '电话', '性别', '是否合格', '累计完成/台', '实际完成/台', '考核标准/台', '考核周期'];
                $field = ['role', 'workers_name', 'area', 'workers_entry_time','add_time', 'workers_phone', 'workers_sex', 'check_out', 'inspection_total', 'inspection_results', 'inspection_standard', 'inspection_date'];
                $xlx = $this->export($tableTile, $field, $sheet_title, $title, $work_list, $file_name, $file_path, $suffix, $output);
                $url = $this->config->get('callback_domain_name');
                if ($this->dev_mode == 1) {
                    $url = $this->config->get('debug_config.callback_domain_name');
                }
                $export_data = [
                    //'url' => $url . '/export_achievement/' . $xlx
                    'url' => 'export_achievement/' . $xlx
                ];
                return $this->jsonend(1000, 'ok', $export_data);
            } else {

                $this->Achievement->table = 'oa_achievement_workers';
                $count = $this->Achievement->selectData($where, 'rq_oa_achievement_workers.id', $joins, [], 'w.workers_id');
                $count = count($count);
                $work_list = arrPage($work_list, $page, $pageSize);
                $data = [
                    'inspection_info' => $inspection_info,
                    'data' => $work_list,
                    'page' => [
                        'current_page' => $page,
                        'row' => $pageSize,
                        'total' => $count,
                        'total_page' => ceil($count / $pageSize)
                    ]
                ];
                return $this->jsonend(1000, 'ok', $data);
            }


        } else if ($type == 2 || $type==-1) {//业绩考核明细
            if ($is_export == 1) {
                //组装导出明细数据
                $sheet_title = '经销商业绩考核明细表';
                $title = '经销商业绩考核明细表';
                $file_name = date('YmdHis') . '-经销商业绩考核明细表';
                $path = WWW_DIR . '/export_achievement/';
                file_exists($path) || mkdir($path, 0777);
                $file_path = $path;
                $suffix = 'xlsx';
                $output = false;
                $tableTile = ['合同编号','客户', '装机时间', '签约时间','经销商姓名', '升级（入职）时间','添加时间', '设备台数/台', '实际完成/台', '考核标准/台', '考核周期'];
                $field = ['contract_number','user', 'installed_time','now_date', 'name', 'workers_entry_time', 'add_time','equipments_number', 'inspection_results', 'inspection_standard', 'inspection_date'];
                $xlx = $this->export($tableTile, $field, $sheet_title, $title, $contract_data, $file_name, $file_path, $suffix, $output);
                $url = $this->config->get('callback_domain_name');
                if ($this->dev_mode == 1) {
                    $url = $this->config->get('debug_config.callback_domain_name');
                }
                $export_data = [
                    //'url' => $url . '/export_achievement/' . $xlx
                    'url' => 'export_achievement/' . $xlx
                ];
                return $this->jsonend(1000, 'ok', $export_data);
            } else {
                $this->Achievement->table = 'oa_achievement_workers';
                $count = $this->Achievement->selectData($where, 'rq_oa_achievement_workers.id', $joins, [], 'h.contract_number,w.workers_id');
                $count = count($count);
                $result = arrPage($result, $page, $pageSize);
                $inspection_info['inspection_results'] = $count;
                //查询数据
                $this->Achievement->table = 'oa_achievement_workers';
                $total_joins = [
                    ['oa_achievement h', 'h.achievement_id=rq_oa_achievement_workers.achievement_id', 'left'],
                    ['oa_workers w', 'w.workers_id=rq_oa_achievement_workers.workers_id', 'left'],
                    ['contract c', 'c.contract_no=h.contract_number', 'left'],
                    ['customer u', 'c.user_id=u.user_id', 'left'],
                ];
                $total_where = [
                    'rq_oa_achievement_workers.workers_position' => 3,
                    'c.company_id' => $this->company,
                    'w.position' => 3,
                    'w.user_id'=>$user_id
                ];
                $total_count = $this->Achievement->selectData($total_where, 'rq_oa_achievement_workers.id', $total_joins, [], 'h.contract_number,w.workers_id');
                $total_count = count($total_count);
                $inspection_info['inspection_total'] = $total_count;
                $data = [
                    'inspection_info' => $inspection_info,
                    'data' => $result,
                    'page' => [
                        'current_page' => $page,
                        'row' => $pageSize,
                        'total' => $count,
                        'total_page' => ceil($count / $pageSize)
                    ]
                ];
                return $this->jsonend(1000, 'ok', $data);
            }
        }


    }
    /*
     * 导出考核数据
     */
    public function http_examineAchievementDetail()
    {
        //接收参数
        $type = $this->parm['type'] ?? -1;//1导出统计 2导出明细
        $keywords = $this->parm['keywords'] ?? '';//关键字搜索
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $is_export = $this->parm['is_export'] ?? 0;//是否是导出
        $is_admin = $this->parm['is_admin'] ?? 0;//是否是后台查看
        if ($is_export == 1) {
            $pageSize = -1;
        }
        $c_time = $this->parm['c_time'] ?? '';
        $inspection_standard = 0;
        $inspection_results = 0;
        $start_time = empty($this->parm['start_time'] ?? '') ? 0 : strtotime($this->parm['start_time'] . '00:00:00');
        $end_time = empty($this->parm['end_time'] ?? '') ? strtotime(date('Y-m-d 23:59:59', time())) : strtotime($this->parm['end_time'] . '23:59:59');
        //如果查询某个人的业绩,仅查询考核周期内的
        if ($is_admin == 0 && (!empty($this->user_id) || !empty($this->parm['user_id']))) {
            $user_id = empty($this->parm['user_id']) ? $this->user_id : $this->parm['user_id'];
            $this->Achievement->table = 'oa_workers';
            $workers_info = $this->Achievement->findJoinData(['user_id' => $user_id, 'is_delete' => 0], 'inspection_start_time,inspection_end_time,inspection_standard,inspection_results', []);
            if (!empty($workers_info)) {
                $start_time = $workers_info['inspection_start_time'];
                $end_time = $workers_info['inspection_end_time'];
                $inspection_standard = $workers_info['inspection_standard'];
                $inspection_results = $workers_info['inspection_results'];
            }

        }


        //业绩系统传的时间格式
        if (!empty($c_time) && stripos($c_time, '~') === false) {
            return $this->jsonend(-1000, '时间格式错误');
        }
        if (!empty($c_time)) {
            $c_time = explode('~', $c_time);
            $start_time = strtotime(trim($c_time[0]) . ' 00:00:00');
            $end_time = strtotime(trim($c_time[1]) . ' 23:59:59');
        }

        //获取2019装机的合同
        $this->Achievement->table = 'oa_achievement';
        $join = [
            ['contract', 'rq_contract.contract_no=rq_oa_achievement.contract_number', 'left'],
        ];
        $lists = $this->Achievement->selectData(['rq_contract.status' => ['IN', [4, 5, 6]], 'rq_contract.now_date' => ['between', [$start_time, $end_time], 'rq_contract.company_id' => $this->company, 'achievement_work_order_type' => 3]], 'rq_contract.contract_no,rq_contract.installed_time,rq_contract.effect_time,now_date,rq_contract.status,rq_contract.contract_id', $join);
        if (empty($lists)) {
            return $this->jsonend(-1000, '暂无数据');
        }
        $contrac_no = [];
        $i = 0;
        foreach ($lists as $k => $v) {
            $achievement_time = $v['now_date'];
            //如果是2019.1.1前装机的根据派单时间
            if ($v['installed_time'] < strtotime('2019-01-01')) {
                $achievement_time = '';
                $this->Achievement->table = 'work_order';
                $join = [
                    ['work_order_business wob', 'wob.work_order_id=rq_work_order.work_order_id', 'left']
                ];
                $work_order_info = $this->Achievement->findJoinData(['contract_id' => $v['contract_id'], 'wob.business_id' => 3], 'order_time', $join);
                if (!empty($work_order_info)) {
                    $achievement_time = $work_order_info['order_time'];
                }
            }
            if (!empty($achievement_time) && $achievement_time < $end_time) {
                //判断是否结算
                $this->Achievement->table = 'oa_achievement';
                $achievement = $this->Achievement->findJoinData(['contract_number' => $v['contract_no'], 'achievement_work_order_type' => 3, 'state' => 1], 'achievement_id', []);
                if (empty($achievement)) {
                    continue;
                }
                array_push($contrac_no, $v['contract_no']);
                $i++;
                // FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', $v['contract_no'] . '-' . date('Y-m-d H:i:s', $achievement_time) . PHP_EOL);
            }
        }
        if(empty($contrac_no)){
            $inspection_info['inspection_results'] = $inspection_results;
            $inspection_info['inspection_standard'] = $inspection_standard;
            $inspection_info['inspection_date'] = date('Y年m月d日', $start_time) . ' 至 ' . date('Y年m月d日', $end_time);
            $data = [
                'inspection_info' => $inspection_info,
                'data' => [],
                'page' => [
                    'current_page' => $page,
                    'row' => $pageSize,
                    'total' => 0,
                    'total_page' => 0
                ]
            ];
            return $this->jsonend(-1000, '暂无数据', $data);
        }
        //查询数据
        $this->Achievement->table = 'oa_achievement_workers';
        $joins = [
            ['oa_achievement h', 'h.achievement_id=rq_oa_achievement_workers.achievement_id', 'left'],
            ['oa_workers w', 'w.workers_id=rq_oa_achievement_workers.workers_id', 'left'],
            ['contract c', 'c.contract_no=h.contract_number', 'left'],
            ['customer u', 'c.user_id=u.user_id', 'left'],
        ];


        $field = 'rq_oa_achievement_workers.id,
                rq_oa_achievement_workers.money,
                rq_oa_achievement_workers.is_available,
                rq_oa_achievement_workers.source,
                rq_oa_achievement_workers.workers_position,
                rq_oa_achievement_workers.add_time,
                c.contract_id,
                h.contract_number,
                h.contract_customer,
                h.equipments_number,          
                h.achievement_work_order_type,          
                w.workers_id,
                w.workers_name,
                w.workers_phone,
                w.workers_number,
                w.workers_entry_time,
                w.city,
                w.area,
                w.check_out,
                w.add_time,
                w.inspection_total,
                w.inspection_results,
                w.inspection_standard,
                w.inspection_start_time,
                w.inspection_end_time,
                c.installed_time,c.now_date,u.realname,u.telphone';

        $where = [
            'rq_oa_achievement_workers.workers_position' => 3,
            'h.contract_number' => ['IN', $contrac_no],
            'c.company_id' => $this->company,
            'w.position' => 3,

        ];
        if (!empty($this->user_id) && $is_admin == 0) {
            $where['w.user_id'] = $this->user_id;
        }
        if (!empty($this->parm['user_id'])) {
            $where['w.user_id'] = $this->parm['user_id'];
        }
        if (!empty($keywords)) {
            $where['h.contract_number|w.workers_phone|w.workers_name'] = ['LIKE', '%' . trim($keywords) . '%'];
        }
        if (!empty($this->parm['inspection_end_time'])) {
            $where['w.inspection_end_time'] = ['<=', strtotime($this->parm['inspection_end_time'])+ 86400 - 1];
        }
        $order = [
            'h.add_time' => 'desc'
        ];
        $inspection_info = [];
        $workers_id = [];
        $result = $this->Achievement->selectPageData($where, $field, $joins, -1, 1, $order, 'h.contract_number,w.workers_id');
        $inspection_info['inspection_results'] = $inspection_results;
        $inspection_info['inspection_standard'] = $inspection_standard;
        $inspection_info['inspection_date'] = date('Y年m月d日', $start_time) . ' 至 ' . date('Y年m月d日', $end_time);
        if (empty($result)) {
            $data = [
                'inspection_info' => $inspection_info,
                'data' => [],
                'page' => [
                    'current_page' => $page,
                    'row' => $pageSize,
                    'total' => 0,
                    'total_page' => 0
                ]
            ];
            return $this->jsonend(-1000, '暂无数据', $data);
        }
        foreach ($result as $k => $v) {
            $result[$k]['name'] = $v['workers_name'] . '(tel:' . $v['workers_phone'] . ')';
            $result[$k]['contract_number'] = $v['contract_number'] . ' ';
            $result[$k]['installed_time'] = $v['installed_time'] ? date('Y年m月d日', $v['installed_time']) : '--';
            $result[$k]['now_date'] = $v['now_date'] ? date('Y年m月d日', $v['now_date']) : '--';
            $result[$k]['add_time'] = $v['add_time'] ? date('Y年m月d日', $v['add_time']) : '--';
            $result[$k]['workers_entry_time'] = $v['workers_entry_time'] ? date('Y年m月d日', $v['workers_entry_time']) : '--';
            $result[$k]['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);
            $result[$k]['equipments_number'] = $v['equipments_number'] . ' ' ?? '';
            $result[$k]['inspection_total'] = $v['inspection_total'] . ' ' ?? 0;
            $result[$k]['inspection_results'] = $v['inspection_results'] . ' ' ?? 0;
            $result[$k]['inspection_standard'] = $v['inspection_standard'] . ' ' ?? 0;
            $result[$k]['user'] = $v['realname'] . '(联系电话:' . $v['telphone'] . ')';
            //组装导出明细数据
            $item['name'] = $v['workers_name'] . '(tel:' . $v['workers_phone'] . ')';
            $item['contract_number'] = $v['contract_number'] . ' ';
            $item['installed_time'] = $v['installed_time'] ? date('Y年m月d日', $v['installed_time']) : '--';
            $item['now_date'] = $v['now_date'] ? date('Y年m月d日', $v['now_date']) : '--';
            $item['add_time'] = $v['add_time'] ? date('Y年m月d日', $v['add_time']) : '--';
            $item['workers_entry_time'] = $v['workers_entry_time'] ? date('Y年m月d日', $v['workers_entry_time']) : '--';
            $item['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);
            $item['equipments_number'] = $v['equipments_number'] . ' ' ?? '';
            $item['inspection_total'] = $v['inspection_total'] . ' ' ?? 0;
            $item['inspection_results'] = $v['inspection_results'] . ' ' ?? 0;
            $item['inspection_standard'] = $v['inspection_standard'] . ' ' ?? 0;
            $item['user'] = $v['realname'] . '(联系电话:' . $v['telphone'] . ')';
            $contract_data[] = $item;
            array_push($workers_id, $v['workers_id']);

        }

        if ($type == 1) {//业绩考核统计
            $this->Achievement->table = 'oa_workers';
            $wheres['workers_id'] = ['IN', $workers_id];
            $work_list = $this->Achievement->selectPageData($wheres, '*', [], -1, 1, ['inspection_total' => 'desc'], 'workers_id');
            foreach ($work_list as $k => $v) {
                $work_list[$k]['workers_entry_time'] = $v['workers_entry_time'] ? date('Y年m月d日', $v['workers_entry_time']) : '--';
                $work_list[$k]['add_time'] = $v['add_time'] ? date('Y年m月d日', $v['add_time']) : '--';
                $work_list[$k]['workers_sex'] = $v['workers_sex'] == 1 ? '男' : '女';
                $work_list[$k]['role'] = '经销商';
                $work_list[$k]['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);
                $work_list[$k]['inspection_total'] = $v['inspection_total'] . ' ' ?? 0;
                $work_list[$k]['inspection_results'] = $v['inspection_results'] . ' ' ?? 0;
                $work_list[$k]['inspection_standard'] = $v['inspection_standard'] . ' ' ?? 0;
                $work_list[$k]['workers_position'] = $v['position'];
                $work_list[$k]['check_out'] = $v['check_out'] == 1 ? '是' : '否';
                $work_list[$k]['workers_name'] = $v['workers_name'] . '(工号:' . $v['workers_number'] . ',联系电话:' . $v['workers_phone'] . ')';
            }

            if ($is_export == 1) {
                //导出统计
                $sheet_title = '经销商业绩考核统计表';
                $title = '经销商业绩考核统计表';
                $file_name = date('YmdHis') . '-经销商业绩考核统计表';
                $path = WWW_DIR . '/export_achievement/';
                file_exists($path) || mkdir($path, 0777);
                $file_path = $path;
                $suffix = 'xlsx';
                $output = false;
                $tableTile = ['角色', '员工', '区域', '升级（入职）时间','添加时间', '电话', '性别', '是否合格', '累计完成/台', '实际完成/台', '考核标准/台', '考核周期'];
                $field = ['role', 'workers_name', 'area', 'workers_entry_time','add_time', 'workers_phone', 'workers_sex', 'check_out', 'inspection_total', 'inspection_results', 'inspection_standard', 'inspection_date'];
                $xlx = $this->export($tableTile, $field, $sheet_title, $title, $work_list, $file_name, $file_path, $suffix, $output);
                $url = $this->config->get('callback_domain_name');
                if ($this->dev_mode == 1) {
                    $url = $this->config->get('debug_config.callback_domain_name');
                }
                $export_data = [
                    //'url' => $url . '/export_achievement/' . $xlx
                    'url' => 'export_achievement/' . $xlx
                ];
                return $this->jsonend(1000, 'ok', $export_data);
            } else {

                $this->Achievement->table = 'oa_achievement_workers';
                $count = $this->Achievement->selectData($where, 'rq_oa_achievement_workers.id', $joins, [], 'w.workers_id');
                $count = count($count);
                $work_list = arrPage($work_list, $page, $pageSize);
                $data = [
                    'inspection_info' => $inspection_info,
                    'data' => $work_list,
                    'page' => [
                        'current_page' => $page,
                        'row' => $pageSize,
                        'total' => $count,
                        'total_page' => ceil($count / $pageSize)
                    ]
                ];
                return $this->jsonend(1000, 'ok', $data);
            }


        } else if ($type == 2 || $type==-1) {//业绩考核明细
            if ($is_export == 1) {
                //组装导出明细数据
                $sheet_title = '经销商业绩考核明细表';
                $title = '经销商业绩考核明细表';
                $file_name = date('YmdHis') . '-经销商业绩考核明细表';
                $path = WWW_DIR . '/export_achievement/';
                file_exists($path) || mkdir($path, 0777);
                $file_path = $path;
                $suffix = 'xlsx';
                $output = false;
                $tableTile = ['合同编号','客户', '装机时间', '签约时间','经销商姓名', '升级（入职）时间','添加时间', '设备台数/台', '实际完成/台', '考核标准/台', '考核周期'];
                $field = ['contract_number','user', 'installed_time','now_date', 'name', 'workers_entry_time', 'add_time','equipments_number', 'inspection_results', 'inspection_standard', 'inspection_date'];
                $xlx = $this->export($tableTile, $field, $sheet_title, $title, $contract_data, $file_name, $file_path, $suffix, $output);
                $url = $this->config->get('callback_domain_name');
                if ($this->dev_mode == 1) {
                    $url = $this->config->get('debug_config.callback_domain_name');
                }
                $export_data = [
                    //'url' => $url . '/export_achievement/' . $xlx
                    'url' => 'export_achievement/' . $xlx
                ];
                return $this->jsonend(1000, 'ok', $export_data);
            } else {
                $this->Achievement->table = 'oa_achievement_workers';
                $count = $this->Achievement->selectData($where, 'rq_oa_achievement_workers.id', $joins, [], 'h.contract_number,w.workers_id');
                $count = count($count);
                $result = arrPage($result, $page, $pageSize);
                $inspection_info['inspection_results'] = $count;
                $data = [
                    'inspection_info' => $inspection_info,
                    'data' => $result,
                    'page' => [
                        'current_page' => $page,
                        'row' => $pageSize,
                        'total' => $count,
                        'total_page' => ceil($count / $pageSize)
                    ]
                ];
                return $this->jsonend(1000, 'ok', $data);
            }
        }


    }
    /*
        * 导出历史考核数据
        */
    public function http_examineOldAchievement()
    {
        //接收参数
        $type = $this->parm['type'] ?? -1;//1导出统计 2导出明细
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $is_export = $this->parm['is_export'] ?? 0;//是否是导出
        if ($is_export == 1) {
            $pageSize = -1;
        }
        $this->Achievement->table = 'oa_workers_examine';
        $join = [
            ['oa_workers', 'rq_oa_workers.workers_id=rq_oa_workers_examine.workers_id', 'left'],
            ['customer', 'rq_customer.user_id=rq_oa_workers.user_id', 'left'],
        ];
        $where = [];
        if(!empty($this->parm['is_standard'])){
            $where['is_standard'] = $this->parm['is_standard'];
        }
        $where['rq_customer.is_dealer'] = 1;
        $lists = $this->Achievement->selectData($where,'rq_oa_workers_examine.*,rq_oa_workers.workers_name,rq_oa_workers.workers_phone,rq_oa_workers.workers_entry_time,workers_number', $join);
        if (empty($lists)) {
            return $this->jsonend(-1000, '暂无数据');
        }
        foreach ($lists as $k => $v) {
            $lists[$k]['workers_entry_time'] = $v['workers_entry_time'] ? date('Y年m月d日', $v['workers_entry_time']) : '--';
            $lists[$k]['create_time'] = $v['create_time'] ? date('Y年m月d日', $v['create_time']) : '--';
            $lists[$k]['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);
            $lists[$k]['inspection_standard'] = $v['inspection_standard'] . ' ' ?? 0;
            $lists[$k]['complete_num'] = $v['complete_num'].' ';
            $lists[$k]['is_standard'] = $v['is_standard'] == 1 ? '是' : '否';
            $lists[$k]['workers_name'] = $v['workers_name'] . '(工号:' . $v['workers_number'] . ',联系电话:' . $v['workers_phone'] . ')';
        }
        if ($is_export == 1) {
            //导出统计
            $sheet_title = '经销商业绩考核统计表';
            $title = '经销商业绩考核统计表';
            $file_name = date('YmdHis') . '-经销商业绩考核统计表';
            $path = WWW_DIR . '/export_achievement/';
            file_exists($path) || mkdir($path, 0777);
            $file_path = $path;
            $suffix = 'xlsx';
            $output = false;
            $tableTile = [ '员工', '升级（入职）时间','考核时间',  '是否合格',  '实际完成/台', '考核标准/台', '考核周期'];
            $field = [ 'workers_name', 'workers_entry_time','create_time',  'is_standard',  'complete_num', 'inspection_standard', 'inspection_date'];
            $xlx = $this->export($tableTile, $field, $sheet_title, $title, $lists, $file_name, $file_path, $suffix, $output);
            $url = $this->config->get('callback_domain_name');
            if ($this->dev_mode == 1) {
                $url = $this->config->get('debug_config.callback_domain_name');
            }
            $export_data = [
                //'url' => $url . '/export_achievement/' . $xlx
                'url' => 'export_achievement/' . $xlx
            ];
            return $this->jsonend(1000, 'ok', $export_data);
        } else {
            $this->Achievement->table = 'oa_workers_examine';
            $count = $this->Achievement->selectData($where, 'oa_workers_examine.id', $join, []);
            $count = count($count);
            $work_list = arrPage($lists, $page, $pageSize);
            $data = [
                'inspection_info' => [],
                'data' => $work_list,
                'page' => [
                    'current_page' => $page,
                    'row' => $pageSize,
                    'total' => $count,
                    'total_page' => ceil($count / $pageSize)
                ]
            ];
            return $this->jsonend(1000, 'ok', $data);
        }

    }

    /**
     * 更新经销商考核业绩
     */
    public function http_sysnAchievementChecking(){
        $this->Achievement->table = 'oa_workers';
        $workers = $this->Achievement->selectData(['position'=>3], 'workers_id,workers_name,workers_phone');
        $joins = [
            ['oa_achievement h', 'h.achievement_id=rq_oa_achievement_workers.achievement_id', 'left'],
            ['contract', 'rq_contract.contract_no=h.contract_number', 'left'],
        ];

        $arr = [];
        foreach ($workers as $k=>$v){
            $this->Achievement->table = 'oa_achievement_workers';
            $where = [];
            $where['workers_id'] = $v['workers_id'];
            $where['workers_position'] = 3;
            $where['achievement_work_order_type'] = 3;
            $where['now_date'] = ['between', [1661961600, 1696175999]];
            $list = $this->Achievement->selectData($where, 'rq_oa_achievement_workers.id,contract_no,now_date',$joins);
            $count = count($list);
            //  dump($v['workers_id'].'==='.$count);
            $item = [];
            $item['workers_id'] = $v['workers_id'];
            $item['workers_phone'] = $v['workers_phone'];
            $item['workers_name'] = $v['workers_name'];
            $item['count'] = $count;
            $item['list'] = $list;
            //    $this->Achievement->table = 'oa_workers_examine';
            //    $this->Achievement->updateData(['workers_id'=>$v['workers_id']],['complete_num'=>$count]);
            $this->Achievement->table = 'oa_workers';
            $this->Achievement->updateData(['workers_id'=>$v['workers_id']],['inspection_results'=>$count]);
            array_push($arr,$item);
        }
        return $this->jsonend(1000, 'ok', $arr);
        // $this->Achievement->table = 'equipments_parts';
        // $join = [
        //     ['equipment_lists el','el.equipment_id=rq_equipments_parts.equipment_id','left']
        // ];
        // $data = $this->Achievement->selectData(['is_filter'=>1],'rq_equipments_parts.id,el.device_no,rq_equipments_parts.equipment_id,parts_id,expire_time,is_filter',$join);
        // $arr = array_group_by($data,'equipment_id');
        // $return = [];
        // foreach ($arr as $k=>$v){
        //     if(!empty($v[1]) && !empty($v[2]) && abs($v[1]['expire_time']-$v[2]['expire_time'])>1000){
        //         array_push($return,$v[0]['device_no']);
        //        $this->Achievement->updateData(['id'=>$v[2]['id']],['expire_time'=>$v[1]['expire_time']]);
        //     }
        // }
        // return $this->jsonend(1000, 'ok', $return);


    }
    /**
     * 新装未结算合同
     */
    public function http_newInstallNoSettleContract(){
        $where['rq_work_order_return_visit.visit_time'] = ['between', [strtotime($this->parm['start']), strtotime($this->parm['end']) + (60 * 60 * 24 - 1)]]; //终止到当天23:59:59
        $where['wb.business_id'] = 3;
        $join = [
            ['work_order_business wb','wb.work_order_id=rq_work_order_return_visit.work_order_id','left'],
            ['work_order w','w.work_order_id=rq_work_order_return_visit.work_order_id','left'],
            ['contract c','c.contract_id=w.contract_id','left'],
        ];
        $data = $this->WorkOrderReturnVisitModel->getAll($where,1,-1,$join,'c.contract_no,visit_time',['visit_time'=>'ASC'],'work_order_return_visit');
        $result = [];
        if(!empty($data)){
            foreach($data as $key=>$val){
                $info = $this->OaAchievementModel->getOne(['contract_number'=>$val['contract_no'],'achievement_work_order_type'=>3],'achievement_id');
                if(empty($info)){
                    array_push($result,['no'=>$val['contract_no'],'visit_time'=>$val['visit_time']]);
                }
            }
        }
        return $this->jsonend(1000, 'ok', $result);
    }
    public function http_staticsAcivement(){
        $this->Achievement->table = 'oa_achievement';
        $where = [];
        $where['add_time'] = ['between', [1688140800, 1690819200]];
        $list = $this->Achievement->selectData($where, 'achievement_id,equipments_number,contract_number');
        $total_money = 0;
        $total_num = 0;
        $total_d = 0;
        $total_c = 0;
        $total_e = 0;
        $error = [];
        foreach ($list as $key=>$val){
            $this->Achievement->table = 'oa_achievement_workers';
            $d_moneys = $this->Achievement->selectData(['achievement_id'=>$val['achievement_id']], 'money');
            $d_money = array_sum(array_column($d_moneys,'money'));
            $this->Achievement->table = 'oa_engineers_performance';
            $e_moneys = $this->Achievement->selectData(['achievement_id'=>$val['achievement_id']], 'money');
            $e_money = 0;
            if(!empty($e_moneys)){
                $e_money = array_sum(array_column($e_moneys,'money'));
            }
            $this->Achievement->table = 'coupon';
            $coupons = $this->Achievement->countData(['from_achievement_id'=>$val['achievement_id']]);
            $c_money = 0;
            if(!empty($coupons)){
                $c_money = $coupons*50;
            }
            $sum = $d_money+$e_money+$c_money;
            $total_money +=$sum;
            $total_d +=$d_money;
            $total_e +=$e_money;
            $total_c +=$c_money;
            $total_num +=$val['equipments_number'];
            $equipments_money = $val['equipments_number']*150;
            // echo '['.($key+1).']'.$val['contract_number'].'-'.$sum.'-'.$equipments_money."优惠券:".$c_money.",工程:".$e_money.",经销商:".$d_money.PHP_EOL;
            if($sum!=$equipments_money){
                echo $val['contract_number'].'错误'.PHP_EOL;
                array_push($error,$val['contract_number'].':'.$sum.'>>>'.$equipments_money);
            }
        }
        $return['error'] = $error;
        $return['total_money'] = $total_money;
        $return['total_num'] = $total_num;
        $return['total_d'] = $total_d;
        $return['total_e'] = $total_e;
        $return['total_c'] = $total_c;
        return $this->jsonend(1000, 'ok', $return);

    }


    // 自动结算 查询
    public function http_newEquipmentAutoSettlement(){
        dump('进入结算查询');
        // 结算失败的合同
        //$contractStr = $this->redis->get('achievement_error_contract_no');
        //$notSettlementContractArr = json_decode($contractStr, true);
        // 不结算的合同
        $notSettlementContractArr[] = '2023110113330448100555';
        $notSettlementContractArr[] = '2023112717131256494957';
        $notSettlementContractArr[] = '2023092009565555575149';
        $notSettlementContractArr = substr_replace(json_encode($notSettlementContractArr), '(', 0, 1);
        $notSettlementContractArr = substr_replace($notSettlementContractArr, ')', -1, 1);
        // 现金支付 对公转账 不结算 "(3,4)"
        $notPayWayArr = "(3,4)";
        // 结算业务类型  新装 报停重装 "(3,9)"
        $businessIds = "(3,9)";

        // 7天前时间戳
        $selectTime = time() - 604800;
        /**
         * 结算条件
         * 工单已完成 11
         * 支付方式 现金 对公转账 不结算
         * 业务类型 限制 为 新装 报停重装
         * 指定合同不结算
         * 产品线为 家用机
         * 特殊合同不结算 is_special 是否特殊合同 1 是  2 否
         * 合同状态为生效中 status  4
         * 没有业绩表 记录  oa_achievement 表数据为空
         * complete_time 查询7天前的数据
         */
        $where = [
            'a.work_order_status'=>11,
            'a.pay_way'=>['not in',$notPayWayArr],
            'b.business_id'=>['in',$businessIds],
            'c.contract_no'=>['not in',$notSettlementContractArr],
            'c.contract_equipments_line'=>1,
            'c.is_special'=>2,
            'c.status'=>4,
            'd.achievement_id'=>null,
            'a.complete_time'=>['<',time()-604800]
        ];
        $join = [
            ['work_order_business b','a.work_order_id = b.work_order_id','LEFT'],
            ['contract c','c.contract_id = a.contract_id','LEFT'],
            ['oa_achievement d','d.contract_number = c.contract_no','LEFT'],
        ];
        $filed = 'a.work_order_id,a.contract_id,d.achievement_id,c.contract_no';
        $order = [
            'a.complete_time'=>'desc'
        ];
        $sql = '
                SELECT a.work_order_id, a.contract_id, d.achievement_id, c.contract_no
                FROM rq_work_order a
                LEFT JOIN rq_work_order_business b ON a.work_order_id = b.work_order_id
                LEFT JOIN rq_contract c ON c.contract_id = a.contract_id
                LEFT JOIN rq_oa_achievement d ON d.contract_number = c.contract_no
                WHERE a.work_order_status = 11 AND 
                      a.pay_way not in  '. $notPayWayArr .'  AND 
                      b.business_id in  '. $businessIds .' AND 
                      c.contract_no not in  '. $notSettlementContractArr .' AND 
                      c.contract_equipments_line = 1 AND 
                      c.is_special = 2 AND 
                      c.status = 4 AND 
                      d.achievement_id IS NULL AND 
                      d.achievement_work_order_type = 3  AND 
                      a.complete_time < ' . $selectTime . ' ORDER BY a.complete_time DESC
                LIMIT 0, 1;
            ';
        $mysql = get_instance()->config['mysql'];
        $prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $this->Achievement->table = $prefix . 'work_order';
        $getOne = $this->Achievement->getOneBySql($sql);
        return $this->jsonend(1000, 'ok', $getOne);
    }


    /**
     * 新增撤销、重新结算记录
     * @param int $type 操作类型 1 重新结算 2 撤销结算
     * @param array $achievement 旧业绩记录数据
     * @param array $user 员工信息
     * @param int $user_type 员工类型 1 经销商 2 工程人员
    */
    public function cancelAchievementLog(int $type,array $achievement,array $user,int $user_type,$money,$admin_info)
    {
        $new_money =  $user['temp_money'] - $money;
        if($user_type == 1){
            $name_key = 'workers_name';
            $id_key = 'workers_id';
            $user_desc = '经销商';
        }else{
            $name_key = 'engineers_name';
            $id_key = 'engineers_id';
            $user_desc = '工程人员';
        }
        $title_desc = $type == 1 ? '重新结算' : '撤销结算';
        $remark = '['.$title_desc.']减少'.$user_desc.'账户:['.$user[$name_key].']['.$user[$id_key].'] 金额['.$money.']['.$user['temp_money'].'修改为:'.$new_money.']';
        // 增加撤销结算记录
        $add = [
            'achievement_id'=>$achievement['achievement_id'],
            'contract_no'=>$achievement['contract_number'],
            'uid'=>$this->getUserIdByContract($achievement['contract_number']),
            'log_type'=>$type,
            'contract_type'=>$achievement['achievement_work_order_type']  == 3 ? 1 : 2,
            'remark'=>$remark,
            'user_type'=>$user_type,
            'user_id'=>$user_type == 1 ? $user['workers_id'] : $user['engineers_id'],
            'money'=>$money,
            'balance_new'=>$new_money,
            'balance_old'=>$user['temp_money'],
            'add_time'=>time(),
            'settlement_time'=>$achievement['add_time'],
            'do_user'=>$admin_info
        ];
        $this->Achievement->table = 'achievement_cancel_log';
        $this->Achievement->insertData($add);
    }

    // 根据合同查询用户的名称和电话
    public function getUserIdByContract(string $no)
    {
        $this->Achievement->table = 'contract';
        $contract = $this->Achievement->findData(['contract_no' => $no], 'user_id');
        return $contract['user_id'];
    }

}