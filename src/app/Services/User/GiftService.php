<?php

namespace app\Services\User;

use app\Services\Common\AdaPayService;
use app\Services\Common\CommonService;
use app\Services\Common\ConfigService;
use app\Services\Common\WxPayService;
use app\Services\Company\CompanyService;

class GiftService
{
    // 礼品列表
    public function gift_list(array $param)
    {
        $page = $param['page'] ?? 1;
        $limit = $param['limit'] ?? 10;
        $giftModel = get_instance()->loader->model('GiftModel', get_instance());
        $where = [
            'is_delete'=>0,
            'status'=>1
        ];
        $order = ['sale'=>'desc'];
        $filed = 'id,title,cover,price,discount,weight,unit,tags,sale';
        return $giftModel->selectPageData($where,$filed,[],$limit,$page,$order);
    }

    // 礼品详情页
    public function gift_details(array $param): array
    {
        $id = $param['id'] ?? 0;
        if(!$id){
            return [false,'确实必要参数'];
        }
        $giftModel = get_instance()->loader->model('GiftModel', get_instance());
        $gift = $giftModel->getOne(['id'=>$id]);
        if(empty($gift)){
            return [false,'数据不存在'];
        }
        if($gift['is_delete'] == 1){
            return [false,'数据异常:礼品已删除'];
        }
        if($gift['status'] != 1){
            return [false,'数据异常:礼品未上架'];
        }
        $gift['cover'] = UploadImgPath($gift['cover']);
        $image = json_decode($gift['images'],true);
        foreach ($image as $k=>$v){
            $image[$k] = UploadImgPath($v);
        }
        $gift['iamges'] = $image;
        return [true,$gift];
    }

    // 结算页
    public function settlement(array $param,int $user_id): array
    {
        $id = $param['id'] ?? 0;
        if(!$id){
            return [false,'确实必要参数'];
        }
        $giftModel = get_instance()->loader->model('GiftModel', get_instance());
        $addressModel = get_instance()->loader->model('CustomerAddress', get_instance());
        $couponModel = get_instance()->loader->model('CouponModel', get_instance());
        $filed = 'id,title,cover,price,discount,weight,unit,tags';
        $gift = $giftModel->getOne(['id'=>$id],$filed);
        if(empty($gift)){
            return [false,'数据异常:礼品不存在'];
        }
        if($gift['status'] != 1){
            return [false,'状态异常'];
        }
        if($gift['is_delete'] == 1){
            return [false,'数据异常:已删除'];
        }
        $gift['cover'] = UploadImgPath($gift['cover']);
        $result['gift'] = $gift;
        // 用户默认地址
        $address = $addressModel->getOne(['user_id'=>$user_id,'is_default'=>2]);
        $result['address'] = $address;
        $result['amount'] = $gift['price'];
        // 用户优惠券列表
        $where_coupon = [
            'user_id'=>$user_id,
            'status'=>1,
            'valid_time_end'=>['>',time()]
        ];
        $join = [
            ['coupon_cate c','c.id = coupon.cid','left']
        ];
        $filed = 'coupon.*,c.money';
        $order = ['send_time'=>'desc'];
        $coupon = $couponModel->selectData($where_coupon,$filed,$join,$order);
        if(!empty($coupon)){
            foreach ($coupon as $k=>$v){
                $coupon[$k]['money'] = bcdiv($v['money'],100,2);
            }
        }
        $result['coupon'] = $coupon;
        return [true,$result];
    }

    // 计算选择优惠券后的金额
    public function discountMoney(array $param): array
    {
        $id = $param['id'] ?? 0;
        $num = $param['num'] ?? 1; // 兑换/购买 数量
        $coupon_ids = $param['coupon_ids'] ?? [];
        if(!$id){
            return [false,'缺少参数:id'];
        }
        if(count($coupon_ids) > $num){
            return  [false,'优惠券数量不能大于兑换数量'];
        }
        $giftModel = get_instance()->loader->model('GiftModel', get_instance());
        $gift = $giftModel->getOne(['id'=>$id],'*');
        if(empty($gift)){
            return [false,'数据不存在'];
        }
        $total_money = bcmul($gift['price'],$num,2);
        if(!empty($coupon_ids)){
            $couponModel = get_instance()->loader->model('CouponModel', get_instance());
            $where_coupon = ['coupon_id'=>['in',$coupon_ids],];
            $join = [
                ['coupon_cate c','c.id = coupon.cid','left']
            ];
            $filed = 'coupon.*,c.money';
            $order = ['send_time'=>'desc'];
            $coupon = $couponModel->selectData($where_coupon,$filed,$join,$order);
            if(!empty($coupon)){
                $total_money = 0;
                foreach ($coupon as $k=>$v){
                    //$c_money = bcdiv($v['money'],100,2);
                    $c_money = $v['money'] / 100;
                    if($c_money == $gift['discount'] && $c_money == 48){
                        continue;
                    }
                    if($c_money == 39){
                        $total_money = $total_money + 9;
                    }
                }
            }
        }
        $result = [
            'total_money'=>$total_money,
            'num'=>$num
        ];
        return [true,$result];
    }



    // 下单
    public function submit(array $param,int $user_id): array
    {
        $id = $param['id'] ?? 0;
        $coupon_ids = $param['coupon_ids'] ?? 0;
        $post_money = $param['total_money'] ?? 0;
        $address_id = $param['address_id'] ?? 0;
        $num = $param['num'] ?? 1;
        $remarks = $param['remarks'];
        if(!$id){
            return [false,'缺少必要参数'];
        }
        if(!$address_id){
            return [false,'请选择收货地址'];
        }
        $giftModel = get_instance()->loader->model('GiftModel', get_instance());
        $gift = $giftModel->getOne(['id'=>$id],'*');
        if(empty($gift)){
            return [false,'礼品不存在'];
        }
        $couponModel = get_instance()->loader->model('CouponModel', get_instance());
        $where_coupon = ['coupon_id'=>['in',$coupon_ids],];
        $join = [
            ['coupon_cate c','c.id = coupon.cid','left']
        ];
        $filed = 'coupon.*,c.money';
        $order = ['send_time'=>'desc'];
        $coupon = $couponModel->selectData($where_coupon,$filed,$join,$order);
        $coupon_count = count($coupon);
        if($coupon_count > $num){
            return [false,'优惠券数量需小于兑换数量'];
        }
        $total_money = bcmul($gift['price'],$num,2);// 订单金额 不算优惠价格
        $total_amount = $total_money; // 实际支付金额
        $use_coupon_str = '';
        $message = [];
        // 用户使用优惠券情况下
        if(!empty($coupon)){
            $total_amount = 0; // 实际支付金额
            $sy_count = $num; // 剩余计算单位 剩余按原价计算
            $use_coupon = []; // 用于保存使用的优惠券数据
            foreach ($coupon as $k=>$v){
                $c_money = $v['money'] / 100; // 换算成元
                // 当前优惠券满足抵扣标准 不算钱
                if($c_money == $gift['discount'] && $c_money == 48){
                    $use_coupon[] = $v['coupon_id'];
                    $message[] = '使用优惠券:'.$v['code'].'兑换一次';
                    $sy_count--;
                    if($sy_count == 0){
                        break;
                    }
                    continue;
                }
                // 39面值的优惠券 补差价9块
                if($c_money == 39){
                    $use_coupon[] = $v['coupon_id'];
                    $message[] = '使用优惠券:'.$v['code'].'补差一次';
                    $total_amount = $total_amount + 9;
                    $sy_count--;
                    if($sy_count == 0){
                        break;
                    }
                }
            }
            if($sy_count > 0){
                $sy_total_money = bcmul($gift['price'],$sy_count,2);
                $total_amount = bcadd($sy_total_money,$total_amount,2);
            }
            $use_coupon_str = implode(',',$use_coupon);
        }
        // 实际支付金额
        if($total_amount != $post_money){
            return [false,'订单金额验证失败'];
        }
        $addressModel = get_instance()->loader->model('CustomerAddress', get_instance());
        $address = $addressModel->getOne(['address_id'=>$address_id],'*');
        if(empty($address)){
            return [false,'地址不存在'];
        }
        $desc = '待支付';
        // 订单类型
        if($total_amount==0){
            $order['type'] = 1;
            $order['pay_status'] = 2; // 订单金额 为 0  代表已支付
            $order['pay_time'] = time();
            $desc = '已支付';
        }else if($total_amount > 0 && $total_amount < $gift['price']){
            $order['type'] = 2;
        }else{
            $order['type'] = 3;
        }
        // 订单数据
        $order['user_id'] = $user_id;
        $order['sn'] = CommonService::createSn('cloud_contract_no','GI');
        $order['money'] = $total_money;
        $order['amount'] = $total_amount;
        $order['gift_id'] = $id;
        $order['num'] = $num;
        $order['status_refund'] = 1;
        $order['status'] = 1;
        $order['pay_type'] = 1;
        $order['province'] = $address['province'];
        $order['city'] = $address['city'];
        $order['area'] = $address['area'];
        $order['province_code'] = $address['province_code'];
        $order['city_code'] = $address['city_code'];
        $order['area_code'] = $address['area_code'];
        $order['address'] = $address['address'];
        $order['remarks'] = $remarks;
        $order['desc'] = implode(',',$message);
        $order['use_coupon'] = $use_coupon_str;
        $order['add_time'] = time();
        $order['updated_time'] = time();
        $orderModel = get_instance()->loader->model('GiftOrderModel', get_instance());
        $order_id = $orderModel->add($order);
        if(!$order_id){
            return [false,'创建订单失败'];
        }
        $result = [
            'status'=>$order['pay_status'],
            'desc'=>$desc,
            'order_id'=>$order_id
        ];
        return [true,$result];
    }

    // 支付
    public function payment(array $param,int $user_id): array
    {
        $date = date('d', time());
        if (get_instance()->config['system_clearing_swtich'] && get_instance()->config['system_clearing_date'] >= $date) {
            return [false,get_instance()->config['system_clearing_text']];
        }
        $order_id = $param['order_id'] ?? 0;
        if(!$order_id){
            return [false,'请选择订单'];
        }
        $redis = get_instance()->loader->redis("redisPool", get_instance());
        $redis_lock = $redis->get('user_order_pay' . $order_id);
        if ($redis_lock) {
            return [false,'请勿频繁操作,10s后重试'];
        }
        $redis->set('user_order_pay' . $order_id, $order_id, 10);
        //查询订单
        $orderModel = get_instance()->loader->model('GiftOrderModel', get_instance());
        $order = $orderModel->findData(['id'=>$order_id]);
        if (empty($order)) {
            return [false,'订单不存在'];
        }
        if($order['pay_status'] == 2){
            return [false,'订单已支付,请勿重新支付'];
        }
        // 用户信息
        $customerModel = get_instance()->loader->model('CustomerModel', get_instance());
        $user = $customerModel->getOne(['user_id'=>$user_id],'openid,ali_uid,is_dealer');
        if(empty($user)){
            return [false,'用户异常'];
        }
        if(empty($user['openid'])){
            return [false,'缺少openid'];
        }
        $pay_way = $param['pay_way'] ?? 1;
        $total_amount = $order['amount']; // 支付金额
        if($pay_way == 2){
            // 余额支付
            if($user['is_dealer'] == 1){
                $oaWorkersModel = get_instance()->loader->model('OaWorkersModel', get_instance());
                $workers = $oaWorkersModel->getOne(['user_id'=>$user['user_id']],'workers_id,user_id,total_money,cash_available');
                if(empty($workers)){
                    return [false,'找不到经销商'];
                }
                if($workers['cash_available'] < $order['amount']){
                    return [false,'余额不足'];
                }
                // 余额支付
                $order_update = [
                    'status'=>1,
                    'pay_status'=>2,
                    'updated_time'=>time(),
                    'pay_time'=>time()
                ];
                $orderModel->updateData(['id'=>$order_id],$order_update);
                // 剩余余额
                $balanceMoney = $workers['cash_available'] - $total_amount;
                // 变更余额
                $oaWorkersModel->save(['workers_id'=>$workers['workers_id']],['cash_available'=>$balanceMoney]);
                // 写入资金变动记录
                $funds_record = [
                    'user_id' => $workers['workers_id'],
                    'user_type' => 1,
                    'source' => 9, // 余额支付
                    'account_type' => 1,
                    'order_id' => $order['id'],
                    'money' => $order['amount'],
                    'note' => '订单单号:' . $order['sn'] . ',礼品兑换余额支付:' . $user['username'] . '(联系电话:' . $user['telphone'] . ')',
                    'add_time' => time(),
                ];
                $fundRecordModel = get_instance()->loader->model('FundsRecordModel', get_instance());
                $fundRecordModel->add($funds_record);

                // 新增资金记录
                $finance_record['type'] = 1;
                $finance_record['order_id'] = $order['id'];
                $finance_record['user_id'] = $order['user_id'];
                $finance_record['money'] = formatMoney($order['money']);
                $finance_record['total_money'] = formatMoney($order['amount'], 1);
                $finance_record['coupon_money'] = formatMoney($order['coupon_money'], 1);
                $finance_record['deduction_account_balance'] = formatMoney($order['amount'], 1);
                $finance_record['create_time'] = time();
                $finance_record['is_online_pay'] = 1;
                $finance_record['pay_way'] = 1;
                $finance_record['callback_num'] = $order['call_back'];
                $finance_record['payment_method'] = 1;
                $finance_record['payment_uid'] = $user_id;
                $finance_record['company_id'] = $param['company_id'];

                $financeRecordModel = get_instance()->loader->model('FinanceRecordModel', get_instance());
                $financeRecordModel->add($finance_record);

            }else{
                return [false,'当前用户不支余额支付'];
            }
        }
        $add_status = false;
        $jsapi = '';
        //调起微信支付
        if ($pay_way == 1) {
            $pay_data['order_sn'] = $order['sn'];
            $pay_data['money'] = $order['amount'];
            $wx_pay_data['money'] = $pay_data['money'];
            $company_config = CompanyService::getCompanyConfig($param['company_id']);
            $wx_config = $company_config['wx_config'];
            $pay_config = $company_config['pay_config'];//支付配置
            $pay_data['app_id'] = $wx_config['adapay_app_id']; // 聚合支付ID
            //三方微信支付
            // 0-100分的随机减扣，避免产生大量相同金额的支付订单
            $deduction = $redis->get('user_order_deduction:' . $order['id']);
            if (!$deduction) {
                // 0-100分的随机减扣，避免产生大量相同金额的支付订单
                $deduction = mt_rand(1, 99);
                $redis->set('user_order_deduction:' . $order['id'], $deduction, 3600 * 24);
            }
            $pay_data['money'] = ($pay_data['money'] * 100 - $deduction) / 100.0;
            $notify_url = get_instance()->config['callback_domain_name']. '/User/Callback/giftPayOrder';
            //$notify_url = 'http://runquan.jiuque.cc/User/Callback/rentAdaPayOrder';
            $dev_mode = ConfigService::getConfig('develop_mode', false, $param['company_id']);
            if ($dev_mode == 1) {
                $pay_data['money'] = '0.02';
                $notify_url = get_instance()->config['debug_config.callback_domain_name'] . '/User/Callback/giftPayOrder';
            }

            $result = AdaPayService::pay($pay_data, $notify_url, $user);
            if ($result['code'] == -1000) {
                $pay_data['money'] = $wx_pay_data['money'];
                dump('调用微信支付');
                // 聚合支付失败时，调用微信支付
                $notify_url = get_instance()->config['callback_domain_name'] . '/User/Callback/giftPayOrder';
                if ($dev_mode == 1) {
                    $pay_data['money'] = 1;
                    $notify_url = get_instance()->config['debug_config.callback_domain_name'] . '/User/Callback/giftPayOrder';
                }
                $jsapi = WxPayService::pay($wx_config, $user['openid'], $pay_data, $notify_url, $pay_config['sub_mch_id'] ?? '', $pay_config['mode'] ?? 1);
            } else {
                dump('聚合支付');
                $jsapi = $result['data'];
            }
            $add_status = true;
        }
        if ($add_status) {
            $order_data['money'] = $total_amount;
            $order_data['order_id'] = $order_id;
            $order_data['jsapi'] = $jsapi;
            return [true,$order_data];
        }
        return [false,'操作失败'];
    }

    // 取消订单
    public function cancel(array $param)
    {
        $order_id = $param['order_id'] ?? 0;
        if(!$order_id){
            return [false,'缺少必要参数'];
        }
        $orderModel = get_instance()->loader->model('GiftOrderModel', get_instance());
        $order = $orderModel->findData(['id'=>$order_id]);
        if(empty($order)){
            return [false,'订单不存在'];
        }
        if($order['status'] == 4){
            return [false,'订单已取消,请勿重复操作'];
        }
        if($order['status'] == 5) {
            return [false,'订单已退款,取消失败'];
        }
        if($order['pay_status']==2){
            return [false,'取消失败,订单已支付'];
        }
        $update = [
            'status'=>4,
            'cancel_time'=>time(),
            'remarks'=>'客户主动取消'
        ];
        $res = $orderModel->updateData(['id'=>$order_id],$update);
        if(!$res){
            return [false,'取消失败,稍后重试'];
        }
        // TODO 优惠券回退

        return [true,'取消成功'];
    }

    // 申请退款
    public function refund(array $param,int $user_id)
    {
        $order_id = $param['order_id'];
        $reason = $param['reason'];
        if(!$order_id){
            return [false,'缺少必要参数'];
        }
        if(!$reason){
            return [false,'请填写退款原因'];
        }
        $orderModel = get_instance()->loader->model('GiftOrderModel', get_instance());
        $order = $orderModel->findData(['id'=>$order_id]);
        if(empty($order)){
            return [false,'订单不存在'];
        }
        if($order['pay_status'] == 1){
            return [false,'订单未支付'];
        }
        $refundModel = get_instance()->loader->model('GiftRefundModel', get_instance());
        $refund = [
            'user_id'=>$user_id,
            'gift_order_id'=>$order_id,
            'sn'=>CommonService::createSn('cloud_contract_no','RE'),
            'status'=>1,
            'refund_money'=>$order['amount'],
            'reason'=>$reason,
            'add_time'=>time(),
            'updated_time'=>time()
        ];
        $refundModel->insertData($refund);
        return [true,'申请已提交'];
    }

    // 兑换记录
    public function gift_order_list(int $status,int $user_id)
    {
        $orderModel = get_instance()->loader->model('GiftOrderModel', get_instance());
        $where['user_id'] = $user_id;
        if($status != -1){
            $where['status'] = $status;
        }
        $filed = 'id,user_id,cover,money,amount,title,tags,sale,status';
        $order = ['add_time'=>'desc'];
        $list = $orderModel->selectData($where,$filed,[],$order);
        foreach ($list as $k=>$v){
            $list[$k]['cover'] = UploadImgPath($v['cover']);
            switch ($v['status']){
                case 0:$status_desc = '待支付';break;
                case 1:$status_desc = '待发货';break;
                case 2:$status_desc = '已发货';break;
                case 3:$status_desc = '已完成';break;
                case 4:$status_desc = '已取消';break;
                case 5:$status_desc = '已退款';break;
                default:$status_desc = '未知';
            }
            $list[$k]['status_desc'] = $status_desc;
        }
        return $list;
    }

    // 兑换详情
    public function order_info(int $order_id): array
    {
        if(!$order_id){
            return [false,'缺少必要参数'];
        }
        $orderModel = get_instance()->loader->model('GiftOrderModel', get_instance());
        $order = $orderModel->findData(['id'=>$order_id]);
        if(empty($order)){
            return [false,'数据异常'];
        }
        $order['cover'] = UploadImgPath($order['cover']);
        switch ($order['status']){
            case 0:$status_desc = '待支付';break;
            case 1:$status_desc = '待发货';break;
            case 2:$status_desc = '已发货';break;
            case 3:$status_desc = '已完成';break;
            case 4:$status_desc = '已取消';break;
            case 5:$status_desc = '已退款';break;
            default:$status_desc = '未知';
        }
        $order['status_desc'] = $status_desc;
        if($order['status_refund'] == 2){
            $order['status_desc'] = '退款中';
        }
        $order['add_time'] = dateFormat($order['add_time']);
        $order['updated_time'] = dateFormat($order['updated_time']);
        if($order['send_time']){
            $order['send_time'] = dateFormat($order['send_time']);
        }
        if($order['complete_time']){
            $order['complete_time'] = dateFormat($order['complete_time']);
        }
        if($order['refund_time']){
            $order['refund_time'] = dateFormat($order['refund_time']);
        }
        if($order['pay_time']){
            $order['pay_time'] = dateFormat($order['pay_time']);
        }
        if($order['cancel_time']){
            $order['cancel_time'] = dateFormat($order['cancel_time']);
        }
        return [true,$order];
    }

}