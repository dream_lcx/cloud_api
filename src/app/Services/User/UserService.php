<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2019-04-23
 * Time: 上午 11:27
 */

namespace app\Services\User;

use Server\CoreBase\Controller;

/**
 * 用户逻辑
 * Class UserService
 * @package app\Services\User
 */
class UserService
{


    /**
     * 用户账户变动
     * @param $user_id 用户id
     * @param $money 变动金额,单位元
     * @param $type 变动类型 1:增加 2:减少
     * @param $account_type 变动账户类型 1:可提现账户 2:冻结账户
     * @param $source 变动类型 1:渠道业绩结算 2:渠道推广员业绩结算 3:分成给渠道推广员 4提现
     * @param $order_id 关联订单ID，若source=1,2,3时传工单ID
     *
     */
    public function changeAccount($user_id, $money, $source = 1, $order_id = '', $remarks = '', $type = 1, $account_type = 1, $add_time = '')
    {
        if (empty($user_id)) {
            return false;
        }
        if (empty($add_time)) {
            $add_time = time();
        }
        $customerAccountModel = get_instance()->loader->model('CustomerAccountModel', get_instance());
        $customerAccountBillModel = get_instance()->loader->model('CustomerAccountBillModel', get_instance());
        $money = formatMoney($money, 1);//将元转为分
        //判断是否创建了账户
        $account_info = $customerAccountModel->getOne(['user_id' => $user_id], 'id,total_money,available_money,frozen_money');
        if (!empty($account_info)) {
            $id = $account_info['id'];
            $total_money = $account_info['total_money'];
            $available_money = $account_info['available_money'];
            $frozen_money = $account_info['frozen_money'];
        } else {
            //创建账户
            $account_data['user_id'] = $user_id;
            $account_data['total_money'] = 0;
            $account_data['available_money'] = 0;
            $account_data['frozen_money'] = 0;
            $account_data['create_time'] = $add_time;
            $id = $customerAccountModel->add($account_data);

            $total_money = 0;
            $available_money = 0;
            $frozen_money = 0;
        }

        //更新账户余额
        if ($account_type == 1) {
            //可提现账户
            switch ($type) {
                case 1:
                    $edit_account_data['total_money'] = $total_money + $money;
                    $edit_account_data['available_money'] = $available_money + $money;
                    break;

                case 2:
                    $edit_account_data['available_money'] = $available_money - $money;
                    break;
            }
        } else if ($account_type == 2) {
            //冻结账户
            switch ($type) {
                case 1:
                    $edit_account_data['frozen_money'] = $frozen_money + $money;
                    break;

                case 2:
                    $edit_account_data['frozen_money'] = $frozen_money - $money;
                    break;
            }
        }
        $edit_account_result = $customerAccountModel->save(['id' => $id], $edit_account_data);
        //写入账户记录
        $bill_data['user_id'] = $user_id;
        $bill_data['money'] = $money;
        $bill_data['change_type'] = $type;
        $bill_data['account_type'] = $account_type;
        $bill_data['source'] = $source;
        //$bill_data['work_order_id'] = $order_id;
        $bill_data['remarks'] = $remarks;
        $bill_data['create_time'] = $add_time;
        $add_bill_result = $customerAccountBillModel->add($bill_data);
        return $edit_account_result && $add_bill_result;
    }

    /**
     * 用户与运营中心,城市合伙人绑定关系
     * @param $user_id 用户ID
     * @param $provice_code 省份code,如是根据地址划分则必传
     * @param $city_code 城市code,如是根据地址划分则必传
     * @param $area_code 区域code,如是根据地址划分则必传
     * @param $company_id 所属公司ID
     * @param $scene 场景,1:注册 2:下单
     * @param $pid 推荐人ID
     * @return bool
     */
    public function addUserCenter($user_id, $provice_code, $city_code, $area_code, $company_id, $scene = 2, $pid = '')
    {
        $customerAdministrativeCenterModel = get_instance()->loader->model('CustomerAdministrativeCenterModel',get_instance());
        if (in_array($company_id, get_instance()->config->get('logic_mode.huaxin'))) {
            //华信--根据推荐人所属区域划分,如果无推荐人或者推荐人无区域则划分到默认区域[用户注册时绑定]
            if ($scene != 1) {
                return true;
            }
        } else {
            //默认:下单时绑定
            if ($scene != 2) {
                return true;
            }
        }
        //默认根据安装地址划分区域[用户下单时绑定]
        $info = $this->getOwnArea($user_id, $pid, $provice_code, $city_code, $area_code, $company_id);
        $data['administrative_id'] = $info['a_id'];
        $data['operation_id'] = $info['operation'];
        $data['user_id'] = $user_id;
        $has = $customerAdministrativeCenterModel->getOne(array('user_id' => $user_id, 'administrative_id' => $data['administrative_id']), '*');
        if (!empty($has)) {
            return true;
        }
        $res = $customerAdministrativeCenterModel->add($data);
        if ($res) {
            return true;
        }
        return false;
    }

    /**
     * 获取用户所属区域
     * @param $user_id
     * @param $pid
     * @param $provice_code
     * @param $city_code
     * @param $area_code
     * @param int $company_id
     * @return bool|null
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     */
    public function getOwnArea($user_id, $pid = '', $provice_code = '', $city_code = '', $area_code = '', $company_id = 1)
    {
        $customerModel = get_instance()->loader->model('CustomerModel',  get_instance());
        $customerAdministrativeCenterModel = get_instance()->loader->model('CustomerAdministrativeCenterModel', get_instance());
        if (in_array($company_id, get_instance()->config->get('logic_mode.huaxin'))) {
            //华信:如果已有所属区域直接获取已有的,若无根据推荐人所属划分
            //如果已有直接获取并返回
            $user_administrative_center_info = $customerAdministrativeCenterModel->getOne(array('user_id' => $user_id), 'operation_id,administrative_id');
            if(!empty($user_administrative_center_info) && !empty($user_administrative_center_info['operation_id']) && !empty($user_administrative_center_info['administrative_id'])){
                $info['a_id'] = $user_administrative_center_info['administrative_id'];
                $info['operation'] = $user_administrative_center_info['operation_id'];
                return $info;
            }
            if (empty($pid)) {
                $user_info = $customerModel->getOne(['user_id' => $user_id], 'source_is_valid,source');
                if (!empty($user_info) && !empty($user_info['source'])) {
                    $pid = $user_info['source'];
                }
            }
            $info['a_id'] = '';//默认划分到运营中心
            $info['operation'] = '';//默认划分到城市合伙人
            if (!empty($pid)) {
                $source_info = $customerAdministrativeCenterModel->getOne(array('user_id' => $pid), 'operation_id,administrative_id');
                if (!empty($source_info)) {
                    $info['a_id'] = $source_info['administrative_id'];
                    $info['operation'] = $source_info['operation_id'];
                }
            }
        } else {
            //默认:根据地址划分
            $db = get_instance()->loader->mysql("mysqlPool",  get_instance());
            $mysql = get_instance()->config['mysql'];
            $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';
            $map['province_id'] = $provice_code;
            $map['city_id'] = $city_code;
            $map['area_id'] = $area_code;
            $sql = "select * from " . $this->prefix . "administrative_info where locate('," . $area_code . ",',area_id) AND locate('," . $city_code . ",',city_id) AND locate('," . $provice_code . ",',province_id) AND company_id=" . $company_id;
            $info = $db->query($sql)->row();
        }

        if (empty($info) || empty($info['a_id'])) {
            return false;
        }
        return $info;
    }


    /*
    * 获取用户的推荐人身份信息
    * @param 必选 $user_id int 用户ID
    * @param 必选 company_id int 公司ID
    * @return_param role int 角色3合伙人4经销商
    * @return_param uid int 角色对应id
    */
    public function getUserBelongTo($user_id,$company_id=1){
        $customerModel = get_instance()->loader->model('CustomerModel',  get_instance());
        $oaWorkersModel = get_instance()->loader->model('OaWorkersModel', get_instance());
        //查询用户推荐人信息
        $user_info = $customerModel->getOne(['user_id' => $user_id], 'source_is_valid,source');

        $data=[];
        if(!empty($user_info)){//有推荐人用户推荐人的身份是否是经销商

            $workers_info=$oaWorkersModel->getOne(['user_id'=>$user_info['source'],'is_delete'=>0],'workers_id');
            if(!empty($workers_info)){
                $data['role']=4;//经销商
                $data['uid']=$workers_info['workers_id'];
                return $data;
            }else{
                //如果推荐人不是经销商，则查看推荐人属于哪个合伙人下
                $info=$this->getOwnArea($user_info['source'],'','','','',$company_id);
                if(!empty($info)){
                    $data['role']=3;//合伙人
                    $data['uid']=$info['a_id'];
                    return $data;
                }

            }

        }

        return $data;

    }
}