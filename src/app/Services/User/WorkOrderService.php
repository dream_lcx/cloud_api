<?php

namespace app\Services\User;


use Server\CoreBase\SwooleException;

class WorkOrderService
{
    /**
     * @续费验证 工单状态 / 用户订单是否支付
     * @param int $contractId 合同id
     * @return array
     * @throws SwooleException
     */
    public function isPay(int $contractId): array
    {
        if(!$contractId){
            return [false,'合同ID不存在'];
        }
        // 加载工单模型
        $workOrderModel = get_instance()->loader->model('WorkOrderModel', get_instance());
        $workOrder = $workOrderModel->getOne(['contract_id'=>$contractId],'work_order_status,order_id,contract_id');
        if(empty($workOrder)){
            return [false,'工单不存在'];
        }

        // 11 工单已完成 待评价  12 已评价
//        if(!in_array($workOrder['work_order_status'],[11,12])){
//            return [false,'续费失败,你当前订单未支付'];
//        }
//        // 调用订单模型
//        $orderModel = get_instance()->loader->model('OrderModel', get_instance());
//        $order = $orderModel->getOne(['order_id'=>$workOrder['order_id']],'*');
//        if(empty($order)){
//            return [false,'订单不存在'];
//        }
//        if($order['pay_status'] == 1){
//            return [false,'续费失败,你当前合同订单暂未支付'];
//        }
        return [true,'success'];
    }

}