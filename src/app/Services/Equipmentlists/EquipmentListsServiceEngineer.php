<?php

namespace app\Services\Equipmentlists;

use app\Services\EngineerBaseService;
use app\Services\Common\HttpService;
use app\Services\Device\DeviceService;

class EquipmentListsServiceEngineer extends EngineerBaseService
{

//    // 设备替换
//    public function deviceReplace(array $data)
//    {
//        $deviceOld = $data['device_one'] ?? 0;
//        $deviceNew = $data['device_two'] ?? '';
//        $product = $data['product'] ?? '';
//        $packageId = $data['package'] ?? '';
//        $note = $data['note'] ?? '';
//        if(!$deviceOld)             return [false,'请选择需要替换的设备'];
//        if(!$deviceNew)             return [false,'请输入替换设备编号'];
//        if(!$product)               return [false,'请选择产品'];
//
//        // 判断旧设备
//        $publicModel = get_instance()->loader->model('AchievementModel', get_instance());
//        $publicModel->table = 'equipment_lists';
//        $oldEquipmentWhere['equipment_id'] = $deviceOld;
//        $oldEquipmentFiled = 'equipment_id,equipments_id,device_no,remaining_traffic,remaining_days,used_traffic,used_days';
//        $oldEquipment = $publicModel->findData($oldEquipmentWhere,$oldEquipmentFiled);
//        if(empty($oldEquipment)){
//            return [false,'需要替换的设备不存在'];
//        }
//
//        // 新设备产品
//        $publicModel->table = 'equipments';// 产品表
//        $productNewWhere['equipments_id'] = $product;
//        $productNewFiled = 'equipments_id,equipments_name,filter_element,equipments_type,type';
//        $productNew =  $publicModel->findData($productNewWhere,$productNewFiled);
//        if(empty($productNew)){
//            return [false,'产品不存在'];
//        }
//
//        // TODO 没选择套餐 默认使用旧设备的套餐
//        if(!$packageId){
//            // 查询旧设备合同的套餐
//            $publicModel->table = 'contract_package_record';
//            $contractPackageRecord =  $publicModel->findData(['equipment_id'=>$deviceOld],'contract_id');
//            if(empty($contractPackageRecord)){
//                return [false,'合同套餐下发记录不存在'];
//            }
//            $publicModel->table = 'contract';// 合同表
//            $contract = $publicModel->findData(['contract_id'=>$contractPackageRecord['contract_id']]);
//            if(empty($contract)){
//                return [false,'合同不存在'];
//            }
//            $packageId = $contract['package_id'];
//        }
//        // 判断套餐
//        $publicModel->table = 'equipments_package';// 产品套餐表
//        $packageWhere['id'] = $packageId;
//        $package = $publicModel->findData($packageWhere);
//        if(empty($package)){
//            return [false,'套餐不存在'];
//        }
//
//        // 非自能设备
//        if($productNew['type'] == 2){
//            $deviceNew = DeviceService::createNonEqNo();
//            $equipmentLibrary = [
//                'company_id'=>$this->company,
//                'model_id'=>1,
//                'device_no'=>$deviceNew,
//                'device_status'=>1,
//                'add_time'=>time()
//            ];
//            $publicModel->table = 'equipment_library';
//            $publicModel->insertData($equipmentLibrary);
//        }
//
//        // 判断新设备
//        $publicModel->table = 'equipment_library';// 设备库表
//        $equipmentLibraryNew = $publicModel->findData(['device_no'=>$deviceNew],'id,device_status,device_no');
//        if(empty($equipmentLibraryNew)){
//            return [false,'新设备编号[' . $deviceNew . ']不存在设备库中'];
//        }
//        if($equipmentLibraryNew['device_status'] == 3){
//            return [false,'新设备编号['.$deviceNew.']已注销'];
//        }
//
//        // 判断新设备 是否存在设备表中 不存在直接替换 存在判断是否占用 占用不可替换
//        $publicModel->table = 'equipment_lists';// 设备表
//        $equipmentListsNew =   $publicModel->findData(['device_no'=>$deviceNew],'equipment_id,device_no');
//        // 新设备 存在设备表中
//        if(!empty($equipmentListsNew)){
//            // 查询新设备 是否被用户绑定
//            $customerBindEquipmentWhere = [
//                'equipment_id'=>$equipmentListsNew['equipment_id'],
//                'is_owner'=>2,
//                'status'=>['<>',2]
//            ];
//            $publicModel->table = 'customer_bind_equipment';
//            $haveCustomerBindEquipment =  $publicModel->findData($customerBindEquipmentWhere);
//            if(!empty($haveCustomerBindEquipment)){
//                return [false,'新设备编号['.$deviceNew.']已被用户绑定'];
//            }
//        }
//
//        $updateDeviceList = [];   //修改设备表中的数据集
//        $deleteDeviceList = [];   //删除设备表中的数据集
//
//        // 查询旧设备库信息
//        $publicModel->table = 'equipment_library';
//        $oldEquipmentLibrary = $publicModel->findData(['device_no'=>$oldEquipment['device_no']],'id');
//
//        $oldEquipmentUpdate = [
//            'equipment_id'=>$oldEquipment['equipment_id'],
//            'device_no'=>$equipmentLibraryNew['device_no'] ?? $deviceNew,
//            'device_status'=>1,
//            'equipments_id'=>$product
//        ];
//        // 设备库需要修改的东西
//        // 修改旧设备库状态为注销和新设备库为已注册
//        $equipmentLibraryUpdate = [
//            [
//                'id'=>$oldEquipmentLibrary['id'],
//                'device_status'=>1,
//            ],
//            [
//                'id'=>$equipmentLibraryNew['id'],
//                'device_status'=>2,
//                'company_id'=>$this->company
//            ]
//
//        ];
//        // 新设备 存在 设备表中
//        if(!empty($equipmentListsNew)){
//            $delDeviceList = [
//                'equipment_id'=>$oldEquipment['equipment_id']
//            ];
//            $deleteDeviceList[] = $delDeviceList;
//        }
//
//
//        // 若旧设备已被占用（绑定），且产品替换,则需要重新签订合同
//        $contract_join = [
//            ['contract c', 'rq_contract_equipment.contract_id = c.contract_id', 'LEFT']
//        ];
//        $contract_where['rq_contract_equipment.equipment_id'] = $be_replaced_device['equipment_id'];
//        $contract_where['rq_contract_equipment.state'] = ['IN', [0, 1]];//正常和拆机的都改
//        $contract = $this->contract_ep_model->getAlls($contract_where, 'rq_contract_equipment.contract_id,c.status,c.user_id', $contract_join);
//
//        // 判断合同是否存在且不是待签
////        if ($be_replaced_device['equipments_id'] != $product && !empty($contract) && $contract['status'] != 3) {
////            $update_contract = [
////                'contract_id' => $contract['contract_id'],
////                'status' => 3
////            ];2020071548989798
////        }2020071599985257
//
//        // 获取产品
//        $old_product = $this->equipments_model->getOne(['equipments_id' => $be_replaced_device['equipments_id']], 'equipments_id,equipments_name');
//
//        //查询工程人员姓名
//        $engineers_info = $this->engineers_model->getOne(['engineers_id' => $this->user_id], 'engineers_name');
//        // 日志
//        $log = [
//            'equipment_id' => $be_replaced_device['equipment_id'],
//            'old_number' => $be_replaced_device['device_no'],
//            'new_number' => $replaced_device_lib['device_no'] ?? $device_two,
//            'old_product_id' => $old_product['equipments_id'] ?? $be_replaced_device['equipments_id'],
//            'old_product_name' => $old_product['equipments_name'] ?? '',
//            'new_product_id' => $new_product['equipments_id'] ?? $product,
//            'new_product_name' => $new_product['equipments_name'] ?? '',
//            'source' => 3,//工程端
//            'operator_id' => $this->user_id,
//            'operator' => $engineers_info['engineers_name'] ?? '',
//            'note' => $note,
//            'add_time' => time(),
//        ];
//
//        $add_status = false;
//        $this->db->begin(function () use (&$add_status, $update_device_list, $product, $be_replaced_device, $update_device_lib, $delete_device_list, $log, $device_two, $package, $replaced_device_lib, $new_product, $contract) {
//            // 数据库操作
//            if (isset($update_contract)) {
//                $this->contract_model->save(['contract_id' => $update_contract['contract_id']], $update_contract);
//            }
//            foreach ($update_device_list as $key => $value) {
//                if ($product != $be_replaced_device['equipments_id']) {
//                    $this->customerBindEquipmentModel->save(['equipment_id' => $value['equipment_id']], ['equipments_id' => $value['equipments_id']]);//修改用户绑定表产品ID
//                }
//                $this->equipmentlistsModel->save(['equipment_id' => $value['equipment_id']], $value);//修改设备表
//            }
//            if (!empty($contract)) {
//                foreach ($contract as $k => $v) {
//                    $this->contract_ep_model->save(['equipment_id' => $be_replaced_device['equipment_id'], 'contract_id' => $v['contract_id']], ['equipments_id' => $product]);//更新合同设备表
//                    //新增修改工单信息
//                    $this->work_order_model->save(['company_id' => $this->company, 'user_id' => $v['user_id'], 'contract_id' => $v['contract_id']], ['equipments_id' => $product]);
//                }
//
//            }
//            foreach ($update_device_lib as $key => $value) {
//                $this->eq_library_model->save(['id' => $value['id']], $value);
//            }
//
//            foreach ($delete_device_list as $key => $value) {
//                $this->equipmentlistsModel->delete(['equipment_id' => $value['equipment_id']], $value);
//            }
//            //判断记录是否存在
//            $replace_id = $this->device_replace_log_model->getOne(['equipment_id' => $be_replaced_device['equipment_id'], 'old_number' => $be_replaced_device['device_no'], 'new_number' => $replaced_device_lib['device_no'] ?? $device_two], 'id');
//            if (empty($replace_id)) {
//                $this->device_replace_log_model->add($log);
//            }
//
//            // 删除redis
//            if ($this->redis->get('device_heartbeat:' . $be_replaced_device['device_no']))
//                $this->redis->del('device_heartbeat:' . $be_replaced_device['device_no']);
//            if ($this->redis->get('work_state:' . $be_replaced_device['device_no']))
//                $this->redis->del('work_state:' . $be_replaced_device['device_no']);
//            if ($this->redis->get('device_heartbeat:' . $device_two))
//                $this->redis->del('device_heartbeat:' . $device_two);
//            if ($this->redis->get('work_state:' . $device_two))
//                $this->redis->del('work_state:' . $device_two);
//            // 下发套餐到设备，并写入下发记录，防止下发失败，记录将由API定时任务监控下发
//            $domain = $this->config->get('debug_config.static_resource_host'); //接口地址
//            // 获取套餐
//            if(!$package){
//                // TODO 2024-5-8没选择套装 就 查询旧设备的套餐
//                $equipmentListInfo = $this->equipmentlistsModel->getOne(['device_no' => $device_two], 'equipments_id');
//                $package = $equipmentListInfo['equipments_id'];
//            }
//            $package_info = $this->eq_rental_package_model->getOne(['package_id' => $package], 'mode');
//            // 获取滤芯最大值
//            if (isset($new_product['filter_element']) && !empty($new_product['filter_element'])) {
//                $filter_element_json = json_decode($new_product['filter_element'], 1);
//                $filter_element = [];
//                foreach ($filter_element_json as $key => $value) {
//                    array_push($filter_element, $value['cycle']);
//                }
//            } else {
//                $filter_element = [180, 180, 365, 365, 365];
//            }
//            // 设备应该下发剩余值
//            // 创建下发套餐记录
//            $package_log = [
//                'contract_id' => $contract['contract_id'] ?? 0,
//                'equipment_id' => $be_replaced_device['equipment_id'],
//                'working_mode' => $package_info['mode'] ?? 1,
//                'filter_element_max' => json_encode($filter_element),
//                'used_days' => $be_replaced_device['used_days'],
//                'remaining_days' => $be_replaced_device['remaining_days'],
//                'used_traffic' => $be_replaced_device['used_traffic'],
//                'remaining_traffic' => $be_replaced_device['remaining_traffic'],
//                'status' => 1,
//                'hand_time' => time(),
//            ];
//            $package_log_id = $this->contract_package_record_model->add($package_log);
//
//            // 将下发记录存到redis
//            $PlanToMonitor = $this->redis->get('PlanToMonitor');
//            $PlanToMonitor = json_decode($PlanToMonitor, true);
//            $package_log['id'] = $package_log_id;
//            array_push($PlanToMonitor, $package_log);
//            $this->redis->set('PlanToMonitor', json_encode($PlanToMonitor));
//
//            // 下发套餐
//            $request_data = [
//                'sn' => $device_two,
//                'working_mode' => $package_info['mode'] ?? 1,
//                'filter_element_max' => $filter_element,
//            ];
//            //$request_data = json_encode($request_data);
//            $request_result = HttpService::Thrash($request_data, $domain . 'House/Issue/bindingPackage');
//
//            if (json_decode($request_result, 1) === false) {
//                return $this->jsonend(-1000, "设备替换失败,失败原因：套餐下发失败");
//            }
//            dump($device_two);
//            dump($domain . 'House/Issue/heartbeat');
//            dump('-----------------');
//            sleep(3);
//            //获取心跳
//            //post_request($domain . 'House/Issue/heartbeat', json_encode(['sn' => $device_two]));
//            HttpService::Thrash(['sn' => $device_two], $domain . 'House/Issue/heartbeat');
//            sleep(1);
//            //获取版本号
//            //post_request($domain . 'House/Issue/version', json_encode(['sn' => $device_two]));
//            HttpService::Thrash(['sn' => $device_two], $domain . 'House/Issue/version');
//            $add_status = true;
//
//        }, function ($e) {
//            return $this->jsonend(-1000, '替换失败', $e->error);
//        });
//        if (!$add_status) {
//            return $this->jsonend(-1000, "操作失败,请重试");
//        }
//        return $this->jsonend(1000, "替换成功，请检查设备是否正常");
//    }
}