<?php

namespace app\Services\Engineer;

use Exception;

class WorkOrderService
{

    # 报停重装 更换产品
    public function changeProduct($data): array
    {
        $work_order_id = $data['work_order_id'] ?? 0;
        $product_id = $data['product_id'] ?? 0;
        $package_id = $data['package_id'] ?? 0;
        if(!$work_order_id)         return [false,'参数异常'];
        if(!$product_id)            return [false,'请选择产品'];
        $workOrderModel =  get_instance()->loader->model('WorkOrderModel', get_instance());
        $equipmentsModel = get_instance()->loader->model('EquipmentModel', get_instance());
        $equipmentRentalPackageModel = get_instance()->loader->model('EquipmentRentalPackageModel', get_instance());
        $contractModel = get_instance()->loader->model('ContractModel', get_instance());
        $ordersModel = get_instance()->loader->model('OrderModel', get_instance());
        # 查询工单
        $join = [
            ['work_order_business','rq_work_order_business.work_order_id = rq_work_order.work_order_id','LEFT'],
        ];
        $field = 'rq_work_order.*,rq_work_order_business.business_id as work_business_id';
        $workOrder = $workOrderModel->getOne(['rq_work_order.work_order_id'=>$work_order_id],$field,$join);
        if(empty($workOrder))                               return [false,'工单不存在'];
        if($workOrder['work_business_id'] != 9)                  return [false,'工单类型异常'];
        if($workOrder['equipments_id'] == $product_id)      return [false,'未更新产品'];

        # 查询产品
        $equipments = $equipmentsModel->getDetail(['equipments_id'=>$product_id],'*');
        if(empty($equipments))                  return [false,'产品不存在'];

        # 如果选择了套餐
        $package = [];
        if($package_id){
            $package = $equipmentRentalPackageModel->getOne(['package_id'=>$package_id]);
            if(empty($package))                 return [false,'套餐不存在'];
        }

        # 查询
        $contract = $contractModel->getOne(['contract_id'=>$workOrder['contract_id']]);
        if(empty($contract))                    return [false,'合同不存在'];

        # 没有选择套餐 用以前的套餐
        if(!$package_id)        $package_id = $contract['package_id'];

        # 查询当前套餐 是否支持当前产品
        $checkPackage = $equipmentsModel->getOne(['equipments_id'=>$product_id],'package_ids');
        $total_package_ids = explode(',',$checkPackage['package_ids']);

        $where = [
            'is_old'=>0,
            'equipments_id'=>$product_id
        ];
        $oldPackage = $equipmentRentalPackageModel->getAll2($where,'package_id',['created_time' => 'DESC'],1,-1,[]);
        $old_package_ids = array_column($oldPackage,'package_id');
        $total_package_ids = array_merge($total_package_ids,$old_package_ids);
        $total_package_ids = array_values(array_unique($total_package_ids));

        if(!in_array($package_id,$total_package_ids)){
            return [false,'当前套餐不支持此产品,请重新选择套餐'];
        }

        # 更新操作
        try {
            # 更新工单
            $workOrderModel->save(['work_order_id'=>$work_order_id],['equipments_id'=>$product_id]);
            # 选择套餐 更新套餐
            if(!empty($package)){
                # 更新合同
                $contract_data = [
                    'package_id'=>$package['package_id'],
                    'package_name'=>$package['package_name'],
                    'package_mode'=>$package['mode'],
                    'package_value'=>$package['num'],
                    'package_money'=>$package['total_money'],
                    'package_cycle'=>$package['cycle'],
                    'renew_money'=>$package['renew_money'],
                    'contract_money'=>$package['total_money']
                ];
                $contractModel->save(['contract_id'=>$contract['contract_id']],$contract_data);

                # 更新订单
                if(!empty($workOrder['order_id'])){
                    $order_data = [
                        'package_id'=>$package['package_id'],
                        'package_name'=>$package['package_name'],
                        'package_mode'=>$package['mode'],
                        'package_value'=>$package['num'],
                        'package_money'=>$package['total_money'],
                        'package_cycle'=>$package['cycle'],
                        'package_renew_money'=>$package['renew_money']
                    ];
                    $ordersModel->save(['order_id'=>$workOrder['order_id']],$order_data);
                }
            }

        }catch (Exception $exception){
            return [false,$exception->getMessage()];
        }
        return [true,'更换成功'];
    }

    # 拆机冻结 已报停 已买断 介绍人所产生优惠券划给 总账户 全部
    public function couponToAdmin($contract_id,$remark): bool
    {
        if(!$contract_id)    return false;
        $config = get_instance()->config;
        $accountId = $config->get('account')['total_account_number'] ?? 855182;
        $couponModel =  get_instance()->loader->model('CouponModel', get_instance());
        $where = [
            'from_contract_id'=>$contract_id
        ];
        $totalCoupon = $couponModel->getAll($where);
        # 没有优惠券 不操作
        if(empty($totalCoupon)){
            return false;
        }
        # 开始处理
        foreach ($totalCoupon as $key=>$value){
            $where = ['coupon_id'=>$value['coupon_id']];
            # 扩展信息
            $extend = [
                'contract_id'=>$contract_id,
                'do_time'=>date('Y-m-d H:i:s',time()),
                'old_uid'=>$value['uid'],
                'new_uid'=>$accountId,
                'remark'=>$remark.'-优惠券划拨给总账户'
            ];
            $extend = json_encode($extend,JSON_UNESCAPED_UNICODE);
            $map = ['uid'=>$accountId,'old_uid'=>$value['uid'],'extend'=>$extend];
            $couponModel->edit($where,$map);
        }
        return true;
    }

}
