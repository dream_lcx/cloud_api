<?php

namespace app\Services;

use app\Library\SLog;
use app\Services\Common\ConfigService;
use app\Services\Company\CompanyService;
use app\Services\Log\FileLogService;
use Server\CoreBase\Controller;
use Server\CoreBase\SwooleException;
use Server\CoreBase\SwooleRedirectException;
use Server\SwooleMarco;

class EngineerBaseService extends Controller
{

    protected $parm;
    protected $user_id;
    protected $work_log_model;
    protected $time_model;
    protected $log_model;
    protected $user_info;
    protected $contract_log_model;
    protected $dev_mode;
    protected $app_debug;
    protected $wx_config;
    protected $work_eq_model;
    protected $engineer_wx_name;
    protected $user_wx_name;
    protected $company;
    protected $contract_model;
    protected $machine_model;
    protected $pay_config;
    protected $user_model;
    protected $user_log_model;
    protected $contract_eq_model;


    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->user_id = $this->request->user_id;
        $this->company = $this->request->company;
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        $this->work_log_model = $this->loader->model('WorkOrderLogModel', $this);
        $this->time_model = $this->loader->model('WorkOrderTimeModel', $this);
        $this->log_model = $this->loader->model('OrderLogModel', $this);
        $this->engineers_model = $this->loader->model('EngineersModel', $this);
        $this->contract_log_model = $this->loader->model('ContractLogModel', $this);
        $this->work_eq_model = $this->loader->model('WorkEquipmentModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->machine_model = $this->loader->model('MachineModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->user_log_model = $this->loader->model('CustomerLogModel', $this);
        $this->contract_eq_model = $this->loader->model('ContractEquipmentModel', $this);
        //判断用户状态是否正常
        if (!empty($this->user_id)) {
            $userinfo = $this->engineers_model->getOne(array('engineers_id' => $this->user_id), 'openid,engineers_status,is_delete,role');
            if (!empty($userinfo) && ($userinfo['engineers_status'] == 1 || $userinfo['is_delete'] == 1)) {
                return $this->jsonend(-1006, "该账号已被冻结,更多信息请联系后台管理员");
            }
            $openid = $this->request->header['openid'] ?? '';
            if (empty($openid)) {
                return $this->jsonend(-1004, '未登录');
            }
            if ($userinfo['role'] != 3 && $openid != $userinfo['openid'] && $openid != 'oQbwX0WJaEmgkG0g5yEtFUMobehE' && $openid != 'oQbwX0TvKvZ11Yg4Zvg0cgK3ZJ3Y') {
                return $this->jsonend(-1006, "该账号已在其他端登录,若不是本人操作修改登录密码");
            }
        }

        //开发者模式
        $this->dev_mode = ConfigService::getConfig('develop_mode', false, $this->company);
        //调试模式
        $this->app_debug = ConfigService::getConfig('app_debug', false, $this->company);
        $company_config = CompanyService::getCompanyConfig($this->company);
        $this->wx_config = $company_config['engineer_wx_config'];
        $this->pay_config = $company_config['pay_config'];//支付配置
        $this->engineer_wx_name = $company_config['engineer_wx_config']['app_wx_name'] ?? '优净云';
        $this->user_wx_name = $company_config['wx_config']['app_wx_name'] ?? '优净云';
    }

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 异常的回调(如果需要继承$autoSendAndDestroy传flase)
     * @param \Throwable $e
     * @param callable $handle
     */
    public function onExceptionHandle(\Throwable $e, $handle = null)
    {
        $log = '--------------------------[报错信息]----------------------------' . PHP_EOL;
        $log .= 'client：Engineer' . PHP_EOL;
        $log .= 'DATE：' . date("Y-m-d H:i:s") . PHP_EOL;
        $log .= 'getMessage：' . $e->getMessage() . PHP_EOL;
        $log .= 'getCode：' . $e->getCode() . PHP_EOL;
        $log .= 'getFile：' . $e->getFile() . PHP_EOL;
        $log .= 'getLine：' . $e->getLine();
        FileLogService::WriteLog(SLog::ERROR_LOG, 'error', $log);

        //必须的代码
        if ($e instanceof SwooleRedirectException) {
            $this->http_output->setStatusHeader($e->getCode());
            $this->http_output->setHeader('Location', $e->getMessage());
            $this->http_output->end('end');
            return;
        }
        if ($e instanceof SwooleException) {
            FileLogService::WriteLog(SLog::ERROR_LOG, 'error', $e->getMessage() . "\n" . $e->getTraceAsString());
        }
        //可以重写的代码
        if ($handle == null) {
            switch ($this->request_type) {
                case SwooleMarco::HTTP_REQUEST:
                    $e->request = $this->request;
                    $data = get_instance()->getWhoops()->handleException($e);
                    $this->http_output->setStatusHeader(500);
                    $this->http_output->end($data);
                    break;
                case SwooleMarco::TCP_REQUEST:
                    $this->send($e->getMessage());
                    break;
            }
        } else {
            \co::call_user_func($handle, $e);
        }
    }

    /**
     * 公用输出
    */
    public function show($code = '',$message = '',$data = '',$gzip = true)
    {
        $result = [
            'code'=>$code ?? 1000,
            'message'=>$message ?? '请求成功',
            'data' => $data ?? []
        ];
        if (!$this->canEnd()) {
            return;
        }
        $this->response->header('Content-Type', 'text/html; charset=UTF-8');
        $result = json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        $this->response->end($result);
        $this->endOver();
    }
    /**
     * 返回json格式
     */
    public function jsonend($code = '', $message = '', $data = '', $gzip = true)
    {
        $output = array('code' => $code, 'message' => $message, 'data' => $data);

        if (!$this->canEnd()) {
            return;
        }
//        if (!get_instance()->config->get('http.gzip_off', false)) {
//            //低版本swoole的gzip方法存在效率问题
////            if ($gzip) {
////                $this->response->gzip(1);
////            }
//            //压缩备用方案
//            /* if ($gzip) {
//              $this->response->header('Content-Encoding', 'gzip');
//              $this->response->header('Vary', 'Accept-Encoding');
//              $output = gzencode($output . " \n", 9);
//              } */
//        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }
        $this->response->end($output);
        $this->endOver();
    }

    //根据工单ID获取工单类型以及保修时间
    function getWorkorderRange($work_order_id)
    {
        $work_eq_join = [
            ['equipment_lists el', 'el.equipment_id = rq_work_equipment.equipment_id', 'left join'],
        ];
        $equipment_info = $this->work_eq_model->getAll(array('rq_work_equipment.work_order_id' => $work_order_id), 'rq_work_equipment.equipment_id,el.device_no,equipment_type,start_time,end_time,el.contract_id', ['rq_work_equipment.create_time' => 'DESC'], $work_eq_join);
        if (empty($equipment_info)) {
            return false;
        }
        return $equipment_info[0];
    }

    public function deviceModel($equipment_ids)
    {
        $join = [
            ['equipment_lists el', 'el.equipment_id=rq_contract_equipment.equipment_id']
        ];
        $machines = $this->contract_eq_model->getAll(['rq_contract_equipment.equipment_id' => ['IN', $equipment_ids], 'rq_contract_equipment.state' => 1], 'machine_id', $join);
        if (empty($machines)) {
            return null;
        }
        $machine_ids = array_column($machines, 'machine_id');
        $machine_names = $this->machine_model->getAll(['machine_id' => ['IN', $machine_ids]], 1, -1, 'name,machine_id');
        $machines = [];
        foreach ($machine_names as $k => $v) {
            $machines[$v['machine_id']] = $v['name'];
        }
        $counts = array_count_values($machine_ids);//分组计数
        $result = [];
        foreach ($counts as $key => $value) {
            if (empty($key)) {
                continue;
            }
            if (count($counts) > 1) {
                array_push($result, $machines[$key] . '/' . $value);
            } else {
                array_push($result, $machines[$key]);
            }
        }
        return implode('、', $result);

    }

    /**
     * @desc   添加用户登录日志
     * @param $user_id 用户信息
     * @param $client_info 客户端信息
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addUserLog($user_id, $client_info, $operator_type = 2, $filename = '', $old_content = '', $new_content = '')
    {
        $data['operator_type'] = $operator_type;
        $data['operator_id'] = $user_id;
        $data['filename'] = $filename;
        $data['new_content'] = $new_content;
        $data['old_content'] = $old_content;
        $user_info = $this->user_model->getOne(array('user_id' => $user_id), 'realname,username,account');
        if (!empty($user_info)) {
            $data['operator_name'] = $user_info['username'];
            $data['operator_account'] = $user_info['account'];
            $data['customer_id'] = $user_id;
            $data['add_time'] = time();
            $data['terminal_type'] = 3;
            $data['client_ip'] = $this->request->server['remote_addr'];
            if (!empty($client_info)) {
                $data['client_device_brand'] = $client_info['client_device_brand']; //品牌
                $data['client_device_model'] = $client_info['client_device_model']; //型号
                $data['client_device_version'] = $client_info['clinet_device_wx_version'] . '-' . $client_info['clinet_device_system_version'] . '-' . $client_info['clinet_device_base_version'];
                $data['client_device_platform'] = $client_info['client_device_platform'];
            }

            return $this->user_log_model->add($data);
        }
        return false;
    }

    /**
     * @desc   添加工单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addWorkOrderLog($work_order_id, $time, $status, $remark)
    {
        $log['work_order_id'] = $work_order_id;
        $log['create_work_time'] = $time;
        $log['operating_time'] = time();
        $log['do_id'] = $this->user_id;
        $log['operating_type'] = 2;
        $log['operating_status'] = $status;
        $log['remarks'] = $remark;
        $res = $this->work_log_model->add($log);
        return $res;
    }

    /**
     * @desc   添加订单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLog($order_id, $status, $remark, $is_to_user = 0)
    {
        $data['operater_role'] = 2;
        $data['order_id'] = $order_id;
        $data['status'] = $status;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['is_to_user'] = $is_to_user;
        $res = $this->log_model->add($data);
        return $res;
    }

    /**
     * @desc   获取时间范围详情
     * @param
     * @date   2018-07-24
     * @return [type]     [description]
     * @author lcx
     */
    public function getRangeInfo($time_id)
    {
        $info = $this->time_model->getOne(array('time_id' => $time_id));
        if (!empty($info)) {
            return $info;
        }
        return false;
    }

    /**
     * @desc   添加合同操作日志
     * @param $status 1增2删3改4解绑'
     * @date   2018-07-19
     * @author lcx
     * @return [type]     [description]
     */
    public function addContractLog($contract_id, $status = 1, $remark)
    {
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $this->user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $this->contract_log_model->add($data);
        return $res;
    }

    /**
     * 处理不同基础服务费用
     * @param array $ser_info 服务信息 => $rental_cost 租赁时费用 $buy_cost 购买时费用 $usiness_type_id 服务ID
     * @param array $eq_info 设备信息  =>$equipment_type 类型  $start_time保修开始时间 $end_time 保修结束时间
     */
    public function baseServiceCost($ser_info, $eq_info)
    {
        $cost = $ser_info['rental_cost'];
        //购买的设备--处理不同业务类型费用
        if ($eq_info && $eq_info['equipment_type'] == 2) {
            $cost = $ser_info['buy_cost'];
            //如果在保修期内，维修不收费
            if ($ser_info['business_type_id'] == 6 && $eq_info['end_time'] > time()) {
                $cost = 0;
            }
        }
        return $cost;
    }

    /**
     * 处理不同滤芯费用
     * @param array $part_info 配件信息 => $parts_money 租赁时费用 $parts_buy_money 购买时费用 $usiness_type_id 服务ID
     * @param array $eq_info 设备信息  =>$equipment_type 类型  $start_time保修开始时间 $end_time 保修结束时间
     */
    public function partCost($part_info, $eq_info)
    {
        $cost = $part_info['parts_money'];
        //购买的设备--处理不同业务类型费用
        if ($eq_info && $eq_info['equipment_type'] == 2) {
            $cost = $part_info['parts_buy_money'];
        }
        return $cost;
    }

    /**
     * 处理不同额外服务费用
     * @param array $ser_info 服务信息 => $parts_money 租赁时费用 $parts_buy_money 购买时费用 $usiness_type_id 服务ID
     * @param array $eq_info 设备信息  =>$equipment_type 类型  $start_time保修开始时间 $end_time 保修结束时间
     */
    public function addtionalServiceCost($ser_info, $eq_info)
    {
        $cost = $ser_info['service_rental_cost'];
        //购买的设备--处理不同业务类型费用
        if ($eq_info && $eq_info['equipment_type'] == 2) {
            $cost = $ser_info['service_buy_cost'];
        }
        return $cost;
    }

    //根据工单查询合同时间
    public function getContractTime($work_order_id, $contract_id = '')
    {
        $return = [];
        //合同是否签约
        if (empty($contract_id)) {
            //根据工单找设备，设备找合同
            $join = [
                ['contract_equipment', 'rq_contract_equipment.equipment_id=rq_work_equipment.equipment_id']
            ];
            $info = $this->work_eq_model->getOne(['work_order_id' => $work_order_id, 'state' => 1], 'contract_id', $join);
            $contract_id = $info['contract_id'];
        }
        if (!empty($contract_id)) {
            $contract_info = $this->contract_model->getOne(['contract_id' => $contract_id], 'effect_time,exire_date');
            $return['effect_time'] = empty($contract_info['effect_time']) ? '' : date('Y年m月d日', $contract_info['effect_time']);
            $return['exire_date'] = empty($contract_info['exire_date']) ? '' : date('Y年m月d日', $contract_info['exire_date']);
        }
        return $return;

    }
}