<?php

namespace app;

use app\Wechat\wxcrypt\WXBizDataCrypt;
use cmiot\OneNetApi;
use Endroid\QrCode\QrCode;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use Server\Asyn\HttpClient\HttpClientPool;
use Server\Asyn\Redis\RedisAsynPool;
use Server\CoreBase\HttpInput;
use Server\CoreBase\Loader;
use Server\SwooleDistributedServer;
use Server\Components\Process\ProcessManager;
use app\Process\SimProcess;
use Server\Asyn\Mysql\MysqlAsynPool;
use app\Services\Common\ConfigService;
use tech\core\eSign;

/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-9-19
 * Time: 下午2:36
 */
class AppServer extends SwooleDistributedServer
{
    public $Memcached;
    public $tmp_table;
    public $Spreadsheet;
    public $QrCode;
    public $QiniuAuth;
    public $QiniuUploadManager;
    public $WXBizDataCrypt;
    public $eSign;
    public $OneNetApi;

    /**
     * 可以在这里自定义Loader，但必须是ILoader接口
     * AppServer constructor.
     */
    public function __construct()
    {
        $this->setLoader(new Loader());
        parent::__construct();
    }


    /**
     * 开服初始化(支持协程)
     * @return mixed
     */
    public function onOpenServiceInitialization()
    {
        parent::onOpenServiceInitialization();
    }

    /**
     * 这里可以进行额外的异步连接池，比如另一组redis/mysql连接
     * @param $workerId
     * @return void
     * @throws \Server\CoreBase\SwooleException
     * @throws \Exception
     */
    public function initAsynPools($workerId)
    {
        parent::initAsynPools($workerId);
        //开发者模式
        $dev_mode = ConfigService::getConfig('develop_mode');
        //获取微信配置
        if ($dev_mode == 1) {
            $callback_domain_name = $this->config->get('debug_config.callback_domain_name');
            $boss_api_url = $this->config->get('debug_config.boss_api_url');
        } else {
            $callback_domain_name = $this->config->get('callback_domain_name');
            $boss_api_url = $this->config->get('boss_api_url');
        }
        $this->addAsynPool('BuyWater', new HttpClientPool($this->config, $callback_domain_name));
        $this->addAsynPool('SimState', new HttpClientPool($this->config, $this->config->get('shumi.testUrl')));
        $this->addAsynPool('BossApi', new HttpClientPool($this->config, $boss_api_url));

        //增加redis备用连接池
        $this->addAsynPool('redisPoolSpare', new RedisAsynPool($this->config, $this->config->get('redis.active')));
    }

    /**
     * 用户进程
     * @throws \Exception
     */
    public function startProcess()
    {
        parent::startProcess();
        //ProcessManager::getInstance()->addProcess(SimProcess::class,'sim');
    }

    /**
     * 可以在这验证WebSocket连接,return true代表可以握手，false代表拒绝
     * @param HttpInput $httpInput
     * @return bool
     */
    public function onWebSocketHandCheck(HttpInput $httpInput)
    {
        return true;
    }

    /**
     * @return string
     */
    public function getCloseMethodName()
    {
        return 'onClose';
    }

    /**
     * @return string
     */
    public function getEventControllerName()
    {
        return 'AppController';
    }

    /**
     * @return string
     */
    public function getConnectMethodName()
    {
        return 'onConnect';
    }

    public function beforeSwooleStart()
    {
        parent::beforeSwooleStart();
        /*
        $this->tmp_table = new \swoole_table(1024);
        $this->tmp_table->column('id', \swoole_table::TYPE_INT, 8);
        $this->tmp_table->column('name', \swoole_table::TYPE_STRING, 50);
        $this->tmp_table->create();
        */

        //加载excel扩展
        $this->Spreadsheet = new Spreadsheet();

        //加载二维码扩展
        $this->QrCode = new QrCode();

        //七牛云
        $this->QiniuAuth = new Auth($this->config->get('qiniu.accessKey'), $this->config->get('qiniu.secrectKey'));
        $this->QiniuUploadManager = new UploadManager();

        //微信小程序用户加密数据的解密
        $this->WXBizDataCrypt = new WXBizDataCrypt();

        //中移API服务
        require_once APP_DIR . '/Library/cmiot/OneNetApi.php';
        //秘钥
        $apiKey = 'z2I9Ao6L6RMz10oKkoDhbM2GgxHdMa9DIm89SA6Qfur63ArG73Qae9HFDLgEUGeGMkwZ4ZU8GlIBrKR774TlQQ=='; // 正式环境
        // $apiKey = 'V1I7+XB3pbNNIcERNXhn5vemOX3ihusH3zq4tQ7yE6IcbgRGH8xI7zDkVfA3wx2VYXCBIOf+z1MpsCTBAB1ipg=='; // 测试环境
        //链接地址
        $apiUrl = 'https://iot-api.heclouds.com';
        //产品ID 5TPD49eaP2：测试环境，4pLsPK8au9：展示环境
       // $productId = '5TPD49eaP2';
      $productId = '4pLsPK8au9';

        //创建api对象
        $this->OneNetApi = new OneNetApi($apiKey, $apiUrl,$productId);
        $debug = false;//线上需要改成false
        // TODO ss
        // 测试编辑
        if (!$debug) {
            require_once APP_DIR . '/Library/eSign/API/eSignOpenAPI.php';
            $this->eSign = new eSign();
        }
        if(true){
            // 加载基础 adapay 基础类
            // SDK 初始化文件加载
            require_once  APP_DIR. "/Library/AdapaySdk/init.php";
            // 在文件中设置 DEBUG 为 true 时， 则可以打印日志到指定的日志目录下 LOG_DIR
            require_once  APP_DIR. "/Library/AdapaySdk/config.php";
        }
    }

}
