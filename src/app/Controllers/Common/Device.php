<?php


namespace app\Controllers\Common;

use app\Library\SLog;
use app\Services\Common\HttpService;
use app\Services\Device\DeviceService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use Server\CoreBase\SwooleException;

/**
 * 设备公共接口
 * Class Contract
 * @package app\Controllers\Common
 */
class Device extends Base
{

    protected $equipment_list_model;
    protected $equipmentsPartsModel;
    protected $partsModel;
    protected $equipmentListsModel;
    protected $Achievement;
    protected $contractEquipmentModel;
    protected $equipmentWaterRecordModel;
    protected $contract_ep_model;
    protected $filterPackageModel;

    // 前置方法，加载模型
    public function initialization($controller_name, $method_name)
    {
        if ($this->response !== null && $this->request_type == 'http_request') {
            $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
            $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token");
            $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        }
        parent::initialization($controller_name, $method_name);

        $this->equipment_list_model = $this->loader->model('EquipmentListsModel', $this);
        $this->equipmentsPartsModel = $this->loader->model('EquipmentsPartsModel', $this);
        $this->partsModel = $this->loader->model('PartsModel', $this);
        $this->equipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->contractEquipmentModel = $this->loader->model('ContractEquipmentModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->equipmentWaterRecordModel = $this->loader->model('EquipmentWaterRecordModel', $this);
        $this->filterPackageModel = $this->loader->model('FilterPackageModel', $this);
        $this->forCoreRecordModel = $this->loader->model('ForCoreRecordModel', $this);
    }

    /**
     * showdoc
     * @catalog API文档/公共API/设备相关
     * @title 同步滤芯数据
     * @description
     * @method POST
     * @url /Common/Device/syncFilterData
     * @param equipment_id 可选 int 设备ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_syncFilterData()
    {

        if (!empty($this->parm['equipment_id'])) {
            $map['equipment_id'] = $this->parm['equipment_id'];
        } else {
            $map['device_status'] = 4;
        }
        $lists = $this->equipment_list_model->selectEquipmentLists($map, 'shutdown_state,equipment_id,device_no,device_status,remaining_days');
        if (empty($lists)) {
            return $this->jsonend(-1002, "暂无处于滤芯待复位状态的设备");
        }
        $join = [
            ['equipments_parts ep', 'ep.parts_id = rq_parts.parts_id', 'left'],  // 设备配件表 -- 配件表
        ];
        $field = 'ep.id,rq_parts.parts_id,rq_parts.parts_name,ep.cycle,ep.expire_time';
        foreach ($lists as $k => $v) {
            // 获取设备滤芯
            $where['ep.equipment_id'] = $v['equipment_id'];
            $where['ep.is_filter'] = 1;  // 1表示滤芯
            $where['ep.is_delete'] = 0;  // 0表示未删除
            $equipments_parts = $this->partsModel->getJoinAll($where, $field, $join);
            unset($where);
            if ($equipments_parts) {
                foreach ($equipments_parts as $kk => &$vv) {
                    $residual_value = floor(($vv['expire_time'] - time()) / 86400); // 剩余天数，当前仅根据时间计算，算法待定
                    if ($v['remaining_days'] < 1 || $v['shutdown_state'] == 1) {
                        $residual_value = 180; //如果欠费和滤芯待复位同时存在，下发固定的滤芯值
                    }
                    $params = ['sn' => $v['device_no'], 'type' => 2, 'key' => $kk + 1, 'value' => $residual_value + 1];
                    $path = '/House/Issue/filter_element_reset_modification';
                    sleepCoroutine(2000);
                    $result = HttpService::Thrash($params, $path);
                    if (!$result) {
                        return $this->jsonend(-1002, $v['device_no'] . '发送失败');
                    }
                }
                //删除redis数据，用于同步
                $this->redis->del('device_heartbeat:' . $v['device_no']);
                $this->redis->del('work_state:' . $v['device_no']);

                //请求心跳
                sleepCoroutine(2000);
                $state_params['sn'] = $v['device_no'];
                $state_path = '/House/Issue/heartbeat';
                HttpService::Thrash($state_params, $state_path);
            }
        }
        return $this->jsonend(1000, "处理完成,请在设备信息里确认是否同步成功");
    }

    /**
     * 获取所有设备版本
     */
    public function http_getVersion()
    {
        $where['version'] = '';
        $data = $this->equipmentListsModel->selectEquipmentLists($where, 'equipment_id,version,device_no');
        if (empty($data)) {
            return $this->jsonend(-1000, "暂无数据");
        }

        foreach ($data as $k => $v) {
            if ($v['device_no'] == 0) {
                continue;
            }
            $params = ['sn' => $v['device_no']];
            $path = '/House/Issue/version';
            $result = HttpService::Thrash($params, $path);
            if (!$result) {
                return $this->jsonend(-1002, $v['device_no'] . '发送失败');
            }
        }
        return $this->jsonend(1000, "获取版本成功,请在设备信息里确认");
    }

    //同步工单表机型到设备表--临时
    public function http_updateDeviceModel()
    {
        $this->Achievement->table = 'work_order_business';
        $join = [
            ['work_order w', 'rq_work_order_business.work_order_id=w.work_order_id', 'left']
        ];
        $lists = $this->Achievement->selectData(['rq_work_order_business.business_id' => 3, 'work_order_status' => 11], 'device_model,w.work_order_id', $join);
        if (!empty($lists)) {
            foreach ($lists as $k => $v) {
                $this->Achievement->table = 'work_equipment';
                $work_equipment = $this->Achievement->selectData(['work_order_id' => $v['work_order_id']], 'equipment_id');
                if (!empty($work_equipment)) {
                    foreach ($work_equipment as $kk => $vv) {
                        $this->Achievement->table = 'equipment_lists';
                        $this->Achievement->updateData(['equipment_id' => $vv['equipment_id']], ['machine_id' => $v['device_model']]);
                    }
                }
                $this->Achievement->table = 'work_order';
                $this->Achievement->updateData(['work_order_id' => $v['work_order_id']], ['device_model' => $v['device_model']]);
            }
        }
    }

    public function http_syncTime()
    {
        // 接收参数
        $key_word = $this->parm['key'] ?? '';
        $keywords = $this->parm['keywords'] ?? ''; //设备号|电话|昵称|姓名
        $searchStatus = $this->parm['status'] ?? '';
        $searchType = $this->parm['type'] ?? '';
        $device_type = $this->parm['device_type'] ?? '';
        $equipment_type = $this->parm['equipment_type'] ?? '';
        $deviceStatus = $this->parm['device_status'] ?? 99;
        $statusname = $this->parm['statusname'] ?? '';
        $filter_element_state = $this->parm['filter_element_state'] ?? '';
        if (!empty($this->company_id)) {
            $map['rq_equipment_lists.company_id'] = $this->company_id;
        }
        if (!empty($searchStatus)) {
            $map[$searchStatus] = $searchType;
        }
        if (!empty($key_word)) {
            $map['device_no|alias_name|equipments_name|version|m.model'] = ['like', "%" . $key_word . "%"];
        }

        if (!empty($device_type)) {
            $map['e.type'] = $device_type;
        }
        if (!empty($deviceStatus) && $deviceStatus != 99) {
            $map['rq_equipment_lists.device_status'] = $deviceStatus;
        }
        if ($deviceStatus === 0) {
            $map['rq_equipment_lists.device_status'] = $deviceStatus;
        }
        if (!empty($statusname)) {
            $map['rq_equipment_lists.status'] = $statusname;
        }
        //查询设备类型
        if (isset($this->parm['equipment_type']) && $this->parm['equipment_type'] != '') {
            $map['equipment_type'] = ['=', $this->parm['equipment_type']];
            if ($this->parm['equipment_type'] == -1) { //已经绑定用户
                $map['equipment_type'] = ['IN', [2, 1]];
            }
        }
        // 搜索-合同到期时间
        if (!empty($this->parm['startTime'])) {
            //查找该运营中心的合同
            $join = [
                ['contract c', 'c.contract_id=rq_contract_equipment.contract_id', 'left']
            ];
            $startTime = strtotime($this->parm['startTime']);
            $endTime = empty($this->parm['endTime']) ? time() : strtotime($this->parm['endTime']) + (60 * 60 * 24 - 1);
            $contract_where['c.exire_date'] = ['between', [$startTime, $endTime]]; //终止到当天23:59:59
            $contract_equipment = $this->contractEquipmentModel->getAll($contract_where, 'equipment_id', $join);
            if (!empty($contract_equipment)) {
                $equipment_ids = array_column($contract_equipment, 'equipment_id');
                $map['rq_equipment_lists.equipment_id'] = ['IN', $equipment_ids];
            }
        }
        $join = [
            ['equipments e', 'rq_equipment_lists.equipments_id = e.equipments_id', 'LEFT'],
            ['mainboard m', 'rq_equipment_lists.mainboard_batch_id = m.mainboard_id', 'LEFT'],
            ['customer_bind_equipment b', 'b.equipment_id = rq_equipment_lists.equipment_id', 'LEFT'],
            ['customer c', 'b.user_id = c.user_id', 'LEFT'],
        ];
        $map['b.is_owner'] = 2;
        $map['b.status'] = 1;
        $map['e.type'] = 1;

        if (!empty($keywords)) {
            $map['rq_equipment_lists.device_no|c.realname|c.username|c.telphone'] = ['like', "%" . $keywords . "%"];
        }
        if (!empty($equipment_type) && $equipment_type != -1) {
            $map['rq_equipment_lists.equipment_type'] = $equipment_type;
        }
        if (!empty($filter_element_state)) {
            $map['rq_equipment_lists.filter_element_state'] = $filter_element_state;
        }
        $field = 'rq_equipment_lists.device_no,rq_equipment_lists.equipment_id';
        // 列表查询
        $data = $this->equipment_list_model->selectEquipmentLists($map, $field, [], "", $join);
        if (empty($data)) {
            return $this->jsonend(-1000, "无相关数据");
        }
        foreach ($data as $k => $v) {
            //查找该设备关联的合同
            $c_join = [];
            $map = [];
            $c_join = [
                ['contract c', 'c.contract_id = rq_contract_equipment.contract_id', 'left']
            ];
            $map['c.status'] = ['IN', [4, 5]]; //生效中或者已过期的合同
            $map['rq_contract_equipment.is_delete'] = 0;
            $map['rq_contract_equipment.state'] = 1;
            $map['equipment_id'] = $v['equipment_id'];
            $contract_info = $this->contract_ep_model->getAll($map, 'c.contract_id,c.package_mode,c.package_value,c.package_cycle,effect_time,exire_date', $c_join);
            if (empty($contract_info)) {
                AsyncFile::write('issue_package', date('Y-m-d H:i:s', time()) . '-' . $v['device_no'] . ':没有相关合同,无法进行套餐下发');
                return $this->jsonend(-1000, "没有相关合同,无法进行套餐下发");
            }
            $working_mode = 1;
            if ($contract_info[0]['package_mode'] == 2) {
                $working_mode = 0;
            }
            //计算数据
            $used_days = ceil((time() - $contract_info[0]['effect_time']) / 86400); //已用天数，当前时间-签约时间
            //设备在到期时间当月最后一天前都可以使用--2020-04-13修改
            $shutdown = $this->parm['shutdown'] ?? 0;
            $exire_date = strtotime(getMonthLastDay($contract_info[0]['exire_date']));   //获取签约月份最后一天
            if ($shutdown == 1) {
                $exire_date = $contract_info[0]['exire_date'];
            }
            $remaining_days = floor(($exire_date - time()) / 86400); //剩余天数,过期时间-当前时间
            //统计设备已用流量
            $equipment_water_record_sum = $this->equipmentWaterRecordModel->getOne(['equipment_id' => $v['equipment_id']], 'sum(water) total_water');  // 总用水量
            $used_traffic = $equipment_water_record_sum['total_water'] ?? 0; //已用流量
            $remaining_traffic = 999999999;
            if ($working_mode == 0) {
                $remaining_traffic = $contract_info[0]['package_value'] - $used_traffic; //如果是流量模式,剩余流量=套餐值-已用流量
            }
            //下发--数据同步
            $data_sync_params['sn'] = $v['device_no'];
            $data_sync_params['used_days'] = $used_days;
            $data_sync_params['used_traffic'] = $used_traffic;
            $data_sync_params['remaining_days'] = $remaining_days;
            $data_sync_params['remaining_traffic'] = $remaining_traffic;
            $data_sync_path = '/House/Issue/data_sync';
            HttpService::Thrash($data_sync_params, $data_sync_path);
            //保存设备数据
            $this->equipment_list_model->updateEquipmentLists(array('working_mode' => $working_mode, 'remaining_traffic' => $data_sync_params['remaining_traffic'], 'remaining_days' => $data_sync_params['remaining_days'], 'used_traffic' => $data_sync_params['used_traffic'], 'used_days' => $data_sync_params['used_days']), array('equipment_id' => $v['equipment_id']));
            $this->redis->del('device_heartbeat:' . $v['device_no']); //删除redis心跳
            //删除redis数据，用于同步
            $this->redis->del('device_heartbeat:' . $v['device_no']);
            $this->redis->del('work_state:' . $v['device_no']);
            //请求心跳
            sleepCoroutine(2000);
            $state_params['sn'] = $v['device_no'];
            $state_path = '/House/Issue/heartbeat';
            HttpService::Thrash($state_params, $state_path);
            AsyncFile::write('issue_package', date('Y-m-d H:i:s', time()) . '-' . $v['device_no'] . ':' . json_encode(['data_sync_params' => $data_sync_params]));
        }
        return $this->jsonend(1000, "操作成功");
    }

    /**
     * showdoc
     * @catalog API文档/公共API/设备相关
     * @title 快速换芯
     * @description
     * @method POST
     * @url /Common/Device/changeFilterElement
     * @param equipment_id 必选 int 设备ID
     * @param filter_package_id 必选 int 套餐ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_changeFilterElement()
    {
        $service = new DeviceService();
        [$ok,$result] = $service->changeFilterElement($this->parm);
        return $this->jsonend($ok? 1000 : -1000,$result);
    }

    /**
     * sgg
     * 设备列表批量操作换芯
     * @return null
     * @throws \RedisException
     */
    public function http_changeFilterElementAll()
    {
        $service = new DeviceService();
        [$ok,$result] = $service->changeFilterElementAll($this->parm);
        return $this->jsonend($ok ? 1000 : -1000,$result);
    }


    /**
     * 计算滤芯剩余天数
     */
    public function remainFilterDays($install_time, $cycle)
    {
        $md = date('m-d', $install_time);
        $exipre_time = strtotime((date('Y')) . '-' . $md . ' 0:0:0') + $cycle * 86400;//换芯时间
        $exipre_days = time() < $exipre_time ? 0 : ceil((time() - $exipre_time) / 86400);
        $remain_days = $exipre_days > $cycle ? $cycle : $cycle - $exipre_days;
        return $remain_days;
    }
}
