<?php

namespace app\Controllers\Common;

use app\Services\Common\ConfigService;

/**
 * 测试数据模块--接口
 */
class Test extends Base {

	protected $eq_libray_model;
    protected $eq_qrcode_model;

	public function initialization($controller_name, $method_name) {
		parent::initialization($controller_name, $method_name);
		$this->eq_libray_model = $this->loader->model('EquipmentLibraryModel', $this);
        $this->eq_qrcode_model = $this->loader->model('EquipmentQrcodeModel', $this);
	}

	/**
	 * showdoc
	 * @catalog API文档/公共API/测试相关
	 * @title 获取绑定设备测试数据
	 * @description 
	 * @method POST
	 * @url Common/Test/bindDeviceNo
	 * @return { "code": 1000,"message": "获取数据成功", "data": {"status": "1", "data": [ {"device_no": "866050034443331" }] }}
	 * @return_param device_no string 设备编号
	 * @return_param status int 是否开启
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2018-10-18
	 */
	public function http_bindDeviceNo() {
		$key = 'test_device_no';
		$device_no = ConfigService::getConfig($key,false,$this->company);
		if (empty($device_no)) {
			return $this->jsonend(-1003, "暂无相关数据");
		}
		$arr = explode(',', $device_no);
		foreach ($arr as $k => $v) {
			$a['device_no'] = $v;
			$new_arr[] = $a;
		}
		$data['status'] = ConfigService::getConfig('test_switch',false,$this->company);
		$data['data'] = $new_arr;
		return $this->jsonend(1000, '获取数据成功', $data);
	}

	/**
	 * showdoc
	 * @catalog API文档/公共API/测试相关
	 * @title 维修人员测试账号
	 * @description 
	 * @method POST
	 * @url Common/Test/engineer_account
	 * @return {"code": 1000,"message": "获取数据成功", "data": {"status": "1","data": [{"account": "18888888888","pwd": "123456" }]}}
	 * @return_param status int 是否开启
	 * @return_param account string 账号
	 * @return_param pwd string 登陆密码
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2018-10-18
	 */
	public function http_engineer_account() {
		$key = 'test_engineer_account';
		$account = ConfigService::getConfig($key,false,$this->company);
		$pwd = ConfigService::getConfig('test_engineer_pwd',false,$this->company);
		if (empty($account)) {
			return $this->jsonend(-1003, "暂无相关数据");
		}
		$arr = explode(',', $account);
		foreach ($arr as $k => $v) {
			$a['account'] = $v;
			$a['pwd'] = $pwd;
			$new_arr[] = $a;
		}
		$data['status'] = ConfigService::getConfig('test_switch',false,$this->company);
		$data['data'] = $new_arr;
		return $this->jsonend(1000, '获取数据成功', $data);
	}

	/**
	 * showdoc
	 * @catalog API文档/公共API/测试相关
	 * @title 模块状态
	 * @description 
	 * @method POST
	 * @url Common/Test/moduleSwitch
	 * @return 
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2018-10-18
	 */
	public function http_moduleSwitch() {
		$data['invitation'] = true; //是否开启邀请好友（个人中心-邀请好友，我的推广,我的二维码入口）
		$data['channel_apply'] = true; //是否开启申请成为渠道（个人中心-申请成为渠道入口）
		$data['channel'] = true; //是否开启我的渠道（个人中心-我的渠道入口）
        $data['user_install_agent_pay'] = true;//用户端是否支持代付
        $data['user_renew_agent_pay'] = true;//用户端是否支持代付
        $data['engineer_agent_pay'] = true;//工程端是否支持代付
		return $this->jsonend(1000, '获取成功', $data);
	}

	/**
	 * showdoc
	 * @catalog API文档/公共API/测试相关
	 * @title 设备入库
	 * @description 
	 * @method POST
	 * @url Common/Test/importEquipment
	 * @return 
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2018-10-18
	 */
	public function http_importEquipment() {
	    $device_no=$this->parm['device_no']?trim($this->parm['device_no']):'';
        $protocol_version_id = $this->parm['protocol_version_id'] ?? 3;
        if ($protocol_version_id == 2) {
            $mainboard_id = 1;
        } else {
            $mainboard_id = 12;
        }

		if (empty($device_no)) {
			return $this->jsonend(-1001, '请输入设备编号');
		}
        //查询设备信息
        $qr_info=$this->eq_qrcode_model->getOne(['qrcode_no'=>$device_no],'id,device_no');
        if(!empty($qr_info)){
            $device_no=$qr_info['device_no'];
        }
		$check = $this->eq_libray_model->getOne(['device_no' =>$device_no]);
		if (!empty($check)) {
			return $this->jsonend(-1001, '设备编号已经存在');
		}
		$data = [
			'model_id' => $mainboard_id,
			'device_no' => $device_no,
            'protocol_version' => $protocol_version_id,
			'device_status' => 1,
			'add_time' => time()
		];
		$result = $this->eq_libray_model->add($data);
		if ($result) {
			return $this->jsonend(1000, '入库成功');
		}
		return $this->jsonend(-1000, '入库失败');
	}
	

}
