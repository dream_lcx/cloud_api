<?php

namespace app\Controllers\Common;

use app\Services\Common\EmailService;

/**
 * 邮件功能
 *
 * @author lcx 20181008 
 */
class Email extends Base {

    protected $contract_model;

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
        $this->contract_model = $this->loader->model('ContractModel', $this);
    }

    /**
     * showdoc
     * @catalog API文档/公共API/合同相关
     * @title 发送邮件
     * @description 将合同以附件形式发送到邮箱
     * @method POST
     * @url Common/Email/saveContract
     * @param contract_id 必选 string 合同ID  
     * @param email 必选 string 收件人邮箱  
     * @return { "code": 1000,"message": "发送成功", "data": ""}
     * @remark {"email":"916450508@qq.com","contract_id":16}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_saveContract() {
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数合同ID");
        }
        if (empty($this->parm['email'] ?? '')) {
            return $this->jsonend(-1001, "请输入邮箱地址");
        }
        if (!isEmail($this->parm['email'])) {
            return $this->jsonend(-1101, "请输入正确的邮箱地址");
        }
        file_put_contents('./senContractEmail.log',date('Y-m-d H:i:s').$this->parm['email'].'-'.$this->parm['contract_id'].PHP_EOL,FILE_APPEND);
        //查询合同信息
        $contract_info = $this->contract_model->getOne(array('contract_id' => $this->parm['contract_id']), 'esign_filename,contract_no,contract_class');
        if (empty($contract_info)) {
            return $this->jsonend(-1102, "合同不存在");
        }
        if (empty($contract_info['esign_filename'])) {
            return $this->jsonend(-1103, "该合同未签署");
        }
        $content = '您好,查看合同原件信息请下载附件!感谢您的使用';
        $subject = '家用机购买合同-' . $contract_info['contract_no'];
        if ($contract_info['contract_class'] == 1) {
            $subject = '家用机租赁合同-' . $contract_info['contract_no'];
        }

        $attach_path = WWW_DIR . '/' . $contract_info['esign_filename'];
        file_put_contents('./senContractEmail.log',date('Y-m-d H:i:s').$this->parm['email'].'-'.$this->parm['contract_id'].'--'.$attach_path.PHP_EOL,FILE_APPEND);

        // $attach_path = UploadImgPath($contract_info['esign_filename']);
        $attach_name = date('YmdHis') . $contract_info['contract_no'] . '.pdf';
        $res = EmailService::sendEmailAsync($this->parm['email'], $content, $subject, $attach_path, $attach_name,'',$this->company);//异步发送
		return $this->jsonend(1000, "发送成功");
//        if ($res) {
//            return $this->jsonend(1000, "发送成功");
//        }
//        return $this->jsonend(-1000, "发送失败");
    }
}
