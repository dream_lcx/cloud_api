<?php

namespace app\Controllers\Common;


/**
 * 系统配置信息
 *
 * @author lcx
 */
class Conf extends Base {

    protected $screen_model;
    protected $timed_model;

    // 前置方法,加载模型
    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name); 
        $this->screen_model = $this->loader->model('EquipmentScreenModeConfigModel', $this);
        $this->timed_model = $this->loader->model('EquipmentTimedFlushConfigModel', $this);
    }

    /**
    * showdoc
    * @catalog API文档/公共API/设备相关
    * @title 屏显模式配置
    * @description 通过后台配置
    * @method POST
    * @url Common/Conf/screenMode
    * @return { "code": 1000,"message": "获取数据成功","data": [{ "time_mode": "0", "flow_mode": "2","description": "已用天数和已用流量" }]}
    * @return_param time_mode int 时间模式
    * @return_param flow_mode int 流量模式
    * @return_param description string 配置描述
    * @remark 
    * @number 0
    * @author lcx
    * @date 2018-10-18
    */
    public function http_screenMode() {
        $map['status'] = 1;
        $field = 'time_mode,flow_mode,description';
        $data = $this->screen_model->getAll($map,$field);
        if(empty($data)){
            return $this->jsonend(-1003,"暂无相关数据");
        }
        return $this->jsonend(1000,"获取数据成功",$data);
    }
    /**
    * showdoc
    * @catalog API文档/公共API/设备相关
    * @title 定时冲洗时间配置
    * @description 通过后台配置
    * @method POST
    * @url Common/Conf/timedFlush
    * @return {"code": 1000,"message": "获取数据成功","data": [{"value": "2", "description": "2小时冲洗一次" }]}
    * @return_param value int 时长
    * @return_param description string 描述
    * @remark 
    * @number 0
    * @author lcx
    * @date 2018-10-18
    */
    public function http_timedFlush() {
        $map['status'] = 1;
        $field = 'value,description';
        $data = $this->timed_model->getAll($map,$field);
        if(empty($data)){
            return $this->jsonend(-1003,"暂无相关数据");
        }
        foreach ($data as $key=>$value){
            $data[$key]['value'] = intval($value['value']);
        }
        return $this->jsonend(1000,"获取数据成功",$data);
    }

}
