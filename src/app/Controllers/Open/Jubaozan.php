<?php


namespace app\Controllers\Open;


use app\Services\Common\ConfigService;
use app\Services\Common\HttpService;
use app\Services\Common\NotifiyService;

/**
 * 水行家==聚宝赞接口对接
 * Class Jubaozan
 * @package app\Controllers\Open
 */
class Jubaozan extends Base
{

    protected $contractModel;
    protected $equipmentlistsModel;
    protected $equipmentQrcodeModel;
    protected $contractEquipmentModel;
    protected $renewOrderModel;
    protected $contractEpModel;
    protected $renewRecordModel;
    protected $renewOrderEquipmentModel;
    protected $contractLogModel;
    protected $userModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->contractModel = $this->loader->model('ContractModel', $this);
        $this->equipmentlistsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->equipmentQrcodeModel = $this->loader->model('EquipmentQrcodeModel', $this);
        $this->contractEquipmentModel = $this->loader->model('ContractEquipmentModel', $this);
        $this->renewOrderModel = $this->loader->model('RenewOrderModel', $this);
        $this->contractEpModel = $this->loader->model('ContractEquipmentModel', $this);
        $this->renewRecordModel = $this->loader->model('RenewRecordModel', $this);
        $this->renewOrderEquipmentModel = $this->loader->model('RenewOrderEquipmentModel', $this);
        $this->contractLogModel = $this->loader->model('ContractLogModel', $this);
        $this->userModel = $this->loader->model('CustomerModel', $this);
    }

    /**
     * 授权地址
     * https://rqcrm.youheone.com/Open/Jubaozan/orderPayCallback
     */
    public function http_orderPayCallback()
    {
        $param = $this->http_input->getRawContent();
        file_put_contents('./jubaozan.log', date('Y-m-d H:i:s') . json_encode($param));
        return $this->jsonend(0, "OK");
    }

    /**
     * 订单消息推送地址
     * https://rqcrm.youheone.com/Open/Jubaozan/orderPayPush
     * @return string
     * @throws \Server\CoreBase\SwooleException
     */
    public function http_orderPayPush()
    {
        $data = $this->http_input->getRawContent();
        file_put_contents('./jubaozan_push.log', json_encode($data));
        // $data = file_get_contents('./jubaozan_push.log');
        //$data = json_decode(json_decode($data, true), true);
        $data = json_decode($data, true);
        $msg = json_decode(urldecode($data['msg']), true);
        //直接返回==非买家已签收状态;要求留言为空
        if (!empty($msg['status']) && $msg['status'] != 'TRADE_BUYER_SIGNED' || empty($remark_no)) {
            return 'SUCCESS';
        }
        $remark_no = $this->parm['remark_no'] ?? '';//要求留言,合同编号或者设备编号==暂时读取的买家备注
        $messages = $data['messages'];
        if (empty($messages)) {
            return 'SUCCESS';
        }
        //从自定义留言获取编号
        foreach ($messages as $k => $v) {
            $remark_no = $v['message'];
            //此处待确认
//            if($v['messageName']==''){
//                $remark_no = $v['message'];
//            }
        }
        if (empty($remark_no)) {
            var_dump("缺少参数编号");
            return "缺少参数编号";
        }
        //根据编号查询用户合同信息
        $contract_info = [];
        //根据设备查询
        $where['status'] = 2;
        $where['device_no|qrcode_no'] = $remark_no;
        $device_info = $this->equipmentQrcodeModel->getDetail($where, 'device_no');
        unset($where);
        $field = 'contract_no,rq_contract.company_id,rq_contract.contract_id,rq_contract.user_id,package_mode,package_value,package_money,package_cycle,renew_money,exire_date';
        if (!empty($device_info)) {
            $join = [
                ['equipment_lists', 'rq_contract_equipment.equipment_id=rq_equipment_lists.equipment_id'],
                ['contract', 'rq_contract_equipment.contract_id=rq_contract.contract_id'],
            ];
            $where['device_no'] = $device_info['device_no'];
            $where['is_delete'] = 0;
            $where['state'] = 1;
            $contract_info = $this->contractEquipmentModel->getOne($where, $field, $join);
        }
        //根据合同编号查询
        if (empty($contract_info)) {
            $contract_info = $this->contractModel->getOne(['contract_no' => $remark_no], $field);
        }
        if (empty($contract_info)) {
            return 'SUCCESS';
        }
        //避免重复处理
        $renew_info = $this->renewOrderModel->getOne(['wx_callback_num' => $msg['tid'], 'is_third_source' => 1], 'renew_order_id');
        if (!empty($renew_info)) {
            return 'SUCCESS';
        }
        $renew_data['user_id'] = $contract_info['user_id'];
        $renew_data['renew_order_no'] = pay_sn('YJY');
        $renew_data['contract_id'] = $contract_info['contract_id'];
        $renew_data['package_mode'] = $contract_info['package_mode'];
        $renew_data['package_value'] = $contract_info['package_value'];
        $renew_data['package_money'] = $contract_info['package_money'];
        $renew_data['package_cycle'] = $contract_info['package_cycle'];
        $renew_data['order_actual_money'] = $contract_info['renew_money'];
        $renew_data['pay_status'] = 2;
        $renew_data['create_time'] = time();
        $renew_data['status'] = 3;
        $renew_data['opertaor_role'] = 1;
        $renew_data['opertaor_uid'] = $contract_info['user_id'];
        $renew_data['pay_time'] = strtotime($msg['pay_time']);
        $renew_data['renew_time'] = time();
        $renew_data['wx_callback_num'] = $msg['tid'];//聚宝赞方单号
        $renew_data['is_third_source'] = 1;
        $renew_data['third_source'] = "JUBAOZAN";
        $status = false;
        $exire_date = $contract_info['exire_date'] + ($renew_data['package_cycle'] * 60 * 60 * 24);  // 到期时间延后
        $this->db->begin(function () use ($contract_info, $msg, $data, $renew_data, $exire_date, &$status) {
            $renew_order_id = $this->renewOrderModel->add($renew_data); //新增续费订单
            $contract['exire_date'] = $exire_date;
            $contract['real_exire_date'] = $exire_date;
            if ($exire_date > time()) {
                $contract['status'] = 4;
            }
            $this->contractModel->save(['contract_id' => $contract_info['contract_id']], $contract);
            //添加合同日志
            $this->addContractLog($contract_info['contract_id'], 3, "【第三方】续费成功,修改合同到期时间为" . date('Y-m-d H:i:s', $exire_date), $contract_info['user_id']);
            //查询合同设备信息
            $contract_ep_info = $this->contractEpModel->getAll(array('contract_id' => $contract_info['contract_id']), 'equipment_id');
            if (empty($contract_ep_info)) {
                return false;
            }
            //添加续费-设备关联表
            foreach ($contract_ep_info as $key => $value) {
                $eq_data = [];
                $eq_data['renew_order_id'] = $renew_order_id;
                $eq_data['equipment_id'] = $value['equipment_id'];
                $eq_data['create_time'] = time();
                $this->renewOrderEquipmentModel->add($eq_data);
            }
            //添加续费记录
            $record = [];
            $record['renew_order_id'] = $renew_order_id;
            $record['user_id'] = $contract_info['user_id'];
            $record['contract_id'] = $contract_info['contract_id'];
            $record['remarks'] = '用户合同续费，微信支付';
            $record['renew_record_time'] = time();
            $this->renewRecordModel->add($record);

            //续费设备--下发套餐--数据同步指令
            $renew_eq_map['renew_order_id'] = $renew_order_id;
            $renew_eq_join = [
                ['equipment_lists as el', 'rq_renew_order_equipment.equipment_id = el.equipment_id', 'LEFT'],
                ['equipments as e', 'e.equipments_id = el.equipments_id', 'LEFT'],
            ];
            $renew_eq_data = $this->renewOrderEquipmentModel->getAll($renew_eq_map, 'el.equipment_id,el.device_no,e.type,el.used_days,el.used_traffic,el.remaining_days,el.remaining_traffic', $renew_eq_join);
            if (!empty($renew_eq_data)) {
                foreach ($renew_eq_data as $k => $v) {
                    //修改设备表租赁时间
                    $this->equipmentlistsModel->updateEquipmentLists(array('end_time' => $contract['exire_date']), array('equipment_id' => $v['equipment_id']));
                    if ($v['type'] == 1) {//如果是智能设备
                        $exire_date = strtotime(getMonthLastDay($contract['exire_date']));   //获取签约月份最后一天
                        $data_sync_params['sn'] = $v['device_no'];
                        $data_sync_params['used_days'] = $v['used_days'];
                        $data_sync_params['used_traffic'] = $v['used_traffic'];
                        $data_sync_params['remaining_days'] = $v['remaining_days'];
                        $data_sync_params['remaining_traffic'] = $v['remaining_traffic'];
                        if ($contract_info['package_mode'] == 2) {
                            $data_sync_params['remaining_traffic'] = $contract_info['package_value'] + $v['remaining_traffic'];
                        } else if ($contract_info['package_mode'] == 1) {
                            $data_sync_params['remaining_days'] = ceil(($exire_date - time()) / 86400);
                        }
                        $data_sync_path = '/House/Issue/data_sync';
                        HttpService::Thrash($data_sync_params, $data_sync_path);
                        sleepCoroutine(2000);
                        //请求心跳
                        $state_params['sn'] = $v['device_no'];
                        $state_path = '/House/Issue/heartbeat';
                        HttpService::Thrash($state_params, $state_path);
                    } else {
                        continue;
                    }
                }
            }
            $status = true;
        });
        if ($status) {

            // 模板信息
            $exire_date = date('Y-m-d H:i:s', $exire_date);
            $now = date('Y-m-d H:i:s', time());
            $package_mode = '时长模式';
            $package = $package_mode . '(' . $contract_info['package_value'] . '天)';
            if ($contract_info['package_mode'] == 2) {
                $package_mode = '流量模式';
                $package = $package_mode . '(' . $contract_info['package_value'] . 'ML)';
            }
            $user_notice_config = ConfigService::getTemplateConfig('renew_success', 'user', $contract_info['company_id']);
            if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                $template_id = $user_notice_config['template']['wx_template_id'];
                $mp_template_id = $user_notice_config['template']['mp_template_id'];
                $user_info = $this->userModel->getOne(array('user_id' => $contract_info['user_id']), 'username,openid,telphone');
                // 模板内容
                $template_data = [
                    'keyword1' => ['value' => $renew_data['renew_order_no']], //订单号
                    'keyword2' => ['value' => $contract_info['package_cycle']], // 续费时长
                    'keyword3' => ['value' => $package], // 续费类型
                    'keyword4' => ['value' => $exire_date], //到期时间
                    'keyword5' => ['value' => $now], //续费时间
                    'keyword6' => ['value' => $renew_data['order_actual_money']], // 金额
                    'keyword7' => ['value' => '您的续费已完成。感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾,合同编号:' . $contract_info['contract_no']], // 备注
                ];
                $weapp_template_keyword = '';
                $mp_template_data = [
                    'first' => ['value' => '恭喜!您已成功续费', 'color' => '#4e4747'],
                    'keyword1' => ['value' => "合同续费(合同编号:" . $contract_info['contract_no'] . ")", 'color' => '#4e4747'],
                    'keyword2' => ['value' => $exire_date, 'color' => '#4e4747'],
                    'remark' => ['value' => '感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾!若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $contract_info['company_id']), 'color' => '#173177'],
                ];
                $template_tel = $user_info['telphone'];  // 电话
                $template_content = '您的续费的' . $package . '已成功，续费时长为' . $contract_info['package_cycle'] . '，续费金额为' . $renew_data['order_actual_money'] . '。打开微信进入小程序可查看详情,如有疑问请联系客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $contract_info['company_id']);
                NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, '', $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $contract_info['company_id']);
            }

            return 'SUCCESS';
        }
        return 'FAIL';
    }

    public function addContractLog($contract_id, $status = 1, $remark, $user_id)
    {
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $this->contractLogModel->add($data);
        return $res;
    }
}