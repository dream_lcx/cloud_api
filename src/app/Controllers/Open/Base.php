<?php

namespace app\Controllers\Open;

use app\Services\Common\VaildateService;
use app\Services\Company\CompanyService;
use app\Services\Log\AsyncFile;
use Server\CoreBase\Controller;
use app\Services\Common\ConfigService;
use Server\CoreBase\SwooleRedirectException;
use Server\CoreBase\SwooleException;
use Monolog\Logger;
use Server\SwooleMarco;

/*
 * 基础类
 */

class Base extends Controller
{

    protected $parm;
    public function initialization($controller_name, $method_name)
    {
        if ($this->response !== null && $this->request_type == 'http_request') {
            $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
            $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token,company,openid,role");
            $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        }
        parent::initialization($controller_name, $method_name);
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        $this->parm = VaildateService::validateParam($this->parm);//参数验证

    }

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 异常的回调(如果需要继承$autoSendAndDestroy传flase)
     * @param \Throwable $e
     * @param callable $handle
     */
    public function onExceptionHandle(\Throwable $e, $handle = null)
    {
        $log = '--------------------------[报错信息]----------------------------' . PHP_EOL;
        $log .= 'client：Open' . PHP_EOL;
        $log .= 'DATE：' . date("Y-m-d H:i:s") . PHP_EOL;
        $log .= 'getMessage：' . $e->getMessage() . PHP_EOL;
        $log .= 'getCode：' . $e->getCode() . PHP_EOL;
        $log .= 'getFile：' . $e->getFile() . PHP_EOL;
        $log .= 'getLine：' . $e->getLine();
        AsyncFile::write('error', $log);
        //必须的代码
        if ($e instanceof SwooleRedirectException) {
            $this->http_output->setStatusHeader($e->getCode());
            $this->http_output->setHeader('Location', $e->getMessage());
            $this->http_output->end('end');
            return;
        }
        if ($e instanceof SwooleException) {
            secho("EX", "--------------------------[报错指南]----------------------------" . date("Y-m-d h:i:s"));
            secho("EX", "异常消息：" . $e->getMessage());
            print_context($this->getContext());
            secho("EX", "--------------------------------------------------------------");
            $this->log($e->getMessage() . "\n" . $e->getTraceAsString(), Logger::ERROR);
        }
        //可以重写的代码
        if ($handle == null) {
            switch ($this->request_type) {
                case SwooleMarco::HTTP_REQUEST:
                    $e->request = $this->request;
                    $data = get_instance()->getWhoops()->handleException($e);
                    $this->http_output->setStatusHeader(500);
                    $this->http_output->end($data);
                    break;
                case SwooleMarco::TCP_REQUEST:
                    $this->send($e->getMessage());
                    break;
            }
        } else {
            \co::call_user_func($handle, $e);
        }
    }

    /**
     * 返回json格式
     */
    public function jsonend($code = '', $message = '', $data = '', $gzip = true)
    {
        $output = array('code' => $code, 'message' => $message, 'data' => $data);
        if (!$this->canEnd()) {
            return;
        }
        if (!get_instance()->config->get('http.gzip_off', false)) {
            //低版本swoole的gzip方法存在效率问题
//            if ($gzip) {
//                $this->response->gzip(1);
//            }
            //压缩备用方案
            /* if ($gzip) {
              $this->response->header('Content-Encoding', 'gzip');
              $this->response->header('Vary', 'Accept-Encoding');
              $output = gzencode($output . " \n", 9);
              } */
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'application/json; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }

        $this->response->end($output);
        $this->endOver();
    }






}
