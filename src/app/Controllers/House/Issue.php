<?php

/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2018/7/23
 * Time: 10:41
 */

namespace app\Controllers\House;

use app\Services\Log\AsyncFile;
use cmiot\OneNetApi;
use cmiot\Util;

/**
 * 下发指令
 * Class Issue
 * @package app\Controllers\House
 */
class Issue extends Base
{

    protected $EquipmentListsModel;
    protected $RemoteFileModel;
    protected $EquipmentUpgradeLog;
    protected $EquipmentUpgradeLogEquipment;
    protected $EquipmentLibraryModel;
    protected $device_sn;
    protected $Achievement;
    protected $OneNetApi;
    protected $device_protocol_version;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->EquipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->RemoteFileModel = $this->loader->model('RemoteFileModel', $this);
//        $this->EquipmentUpgradeLog = $this->loader->model('EquipmentUpgradeLog', $this);
        $this->EquipmentUpgradeLogEquipment = $this->loader->model('EquipmentUpgradeLogEquipment', $this);
        $this->EquipmentLibraryModel = $this->loader->model('EquipmentLibraryModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        //10进制设备编号，下发为16进制编号,总长度20
        if (!is_array($this->parm['sn']) && strlen($this->parm['sn']) > 0) {
            $device_library = $this->EquipmentLibraryModel->getOne(['device_no' => $this->parm['sn']], 'protocol_version');
            $this->device_protocol_version = empty($device_library['protocol_version']) ? 1 : $device_library['protocol_version'];//硬件协议版本硬件协议版本 1:2G主板(2023年以前的主板)   2:刷卡版 3:4G主板(新4G主板(中移平台))
            if ($this->device_protocol_version == 3) {
                $this->device_sn = $this->parm['sn'];
            } else {
                $this->device_sn = $this->dechexBin($this->parm['sn'], 20);//2G主板通信需要转
            }

        }
        /************************中移****************/
        require_once APP_DIR . '/Library/cmiot/OneNetApi.php';
        $this->OneNetApi = get_instance()->OneNetApi;
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 绑定套餐-02指令
     * @description
     * @method POST
     * @url House/Issue/bindingPackage
     * @param sn 必选 int 设备编号
     * @param working_mode 必选 int 工作模式,0:流量模式,1:时长模式
     * @param filter_element_max 必选 array 滤芯最大值（五个）,一维数组
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_bindingPackage()
    {
        dump('进入绑定套餐');
        $sn = $this->parm['sn'] ?? '';
        $working_mode = $this->parm['working_mode'] ?? '';
        $filter_element_max = $this->parm['filter_element_max'] ?? [];
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (strlen($working_mode) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：working_mode');
        }
        if ($working_mode != 0 && $working_mode != 1) {
            return $this->jsonend(-1000, '参数错误：working_mode');
        }
        if (empty($filter_element_max)) {
            return $this->jsonend(-1000, '缺少必要参数：filter_element_max');
        }
        if (count($filter_element_max) != count($filter_element_max, 1) && count($filter_element_max) != 5) {
            return $this->jsonend(-1000, '参数错误：filter_element_max');
        }
        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            $this->redis->del('4g_library:' . $this->parm['sn']);
            $this->redis->del('device_heartbeat:' . $this->parm['sn']);
            $result = $this->OneNetApi->packageSet($this->parm['sn'], $working_mode, $filter_element_max);
            return $this->jsonend(1000, '发送成功');
        }
        //获取设备信息
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn], '*');
        if (empty($equipmentInfo)) {
            return $this->jsonend(-1000, '设备不存在1，设备编号：' . $sn);
        }
        //滤芯值转换
        $tmp = $this->dechexBin($working_mode, 2);
        foreach ($filter_element_max as $key => $value) {
            $tmp .= $this->dechexBin($value, 4);
        }
        # TODO 获取设备版本 2025-2-6 HCP
        # TODO 2000版本 需要更换下发方式
        $is_line = $this->queryDeviceProperty($this->device_sn);
        dump('2000版本:'.$is_line);
        if($is_line){
            dump('2000版本');
            # 2000版本 先下发套餐 再数据同步
            # 下发套餐
            $this->OneNetApi->packageSet($this->parm['sn'], $working_mode, $filter_element_max);
            # 数据同步
            $this->OneNetApi->usedTimeSet($this->parm['sn'],$equipmentInfo['used_days'] ?? 1,$equipmentInfo['remaining_days'] ?? 365,$equipmentInfo['used_traffic'] ?? 0,$equipmentInfo['remaining_traffic'] ?? 100000000);
            //return $this->jsonend(1000, '发送成功');
        }
        dump('正常版本');
        //      设备号|指令|工作模式|滤芯
        $data = $this->device_sn . '02' . $tmp;
        $this->sendToUid($this->device_sn, $data);
        //获取iccid
        $command = $this->device_sn . '8C' . '01';
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 下发屏幕状态-03|04指令
     * @description
     * @method POST
     * @url House/Issue/screen_status
     * @param sn 必选 int 设备编号
     * @param screen_status 必选 int 屏幕状态,0:开,1:关闭
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_screen_status()
    {
        $sn = $this->parm['sn'] ?? '';
        $screen_status = $this->parm['screen_status'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (strlen($screen_status) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：screen_status');
        }
        if ($screen_status != 0 && $screen_status != 1) {
            return $this->jsonend(-1000, '参数错误：screen_status');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        //获取设备信息
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn], 'switch_machine');
        if (empty($equipmentInfo)) {
            return $this->jsonend(-1000, '设备不存在，设备编号：' . $sn);
        }
        if ($equipmentInfo['switch_machine'] == 0) {
            return $this->jsonend(-1000, '设备已关机,无法进行操作，设备编号：' . $sn);
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            $param = $this->parm;
            $result = $this->OneNetApi->screenSwitch($param['sn'], intval($param['screen_status']));
            $this->EquipmentListsModel->updateEquipmentLists(['screen_status' => intval($param['screen_status'])], ['device_no' => $sn]);
            return $this->jsonend(1000, '发送成功');
        }


        //打开和关闭指令不同
        if ($screen_status == 0) {
            $command = '04';
        } else {
            $command = '03';
        }

        $data = $this->device_sn . $command . '00';
        $this->sendToUid($this->device_sn, $data);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 下发开关机指令-05|06指令
     * @description
     * @method POST
     * @url House/Issue/power_off
     * @param sn 必选 int 设备编号
     * @param device_status 必选 int 开关机,0:开,1:关闭
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_power_off()
    {
        $sn = $this->parm['sn'] ?? '';
        $device_status = $this->parm['device_status'] ?? '';
        $is_manager = $this->parm['is_manager'] ?? 0;//是否是管理员 0否 1是
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (strlen($device_status) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：device_status');
        }
        if ($device_status != 0 && $device_status != 1) {
            return $this->jsonend(-1000, '参数错误：device_status');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        //获取设备信息
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn], 'shutdown_state,equipment_id');
        if (empty($equipmentInfo)) {
            return $this->jsonend(-1000, '设备不存在，设备编号：' . $sn);
        }
        //如果不是管理员并且设备处于停机状态将不能进行开关机
//        if ($is_manager == 0 && $equipmentInfo['shutdown_state'] == 1) {
//            return $this->jsonend(-1000, '抱歉!设备已停机!如有疑问请联系客服人员');
//        }
        //如果是停机,修改停机状态
        $edit_data = [];
        if (isset($this->parm['shutdown_state']) && $equipmentInfo['shutdown_state'] != $this->parm['shutdown_state']) {
            if ($this->parm['shutdown_state'] == 0) {//恢复
                $edit_data['shutdown_state'] = 0;
            } else {//停机
                $edit_data['shutdown_time'] = time();
                $edit_data['shutdown_state'] = 1;
            }
            $this->EquipmentListsModel->updateEquipmentLists($edit_data, ['equipment_id' => $equipmentInfo['equipment_id']]);
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            $param = $this->parm;
            // 0-开启制水,1-关闭制水
            $result = $this->OneNetApi->openWaterProduction($param['sn'], $param['device_status']);
            // 开关机，0：关机，1：开机
            $this->EquipmentListsModel->updateEquipmentLists(['switch_machine' => (($param['device_status'] == 1) ? 0 : 1)], ['equipment_id' => $equipmentInfo['equipment_id']]);
            return $this->jsonend(1000, '发送成功');
        }

        //打开和关闭指令不同
        if ($device_status == '0') {
            $command = '06';
        } else {
            $command = '05';
        }
        $data = $this->device_sn . $command . '00';
        $this->sendToUid($this->device_sn, $data);
        return $this->jsonend(1000, '发送成功', $edit_data);
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 强制冲洗-07指令
     * @description
     * @method POST
     * @url House/Issue/mandatory_rinse
     * @param sn 必选 int 设备编号
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_mandatory_rinse()
    {
        $sn = $this->parm['sn'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        //获取设备信息
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn], 'switch_machine');
        if (empty($equipmentInfo)) {
            return $this->jsonend(-1000, '设备不存在，设备编号：' . $sn);
        }
        if ($equipmentInfo['switch_machine'] == 0) {
            return $this->jsonend(-1000, '设备已关机,无法进行强制冲洗，设备编号：' . $sn);
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            $param = $this->parm;
            $result = $this->OneNetApi->washSwitch($param['sn']);
            return $this->jsonend(1000, '发送成功');
        }


        $data = $this->device_sn . '07' . '00';
        $this->sendToUid($this->device_sn, $data);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 数据同步-08指令
     * @description
     * @method POST
     * @url House/Issue/data_sync
     * @param sn 必选 int 设备编号
     * @param used_days 必选 int 已用天数
     * @param remaining_days 必选 int 剩余天数
     * @param used_traffic 必选 int 已用流量
     * @param remaining_traffic 必选 int 剩余流量
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_data_sync()
    {
        $sn = $this->parm['sn'] ?? '';
        $used_days = $this->parm['used_days'] ?? '';
        $remaining_days = $this->parm['remaining_days'] ?? 0;
        $used_traffic = $this->parm['used_traffic'] ?? '';
        $remaining_traffic = $this->parm['remaining_traffic'] ?? '';

        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        if (strlen($used_days) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：used_days');
        }
        if ($used_days < 0) {
            return $this->jsonend(-1000, '参数错误：used_days');
        }
        if (strlen($remaining_days) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：remaining_days');
        }
//        if ($remaining_days < 0) {
//            return $this->jsonend(-1000, '参数错误：remaining_days');
//        }
        if (strlen($used_traffic) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：used_traffic');
        }
        if ($used_traffic < 0) {
            return $this->jsonend(-1000, '参数错误：used_traffic');
        }
        if (strlen($remaining_traffic) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：remaining_traffic');
        }
        if ($remaining_traffic < 0) {
            return $this->jsonend(-1000, '参数错误：remaining_traffic');
        }

        if ($this->device_protocol_version == 3) {
            $this->redis->del('4g_library:' . $this->parm['sn']);
            $this->redis->del('device_heartbeat:' . $this->parm['sn']);
           // $result = $this->OneNetApi->usedTimeSet($this->parm['sn'], intval($used_days), intval($remaining_days), intval($used_traffic), intval($remaining_traffic));
            $this->OneNetApi->activation($this->parm['sn'], intval($remaining_days), intval($used_days), intval($remaining_traffic), intval($used_traffic));
            return $this->jsonend(1000, '发送成功');
        }

        $command = $this->device_sn . '08' . $this->dechexBin($used_days, 4);
        $command .= $this->dechexBin($remaining_days, 4);
        $command .= $this->dechexBin($used_traffic, 8);
        $command .= $this->dechexBin($remaining_traffic, 8);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');


    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 用时同步-BA|BB指令
     * @description
     * @method POST
     * @url House/Issue/time_sync
     * @param sn 必选 int 设备编号
     * @param type 必选 int 类型,1:已用天数,2:剩余天数
     * @param value 必选 int 数值
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_time_sync()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        $type = $this->parm['type'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        if (empty($type)) {
            return $this->jsonend(-1000, '缺少必要参数：key');
        }
        if ($type != 1 && $type != 2) {
            return $this->jsonend(-1000, '类型错误');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value < 0) {
            return $this->jsonend(-1000, '同步数值必须大于0');
        }
        //获取设备信息
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn], '*');
        if (empty($equipmentInfo)) {
            return $this->jsonend(-1000, '设备不存在，设备编号：' . $sn);
        }
        //判断设备工作模式，非时长模式不能同步
        if ($equipmentInfo['working_mode'] == 0) {
            return $this->jsonend(-1000, '该设备工作模式为：流量模式，不能用时同步');
        }

        if ($type == 1) {
            $command = 'BA';
        } else {
            $command = 'BB';
        }

        $data = $this->device_sn . $command . $this->decto_bin($value, 4);
        $this->sendToUid($this->device_sn, $data);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 请求设备发送心跳包-0D指令
     * @description
     * @method POST
     * @url House/Issue/heartbeat
     * @param sn 必选 string 设备编号
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_heartbeat()
    {
        $sn = $this->parm['sn'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        if ($this->device_protocol_version == 3) {
            return $this->jsonend(1000, '中移不需要下发');
        }
        //获取设备信息
//        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn], '*');
//        if (empty($equipmentInfo)) {
//            return $this->jsonend(-1000, '设备不存在，设备编号：' . $sn);
//        }
        $data = $this->device_sn . '0D' . '00';
        $this->sendToUid($this->device_sn, $data);
        $this->redis->set('getLeartLock:' . $sn, true);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 滤芯复位与修改-0E指令
     * @description
     * @method POST
     * @url House/Issue/filter_element_reset_modification
     * @param sn 必选 int 设备编号
     * @param type 必选 int 类型,1:复位,2:修改
     * @param key 必选 int 第几级滤芯（当type为1时，0表示均不复位,6表示全部复位，反之，1表示第一级，2表示第二级....最大数值为5）
     * @param value 必选 int 修改剩余值（修改时必须）
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_filter_element_reset_modification()
    {
        $sn = $this->parm['sn'] ?? '';
        $type = $this->parm['type'] ?? '';
        $key = $this->parm['key'] ?? '';
        $value = $this->parm['value'] ?? -1;
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (empty($type)) {
            return $this->jsonend(-1000, '缺少必要参数：type');
        }
        if ($type != 1 && $type != 2) {
            return $this->jsonend(-1000, '参数错误：type');
        }
        if (strlen($key) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：key');
        }
        if ($type == 2 && $key > 5) {
            return $this->jsonend(-1000, '参数错误：key');
        }
        if ($type == 2 && $value < 0) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        //获取设备信息
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn], '*');
        if (empty($equipmentInfo)) {
            return $this->jsonend(-1000, '设备不存在，设备编号：' . $sn);
        }

        //判断类型,1:复位，2：修改
        if ($type == 1) {
            $key = $this->decto_bin($key, 2);
            if ($key == '00') {
                $command = $this->device_sn . '0E' . '00' . '0000';
            } else {
                $command = $this->device_sn . '0E' . $key . 'FFFF';
            }
            $this->sendToUid($this->device_sn, $command);
            return $this->jsonend(1000, '发送成功');
        } else {
            $key = $this->decto_bin($key + 6, 2);
            $command = $this->device_sn . '0E' . $key . $this->decto_bin($value, 4);
            $this->sendToUid($this->device_sn, $command);
            return $this->jsonend(1000, '发送成功');
        }
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 切换工作模式-0F指令
     * @description
     * @method POST
     * @url House/Issue/mode_switch
     * @param sn 必选 int 设备编号
     * @param working_mode 必选 int 工作模式,0:流量模式,1:时长模式
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_mode_switch()
    {
        $sn = $this->parm['sn'] ?? '';
        $working_mode = $this->parm['working_mode'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (strlen($working_mode) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：working_mode');
        }
        if ($working_mode != 0 && $working_mode != 1) {
            return $this->jsonend(-1000, '参数错误：working_mode');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . '0F' . $this->decto_bin($working_mode, 2);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 恢复出厂设置-10指令
     * @description
     * @method POST
     * @url House/Issue/factory_data_reset
     * @param sn 必选 int 设备编号
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_factory_data_reset()
    {
        $sn = $this->parm['sn'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);

        if (empty($sn)) {
            return $this->jsonend(-1000, '设备不存在');
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            $param = $this->parm;
            $result = $this->OneNetApi->factoryReset($param['sn'], 1);
            $this->redis->del('4g_library:' . $this->parm['sn']);
            $this->redis->del('device_heartbeat:' . $this->parm['sn']);
            $this->redis->del('device_heartbeat:' . $sn);
            $data = [
                'device_status' => 1,
                'remaining_days' => 0
            ];
            // 设置为待激活状态
            $this->EquipmentListsModel->updateEquipmentLists($data, ['device_no' => $sn]);
            return $this->jsonend(1000, '发送成功');
        }

        $command = $this->device_sn . '10' . '00';
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 修改强制冲洗时间-11指令
     * @description
     * @method POST
     * @url House/Issue/modify_mandatory_flushing_times
     * @param sn 必选 string 设备编号
     * @param value 必选 int 数值
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_modify_mandatory_flushing_times()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value < 0) {
            return $this->jsonend(-1000, '强制冲洗时间必须大于等于0');
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            $param = $this->parm;
            $result = $this->OneNetApi->washTimeSet($param['sn'], intval($value));

            // 设置冲洗时间
            $this->EquipmentListsModel->updateEquipmentLists(['forced_flushing_time' => $value], ['device_no' => $sn]);
            return $this->jsonend(1000, '发送成功');
        }

        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . '11' . $this->decto_bin($value, 2);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 定时冲洗参数修改-12指令
     * @description
     * @method POST
     * @url House/Issue/modified_timed_washing
     * @param sn 必选 string 设备编号
     * @param value 必选 int 数值(s)
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_modified_timed_washing()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value < 0) {
            return $this->jsonend(-1000, '定时冲洗参数必须大于0');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        //获取设备信息
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn], 'switch_machine');
        if (empty($equipmentInfo)) {
            return $this->jsonend(-1000, '设备不存在，设备编号：' . $sn);
        }
        if ($equipmentInfo['switch_machine'] == 0) {
            return $this->jsonend(-1000, '设备已关机,无法进行强制冲洗，设备编号：' . $sn);
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            //获取设备信息
            $this->EquipmentListsModel->updateEquipmentLists(['wash_regularly' => $value * 60 * 60], ['device_no' => $sn]);
            return $this->jsonend(1000, '发送成功');
        }

        $value = round($value * 60 * 60);//转换为s,
        $command = $this->device_sn . '12' . $this->decto_bin($value, 6);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 检修参数修改-13指令
     * @description
     * @method POST
     * @url House/Issue/modify_maintenance_parameters
     * @param sn 必选 string 设备编号
     * @param value 必选 int 数值(s)
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_modify_maintenance_parameters()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value < 0) {
            return $this->jsonend(-1000, '检修状态时长必须大于0');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $value = round($value * 60 * 60);//转换为s
        if ($this->device_protocol_version == 3) {
            $param = $this->parm;
            $this->OneNetApi->setMaintenanceTime($param['sn'], intval($value));
            return $this->jsonend(1000, '发送成功');
        }
        $command = $this->device_sn . '13' . $this->decto_bin($value, 6);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 控制参数修改1(TDS值设置)-14指令
     * @description
     * @method POST
     * @url House/Issue/modify_control_parameter_one
     * @param sn 必选 string 设备编号
     * @param key 必选 string 类型,1:纯水,2:原水
     * @param switch 必选 string 开关,0:关,1:开
     * @param value 必选 int 默认值
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_modify_control_parameter_one()
    {
        $sn = $this->parm['sn'] ?? '';
        $key = $this->parm['key'] ?? '';
        $switch = $this->parm['switch'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (empty($key)) {
            return $this->jsonend(-1000, '缺少必要参数：key');
        }
        if ($key != 1 && $key != 2) {
            return $this->jsonend(-1000, '参数错误：key');
        }
        if ($switch == '') {
            return $this->jsonend(-1000, '缺少必要参数：switch');
        }
        if ($switch != 0 && $switch != 1) {
            return $this->jsonend(-1000, '参数错误：switch');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value < 0) {
            return $this->jsonend(-1000, '默认值大于等于0');
        }
        if ($key == 1) {
            $key = '01';
        } else {
            $key = '02';
        }
        if ($switch == 0) {
            $switch = '00';
        } else {
            $switch = '01';
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . '14' . $key . $switch . $this->decto_bin($value, 4);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 控制参数修改2(联网参数修改)-15指令
     * @description
     * @method POST
     * @url House/Issue/modify_control_parameter_two
     * @param sn 必选 string 设备编号
     * @param key 必选 string 类型，1：心跳间隔，2：断网重连
     * @param value 必选 int 时长
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_modify_control_parameter_two()
    {
        $sn = $this->parm['sn'] ?? '';
        $key = $this->parm['key'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (empty($key)) {
            return $this->jsonend(-1000, '缺少必要参数：key');
        }
        if ($key != 1 && $key != 2) {
            return $this->jsonend(-1000, '参数错误：key');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value < 0) {
            return $this->jsonend(-1000, '时长大于等于0');
        }
        if ($key == 1) {
            $key = '01';
        } else {
            $key = '02';
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . '15' . $key . $this->decto_bin($value, 4);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 开启关闭测试模式-16指令
     * @description
     * @method POST
     * @url House/Issue/switch_test_mode
     * @param sn 必选 string 设备编号
     * @param value 必选 int 开关，0：开启，1：关闭
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_switch_test_mode()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (strlen($value) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value != 0 && $value != 1) {
            return $this->jsonend(-1000, '参数错误：value');
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            // $this->parm['value']：0-打开，1-关闭
            $value = ($value == 0) ? 1 : 2; // 1-进入测试模式，2-退出测试模式
            $result = $this->OneNetApi->overhaulTestSet($sn, $value);
            return $this->jsonend(1000, '发送成功');
        }

        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . '16' . $this->decto_bin($value, 2);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 电脑板时间同步2-18指令
     * @description
     * @method POST
     * @url House/Issue/computer_board_time_sync
     * @param sn 必选 string 设备编号
     * @param value 必选 int 时间戳，默认系统时间
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_computer_board_time_sync()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value < 0) {
            return $this->jsonend(-1000, '参数错误：value');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
//        $command = $sn . '18' . $this->decto_bin($value, 10);
        $command = $this->device_sn . '18' . $this->strPad(dechex($value), 10, '0');
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 下发获取滤芯状态-19指令
     * @description
     * @method POST
     * @url House/Issue/filter_element
     * @param sn 必选 string 设备编号
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_filter_element()
    {
        $sn = $this->parm['sn'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }

        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        if (empty($sn)) {
            return $this->jsonend(-1000, '设备不存在');
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            $param = $this->parm;
            $result = $this->OneNetApi->getFilterState($param['sn']);

            return $this->jsonend(1000, '发送成功');
        }

        $command = $this->device_sn . '1901';
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 用水量同步-AA|AB指令
     * @description
     * @method POST
     * @url House/Issue/water_sync
     * @param sn 必选 string 设备编号
     * @param key 必选 int 类型，1：已用流量，2：剩余流量
     * @param value 必选 否 数值
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_water_sync()
    {
        $sn = $this->parm['sn'] ?? '';
        $key = $this->parm['key'] ?? '';
        $value = $this->parm['value'] ?? '';

        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (empty($key)) {
            return $this->jsonend(-1000, '缺少必要参数：key');
        }
        if ($key != 1 && $key != 2) {
            return $this->jsonend(-1000, '要参数错误：key');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value < 0) {
            return $this->jsonend(-1000, '参数错误：value');
        }
        if ($key == 1) {
            $key = 'AA';
        } else {
            $key = 'AB';
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . $key . $this->decto_bin($value, 8);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 远程升级请求(获取版本号)-1A
     * @description
     * @method POST
     * @url House/Issue/version
     * @param sn 必选 string 设备编号
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_version()
    {
        $sn = $this->parm['sn'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $device_info = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn]);
        if (empty($device_info)) {
            return $this->jsonend(-1000, '设备编号不存在，设备编号：' . $sn);
        }
        if (empty($device_info['version'])) {
            $device_info['version'] = 'v1.0';
        }
        $hex_version = $this->String2Hex($device_info['version']);
        $command = $this->device_sn . '1A' . $hex_version;
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 远程固件下发与接收-2A|3A|4A指令
     * @description
     * @method POST
     * @url House/Issue/switch_screen_display_mode
     * @param sn 必选 string 设备编号
     * @param class 必选 int 类型，1：单台设备，2：多台设备（必须是一位数组，如：['sn1','sn2',.....]）
     * @param file_id 必选 int 下发文件ID（远程文件数据表
     * @param first 必选 bool 是否首次下发，默认true，若该参数为false，参数：sn必须
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_remote_firmware()
    {
        $class = $this->parm['class'] ?? 1;
        $sn = $this->parm['sn'] ?? '';
        $file_id = $this->parm['file_id'] ?? '';
        $first = $this->parm['first'] ?? true;
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        //是否首次下发
        if ($first) {
            if (empty($class)) {
                return $this->jsonend(-1000, '缺少必要参数：class');
            }
            if ($class != 1 && $class != 2) {
                return $this->jsonend(-1000, '参数错误：class');
            }
            if ($class == 2 && !is_array($sn)) {
                return $this->jsonend(-1000, '参数错误：sn');
            }
            if (empty($file_id)) {
                return $this->jsonend(-1000, '缺少必要参数：file_id');
            }
            if ($class == 1) {
                //判断设备
                $device_info = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn]);
                if (empty($device_info)) {
                    return $this->jsonend(-1000, '设备不存在，设备编号：' . $sn);
                }
                if ($device_info['status'] == 1) {
                    return $this->jsonend(-1000, '设备不在线，离现时间：' . date('Y-m-d H:i:s', $device_info['offline_time']));
                }
                $check = $this->remote_file($file_id);
                if ($check['check']) {
                    //获取分包结果
                    $result = $this->subcontract($check['data']['url'], WWW . $check['data']['url']);
                    if ($class == 1) {
                        //起始帧
                        //          设备号|指令|数据帧
                        $command = $this->device_sn . '2A' . $result[0];
                        $this->sendToUid($this->device_sn, $command);
                    }
                    //将结果存储到redis
                    $data = [
                        'data' => $result,                             //分包结果
                        'version' => $check['data']['version'],        //当前版本
                        'error' => 0,                                  //失败次数（失败三次后，不在下发）
                        'total' => count($result),                     //共需要发送几次
                        'current_time' => time(),                      //当前发送帧的时间
                        'current' => 0                                 //当前发送分包的KEY,如果失败，将再次发送
                    ];
                    $this->redis->set('remoteFirmware:' . $sn, json_encode($data));

                    //升级记录
                    $log = [
                        'error' => 0,
                        'equipment_id' => $device_info['equipment_id'],
                        'old_version' => $device_info['version'],
                        'new_version' => $check['data']['version'],
                        'file_id' => $file_id,
                        'status' => 1,
                        'create_time' => time(),
                        'end_time' => 0,
                    ];
                    $this->EquipmentUpgradeLogEquipment->add($log);

                    return $this->jsonend(1000, '发送成功');
                } else {
                    return $this->jsonend(-1000, '读取远程文件失败,请稍候再试');
                }
            } else {
                //多个设备，循环下发
                $check = $this->remote_file($file_id);
                if ($check['check']) {
                    //获取分包结果
                    $result = $this->subcontract($check['data']['url'], WWW . $check['data']['url']);

                    //判断设备
                    foreach ($sn as $key => $value) {
                        //只需验证设备编号是否存在，离线时间
                        $device = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $value]);
                        if (empty($device)) {
                            return $this->jsonend(-1000, '设备不存在，设备编号：' . $value);
                        }
                        if ($device['status'] == 1) {
                            return $this->jsonend(-1000, $value . '设备不在线，离现时间：' . date('Y-m-d H:i:s', $device['offline_time']));
                        }

                        //起始帧
                        //          设备号|指令|数据帧
                        $command = $this->dechexBin($value, 20) . '2A' . $result[0];
                        $this->sendToUid($this->dechexBin($value, 20), $command);
                        //将结果存储到redis
                        $data = [
                            'data' => $result,                             //分包结果
                            'version' => $check['data']['version'],        //当前版本
                            'error' => 0,                                  //失败次数（失败三次后，不在下发）
                            'total' => count($result),                     //共需要发送几次
                            'current_time' => time(),                      //当前发送帧的时间
                            'current' => 0                                 //当前发送分包的KEY,如果失败，将再次发送
                        ];
                        $this->redis->set('remoteFirmware:' . $value, json_encode($data));

                        //升级记录
                        $log = [
                            'error' => 0,
                            'equipment_id' => $device['equipment_id'],
                            'old_version' => $device['version'],
                            'new_version' => $check['data']['version'],
                            'file_id' => $file_id,
                            'status' => 1,
                            'create_time' => time(),
                            'end_time' => 0,
                        ];
                        $this->EquipmentUpgradeLogEquipment->add($log);

                        //间隔几毫秒
                        sleepCoroutine(10);

                    }
                    return $this->jsonend(1000, '发送成功');
                } else {
                    return $this->jsonend(-1000, '读取远程文件失败,请稍候再试');
                }
            }
        }

        //判断redis
        //获取redis
        $redis_info = $this->redis->get('remoteFirmware:' . $sn);
        if ($redis_info) {
            $redis_info = json_decode($redis_info, 1);
            if ($redis_info['current'] == $redis_info['total']) {
                return $this->jsonend(-1000, '数据包已下发完毕');
            }
            if ($redis_info['error'] > 3) {
                return $this->jsonend(-1000, '数据包已下发失败超过3次，停止下发');
            }
            //判断结束帧
            if (($redis_info['current'] + 1) == ($redis_info['total'] - 1)) {
                $instruct = '4A';
            } else {
                //数据帧
                $instruct = '3A';
            }
            //          设备号|指令|数据帧
            $command = $this->device_sn . $instruct . $redis_info['data'][$redis_info['current'] + 1];
            $this->sendToUid($this->device_sn, $command);
            //更改redis
            //将结果存储到redis
            $data = [
                'data' => $redis_info['data'], //分包结果
                'version' => $redis_info['version'], //当前版本
                'error' => $redis_info['error'], //失败次数（失败三次后，不在下发）
                'total' => $redis_info['total'], //共需要发送几次
                'current_time' => time(),                      //当前发送帧的时间
                'current' => $redis_info['current'] + 1       //当前发送分包的KEY,如果失败，将再次发送
            ];
            $this->redis->set('remoteFirmware:' . $sn, json_encode($data));
            return $this->jsonend(1000, '发送成功');
        }
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 屏幕显示模式切换-5A指令
     * @description
     * @method POST
     * @url House/Issue/switch_screen_display_mode
     * @param sn 必选 string 设备编号
     * @param value 必选 int 值,0:已用天数,1:剩余天数,2:已用流量,3:剩余流量.(注:0和1,2和3不能同时存在）,一维数组如：[0,2]或者[0,3]
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_switch_screen_display_mode()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? [];
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if (count($value) != count($value, 1)) {
            return $this->jsonend(-1000, '参数错误：value | It has to be an array');
        }
        if (count($value) != 2) {
            return $this->jsonend(-1000, '参数错误：value | An array must have two values');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        //获取设备信息
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $sn], 'switch_machine');
        if (empty($equipmentInfo)) {
            return $this->jsonend(-1000, '设备不存在，设备编号：' . $sn);
        }
        if ($equipmentInfo['switch_machine'] == 0) {
            return $this->jsonend(-1000, '设备已关机,无法进行强制冲洗，设备编号：' . $sn);
        }
        asort($value);
        if (empty(array_diff($value, [0, 1]))) {
            return $this->jsonend(-1000, '参数错误：value < 0 OR 1>');
        }
        if (empty(array_diff($value, [2, 3]))) {
            return $this->jsonend(-1000, '参数错误：value < 2 OR 3>');
        }
        $data = '';
        foreach ($value as $k => $v) {
            if (!in_array($v, [0, 1, 2, 3])) {
                return $this->jsonend(-1000, '参数错误：value');
            }
            $data .= $this->decto_bin($v, 2);
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            $param = $this->parm;
            $result = $this->OneNetApi->showContentsSet($param['sn'], intval($value[0]), intval($value[1]));
            return $this->jsonend(1000, '发送成功');
        }

        $command = $this->device_sn . '5a' . $data;
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');

    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 检修状态切换-6A指令
     * @description
     * @method POST
     * @url House/Issue/maintenance_state_switching
     * @param sn 必选 string 设备编号
     * @param value 必选 int 状态,0:解除检修,1:进入检修
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_maintenance_state_switching()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (strlen($value) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value != 0 && $value != 1) {
            return $this->jsonend(-1000, '参数错误：value');
        }

        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            // $this->parm['value']：1-打开，0-关闭
            $value = ($value == 1) ? 3 : 4; // 3-进入检修模式，4-退出检修模式
            $result = $this->OneNetApi->overhaulTestSet($sn, $value);
            return $this->jsonend(1000, '发送成功');
        }

        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . '6A' . $this->decto_bin($value, 2);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');

    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 远程重启-7A指令
     * @description
     * @method POST
     * @url House/Issue/remote_restart
     * @param sn 必选 string 设备编号
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_remote_restart()
    {
        $sn = $this->parm['sn'] ?? '';
        //如果是中移4G主板
        if ($this->device_protocol_version == 3) {
            $result = $this->OneNetApi->remoteReset($sn);
            return $this->jsonend(1000, '发送成功');
        }
        $command = $this->device_sn . '7A' . $this->decto_bin(1, 2);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 下次启动设备时是否运行新固件-8A指令
     * @description
     * @method POST
     * @url House/Issue/run_new_firmware
     * @param sn 必选 string 设备编号
     * @param value 必选 int 指令,0:不运行,1:运行新固件
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_run_new_firmware()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (strlen($value) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value != 0 && $value != 1) {
            return $this->jsonend(-1000, '参数错误：value');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . '8A' . $this->decto_bin($value, 2);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 运行固件切换-9A指令
     * @description
     * @method POST
     * @url House/Issue/firmware_switch
     * @param sn 必选 string 设备编号
     * @param value 必选 int 指令,0运行BootLoader 1运行A区固件 2运行B区固件
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_firmware_switch()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (strlen($value) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value != 0 && $value != 1 && $value != 2) {
            return $this->jsonend(-1000, '参数错误：value');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . '9A' . $this->dechexBin($value, 2);
        $this->sendToUid($this->device_sn, $command);
        //切换后重启
        $this->sendToUid($this->device_sn, $this->device_sn . '7A' . $this->decto_bin(1, 2));
        return $this->jsonend(1000, '发送成功');
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 关闭开启蜂鸣器（设备收到数据时鸣叫）-2C指令
     * @description
     * @method POST
     * @url House/Issue/buzzer
     * @param sn 必选 string 设备编号
     * @param value 必选 int 0:关,1:开
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author ligang
     * @date 2018/7/23 11:52
     */
    public function http_buzzer()
    {
        $sn = $this->parm['sn'] ?? '';
        $value = $this->parm['value'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if (strlen($value) == 0) {
            return $this->jsonend(-1000, '缺少必要参数：value');
        }
        if ($value != 0 && $value != 1) {
            return $this->jsonend(-1000, '参数错误：value');
        }
        if (empty($this->parm['sn'])) {
            return $this->jsonend(-1000, '缺少设备编号');
        }
        $sn = $this->EquipmentListsModel->getDeviceNo($sn);
        $command = $this->device_sn . '2C' . $this->dechexBin($value, 2);
        $this->sendToUid($this->device_sn, $command);
        var_dump('<<<蜂鸣器<<<蜂鸣器下发' . $value . '设备编号：' . $this->device_sn);
        return $this->jsonend(1000, '发送成功');
    }

    /**
     * 判断远程文件
     * @param int $file_id 远程文件ID
     * @return array
     * @date 2018/8/6 10:13
     * @author ligang
     */
    protected function remote_file(int $file_id)
    {
        //判断下发文件
        $remote_file = $this->RemoteFileModel->findRemoteFile(['id' => $file_id, 'class' => 1]);
        if (empty($remote_file)) {
            return $this->jsonend(-1000, '不存在远程文件');
        }
        if ($remote_file['is_issue'] == 1) {
            return $this->jsonend(-1000, '远程文件已被下发过，请更改下发状态后操作');
        }
        if ($remote_file['is_delete'] == 1) {
            return $this->jsonend(-1000, '远程文件已被删除');
        }

        //如果远程文件不存在，就去下载
        if (!file_exists(WWW . $remote_file['url'])) {
            //下载文件到本地
            $check = $this->curlDownFile(get_instance()->config['qiniu']['qiniu_url'] . $remote_file['url'], WWW, $remote_file['url']);
            //防止获取远程文件失败
            sleepCoroutine(2);

        } else {
            $check = true;
        }

        $data = [
            'data' => $remote_file,
            'check' => $check
        ];
        return $data;
    }

    /**
     * 分包
     * @param string $url 文件地址
     * @param int $Byte 字节
     * @param string $file_name 文件地址（含文件名）
     * @return array
     * @date 2018/8/1 9:27
     * @author ligang
     */
    protected function subcontract(string $file_name, string $url, int $Byte = 1024)
    {
        $fh = fopen($url, "rb");
        $fileSize = filesize($url);
        $data = [];
        //构建起始帧    初始帧|数据帧序号|补码|                       文件名|          文件结束标志|文件大小        |文件大小结束标志|
        $start_frame = '01' . '00' . $this->complement(0) . $this->String2Hex($file_name) . '00' . $this->String2Hex($fileSize) . '00';
        //补位（起始帧 1 Byte |数据帧序号 1 Byte| 补码 1 Byte|文件名 n Byte| 文件结束标志 1 Byte|文件大小 n Byte| 文件大小结束标志 1 Byte|数据 128 Byte
        $start_length = 2 + 2 + 2 + strlen($this->String2Hex($file_name)) + 2 + strlen($this->String2Hex($fileSize)) + 2 + 128 * 2;
        $start_frame = $this->strPad($start_frame, $start_length, '0', STR_PAD_RIGHT);
        array_push($data, $start_frame);
        $i = 1;
        do {
            $tmp_data = fread($fh, $Byte); // 每次读取 $Byte 个字节
            if (!$tmp_data) {
                break;
            }
            //构建Ymodem协议
            //判断是否128~1024 Byte之间,不够1024补位
            //      数据帧|数据帧序号|补码|数据部分|
            $frame = '02' . $this->decto_bin($i, 2) . $this->complement($i) . bin2hex($tmp_data);
            //补位（数据部分<除开序号>1024 Byte） 1024*2 + 2 + 2 + 2
            $frame = $this->strPad($frame, 2054, '1a', STR_PAD_RIGHT);
            array_push($data, $frame);
            $i++;
        } while (true);

        //构建结束帧
        //      结束帧|数据帧序号|补码|
        $end_frame = '01' . '00' . 'FF';
        //补位（结束帧<除开序号>128 Byte） 128*2 + 2
        $end_frame = $this->strPad($end_frame, 258, '0', STR_PAD_RIGHT);
        array_push($data, $end_frame);
        return $data;
    }

    /**
     * curl 下载文件
     * @param string $img_url 下载文件地址
     * @param string $save_path 下载文件保存目录
     * @param string $filename 下载文件保存名称
     * @return bool
     */
    protected function curlDownFile($img_url, $save_path = '', $filename = '')
    {
        if (trim($img_url) == '') {
            return false;
        }
        if (trim($save_path) == '') {
            $save_path = './';
        }

        //创建保存目录
        if (!file_exists($save_path) && !mkdir($save_path, 0777, true)) {
            return false;
        }
        if (trim($filename) == '') {
            $img_ext = strrchr($img_url, '.');
            $img_exts = array('.gif', '.jpg', '.png');
            if (!in_array($img_ext, $img_exts)) {
                return false;
            }
            $filename = time() . $img_ext;
        }

        // curl下载文件
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $img_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $img = curl_exec($ch);
        curl_close($ch);

        // 保存文件到制定路径
        file_put_contents($save_path . $filename, $img);

        unset($img, $url);
        return true;
    }

    /**
     *
     * @throws \Exception
     * @date 2019/4/18 18:37
     * @author ligang
     * @RequestType GET|POST
     */
    public function http_syncRedisToDevice()
    {
        //心跳、工作状态
        if (empty($this->parm['sn'])) {
            return $this->jsonend(-1000, '缺少设备编号');
        }
        $device_no = $this->EquipmentListsModel->getDeviceNo($this->parm['sn']);

        //验证设备
        $where = [
            'device_no' => $device_no
        ];
        $field = '
            equipment_id
        ';
        $this->Achievement->table = 'equipment_lists';
        $info = $this->Achievement->findData($where, $field);
        if (empty($info)) {
            return $this->jsonend(-1000, '设备不存在');
        }
        if ($this->device_protocol_version == 3) {
            $new_data = [];
            try {
                $data = $this->OneNetApi->queryDeviceProperty($this->parm['sn']);
                if (!empty($data)) {
                $result = [];
                    foreach ($data as $item) {
                        $result[$item['identifier']] = $item['value'] ??0;
                    }
                    $this->redis->del('4g_library:' . $this->parm['sn']);
                    $this->redis->del('device_heartbeat:' . $this->parm['sn']);
                    $new_data = [
                        'signal_strength_value' => $result['CSQ'], // 信号强度
                        'remaining_days' => $result['days_remaining'], // 剩余天数
                        'used_days' => $result['days_used'], // 已用天数
                        'device_status' => $result['device_state'], // 设备状态
                        'leaking_state' => $result['leakage_state'], // 漏水状态
                        'repair_state' => $result['maintenance_status'], // 检修状态
                        'switch_machine' => ($result['poweron_status'] == 0) ? 1 : 0, // 开关机
//                        'switch_machine' => 1, // 开关机
                        'water_purification_TDS' => $result['purified_water'], // 净水TDS
                        'raw_water_TDS' => $result['raw_water'], // 原水TDS
                        'remaining_traffic' => $result['residual_flow'], // 剩余流量
                        'screen_status' => $result['screen_status'], // 屏幕状态
                        'used_traffic' => $result['used_traffic'], // 已用流量
                        'water_full_state' => $result['water_full_state'], // 水满状态
                        'water_making_state' => $result['water_production_status'], // 制水状态
                        'exsiccosis' => $result['water_shortage_state'], // 缺水状态
                        'working_mode' => $result['working_mode'], // 工作模式

                    ];
                    // 更新设备信息
                    $this->redis->del('device_heartbeat:' . $device_no);
                    $this->redis->del('work_state:' . $device_no);
                }
                //获取设备状态
                $report_device_info = $this->OneNetApi->device($device_no);
                if($report_device_info){
                    $new_data['status'] = $report_device_info['status']+1;//$report_device_info['status']设备状态，0-离线，1-在线，2-未激活
                }
                if(!empty($new_data)){
                    $this->EquipmentListsModel->updateEquipmentLists($new_data, ['device_no' => $device_no, 'equipment_id' => $info['equipment_id']]);
                }
                return $this->jsonend(1000, '同步成功');
            } catch (\Exception $e) {
//                    return $this->jsonend(-1000, '同步失败');
                return $this->jsonend(-1000, $e->getMessage());
            }
            return $this->jsonend(-1000, '同步失败');
        }
        $this->redis->del('device_heartbeat:' . $device_no);
        $this->redis->del('work_state:' . $device_no);
        $data = $this->device_sn . '0D' . '00';
        $this->sendToUid($this->device_sn, $data);
        return $this->jsonend(1000, '同步成功');
    }

    public function http_delDevice()
    {
        if (empty($this->parm['sn'])) {
            return $this->jsonend(-1000, '缺少设备编号');
        }
        $device_no = $this->EquipmentListsModel->getDeviceNo($this->parm['sn']);
        $where = [
            'device_no' => $device_no
        ];
        $this->db->begin(function () use ($where, $device_no) {
            $this->Achievement->table = 'equipment_lists';
            $this->Achievement->deleteData($where);
            $this->Achievement->table = 'equipment_library';
            $this->Achievement->updateData($where, ['device_status' => 1]);
            $this->redis->del('device_heartbeat:' . $device_no);
            $this->redis->del('work_state:' . $device_no);
        }, function ($e) {
            return $this->jsonend(-1000, '删除失败', $e->error);
        });
        return $this->jsonend(-1000, '删除成功');
    }

    /**
     * 读取log文件
     * @desc 读取文件
     * @throws \Exception
     * @date 2019/4/25 14:55
     * @author ligang
     * @RequestType POST
     */
    public function http_readFile()
    {
        $path = $this->parm['path'] ?? '';
        $filename = $this->parm['filename'] ?? '';
        $line = $this->parm['line'] ?? 30;
        try {
            AsyncFile::$log_file = LOG_DIR . '/' . $path;
            $result = AsyncFile::read($filename, $line);
        } catch (\Exception $e) {
            return $this->jsonend(-1000, '出错了，错误：' . $e->getMessage());
        }
//        $this->http_output->end($result);
        return $this->jsonend(1000, '获取成功', $result);
    }


    /**
     * showdoc
     * @catalog API文档/设备API/下发
     * @title 配置是否接烧水壶-3C指令
     * @description 2020年8月11日新增
     * @method POST
     * @url House/Issue/kettle
     * @param sn 必选 string 设备编号
     * @param status 必选 int 状态,0:未接烧水壶,1:接烧水壶
     * @param flow 必选 int 额定水量,单位ml
     * @return
     * @return_param
     * @remark
     * @number 0
     * @author lcx
     * @date 2020/8/11
     */
    public function http_kettle()
    {
        $sn = $this->parm['sn'] ?? '';
        $status = $this->parm['status'] ?? 0;
        $flow = $this->parm['flow'] ?? 1200;
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        if ($status == 1 && empty($flow)) {
            return $this->jsonend(-1000, '请设置额定水量');
        }
        $status = $this->dechexBin($status, 2);
        $flow = $this->dechexBin($flow, 4);
        $command = $this->device_sn . '3C' . $status . $flow;
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    public function http_getIccid()
    {
        $sn = $this->parm['sn'] ?? '';
        if (empty($sn)) {
            return $this->jsonend(-1000, '缺少必要参数：sn');
        }
        $command = $this->device_sn . '8C' . '01';
        var_dump($command);
        $this->sendToUid($this->device_sn, $command);
        return $this->jsonend(1000, '发送成功');
    }

    public function queryDeviceProperty($sn): bool
    {
        dump('获取设备属性详情');
        $queryDeviceProperty =  $this->OneNetApi->queryDeviceProperty($sn);
        foreach ($queryDeviceProperty as $k=>$v){
            if($v['identifier'] == 'software_version2'){
                return $v['value'] == 2000;
            }
            break;
        }
        return false;
    }
}
