<?php

/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/3/12 0012
 * Time: 下午 2:57
 */

namespace app\Controllers\House;

use app\Library\SLog;
use app\Services\Common\DataService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use app\Tasks\DeviceTask;
use Server\CoreBase\Controller;
use Server\CoreBase\SelectCoroutine;
use Server\CoreBase\SwooleException;
use Server\CoreBase\SwooleInterruptException;
use Server\CoreBase\SwooleRedirectException;
use Server\SwooleMarco;

/*
 * TCP连接基础类
 */

class Base extends Controller
{

    protected $parm;
    protected $GetIPAddressHttpClient;
    protected $EquipmentListsModel;

    public function initialization($strontroller_name, $method_name)
    {

        if ($this->response !== null && $this->request_type == 'http_request') {
            $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
            $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token");
            $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        }
        parent::initialization($strontroller_name, $method_name);
        if ($this->request_type == 'http_request') {
            $this->parm = json_decode($this->http_input->getRawContent(), true);
        }
        $this->EquipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');

    }

    /**
     * 异常的回调(如果需要继承$autoSendAndDestroy传flase)
     * @param \Throwable $e
     * @param callable $handle
     */
    public function onExceptionHandle(\Throwable $e, $handle = null)
    {
        $log = '--------------------------[报错信息]----------------------------' . PHP_EOL;
        $log .= 'client：House' . PHP_EOL;
        $log .= 'DATE：' . date("Y-m-d H:i:s") . PHP_EOL;
        $log .= 'getMessage：' . $e->getMessage() . PHP_EOL;
        $log .= 'getCode：' . $e->getCode() . PHP_EOL;
        $log .= 'getFile：' . $e->getFile() . PHP_EOL;
        $log .= 'getLine：' . $e->getLine();
        //AsyncFile::write('error', $log);

        //必须的代码
        if ($e instanceof SwooleRedirectException) {
            $this->http_output->setStatusHeader($e->getCode());
            $this->http_output->setHeader('Location', $e->getMessage());
            $this->http_output->end('end');
            return;
        }
        //中断信号
        if ($e instanceof SwooleInterruptException) {
           // FileLogService::WriteLog(SLog::DEBUG_LOG,'debug','中断');
            return;
        }
        if ($e instanceof SwooleException) {
            secho("EX", "--------------------------[报错指南]----------------------------" . date("Y-m-d h:i:s"));
            secho("EX", "异常消息：" . $e->getMessage());
            secho("EX", "异常行数：" . $e->getLine());
            $context = $this->getContext();
            if (isset($context['RunStack'])) {
                print_context($this->getContext());
            }
            secho("EX", "--------------------------------------------------------------");
        }
        $this->context['error_message'] = $e->getMessage();
        //如果是HTTP传递request过去
        if ($this->request_type == SwooleMarco::HTTP_REQUEST) {
            $e->request = $this->request;
        }
        //生成错误数据页面
        $error_data = get_instance()->getWhoops()->handleException($e);
        if ($this->isEnableError) {
            try {
                $this->Error->push($e->getMessage(), $error_data);
            } catch (\Throwable $e) {
                var_dump($e->getMessage());
            }
        }

        //可以重写的代码
        if ($handle == null) {
            switch ($this->request_type) {
                case SwooleMarco::HTTP_REQUEST:
                    $this->http_output->setStatusHeader(500);
                    if ($this->isErrorHttpShow) {
                        $this->http_output->end($error_data);
                    } else {
                        $this->http_output->end($e->getMessage());
                    }
                    break;
                case SwooleMarco::TCP_REQUEST:
                    try {
                        $this->send($e->getMessage());
                    } catch (\Exception $e) {
                    }
                    break;
            }
        } else {
            sd_call_user_func($handle, $e);
        }
    }

    /**
     * 关闭连接
     * @throws \Exception
     * @date 2018/7/25 17:46
     * @author ligang
     */
    public function onClose()
    {
        // var_dump($this->fd.'断开连接');
        if ($this->uid) {
            //获取设备信息
            $this->getFdInfo();
            $device_no = hexdec($this->uid);

            //异步任务
            $DeviceTask = $this->loader->task(DeviceTask::class, $this);
            $DeviceTask->startTask('deviceOffline', [$device_no]);
            //写入设备离线日志
            FileLogService::WriteLog(SLog::DEVICE_OFFLINE_LOG, $device_no, ['msg' => date('Y-m-d H:i:s', time()) . '设备已离线'],true);


            //大数据
            DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 4, 'device_no' => $device_no, 'status' => '设备离线了']]);
          //  secho('---断开连接---', '设备断开连接，设备编号：' . $device_no);
        } else {
           // secho('---断开连接---', '设备断开连接，设备编号未绑定UID,UID：' . $this->uid);
        }
        $this->kickUid($this->uid);
        $this->destroy();
    }

    //打开连接
    public function onConnect()
    {
        //大数据
        DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 5]]);
       // secho('===设备连接===', '家用机打开连接');
    //    var_dump('家用机打开连接'.$this->fd);
        $this->destroy();
    }
    //输出文字
    public function echoMsg($message = '')
    {
        $output = $message;
        if (!$this->canEnd()) {
            return;
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            // $output = "<pre>$output</pre>";
        }
        $this->response->end($output);
        $this->endOver();
        $this->interrupt();
    }
    /**
     * 返回json格式
     */
    public function jsonend($strode = '', $message = '', $data = '', $gzip = true)
    {

        $output = array('code' => $strode, 'message' => $message, 'data' => $data);

        if (!$this->canEnd()) {
            return;
        }
        if (!get_instance()->config->get('http.gzip_off', false)) {
            //低版本swoole的gzip方法存在效率问题
//            if ($gzip) {
//                $this->response->gzip(1);
//            }
            //压缩备用方案
            /* if ($gzip) {
              $this->response->header('Content-Encoding', 'gzip');
              $this->response->header('Vary', 'Accept-Encoding');
              $output = gzencode($output . " \n", 9);
              } */
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            // $output = "<pre>$output</pre>";
        }
        $this->response->end($output);
        $this->endOver();
        $this->interrupt();
    }

    /**
     * 十进制转十六进制 不足位数前面补零*
     *
     * @param string $data 传入数据
     * @param string $length 转换的十六进制长度
     * @return string 返回数据
     *
     */
    function decto_bin($data, $length)
    {
        $length = hexdec($length);
        $var = sprintf("%0" . $length . "s", dechex($data));
        return $var;
    }

    function dechexBin($data, $length)
    {
        $var = sprintf("%0" . $length . "s", dechex($data));
        return $var;
    }

    /**
     * 十进制转二进制
     * @param $data
     * @param $length
     * @return string
     * @date 2018/7/31 15:01
     * @author ligang
     */
    function decbin($data, $length)
    {
        $length = hexdec($length);
        $var = sprintf("%0" . $length . "s", decbin($data));
        return $var;
    }

    /**
     * 十六进制转二进制 不足位数前面补零0
     *
     * @param string $data 传入数据
     * @param string $length 转换的二进制长度
     * @return string 返回数据
     *
     */
    function hex_2bin($data, $length)
    {
        $var = sprintf("%0" . $length . "s", base_convert($data, 16, 2));
        return $var;
    }

    /**
     * @desc 十进制卡号 不足位补0
     * @param type $data
     * @param type $length
     * @return type
     */
    public function dec_card($data, $length = 10)
    {
        $var = sprintf("%0" . $length . "s", $data);
        return $var;
    }

    /**
     *  生成流水号
     * @return string
     * @date 2018-04-16 下午 2:23
     * @author bg
     */
    protected function build_order_no()
    {
        return substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
    }


    /**
     * 包序列号的补码
     * 请求参数     请求类型      是否必须      说明
     * @author ligang
     * @date 2018/7/31 15:53
     */
    public function complement($number)
    {
        //十进制转二进制,位数8
        $a = $this->decbin($number, 8);
        $b = '';
        //获取反码
        for ($i = 0; $i < 8; $i++) {
            $b .= substr($a, $i, 1) ? 0 : 1;
        }
        //二进制转十进制
        $str = bindec($b);
        //十进制转十六进制
        $stromplement = $this->decto_bin($str, 2);
        return $stromplement;
    }

    /**
     * 补位
     * @param string $str 带填补的字符串
     * @param int $length 补后的长度
     * @param string $fill_str 填补字符串
     * @param string $place 填补位置（左边：STR_PAD_LEFT,右边：STR_PAD_RIGHT,两端：STR_PAD_BOTH）
     * @return string
     * @date 2018/7/31 18:37
     * @author ligang
     */
    public function strPad(string $str, int $length, string $fill_str, string $place = STR_PAD_LEFT)
    {
        return str_pad($str, $length, $fill_str, $place);
    }

    public function sprintf($data, $length)
    {
        $var = sprintf("%" . $length . "s1A", $data);
        return $var;
    }

    /**
     * 将字符串（含符号）转换为16进制
     * @param string $string
     * @return string
     * @date 2018/8/2 11:08
     * @author ligang
     */
    function String2Hex(string $string)
    {
        $hex = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $hex .= dechex(ord($string[$i]));
        }
        return $hex;
    }

    /**
     * 将16进制转换为字符串（含符号）
     * @param $hex
     * @return string
     * @date 2018/8/2 11:09
     * @author ligang
     */
    function Hex2String(string $hex)
    {
        $string = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
        }
        return $string;
    }

}
