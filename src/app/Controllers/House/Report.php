<?php

/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2018/7/20
 * Time: 16:06
 */

namespace app\Controllers\House;

use app\Library\SLog;
use app\Services\Common\DataService;
use app\Services\Common\HttpService;
use app\Services\Device\DeviceService;
use app\Services\Log\AsyncFile;
use app\Services\Log\Memcached;
use app\Services\Log\FileLogService;
use cmiot\Util;

/**
 *
 * Class Report
 * @package app\Controllers\House
 */
class Report extends Base
{

    protected $EquipmentListsModel;
    protected $EquipmentLibraryModel;
    protected $EquipmentLogModel;
    protected $CustomerBindEquipmentModel;
    protected $EquipmentUpgradeLogEquipment;
    protected $device_no;
    protected $ContractPackageRecordModel;
    protected $Achievement;
    protected $OneNetApi;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->EquipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->EquipmentLibraryModel = $this->loader->model('EquipmentLibraryModel', $this);
        $this->EquipmentLogModel = $this->loader->model('EquipmentLogModel', $this);
        $this->CustomerBindEquipmentModel = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->EquipmentUpgradeLogEquipment = $this->loader->model('EquipmentUpgradeLogEquipment', $this);
        $this->ContractPackageRecordModel = $this->loader->model('ContractPackageRecordModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        //设备编号上报为16进制，转为10进制，仅存储数据库使用
        $this->device_no = hexdec($this->client_data['device_sn']);

        /************************中移****************/
        require_once APP_DIR . '/Library/cmiot/OneNetApi.php';
        $this->OneNetApi = get_instance()->OneNetApi;
    }

    /**
     * +--------------------+--------------------------------------------------
     * |       Date         |                   Note
     * +--------------------+--------------------------------------------------
     * |    2018-8-24       |  将网络请求更改为直接send下发指令
     * +--------------------+--------------------------------------------------
     * |    2018-8-24       |  优化心跳包逻辑
     * +--------------------+--------------------------------------------------
     * |    2018-8-24       |  更改滤芯复位与修改的逻辑
     * +--------------------+--------------------------------------------------
     * |    2018-8-24       |  更改设备主动同步时间，大于1小时同步时间和数据
     * +--------------------+--------------------------------------------------
     * |    2018-8-24       |  更改已用流量同步
     * +--------------------+--------------------------------------------------
     * |    2018-8-24       |  更改剩余流量同步
     * +--------------------+--------------------------------------------------
     * |    2018-8-24       |  增加屏幕显示模式切换数据库储存
     * +--------------------+--------------------------------------------------
     * |    2018-8-24       |  增加更改检修模式
     * +--------------------+--------------------------------------------------
     * |    2018-08-27      |  优化心跳包逻辑
     * +--------------------+--------------------------------------------------
     * |    2018-10-16      |   修复重大BUG，剩余流量为负数时下发数据同步，置为最大值
     * +--------------------+--------------------------------------------------
     * |    2018-11-16      |   修改底层文件，拦截升级错误（该错误不影响正常升级）
     * +--------------------+--------------------------------------------------
     * |    2019-04-24      |   重写心跳
     * +--------------------+--------------------------------------------------
     */

    /**
     * 上报设备信息------------->01指令
     * @return bool
     * @throws \Exception
     * @date 2018/7/20 16:25
     * @author ligang
     */
    public function machine_state()
    {
        //  FileLogService::WriteLog(SLog::DEVICE_HEARTBEAT_LOG, $this->device_no, '<<<上报心跳，设备号：' . $this->device_no,true);
        //设备编号
        $device_sn = $this->client_data['device_sn'];
        //阻止新设备连接时重复注册
        $checkDeviceNumber = $this->LockDevice($this->device_no, 'heartbeat');
        if ($checkDeviceNumber) {
            return false;
        }
        //当UID不等于设备编号时绑定
        if (!$this->uid || $this->uid != $device_sn) {
            $this->bindUid($device_sn);
        }
        //阻塞redis防止为null,且设置超时时间（5s）
        $timeout = 100 * 50;
        $is_null = false;
        while ($this->redis == null) {
            sleepCoroutine(100);
            $timeout -= 100;
            if ($timeout <= 0) {
                $is_null = true;
                break;
            }
        }
        if ($is_null) {
            //本次redis获取失败，直接结束
            return false;
        }
        //应答
        $redis_info = $this->redis->get('device_heartbeat:' . $this->device_no);
        if (!$redis_info) {
            //获取设备库信息
            $lib_field = '
                equipments_id,
                device_no,
                id
            ';
            $equipmentLibraryInfo = $this->EquipmentListsModel->findEquipmentLibrary(['device_no' => $this->device_no], $lib_field);
            //不存在设备库信息断开连接
            if (empty($equipmentLibraryInfo)) {
                FileLogService::WriteLog(SLog::DEVICE_HEARTBEAT_LOG, $this->device_no, '<<<心跳检测<<<接受上报参数，设备库中没有该设备信息，拒绝连接，设备号：' . $this->device_no, true);
                $this->close();
                return false;
            }

        }

        $this->reply($device_sn);
        FileLogService::WriteLog(SLog::DEVICE_HEARTBEAT_LOG, $this->device_no, '回复心跳', false);
        //获取redis
        if (!$redis_info) {
            //获取设备库信息
//            $lib_field = '
//                equipments_id,
//                device_no,
//                id
//            ';
//            $equipmentLibraryInfo = $this->EquipmentListsModel->findEquipmentLibrary(['device_no' => $this->device_no], $lib_field);
//            //不存在设备库信息断开连接
//            if (empty($equipmentLibraryInfo)) {
//                FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<心跳检测<<<接受上报参数，设备库中没有该设备信息，拒绝连接，设备号：' . $this->device_no);
//                $this->close();
//                return false;
//            }
            //获取设备信息
            $field = '
                equipment_id,
                status,
                water_making_state,
                device_status,
                screen_status,
                working_mode,
                remaining_traffic,
                remaining_days,
                used_traffic,
                used_days,
                water_purification_TDS,
                raw_water_TDS,
                signal_strength_value,
                lac_value,
                cid_value,
                shutdown_state
            ';
            $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
            //不存在设备信息，则添加该设备信息
            if (empty($equipmentInfo)) {
                $report_data['equipments_id'] = $equipmentLibraryInfo['equipments_id'];
                $report_data['device_no'] = $equipmentLibraryInfo['device_no'];
                $report_data['create_time'] = time();
                $report_data['status'] = 2;
                //插入设备信息，并修改设备库状态
                $check = false;
                $this->db->begin(function () use ($report_data, $equipmentLibraryInfo, &$check) {
                    $this->EquipmentListsModel->insertEquipmentLists($report_data);
                    $this->EquipmentListsModel->updateEquipmentLibrary(['device_status' => '2'], ['id' => $equipmentLibraryInfo['id']]);
                    $check = true;
                }, function ($e) {
                    //报告错误
                    $this->onExceptionHandle($e);
                });
                if ($check) {
                    //注册成功后，获取心跳
                    $data = $device_sn . '0D' . '00';
                    $this->sendToUid($device_sn, $data);
                    return false;
                } else {
                    //注册失败，终止本次心跳
                    FileLogService::WriteLog(SLog::DEVICE_HEARTBEAT_LOG, $this->device_no, '<<<注册设备<<<注册设备失败，设备编号：' . $this->device_no);
                    return false;
                }
            }

            $redis_info = [
                'data' => [
                    'equipment_id' => $equipmentInfo['equipment_id'], //设备ID
                    'status' => $equipmentInfo['status'], //设备状态
                    'water_making_state' => $equipmentInfo['water_making_state'], //设备ID
                    'device_status' => $equipmentInfo['device_status'], //设备状态
                    'screen_status' => $equipmentInfo['screen_status'], //屏幕状态
                    'working_mode' => $equipmentInfo['working_mode'], //工作模式
                    'remaining_traffic' => $equipmentInfo['remaining_traffic'], //剩余流量
                    'remaining_days' => $equipmentInfo['remaining_days'], //剩余天数
                    'used_traffic' => $equipmentInfo['used_traffic'], //已用流量
                    'used_days' => $equipmentInfo['used_days'], //已用天数
                    'water_purification_TDS' => $equipmentInfo['water_purification_TDS'], //净水TDS
                    'raw_water_TDS' => $equipmentInfo['raw_water_TDS'], //原水TDS
                    'signal_strength_value' => $equipmentInfo['signal_strength_value'], //信号强度值
                    'lac_value' => $equipmentInfo['lac_value'], //LAC值
                    'cid_value' => $equipmentInfo['cid_value'], //CID值
                    'shutdown_state' => $equipmentInfo['shutdown_state'],
                ],
                'time' => time()
            ];
        } else {
            $redis_info = json_decode($redis_info, 1);
        }

        //获取上报数据
        $command = $this->client_data['data'];
        //构建上报数据
        $remaining_days = hexdec(substr($command, 14, 4));
        $remaining_days = $remaining_days == 65535 ? 0 : $remaining_days;
        $device_status = hexdec(substr($command, 0, 2));
        $report_data = [
            'status' => 2,
            'device_status' => $device_status, //设备状态
            'screen_status' => hexdec(substr($command, 2, 2)), //屏幕状态
            'working_mode' => hexdec(substr($command, 4, 2)), //工作模式
            'remaining_traffic' => hexdec(substr($command, 6, 8)), //剩余流量
            'remaining_days' => $remaining_days, //剩余天数
            'used_traffic' => hexdec(substr($command, 18, 8)), //已用流量
            'used_days' => hexdec(substr($command, 26, 4)), //已用天数
            'raw_water_TDS' => hexdec(substr($command, 34, 4)),//原水TDS
            'signal_strength_value' => hexdec(substr($command, 38, 2)), //信号强度值
            'lac_value' => hexdec(substr($command, 40, 4)), //LAC值
            'cid_value' => hexdec(substr($command, 44, 4)), //CID值
        ];
        //处理TDS,仅正在制水时更新TDS值--2021/5/17修改
        $report_data['water_purification_TDS'] = $redis_info['data']['water_purification_TDS']; //净水TDS
        if ($redis_info['data']['water_making_state'] == 1) {
            $report_data['water_purification_TDS'] = hexdec(substr($command, 30, 4)); //净水TDS
        }
        try {
            //写入上报心跳日志，并记录设备上传时间
            $device_heartbeat_log = $report_data;
            $device_heartbeat_log['device_time'] = date('Y-m-d H:i:s', hexdec(substr($command, 48, 10)));
            $device_heartbeat_log['service_time'] = date('Y-m-d H:i:s', time());
            $device_heartbeat_log['diff_time'] = abs(hexdec(substr($command, 48, 10)) - time());
            $lock = $this->redis->get('getLeartLock:' . $this->device_no);
            if (empty($lock)) {
                $lock = false;
            }
            FileLogService::WriteLog(SLog::DEVICE_HEARTBEAT_LOG, $this->device_no, $device_heartbeat_log, false);
        } catch (\Exception $e) {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<心跳日志写入失败<<<失败原因：' . $e->getMessage(), true);
        }
        //设备时间戳
        $device_time = hexdec(substr($command, 48, 10));
        //时间差
        $diff_time = abs(time() - $device_time);
        //  var_dump("服务端时间:".date('Y-m-d H:i:s').'===上报时间'.date('Y-m-d H:i:s',$device_time));
        //误差时间大于360s，发起时间同步
        if ($diff_time > 360) {
            $this->sendToUid($device_sn, $device_sn . '18' . $this->dechexBin(time(), 10));
        }
        //设备状态为滤芯待复位,请求设备上报滤芯状态
        if ($report_data['device_status'] == 4) {
            DeviceService::syncFilterData($this->device_no);//重新同步滤芯值
        }
        //工作模式其他值，都默认为1
        if (!in_array($report_data['working_mode'], [0, 1])) {
            $report_data['working_mode'] = 1;
        }
        //判断模式，放弃模式不匹配的数据比较
        if ($redis_info['data']['working_mode'] == 1 && $report_data['working_mode'] == 1) {
            //如果是时长模式，不比较剩余流量
            $report_data['remaining_traffic'] = $redis_info['data']['remaining_traffic'];
        } else {
            //如果是流量模式，不比较剩余天数
            $report_data['remaining_days'] = $redis_info['data']['remaining_days'];
        }

        //若设备工作状态是制水状态则不比较流量
        if ($redis_info['data']['water_making_state'] == 1) {
            $report_data['remaining_traffic'] = $redis_info['data']['remaining_traffic'];
            $report_data['used_traffic'] = $redis_info['data']['used_traffic'];
        }
        //判断流量误差，小于10ml的则不同步数据
        $error_remaining_traffic = abs($report_data['remaining_traffic'] - $redis_info['data']['remaining_traffic']);
        $error_used_traffic = abs($report_data['used_traffic'] - $redis_info['data']['used_traffic']);
        if ($error_remaining_traffic < 10) {
            $report_data['remaining_traffic'] = $redis_info['data']['remaining_traffic'];
        }
        if ($error_used_traffic < 10) {
            $report_data['used_traffic'] = $redis_info['data']['used_traffic'];
        }
        //是否相等（时长除外）
        $is_eq = false;
        //是否需要同步数据
        $is_sync = false;
        //获取上报数据的key
        $report_key = array_keys($report_data);
        $data_sync_array = ['remaining_days', 'used_days', 'remaining_traffic', 'used_traffic'];
        foreach ($report_key as $key => $value) {
            if ($report_data[$value] != $redis_info['data'][$value]) {
                if (in_array($value, $data_sync_array)) {
                    //需要同步数据
                    $is_sync = true;
                    continue;
                }
                $is_eq = true;
                $redis_info['data'][$value] = $report_data[$value];
                if ($value == 'remaining_days' && $report_data[$value] == 65535) {
                    var_dump($this->device_no . $value . $report_data[$value]);
                    $redis_info['data']['remaining_days'] = 0;
                }
            }
        }

        //若设备剩余时长大于0，且上报状态为欠费，开启同步时长，避免无辜欠费情况
        $shutdown_state = $redis_info['data']['shutdown_state'] ?? 0;
        if ($report_data['device_status'] == 3 && $shutdown_state == 0) {
            if ($redis_info['data']['remaining_days'] > 0) {
                $is_sync = true;
            } else {
                //欠费且剩余天数为0，重新计算天数
                //获取设备的合同ID
                $where = [
                    'equipment_id' => $redis_info['data']['equipment_id']
                ];
                $join = [
                    ['contract', 'rq_equipment_lists.contract_id = rq_contract.contract_id', 'LEFT']
                ];
                $this->Achievement->table = 'equipment_lists';
                $result = $this->Achievement->findJoinData($where, 'exire_date,effect_time', $join);
                if (!empty($result)) {
                    $is_eq = true;
                    $redis_info['data']['remaining_days'] = 0;
                    $exire_date = strtotime(getMonthLastDay($result['exire_date']));   //获取签约月份最后一天
                    $redis_info['data']['remaining_days'] = ceil(($exire_date - time()) / 86400);
                    $redis_info['data']['used_days'] = ceil((time() - $result['effect_time']) / 86400);
                    //  var_dump("服务端天数:".$redis_info['data']['remaining_days'].'===上报天数'.$report_data['remaining_days']);
                    if ($redis_info['data']['remaining_days'] != $report_data['remaining_days']) {
                        $is_sync = true;
                    }
                }
            }
        }

        //数据同步
        if ($is_sync) {
            sleepCoroutine(200);
            //下发服务器剩余流量、剩余天数、已用流量、已用天数
            $data_sync = $device_sn . '08' . $this->dechexBin($redis_info['data']['used_days'], 4);
            $data_sync .= $this->dechexBin($redis_info['data']['remaining_days'], 4);
            $data_sync .= $this->dechexBin($redis_info['data']['used_traffic'], 8);
            $data_sync .= $this->dechexBin($redis_info['data']['remaining_traffic'] < 0 ? 0 : $redis_info['data']['remaining_traffic'], 8);
            $this->sendToUid($device_sn, $data_sync);
            //写入数据同步日志
            FileLogService::WriteLog(SLog::DEVICE_DATA_SYNC, $this->device_no, [
                'type' => 'down',
                'used_days' => $redis_info['data']['used_days'],
                'remaining_days' => $redis_info['data']['remaining_days'],
                'used_traffic' => $redis_info['data']['used_traffic'],
                'remaining_traffic' => $redis_info['data']['remaining_traffic'],
            ]);
        }

        //更新redis和数据库
        $data = [];
        if ($is_eq) {
            $data = $redis_info['data'];
        }
        //修正净水TDS值
        if ($report_data['water_purification_TDS'] > 50 && $redis_info['data']['water_making_state'] == 1) {
            //获取设备区县
            $this->Achievement->table = 'equipment_lists';
            $a_code = $this->Achievement->findData(['equipment_id' => $redis_info['data']['equipment_id']], 'area_code');
            $a_code = $a_code['area_code'] ?? '';
            $data['water_purification_TDS'] = $this->CityTDS($a_code, $report_data['water_purification_TDS']);
            $data['device_water_purification_TDS'] = $report_data['water_purification_TDS'];
            $redis_info['data']['water_purification_TDS'] = $data['water_purification_TDS'];
        }

        //更新redis
        $this->redis->setex('device_heartbeat:' . $this->device_no, '360', json_encode($redis_info));

        //数据操作
        if (!empty($data)) {
            $check = false;
            $this->db->begin(function () use ($device_sn, $redis_info, $data, &$check) {
                //若存在设备状态，且是欠费，写入设备记录，（设备状态不一致操作）
                if (isset($data['device_status']) && $data['device_status'] == 3 && $redis_info['data']['remaining_days'] < 1) {
                    $user_bind_device_where = [
                        'equipment_id' => $redis_info['data']['equipment_id'],
                        'status' => 1,
                        'is_owner' => 2
                    ];
                    $user_bind_device = $this->CustomerBindEquipmentModel->getOne($user_bind_device_where, 'user_id');
                    if ($user_bind_device) {
                        $log_map['create_time'] = ['between', [strtotime("-7 day"), time()]];
                        $log_map['equipment_id'] = $redis_info['data']['equipment_id'];
                        $log_map['user_id'] = $user_bind_device['user_id'];
                        $log_map['type'] = 5;
                        $log_info = $this->EquipmentLogModel->getOne($log_map, 'id');
                        if (empty($log_info)) {
                            $log = [
                                'user_id' => $user_bind_device['user_id'],
                                'equipment_id' => $redis_info['data']['equipment_id'],
                                'type' => 5,
                                'user_role' => 4,
                                'remark' => '设备欠费，设备编号：' . $this->device_no,
                                'create_time' => time()
                            ];
                            //向大数据平台发送通知
                            DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 8, 'status' => $log['remark'], 'device_no' => $this->device_no]]);
                            $this->EquipmentLogModel->add($log);
                        }
                    }
                }
                //更新设备信息
                $this->EquipmentListsModel->updateEquipmentLists($data, ['equipment_id' => $redis_info['data']['equipment_id']]);
                $check = true;
            }, function ($e) {
                //心跳数据库操作失败
                FileLogService::WriteLog(SLog::DEVICE_HEARTBEAT_LOG, $this->device_no, ['msg' => '心跳数据库操作失败', 'error' => $e->error]);
            });
        }
    }

    /**
     * 上报设备套餐，需下发指令后上报------------->02指令
     * @author ligang
     * @date 2018/7/23 13:51
     */
    public function bindingPackage()
    {
        $device_sn = $this->client_data['device_sn'];
        $command = $this->client_data['data'];
        //获取设备信息
        $field = '
            filter_element,
            equipment_id
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<上报套餐<<<上报设备套餐，未找到设备信息，断开连接，设备编号：' . $this->device_no);
            $this->close();
            return false;
        }
        if (empty($equipmentInfo['filter_element'])) {
            $equipmentInfo['filter_element'] = [
                [
                    'residual_value' => hexdec(substr($command, 2, 4)),
                    'max' => hexdec(substr($command, 2, 4)),
                ],
                [
                    'residual_value' => hexdec(substr($command, 6, 4)),
                    'max' => hexdec(substr($command, 6, 4)),
                ],
                [
                    'residual_value' => hexdec(substr($command, 10, 4)),
                    'max' => hexdec(substr($command, 10, 4)),
                ],
                [
                    'residual_value' => hexdec(substr($command, 14, 4)),
                    'max' => hexdec(substr($command, 14, 4)),
                ],
                [
                    'residual_value' => hexdec(substr($command, 18, 4)),
                    'max' => hexdec(substr($command, 18, 4)),
                ]
            ];
        } else {
            $equipmentInfo['filter_element'] = json_decode($equipmentInfo['filter_element'], true);
            $i = 2;
            foreach ($equipmentInfo['filter_element'] as $key => &$value) {
                $value['max'] = hexdec(substr($command, $i, 4));
                $i += 4;
            }
        }
        $working_mode = hexdec(substr($command, 0, 2));

        $data = [
            'working_mode' => $working_mode,
            'filter_element' => json_encode($equipmentInfo['filter_element']),
            'work_time' => time(), //激活时间
        ];
        $result = $this->EquipmentListsModel->updateEquipmentLists($data, ['equipment_id' => $equipmentInfo['equipment_id']]);
        //修改下发套餐状态
        $this->ContractPackageRecordModel->save(['equipment_id' => $equipmentInfo['equipment_id'], 'working_mode' => $working_mode, 'status' => 1], ['status' => 3, 'hand_success_time' => time()]);

        //设备套餐下发成功解除监控
        $package = $this->redis->get('PlanToMonitor');
        if ($package) {
            $package = json_decode($package, 1);
            foreach ($package as $key => $value) {
                if ($value['equipment_id'] == $equipmentInfo['equipment_id']) {
                    unset($package[$key]);
                }
            }
            array_values($package);
            $this->redis->set('PlanToMonitor', json_encode($package));
        }

        if ($result < 0) {
            return false;
        }
        //套餐下发成功后，获取滤芯状态
        $this->sendToUid($device_sn, $device_sn . '1901');
    }

    /**
     * 上报屏幕状态(关闭屏幕)------------->03指令
     * @throws \Exception
     * @date 2018/7/23 14:18
     * @author ligang
     */
    public function screen_status()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = [
            'screen_status' => 1
        ];
        $result = $this->EquipmentListsModel->updateEquipmentLists($data, ['device_no' => $this->device_no]);
        if ($result < 0) {
            return false;
        }
    }

    /**
     * 上报屏幕状态(开启屏幕)------------->04指令
     * @throws \Exception
     * @date 2018/7/23 14:18
     * @author ligang
     */
    public function screen_status_two()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = [
            'screen_status' => 0
        ];
        $result = $this->EquipmentListsModel->updateEquipmentLists($data, ['device_no' => $this->device_no]);
        if ($result < 0) {
            return false;
        }
    }

    /**
     * 上报开关机指令（关机）------------->05指令
     * @author ligang
     * @date time
     */
    public function power_off()
    {
        $device_sn = $this->client_data['device_sn'];
        //获取设备信息
        $field = '
            switch_machine,
            equipment_id
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }

        //设备开机才能关机
        if ($equipmentInfo['switch_machine'] == 1) {
            $update_data = [
                'switch_machine' => 0
            ];
            $result = $this->EquipmentListsModel->updateEquipmentLists($update_data, ['equipment_id' => $equipmentInfo['equipment_id']]);
            if ($result < 0) {
                return false;
            }
            return false;
        }
    }

    /**
     * 上报开关机指令（开机）------------->06指令
     * @author ligang
     * @date time
     */
    public function power_off_two()
    {
        $device_sn = $this->client_data['device_sn'];
        //获取设备信息
        $field = '
            switch_machine,
            equipment_id
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }
        //设备关机才能开机
        if ($equipmentInfo['switch_machine'] == 0) {
            $update_data = [
                'switch_machine' => 1
            ];
            $result = $this->EquipmentListsModel->updateEquipmentLists($update_data, ['equipment_id' => $equipmentInfo['equipment_id']]);
            if ($result < 0) {
                return false;
            }
            return false;
        }
    }

    /**
     * 工作状态同步------------->0c指令
     * @return bool
     * @throws \Exception
     * @date 2018/8/21 16:09
     * @author ligang
     */
    public function working_state_synchronization()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $this->reply($device_sn, '0c');
        $LockDevice = $this->LockDevice($this->device_no, 'working_state');
        if ($LockDevice) {
            return false;
        }

        //阻塞redis防止为null,且设置超时时间（5s）
        $timeout = 100 * 50;
        $is_null = false;
        while ($this->redis == null) {
            sleepCoroutine(100);
            $timeout -= 100;
            if ($timeout <= 0) {
                $is_null = true;
                break;
            }
        }
        if ($is_null) {
            //本次redis获取失败，直接结束
            return false;
        }

        //获取redis
        $redis_info = $this->redis->get('work_state:' . $this->device_no);
        //判断redis
        if ($redis_info) {
            $redis_info = json_decode($redis_info, 1);
        } else {
            $field = '
                equipment_id,
                water_making_state,
                water_full_state,
                repair_state,
                leaking_state,
                switch_machine,
                exsiccosis
            ';
            $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
            if (empty($equipmentInfo)) {
                $this->close();
                return false;
            }
            $redis_info = [
                'equipment_id' => $equipmentInfo['equipment_id'],
                'water_making_state' => $equipmentInfo['water_making_state'], //制水状态，0：未制水，1：正在制水
                'water_full_state' => $equipmentInfo['water_full_state'], //水满状态，0：未水满，1：水满
                'repair_state' => $equipmentInfo['repair_state'], //检修状态，0：正常，1：检修
                'leaking_state' => $equipmentInfo['leaking_state'], //漏水状态，0：未漏水，1：漏水
                'switch_machine' => $equipmentInfo['switch_machine'], //开关机，0：关机，1：开机
                'exsiccosis' => $equipmentInfo['exsiccosis'], //缺水状态，0：未缺水，1：缺水
            ];
            $this->redis->setex('work_state:' . $this->device_no, '360', json_encode($redis_info));
        }

        $report_data = [
            'equipment_id' => $redis_info['equipment_id'],
            'water_making_state' => hexdec(substr($data, 0, 2)), //制水状态，0：未制水，1：正在制水
            'water_full_state' => hexdec(substr($data, 2, 2)), //水满状态，0：未水满，1：水满
            'repair_state' => hexdec(substr($data, 4, 2)), //检修状态，0：正常，1：检修
            'leaking_state' => hexdec(substr($data, 6, 2)), //漏水状态，0：未漏水，1：漏水
            'switch_machine' => hexdec(substr($data, 8, 2)), //开关机，0：关机，1：开机
            'exsiccosis' => hexdec(substr($data, 10, 2)), //缺水状态，0：未缺水，1：缺水
        ];

        //写入设备工作状态日志
        FileLogService::WriteLog(SLog::DEVICE_WORK_STATE_LOG, $this->device_no, $report_data);

        //上传数据和redis做比较，更改后更新redis和数据
        $is_eq = false;
        $key_array = array_keys($report_data);
        foreach ($key_array as $key => $value) {
            if ($report_data[$value] != $redis_info[$value]) {
                $is_eq = true;
                $redis_info[$value] = $report_data[$value];
            }
        }

        if ($is_eq) {
            $check = false;
            $this->db->begin(function () use ($redis_info, $device_sn, &$check) {
                //若存在设备状态，且是欠费，写入设备记录，（设备状态不一致操作）
                if (isset($redis_info['repair_state']) && $redis_info['repair_state'] == 1 || isset($redis_info['leaking_state']) && $redis_info['leaking_state'] == 1 || isset($redis_info['exsiccosis']) && $redis_info['exsiccosis'] == 1) {
                    $user_bind_device_where = [
                        'equipment_id' => $redis_info['equipment_id'],
                        'status' => 1,
                        'is_owner' => 2
                    ];
                    $user_bind_device = $this->CustomerBindEquipmentModel->getOne($user_bind_device_where, 'user_id');
                    if (!empty($user_bind_device)) {

                        $log = [];
                        $tmp = [
                            'user_id' => $user_bind_device['user_id'],
                            'equipment_id' => $redis_info['equipment_id'],
                            'user_role' => 4,
                            'create_time' => time()
                        ];
                        //检修状态
                        if (isset($redis_info['repair_state']) && $redis_info['repair_state'] == 1) {
                            $tmp['type'] = 6;
                            $tmp['remark'] = '设备检修，设备编号：' . $this->device_no;
                            array_push($log, $tmp);
                        }
                        //漏水状态
                        if (isset($redis_info['leaking_state']) && $redis_info['leaking_state'] == 1) {
                            $tmp['type'] = 7;
                            $tmp['remark'] = '设备漏水，设备编号：' . $this->device_no;
                            array_push($log, $tmp);
                        }
                        //缺水状态
                        if (isset($redis_info['exsiccosis']) && $redis_info['exsiccosis'] == 1) {
                            $tmp['type'] = 8;
                            $tmp['remark'] = '设备缺水，设备编号：' . $this->device_no;
                            array_push($log, $tmp);
                        }
                        foreach ($log as $k => $v) {
                            //向大数据平台发送通知
                            DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 8, 'status' => $v['remark'], 'device_no' => $this->device_no]]);
                            //每7天计算一次
                            $log_map['create_time'] = ['between', [strtotime("-7 day"), time()]];
                            $log_map['equipment_id'] = $v['equipment_id'];
                            $log_map['user_id'] = $v['user_id'];
                            $log_map['type'] = $v['type'];
                            $log_info = $this->EquipmentLogModel->getOne($log_map, 'id');
                            if (empty($log_info)) {
                                $this->EquipmentLogModel->add($v);
                            }
                        }
                    }
                }
                $this->EquipmentListsModel->updateEquipmentLists($redis_info, ['equipment_id' => $redis_info['equipment_id']]);
                //更新redis
                $this->redis->setex('work_state:' . $this->device_no, '360', json_encode($redis_info));
                $check = true;
            }, function ($e) {
                FileLogService::WriteLog(SLog::DEVICE_WORK_STATE_LOG, $this->device_no, ['msg' => '工作状态同步数据库操作失败', 'error' => $e->error]);
            });
            if ($check) {
                //更新心跳包数据
                $device_heartbeat = $this->redis->get('device_heartbeat:' . $this->device_no);
                if ($device_heartbeat) {
                    $device_heartbeat = json_decode($device_heartbeat, 1);
                    $device_heartbeat['data']['water_making_state'] = $report_data['water_making_state'];
                    $this->redis->setex('device_heartbeat:' . $this->device_no, '360', json_encode($device_heartbeat));
                }

                return false;
            }
            return false;
        }
    }

    /**
     * 强制冲洗------------->07指令
     * @author ligang
     * @date 2018/7/23 15:52
     */
    public function mandatory_rinse()
    {
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<强制冲洗<<<上报强制冲洗，设备编号：' . $this->device_no);
    }

    /**
     * 更改为数据同步------------->08指令
     * @author ligang
     * @date 2018/7/23 16:17
     */
    public function charge_as()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $field = '
            equipment_id
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }
        $update = [
            'used_days' => hexdec(substr($data, 0, 4)),
            'remaining_days' => hexdec(substr($data, 4, 4)),
            'used_traffic' => hexdec(substr($data, 8, 8)),
            'remaining_traffic' => hexdec(substr($data, 16, 8))
        ];

        //写入上报数据同步日志
        FileLogService::WriteLog(SLog::DEVICE_DATA_SYNC, $this->device_no, [
            'type' => 'up',
            'used_days' => $update['used_days'],
            'remaining_days' => $update['remaining_days'],
            'used_traffic' => $update['used_traffic'],
            'remaining_traffic' => $update['remaining_traffic'],
        ]);

        $result = $this->EquipmentListsModel->updateEquipmentLists($update, ['equipment_id' => $equipmentInfo['equipment_id']]);
        if ($result < 0) {
            return false;
        }
        /**
         * 更改心跳包redis
         * 为保证redis和数据同步
         */
        $redis_info = $this->redis->get('device_heartbeat:' . $this->device_no);
        if ($redis_info) {
            $redis_info = json_decode($redis_info, 1);
            $redis_info['data']['used_days'] = hexdec(substr($data, 0, 4));
            $remaining_days = hexdec(substr($data, 4, 4));
            $redis_info['data']['remaining_days'] = $remaining_days == 65535 ? 0 : $remaining_days;
            $redis_info['data']['used_traffic'] = hexdec(substr($data, 8, 8));
            $redis_info['data']['remaining_traffic'] = hexdec(substr($data, 16, 8));
            $this->redis->setex('device_heartbeat:' . $this->device_no, '360', json_encode($redis_info));
        }

    }

    /**
     * 滤芯状态上传------------->09指令
     * @return bool
     * @throws \Exception
     * @date 2018/8/21 15:41
     * @author ligang
     */
    public function as_negative()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $this->reply($device_sn, '09');
        //获取设备信息
        $field = '
            equipment_id
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }

        //五级滤芯的剩余值和最大值
        $report_data = [
            [
                'residual_value' => hexdec(substr($data, 0, 4)),
                'max' => hexdec(substr($data, 20, 4)),
            ],
            [
                'residual_value' => hexdec(substr($data, 4, 4)),
                'max' => hexdec(substr($data, 24, 4)),
            ],
            [
                'residual_value' => hexdec(substr($data, 8, 4)),
                'max' => hexdec(substr($data, 28, 4)),
            ],
            [
                'residual_value' => hexdec(substr($data, 12, 4)),
                'max' => hexdec(substr($data, 32, 4)),
            ],
            [
                'residual_value' => hexdec(substr($data, 16, 4)),
                'max' => hexdec(substr($data, 36, 4)),
            ]
        ];

        //写入设备滤芯上报日志
        FileLogService::WriteLog(SLog::DEVICE_FILTER_ELEMENT_LOG, $this->device_no, $report_data, true);


        //滤芯不存在的情况,废弃设备表中的滤芯,优化滤芯对比逻辑
        /*
        if (empty($equipmentInfo['filter_element'])){
            $filter_element = [
                [
                    'residual_value' => hexdec(substr($data, 20, 4)),
                    'max' => hexdec(substr($data, 20, 4)),
                ],
                [
                    'residual_value' => hexdec(substr($data, 24, 4)),
                    'max' => hexdec(substr($data, 24, 4)),
                ],
                [
                    'residual_value' => hexdec(substr($data, 28, 4)),
                    'max' => hexdec(substr($data, 28, 4)),
                ],
                [
                    'residual_value' => hexdec(substr($data, 32, 4)),
                    'max' => hexdec(substr($data, 32, 4)),
                ],
                [
                    'residual_value' => hexdec(substr($data, 36, 4)),
                    'max' => hexdec(substr($data, 36, 4)),
                ]
            ];
            $this->Achievement->table = 'equipment_lists';
            $this->Achievement->updateData(['equipment_id'=>$equipmentInfo['equipment_id']],['filter_element'=>json_encode($filter_element)]);
            return false;
        }

        $filter_element = json_decode($equipmentInfo['filter_element'], 1);
        //比较两个滤芯的是否一致，不一致下发服务器参数
        if ($report_data != $filter_element) {
            $user_bind_device_where = [
                'equipment_id' => $equipmentInfo['equipment_id'],
                'status' => 1,
                'is_owner' => 2
            ];
            $user_bind_device = $this->CustomerBindEquipmentModel->getOne($user_bind_device_where, 'user_id');
            foreach ($filter_element as $key => $value) {
                foreach ($report_data as $k => $v) {
                    //判断是否到期
                    if (!empty($user_bind_device) && $value['residual_value'] == 0) {
                        $log = [
                            'user_id' => $user_bind_device['user_id'],
                            'equipment_id' => $equipmentInfo['equipment_id'],
                            'type' => 2,
                            'user_role' => 4,
                            'remark' => '设备第' . ($key + 1) . '级滤芯到期，设备编号：' . $this->device_no,
                            'create_time' => time()
                        ];
                        $this->EquipmentLogModel->add($log);
                    }
                    //哪一级不一致，下发哪一级
                    if ($value['residual_value'] != $v['residual_value']) {
                        $str = $this->dechexBin(($key + 7), 2) . $this->dechexBin($value['residual_value'], 4);
                        //防止连续下发，导致设备接受失败
                        sleepCoroutine(2000);
                        $this->reply($device_sn, '0E', $str);
                        break;
                    }
                }
            }
        }
        */

        //获取设备滤芯
        $where = [
            'equipment_id' => $equipmentInfo['equipment_id'],
            'is_filter' => 1,
            'is_delete' => 0
        ];
        $field = '
            filter_level,
            expire_time
        ';
        $order = [
            'filter_level' => 'ASC'
        ];
        $this->Achievement->table = 'equipments_parts';
        $filter_element = $this->Achievement->selectData($where, $field, [], $order);
        if (empty($filter_element)) {
            //没有找到该设备的滤芯
            return false;
        }
        $reckon_now = 1;
        foreach ($filter_element as $key => $value) {
            $Report = $report_data[$value['filter_level'] - 1]['residual_value'];
            $diff = $value['expire_time'] - time();
            $diff = $diff < 0 ? 0 : $diff;
            $reckon = ceil($diff / 86400);
            if ($reckon_now == 1 && $reckon < 1) {
                $reckon_now = 0;
            }
            if ($Report != $reckon) {
                $sync = $value;
                $sync['report_residual_value'] = $Report;
                $sync['reckon'] = $reckon;
                $sync['device_sn'] = $this->device_no;
                $params = ['sn' => $this->device_no, 'type' => 2, 'key' => $value['filter_level'], 'value' => $reckon];
                if ($reckon < -1) {//出现滤芯值小于0直接退出
                    continue;
                }
                $path = '/House/Issue/filter_element_reset_modification';
                //防止连续下发，导致设备接受失败
                sleepCoroutine(400);
                $result = HttpService::Thrash($params, $path);//重新下发
                if (!$result) {
                    FileLogService::WriteLog(SLog::DEVICE_FILTER_ELEMENT_LOG, 'serviceActiveSyncFilter', $sync);
                }
//                $str = $this->dechexBin(($value['filter_level'] + 6), 2) . $this->dechexBin($reckon, 4);
//                sleepCoroutine(400);
//                $this->reply($device_sn, '0E', $str);
                continue;
            }

        }
        if ($reckon_now == 0) {
            $user_bind_device_where = [
                'equipment_id' => $equipmentInfo['equipment_id'],
                'status' => 1,
                'is_owner' => 2
            ];
            $user_bind_device = $this->CustomerBindEquipmentModel->getOne($user_bind_device_where, 'user_id');
            //判断是否到期
            if (!empty($user_bind_device)) {
                //每7天计算一次
                $log_map['create_time'] = ['between', [strtotime("-7 day"), time()]];
                $log_map['equipment_id'] = $equipmentInfo['equipment_id'];
                $log_map['user_id'] = $user_bind_device['user_id'];
                $log_map['type'] = 2;
                $log_info = $this->EquipmentLogModel->getOne($log_map, 'id');
                if (empty($log_info)) {
                    $log = [
                        'user_id' => $user_bind_device['user_id'],
                        'equipment_id' => $equipmentInfo['equipment_id'],
                        'type' => 2,
                        'user_role' => 4,
                        'remark' => '滤芯到期，设备编号：' . $this->device_no,
                        'create_time' => time()
                    ];
                    $this->EquipmentLogModel->add($log);
                }

            }
        }


        $device_time = hexdec(substr($data, 40, 10));
        //判断误差时间,若误差大于360s，则发起时间同步
        $error_time = time() - $device_time;
        if (abs($error_time) > 360) {
            $this->sendToUid($device_sn, $device_sn . '17' . $this->dechexBin(time(), 10));
        }
    }

    /**
     * 用水同步------------->0A指令
     * @return bool
     * @throws \Server\CoreBase\SwooleException
     * @date 2018/8/24 16:03
     * @author ligang
     */
    public function synchronization_with_water()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $field = '
            equipment_id,
            used_traffic,
            working_mode,
            remaining_traffic,
            used_days,
            remaining_days
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }
        $water_yield = hexdec(substr($data, 0, 8));
        $insert_data = [
            'equipment_id' => $equipmentInfo['equipment_id'],
            'water' => $water_yield,
            'create_time' => time(),
        ];

        //写入设备用水量日志
        FileLogService::WriteLog(SLog::DEVICE_WITH_WATER_LOG, $this->device_no, $insert_data);

        $device_time = hexdec(substr($data, 8, 10));
        //判断误差时间,若误差大于1小时，则发起时间同步
        $error_time = abs(time() - $device_time);

        $check = false;
        $this->db->begin(function () use ($error_time, $water_yield, $equipmentInfo, $insert_data, $data, $device_sn, &$check) {
            $this->EquipmentListsModel->insertEquipmentWaterRecord($insert_data);

            $update = [
                'used_traffic' => $equipmentInfo['used_traffic'] + $water_yield
            ];
            //时长模式不减剩余流量
            if ($equipmentInfo['working_mode'] == 0) {
                $update['remaining_traffic'] = $equipmentInfo['remaining_traffic'] - $water_yield;
            }
            //更改redis中的已用流量
            $redis_info = $this->redis->get('device_heartbeat:' . $this->device_no);
            if ($redis_info) {
                $redis_info = json_decode($redis_info, 1);
                $redis_info['data']['used_traffic'] = $update['used_traffic'];
                $redis_info['data']['remaining_traffic'] = $update['remaining_traffic'];
                $this->redis->setex('device_heartbeat:' . $this->device_no, '360', json_encode($redis_info));
            }

            //时间误差操作1小时发起同步
            if ($error_time > 360) {
                $update['error_time'] = $error_time;
                //先同步时间，在同步数据
                $this->sendToUid($device_sn, $device_sn . '17' . $this->dechexBin(time(), 10));
            }
            $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
            $check = true;
        });

        if (!$check) {
            return false;
        }
        /**
         * 更改心跳包redis
         * 为保证redis和数据同步，上传用水量更改已用流量和剩余流量
         */
        $redis_info = $this->redis->get('device_heartbeat:' . $this->device_no);
        if ($redis_info) {
            $redis_info = json_decode($redis_info, 1);
            $redis_info['data']['used_traffic'] += $water_yield;
            $redis_info['data']['remaining_traffic'] -= $water_yield;
            $this->redis->setex('device_heartbeat:' . $this->device_no, '360', json_encode($redis_info));
        }
        $this->reply($device_sn, '0a');
    }

    /**
     * 用时同步(已用天数)------------->BA指令
     * @author ligang
     * @date 2018/7/23 17:25
     */
    public function time_synchronization()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $field = '
            equipment_id
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }

        $update_data = [
            'used_days' => hexdec($data)
        ];
        $result = $this->EquipmentListsModel->updateEquipmentLists($update_data, ['device_no' => $this->device_no]);
        if ($result < 0) {
            return false;
        }
        /**
         * 更改心跳包redis
         * 为保证redis和数据同步
         */
        $redis_info = $this->redis->get('device_heartbeat:' . $this->device_no);
        if ($redis_info) {
            $redis_info = json_decode($redis_info, 1);
            $redis_info['data']['used_days'] = hexdec($data);
            $this->redis->setex('device_heartbeat:' . $this->device_no, '360', json_encode($redis_info));
        }
    }

    /**
     * 用时同步(剩余天数)------------->BB指令
     * @author ligang
     * @date 2018/7/30 16:46
     */
    public function time_sync()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $field = '
            equipment_id
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }

        $update_data = [
            'remaining_days' => hexdec($data)
        ];
        $result = $this->EquipmentListsModel->updateEquipmentLists($update_data, ['device_no' => $this->device_no]);
        if ($result < 0) {
            return false;
        }
        /**
         * 更改心跳包redis
         * 为保证redis和数据同步
         */
        $redis_info = $this->redis->get('device_heartbeat:' . $this->device_no);
        if ($redis_info) {
            $redis_info = json_decode($redis_info, 1);
            $remaining_days = hexdec($data);
            $redis_info['data']['remaining_days'] = $remaining_days == 65535 ? 0 : $remaining_days;
            $this->redis->setex('device_heartbeat:' . $this->device_no, '360', json_encode($redis_info));
        }
    }

    /**
     * 滤芯复位与修改------------->0E指令
     * @author ligang
     * @date 2018/7/23 19:05
     */
    public function filter_element_reset_modification()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        //获取设备上传参数
        $series = hexdec(substr($data, 0, 2));
        $number = hexdec(substr($data, 2));
        if ($series < 7) {
            $msg = '滤芯复位';
        } else {
            $msg = '滤芯修改';
        }
        $field = '
            filter_element
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }

        //判断设备滤芯值是否存在
        if (!empty($equipmentInfo['filter_element'])) {
            $filter_element = json_decode($equipmentInfo['filter_element'], 1);
            foreach ($filter_element as $key => $value) {
                if ($series == 0) {
                    //均不复位
                    break;
                } elseif ($series <= 5 && $series > 0) {
                    //复位单个滤芯
                    $filter_element[($series - 1)]['residual_value'] = $filter_element[($series - 1)]['max'];
                    break;
                } elseif ($series == 6) {
                    //复位全部滤芯
                    $filter_element[$key]['residual_value'] = $value['max'];
                } elseif ($series > 6) {
                    //修改
                    //如果修改的值大于最大值，则最大值等于修改值
                    if ($number > $filter_element[($series - 7)]['max']) {
                        $filter_element[($series - 7)]['max'] = $number;
                    }
                    $filter_element[($series - 7)]['residual_value'] = $number;
                    break;
                } else {
                    //不在范围之内的不做操作
                    break;
                }
            }
            $update_data = [
                'filter_element' => json_encode($filter_element)
            ];
            $result = $this->EquipmentListsModel->updateEquipmentLists($update_data, ['device_no' => $this->device_no]);
            if ($result < 0) {
                return false;
            }
        }
    }

    /**
     * 模式切换------------->0F指令
     * @date 2018/7/23 19:05
     * @throws \Exception
     * @author ligang
     */
    public function mode_switch()
    {
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<模式切换<<<模式切换，设备编号：' . $this->device_no);
        //请求心跳包
        $device_sn = $this->client_data['device_sn'];
        $this->reply($device_sn, '0D');
    }

    /**
     * 恢复出厂设置------------->10指令
     * @throws \Exception
     * @date 2018/8/10 14:03
     * @author ligang
     */
    public function factory_data_reset()
    {
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<恢复出厂设置<<<恢复出厂设置，设备编号：' . $this->device_no);
        //请求心跳包
        $device_sn = $this->client_data['device_sn'];
        $this->reply($device_sn, '0D');
    }

    /**
     * 修改强制冲洗时间------------->11指令
     * @throws \Exception
     * @date 2018/8/10 14:07
     * @author ligang
     */
    public function modify_mandatory_flushing_times()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        $update = [
            'forced_flushing_time' => hexdec($data)
        ];

        $result = $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
        if ($result) {
            return false;
        }
    }

    /**
     * 定时冲洗参数修改------------->12指令
     * @author ligang
     * @date 2018/7/30 16:54
     */
    public function modified_timed_washing()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        $update = [
            'wash_regularly' => hexdec($data)
        ];

        $result = $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
        if ($result) {
            return false;
        }

    }

    /**
     * 检修参数修改------------->13指令
     * @author ligang
     * @date 2018/7/30 16:55
     */
    public function modify_maintenance_parameters()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        $update = [
            'maintenance_duration' => round(hexdec($data) / 3600, 4)
        ];
        $result = $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
        if ($result) {
            return false;
        }
    }

    /**
     * 控制参数修改1(TDS值设置)------------->14指令
     * @author ligang
     * @date 2018/7/30 16:55
     */
    public function modify_control_parameter_one()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $type = hexdec(substr($data, 0, 2));
        if ($type == 1) {
            $update = [
                'pure_water_TDS_detection_switch' => hexdec(substr($data, 2, 2)),
                'pure_water_default' => hexdec(substr($data, 4)),
            ];
        } elseif ($type == 2) {
            $update = [
                'raw_water_TDS_detection_switch' => hexdec(substr($data, 2, 2)),
                'raw_water_default' => hexdec(substr($data, 4)),
            ];
        } else {
            $update = [];
        }
        if (!empty($update)) {
            $result = $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
            if ($result) {
                return false;
            }
        }
    }

    /**
     * 控制参数修改2(联网参数修改)------------->15指令
     * @author ligang
     * @date 2018/7/30 16:55
     */
    public function modify_control_parameter_two()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $type = hexdec(substr($data, 0, 2));
        if ($type == 1) {
            $update = [
                'heartbeat_interval' => hexdec(substr($data, 2))
            ];
        } elseif ($type == 2) {
            $update = [
                'broken_network_reconnection' => hexdec(substr($data, 2))
            ];
        } else {
            $update = [];
        }
        if (!empty($update)) {
            $result = $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
            if ($result) {
                return false;
            }
        }
    }

    /**
     * 开启关闭测试模式------------->16指令
     * @throws \Exception
     * @date 2018/8/10 14:00
     * @author ligang
     */
    public function switch_test_mode()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $update = [
            'debug_mode' => hexdec($data)
        ];
        $result = $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
        $msg = hexdec($data) ? '关闭' : '开启';
        if ($result) {
            return false;
        }
    }

    /**
     * 电脑板时间同步1(设备主动上传)------------->17指令
     * @throws \Exception
     * @date 2018/8/10 14:00
     * @author ligang
     */
    public function device_time_sync()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        //获取设备信息
        $field = '
            used_days,
            remaining_days,
            used_traffic,
            remaining_traffic
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }
        //服务端时间
        $time = time();
        //设备时间
        $device_time = hexdec($data);
        //误差时间
        $error_time = abs($time - $device_time);

        //写入设备时间同步日志
        $log = [
            'device_time' => date('Y-m-d H:i:s', $device_time),
            'server_time' => date('Y-m-d H:i:s', time()),
            'error_time' => $error_time
        ];
        FileLogService::WriteLog(SLog::DEVICE_TIME_SYNC_LOG, $this->device_no, $log);

        //误差时间在大于1h时，开始同步时间和同步数据
        if ($error_time > 360) {
            $command = $device_sn . '18' . $this->dechexBin(time(), 10);
            $this->sendToUid($device_sn, $command);
            $update = [
                'error_time' => $error_time
            ];
            $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
            return false;
        }
    }

    /**
     * 电脑板时间同步2(服务端主动下发)------------->18指令
     * @throws \Exception
     * @date 2018/8/10 13:56
     * @author ligang
     */
    public function computer_board_time_sync()
    {
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<时间同步<<<电脑板时间同步2(服务端主动下发)，，设备编号：' . $this->device_no);
    }

    /**
     * 用水量同步(已用流量)------------->AA指令
     * @throws \Exception
     * @date 2018/8/10 13:56
     * @author ligang
     */
    public function water_used_traffic()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        //获取设备信息
        $field = '
            used_traffic
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }
        $value = hexdec($data);
        if ($value != $equipmentInfo['used_traffic']) {
            $update = [
                'used_traffic' => $value
            ];
            $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
            /**
             * 更改心跳包redis
             * 为保证redis和数据同步
             */
            $redis_info = $this->redis->get('device_heartbeat:' . $this->device_no);
            if ($redis_info) {
                $redis_info = json_decode($redis_info, 1);
                $redis_info['data']['used_traffic'] = $value;
                $this->redis->setex('device_heartbeat:' . $this->device_no, '360', json_encode($redis_info));
            }
            return false;
        }
    }

    /**
     * 用水量同步(剩余流量)------------->AB指令
     * @throws \Exception
     * @date 2018/8/10 13:56
     * @author ligang
     */
    public function water_remaining_traffic()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        //获取设备信息
        $field = '
            remaining_traffic
        ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
        if (empty($equipmentInfo)) {
            $this->close();
            return false;
        }
        $value = hexdec($data);
        if ($value != $equipmentInfo['remaining_traffic']) {
            $update = [
                'remaining_traffic' => $value
            ];
            $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
            /**
             * 更改心跳包redis
             * 为保证redis和数据同步
             */
            $redis_info = $this->redis->get('device_heartbeat:' . $this->device_no);
            if ($redis_info) {
                $redis_info = json_decode($redis_info, 1);
                $redis_info['data']['remaining_traffic'] = $value;
                $this->redis->setex('device_heartbeat:' . $this->device_no, '360', json_encode($redis_info));
            }
            return false;
        }
    }

    /**
     * 远程升级请求：（获取版本号）)------------->1A指令
     * @author ligang
     * @date 2018/8/2 9:38
     */
    public function version()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        $update = [
            'version' => $this->Hex2String($data)
        ];
        //直接更新
        $result = $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
        if ($result) {
            return false;
        }
    }

    /**
     * 远程固件下发与接收------------->2A指令（起始帧）
     * @return bool
     * @throws \Exception
     * @date 2018/8/2 15:19
     * @author ligang
     */
    public function remote_firmware_start()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $redis_info = $this->redis->get('remoteFirmware:' . $this->device_no);
        $redis_info = json_decode($redis_info, 1);
        //设备应答失败，重新下发起始帧
        if (hexdec($data) != 1) {
            //失败次数操作三次，不在下发
            if ($redis_info['error'] > 3) {
                //删除redis
                $this->redis->delete('remoteFirmware:' . $this->device_no);
                //更改设备升级记录
                $device_info = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], 'equipment_id');
                if (!empty($device_info)) {
                    $log = $this->EquipmentUpgradeLogEquipment->getOne(['equipment_id' => $device_info['equipment_id'], 'status' => 1]);
                    if (!empty($log)) {
                        $this->EquipmentUpgradeLogEquipment->save(['upgrade_equipment_id' => $log['upgrade_equipment_id']], ['error' => 3, 'status' => 3, 'end_time' => time()]);
                    }
                }
                return false;
            }

            //          设备号|指令|数据帧
            $command = $device_sn . '2A' . $redis_info['data'][0];
            $this->sendToUid($device_sn, $command);

            $data = [
                'data' => $redis_info['data'], //分包结果
                'version' => $redis_info['version'], //当前版本
                'error' => $redis_info['error'] + 1, //失败次数（失败三次后，不在下发）
                'total' => $redis_info['total'], //共需要发送几次
                'current_time' => time(), //当前发送帧的时间
                'current' => $redis_info['current']       //当前发送分包的KEY,如果失败，将再次发送
            ];
            $this->redis->set('remoteFirmware:' . $this->device_no, json_encode($data));
            return false;
        }

        if ($redis_info) {
            if ($redis_info['current'] == $redis_info['total']) {
                return false;
            }
            if ($redis_info['error'] > 3) {
                FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<远程升级<<数据包已下发失败超过3次，停止下发：' . $this->device_no);
                return false;
            }
            //判断结束帧
            if (($redis_info['current'] + 1) == ($redis_info['total'] - 1)) {
                $instruct = '4A';
            } else {
                //数据帧
                $instruct = '3A';
            }
            //          设备号|指令|数据帧
            $command = $device_sn . $instruct . $redis_info['data'][$redis_info['current'] + 1];
            sleepCoroutine(1000);
            $this->sendToUid($device_sn, $command);
//            $this->send($command);
            //更改redis
            //将结果存储到redis
            $data = [
                'data' => $redis_info['data'], //分包结果
                'version' => $redis_info['version'], //当前版本
                'error' => $redis_info['error'], //失败次数（失败三次后，不在下发）
                'total' => $redis_info['total'], //共需要发送几次
                'current_time' => time(), //当前发送帧的时间
                'current' => $redis_info['current'] + 1       //当前发送分包的KEY,如果失败，将再次发送
            ];
            $this->redis->set('remoteFirmware:' . $this->device_no, json_encode($data));
            return $this->jsonend(1000, '发送成功');
        } else {

        }
    }

    /**
     * 远程固件下发与接收------------->3A指令（数据帧）
     * @return bool
     * @throws \Exception
     * @date 2018/8/2 15:20
     * @author ligang
     */
    public function remote_firmware_process()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        $redis_info = $this->redis->get('remoteFirmware:' . $this->device_no);
        $redis_info = json_decode($redis_info, 1);

        if (hexdec($data) != 1) {
            //失败次数操作三次，不在下发
            if ($redis_info['error'] > 3) {
                //删除redis
                $this->redis->delete('remoteFirmware:' . $this->device_no);
                //更改设备升级记录
                $device_info = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], 'equipment_id');
                if (!empty($device_info)) {
                    $log = $this->EquipmentUpgradeLogEquipment->getOne(['equipment_id' => $device_info['equipment_id'], 'status' => 1]);
                    if (!empty($log)) {
                        $this->EquipmentUpgradeLogEquipment->save(['upgrade_equipment_id' => $log['upgrade_equipment_id']], ['error' => 3, 'status' => 3, 'end_time' => time()]);
                    }
                }
                return false;
            }
            //判断结束帧
            if (($redis_info['current'] + 1) == ($redis_info['total'] - 1)) {
                $instruct = '4A';
            } else {
                //数据帧
                $instruct = '3A';
            }
            $command = $device_sn . $instruct . $redis_info['data'][$redis_info['current']];
            $this->sendToUid($device_sn, $command);
            $data = [
                'data' => $redis_info['data'], //分包结果
                'version' => $redis_info['version'], //当前版本
                'error' => $redis_info['error'] + 1, //失败次数（失败三次后，不在下发）
                'total' => $redis_info['total'], //共需要发送几次
                'current_time' => time(), //当前发送帧的时间
                'current' => $redis_info['current']       //当前发送分包的KEY,如果失败，将再次发送
            ];
            $this->redis->set('remoteFirmware:' . $this->device_no, json_encode($data));
            return false;
        }

        if ($redis_info) {
            if ($redis_info['current'] == $redis_info['total']) {
                FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<远程升级<<数据包已下发完毕,设备编号：' . $this->device_no);
                return false;
            }
            if ($redis_info['error'] == 3) {
                FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<远程升级<<数据包已下发失败超过3次停止下发,设备编号：' . $this->device_no);
                return false;
            }
            //判断结束帧
            if (($redis_info['current'] + 1) == ($redis_info['total'] - 1)) {
                $instruct = '4A';
            } else {
                //数据帧
                $instruct = '3A';
            }
            //          设备号|指令|数据帧
            $command = $device_sn . $instruct . $redis_info['data'][$redis_info['current'] + 1];
            sleepCoroutine(1000);
            $this->sendToUid($device_sn, $command);
//            $this->send($command);
            //更改redis
            //将结果存储到redis
            $data = [
                'data' => $redis_info['data'], //分包结果
                'version' => $redis_info['version'], //当前版本
                'error' => $redis_info['error'], //失败次数（失败三次后，不在下发）
                'total' => $redis_info['total'], //共需要发送几次
                'current_time' => time(), //当前发送帧的时间
                'current' => $redis_info['current'] + 1       //当前发送分包的KEY,如果失败，将再次发送
            ];
            $this->redis->set('remoteFirmware:' . $this->device_no, json_encode($data));
            return $this->jsonend(1000, '发送成功');
        } else {

        }
    }

    /**
     * 远程固件下发与接收------------->4A指令（结束帧）
     * @throws \Exception
     * @date 2018/8/2 17:04
     * @author ligang
     */
    public function remote_firmware_end()
    {
        /**
         * 文件传输完毕，收到应答
         */
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        $redis_info = $this->redis->get('remoteFirmware:' . $this->device_no);
        $redis_info = json_decode($redis_info, 1);
        if (hexdec($data) != 1) {
            //失败次数操作三次，不在下发
            if ($redis_info['error'] > 3) {
                //删除redis
                $this->redis->delete('remoteFirmware:' . $this->device_no);
                //更改设备升级记录
                $device_info = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], 'equipment_id');
                if (!empty($device_info)) {
                    $log = $this->EquipmentUpgradeLogEquipment->getOne(['equipment_id' => $device_info['equipment_id'], 'status' => 1]);
                    if (!empty($log)) {
                        $this->EquipmentUpgradeLogEquipment->save(['upgrade_equipment_id' => $log['upgrade_equipment_id']], ['error' => 3, 'status' => 3, 'end_time' => time()]);
                    }
                }
                return false;
            }
            $command = $device_sn . '4A' . $redis_info['data'][$redis_info['current']];
            $this->sendToUid($device_sn, $command);
            $data = [
                'data' => $redis_info['data'], //分包结果
                'version' => $redis_info['version'], //当前版本
                'error' => $redis_info['error'] + 1, //失败次数（失败三次后，不在下发）
                'total' => $redis_info['total'], //共需要发送几次
                'current_time' => time(), //当前发送帧的时间
                'current' => $redis_info['current']       //当前发送分包的KEY,如果失败，将再次发送
            ];
            $this->redis->set('remoteFirmware:' . $this->device_no, json_encode($data));
            return false;
        }

        //删除redis
        $this->redis->delete('remoteFirmware:' . $this->device_no);

        $check = false;
        $this->db->begin(function () use ($device_sn, $redis_info, &$check) {
            //更新版本
            $update = [
                'version' => $redis_info['version']
            ];
            //直接更新
            $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
            //更改设备升级记录
            $device_info = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], 'equipment_id');
            if (!empty($device_info)) {
                $log = $this->EquipmentUpgradeLogEquipment->getOne(['equipment_id' => $device_info['equipment_id'], 'status' => 1]);
                if (!empty($log)) {
                    $this->EquipmentUpgradeLogEquipment->save(['upgrade_equipment_id' => $log['upgrade_equipment_id']], ['error' => $redis_info['error'], 'status' => 2, 'end_time' => time()]);
                }
            }
            $check = true;
        }, function ($e) {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<远程升级<<服务端更新失败,设备编号：' . $this->device_no);
            return false;
        });
        //是否保持一致性？不保持一致性（服务端与设备不一致）则不判断服务端是否操作成功
        if ($check) {
            /**
             * 设备更新成功后，下发设备下次运行新固件，然后重启设备
             */
            $this->sendToUid($device_sn, $device_sn . '8A01');
            sleepCoroutine(2000);
            $this->sendToUid($device_sn, $device_sn . '7A01');
            return false;
        }

    }

    /**
     * 屏幕显示模式切换------------->5A指令
     * @author ligang
     * @date 2018/8/13 9:40
     */
    public function switch_screen_display_mode()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        switch ($data) {
            case '0200':
            case '0002':
                $value = 1;
                break;
            case '0300':
            case '0003':
                $value = 2;
                break;
            case '0201':
            case '0102':
                $value = 3;
                break;
            case '0301':
            case '0103':
                $value = 4;
                break;
            default:
                $value = 1;
        }
        if (!empty($value)) {
            $update = [
                'screen_mode' => $value
            ];
            //直接更新
            $result = $this->EquipmentListsModel->updateEquipmentLists($update, ['device_no' => $this->device_no]);
            if ($result) {
                return false;
            }
        }
    }

    /**
     * 检修状态切换------------->6A指令
     * @author ligang
     * @date 2018/8/13 10:41
     */
    public function maintenance_state_switching()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        $value = hexdec($data);
        $this->EquipmentListsModel->updateEquipmentLists(['repair_state' => $value], ['device_no' => $this->device_no]);
        if ($value == 0) {
            return false;
        }
    }

    /**
     * 远程重启------------->7A指令
     * @author ligang
     * @date 2018/9/28 11:23
     */
    public function remote_restart()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<远程重启<<远程重启,设备编号：' . $this->device_no);
    }

    /**
     * 下次启动设备时是否运行新固件------------->8A指令
     * @author ligang
     * @date 2018/9/28 11:23
     */
    public function run_new_firmware()
    {

        FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<下次启动设备时是否运行新固件，设备编号：' . $this->device_no);
    }

    /**
     * 运行固件切换------------->9A指令
     * @author ligang
     * @date 2018/9/28 11:23
     */
    public function firmware_switch()
    {
        $device_sn = $this->client_data['device_sn'];
        $data = $this->client_data['data'];

        if (hexdec($data) == 1) {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<固件切换<<<固件切换成功，设备编号：' . $this->device_no);
        } else {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<固件切换<<<固件切换失败，设备编号：' . $this->device_no);
        }
    }

    /**
     * 关闭开启蜂鸣器（设备收到数据时鸣叫）
     * @author ligang
     * @date 2019/6/20 16:08
     */
    public function buzzer()
    {
        $data = $this->client_data['data'];
        $msg = hexdec($data) == 1 ? '关闭' : '开启';
        var_dump('<<<蜂鸣器<<<蜂鸣器成功' . $msg . '设备编号：' . $this->device_no);
        $this->Achievement->table = 'equipment_lists';
        $this->Achievement->updateData(['device_no' => $this->device_no], ['buzzer' => hexdec($data)]);
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<蜂鸣器<<<蜂鸣器成功' . $msg . '设备编号：' . $this->device_no);
    }

    /**
     * 下发设备空数据
     * 请求参数     请求类型      是否必须      说明
     * $sn          string          是           设备号
     * $command     string          否           指令，默认01
     * $content     string          否           数据内容（十六进制）
     * @param string $sn
     * @param string $command
     * @param string $content
     * @throws \Exception
     * @date 2018/8/24 14:56
     * @author ligang
     */
    protected function reply(string $sn, string $command = '01', string $content = '00')
    {
        $data = $sn . $command . $content;
        $res = $this->sendToUid($sn, $data);

    }

    /**
     * 给设备编号加锁，防止重复添加
     * 请求参数     请求类型      是否必须      说明
     * device_no    string          是           设备编号
     * redis_key    string          否           需要枷锁的KEY，默认心跳检查KEY
     * @param string $device_no
     * @param string $redis_key
     * @return bool
     * @date 2018/11/16 9:43
     * @author ligang
     */
    protected function LockDevice(string $device_no, string $redis_key = 'checkDeviceNumber')
    {
        /*
        $expire = 5;
        $lock_key = 'LOCK_PREFIX' . $redis_key;
        $is_lock = $this->redis->setnx($lock_key, 1); // 加锁
        if ($is_lock == true) { // 获取锁权限
            $this->redis->setex($lock_key, $expire, $device_no); // 写入内容
            return false;
        } else {
            // 防止死锁
            if ($this->redis->ttl($lock_key) == -1) {
                $this->redis->expire($lock_key, 5);
            }
            return true;
        }
        */
        /*
        $lock_key = 'LOCK_PREFIX' . $redis_key;
        $lock = $this->redis->set($lock_key,$device_no,['NX','EX'=>5]);//成功返回‘OK’，否则为null
        if ($lock){
            return false;
        }
        return true;
        */
        //若出现大量设备离线的情况清除差Memcached服务是否存在
        //若不存在则执行命令：memcached -p 11211 -m 1024m -d -u root
        //请先安装Memcached服务以及PHP的Memcached扩展
        $Memcached = Memcached::init();
        $lock = $Memcached->add($redis_key . $device_no, 1, 5);
        if ($lock) {
            return false;
        }
        //  return true;
        return false;
    }


    /**
     * 获取城市TDS
     * @desc 获取城市TDS
     * @param string $code 设备区县码
     * @param int $tds
     * @return int
     * @date 2019/5/5 15:05
     * @author ligang
     */
    protected function CityTDS(string $code = '', int $tds = 0)
    {
        $cityTDS = $this->redis->get('CityTDS');
        if (!$cityTDS) {
            $this->Achievement->table = 'address_tds';
            $cityTDS = $this->Achievement->selectData([], '*', [], []);
            $this->redis->setex('CityTDS', '86400', json_encode($cityTDS));
        } else {
            $cityTDS = json_decode($cityTDS, 1);
        }
        if (empty($cityTDS) || empty($code)) {
            return ceil($tds * 0.8);
        }

        $search = array_column($cityTDS, 'a_code');
        $key = array_search($code, $search);
        if ($key === false) {
            return ceil($tds * 0.8);
        }
        if ($cityTDS[$key]['tds'] < $tds) {
            $max = $cityTDS[$key]['tds'];
        } else {
            $max = ceil($tds * 0.8);
        }
        return $max;
    }

    public function iccid()
    {
        $data = $this->client_data['data'];
        $iccid = $this->Hex2String($data);
        $this->Achievement->table = 'equipment_library';
        $this->Achievement->updateData(['device_no' => $this->device_no], ['sim_sn' => $iccid]);
        var_dump($this->device_no . '回复ICCID:' . $this->client_data['data'] . '====' . $iccid);
    }

    /**
     * 中移设备推送数据流API
     * @return mixed
     */
    public function http_oneNetReport()
    {

//        require_once APP_DIR . '/Library/cmiot/Util.php';
//        /**
//         * 设备推送数据流
//         */
//        $raw_input = $this->http_input->getAllPostGet();
//        $resolved_body = Util::resolveBody(json_encode($raw_input));
//        /**
//         * 最后得到的$resolved_body就是推送过后的数据
//         * At last, var $resolved_body is the data that is pushed
//         */
//        Util::l($resolved_body);
//        return $this->echoMsg($resolved_body);

//
        require_once APP_DIR . '/Library/cmiot/Util.php';
        /**
         * 设备推送数据流
         */
        $raw_input = json_decode($this->http_input->getRawContent(), true);
        if (empty($raw_input)) {
            return false;
        }
        $report_data = json_decode($raw_input['msg'], true);
        try {
            $this->handle($report_data);
        } catch (\Exception $e) {
            FileLogService::WriteLog(SLog::DEBUG_LOG, 'debug', '<<<心跳日志写入失败<<<失败原因：' . $e->getMessage(), true);
        }

        $resolved_body = Util::resolveBody(json_encode($raw_input));
        Util::l($resolved_body);
        return $this->echoMsg($resolved_body);//回复
    }

    /**
     * @param $data
     * @return bool
     */
    public function handle($data)
    {
        //开始处理具体业务逻辑 文档地址:https://open.iot.10086.cn/doc/v5/fuse/detail/1520
        $productId = $data['productId'] ?? '';
        $deviceName = $data['deviceName'] ?? '';
        $messageType = $data['messageType'] ?? '';
        $notifyType = $data['notifyType'] ?? '';
        $device_data = $data['data'] ?? [];
        $type = $data['type'] ?? '';

        if ($type == 2) {//设备在线离线更新
            FileLogService::WriteLog(SLog::DEVICE_HEARTBEAT_LOG, $data['dev_name'], '<<<中移设备上报设备在线离线<<<' . json_encode($data), true);
            $status = $data['status'] ?? 0;
            $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $data['dev_name']]);
            if (!empty($equipmentInfo)) {
                $this->EquipmentListsModel->updateEquipmentLists(['status' => $status + 1], ['device_no' => $data['dev_name']]);
            }
            return true;
        }

        //阻塞redis防止为null,且设置超时时间（5s）
        $timeout = 100 * 50;
        $is_null = false;
        while ($this->redis == null) {
            sleepCoroutine(100);
            $timeout -= 100;
            if ($timeout <= 0) {
                $is_null = true;
                break;
            }
        }
        if ($is_null) {
            //本次redis获取失败，直接结束
            return false;
        }

        $exists_library = $this->redis->get('4g_library:' . $deviceName);
        // 非空等于true
        if ($exists_library) {
            // 不存在于设备库中
            if ($exists_library == -1) {
                return false;
            }
        } else {
            // 获取设备的设备库信息
            $equipment_library_info = $this->EquipmentLibraryModel->getOne(['device_no' => $deviceName]);
            if (empty($equipment_library_info)) {
                $this->redis->setex('4g_library:' . $deviceName, 360, -1);
                return false;
            } else {
                $this->redis->setex('4g_library:' . $deviceName, 360, 1);
            }
        }

        $exists_equipment_list = $this->redis->get('device_heartbeat:' . $deviceName);
        $equipment_redis_data = empty($exists_equipment_list) ? '' : json_decode($exists_equipment_list, true);
        //REDIS数据初始化
        if (!$exists_equipment_list) {
            // 获取设备列表中信息
            $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $deviceName]);
            if (empty($equipmentInfo)) {
                $e_data = [
                    'create_time' => time(),
                    'device_no' => $deviceName,
//                    'status' => 2,
                    'switch_machine' => 1, // 开关机，0：关机，1：开机
                ];
                // 写入设备表
                $this->EquipmentListsModel->insertEquipmentLists($e_data);
                return false;
            } else {
                $equipment_redis_data = [
                    'data' => [
                        'equipment_id' => $equipmentInfo['equipment_id'], //设备ID
//                        'status' => 2, //设备状态
                        'water_making_state' => $equipmentInfo['water_making_state'], //设备ID
                        'device_status' => $equipmentInfo['device_status'], //设备状态
                        'screen_status' => $equipmentInfo['screen_status'], //屏幕状态
                        'working_mode' => $equipmentInfo['working_mode'], //工作模式
                        'remaining_traffic' => $equipmentInfo['remaining_traffic'], //剩余流量
                        'remaining_days' => $equipmentInfo['remaining_days'], //剩余天数
                        'used_traffic' => $equipmentInfo['used_traffic'], //已用流量
                        'used_days' => $equipmentInfo['used_days'], //已用天数
                        'water_purification_TDS' => $equipmentInfo['water_purification_TDS'], //净水TDS
                        'raw_water_TDS' => $equipmentInfo['raw_water_TDS'], //原水TDS
                        'signal_strength_value' => $equipmentInfo['signal_strength_value'], //信号强度值
                        'lac_value' => $equipmentInfo['lac_value'], //LAC值
                        'cid_value' => $equipmentInfo['cid_value'] //CID值
                    ],
                    'time' => time()
                ];
              //  $this->EquipmentListsModel->updateEquipmentLists(['status' => 2], ['device_no' => $deviceName]);
                $this->redis->setex('device_heartbeat:' . $deviceName, 360, json_encode($equipment_redis_data, true));
            }
        }


        FileLogService::WriteLog(SLog::DEVICE_HEARTBEAT_LOG, $deviceName, '<<<中移设备上报<<<' . json_encode($data), true);
        if ($messageType == 'notify' && $notifyType == 'property') {
            $flag = false;
//            if ($equipment_redis_data['data']['status'] != 2) {
//                $flag = true;
//            }
            if ($equipment_redis_data['data']['signal_strength_value'] != $device_data['params']['CSQ']['value']) {
                $flag = true;
            }
            if ($equipment_redis_data['data']['water_purification_TDS'] != $device_data['params']['purified_water']['value']) {
                $flag = true;
            }
            if ($equipment_redis_data['data']['device_status'] != $device_data['params']['device_state']['value']) {
                $flag = true;
            }
            if ($equipment_redis_data['data']['working_mode'] != $device_data['params']['working_mode']['value']) {
                $flag = true;
            }
            if ($equipment_redis_data['data']['used_traffic'] != $device_data['params']['used_traffic']['value']) {
                $flag = true;
            }
            if ($equipment_redis_data['data']['raw_water_TDS'] != $device_data['params']['raw_water']['value']) {
                $flag = true;
            }
            if ($equipment_redis_data['data']['remaining_traffic'] != $device_data['params']['residual_flow']['value']) {
                $flag = true;
            }
            if ($equipment_redis_data['data']['screen_status'] != $device_data['params']['screen_status']['value']) {
                $flag = true;
            }
            if ($equipment_redis_data['data']['remaining_days'] != $device_data['params']['days_remaining']['value']) {
                $flag = true;
            }
            if ($equipment_redis_data['data']['used_days'] != $device_data['params']['days_used']['value']) {
                $flag = true;
            }

            $data = [
                'signal_strength_value' => $device_data['params']['CSQ']['value'], // 信号强度
                'water_purification_TDS' => $device_data['params']['purified_water']['value'], // 净水TDS
                'device_status' => $device_data['params']['device_state']['value'], // 设备状态 0 - 正常 1 - 待激活 3 - 欠费 5 - 硬件调试
                'working_mode' => $device_data['params']['working_mode']['value'], // 工作模式 0 - 流量模式 1 - 时长模式
                'used_traffic' => $device_data['params']['used_traffic']['value'], // 已用流量
                'raw_water_TDS' => $device_data['params']['raw_water']['value'], // 原水TDS
                'remaining_traffic' => $device_data['params']['residual_flow']['value'], // 剩余流量
                'screen_status' => $device_data['params']['screen_status']['value'], // 屏幕状态 0 - 屏幕打开 1 - 屏幕关闭
                'remaining_days' => $device_data['params']['days_remaining']['value'], // 剩余天数
                'used_days' => $device_data['params']['days_used']['value'], // 已用天数
                'shutdown_state' => ($device_data['params']['days_remaining']['value'] == 0) ? 1 : 0, // 是否停机 0否 1是
//                'status' => 2,
                'switch_machine' => 1, // 开关机，0：关机，1：开机
            ];
            $equipment_redis_data = [
                'data' => [
                    'equipment_id' => $equipment_redis_data['data']['equipment_id'], //设备ID
//                    'status' => 2, //设备状态
                    'water_making_state' => $equipment_redis_data['data']['water_making_state'], //设备ID
                    'device_status' => $data['device_status'], //设备状态
                    'screen_status' => $data['screen_status'], //屏幕状态
                    'working_mode' => $data['working_mode'], //工作模式
                    'remaining_traffic' => $data['remaining_traffic'], //剩余流量
                    'remaining_days' => $data['remaining_days'], //剩余天数
                    'used_traffic' => $data['used_traffic'], //已用流量
                    'used_days' => $data['used_days'], //已用天数
                    'water_purification_TDS' => $data['water_purification_TDS'], //净水TDS
                    'raw_water_TDS' => $data['raw_water_TDS'], //原水TDS
                    'signal_strength_value' => $data['signal_strength_value'], //信号强度值
                    'lac_value' => $equipment_redis_data['data']['lac_value'], //LAC值
                    'cid_value' => $equipment_redis_data['data']['cid_value'] //CID值
                ],
                'time' => time()
            ];
            $this->redis->setex('device_heartbeat:' . $deviceName, 360, json_encode($equipment_redis_data, true));
            if ($flag) {
                // 更新设备信息
                $this->EquipmentListsModel->updateEquipmentLists($data, ['device_no' => $deviceName]);
                return true;
            }
        }
        return false;
    }


    public function oneNetHandle($data)
    {
        //开始处理具体业务逻辑 文档地址:https://open.iot.10086.cn/doc/v5/fuse/detail/1520
        $type = $data['type'] ?? '';
        $messageType = $data['messageType'] ?? '';
        $notifyType = $data['notifyType'] ?? '';
        $this->device_no = $data['deviceName'];
        $field = '
                equipment_id,
                status,
                water_making_state,
                device_status,
                screen_status,
                working_mode,
                remaining_traffic,
                remaining_days,
                used_traffic,
                used_days,
                water_purification_TDS,
                raw_water_TDS,
                signal_strength_value,
                lac_value,
                cid_value,
                shutdown_state
            ';
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $this->device_no], $field);
//        var_dump($equipmentInfo);
        FileLogService::WriteLog(SLog::DEVICE_HEARTBEAT_LOG, $this->device_no, '<<<中移设备上报<<<' . json_encode($data), true);
        if ($type == 2) {//设备在线离线更新
            $status = $data['status'] ?? 0;
            if (!empty($equipmentInfo)) {
                $this->EquipmentListsModel->updateEquipmentLists(['status' => $status + 1], ['device_no' => $this->device_no]);
            }
            return true;
        }
        //物理模型-设备属性(心跳)
        if ($messageType == 'notify' && $notifyType == 'property') {
            $params = $data['data']['params'];
//            $edit_device['status'] = 2;
            $edit_device['device_status'] = $params['device_state']['value'];
            $edit_device['signal_strength_value'] = bcdiv((string)$params['CSQ']['value'], "2", 0);
            $edit_device['water_purification_TDS'] = $params['purified_water']['value'];
            $edit_device['raw_water_TDS'] = $params['raw_water']['value'];
            $edit_device['working_mode'] = $params['working_mode']['value'];
            //    $edit_device['used_traffic'] = $params['used_traffic']['value'];
            // $edit_device['remaining_traffic'] = $params['residual_flow']['value'];
            $edit_device['screen_status'] = $params['screen_status']['value'];
            $edit_device['remaining_days'] = $params['days_remaining']['value'];
            $edit_device['used_days'] = $params['days_used']['value'];
            if (!empty($equipmentInfo)) {
                $this->EquipmentListsModel->updateEquipmentLists($edit_device, ['device_no' => $this->device_no]);
            }
            return true;
        }
    }

    // 套餐绑定
    public function http_bindingPackage()
    {
        $param = $this->parm;
        $equipmentInfo = $this->EquipmentListsModel->findEquipmentLists(['device_no' => $param['sn']], 'switch_machine');
        if (empty($equipmentInfo)) {
            return $this->jsonend(-1000, '设备不存在，设备编号：' . $param['sn']);
        }
        $result = $this->OneNetApi->packageSet($param['sn'], $param['working_mode'], $param['filter_element_max']);
        return $this->jsonend(1000, '下发套餐成功');
    }

    // 时间数据同步
    public function http_usedTimeSet()
    {
        $params = $this->http_input->request->post;
        $identifier = 'used_time_set';
        $data = [
            'used_days' => 65,
            'remain_days' => 300,
            'used_traffic' => 10000,
            'remain_flow' => 100000,
            'aacmd08' => '1'
        ];
        $result = $this->OneNetApi->callService($params['device_sn'], $identifier, $data);
    }

    public function http_getState()
    {
        $params = $this->http_input->request->post;
        dump($params);
        $result = $this->OneNetApi->getState($params['device_sn'], 1);
        dump($result);
    }

    // 开启关闭制水功能
    public function http_openWaterProduction()
    {
        $param = $this->http_input->getAllPostGet();
        dump($param);
        $result = $this->OneNetApi->openWaterProduction($param['device_no'], intval($param['water_production_switch']));
        dump($result);
    }

    // 剩余天数设置
    public function http_remainDays()
    {
        $param = $this->http_input->getAllPostGet();
        dump($param);
        $result = $this->OneNetApi->remainDays($param['device_no'], intval($param['remain_days_set']));
        dump($result);
    }

    // 屏幕开关
    public function http_screenSwitch()
    {
        $param = $this->parm;
        $result = $this->OneNetApi->screenSwitch($param['sn'], intval($param['screen_status']));
        return $this->jsonend(1000, '操作成功');
    }

    // 设备属性最新数据查询
    public function http_queryDeviceProperty()
    {
        $params = $this->http_input->request->post;
//        $param = $this->http_input->getAllPostGet();
//        dump($param);
        dump($params);
        $result = $this->OneNetApi->queryDeviceProperty($params['sn']);
        dump($result);
    }

    // 获取设备属性详情
    public function http_queryDevicePropertyDetail()
    {
//        $param = $this->parm;
//        dump($param);
        $param = $this->http_input->getAllPostGet();
        dump($param);
        $result = $this->OneNetApi->queryDevicePropertyDetail($param['sn'], []);
    }

    public function http_overhaulTestSet()
    {
        $param = $this->http_input->getAllPostGet();
        dump($param);

        // 参数值
        //参数描述
        //0 无效
        //1 进入测试模式
        //2 退出测试模式
        //3 进入检修模式
        //4 退出检修模式
        $result = $this->OneNetApi->overhaulTestSet($param['sn'], intval($param['overhaul_test_switch']));
        dump($result);
    }

    // 获取滤芯状态
    public function http_getFilterState()
    {
        $param = $this->http_input->getAllPostGet();
        dump($param);

        $result = $this->OneNetApi->getFilterState($param['sn'], intval($param['get_filter']));
        dump($result);
    }

    // 屏幕显示内容设置
    public function http_showContentsSet()
    {
        $param = $this->http_input->getAllPostGet();
        dump($param);

        $result = $this->OneNetApi->showContentsSet($param['sn']);
        dump($result);
    }

    // 获取滤芯状态
    public function http_factoryReset()
    {
//        $report_data = [
//            'type' => 2,
//            'status' => 0,
//            'deviceName' => '866179063289297'
//        ];
//        $this->handle($report_data);
        $param = $this->http_input->getAllPostGet();
        $result = $this->OneNetApi->factoryReset($param['sn'], 1);
        dump($result);
    }
}
