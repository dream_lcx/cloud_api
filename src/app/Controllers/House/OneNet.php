<?php

/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2018/7/23
 * Time: 10:41
 */

namespace app\Controllers\House;

use app\Services\Log\AsyncFile;
use cmiot\OneNetApi;
use cmiot\Util;
/**
 * 下发指令
 * Class Issue
 * @package app\Controllers\House
 */
class OneNet extends Base
{

    protected $EquipmentListsModel;
    protected $RemoteFileModel;
    protected $EquipmentUpgradeLog;
    protected $EquipmentUpgradeLogEquipment;
    protected $EquipmentLibraryModel;
    protected $device_sn;
    protected $Achievement;
    protected $OneNetApi;
    protected $device_protocol_version;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->EquipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->RemoteFileModel = $this->loader->model('RemoteFileModel', $this);
//        $this->EquipmentUpgradeLog = $this->loader->model('EquipmentUpgradeLog', $this);
        $this->EquipmentUpgradeLogEquipment = $this->loader->model('EquipmentUpgradeLogEquipment', $this);
        $this->EquipmentLibraryModel = $this->loader->model('EquipmentLibraryModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        //10进制设备编号，下发为16进制编号,总长度20
        if (!is_array($this->parm['sn']) && strlen($this->parm['sn']) > 0) {
            $device_library = $this->EquipmentLibraryModel->getOne(['device_no'=>$this->parm['sn']],'protocol_version');
            $this->device_protocol_version = empty($device_library['protocol_version'])?1:$device_library['protocol_version'];//硬件协议版本硬件协议版本 1:2G主板(2023年以前的主板)   2:刷卡版 3:4G主板(新4G主板(中移平台))
            if($this->device_protocol_version==3){
                $this->device_sn = $this->parm['sn'];
            }else{
                $this->device_sn = $this->dechexBin($this->parm['sn'], 20);//2G主板通信需要转
            }

        }
        /************************中移****************/
        require_once APP_DIR . '/Library/cmiot/OneNetApi.php';
        $this->OneNetApi = get_instance()->OneNetApi;
    }
    //下发套餐
    public function http_packageSet(){
        $filter_element_max = [180,180,180,365,365];
        $working_mode = $this->parm['working_mode']??1;
        $result = $this->OneNetApi->packageSet($this->parm['sn'],$working_mode,$filter_element_max);
        var_dump($this->parm['sn']."套餐下发");
        var_dump($result);
        return $this->jsonend(1000, '发送成功');
    }
    //获取属性
    public function http_queryDevicePropertyDetail(){
        $params = ["CSQ","days_remaining","days_used","device_state","leakage_state","maintenance_status","poweron_status","purified_water","raw_water","screen_status"];
        $result = $this->OneNetApi->queryDevicePropertyDetail($this->parm['sn'],$params);
        var_dump($this->parm['sn']."获取属性");
        var_dump($result);
        return $this->jsonend(1000, '发送成功');
    }

    // 设备属性最新数据查询
    public function http_queryDeviceProperty(){
        $result = $this->OneNetApi->queryDeviceProperty($this->parm['sn']);
        var_dump($this->parm['sn']);
        var_dump($result);
        return $this->jsonend(1000, '发送成功');
    }

    //获取设备详情
    public function http_device(){
        $result = $this->OneNetApi->device($this->parm['sn']);
        var_dump($this->parm['sn']."设备详情");
        var_dump($result);
        return $this->jsonend(1000, '发送成功');
    }

    //开关机
    public function http_powerOn(){
        $status = $this->parm['status']??"0";
        $params = ["screen_switch"=>$status];
        $result = $this->OneNetApi->setDeviceProperty($this->parm['sn'],$params);
        var_dump($this->parm['sn']."开关机");
        var_dump($result);
        return $this->jsonend(1000, '发送成功');
    }
    //屏幕开关
    public function http_screenSwitch(){
        $status = $this->parm['status']??0;
        $result = $this->OneNetApi->screenSwitch($this->parm['sn'],$status);
        var_dump($this->parm['sn']."屏幕状态");
        var_dump($result);
        return $this->jsonend(1000, '发送成功');
    }
    //屏幕显示内容设置
    public function http_showContentsSet(){
        $show_contents_day = $this->parm['show_contents_day']??1;
        $show_flow = $this->parm['show_flow']??2;
        $result = $this->OneNetApi->showContentsSet($this->parm['sn'],$show_contents_day,$show_flow);
        var_dump($this->parm['sn']."屏幕显示内容设置");
        var_dump($result);
        return $this->jsonend(1000, '发送成功');
    }
    //工作模式
    public function http_workModeSet(){
        $status = $this->parm['status']??1;
        $result = $this->OneNetApi->workModeSet($this->parm['sn'],$status);
        var_dump($this->parm['sn']."工作模式");
        var_dump($result);
        return $this->jsonend(1000, '发送成功');
    }
    //数据同步
    public function http_usedTimeSet(){
        $used_days = $this->parm['used_days']??2;
        $remain_days= $this->parm['remain_days']??365;
        $used_traffic=0;
        $remain_flow=100000000;
        $result = $this->OneNetApi->usedTimeSet($this->parm['sn'],$used_days,$remain_days,$used_traffic,$remain_flow);
        var_dump($this->parm['sn']."数据同步");
        var_dump($result);
        return $this->jsonend(1000, '发送成功');
    }

}
