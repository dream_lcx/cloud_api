<?php

namespace app\Controllers\External;

use app\Services\User\UserService;

class User extends Base
{

    public function http_checkUserByPhone()
    {
        if(empty($this->parm['phone'])){
            return [false,'缺少必要参数'];
        }
        $where = [
            'telphone'=>$this->parm['phone']
        ];
        $model = get_instance()->loader->model('CustomerModel', get_instance());
        $user = $model->getOne($where,'*');
        $data['is_set'] = 0;
        if(!empty($user)){
            $data['is_set'] = 1;
        }
        //return $this->show(200,'请求成功',$data);
        return $this->jsonend(1000, '获取成功', $data, true);
    }

    public function show($code,$message,$data = [])
    {
        $data = [
            'code'=>$code ?? 400,
            'message'=>$message ?? '请求失败',
            'data'=>$data
        ];
        $this->response->end($data);
        $this->endOver();
    }
}