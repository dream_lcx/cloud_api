<?php

namespace app\Controllers\User;

use app\Wechat\WxAuth;
use Server\Components\CatCache\CatCacheRpcProxy;

/**
 * 小程序公共API
 */
class Wx extends Base {

    protected $accessKey;
    protected $secrectKey;
    protected $bucket;   // 要上传的空间
    protected $user_model;

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
    }

    public function __construct() {
        parent::__construct();
        $this->accessKey = $this->config->get('qiniu.accessKey');
        $this->secrectKey = $this->config->get('qiniu.secrectKey');
        $this->bucket = $this->config->get('qiniu.bucket');
    }

    /**
     * showdoc
     * @catalog API文档/公共API/微信相关
     * @title 手机号解密
     * @description 获取小程序用户手机信息后进行解密
     * @method POST
     * @url User/Wx/aesPhoneDecrypt
     * @param session_key 必选 string 用户名
     * @param data 必选 string 密码
     * @param iv 可选 string 用户昵称
     * @return {"code": 1000, "message": "用户信息解析成功", "data": { "phoneNumber": "18883880448", "purePhoneNumber": "18883880448","countryCode": "86", "watermark": {"timestamp": 1539831979, "appid": "wx76e19f9eb5bcb658" } }
     * @return_param phoneNumber int 手机号
     * @return_param countryCode string 国家编号
     * @remark {data:"Q1ocwpmYHhzy+oe01hHpDQxv8wv1jckHMpVfLT2FLn8ipRLx4vrtZJ/SRb0ZAaqkvWRDUjeDj7HOkL8L9cw2fOcpfdABSvvJ6Gm7PXtZdgwEoALjvlm71O+QGAcSY+gDUXNOvQCB0v+vfvmsplJA2gIret3pL50rBzHLyVpF5LMEBKgiAXTUrWZ4YetWE/wQkQghrQRky2wI4yqCicDQig=="iv:"M1dbvvl8mZWTbtptBxAOiA=="openid:"o-abM4ve_N2NzGzadJtGB1gVbp4A"}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_aesPhoneDecrypt()
    {
        $wx_config = $this->wx_config;
        $session_key = CatCacheRpcProxy::getRpc()->offsetGet('session_key_' . $this->parm['openid']);
        if (empty($session_key)) {
            return $this->jsonend(-1001, "缺少参数session_key");
        }
        if (empty($this->parm['data'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数加密串");
        }
        if (empty($this->parm['iv'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数iv");
        }
        $encryptedData = $this->parm['data'];
        $iv = $this->parm['iv'];


        //$pc = new WXBizDataCrypt($wx_config['appId'], $sessionKey);
        $pc = get_instance()->WXBizDataCrypt;
        $pc->setSessionKey($session_key);
        $pc->setAppid($wx_config['appId']);

        $errCode = $pc->decryptData($encryptedData, $iv, $data);
        if ($errCode == 0) {
            $arr = json_decode($data, true);
            return $this->jsonend(1000, '用户信息解析成功', $arr);
        }
        return $this->jsonend(-1000, '用户信息解析失败');
    }



}
