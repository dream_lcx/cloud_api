<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2019-04-19
 * Time: 上午 10:56
 */
namespace app\Controllers\User;

/**
 * 用户浏览记录
 * Class Browse
 * @package app\Controllers\User
 */
class Browse extends Base{


    protected $browseMode;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name); // TODO: Change the autogenerated stub
        $this->browseMode = $this->loader->model('BrowseLogModel',$this);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/浏览记录相关
     * @title 浏览记录
     * @description
     * @method POST
     * @url User/Share/shareLog
     * @param browse_page 必选  页面路经(如首页传参index)
     * @return {"code":1000,"message":"添加成功","data":true}
     * @remark
     * @number 0
     * @author bg
     * @date 2019-04-19
     */
    public function http_browseLog(){
        $token = $this->request->header['token'] ?? '';
        $userInfo = getTokenUserInfo($token);
        if(!$userInfo){
            return $this->jsonend(-1000, '用户没有登录');
        }
        $browse_page = $this->parm['browse_page']??'';
        if(empty($browse_page)){
            return $this->jsonend(-1000, '参数错误');
        }
        $user_id = $userInfo['user_id'];
        $data = [
            'browse_uid' => $user_id,
            'browse_upid' => 0,
            'browse_page' => $browse_page,
            'browse_par' => json_encode($this->parm),
            'ctime' => time()
        ];
        try{
            $this->browseMode->insertBrowseLog($data);
            return $this->jsonend(1000, '添加成功');
        }catch (\Exception $e){
            return $this->jsonend(-1000, '添加失败');
        }
    }
}