<?php


namespace app\Controllers\User;


class Finance extends Base
{
    protected $fundsRecordModel;
    protected $oaWorkersModel;
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->fundsRecordModel = $this->loader->model('FundsRecordModel', $this);
        $this->oaWorkersModel = $this->loader->model('OaWorkersModel', $this);
    }
    //资金记录
    public function http_record(){
        $user_id = $this->user_id;
        if(!empty($this->parm['user_id'])){
            $user_id= $this->parm['user_id'];
        }
        $workers_info = $this->oaWorkersModel->getOne(['user_id'=>$user_id,'is_delete'=>0],'workers_id');
        if(empty($workers_info)){
            return $this->jsonend(-1003, '无经销商信息');
        }
        if(empty($this->parm['page'])){
            return $this->jsonend(-1000, '暂无数据');
        }
        $where['user_id'] = $workers_info['workers_id'];
        $where['user_type'] = 1;
        $where['is_to_user'] = 1;
        $row = $this->parm['row'] ?? 100;
        $page = $this->parm['page'] ?? 1;
        $data = $this->fundsRecordModel->getAll($where,'*',$page,$row);
        if(empty($data)){
            return $this->jsonend(-1000, '暂无数据');
        }
        foreach ($data as $key=>$val) {
            $data[$key]['add_time'] = date('Y-m-d H:i',$val['add_time']);
            $data[$key]['source_desc'] = $this->config->get('funds_record_source')[$val['source']]??'';
        }
        $count = $this->fundsRecordModel->count($where);
        $allPage = ceil($count/$row);
        $data = [
            'data' => $data,
            'page' => [
                'current_page' => $page,
                'row' => $row,
                'total' => $count,
                'total_page' => $allPage
            ]
        ];
        return $this->jsonend(1000, '获取成功', $data);
    }
}