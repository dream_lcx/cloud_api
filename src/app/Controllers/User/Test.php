<?php

namespace app\Controllers\User;

use app\Services\Common\DataService;
use app\Tasks\AppTask;
use app\Services\Common\HttpService;

/**
 * 测试
 */
class Test extends Base {

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
    }

    // 用户催单
    public function http_test() {

        // 短信内容
        $sendMsg = '您好，您家净水器合同已到期，为不影响您正常用水，可登陆《重庆润泉》小程序，我的合同续费；如已续费，请忽略此信息！';
        // 获取当日开始和结束的时间戳
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        // 查询今日续费到期的合同下的用户id
        $where = [
            'exire_date'=>['BETWEEN', [$beginToday, $endToday]],
        ];
        // 根据合同续费时间
        $data = $this->db->select('contract_id,user_id')
            ->from('contract')
            ->TPWhere($where)
            ->query()
            ->result_array();
        // 有数据运行
        $userIdsArr = [];
        $userPhoneArr = [];
        if(!empty($data)){
            $userIdsArr = array_column($data,'user_id');
            $userIdsArr = array_values(array_filter($userIdsArr));
            $userWhere = [
                'user_id'=>['in',$userIdsArr]
            ];
            $allUser = $this->db->select('user_id,telphone')
                ->from('customer')
                ->TPWhere($userWhere)
                ->query()
                ->result_array();
            if(!empty($allUser)){
                $userPhoneArr = array_column($allUser,'telphone');
            }
        }
        if(!empty($userPhoneArr)){
            // TODO 批量发送短信

        }




//        $company_info = HttpService::requestBossApi(['company_id'=>1],'/api/Company/getCompany');
//        dump($company_info);
//        return $this->jsonend(1000, '催单成功，我们会尽快为您处理');
//        $info['name'] = '';
//        $notice[] = ['title'=>'','content'=>"刘师傅正在上门服务中"];
//        $info['value'] = [106.8015289307, 29.8001345484,'上门通知',$notice,3];
//        DataService::sendToUids(['command'=>'A1','data'=>['type'=>6,'engineer'=>"刘师傅",'info'=>[$info]]]);

    }



}
