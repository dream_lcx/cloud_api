<?php

namespace app\Controllers\User;

use app\Models\WorkOrderProblemModel;
use app\Services\Common\DataService;
use app\Services\Common\EmailService;
use app\Services\Common\ConfigService;
use app\Services\Common\SmsService;
use app\Services\Common\NotifiyService;
use app\Services\Common\CommonService;
use app\Services\Common\SecureService;
use app\Services\Company\CompanyService;
use app\Services\Contract\ContractService;
use app\Services\Device\DeviceService;
use app\Services\User\UserService;

/**
 * 工单模块API
 */
class Work extends Base
{

    protected $user_model;
    protected $order_model;
    protected $equip_model;
    protected $rent_package_model;
    protected $log_model;
    protected $work_model;
    protected $work_log_model;
    protected $auth_model;
    protected $addr_model;
    protected $contract_model;
    protected $work_eq_model;
    protected $eq_list_model;
    protected $ep_part_model;
    protected $parts_model;
    protected $evaluate_model;
    protected $work_order_problem_model;
    protected $work_order_business_model;
    protected $business_type_model;
    protected $new_install_business_id;
    protected $repair_business_id;
    protected $move_business_id;
    protected $for_core_business_id;
    protected $engineersModel;
    protected $work_parts_model;
    protected $work_additional_service_model;
    protected $for_core_model;
    protected $bind_model;
    protected $contract_eq_model;
    protected $Achievement;
    protected $admin_user_model;
    protected $op_user_model;
    protected $administrative_user_model;
    protected $userService;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->order_model = $this->loader->model('OrderModel', $this);
        $this->equip_model = $this->loader->model('EquipmentModel', $this);
        $this->rent_package_model = $this->loader->model('RentalPackageModel', $this);
        $this->log_model = $this->loader->model('OrderLogModel', $this);
        $this->work_model = $this->loader->model('WorkOrderModel', $this);
        $this->work_log_model = $this->loader->model('WorkOrderLogModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->addr_model = $this->loader->model('UseraddressModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->work_eq_model = $this->loader->model('WorkEquipmentModel', $this);
        $this->eq_list_model = $this->loader->model('EquipmentListsModel', $this);
        $this->ep_part_model = $this->loader->model('EquipmentsPartsModel', $this);
        $this->parts_model = $this->loader->model('PartsModel', $this);
        $this->evaluate_model = $this->loader->model('EvaluateModel', $this);
        $this->work_order_problem_model = $this->loader->model('WorkOrderProblemModel', $this);
        $this->work_order_business_model = $this->loader->model('WorkOrderBusinessModel', $this);
        $this->business_type_model = $this->loader->model('BusinessTypeModel', $this);
        $this->engineersModel = $this->loader->model('EngineersModel', $this);
        $this->work_parts_model = $this->loader->model('WorkPartsModel', $this);
        $this->work_additional_service_model = $this->loader->model('WorkAdditionalServiceModel', $this);
        $this->for_core_model = $this->loader->model('ForCoreRecordModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->contract_eq_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->admin_user_model = $this->loader->model('AdminUserModel', $this);
        $this->op_user_model = $this->loader->model('OperationUserModel', $this);
        $this->administrative_user_model = $this->loader->model('AdministrativeUserModel', $this);
        $this->customer_administrative_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        //业务类型ID
        $this->new_install_business_id = 3; //新装业务ID
        $this->repair_business_id = 6; //维修业务ID
        $this->move_business_id = 5; //移机业务ID
        $this->for_core_business_id = 2; //换芯
        $this->userService = new UserService();
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 预约新装
     * @description 新增新装工单，给用户发送消息通知，给后台发送工单提醒
     * @method POST
     * @url User/Work/newInstall
     * @param appointment_date 必选 string 预约日期
     * @param appointment_time_id 必选 int 预约时间段ID
     * @param contacts 必选 string 联系人
     * @param contact_number 必选 string 联系电话
     * @param order_id 必选 int 订单ID
     * @param remarks 可选 string 备注
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_newInstall()
    {
        $business_id = $this->new_install_business_id; //新装
        if (empty($this->parm['appointment_date'] ?? '')) {
            return $this->jsonend(-1001, "请选择预约年月日");
        }
        if (empty($this->parm['appointment_time_id'] ?? '')) {
            return $this->jsonend(-1001, "请选择预约时间范围");
        }
        $time_info = $this->getRangeInfo($this->parm['appointment_time_id']);
        if (!$time_info) {
            $this->jsonend(-1001, "请选择有效的时间范围");
        }
        //处理预约时间
        $time = $this->parm['appointment_date'] . ' ' . $time_info['start'] . '-' . $time_info['end'];
        $time_start = strtotime($this->parm['appointment_date'] . ' ' . $time_info['start']);
        $time_end = strtotime($this->parm['appointment_date'] . ' ' . $time_info['end']);
        if ($time_start < time()) {
            return $this->jsonend(-1002, "预约时间不能小于当前时间");
        }
        if (empty($this->parm['contacts'] ?? '')) {
            return $this->jsonend(-1001, "请输入联系人");
        }
        if (empty($this->parm['contact_number'] ?? '')) {
            return $this->jsonend(-1001, "请输入联系电话");
        }
        if (!isMobile($this->parm['contact_number'])) {
            return $this->jsonend(-1002, "联系电话不合法");
        }
        //查询用户信息
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_id,source,username,openid,telphone,user_belong_to_promoter,user_belong_to_channel');
        if (empty($user_info)) {
            return $this->jsonend(-1001, "用户信息不存在");
        }
        //判断地址合法性
        //判断用户是否通过实名认证
//        $auth_info = $this->auth_model->getAll(array('user_id' => $this->user_id), 'status');
//        if (empty($auth_info) || $auth_info['status'] != 2) {
//            return $this->jsonend(-1105, "您还未通过实名认证", $auth_info);
//        }
        //判断是否已经发起过新装工单
        $work_info = $this->work_model->getWorkDetail(array('order_id' => $this->parm['order_id']), array(), 'work_order_status');
        if (!empty($work_info) && $work_info['work_order_status'] != 2) {
            return $this->jsonend(-1104, "该订单已经发起过预约安装,不需要重复发送");
        }
        //查询订单信息
        $order_info = $this->order_model->getOne(array('order_id' => $this->parm['order_id']), '*');
        if (empty($order_info)) {
            return $this->jsonend(-1101, "订单不存在");
        }
        if ($order_info['pay_status'] == 1) {
            return $this->jsonend(-1102, "该订单未支付,请先支付");
        }
        if ($order_info['order_status'] != 3) {
            return $this->jsonend(-1103, "该订单不合法");
        }
        //组装数据
        $data['appointment_time_id'] = $this->parm['appointment_time_id'];
        $data['appointment_time'] = $time;
        $data['appointment_start_time'] = $time_start;
        $data['appointment_end_time'] = $time_end;
        $data['order_number'] = CommonService::createSn('cloud_work_order_no');
        $data['contacts'] = $this->parm['contacts'];
        $data['contact_number'] = $this->parm['contact_number'];
        $data['province'] = $order_info['province'];
        $data['city'] = $order_info['city'];
        $data['area'] = $order_info['area'];
        $data['province_code'] = $order_info['province_code'];
        $data['city_code'] = $order_info['city_code'];
        $data['area_code'] = $order_info['area_code'];
        $data['service_address'] = $order_info['address'];
        $data['lng'] = $order_info['lng'];
        $data['lat'] = $order_info['lat'];
        $data['work_order_status'] = 1;
        $data['reception_user'] = 1;
        $data['order_id'] = $this->parm['order_id'];
        $data['equipments_id'] = $order_info['equipments_id'];
        $data['user_id'] = $this->user_id;
        $data['equipment_num'] = $order_info['equipment_num'];
        $data['remarks'] = $this->parm['remarks'] ?? '';
        $data['company_id'] = $order_info['company_id'];
        $data['work_belong_to_client'] = $this->request->client;
        $data['work_belong_to_promoter'] = $user_info['user_belong_to_promoter'];
        $data['work_belong_to_channel'] = $user_info['user_belong_to_channel'];
        //获取行政中心信息
        $admin_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order_info['province_code'], $order_info['city_code'], $order_info['area_code'], $this->company);
        if (!empty($admin_info)) {
            $data['administrative_id'] = $admin_info['a_id'];
            $data['operation_id'] = $admin_info['operation'];
        }
        $data['order_time'] = time();
        $add_status = false;
        $work_id = '';
//        $ap_time = '';
//        $ap_start = '';
//        $ap_end = '';
        $this->db->begin(function () use ($data, &$add_status, $order_info, &$work_id, $business_id, $user_info) {
            //新增新装合同
            if ($order_info['order_type'] != 3) {
                $contract_id = $this->addContact($order_info, $business_id);
                $data['contract_id'] = $contract_id;
            }
            //添加工单
            $work_id = $this->work_model->add($data);
            //添加工单-业务类型关联表
            $b_data['work_order_id'] = $work_id;
            $b_data['business_id'] = $business_id;
            $this->work_order_business_model->add($b_data);
            //添加工单日志
            $log['work_order_id'] = $work_id;
            $log['create_work_time'] = $data['order_time'];
            $log['operating_time'] = time();
            $log['do_id'] = $this->user_id;
            $log['operating_type'] = 3;
            $log['operating_status'] = 1;
            $log['remarks'] = '【用户端】客户【' . $user_info['username'] . '】【客户ID为:' . $user_info['user_id'] . '】发起新装工单';
            $this->work_log_model->add($log);
            //非智能设备绑定
            /**
             * 非智能绑定设备更改到上门时绑定
             */
            //$this->bindNonEquipment($work_id);
            $add_status = true;
        });
        $add_status = true;
        if ($add_status) {
            $business = $this->business_type_model->getOne(array('business_type_id' => $business_id, 'company_id' => $this->company), 'name');
            //通知到用户

            $user_notice_config = ConfigService::getTemplateConfig('user_appoint_success', 'user', $this->company);
            if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                $template_id = $user_notice_config['template']['wx_template_id'];
                $mp_template_id = $user_notice_config['template']['mp_template_id'];
                $template_data = [
                    'keyword1' => ['value' => $business['name']], // 预约类型
                    'keyword2' => ['value' => $data['contacts']], // 联系人
                    'keyword3' => ['value' => $data['contact_number']], // 联系方式
                    'keyword4' => ['value' => $data['appointment_time']], // 预约时间
                    'keyword5' => ['value' => '您已成功预约新装设备,我们会尽快为您处理!若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company)], // 预约内容
                    'keyword6' => ['value' => $data['province'] . $data['city'] . $data['area'] . $data['service_address']] // 服务地址
                ];
                $weapp_template_keyword = '';
                $ap_time = date('Y年m月d日', $data['appointment_start_time']);
                $ap_start = date('H:i', $data['appointment_start_time']);
                $ap_end = date('H:i', $data['appointment_end_time']);
                $mp_template_data = [
                    'first' => ['value' => '恭喜您成功预约新装服务!', 'color' => '#4e4747'],
                    'keyword1' => ['value' => $data['contacts'], 'color' => '#4e4747'],
                    'keyword2' => ['value' => $ap_time, 'color' => '#4e4747'],
                    'keyword3' => ['value' => $ap_start . '-' . $ap_end, 'color' => '#4e4747'],
                    'keyword4' => ['value' => $data['contact_number'], 'color' => '#4e4747'],
                    'remark' => ['value' => '若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company), 'color' => '#173177'],
                ];

                $template_tel = $user_info['telphone'];  // 电话
                $template_content = '恭喜!您已成功预约:' . $business['name'] . '服务,我们将尽快为您处理!若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company);   // 短信内容
                $template_url = 'pages/user/afterSaleDetail/afterSaleDetail?detail=' . $work_id;
                //NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $this->company);//异步发送
            }
            //通知到后台
            $admin_notice_config = ConfigService::getTemplateConfig('user_appoint_success', 'admin', $this->company);
            if (!empty($admin_notice_config)) {
                //站内
                if (!empty($admin_notice_config['websocket']) && $admin_notice_config['websocket']['switch']) {
                    $msg['type'] = 2;
                    $msg['msg'] = '有新工单啦';
                    $this->sendToUid($this->uid, $msg);
                    //通知行政中心
                    $msg['type'] = 1;
                    $msg['msg'] = '有新订单拉';
                    $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $data['province_code'], $data['city_code'], $data['area_code'], $this->company);
                    if (!empty($a_info)) {
                        $this->sendToUid($this->uid, $msg);
                    }
                }
                $ap_time = date('Y年m月d日', $data['appointment_start_time']);
                $ap_start = date('H:i', $data['appointment_start_time']);
                $ap_end = date('H:i', $data['appointment_end_time']);
                //发邮件通知
                if (!empty($admin_notice_config['email']) && $admin_notice_config['email']['switch'] ) {
                    $content = '<h2>' . $business['name'] . '工单通知</h2><br/><div>工单编号:' . $data['order_number'] . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>安装地址:' . $data['province'] . $data['city'] . $data['area'] . $data['service_address'] . '</div><div>预约安装时间:' . $ap_time . $ap_start . '-' . $ap_end . '</div><div style="color:#808080">请尽快进入管理后台审核工单!</div>';
                    //总后台
                    $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                    if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                        EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                    }
                    if (!empty($a_info)) {
                        //对应运营中心
                        $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                        if (!empty($op_email) && !empty($op_email['notice_email'])) {
                            EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                        }
                        //对应合伙人
                        $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                        if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                            EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                        }
                    }
                }
            }
            //通知到大数据平台
            $big_data_notice_config = ConfigService::getTemplateConfig('user_appoint_success', 'big_data', $this->company);
            if (!empty($big_data_notice_config) && !empty($big_data_notice_config['websocket']) && $big_data_notice_config['websocket']['switch']) {
                $tel = substr($user_info['telphone'], 0, 3) . '****' . substr($user_info['telphone'], 7, 4);
                DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 2, 'product' => $business['name'], 'user_tel' => $tel, 'lng' => $data['lng'], 'lat' => $data['lat']]]);

            }


            return $this->jsonend(1000, "预约成功");
        }
        return $this->jsonend(-1000, "预约失败");
    }



    //非智能设备新装工单生成时绑定
    public function bindNonEquipment($work_order_id)
    {
        $this->parm['work_order_id'] = $work_order_id;
        if (empty($this->parm['work_order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数工单ID");
        }
        //查询工单信息
        $wo_map['work_order_id'] = $this->parm['work_order_id'];
        $work_order_info = $this->work_model->getWorkDetail($wo_map, array(), '*');
        if (empty($work_order_info)) {
            return $this->jsonend(-1102, "工单不存在");
        }
        $warranty_time['start'] = '';
        $warranty_time['end'] = '';
        //查询产品信息
        $eq_info = $this->equip_model->getDetail(array('equipments_id' => $work_order_info['equipments_id']), 'type,equipments_name,model,equipments_type,warranty_time,mainboard_id');
        //如果不是非智能设备,不走绑定流程
        if ($eq_info['type'] != 2) {
            return true;
        }
        $this->parm['device_no'] = DeviceService::createNonEqNo(); //生成设备编号
        /*		 * * *****设备保修时间，若从走下单流程，保修时间从确认收货时开始计算，若从不走下单流程，直接发起新装工单，保修时间从设备绑定时开始计算****** */
        if ($eq_info['equipments_type'] == 2) {
            $warranty_time['start'] = time();
            $warranty_time['end'] = $warranty_time['start'] + 86400 * $eq_info['warranty_time'];
        }
        //如果是后台添加的工单,没有订单数据
        if (!empty($work_order_info['order_id'])) {
            //判断该用户是否租赁该类型设备
            $or_map['equipments_id'] = $work_order_info['equipments_id'];
            $or_map['user_id'] = $work_order_info['user_id'];
            $or_map['pay_status'] = 2;
            $or_map['order_id'] = $work_order_info['order_id'];
            $order = $this->order_model->getOne($or_map, 'warranty_start_time,warranty_end_time');
            if (empty($order)) {
                return $this->jsonend(-1103, "该用户未租赁该产品，不能绑定");
            }
            if ($eq_info['equipments_type'] == 2) {
                $warranty_time['start'] = $order['warranty_start_time'];
                $warranty_time['end'] = $order['warranty_end_time'];
            }
        }

        //判断设备是否已经存在
        $equipment_info = $this->eq_list_model->findEquipmentLists(array('device_no' => $this->parm['device_no']), 'equipment_id');
        if (!empty($equipment_info)) {
            //判断用户是否重复绑定设备
            $bd_map['equipment_id'] = $equipment_info['equipment_id'];
            $bd_map['is_owner'] = 2;
            $bind_info = $this->bind_model->getOne($bd_map, '*');
            if (!empty($bind_info)) {
                return $this->jsonend(-1104, "该设备已被绑定过，不能重复绑定");
            }
        }
        //判断用户是否已经绑定完
        $bind_map['equipments_id'] = $work_order_info['equipments_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['user_id'] = $work_order_info['user_id'];
        $bind_map['work_order_id'] = $this->parm['work_order_id'];
        $bind_list = $this->bind_model->getAll($bind_map, '*');
        if (!empty($bind_list) && count($bind_list) >= $work_order_info['equipment_num']) {
            return $this->jsonend(-1105, '该用户租赁该类型设备' . $work_order_info['equipment_num'] . '台，已绑定' . count($bind_list) . '台');
        }

        $eq_data['province'] = $work_order_info['province'];
        $eq_data['city'] = $work_order_info['city'];
        $eq_data['area'] = $work_order_info['area'];
        $eq_data['province_code'] = $work_order_info['province_code'];
        $eq_data['city_code'] = $work_order_info['city_code'];
        $eq_data['area_code'] = $work_order_info['area_code'];
        $eq_data['address'] = $work_order_info['service_address'];
        $eq_data['lng'] = $work_order_info['lng'];
        $eq_data['lat'] = $work_order_info['lat'];
        $eq_data['contact_number'] = $work_order_info['contact_number'];
        $eq_data['contact'] = $work_order_info['contacts'];
        $eq_data['alias_name'] = $eq_info['equipments_name'];
        $eq_data['equipment_type'] = $eq_info['equipments_type'];
        if ($eq_info['equipments_type'] == 2) {
            $eq_data['start_time'] = $warranty_time['start'];
            $eq_data['end_time'] = $warranty_time['end'];
        }
        $eq_data['company_id'] = $work_order_info['company_id'];
        if (empty($equipment_info)) {
            //新增设备记录
            $eq_data['equipments_id'] = $work_order_info['equipments_id'];
            $eq_data['device_no'] = $this->parm['device_no'];
            $eq_data['create_time'] = time();
            $eq_data['status'] = 1;
            $eq_data['contract_id'] = $work_order_info['contract_id'];

            $equipment_id = $this->eq_list_model->insertEquipmentLists($eq_data);
        } else {
            //修改设备信息
            $eq_data['contract_id'] = $work_order_info['contract_id'];
            $equipment_id = $equipment_info['equipment_id'];
            $eq_data['equipments_id'] = $work_order_info['equipments_id'];
            $this->eq_list_model->updateEquipmentLists($eq_data, array('equipment_id' => $equipment_id));
        }
        //添加绑定记录
        $bind_data['equipments_id'] = $work_order_info['equipments_id'];
        $bind_data['work_order_id'] = $this->parm['work_order_id'];
        $bind_data['equipment_id'] = $equipment_id;
        $bind_data['user_id'] = $work_order_info['user_id'];
        $bind_data['create_time'] = time();
        $bind_data['is_owner'] = 2;
        $this->bind_model->addCustomerBindEquipment($bind_data);

        //添加合同设备关联记录
        $contract_eq_data['contract_id'] = $work_order_info['contract_id'];
        $contract_eq_data['equipments_id'] = $work_order_info['equipments_id'];
        $contract_eq_data['equipment_id'] = $equipment_id;
        $contract_eq_data['equipments_address'] = $work_order_info['province'] . $work_order_info['city'] . $work_order_info['area'] . $work_order_info['service_address'];
        $contract_eq_data['addtime'] = time();
        $this->contract_eq_model->add($contract_eq_data);

        //添加工单设备关联表
        $work_eq_data['equipment_id'] = $equipment_id;
        $work_eq_data['work_order_id'] = $this->parm['work_order_id'];
        $work_eq_data['create_time'] = time();
        $this->work_eq_model->add($work_eq_data);

        //已判定完修改相应状态
        $bind_map['equipments_id'] = $work_order_info['equipments_id'];
        $bind_map['user_id'] = $work_order_info['user_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['work_order_id'] = $this->parm['work_order_id'];
        $bind_lists = $this->bind_model->getAll($bind_map, '*');
        if (count($bind_lists) >= $work_order_info['equipment_num']) {
            $this->work_model->editWork(array('work_order_id' => $this->parm['work_order_id']), array('is_bind_equipment' => 1));
            //合同状态
            $this->contract_model->save(array('contract_id' => $work_order_info['contract_id']), array('status' => 3));
            //添加合同日志
            $this->addContractLog($work_order_info['contract_id'], 3, "设备绑定成功,待签署合同");

            //新增----------2019年1月22日11:00:46----------------ligang
            //绑定设备完成后，更改合同关系表设备数
            $contract = $this->contract_model->getOne(['contract_id' => $work_order_info['contract_id']], 'contract_no');
            $this->Achievement->table = 'contract_rank';
            $contract_rank = $this->Achievement->findData(['contract_no' => $contract['contract_no']], 'id');
            $this->Achievement->updateData(['id' => $contract_rank['id']], ['device_number' => $work_order_info['equipment_num']]);
        }
    }

    //生成合同
    public function addContact($info, $business_id)
    {
        // 获取用户信息
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_id,source,username,openid,telphone');
        //获取配置
        $customer_administrative_center = $this->customer_administrative_center_model->getOne(['user_id' => $user_info['user_id']], 'operation_id');
        $operation_id = empty($customer_administrative_center) ? 0 : $customer_administrative_center['operation_id'];
        $data['contact_type'] = ConfigService::getConfig('sign_way', false, $this->company, $operation_id); //2电子合同-电子签名 1电子合同-确认同意 3纸质合同
        $data['user_id'] = $info['user_id'];
        $addr = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $info['province_code'], $info['city_code'], $info['area_code'], $this->company);
        $data['house_type'] = $info['house_type'];
        $data['administrative_id'] = $addr['a_id'];
        $data['operation_id'] = $addr['operation'];
        $data['province'] = $info['province'];
        $data['city'] = $info['city'];
        $data['area'] = $info['area'];
        $data['eq_count'] = $info['equipment_num'];
        $data['province_code'] = $info['province_code'];
        $data['city_code'] = $info['city_code'];
        $data['area_code'] = $info['area_code'];
        $data['address'] = $info['address'];
        $data['business_id'] = $business_id;
        $data['contract_no'] = CommonService::createSn('cloud_contract_no');
        $data['contact_person'] = $info['contact'];
        $data['phone'] = $info['contact_tel'];
        $data['contract_deposit'] = $info['total_deposit'];
        $data['contract_money'] = $info['order_actual_money'];
//        $data['renew_money'] = $info['order_actual_money'] - $info['total_deposit'];
        $data['add_time'] = time();
        $data['contract_class'] = $info['order_type'];
        $data['company_id'] = $info['company_id'];
        //套餐信息
        $data['package_mode'] = $info['package_mode'];
        $data['package_value'] = $info['package_value'];
        $data['package_money'] = $info['package_money'];
        $data['package_cycle'] = $info['package_cycle'];
        $data['renew_money'] = $info['package_renew_money'];
        $data['package_name'] = $info['package_name'];
        $data['package_id'] = $info['package_id'];
        $data['view_layout'] = 1;// TODO 润泉合同模版

        // TODO 2024-07-15 根据产品更正合同产品线
        $equipment = $this->equip_model->getOne(['equipments_id'=>$info['equipments_id']],'*');
        if(!empty($equipment)){
            $data['contract_equipments_line'] = $equipment['equipments_line'];
        }
        //获取合同模板
        //$template_info = ContractService::getTemplate($info['company_id'], 3, $info['order_type'], $data['operation_id']);
//        $contractService = new ContractService();
//        $newData = $data;
//        $newData['business_id'] = 3;
//        $newData2['contract_info'] = $newData;
//        $template_info = $contractService->getTemplate2($newData2);
//        if (!empty($template_info)) {
//            $data['view_layout'] = $template_info['view_layout'];
//        }
        //		if ($info['order_type'] == 2){
//		    //购买直接为生效中
//            $data['status'] = 4;
//            $data['real_exire_date'] = time()+86400*365*100;
//            $data['exire_date'] = time()+86400*365*100;
//            $data['effect_time'] = time();
//        }
        $res = $this->contract_model->add($data);
        $this->addContractLog($res, 1, "用户【" . $user_info['username'] . "】【用户ID:" . $user_info['user_id'] . "】发起新装工单时新增新装合同");
        return $res;
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 提交售后
     * @description
     * @method POST
     * @url User/Work/subWorkOrder
     * @param business_id 必选 int 业务类型ID
     * @param equipment_id 必选 int 设备ID
     * @param appointment_date 必选 string 预约日期
     * @param appointment_time_id 必选 int 预约时间段ID
     * @param contacts 必选 string 联系人
     * @param contact_number 必选 string 联系电话
     * @param order_id 必选 int 订单ID
     * @param remarks 可选 string 备注
     * @param problem_key 可选 int 维修主要问题 如果是维修必传
     * @param problem 可选 string 维修问题说明
     * @param picture 可选 array 图片说明
     * @param move_province_code 可选 string 移机省份code,移机必传
     * @param move_province_code 可选 string 移机城市code，移机必传
     * @param move_province_code 可选 string 移机区域code，移机必传
     * @param move_address 可选 string 移机地址，移机必传
     * @param move_lng 可选 string 移机经度 ，移机必传
     * @param move_lat 可选 string 移机维度 ，移机必传
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_subWorkOrder()
    {
        //处理业务类型
        $business_id = $this->parm['business_id'];
        // 判断参数
        if (empty($this->parm['equipment_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少设备ID");
        }
        if (empty($this->parm['appointment_date'] ?? '')) {
            return $this->jsonend(-1001, "请选择预约年月日");
        }
        if (empty($this->parm['appointment_time_id'] ?? '')) {
            return $this->jsonend(-1001, "请选择预约时间范围");
        }
        $time_info = $this->getRangeInfo($this->parm['appointment_time_id']);
        if (!$time_info) {
            $this->jsonend(-1001, "请选择有效的时间范围");
        }

        // 处理预约时间
        $time = $this->parm['appointment_date'] . ' ' . $time_info['start'] . '-' . $time_info['end'];
        $time_start = strtotime($this->parm['appointment_date'] . ' ' . $time_info['start']);
        $time_end = strtotime($this->parm['appointment_date'] . ' ' . $time_info['end']);
        if ($time_start < time()) {
            return $this->jsonend(-1002, "预约时间不能小于当前时间");
        }

        if (empty($this->parm['contacts'] ?? '')) {
            return $this->jsonend(-1001, "请输入联系人");
        }
        if (empty($this->parm['contact_number'] ?? '')) {
            return $this->jsonend(-1001, "请输入联系电话");
        }
        if (!isMobile($this->parm['contact_number'])) {
            return $this->jsonend(-1002, "联系电话不合法");
        }
        if (!is_array($business_id) || count($business_id) == 0) {
            return $this->jsonend(-1002, "请选择业务类型");
        }
        // 判断用户是否通过实名认证
        $auth_info = $this->auth_model->getAll(array('user_id' => $this->user_id), '*');

        if (empty($auth_info) || $auth_info['status'] != 2) {
            return $this->jsonend(-1105, "您还未通过实名认证", $auth_info);
        }
        // 查询设备信息
        $eq_info = $this->eq_list_model->findEquipmentLists(array('equipment_id' => $this->parm['equipment_id']));
        if (empty($eq_info)) {
            return $this->jsonend(-1104, "该设备未绑定");
        }


        /*		 * *****************************不同业务类型处理start************************** */

        // 判断是否填写备注
        if (!empty($this->parm['remarks'])) {
            // 检测内容是否合同
            $res = SecureService::checkContent($this->parm['remarks'], 'user', $this->company);
            if ($res['errcode'] != 0 && $res['errmsg'] != 'ok') {
                return $this->jsonend(-1000, '内容不合法', $res);
            }
        }

        //维修
        $problem_title = [];
        if (in_array($this->repair_business_id, $business_id)) {
            if (empty($this->parm['problem_key'] ?? '')) {
                return $this->jsonend(-1001, "请选择维修主要问题");
            }
            //获取问题描述

            foreach($this->parm['problem_key'] as $key=>$val){
                  $problem_info = $this->work_order_problem_model->getOne(['problem_id'=>$val],'problem_desc');
                    if(!empty($problem_info)){
                     array_push($problem_title,$problem_info['problem_desc']);
                    }
                }
            $data['problem_key'] = json_encode($this->parm['problem_key']);
            if (empty($this->parm['problem'] ?? '')) {
                return $this->jsonend(-1001, "请具体描述存在问题");
            }
            $data['problem'] = $this->parm['problem'];
            if (!empty($this->parm['picture'] ?? '')) {
                $data['picture'] = json_encode($this->parm['picture']);
            }
            // 检测内容是否合法
            $res = SecureService::checkContent($this->parm['problem'], 'user', $this->company);
            if ($res['errcode'] != 0 && $res['errmsg'] != 'ok') {
                return $this->jsonend(-1000, '内容不合法', $res);
            }
            $this->Achievement->table = 'contract';
            $contract = $this->Achievement->findData(['contract_id' => $eq_info['contract_id']],'installed_time');
            $installed_time = empty($contract)?'':date('Y年m月d日',$contract['installed_time']);
        }


        // 获取行政中心信息
        $admin_info = $this->userService->getOwnArea($this->user_id, '', $eq_info['province_code'], $eq_info['city_code'], $eq_info['area_code'], $this->company);
        if (!empty($admin_info)) {
            $data['administrative_id'] = $admin_info['a_id'];
            $data['operation_id'] = $admin_info['operation'];
        }

        // 获取用户信息
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_id,source,username,realname,openid,telphone,user_belong_to_promoter,user_belong_to_channel');
        if (empty($user_info)) {
            return $this->jsonend(-1001, "用户信息不存在");
        }

        //移机
        $is_replacement = false;
        $contact_log = [];
        $contact_data = [];
        $insert_cac = [];
        if (in_array($this->move_business_id, $business_id)) {

            $is_replacement = true;
            if (empty($this->parm['move_province_code'] ?? '') || empty($this->parm['move_city_code'] ?? '') || empty($this->parm['move_area_code'] ?? '')) {
                return $this->jsonend(-1001, "请选择移机区域");
            }
            if (empty($this->parm['move_address'] ?? '')) {
                return $this->jsonend(-1001, "请选择移机详细地址");
            }
            if (empty($this->parm['move_lng'] ?? '') || empty($this->parm['move_lat'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数，移机地址经纬度");
            }
            $province = $this->getAreaInfo($this->parm['move_province_code']);
            $city = $this->getAreaInfo($this->parm['move_city_code']);
            $area = $this->getAreaInfo($this->parm['move_area_code']);
            if (empty($province) || empty($city) || empty($area)) {
                return $this->jsonend(-1001, "选择移机区域信息错误");
            }

            //移机区域信息
            $move_area_info = $this->userService->getOwnArea($this->user_id, '',$this->parm['move_province_code'], $this->parm['move_city_code'], $this->parm['move_area_code'], $this->company);
            if (empty($move_area_info)) {
                return $this->jsonend(-1000, $province['area_name'] . $city['area_name'] . $area['area_name'] . ' 地区暂无开通业务');
            }

            //验证用户区域信息
            $this->Achievement->table = 'customer_administrative_center';
            $customer_administrative_center = $this->Achievement->findData(['user_id' => $this->user_id, 'administrative_id' => $move_area_info['a_id']], 'id');
            if (empty($customer_administrative_center)) {
                $insert_cac = [
                    'user_id' => $this->user_id,
                    'operation_id' => $move_area_info['operation'],
                    'administrative_id' => $move_area_info['a_id'],
                    'update_time' => time(),
                ];
            }

            $data['move_province_code'] = $this->parm['move_province_code'];
            $data['move_city_code'] = $this->parm['move_city_code'];
            $data['move_area_code'] = $this->parm['move_area_code'];
            $data['move_province'] = $province['area_name'];
            $data['move_city'] = $city['area_name'];
            $data['move_area'] = $area['area_name'];
            $data['move_address'] = $this->parm['move_address'];
            $data['move_lng'] = $this->parm['move_lng'];
            $data['move_lat'] = $this->parm['move_lat'];

            //-----------------------------移机begin-----------------------------
            $data['is_new_word_order'] = 1;//新建移机装工单
            $data['replacement_type'] = 2;//移机拆
            //获取原合同

            $this->Achievement->table = 'contract';
            $contract = $this->Achievement->findData(['contract_id' => $eq_info['contract_id']]);
            //新建移机合同
            $contact_data = $contract;
            $contact_data['pid'] = $eq_info['contract_id'];
            $contact_data['user_id'] = $this->user_id;
            $contact_data['administrative_id'] = $move_area_info['a_id'];
            $contact_data['operation_id'] = $move_area_info['operation'];
            $contact_data['province'] = $province['area_name'];
            $contact_data['city'] = $city['area_name'];
            $contact_data['area'] = $area['area_name'];
            $contact_data['province_code'] = $this->parm['move_province_code'];
            $contact_data['city_code'] = $this->parm['move_city_code'];
            $contact_data['area_code'] = $this->parm['move_area_code'];
            $contact_data['address'] = $this->parm['move_address'];
            if($contact_data['is_old_contract']==1){
                $contact_data['contract_no'] = CommonService::createSn('cloud_contract_no','RQ');
            }else{
                $contact_data['contract_no'] = CommonService::createSn('cloud_contract_no');
            }

            $contact_data['contact_person'] = $this->parm['contacts'];
            $contact_data['phone'] = $this->parm['contact_number'];
            $contact_data['business_id'] = 5;
            $contact_data['add_time'] = time();
            $contact_data['status'] = 3;
            unset($contact_data['contract_id']);
            unset($contact_data['contract_id']);
            unset($contact_data['now_date']);
            unset($contact_data['esign_filename']);
            unset($contact_data['esign_user_sign']);
            //获取配置
            $customer_administrative_center = $this->customer_administrative_center_model->getOne(['user_id' => $contact_data['user_id']], 'operation_id');
            $operation_id = empty($customer_administrative_center) ? 0 : $customer_administrative_center['operation_id'];
            //创建合同日志
            $contact_log['contract_id'] = $eq_info['contract_id'];
            $contact_log['do_id'] = 0;
            $contact_log['do_type'] = 1;
            $contact_log['terminal_type'] = 4;
            $contact_log['do_time'] = time();
            $contact_log['remark'] = '【用户端】客户【' . $user_info['realname'] . '】【客户ID为:' . $this->user_id . '】新增移机工单同时生成合同';
            //-----------------------------移机end-------------------------------
        }

        /*		 * *****************************不同业务类型处理end************************** */
        // 组装数据
        $data['appointment_time'] = $time;
        $data['appointment_start_time'] = $time_start;
        $data['appointment_end_time'] = $time_end;
        $data['appointment_time_id'] = $this->parm['appointment_time_id'];
        $data['order_number'] = CommonService::createSn('cloud_work_order_no');
        $data['contacts'] = $this->parm['contacts'];
        $data['contact_number'] = $this->parm['contact_number'];
        $data['province'] = $eq_info['province'];
        $data['city'] = $eq_info['city'];
        $data['area'] = $eq_info['area'];
        $data['province_code'] = $eq_info['province_code'];
        $data['city_code'] = $eq_info['city_code'];
        $data['area_code'] = $eq_info['area_code'];
        $data['service_address'] = $eq_info['address'];
        $data['lng'] = $eq_info['lng'];
        $data['lat'] = $eq_info['lat'];
        $data['work_order_status'] = 1;
        $data['reception_user'] = 1;
        $data['equipments_id'] = $eq_info['equipments_id'];
        $data['user_id'] = $this->user_id;
        $data['equipment_num'] = 1;
        $data['remarks'] = empty($this->parm['remarks'] ?? '') ? '' : $this->parm['remarks'];
        $data['company_id'] = $eq_info['company_id'];
        $data['work_belong_to_client'] = $this->request->client;
        $data['work_belong_to_promoter'] = $user_info['user_belong_to_promoter'];
        $data['work_belong_to_channel'] = $user_info['user_belong_to_channel'];
        $data['order_time'] = time();
        $data['device_model'] = $eq_info['machine_id'];
        $data['contract_id'] = $eq_info['contract_id'];
        // 获取商品名称
        $eq_info = $this->equip_model->getDetail(['equipments_id' => $eq_info['equipments_id']], 'equipments_name');
        $add_status = false;
        $work_id = '';
        $this->db->begin(function () use ($data, $user_info,$contact_data, $insert_cac, $contact_log, $is_replacement, $eq_info, &$add_status, &$work_id, $business_id) {
            //移机操作
            if ($is_replacement) {

                //添加合同
                $this->Achievement->table = 'contract';
                $contact_id = $this->Achievement->insertData($contact_data);
                $data['contract_id'] = $contact_id;
                $new_work = $data;
                $new_work['is_new_word_order'] = 1;
                $new_work['is_bind_equipment'] = 1;
                $new_work['replacement_type'] = 3;//移机装
                $new_work['order_number'] = CommonService::createSn('cloud_work_order_no');;
                //添加合同日志
                $this->Achievement->table = 'contract_log';
                $this->Achievement->insertData($contact_log);
                //复制结算关系
//                $this->Achievement->table = 'contract_rank';
//                $contract_rank = $this->Achievement->getOne(['contract_no'=>$contract['contract_no']]);
//                if(!empty($contract_rank)){
//                    $contract_rank = json_decode(json_encode($contract_rank),true);
//                    $contract_rank['contract_no'] = $contact_data['contract_no'];
//                    $contract_rank['add_time'] = time();
//                    unset($contract_rank['id']);
//                    $this->Achievement->insertData($contract_rank);
//                }
                //移机装工单
                $new_work_id = $this->work_model->add($new_work);
                //移机装工单类型关系
                $this->work_order_business_model->add(['work_order_id' => $new_work_id, 'business_id' => 5]);
                //添加工单设备关联表
                $work_eq_data['equipment_id'] = $this->parm['equipment_id'];
                $work_eq_data['work_order_id'] = $new_work_id;
                $work_eq_data['create_time'] = time();
                $this->work_eq_model->add($work_eq_data);
                //工单日志
                $log = [];
                $log['work_order_id'] = $new_work_id;
                $log['create_work_time'] = $data['order_time'];
                $log['operating_time'] = time();
                $log['do_id'] = $this->user_id;
                $log['operating_type'] = 3;
                $log['operating_status'] = 1;
                $log['remarks'] = '【用户端】客户【' . $user_info['username'] . '】【客户ID为:' . $user_info['user_id'] . '】发起新工单';
                $this->work_log_model->add($log);
                //
                if (!empty($insert_cac)) {
                    $this->Achievement->table = 'customer_administrative_center';
                    $this->Achievement->insertData($insert_cac);
                }
                //新增合同设备关系
                $contract_eq_data['contract_id'] = $contact_id;
                $contract_eq_data['equipments_id'] = $data['equipments_id'];
                $contract_eq_data['equipment_id'] = $this->parm['equipment_id'];
                $contact_data['province_code'] = $this->parm['move_province_code'];
                $contact_data['city_code'] = $this->parm['move_city_code'];
                $contact_data['area_code'] = $this->parm['move_area_code'];
                $contact_data['address'] = $this->parm['move_address'];
                $contract_eq_data['equipments_address'] = $contact_data['province'] . $contact_data['city'] . $contact_data['area'] . $contact_data['address'];
                $contract_eq_data['addtime'] = time();
                $this->contract_eq_model->add($contract_eq_data);


            }
            // 1-- 添加工单
            $work_id = $this->work_model->add($data);
            //添加工单-业务类型关联表
            foreach ($business_id as $k => $v) {
                $b_data['work_order_id'] = $work_id;
                $b_data['business_id'] = $v;
                $this->work_order_business_model->add($b_data);
            }
            //添加工单设备关联表
            $work_eq_data['equipment_id'] = $this->parm['equipment_id'];
            $work_eq_data['work_order_id'] = $work_id;
            $work_eq_data['create_time'] = time();
            $this->work_eq_model->add($work_eq_data);
            // 2-- 添加工单日志
            $log = [];
            $log['work_order_id'] = $work_id;
            $log['create_work_time'] = $data['order_time'];
            $log['operating_time'] = time();
            $log['do_id'] = $this->user_id;
            $log['operating_type'] = 3;
            $log['operating_status'] = 1;
            $log['remarks'] = '【用户端】客户【' . $user_info['username'] . '】【客户ID为:' . $user_info['user_id'] . '】发起新工单';
            $this->work_log_model->add($log);
            $add_status = true;
        });
        if ($add_status) {
            //获取业务类型名称

            $business_name_arr = [];
            foreach ($business_id as $k => $v) {
                $business = $this->business_type_model->getOne(array('business_type_id' => $v, 'company_id' => $this->company), 'name');
                if (!empty($business)) {
                    array_push($business_name_arr, $business['name']);
                }
            }
            $business_name = implode('+', $business_name_arr);
            // 模板内容
            $template_data = [
                'keyword1' => ['value' => $business_name], // 预约类型
                'keyword2' => ['value' => $data['contacts']], // 联系人
                'keyword3' => ['value' => $data['contact_number']], // 联系方式
                'keyword4' => ['value' => $data['appointment_time']], // 预约时间
                'keyword5' => ['value' => '您的售后申请已处理!点击可查看详情。'], // 预约内容
                'keyword6' => ['value' => $data['province'] . $data['city'] . $data['area'] . $data['service_address']] // 服务地址
            ];
            $template_id = $this->config->get('wx_template_' . $this->company)['appoint_success']['id'];  // 模板ID
            $mp_template_id = $this->config->get('mp_user_template_' . $this->company)['appoint_success']['id'];  // 模板ID
            $weapp_template_keyword = '';
            $ap_time = date('Y年m月d日', $data['appointment_start_time']);
            $ap_start = date('H:i', $data['appointment_start_time']);
            $ap_end = date('H:i', $data['appointment_end_time']);
            $mp_template_data = [
                'first' => ['value' => '恭喜您成功预约新装服务!', 'color' => '#4e4747'],
                'keyword1' => ['value' => $data['contacts'], 'color' => '#4e4747'],
                'keyword2' => ['value' => $ap_time, 'color' => '#4e4747'],
                'keyword3' => ['value' => $ap_start . '-' . $ap_end, 'color' => '#4e4747'],
                'keyword4' => ['value' => $data['contact_number'], 'color' => '#4e4747'],
                'remark' => ['value' => '若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company), 'color' => '#173177'],
            ];
            $template_tel = $user_info['telphone'];  // 电话
            $template_content = '您提交的' . $business_name . '售后申请已经处理！打开微信进入' . $this->wx_config['app_wx_name'] . '小程序查看详情！若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company);
            $template_url = 'pages/user/afterSaleDetail/afterSaleDetail?detail=' . $work_id;

           // NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $this->company);//异步发送
            //通知后台
            $msg['type'] = 2;
            $msg['msg'] = '有新工单啦';
            $this->sendToUid($this->uid, $msg);
            //大数据平台
            $tel = substr($user_info['telphone'], 0, 3) . '****' . substr($user_info['telphone'], 7, 4);
            DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 2, 'product' => $business_name, 'user_tel' => $tel, 'lng' => $data['lng'], 'lat' => $data['lat']]]);
            //通知行政中心
            $msg['type'] = 1;
            $msg['msg'] = '有新订单拉';
            $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $data['province_code'], $data['city_code'], $data['area_code'], $this->company);
            if (!empty($a_info)) {
                $this->sendToUid($this->uid, $msg);
            }

            //发邮件通知
            $content = '<h2>' . $business['name'] . '工单通知</h2><br/><div>工单编号:' . $data['order_number'] . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>安装地址:' . $data['province'] . $data['city'] . $data['area'] . $data['service_address'] . '</div><div>预约时间:' . $ap_time . $ap_start . '-' . $ap_end . '</div><div style="color:#808080">请尽快进入管理后台审核工单!</div>';
            if (in_array($this->repair_business_id, $business_id)) {

                $content = '<h2>' . $business['name'] . '工单通知</h2><br/><div>工单编号:' . $data['order_number'] .'</div><div>客户姓名:' . $user_info['realname'] . '</div><div>客户电话:' . $user_info['telphone'] .'</div><div>维修类型:' .implode(',',$problem_title) .'</div><div>问题描述:' . $data['problem'] .'</div><div>装机时间:' .$installed_time . '</div><div>安装地址:' . $data['province'] . $data['city'] . $data['area'] . $data['service_address'] . '</div><div>预约时间:' . $ap_time . $ap_start . '-' . $ap_end . '</div><div style="color:#808080">请尽快进入管理后台审核工单!</div>';
            }
            //总后台
            $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
            if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
            }
            if (!empty($a_info)) {
                //对应运营中心
                $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                if (!empty($op_email) && !empty($op_email['notice_email'])) {
                    EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                }
                //对应合伙人
                $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                    EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                }
            }


            return $this->jsonend(1000, "提交成功");
        }
        return $this->jsonend(-1000, "提交失败");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 工单基本信息
     * @description 支持根据工单ID，工单号查询，如果是新装可以根据订单ID查询
     * @method POST
     * @url User/Work/getWorkInfo
     * @param work_order_id 可选 int 工单ID
     * @param order_number 可选 string 工单号
     * @param order_id 可选 int 订单ID
     * @param business_id 可选 int 业务类型ID,若根据订单ID查询必传
     * @return {"work_order_id":"3","order_number":"CQW917800812842347","work_order_status":"12","pay_status":null,"equipments_id":"2","user_id":"5","contract_id":"3","administrative_id":"23","repair_id":"1","order_id":"5","order_type":"2","pay_business_money":"0","pay_additional_service_money":"0","pay_parts_money":"0","pay_filters_money":"0","pay_other_money":"0","pay_origin_money":"0","pay_way":"1","pay_client":null,"wx_callback_num":null,"order_pay_time":null,"operation_id":"1","business_id":null,"is_reminder":"2","reminder_time":null,"combo_type":"","combo_money":"0","isuser":null,"reception_user":"1","order_time":"1537180081","update_time":"1537180207","assigne_time":"1537180922","assignd_time":"1537180207","reception_time":"1537180160","receive_time":"1537180922","complete_time":"1537181470","province":"重庆市","city":"重庆市","area":"渝北区","province_code":"23","city_code":"271","area_code":"2503","service_address":"思凰庭","lat":"29.60233","lng":"106.52099"}
     * @return_param  work_order_id int 工单ID
     * @return_param order_number string 工单号
     * @return_param work_order_status string 工单状态
     * @return_param combo_money string 支付金额
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getWorkInfo()
    {
        $map = array();
        if (!empty($this->parm['work_order_id'] ?? '')) {
            $map['work_order_id'] = $this->parm['work_order_id'];
        }
        if (!empty($this->parm['order_number'] ?? '')) {
            $map['order_number'] = $this->parm['order_number'];
        }
        if (!empty($this->parm['order_id'] ?? '') && !empty($this->parm['business_id'] ?? '')) {
            $map['order_id'] = $this->parm['order_id'];
            $map['business_id'] = $this->parm['business_id'];
        }
        if (empty($map)) {
            return $this->jsonend(-1001, "缺少参数,工单ID,工单编号或者订单ID+业务类型");
        }
        $data = $this->work_model->getWorkDetail($map, array(), '*');
        if (!empty($data)) {
            return $this->jsonend(1000, "获取详情成功", $data);
        }
        return $this->jsonend(-1000, "获取详情失败");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 工单列表
     * @description
     * @method POST
     * @url User/Work/getWorkList
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @return
     * @return_param  work_order_id int 工单ID
     * @return_param order_number string 工单号
     * @return_param work_order_status string 工单状态
     * @return_param combo_money string 支付金额
     * @return_param work_order_status_name string 状态名称
     * @return_param work_order_status_name string 状态描述
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getWorkList()
    {
        $page = empty($this->parm['page'] ?? '') ? 1 : $this->parm['page'];
        $pageSize = empty($this->parm['pagesize'] ?? '') ? 10 : $this->parm['pagesize'];
        $map['rq_work_order.user_id'] = $this->user_id;
        if(!empty($this->parm['user_id'])){
            $map['rq_work_order.user_id'] = $this->parm['user_id'];
        }
        $map['is_to_user'] = 1;
        $join = [
            ['customer as c', 'c.user_id = rq_work_order.user_id', 'inner',], // 用户表关联工单表
            ['engineers as e', 'e.engineers_id = rq_work_order.repair_id', 'left'], // 工程人员表关联工单表
            ['equipments as ep', 'ep.equipments_id = rq_work_order.equipments_id', 'inner',], // 关联产品表
            ['work_equipment we', 'we.work_order_id = rq_work_order.work_order_id', 'left']
        ];
        $field = 'rq_work_order.work_order_id,rq_work_order.repair_id,rq_work_order.contacts,rq_work_order.contact_number,
                  rq_work_order.appointment_time,rq_work_order.province,rq_work_order.city,rq_work_order.area,
                  rq_work_order.service_address,rq_work_order.remarks,ep.equipments_name,ep.model,
                  rq_work_order.order_number,rq_work_order.work_order_status,c.username,e.engineers_name,e.engineers_phone,
                  ep.scale,ep.main_pic,we.equipment_id';
        $order['rq_work_order.order_time'] = 'DESC';
        $data = $this->work_model->getAll($map, $join, $page, $pageSize, $field, $order);
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $data[$k]['work_order_status_name'] = $this->config->get('user_work_order_status')[$v['work_order_status']];
                $data[$k]['main_pic'] = $this->config->get('qiniu.qiniu_url') . $v['main_pic'];
                $data[$k]['work_order_status_desc'] = $this->config->get('user_work_order_status_desc')[$v['work_order_status']];
                //处理业务类型
                $b_map['work_order_id'] = $v['work_order_id'];
                $b_join = [
                    ['business_type as b', 'b.business_type_id = rq_work_order_business.business_id', 'LEFT',]        // 关联业务类型表
                ];
                $b_map['b.company_id'] = $this->company;
                $business = $this->work_order_business_model->getAll($b_map, $b_join, 'rq_work_order_business.business_id,b.name');
                $b_id = [];
                $b_name = [];
                if (!empty($business)) {
                    foreach ($business as $kk => $vv) {
                        array_push($b_id, $vv['business_id']);
                        array_push($b_name, $vv['name']);
                    }
                }
                $data[$k]['business_name'] = implode('+', $b_name);
                $data[$k]['business_id'] = $b_id;

                //更改提示语
                if ($v['work_order_status'] <= 7) {
                    $data[$k]['note_number'] = 1;
                } elseif ($v['work_order_status'] == 8) {
                    $data[$k]['note_number'] = 2;
                    $data[$k]['engineers_name'] = substr($data[$k]['engineers_name'], 0, 1) . '师傅';
                } elseif ($v['work_order_status'] == 9) {
                    $data[$k]['note_number'] = 3;
                } elseif ($v['work_order_status'] == 10) {
                    $data[$k]['note_number'] = 4;
                } elseif ($v['work_order_status'] == 11 || $v['work_order_status'] == 12) {
                    $data[$k]['note_number'] = 5;
                } else {
                    $data[$k]['note_number'] = 6;
                }
            }
            return $this->jsonend(1000, "获取列表成功", $data);
        }
        return $this->jsonend(-1003, "没有数据");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 工单详情
     * @description
     * @method POST
     * @url User/Work/workOrderDetail
     * @param work_order_id 必选 int 工单ID
     * @return
     * @return_param  work_order_id int 工单ID
     * @return_param order_number string 工单号
     * @return_param work_order_status string 工单状态
     * @return_param combo_money string 支付金额
     * @return_param product_info string 产品信息
     * @return_param work_order_log array 日志信息
     * @return_param parts_data array 配件信息
     * @return_param evaluate_data array 评价信息
     * @return_param equipment_info array 设备信息
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_workOrderDetail()
    {
        // 接收参数
        $work_order_id = $this->parm['work_order_id'] ?? '';
        // 判断参数
        if (empty($work_order_id)) {
            return $this->jsonend(-1001, '工单ID不能为空');
        }
        // 工单ID字段替换
        $workOrderId = 'rq_work_order.work_order_id';
        // 查询条件
        $where = [$workOrderId => $work_order_id];
        // 连表操作
        $join = [
            ['customer as c', 'c.user_id = rq_work_order.user_id', 'left join',], // 用户表关联工单表
            ['equipments as e', 'e.equipments_id = rq_work_order.equipments_id', 'left join',], // 设备分类表关联工单表
            ['contract as ct', 'ct.contract_id = rq_work_order.contract_id', 'left join',], // 合同表关联工单表
            ['work_parts as wp', 'wp.work_order_id = rq_work_order.work_order_id', 'left join',], // 工单配件表关联工单表
            ['parts as p', 'p.parts_id = wp.parts_id', 'left join',], // 配件表关联工单配件表
            ['engineers as es', 'es.engineers_id = rq_work_order.repair_id', 'left',],
        ];
        // 查询字段
        $field = 'rq_work_order.*,p.parts_name,ct.contract_no,
                e.equipments_name,e.model,e.original_parts,e.main_pic,e.equipments_money,
                es.engineers_name,es.engineers_phone,es.engineers_number,es.avatar_img as engineers_avatar_img,engineers_id';
        // 调用模型查询数据
        $workDetail = $this->work_model->getWorkDetail($where, $join, $field);
        // 判断结果--返回
        if ($workDetail) {
            $workDetail['appointment_time_date'] = date('Y-m-d', $workDetail['appointment_start_time']);
            $workDetail['order_time'] = date('Y-m-d H:i:s', $workDetail['order_time']);
            $workDetail['assigne_time'] = empty($workDetail['assigne_time']) ? '' : date('Y-m-d H:i:s', $workDetail['assigne_time']);
            $workDetail['assignd_time'] = empty($workDetail['assignd_time']) ? '' : date('Y-m-d H:i:s', $workDetail['assignd_time']);
            $workDetail['receive_time'] = empty($workDetail['receive_time']) ? '' : date('Y-m-d H:i:s', $workDetail['receive_time']);
            $workDetail['complete_time'] = empty($workDetail['complete_time']) ? '' : date('Y-m-d H:i:s', $workDetail['complete_time']);
            $workDetail['engineers_avatar_img'] = empty($workDetail['engineers_avatar_img']) ? '' : $this->config->get('qiniu.qiniu_url') . $workDetail['engineers_avatar_img'];
            $workDetail['work_order_status_name'] = $this->config->get('user_work_order_status')[$workDetail['work_order_status']];
            $workDetail['pay_business_money'] = formatMoney($workDetail['pay_business_money']);
            $workDetail['pay_additional_service_money'] = formatMoney($workDetail['pay_additional_service_money']);
            $workDetail['pay_parts_money'] = formatMoney($workDetail['pay_parts_money']);
            $workDetail['pay_filters_money'] = formatMoney($workDetail['pay_filters_money']);
            $workDetail['pay_other_money'] = formatMoney($workDetail['pay_other_money']);
            $workDetail['pay_origin_money'] = formatMoney($workDetail['pay_origin_money']);
            $workDetail['pay_client_desc'] = '--';
            $workDetail['pay_status_desc'] = '--';
            if ($workDetail['combo_money'] > 0) {
                $workDetail['pay_client_desc'] = empty($workDetail['pay_way']) ? '' : $this->config->get('user_bill.pay_way')[$workDetail['pay_way']];
                $workDetail['pay_status_desc'] = $workDetail['pay_status'] == 2 ? '已支付' : '待支付';
            }
            $workDetail['combo_money'] = formatMoney($workDetail['combo_money']);
            //产品信息
            $product_info['equipments_name'] = $workDetail['equipments_name'];
            $product_info['model'] = $workDetail['model'];
            $product_info['equipments_money'] = $workDetail['equipments_money'];
            $original_parts = json_decode($workDetail['original_parts'], true);
            $parts = [];
            if (!empty($original_parts)) {
                foreach ($original_parts as $k => $v) {
                    $part_info = $this->parts_model->getOne(array('parts_id' => $v['parts_id']), 'parts_name,parts_pic');
                    $part['parts_id'] = $v['parts_id'];
                    $part['parts_name'] = $part_info['parts_name'];
                    $part['parts_pic'] = $this->config->get('qiniu.qiniu_url') . $part_info['parts_pic'];
                    $part['cycle'] = $v['cycle'];
                    $parts[] = $part;
                }
            }
            $product_info['original_parts'] = $parts;
            $product_info['main_pic'] = $this->config->get('qiniu.qiniu_url') . $workDetail['main_pic'];
            $workDetail['product_info'] = $product_info;
            $picture = '';
            if (!empty($workDetail['picture'])) {
                $picture = json_decode($workDetail['picture'], true);
                if (!empty($picture)) {
                    foreach ($picture as $k => $v) {
                        $picture[$k] = $this->config->get('qiniu.qiniu_url') . $v;
                    }
                }
            }
            $workDetail['picture'] = $picture;
            $problem_key = '';
            if (!empty($workDetail['problem_key'])) {
                $problem_key = json_decode($workDetail['problem_key'], true);
                if (!empty($problem_key)) {
                    foreach ($problem_key as $k => $v) {
                        $info = $this->work_order_problem_model->getOne(array('problem_id' => $v), 'problem_desc');
                        $problem_key[$k] = '';
                        if (!empty($info)) {
                            $problem_key[$k] = $info['problem_desc'];
                        }
                    }
                }
            }
            $workDetail['problem_key'] = $problem_key;
            //工单轨迹
            $work_log_map['work_order_id'] = $this->parm['work_order_id'];
            $work_log_map['is_to_user'] = 1;
            $work_log_map['operating_status'] = ['IN', [1, 8, 9, 10, 11, 12]];
            $work_log_field = 'work_order_log_id,operating_time,operating_status,remarks';
            $work_log_order['operating_time'] = 'DESC';
            $work_log = $this->work_log_model->getList($work_log_map, $work_log_field, $work_log_order);
            if (!empty($work_log)) {
                foreach ($work_log as $k => $v) {
                    $work_log[$k]['operating_time'] = date('Y-m-d H:i:s', $v['operating_time']);
                    $work_log[$k]['operating_status_name'] = $this->config->get('user_work_order_status')[$v['operating_status']];
                    $work_log[$k]['operating_status_desc'] = $this->config->get('user_work_order_status_desc')[$v['operating_status']];
                }
            }
            $workDetail['work_order_log'] = $work_log;
            //处理业务类型
            $b_map['work_order_id'] = $workDetail['work_order_id'];
            $b_join = [
                ['business_type as b', 'b.business_type_id = rq_work_order_business.business_id', 'LEFT',]        // 关联业务类型表
            ];
            $b_map['b.company_id'] = $this->company;
            $business = $this->work_order_business_model->getAll($b_map, $b_join, 'rq_work_order_business.business_id,b.name');
            $b_id = [];
            $b_name = [];
            if (!empty($business)) {
                foreach ($business as $kk => $vv) {
                    array_push($b_id, $vv['business_id']);
                    array_push($b_name, $vv['name']);
                }
            }
            $workDetail['business_name'] = implode('+', $b_name);
            $workDetail['business_id'] = $b_id;

            // 获取工单配件信息--------------
            $parts_data = [];
            // 1-- 获取原始配件
            // 1.1-- 获取设备ID
            $equipment_id = $this->work_eq_model->getOne(['work_order_id' => $work_order_id], 'equipment_id')['equipment_id'];
            // 1.2-- 根据设备ID获取原始配件
            $parts_data['original_parts'] = $this->ep_part_model->getJoinAll(['equipment_id' => $equipment_id, 'is_original' => 1]);

            // 2-- 获取换芯配件
            $parts_join = [
                ['work_parts wp', 'wp.parts_id = rq_parts.parts_id', 'left join',], // 工单配件表关联配件表
                ['work_order wo', 'wo.work_order_id = wp.work_order_id', 'left join',], // 工单表关联工单配件表
            ];
            $parts_where['wp.work_order_id'] = $work_order_id;
            $parts_where['is_filter'] = 2;
            $parts_field = 'rq_parts.parts_name,rq_parts.parts_id,wp.parts_number';
            $parts_data['core_parts'] = $this->parts_model->getJoinAll($parts_where, $parts_field, $parts_join);
            unset($parts_where);
            // 3-- 获取新增配件
            $parts_join = [
                ['work_parts wp', 'wp.parts_id = rq_parts.parts_id', 'left join',], // 工单配件表关联配件表
                ['work_order wo', 'wo.work_order_id = wp.work_order_id', 'left join',], // 工单表关联工单配件表
            ];
            $parts_where['wp.work_order_id'] = $work_order_id;
            $parts_where['is_filter'] = 1;
            $parts_field = 'rq_parts.parts_name,rq_parts.parts_id,wp.parts_number';
            $parts_data['add_parts'] = $this->parts_model->getJoinAll($parts_where, $parts_field, $parts_join);
            $workDetail['parts_data'] = $parts_data;

            // 获取评价信息------------------
            $evaluate_where['work_order_id'] = $work_order_id;
            $evaluate_where['type'] = 2;
            $evaluate_where['status'] = 1;
            $evaluate_field = 'evaluate_id,evaluate_stars,evaluate_content,evaluate_image,evaluate_time';
            $evaluate_data = $this->evaluate_model->getOne($evaluate_where, $evaluate_field);

            // 获取评价标签
            $evaluate_label = [];
            if ($evaluate_data) {
                if ($evaluate_data['evaluate_image']) {
                    $evaluate_data['evaluate_image'] = json_decode($evaluate_data['evaluate_image'], true);
                    foreach ($evaluate_data['evaluate_image'] as $k => $v) {
                        $evaluate_data['evaluate_image'][$k] = $this->config['qiniu']['qiniu_url'] . $v;
                    }
                }
                $el_where['etl.evaluate_id'] = $evaluate_data['evaluate_id'];
                $el_where['a.status'] = 1;
                $el_where['a.label_type'] = 2;
                $el_join = [
                    ['evaluate_to_label etl', 'etl.label_id = a.label_id', 'left join',], // 评价标签关系表--关联--评价标签表
                ];
                $el_field = 'a.label_name,a.create_time';
                $evaluate_label = $this->evaluate_model->getJoinAll($el_where, $el_field, $el_join, 'evaluate_label a');
            }
            $evaluate_data['evaluate_label'] = $evaluate_label;
            $workDetail['evaluate_data'] = $evaluate_data;
            $work_eq_join = [
                ['equipment_lists el', 'el.equipment_id = rq_work_equipment.equipment_id', 'left join'],
            ];
            $workDetail['equipment_info'] = $this->work_eq_model->getAll(array('rq_work_equipment.work_order_id' => $work_order_id), 'el.equipment_id,el.device_no,el.device_status', ['rq_work_equipment.create_time' => 'DESC'], $work_eq_join);

            return $this->jsonend(1000, '获取订单详情成功', $workDetail, true);
        } else {
            return $this->jsonend(-1000, '获取订单详情失败', $workDetail, true);
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 催单
     * @description
     * @method POST
     * @url User/Work/workReminder
     * @param work_order_id 必选 int 工单ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_workReminder()
    {
        // 接收参数 -- 判断参数
        $work_order_id = $this->parm['work_order_id'] ?? '';
        $user_id = $this->user_id;
        if (empty($work_order_id)) {
            return $this->jsonend(-1001, '工单ID不能为空');
        }

        // 1-- 获取工单信息
        $where['work_order_id'] = $work_order_id;
        $where['user_id'] = $user_id;
        $field = 'repair_id,work_order_status,order_time,contact_number,contacts,order_number,province_code,city_code,area_code,
                  is_reminder,reminder_time';
        $work_data = $this->work_model->getOne($where, $field);
        if (!$work_data) {
            return $this->jsonend(-1000, '暂无工单信息', $work_data);
        }
        // 判断催单时间是否大于一个小时
        if ($work_data['is_reminder'] == 1) {
            // 上次催单时间加一个小时
            $time = date('Y-m-d H:i:s', strtotime('+1 hour', $work_data['reminder_time']));
            $current_time = date('Y-m-d H:i:s', time());
            if ($time > $current_time) {
                return $this->jsonend(-1000, '请于您上次催单时间后一小时在催');
            }
        }

        // 2-- 修改工单为已催单
        $data['is_reminder'] = 1;
        $data['reminder_time'] = time();
        $reminder_data = $this->work_model->editWork($where, $data);
        if (!$reminder_data) {
            return $this->jsonend(-1000, '催单失败，请稍后再试2', $reminder_data);
        }
        // 2-- 通知后台--发送短信
        if ($work_data['work_order_status'] != 2 && $work_data['work_order_status'] != 3 && $work_data['work_order_status'] < 7) {
            $content = '工单号为：' . $work_data['order_number'] . '的用户：' . $work_data['contacts'] . '催单了,请尽快处理';
            $company_config = CompanyService::getCompanyConfig($this->company);
            $sms_config = $company_config['sms_config'];
            SmsService::sendSmsAsync(ConfigService::getConfig('company_hotline', false, $this->company), $content, $sms_config);
        } // 3-- 通知工程人员--模板消息or发送短信
        else {
            // 3.1-- 获取工程人员信息
            $en_where['engineers_id'] = $work_data['repair_id'];
            $repair_data = $this->engineersModel->getOne($en_where);
            //通知到用户
            $engineer_notice_config = ConfigService::getTemplateConfig('reminder', 'engineer', $this->company);
            if (!empty($engineer_notice_config) && !empty($engineer_notice_config['template']) && $engineer_notice_config['template']['switch']) {
                $template_id = $engineer_notice_config['template']['wx_template_id'];
                $mp_template_id = $engineer_notice_config['template']['mp_template_id'];
                // 3.2-- 发送模板消息
                $work_data['order_time'] = date('Y-m-d H:i:s', $work_data['order_time']);
                $content = "催单提醒！！您的" . $work_data['order_number'] . "订单被用户催单了！请尽快处理!更多详情请进入'.$this->engineer_wx_name.'小程序查看！";
                // 模板内容
                $template_data = [
                    'keyword1' => ['value' => $work_data['order_number']], // 订单号
                    'keyword2' => ['value' => $work_data['order_time']], // 下单时间
                    'keyword3' => ['value' => $content], // 催单内容
                ];
                $mp_template_data = [
                    'keyword1' => ['value' => $work_data['order_number']], // 订单号
                    'keyword2' => ['value' => $work_data['order_time']], // 下单时间
                    'keyword3' => ['value' => $content], // 催单内容
                ];
                $template_tel = $repair_data['engineers_phone'];  // 工程人员电话
                $template_content = '催单提醒！！您的' . $work_data['order_number'] . '订单被用户催单了,请尽快处理'; // 短信内容
                $template_url = '/pages/user/afterSaleDetail/afterSaleDetail?detail';
                NotifiyService::sendNotifiyAsync($repair_data['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, '', $mp_template_id, $mp_template_data, 'engineer', $this->company);//异步发送
            }


        }
        return $this->jsonend(1000, '催单成功，我们会尽快为您处理');
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 工单费用详情
     * @description
     * @method POST
     * @url User/Work/getWorkCost
     * @param work_order_id 必选 int 工单ID
     * @return {"code":1000,"message":"获取成功","data":{"total":{"pay_origin_money":"0.00","pay_other_money":"0.00","combo_money":"0.00"},"base":{"total":{"money":"0.00"},"list":[{"single_money":"0.00","total_money":"0.00","num":"1","id":"3","name":"新装"}]},"add":[],"part":[],"filter":[]}}
     * @return_param total array 总金额明细（pay_origin_money 预计支付金额,单位元，pay_other_money 其他金额,比如催费时续费金额 combo_money 实际支付金额）
     * @return_param base array 基础服务金额（total服务总金额，list费用详情（single_money单价total_money总价 num数量 id服务ID name服务名称））
     * @return_param add array 额外服务金额（与基础服务返回一致）
     * @return_param part array 配件金额（与基础服务返回一致）
     * @return_param filter array 滤芯金额（与基础服务返回一致）
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getWorkCost()
    {
        if (empty($this->parm['work_order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数工单ID");
        }
        $work_order_id = $this->parm['work_order_id'];
        $work_order_info = $this->work_model->getOne(array('work_order_id' => $work_order_id), 'pay_origin_money,pay_other_money,combo_money');
        if (empty($work_order_info)) {
            return $this->jsonend(-1001, "工单信息错误");
        }
        $data['total'] = array('pay_origin_money' => formatMoney($work_order_info['pay_origin_money']),
            'pay_other_money' => formatMoney($work_order_info['pay_other_money']),
            'combo_money' => formatMoney($work_order_info['combo_money']));
        //基础服务费用
        $base_join = [
            ['business_type b', 'b.business_type_id = rq_work_order_business.business_id', 'LEFT',]        // 关联业务类型表
        ];
        $base = $this->work_order_business_model->getAll(array('work_order_id' => $work_order_id, 'b.company_id' => $this->company), $base_join, 'rq_work_order_business.*,b.name');
        $base_final = [];
        if (!empty($base)) {
            $base_money = 0;
            $base_arr = [];
            foreach ($base as $k => $v) {
                $base_money += $v['money'];
                $a['single_money'] = formatMoney($v['single_money']);
                $a['total_money'] = formatMoney($v['money']);
                $a['num'] = $v['number'];
                $a['id'] = $v['business_id'];
                $a['name'] = $v['name'];
                $base_arr[] = $a;
            }
            $base_final['total']['money'] = formatMoney($base_money);
            $base_final['list'] = $base_arr;
        }
        //额外服务费用
        $add_join = [
            ['additional_service a', 'a.service_id = rq_work_addtional_service.service_id', 'LEFT',]        // 关联业务类型表
        ];
        $add = $this->work_additional_service_model->getAll('rq_work_addtional_service.*,a.service_name', 1, -1, ['work_service_id' => 'asc'], array('work_order_id' => $work_order_id), $add_join);
        $add_final = [];
        if (!empty($add)) {
            $add_arr = [];
            $add_money = 0;
            foreach ($add as $k => $v) {
                $add_money += $v['total_money'];
                $a['single_money'] = formatMoney($v['single_money']);
                $a['total_money'] = formatMoney($v['total_money']);
                $a['num'] = $v['service_num'];
                $a['id'] = $v['service_id'];
                $a['name'] = $v['service_name'];
                $add_arr[] = $a;
            }
            $add_final['total']['money'] = formatMoney($add_money);
            $add_final['list'] = $add_arr;
        }
        //配件费用
        $part_join = [
            ['parts p', 'p.parts_id = rq_work_parts.parts_id', 'LEFT',]        // 关联业务类型表
        ];
        $part = $this->work_parts_model->getAll(array('work_order_id' => $work_order_id), 1, -1, $part_join, 'rq_work_parts.*,p.parts_name');
        $part_final = [];
        if (!empty($part)) {
            $part_arr = [];
            $part_money = 0;
            foreach ($part as $k => $v) {
                $part_money += $v['total_money'];
                $a['single_money'] = formatMoney($v['single_money']);
                $a['total_money'] = formatMoney($v['total_money']);
                $a['num'] = $v['parts_number'];
                $a['id'] = $v['parts_id'];
                $a['name'] = $v['parts_name'];
                $part_arr[] = $a;
            }
            $part_final['total']['money'] = formatMoney($part_money);
            $part_final['list'] = $part_arr;
        }
        //滤芯费用
        $filter_join = [
            ['parts p', 'p.parts_id = rq_for_core_record.parts_id', 'LEFT',]        // 关联业务类型表
        ];
        $filter = $this->for_core_model->getAll(array('work_order_id' => $work_order_id), 1, -1, $filter_join, 'rq_for_core_record.*,p.parts_name');
        $filter_final = [];
        if (!empty($filter)) {
            $filter_momey = 0;
            $filter_arr = [];
            foreach ($filter as $k => $v) {
                $filter_momey += $v['money'];
                $a['single_money'] = formatMoney($v['money']);
                $a['total_money'] = formatMoney($v['money']);
                $a['num'] = $v['parts_number'];
                $a['id'] = $v['parts_id'];
                $a['name'] = $v['parts_name'];
                $filter_arr[] = $a;
            }
            $filter_final['total']['money'] = formatMoney($filter_momey);
            $filter_final['list'] = $filter_arr;
        }
        $data['base'] = $base_final;
        $data['add'] = $add_final;
        $data['part'] = $part_final;
        $data['filter'] = $filter_final;
        return $this->jsonend(1000, '获取成功', $data);
    }


    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 根据订单获取用户类型
     * @description
     * @method POST
     * @url User/Work/getUserClass
     * @param order_id 必选 int 订单ID
     * @param key 必选 string 对公转账配置值 enginer_transfer_pay
     * @return {"code": 1000,"message": "操作成功"}
     * @return_param
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-19
     */
    public function http_getUserClass()
    {
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数订单ID");
        }
        if (empty($this->parm['key'])) {
            return $this->jsonend(-1001, "缺少参数key");
        }
        $where['order_id'] = $this->parm['order_id'];
        $join = [
            ['customer c', 'c.user_id = rq_orders.user_id', 'left',]  // 关联用户表
        ];
        $info = $this->order_model->getOne($where, 'rq_orders.user_id,rq_orders.order_id,c.user_class', $join);
        $key = ConfigService::getConfig($this->parm['key'], true, $this->company);//获取配置

        if (empty($info || $key)) {
            return $this->jsonend(-1003, "暂无数据");
        }

        $data = [];
        $data['user_class'] = $info['user_class'] ?? 1;
        $data['key'] = $key ?? 0;
        return $this->jsonend(1000, "获取成功", $data);

    }

}
