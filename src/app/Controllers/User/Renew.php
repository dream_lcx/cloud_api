<?php

namespace app\Controllers\User;

use app\Services\Common\AdaPayService;
use app\Services\Common\HttpService;
use app\Services\Common\WxPayService;
use app\Services\Contract\ContractService;
use app\Services\User\WorkOrderService;
use Server\CoreBase\SwooleException;

/**   YSF
 *    续费管理 API 控制器
 *    Date: 2018/8/3
 * Class Renew
 * @package app\Controllers\User
 */
class Renew extends Base
{

    protected $equipmentListsModel;
    protected $renewOrderModel;
    protected $contract_additional_model;
    protected $userModel;
    protected $contract_model;
    protected $contract_ep_model;
    protected $renew_record_model;
    protected $renew_order_eq_model;
    protected $Achievement;
    protected $oaWorkersModel;
    // 前置方式，加载模型
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name); // TODO: Change the autogenerated stub
        // 设备模型
        $this->equipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        // 续费订单模型
        $this->renewOrderModel = $this->loader->model('RenewOrderModel', $this);
        // 用户模型
        $this->userModel = $this->loader->model('CustomerModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->renew_record_model = $this->loader->model('RenewRecordModel', $this);
        $this->renew_order_eq_model = $this->loader->model('RenewOrderEquipmentModel', $this);
        $this->oaWorkersModel = $this->loader->model('OaWorkersModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->contract_additional_model = $this->loader->model('ContractAdditionalModel', $this);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/续费相关
     * @title  续费下单
     * @description
     * @method POST
     * @url User/Renew/addRenewOrder
     * @param contract_id 必选 int 合同ID
     * @param coupon 非必选 array 优惠券ID集合
     * @return
     * @remark
     * @number 0
     * @throws SwooleException
     * @author lcx
     * @date 2018-10-18
     */
    public function http_addRenewOrder()
    {
        /**
         * 扎帐 20230901
         * 1.开启扎帐
         * 2.支付金额大于0
         * 3.当前在扎帐日期期间
         */
        $date = date('d', time());
        if ($this->config['system_clearing_swtich'] == true && $this->config['system_clearing_date'] >= $date) {
            return $this->jsonend(-1000, $this->config['system_clearing_text']);
        }
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数合同ID');
        }
        $coupon = $this->parm['coupon'] ?? [];
        $post_disaccount_money = $this->parm['disaccount_money'] ?? 0;
        //查询合同信息
        $contract_info = $this->contract_model->getOne(array('contract_id' => $this->parm['contract_id']), 'status,package_mode,package_value,package_money,package_cycle,renew_money,package_id');
        if (empty($contract_info)) {
            return $this->jsonend(-1100, "合同不存在");
        }
        /**
         * @2024-3-26 14:25
         * @续费合同 验证工单状态/用户是否支付
         * @author huangchunpeng
        */
       $workOrderService = new WorkOrderService();
       [$ok,$result] = $workOrderService->isPay($this->parm['contract_id']);
       if(!$ok){
           return $this->jsonend(-1000,$result);
       }
//        if ($contract_info['status'] != 4) {
//            return $this->jsonend(-1101, "该合同非正常状态,无法续费");
//        }
        //查询合同设备信息
        $contract_ep_info = $this->contract_ep_model->getAll(array('contract_id' => $this->parm['contract_id']), 'equipment_id');
        if (empty($contract_ep_info)) {
            return $this->jsonend(-1102, "该合同下无正常运行的设备,无法续费");
        }
        //2023年12月21日续费必须签合同
        $renew_order = $this->renewOrderModel->getOne(['contract_id' => $this->parm['contract_id'], 'pay_status' => 2,'pay_time'=>['>',1703088000]], 'renew_order_id, status', [], ['renew_order_id' => 'desc']);
        if (!empty($renew_order)) {
            // 续费状态1待支付 2待下发套餐 3已下发套餐 4已退款
            if ($renew_order['status'] != 4) {
                $contract_additional = $this->contract_additional_model->getOne(['contract_id' => $this->parm['contract_id'], 'renew_order_id' => $renew_order['renew_order_id']]);
                if (empty($contract_additional)) {
                    return $this->jsonend(-1000, '无法续费，请完成上个续费合同的签署');
                }
            }
        }


        $order_actual_money = $contract_info['renew_money'];
        //使用优惠券
        $coupon_number = 0;
        $coupon_total_money = 0;
        $coupon_id = '';
        if (!empty($coupon)) {
            if (!is_array($coupon)) {
                return $this->jsonend(-1000, '优惠券上传格式错误');
            }
            $coupon_id = implode(',', $coupon);
            $where = [
                'coupon_id' => ['IN', $coupon]
            ];
            $field = '
                    rq_coupon_cate.money,
                    rq_coupon.coupon_id,
                    rq_coupon.type,
                    rq_coupon.`status`,
                    rq_coupon.`code`,
                    rq_coupon.valid_time_start,
                    rq_coupon.valid_time_end
                ';
            $order = [
                'coupon_id' => 'ASC'
            ];
            $join = [
                ['coupon_cate', 'rq_coupon.cid = rq_coupon_cate.id', 'LEFT']
            ];
            $this->Achievement->table = 'coupon';
            $coupon = $this->Achievement->selectData($where, $field, $join, $order);

            foreach ($coupon as $key => $value) {
                //判断券类型
                if ($value['type'] != 5) {
                    return $this->jsonend(-1000, '券码：' . $value['code'] . ' 不是续费类型，不能使用');
                }
                if ($value['status'] != 1) {
                    return $this->jsonend(-1000, '券码：' . $value['code'] . ' 状态异常，不能使用');
                }
                if (!empty($value['valid_time_start']) && $value['valid_time_start'] > time()) {
                    return $this->jsonend(-1000, '券码：' . $value['code'] . ' 使用时间还未开始，开始时间：' . date('Y-m-d H:i:s', $value['valid_time_start']));
                }
                if (!empty($value['valid_time_end']) && $value['valid_time_end'] < time()) {
                    return $this->jsonend(-1000, '券码：' . $value['code'] . ' 已过期不能使用，过期时间：' . date('Y-m-d H:i:s', $value['valid_time_end']));
                }
                $coupon_total_money += bcdiv($value['money'], 100);
                $order_actual_money -= bcdiv($value['money'], 100);
                if ($order_actual_money < 0) {
                    return $this->jsonend(-1000, '优惠券最多可选择：' . $coupon_number . '张');
                }
                $coupon_number += 1;
            }
        }
        //账户余额抵扣
        $disaccount_money = 0;
        if ($order_actual_money > 0) {
            $where = [
                'user_id' => $this->user_id
            ];
            $field = '
            rq_oa_workers.workers_id,
            rq_oa_workers.total_money,
            rq_oa_workers.cash_available,
            rq_oa_workers.commissions,
            rq_oa_workers.already_present_money
        ';
            $this->Achievement->table = 'oa_workers';
            $result = $this->Achievement->findData($where, $field);
            $available_money = $result['cash_available'] ?? 0;
            $disaccount_money = $order_actual_money;//优先优惠券抵扣
            if ($order_actual_money > $available_money) {//若余额不足，最多抵扣余额部分
                $disaccount_money = $available_money;
            }
            $order_actual_money -= $disaccount_money;
        }
        // TODO 这里的续费金额 需要重新查询更新
//        $renewMoney = ContractService::findRenewMoneyAndUpdate($this->parm['contract_id'],$contract_info['renew_money'],$contract_info['package_id']);
//        $contract_info['package_money'] = $renewMoney;
        $add_status = false;
        $renew_order_id = '';
        $this->db->begin(function () use (&$add_status, $order_actual_money, $disaccount_money, $coupon_number, $coupon_total_money, $coupon_id, $contract_info, $contract_ep_info, &$renew_order_id) {
            // 2-- 添加一条续费订单
            $data['user_id'] = $this->user_id;
            $data['renew_order_no'] = pay_sn('YJY');
            $data['contract_id'] = $this->parm['contract_id'];
            $data['package_mode'] = $contract_info['package_mode'];
            $data['package_value'] = $contract_info['package_value'];
            $data['package_money'] = $contract_info['package_money'];
            $data['package_cycle'] = $contract_info['package_cycle'];
            $data['order_actual_money'] = $order_actual_money;
            $data['pay_status'] = 1;  // 未支付
            $data['create_time'] = time();
            $data['status'] = 1;
            $data['coupon_number'] = $coupon_number;
            $data['coupon_total_money'] = $coupon_total_money;
            $data['coupon_id'] = $coupon_id;
            $data['order_total_money'] = $contract_info['renew_money'];
            $data['deduction_account_balance'] = $disaccount_money;//账户余额抵扣金额
            $data['opertaor_role'] = 1;
            $data['opertaor_uid'] = $this->user_id;

            $renew_order_id = $this->renewOrderModel->add($data);
            $add_status = true;
        });
        if ($add_status) {
            return $this->jsonend(1000, '续费下单成功', array('renew_order_id' => $renew_order_id));
        } else {
            return $this->jsonend(-1000, '续费下单失败');
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/续费相关
     * @title 续费订单支付
     * @description
     * @method POST
     * @url User/Renew/renewOrderPay
     * @param renew_order_id 必选 int 续费订单ID
     * @param is_paying_agent 可选 int 是否代付,1:是2:否
     * @param paying_agent_deduction_account_balance 可选 int 代付账户抵扣金额
     * @param paying_agent_role 可选 int 代付人角色0:否,1:用户,2:经销商,3:工程
     * @param pay_way 可选 int 支付方式1微信支付，默认1
     * @return
     * @remark
     * @number 0
     * @throws SwooleException
     * @author lcx
     * @date 2018-10-18
     */
    public function http_renewOrderPay()
    {
        // 接收参数
        $user_id = $this->user_id;
        $renew_order_id = $this->parm['renew_order_id'] ?? '';
        $pay_way = $this->parm['pay_way'] ?? '';
        $redis_lock = $this->redis->get('renew_order_pay' . $renew_order_id);
        if ($redis_lock) {
            return $this->jsonend(-1000, '请勿频繁操作,10s后重试');
        }
        $this->redis->set('renew_order_pay' . $renew_order_id, $renew_order_id, 10);
        // 判断参数1
        if (empty($renew_order_id)) {
            return $this->jsonend(-1001, '请选择订单');
        }
        if (empty($pay_way)) {
            return $this->jsonend(-1001, '请选择支付方式');
        }
        if ($pay_way != 1) {
            return $this->jsonend(-1103, '目前仅支持微信支付');
        }

        // 1-- 判断订单是否存在
        $where['renew_order_id'] = $renew_order_id;
        $renew_order_data = $this->renewOrderModel->getOne($where);
        unset($where);
        if (empty($renew_order_data)) {
            return $this->jsonend(-1000, '订单不存在', $renew_order_data);
        }
        if ($renew_order_data['pay_status'] == 2) {
            return $this->jsonend(-1000, '订单已经支付过了');
        }

        // 2-- 判断用户是否正常
        $where['user_id'] = $user_id;
        $user_info = $this->userModel->getOne($where, 'openid, user_id');
        if (empty($user_info) || empty($user_info['openid'])) {
            return $this->jsonend(-1103, "用户信息错误");
        }

        //查询合同设备信息
        $contract_ep_info = $this->contract_ep_model->getAll(array('contract_id' => $renew_order_data['contract_id']), 'equipment_id');
        if (empty($contract_ep_info)) {
            return $this->jsonend(-1102, "该合同下无正常运行的设备,无法续费");
        }
        //查询合同信息
        $contract_info = $this->contract_model->getOne(array('contract_id' => $renew_order_data['contract_id']), 'company_id');
        if (empty($contract_info)) {
            return $this->jsonend(-1100, "合同不存在");
        }
        // 01-- 修改订单信息
        $data['pay_way'] = $this->parm['pay_way'];
        $this->renewOrderModel->edit(['renew_order_id' => $renew_order_data['renew_order_id']], $data);
        $paying_agent_user_openid = $this->parm['paying_agent_user_openid'] ?? $user_info['openid'];//代付人用户OPENID
        $is_paying_agent = $this->parm['is_paying_agent'] ?? 2;//是否代付 1:是 2:否
        $paying_agent_deduction_account_balance = $this->parm['paying_agent_deduction_account_balance'] ?? 0;//代付账户余额抵扣金额
        $paying_agent_user_id = $this->user_id;//代付人用户ID
        $paying_agent_role = $this->parm['paying_agent_role'] ?? 0;//代付人角色 1:用户 2:经销商 3:工程,目前只处理经销商代付
        //代付-判断余额
        if ($is_paying_agent == 1 && $paying_agent_deduction_account_balance > 0) {
            if ($paying_agent_role == 2) {//经销商代付
                $worker_info = $this->oaWorkersModel->getOne(['user_id' => $paying_agent_user_id, 'is_delete' => 0], 'cash_available');
                if ($worker_info['cash_available'] < $paying_agent_deduction_account_balance) {
                    return $this->jsonend(-1103, "账户余额不足");
                }
            } else {
                return $this->jsonend(-1103, "暂不支持其他角色代付");
            }
            if ($paying_agent_deduction_account_balance > $renew_order_data['order_actual_money']) {
                return $this->jsonend(-1102, "最多可抵扣" . $renew_order_data['order_actual_money'] . '元');
            }

        }
        // 3-- 开始支付
        $status = false;
        $jsapi = '';
        $code = 0;
        // 开启事务
        $this->db->begin(function () use (&$status, &$jsapi, &$code, $is_paying_agent, $paying_agent_role, $paying_agent_user_id, $paying_agent_deduction_account_balance, $paying_agent_user_openid, $renew_order_data, $user_info) {
            dump('用户端续费');
            // 02-- 调起微信支付
            if ($this->parm['pay_way'] == 1) {
                $edit_order_data['is_paying_agent'] = $is_paying_agent;
                $edit_order_data['paying_agent_role'] = $paying_agent_role;
                $edit_order_data['paying_agent_user_id'] = $paying_agent_user_id;
                $edit_order_data['paying_agent_deduction_account_balance'] = $paying_agent_deduction_account_balance;
                $edit_order_data['paying_agent_user_openid'] = $paying_agent_user_openid;
                if ($paying_agent_deduction_account_balance > 0) {
                    $edit_order_data['order_actual_money'] = $renew_order_data['order_actual_money'] - $paying_agent_deduction_account_balance;
                }
                $edit_order_data['pay_mode'] = $this->pay_config['mode'] ?? 1;
                $edit_order_data['pay_mchid'] = $this->pay_config['mode'] == 2 ? $this->pay_config['sub_mch_id'] : $this->wx_config['mchid'];
                $edit_order_data['renew_order_no'] = $renew_order_data['renew_order_no'] . rand(1, 9);
                $this->renewOrderModel->edit(['renew_order_id' => $renew_order_data['renew_order_id']], $edit_order_data);//修改订单支付模式
                $pay_data['order_sn'] = $edit_order_data['renew_order_no'];  // 订单编号
                $pay_data['order_money'] = $renew_order_data['order_actual_money'];  // 订单付款金额
                $pay_data['money'] = $renew_order_data['order_actual_money'];  // 订单付款金额
                $wx_pay_data['money'] = $pay_data['money'];
                $pay_data['app_id'] = $this->wx_config['adapay_app_id']; // 聚合支付ID
//                if($pay_data['money']==0){
//                    $pay_data['money'] = 0.01;
//                }
                if ($pay_data['money'] != 0) {
                    // ----------------- begin -----------------
//                    $notify_url = $this->config->get('callback_domain_name') . '/User/Callback/rentRenewOrder';
//                    if ($this->dev_mode == 1) {
//                        $pay_data['money'] = 0.01;
//                        $notify_url = $this->config->get('debug_config.callback_domain_name') . '/User/Callback/rentRenewOrder';
//                    }
//                    $jsapi = WxPayService::pay($this->wx_config, $user_info['openid'], $pay_data, $notify_url, $this->pay_config['sub_mch_id'] ?? '', $this->pay_config['mode'] ?? 1);
//                    $code = 1000;
                    // ----------------- end -----------------

                    // 0-100分的随机减扣，避免产生大量相同金额的支付订单
                    $deduction = $this->redis->get('user_renew_deduction:' . $renew_order_data['contract_id']);
                    if (!$deduction) {
                        // 0-100分的随机减扣，避免产生大量相同金额的支付订单
                        $deduction = mt_rand(1, 99);
                        $this->redis->set('user_renew_deduction:' . $renew_order_data['contract_id'], $deduction, 3600 * 24);
                    }
                    $pay_data['money'] = ($pay_data['money'] * 100 - $deduction) / 100.0;

                    // 顺哥哥-15802333413 的用户ID
                    if ($user_info['user_id'] == 86735) {
                        $arr = ['0.01', '0.02', '0.03', '0.04', '0.05', '0.06'];
                        $pay_data['money'] = $arr[mt_rand(0, 4)];
                    }

                    $notify_url = $this->config->get('callback_domain_name') . '/User/Callback/rentAdaPayRenewOrder';
                    if ($this->dev_mode == 1) {
                        $pay_data['money'] = '0.02';
                        $notify_url = $this->config->get('debug_config.callback_domain_name') . '/User/Callback/rentAdaPayRenewOrder';
                    }
                    $result = AdaPayService::pay($pay_data, $notify_url, $user_info);
                    if ($result['code'] == -1000) {
                        $pay_data['money'] = $wx_pay_data['money'];
                        // 聚合支付失败时，调用微信支付
                        $notify_url = $this->config->get('callback_domain_name') . '/User/Callback/rentRenewOrder';
                        if ($this->dev_mode == 1) {
                            $pay_data['money'] = 0.02;
                            $notify_url = $this->config->get('debug_config.callback_domain_name') . '/User/Callback/rentRenewOrder';
                        }
                        $jsapi = WxPayService::pay($this->wx_config, $user_info['openid'], $pay_data, $notify_url, $this->pay_config['sub_mch_id'] ?? '', $this->pay_config['mode'] ?? 1);
                    } else {
                        $jsapi = $result['data'];
                    }
                    $code = 1000;
                } else {
                    $jsapi = $this->renewOrderModel->renewCallback($pay_data);
//                    $url = '/User/Callback/renewCallback';
//                    $jsapi = HttpService::Thrash($pay_data, $url);
                    if ($jsapi) {
                        $code = 1001;
                    } else {
                        $code = -1000;
                    }

                }
            }
            $status = true;
        }, function ($e) {
            return $this->jsonend(-1000, '支付失败', $e->error);
        });

        // 判断支付结果
        if ($status) {
            $renew_order['renew_order_id'] = $renew_order_id;
            $renew_order['jsapi'] = $jsapi;
            return $this->jsonend($code, '支付成功', $renew_order);
        }
        return $this->jsonend(-1000, '支付失败');
    }

    /**
     * showdoc
     * @catalog API文档/用户端/续费相关
     * @title 查询续费订单详情
     * @description
     * @method POST
     * @url User/Renew/getRenewOrderDetail
     * @param renew_order_id 必选 int 订单ID
     * @return {"code":1000,"message":"获取续费订单成功","data":{"renew_order_id":"1","renew_order_no":"YJY917815783118333","user_id":"5","contract_id":"3","package_mode":"1","package_value":"100","package_money":"600.00","package_cycle":"180","pay_status":"1","wx_callback_num":null,"pay_way":null,"order_actual_money":"600.00","create_time":"1537181578","pay_time":null,"status":"1","renew_time":null}}
     * @return_param renew_order_no string 订单编号
     * @return_param renew_order_id int 订单ID
     * @return_param order_actual_money string 支付金额
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getRenewOrderDetail()
    {
        // 接收参数
        $user_id = $this->user_id;
        $renew_order_id = $this->parm['renew_order_id'];
        // 判断参数
        if (empty($renew_order_id)) {
            return $this->jsonend(-1001, '续费ID不能为空');
        }
        // 条件
        $where['renew_order_id'] = $renew_order_id;
//        $where['user_id'] = $user_id;
        // 调用模型查询数据
        $renew_order_data = $this->renewOrderModel->getOne($where);
        if ($renew_order_data) {
            $renew_order_data['create_time'] = date('Y-m-d H:i:s', $renew_order_data['create_time']);
            return $this->jsonend(1000, '获取续费订单成功', $renew_order_data);
        }
        return $this->jsonend(-1003, '没有数据');
    }

    /**
     * showdoc
     * @catalog API文档/用户端/续费相关
     * @title 获取用户续费优惠券
     * @description
     * @method POST
     * @url User/Renew/getCoupon
     * @param row 非必选 int 每页显示数目
     * @param page 非必选 int 页码
     * @param status 非必选 string|array 状态
     * @return {"code":1000,"message":"获取续费订单成功","data":"详细数据见返回"}
     * @return_param coupon_id int 优惠券ID
     * @return_param code string 优惠券码
     * @return_param status int 优惠券状态，0：待领取 1：待使用，2：已使用，3：已过期
     * @return_param use_time string 优惠券使用时间
     * @return_param send_time string 优惠券发放时间
     * @return_param valid_time_start string 优惠券有效期开始时间
     * @return_param valid_time_end string 优惠券有效期结束时间
     * @return_param is_use int 是否可用,1:可用，0：不可用
     * @author ligang
     * @date 2019/8/1 16:52
     */
    public function http_getCoupon()
    {
        $row = $this->parm['row'] ?? 10;
        $page = $this->parm['page'] ?? 1;
        $status = $this->parm['status'] ?? '';

        $where = [
            'rq_coupon.uid' => $this->user_id,
            'rq_coupon.type' => 5
        ];
        // 查询当前合同
        // TODO 合同之前存在移机情况 移机会生成新合同 发放的优惠券根据合同id来的 新生成的合同id不同 导致优惠券查询不出来
        $selectField = 'contract_id,pid';
        $contractIdArr[] = 0;
        $contractIdArr[] = $this->parm['contract_id'];
        $isTrue = true;
        $contractModel = $this->loader->model("ContractModel",$this);
        $selectContractId = $this->parm['contract_id'];
        while ($isTrue){
            $contract = $contractModel->getOne(['contract_id'=>$selectContractId], $selectField);
            if($contract['pid']){
                $contractIdArr[] = $contract['pid'];
                $selectContractId = $contract['pid'];
            }else{
                $isTrue = false;
            }
        }
        dump($contractIdArr);
        $where['rq_contract.source_contract_id'] = ['IN',$contractIdArr];
        if (strlen($status) > 0) {
            if (is_array($status)) {
                $where['rq_coupon.status'] = ['IN', $status];
            } else {
                $where['rq_coupon.status'] = $status;
                if ($status == 1) {
                    $where['rq_coupon.valid_time_end'] = ['>=', time()];
                }
            }
        }

        $field = '
            rq_coupon.coupon_id,
            rq_coupon.code,
            rq_coupon.status,
            rq_coupon.use_time,
            rq_coupon.send_time,
            rq_coupon.valid_time_start,
            rq_coupon.valid_time_end,
            rq_coupon_cate.money,
            rq_contract.contact_person,
            rq_contract.phone
        ';
        $join = [
            ['coupon_cate', 'rq_coupon.cid = rq_coupon_cate.id', 'left'],   // 优惠券分类表--优惠券表
            ['contract', 'rq_contract.contract_id = rq_coupon.from_contract_id', 'left']   // 优惠券表--合同表
        ];

        $order = [
            'rq_coupon.status' => 'ASC',
            'rq_coupon.send_time' => 'DESC',
            'rq_coupon.coupon_id' => 'ASC',
        ];
        $this->Achievement->table = 'coupon';
        $result = $this->Achievement->selectPageData($where, $field, $join, $row, $page, $order);
        $count = $this->Achievement->findJoinData($where, 'COUNT(*) AS number', $join);
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key]['checkbox'] = false;//是否可用
                $result[$key]['is_use'] = 1;//是否可用
                if ($value['valid_time_start'] > time() || $value['valid_time_end'] < time()) {
                    $result[$key]['is_use'] = 0;
                }
                if ($value['status'] == 1 && $value['valid_time_end'] < time()) {
                    $result[$key]['status'] = 3;
                }
                $arr_key = array_keys($value);
                foreach ($arr_key as $k => $v) {
                    if (is_null($value[$v])) {
                        $result[$key][$v] = '';
                    }
                    if (stripos($v, 'time') !== false && !empty($value[$v])) {
                        $result[$key][$v] = date('Y-m-d H:i:s', $value[$v]);
                    }
                    if (stripos($v, 'phone') !== false && !empty($value[$v])) {
                        $result[$key][$v] = substrBank($value[$v]);
                    }
                    if (stripos($v, 'money') !== false && !empty($value[$v])) {
                        $result[$key][$v] = formatMoney($value[$v], 2);
                    }
                }
            }
        }
        $count = $count['number'] ?? 0;
        $data = [
            'data' => $result,
            'page' => [
                'current_page' => $page,
                'row' => $row,
                'total' => $count,
                'total_page' => ceil($count / $row)
            ]
        ];
        return $this->jsonend(1000, '获取成功', $data);
    }

    /**
     * showdoc
     * @catalog API文档/工程端/续费相关
     * @title 续费记录
     * @description
     * @method POST
     * @url User/Renew/record
     * @param contract_id 必选 contract_id 页数
     * @param page 必选 int 页数
     * @param pagesize 必选 int 每页条数
     * @return
     * @author xg
     * @date 2020/1/5 22:52
     */
    public function http_record()
    {
        $page = empty($this->parm['page'] ?? '') ? 1 : $this->parm['page'];
        $pagesize = empty($this->parm['pagesize'] ?? '') ? 1 : $this->parm['pagesize'];
        $contract_id = $this->parm['contract_id'] ?? '';

        if (empty($contract_id)) {
            return $this->jsonend(-1003, '缺少合同参数');
        }
        $field = 'rq_renew_order.*,rq_contract.contract_no,realname,nickname,telphone';
        $where['rq_renew_order.contract_id'] = $contract_id;
        $join = [
            ['contract', 'rq_contract.contract_id=rq_renew_order.contract_id', 'left'],
            ['customer', 'rq_customer.user_id=rq_renew_order.user_id', 'left'],
        ];
        $data = $this->renewOrderModel->getAll($where, $page, $pagesize, $field, $join, ['renew_time' => 'DESC']);
        if (empty($data)) {
            return $this->jsonend(-1003, '暂无数据');
        }
        foreach ($data as $k => $v) {
            $data[$k]['pay_status_desc'] = $v['pay_status'] == 2 ? '已支付' : '未支付';
            if($v['status']==4){
                $data[$k]['pay_status_desc'] = '已退款';
            }
            $data[$k]['pay_time'] = empty($v['pay_time']) ? '' : date('Y-m-d H:i:s', $v['pay_time']);
            $data[$k]['renew_time'] = empty($v['renew_time']) ? '' : date('Y-m-d H:i:s', $v['renew_time']);
            $data[$k]['deduction_account_balance'] = empty($v['deduction_account_balance']) ? 0 : $v['deduction_account_balance'];
            $data[$k]['username'] = empty($v['realname']) ? urldecode($v['nickname']) : $v['realname'];
        }
        return $this->jsonend(1000, '获取成功', $data);
    }

}
