<?php

namespace app\Controllers\User;

use AdaPaySdk\AdapayTools;
use app\Services\Engineer\WorkOrderService;
use app\Services\Log\FileLogService;
use app\Services\User\UserService;
use Server\CoreBase\Controller;
use app\Wechat\WxPay;
use app\Library\Alipay\aop\AopClient;
use app\Library\SLog;
use Server\CoreBase\SwooleException;
use app\Services\Common\EmailService;
use app\Services\Common\ConfigService;
use app\Services\Common\HttpService;
use app\Services\Common\NotifiyService;
use app\Services\Common\DataService;
use app\Services\Company\CompanyService;

/**
 * 支付回调
 * Class Callback
 * @package app\Controllers\User
 */
class Callback extends Controller
{

    /**
     * 初始化
     * Callback constructor.
     */
    private $order_model;
    private $user_model;
    private $log_model;
    private $admin_info_model;
    private $user_center_model;
    private $contract_model;
    private $renewOrderModel;
    protected $equipment_model;
    private $equipmentListsModel;
    private $eq_log_model;
    private $contract_log_model;
    protected $renew_eq_model;
    protected $wx_config;
    protected $order_logistics_model;
    protected $contract_ep_model;
    protected $admin_user_model;
    protected $op_user_model;
    protected $administrative_user_model;
    protected $finance_record_model;
    protected $renew_record_model;
    protected $Achievement;
    protected $GetIPAddressHttpClient;
    protected $BossApiClient;
    protected $userService;
    protected $service_payment_config;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);

        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->order_model = $this->loader->model('OrderModel', $this);
        $this->log_model = $this->loader->model('OrderLogModel', $this);
        $this->admin_info_model = $this->loader->model('AdministrativeInfoModel', $this);
        $this->user_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->renewOrderModel = $this->loader->model('RenewOrderModel', $this);
        $this->equipment_model = $this->loader->model('EquipmentModel', $this);
        $this->equipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->eq_log_model = $this->loader->model('EquipmentLogModel', $this);
        $this->contract_log_model = $this->loader->model('ContractLogModel', $this);
        $this->renew_eq_model = $this->loader->model('RenewOrderEquipmentModel', $this);
        $this->order_logistics_model = $this->loader->model('OrderLogisticsModel', $this);
        $this->bill_model = $this->loader->model('EngineerBillModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->admin_user_model = $this->loader->model('AdminUserModel', $this);
        $this->op_user_model = $this->loader->model('OperationUserModel', $this);
        $this->administrative_user_model = $this->loader->model('AdministrativeUserModel', $this);
        $this->finance_record_model = $this->loader->model('FinanceRecordModel', $this);
        $this->renew_record_model = $this->loader->model('RenewRecordModel', $this);

        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->userService = new UserService();
        $this->service_payment_config = $this->config->get('service_payment_config');


    }

    public function __construct()
    {
        parent::__construct();
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
        $this->BossApiClient = get_instance()->getAsynPool('BossApi');

    }

    //租赁支付回调
    public function http_rentOrder()
    {
        //验证微信签名
        $postStr = $this->http_input->getRawContent();
        file_put_contents('buy_cloud.txt', $postStr);
        // $postStr = file_get_contents('buy_cloud.txt');
        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($postObj->result_code == 'SUCCESS' && $postObj->return_code == 'SUCCESS') {
            $order_sn = $postObj->out_trade_no;
            $order = $this->order_model->getOne(['order_no' => $order_sn], '*');
            if (empty($order)) {
                file_put_contents('pay_call_order_null.txt', date('Ymd His') . ':' . $order_sn . PHP_EOL, FILE_APPEND | LOCK_EX);
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_mode'] == 2) {
                $wx_config = $this->service_payment_config;
            } else {
                $company_config = CompanyService::getCompanyConfig($order['company_id']);
                $wx_config = $company_config['wx_config'];
            }
            $wx_config['payment_type'] = 'JSAPI';
            //$WxPay = new WxPay($wx_config);
            $WxPay = WxPay::getInstance($wx_config);
            if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);
            $signObj = [];
            foreach ($postObj as $key => $val) {
                if ($key != 'sign') {
                    $signObj[$key] = $val;
                }
            }

            //验证签名
            if ($WxPay->getSign($signObj) == $postObj->sign) {
                $order['wx_callback_num'] = $postObj->transaction_id;
                $total_fee = $postObj->total_fee;
                $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'realname,username,openid,telphone,user_id,source');
                if (!empty($user_info['realname'])) {
                    $user_info['username'] = $user_info['realname'];
                }
                $status = false;
                $this->db->begin(function () use ($order, &$data, &$status, $total_fee, $user_info) {
                    //修改订单状态,如果走发货流程 order_status=2 待发货 反之 order_status=3 待安装
                    $order_status = 3;
                    $deliver_way = 0;
                    if ($order['order_type'] == 2) {
                        $order_status = 2;
                        $deliver_way = 2;
                    }
                    //如果已经是已安装不修改订单状态
                    if ($order['order_status'] == 4) {
                        $order_status = 4;
                    }
                    $this->order_model->save(['order_id' => $order['order_id']], ['order_status' => $order_status, 'order_actual_money' => $total_fee / 100,
                        'pay_status' => 2, 'pay_time' => time(), 'wx_callback_num' => $order['wx_callback_num'], 'deliver_way' => $deliver_way]);
                    //添加订单日志
                    $this->addOrderLog($order['order_id'], $order_status, "您的订单已支付成功,我们将尽快进行处理,订单编号:" . $order['order_no'], 1);
                    //添加用户与行政中心关系
                    $this->userService->addUserCenter($order['user_id'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
                    $user_id = $order['user_id'];
                    $payment_method = $order['is_paying_agent'] == 1 ? 4 : 1;
                    //如果是代付且金额大于0,更新代付人账户余额
                    if (!empty($order['paying_agent_deduction_account_balance']) && $order['paying_agent_deduction_account_balance'] > 0) {
                        //扣取账户
                        $this->Achievement->table = 'oa_workers';
                        $workers = $this->Achievement->findData(['user_id' => $order['paying_agent_user_id']], 'cash_available,workers_id');
                        $cash_available = $workers['cash_available'] - $order['paying_agent_deduction_account_balance'];
                        $this->Achievement->updateData(['user_id' => $order['paying_agent_user_id']], ['cash_available' => $cash_available]);
                        //写入资金变动记录
                        $funds_record = [
                            'user_id' => $workers['workers_id'],
                            'user_type' => 1,
                            'source' => 6,
                            'account_type' => 1,
                            'order_id' => $order['order_id'],
                            'money' => $order['paying_agent_deduction_account_balance'],
                            'note' => '订单单号:' . $order['order_no'] . ',代付发起人:' . $user_info['username'] . '(联系电话:' . $user_info['telphone'] . ')',
                            'add_time' => time(),
                        ];
                        $this->Achievement->table = 'funds_record';
                        $this->Achievement->insertData($funds_record);
                        $user_id = $order['paying_agent_user_id'];
                    }
                    //添加物流信息
                    $this->addOrderLogistics($order['order_id'], 1, "您的订单开始处理", $order['user_id']);
                    $equipments = $this->equipment_model->getOne(['equipments_id'=>$order['equipments_id']]);
                    if(!empty($equipments) && $equipments['equipments_line'] == 3){
                        $finance_record['remarks'] = '礼品兑换';
                    }
                    //新增资金记录
                    $finance_record['type'] = 1;
                    $finance_record['order_id'] = $order['order_id'];
                    $finance_record['user_id'] = $order['user_id'];
                    $finance_record['money'] = $total_fee;
                    $finance_record['total_money'] = formatMoney($order['order_total_money'], 1);
                    $finance_record['coupon_money'] = formatMoney($order['coupon_money'], 1);
                    $finance_record['deduction_account_balance'] = formatMoney($order['paying_agent_deduction_account_balance'], 1);
                    $finance_record['create_time'] = time();
                    $finance_record['is_online_pay'] = 1;
                    $finance_record['pay_way'] = 1;
                    $finance_record['callback_num'] = $order['wx_callback_num'];
                    $finance_record['payment_method'] = $payment_method;
                    $finance_record['payment_uid'] = $user_id;
                    $finance_record['o_id'] = $order['operation_id'];
                    $finance_record['a_id'] = $order['administrative_id'];
                    $finance_record['company_id'] = $order['company_id'];
                    // 按照%0.6 千分之6 来计算 手续费
                    $finance_record['service_money'] = serviceMoneyFormat($finance_record['money']);
                    $this->finance_record_model->add($finance_record);

                    $status = true;
                });
                if ($status) {

                    $eq_info = $this->equipment_model->getDetail(array('equipments_id' => $order['equipments_id']), 'equipments_name,model');
                    //通知到用户
                    $user_notice_config = ConfigService::getTemplateConfig('user_new_order', 'user', $order['company_id']);
                    if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                        $template_id = $user_notice_config['template']['wx_template_id'];
                        $mp_template_id = $user_notice_config['template']['mp_template_id'];
                        //数据组装
                        $pay_way = '微信支付';
                        $order_time = date('Y-m-d H:i:s', $order['create_time']);
                        $package_mode = '时长模式';
                        $package = $package_mode . '(' . $order['package_value'] . '天)';
                        if ($order['package_mode'] == 2) {
                            $package_mode = '流量模式';
                            $package = $package_mode . '(' . $order['package_value'] . 'ML)';
                        }
                        //通知数据组装
                        $template_data = [
                            'keyword1' => ['value' => $order['order_no']],
                            'keyword2' => ['value' => $eq_info['equipments_name']],
                            'keyword3' => ['value' => $order['equipment_num']],
                            'keyword4' => ['value' => $package],
                            'keyword5' => ['value' => $order['order_actual_money']],
                            'keyword6' => ['value' => $pay_way],
                            'keyword7' => ['value' => $order_time],
                            'keyword8' => ['value' => '您的订单已经支付成功，感谢您对我们的的支持。若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id'])]
                        ];

                        $template_tel = $user_info['telphone'];
                        $template_content = '您的订单(订单编号:' . $order['order_no'] . ',商品名称:' . $eq_info['equipments_name'] . ',数量:' . $order['equipment_num'] . ')已完成支付，金额为' . $order['order_actual_money'] . '元。打开微信进入' . $this->wx_config['app_wx_name'] . '可查看支付方式、下单时间等支付详情';
                        $template_url = 'pages/user/myOrderDetail/myOrderDetail?order_id=' . $order['order_id'];
                        $weapp_template_keyword = '';
                        $mp_template_data = [
                            'first' => ['value' => '您的订单已经支付成功，感谢您对我们的支持', 'color' => '#4e4747'],
                            'keyword1' => ['value' => $eq_info['equipments_name'], 'color' => '#4e4747'],
                            'keyword2' => ['value' => $order['order_no'], 'color' => '#4e4747'],
                            'keyword3' => ['value' => $order['order_actual_money'], 'color' => '#4e4747'],
                            'remark' => ['value' => '若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']), 'color' => '#173177'],
                        ];
                        NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $order['company_id']);
                    }

                    //通知到后台
                    $admin_notice_config = ConfigService::getTemplateConfig('user_new_order', 'admin', $order['company_id']);
                    if (!empty($admin_notice_config)) {
                        $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
                        //站内
                        if (!empty($admin_notice_config['websocket']) && $admin_notice_config['websocket']['switch']) {
                            $msg['type'] = 1;
                            $msg['msg'] = '有新订单拉';
                            $this->sendToUid($this->uid, $msg);
                            //通知行政中心
                            $msg['type'] = 1;
                            $msg['msg'] = '有新订单拉';
                            if (!empty($a_info)) {
                                $this->sendToUid($this->uid, $msg);
                                $this->sendToUid($this->uid, $msg);
                            }
                        }
                        //仅用户下单且支付的才邮件通知后台
                        if (!empty($admin_notice_config['email']) && $admin_notice_config['email']['switch'] && $order['opertaor'] == 1) {
                            //发邮件通知
                            $opertaor = $this->config->get('order_opertaor')[$order['opertaor']];//订单来源
                            $content = '<h2>新订单通知</h2><br/><div>订单编号:' . $order['order_no'] . '</div><div>订单来源:' . $opertaor . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>商品名称:' . $eq_info['equipments_name'] . '(数量:' . $order['equipment_num'] . ')</div><div>订单金额:' . $order['order_actual_money'] . '元</div><div>下单时间:' . $order_time . '</div><div style="color:#808080">客户已完成支付,请进入管理后台查看详情!</div>';
                            //总后台
                            $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                            if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                                EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                            }
                            if (!empty($a_info)) {
                                //对应运营中心
                                $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                                if (!empty($op_email) && !empty($op_email['notice_email'])) {
                                    EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                                }
                                //对应合伙人
                                $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                                if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                                    EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                                }
                            }
                        }

                    }
                    //通知到大数据平台
                    $big_data_notice_config = ConfigService::getTemplateConfig('user_new_order', 'big_data', $order['company_id']);
                    if (!empty($big_data_notice_config) && !empty($big_data_notice_config['websocket']) && $big_data_notice_config['websocket']['switch']) {
                        $tel = substr($user_info['telphone'], 0, 3) . '****' . substr($user_info['telphone'], 7, 4);
                        DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 1, 'product' => $eq_info['equipments_name'], 'user_tel' => $tel]]);
                    }


                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $WxPay->arrayToXml($return);
                    return $this->response->end($return);
                }
            }

        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }

    //租赁三方支付回调
    public function http_rentAdaPayOrder()
    {
        //验证微信签名
        $postData = $this->http_input->getPost('data');
        $postDataArr = json_decode($postData, 1);
        $post_data_str = json_encode($postData, JSON_UNESCAPED_UNICODE);
        $postSign = $this->http_input->getPost('sign');
        file_put_contents('buy_cloud.txt', $postData);
        if (isset($postDataArr['status']) && $postDataArr['status'] == 'succeeded') {
            $order_sn = $postDataArr['order_no'];
            $order = $this->order_model->getOne(['order_no' => $order_sn], '*');
            if (empty($order)) {
                file_put_contents('pay_call_order_null.txt', date('Ymd His') . ':' . $order_sn . PHP_EOL, FILE_APPEND | LOCK_EX);
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            $adapay_tools = new AdapayTools();
            $sign_flag = $adapay_tools->verifySign($postData, $postSign);
            //验证签名
            if ($sign_flag) {
                $order['wx_callback_num'] = $postDataArr['id'];
                $total_fee = $postDataArr['pay_amt'];
                $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'realname,username,openid,telphone,user_id,source');
                if (!empty($user_info['realname'])) {
                    $user_info['username'] = $user_info['realname'];
                }
                $status = false;
                $this->db->begin(function () use ($order, &$data, &$status, $total_fee, $user_info) {
                    //修改订单状态,如果走发货流程 order_status=2 待发货 反之 order_status=3 待安装
                    $order_status = 3;
                    $deliver_way = 0;
                    if ($order['order_type'] == 2) {
                        $order_status = 2;
                        $deliver_way = 2;
                    }
                    //如果已经是已安装不修改订单状态
                    if ($order['order_status'] == 4) {
                        $order_status = 4;
                    }
                    // pay_way 支付方式1微信支付 2:支付宝 3.百度支付 4对公转账  5现金支付 6聚合支付
                    $this->order_model->save(['order_id' => $order['order_id']],
                        ['order_status' => $order_status, 'pay_status' => 2, 'pay_time' => time(), 'pay_way' => 6,
                            'wx_callback_num' => $order['wx_callback_num'], 'deliver_way' => $deliver_way, 'order_actual_money' => $total_fee]);
                    //添加订单日志
                    $this->addOrderLog($order['order_id'], $order_status, "您的订单已支付成功,我们将尽快进行处理,订单编号:" . $order['order_no'], 1);
                    //添加用户与行政中心关系
                    $this->userService->addUserCenter($order['user_id'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
                    $user_id = $order['user_id'];
                    $payment_method = $order['is_paying_agent'] == 1 ? 4 : 1;
                    //如果是代付且金额大于0,更新代付人账户余额
                    if (!empty($order['paying_agent_deduction_account_balance']) && $order['paying_agent_deduction_account_balance'] > 0) {
                        //扣取账户
                        $this->Achievement->table = 'oa_workers';
                        $workers = $this->Achievement->findData(['user_id' => $order['paying_agent_user_id']], 'cash_available,workers_id');
                        $cash_available = $workers['cash_available'] - $order['paying_agent_deduction_account_balance'];
                        $this->Achievement->updateData(['user_id' => $order['paying_agent_user_id']], ['cash_available' => $cash_available]);
                        //写入资金变动记录
                        $funds_record = [
                            'user_id' => $workers['workers_id'],
                            'user_type' => 1,
                            'source' => 6,
                            'account_type' => 1,
                            'order_id' => $order['order_id'],
                            'money' => $order['paying_agent_deduction_account_balance'],
                            'note' => '订单单号:' . $order['order_no'] . ',代付发起人:' . $user_info['username'] . '(联系电话:' . $user_info['telphone'] . ')',
                            'add_time' => time(),
                        ];
                        $this->Achievement->table = 'funds_record';
                        $this->Achievement->insertData($funds_record);
                        $user_id = $order['paying_agent_user_id'];
                    }
                    //添加物流信息
                    $this->addOrderLogistics($order['order_id'], 1, "您的订单开始处理", $order['user_id']);
                    $equipments = $this->equipment_model->getOne(['equipments_id'=>$order['equipments_id']]);
                    if(!empty($equipments) && $equipments['equipments_line'] == 3){
                        $finance_record['remarks'] = '礼品兑换';
                    }
                    //新增资金记录
                    $finance_record['type'] = 1;
                    $finance_record['order_id'] = $order['order_id'];
                    $finance_record['user_id'] = $order['user_id'];
                    $finance_record['money'] = $total_fee * 100;
                    $finance_record['total_money'] = formatMoney($order['order_total_money'], 1);
                    $finance_record['coupon_money'] = formatMoney($order['coupon_money'], 1);
                    $finance_record['deduction_account_balance'] = formatMoney($order['paying_agent_deduction_account_balance'], 1);
                    $finance_record['create_time'] = time();
                    $finance_record['is_online_pay'] = 1;
                    $finance_record['pay_way'] = 6;
                    $finance_record['callback_num'] = $order['wx_callback_num'];
                    $finance_record['payment_method'] = $payment_method;
                    $finance_record['payment_uid'] = $user_id;
                    $finance_record['o_id'] = $order['operation_id'];
                    $finance_record['a_id'] = $order['administrative_id'];
                    $finance_record['company_id'] = $order['company_id'];
                    // 按照%0.6 千分之6 来计算 手续费
                    $finance_record['service_money'] = serviceMoneyFormat($finance_record['money']);
                    $this->finance_record_model->add($finance_record);

                    $status = true;
                });
                if ($status) {

                    $eq_info = $this->equipment_model->getDetail(array('equipments_id' => $order['equipments_id']), 'equipments_name,model');
                    //通知到用户
                    $user_notice_config = ConfigService::getTemplateConfig('user_new_order', 'user', $order['company_id']);
                    if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                        $template_id = $user_notice_config['template']['wx_template_id'];
                        $mp_template_id = $user_notice_config['template']['mp_template_id'];
                        //数据组装
                        $pay_way = '微信支付';
                        $order_time = date('Y-m-d H:i:s', $order['create_time']);
                        $package_mode = '时长模式';
                        $package = $package_mode . '(' . $order['package_value'] . '天)';
                        if ($order['package_mode'] == 2) {
                            $package_mode = '流量模式';
                            $package = $package_mode . '(' . $order['package_value'] . 'ML)';
                        }
                        //通知数据组装
                        $template_data = [
                            'keyword1' => ['value' => $order['order_no']],
                            'keyword2' => ['value' => $eq_info['equipments_name']],
                            'keyword3' => ['value' => $order['equipment_num']],
                            'keyword4' => ['value' => $package],
                            'keyword5' => ['value' => $order['order_actual_money']],
                            'keyword6' => ['value' => $pay_way],
                            'keyword7' => ['value' => $order_time],
                            'keyword8' => ['value' => '您的订单已经支付成功，感谢您对我们的的支持。若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id'])]
                        ];

                        $template_tel = $user_info['telphone'];
                        $template_content = '您的订单(订单编号:' . $order['order_no'] . ',商品名称:' . $eq_info['equipments_name'] . ',数量:' . $order['equipment_num'] . ')已完成支付，金额为' . $order['order_actual_money'] . '元。打开微信进入' . $this->wx_config['app_wx_name'] . '可查看支付方式、下单时间等支付详情';
                        $template_url = 'pages/user/myOrderDetail/myOrderDetail?order_id=' . $order['order_id'];
                        $weapp_template_keyword = '';
                        $mp_template_data = [
                            'first' => ['value' => '您的订单已经支付成功，感谢您对我们的支持', 'color' => '#4e4747'],
                            'keyword1' => ['value' => $eq_info['equipments_name'], 'color' => '#4e4747'],
                            'keyword2' => ['value' => $order['order_no'], 'color' => '#4e4747'],
                            'keyword3' => ['value' => $order['order_actual_money'], 'color' => '#4e4747'],
                            'remark' => ['value' => '若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']), 'color' => '#173177'],
                        ];
                        NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $order['company_id']);
                    }

                    //通知到后台
                    $admin_notice_config = ConfigService::getTemplateConfig('user_new_order', 'admin', $order['company_id']);
                    if (!empty($admin_notice_config)) {
                        $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
                        //站内
                        if (!empty($admin_notice_config['websocket']) && $admin_notice_config['websocket']['switch']) {
                            $msg['type'] = 1;
                            $msg['msg'] = '有新订单拉';
                            $this->sendToUid($this->uid, $msg);
                            //通知行政中心
                            $msg['type'] = 1;
                            $msg['msg'] = '有新订单拉';
                            if (!empty($a_info)) {
                                $this->sendToUid($this->uid, $msg);
                                $this->sendToUid($this->uid, $msg);
                            }
                        }
                        //仅用户下单且支付的才邮件通知后台
                        if (!empty($admin_notice_config['email']) && $admin_notice_config['email']['switch'] && $order['opertaor'] == 1) {
                            //发邮件通知
                            $opertaor = $this->config->get('order_opertaor')[$order['opertaor']];//订单来源
                            $content = '<h2>新订单通知</h2><br/><div>订单编号:' . $order['order_no'] . '</div><div>订单来源:' . $opertaor . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>商品名称:' . $eq_info['equipments_name'] . '(数量:' . $order['equipment_num'] . ')</div><div>订单金额:' . $order['order_actual_money'] . '元</div><div>下单时间:' . $order_time . '</div><div style="color:#808080">客户已完成支付,请进入管理后台查看详情!</div>';
                            //总后台
                            $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                            if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                                EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                            }
                            if (!empty($a_info)) {
                                //对应运营中心
                                $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                                if (!empty($op_email) && !empty($op_email['notice_email'])) {
                                    EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                                }
                                //对应合伙人
                                $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                                if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                                    EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                                }
                            }
                        }

                    }
                    //通知到大数据平台
                    $big_data_notice_config = ConfigService::getTemplateConfig('user_new_order', 'big_data', $order['company_id']);
                    if (!empty($big_data_notice_config) && !empty($big_data_notice_config['websocket']) && $big_data_notice_config['websocket']['switch']) {
                        $tel = substr($user_info['telphone'], 0, 3) . '****' . substr($user_info['telphone'], 7, 4);
                        DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 1, 'product' => $eq_info['equipments_name'], 'user_tel' => $tel]]);
                    }


                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $this->arrayToXml($return);
                    return $this->response->end($return);
                }
            }

        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $this->arrayToXml($return);
        return $this->response->end($return);
    }

    //租赁支付宝支付回调
    public function http_ali_rentOrder()
    {
        //验证支付宝签名
        $postStr = $this->http_input->getRawContent();
        file_put_contents('ali_pay.txt', $postStr);
        //$postStr = file_get_contents('ali_pay.txt');
        $postStr = urldecode($postStr);
        $postStr = explode('&', $postStr);

        $post = [];
        foreach ($postStr as $k => $v) {
            $str = explode('=', $v);
            $post[$str[0]] = $str[1];
        }
        if (!empty($post)) {
            $alipay = $this->config->get('alipay');
            $aop = new AopClient();
            $aop->gatewayUrl = $alipay['gatewayUrl'];
            $aop->appId = $alipay['appId'];
            $aop->rsaPrivateKey = $alipay['rsaPrivateKey'];
            $aop->alipayrsaPublicKey = $alipay['alipayrsaPublicKey'];
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset = 'UTF-8';
            $aop->format = 'json';
            $result = $aop->rsaCheckV1($post, $aop->alipayrsaPublicKey, $aop->signType);
            //验证签名
            if ($result) {
                $order_sn = $post['out_trade_no'];
                $order = $this->order_model->getOne(['order_no' => $order_sn], '*');
                if (empty($order)) {
                    file_put_contents('pay_call_order_null.txt', date('Ymd His') . ':' . $order_sn . PHP_EOL, FILE_APPEND | LOCK_EX);
                    return $this->response->end('fail');
                }
                if ($order['pay_status'] == 2) {
                    return $this->response->end('SUCCESS');
                }
                $order['ali_callback_num'] = $post['notify_id'];
                $status = false;
                $this->db->begin(function () use ($order, &$data, &$status) {
                    //修改订单状态,如果走发货流程 order_status=2 待发货 反之 order_status=3 待安装
                    $order_status = 3;
                    $deliver_way = 0;
                    if ($order['order_type'] == 2) {
                        $order_status = 2;
                        $deliver_way = 2;
                    }
                    //如果已经是已安装不修改订单状态
                    if ($order['order_status'] == 4) {
                        $order_status = 4;
                    }
                    $this->order_model->save(['order_id' => $order['order_id']], ['order_status' => $order_status, 'pay_status' => 2, 'pay_time' => time(), 'ali_callback_num' => $order['ali_callback_num'], 'wx_callback_num' => $order['ali_callback_num'], 'deliver_way' => $deliver_way]);
                    //添加订单日志
                    $this->addOrderLog($order['order_id'], $order_status, "您的订单已支付成功,我们将尽快进行处理,订单编号:" . $order['order_no'], 1);
                    //添加用户与行政中心关系
                    $this->userService->addUserCenter($order['user_id'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
                    //添加物流信息
                    $this->addOrderLogistics($order['order_id'], 1, "您的订单开始处理", $order['user_id']);
                    //新增资金记录
                    $finance_record['type'] = 1;
                    $finance_record['order_id'] = $order['order_id'];
                    $finance_record['user_id'] = $order['user_id'];
                    $finance_record['money'] = formatMoney($order['order_actual_money'], 1);
                    $finance_record['total_money'] = formatMoney($order['order_total_money'], 1);
                    $finance_record['coupon_money'] = formatMoney($order['coupon_money'], 1);
                    $finance_record['deduction_account_balance'] = formatMoney($order['paying_agent_deduction_account_balance'], 1);
                    $finance_record['create_time'] = time();
                    $finance_record['is_online_pay'] = 1;
                    $finance_record['pay_way'] = 2;
                    $finance_record['callback_num'] = $order['ali_callback_num'];
                    $finance_record['payment_method'] = 1;
                    $finance_record['payment_uid'] = $order['user_id'];
                    $finance_record['o_id'] = $order['operation_id'];
                    $finance_record['a_id'] = $order['administrative_id'];
                    $finance_record['company_id'] = $order['company_id'];
                    // 按照%0.6 千分之6 来计算 手续费
                    $finance_record['service_money'] = serviceMoneyFormat($finance_record['money']);
                    $this->finance_record_model->add($finance_record);


                    $status = true;
                });
                if ($status) {
                    $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'openid,telphone,user_id,source');
                    $eq_info = $this->equipment_model->getDetail(array('equipments_id' => $order['equipments_id']), 'equipments_name,model');
                    if (empty($user_info) || empty($eq_info)) {
                        return $this->response->end('fail');
                    }
                    $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);

                    //发邮件通知
                    if (1 == 0) {//暂时关闭
                        $order_time = date('Y-m-d H:i:s', $order['create_time']);
                        $opertaor = $this->config->get('order_opertaor')[$order['opertaor']];//订单来源
                        $content = '<h2>新订单通知</h2><br/><div>订单编号:' . $order['order_no'] . '</div><div>订单来源:' . $opertaor . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>商品名称:' . $eq_info['equipments_name'] . '(数量:' . $order['equipment_num'] . ')</div><div>订单金额:' . $order['order_actual_money'] . '元</div><div>下单时间:' . $order_time . '</div><div style="color:#808080">客户已完成支付,请进入管理后台查看详情!</div>';
                        //总后台
                        $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                        if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                            EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                        }
                        if (!empty($a_info)) {
                            //对应运营中心
                            $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                            if (!empty($op_email) && !empty($op_email['notice_email'])) {
                                EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                            }
                            //对应合伙人
                            $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                            if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                                EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                            }
                        }
                    }

                    return $this->response->end('SUCCESS');
                }
            }
        }
        return $this->response->end('fail');
    }

    //百度支付回调
    public function http_baiduRentOrder()
    {
        $this->http_output->setHeader('Content-type: text/html', 'charset=utf-8');
        require_once APP_DIR . '/Library/baiduWeapp/nuomi-openplatform-php-sdk-1.0.3/Autoloader.php';
        // 接收参数 -- 转换参数
        $postStr = file_get_contents('php://input');
        file_put_contents('baidu_notify.txt', $postStr);
        //$data = file_get_contents('./baidu_notify.txt');
        // $postStr = $data;
        $postStr = urldecode($postStr);
        $postStr = explode('&', $postStr);
        $post = [];
        foreach ($postStr as $k => $v) {
            $str = explode('=', $v);
            $post[$str[0]] = $str[1];
        }
        $rsaPublicKeyFilePath = APP_DIR . '/Library/baiduWeapp/rsa/rsa_public_key.pem';
        if (!file_exists($rsaPublicKeyFilePath) || !is_readable($rsaPublicKeyFilePath)) {
            return false;
        }
        $rsaPublicKeyStr = file_get_contents($rsaPublicKeyFilePath);
        $order_sn = $post['tpOrderId'];
        $order = $this->order_model->getOne(['order_no' => $order_sn], '*');
        if (empty($order)) {
            file_put_contents('pay_call_order_null.txt', date('Ymd His') . ':' . $order_sn . PHP_EOL, FILE_APPEND | LOCK_EX);
            return $this->response->end('fail');
        }
        if ($order['pay_status'] == 2) {
            return $this->response->end('SUCCESS');
        }
        $order['ali_callback_num'] = $post['notify_id'];
        $status = false;
        //验签暂未通过
        $checkSignRes = \NuomiRsaSign::checkSignWithRsa($post, $rsaPublicKeyStr);
        $order_sn = $post['tpOrderId'];
        $order = $this->order_model->getOne(['order_no' => $order_sn], '*');
        if (empty($order)) {
            file_put_contents('pay_call_order_null.txt', date('Ymd His') . ':' . $order_sn . PHP_EOL, FILE_APPEND | LOCK_EX);
            return json_encode(['errno' => 1, 'msg' => 'error', 'data' => ['isErrorOrder' => 1, 'isConsumed' => 2]]);
        }
        if ($order['pay_status'] == 2) {
            return json_encode(['errno' => 0, 'msg' => 'success', 'data' => []]);
        }
        //如果订单金额与实际支付金额不一致
//        if(intval(round($order['order_actual_money']*100))!=$post['payMoney']){
//            return json_encode(['errno'=>1,'msg'=>'error','data'=>['isErrorOrder'=>1,'isConsumed'=>2]]);
//        }
        $order['wx_callback_num'] = $post['dealId'];
        $status = false;
        $this->db->begin(function () use ($order, &$data, &$status) {
            //修改订单状态,如果走发货流程 order_status=2 待发货 反之 order_status=3 待安装
            $order_status = 3;
            $deliver_way = 0;
            if ($order['order_type'] == 2) {
                $order_status = 2;
                $deliver_way = 2;
            }
            //如果已经是已安装不修改订单状态
            if ($order['order_status'] == 4) {
                $order_status = 4;
            }
            $this->order_model->save(['order_id' => $order['order_id']], ['order_status' => $order_status, 'pay_status' => 2, 'pay_time' => time(), 'wx_callback_num' => $order['wx_callback_num'], 'deliver_way' => $deliver_way]);
            //添加订单日志
            $this->addOrderLog($order['order_id'], $order_status, "您的订单已支付成功,我们将尽快进行处理,订单编号:" . $order['order_no'], 1);
            //添加用户与行政中心关系
            $this->userService->addUserCenter($order['user_id'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
            //添加物流信息
            $this->addOrderLogistics($order['order_id'], 1, "您的订单开始处理", $order['user_id']);
            //新增资金记录
            $finance_record['type'] = 1;
            $finance_record['order_id'] = $order['order_id'];
            $finance_record['user_id'] = $order['user_id'];
            $finance_record['money'] = formatMoney($order['order_actual_money'], 1);
            $finance_record['total_money'] = formatMoney($order['order_total_money'], 1);
            $finance_record['coupon_money'] = formatMoney($order['coupon_money'], 1);
            $finance_record['deduction_account_balance'] = formatMoney($order['paying_agent_deduction_account_balance'], 1);
            $finance_record['create_time'] = time();
            $finance_record['is_online_pay'] = 1;
            $finance_record['pay_way'] = 2;
            $finance_record['callback_num'] = '';
            $finance_record['payment_method'] = 1;
            $finance_record['payment_uid'] = $order['user_id'];
            $finance_record['o_id'] = $order['operation_id'];
            $finance_record['a_id'] = $order['administrative_id'];
            $finance_record['company_id'] = $order['company_id'];
            // 按照%0.6 千分之6 来计算 手续费
            $finance_record['service_money'] = serviceMoneyFormat($finance_record['money']);
            $this->finance_record_model->add($finance_record);
        }, function ($e) {
            return json_encode(['errno' => 1, 'msg' => 'error', 'data' => []]);
        });
        if ($status) {
            $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'openid,telphone,user_id,source');
            $eq_info = $this->equipment_model->getDetail(array('equipments_id' => $order['equipments_id']), 'equipments_name,model');
            if (empty($user_info) || empty($eq_info)) {
                return json_encode(['errno' => 1, 'msg' => 'error', 'data' => []]);
            }
            $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
            //发邮件通知
            if (1 == 0) {
                $order_time = date('Y-m-d H:i:s', $order['create_time']);
                $opertaor = $this->config->get('order_opertaor')[$order['opertaor']];//订单来源
                $content = '<h2>新订单通知</h2><br/><div>订单编号:' . $order['order_no'] . '</div><div>订单来源:' . $opertaor . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>商品名称:' . $eq_info['equipments_name'] . '(数量:' . $order['equipment_num'] . ')</div><div>订单金额:' . $order['order_actual_money'] . '元</div><div>下单时间:' . $order_time . '</div><div style="color:#808080">客户已完成支付,请进入管理后台查看详情!</div>';
                //总后台
                $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                    EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                }
                if (!empty($a_info)) {
                    //对应运营中心
                    $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                    if (!empty($op_email) && !empty($op_email['notice_email'])) {
                        EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                    }
                    //对应合伙人
                    $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                    if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                        EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                    }
                }
            }

            return json_encode(['errno' => 0, 'msg' => 'success', 'data' => []]);
        }
        return json_encode(['errno' => 1, 'msg' => 'error', 'data' => []]);

    }

    //百度退款审核
    public function http_baiduRefundCheck()
    {

    }

    //百度退款回调
    public function http_baiduRefundCallback()
    {


    }

    // 聚合支付，续费回调
    public function http_rentAdaPayRenewOrder()
    {
        $postData = $this->http_input->getPost('data');
        $postDataArr = json_decode($postData, 1);
        $post_data_str = json_encode($postData, JSON_UNESCAPED_UNICODE);
        $postSign = $this->http_input->getPost('sign');
        file_put_contents('buy_cloud.txt', $postData);
        if (isset($postDataArr['status']) && $postDataArr['status'] == 'succeeded') {
            //判断锁
            $rentRenewOrderLock = $this->redis->hGet('rentRenewOrderLock', $postDataArr['order_no']);
            if (!empty($rentRenewOrderLock)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            $order_sn = $postDataArr['order_no'];
            $this->renewOrderModel->edit(['renew_order_no' => $order_sn], ['order_actual_money' => $postDataArr['pay_amt']]);
            $join = [
                ['contract c', 'c.contract_id=rq_renew_order.contract_id', 'left']
            ];
            $order = $this->renewOrderModel->getOne(['renew_order_no' => $order_sn], 'rq_renew_order.*,c.company_id, c.contract_equipments_line', $join);
            if (empty($order)) {
                file_put_contents('pay_call_order_null.txt', date('Ymd His') . ':' . $order_sn . PHP_EOL, FILE_APPEND | LOCK_EX);
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            $adapay_tools = new AdapayTools();
            $sign_flag = $adapay_tools->verifySign($postData, $postSign);

            //验证签名
            if ($sign_flag) {
                $order['wx_callback_num'] = $postDataArr['id'];
                $total_fee = $postDataArr['pay_amt'] * 100;
                // 获取用户信息
                $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,openid,telphone');
                // 获取合同信息
                $contract_info = $this->contract_model->getOne(['contract_id' => $order['contract_id']]);
                $status = false;
                $exire_date = $contract_info['exire_date'];
                $this->db->begin(function () use ($order, &$status, $postDataArr, $post_data_str, $contract_info, &$exire_date, $user_info, $total_fee) {
                    $this->redis->hSet('rentRenewOrderLock', $postDataArr['order_no'], json_encode($post_data_str));//加锁
                    // pay_way 支付方式，1：微信支付，2：线下支付，6：聚合支付
                    $this->renew($order, $contract_info, $user_info, $total_fee, 6);
                    $status = true;
                });
                $this->redis->hDel('rentRenewOrderLock', $postDataArr['order_no']);//解锁
                if ($status) {
                    //开始结算和发送模板消息
                    $this->sendRenewMsg($order, $contract_info, $user_info, $exire_date);
                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $this->arrayToXml($return);
                    return $this->response->end($return);
                }
            }
        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $this->arrayToXml($return);
        return $this->response->end($return);
    }

    // 聚合支付，续费回调
    public function http_rentAdaPayRenewOrderApifox()
    {
        $postData = $this->http_input->getPost('data');
        $postData = json_encode($postData);
        $postDataArr = json_decode($postData, 1);
        $post_data_str = json_encode($postData, JSON_UNESCAPED_UNICODE);
        $postSign = $this->http_input->getPost('sign');
        file_put_contents('buy_cloud.txt', $postData);
        if (isset($postDataArr['status']) && $postDataArr['status'] == 'succeeded') {
            //判断锁
            $rentRenewOrderLock = $this->redis->hGet('rentRenewOrderLock', $postDataArr['order_no']);
            if (!empty($rentRenewOrderLock)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            $order_sn = $postDataArr['order_no'];
            $this->renewOrderModel->edit(['renew_order_no' => $order_sn], ['order_actual_money' => $postDataArr['pay_amt']]);
            $join = [
                ['contract c', 'c.contract_id=rq_renew_order.contract_id', 'left']
            ];
            $order = $this->renewOrderModel->getOne(['renew_order_no' => $order_sn], 'rq_renew_order.*,c.company_id, c.contract_equipments_line', $join);
            if (empty($order)) {
                file_put_contents('pay_call_order_null.txt', date('Ymd His') . ':' . $order_sn . PHP_EOL, FILE_APPEND | LOCK_EX);
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            $adapay_tools = new AdapayTools();
//            $sign_flag = $adapay_tools->verifySign($postData, $postSign);

            //验证签名
//            if ($sign_flag) {
                $order['wx_callback_num'] = $postDataArr['id'];
                $total_fee = $postDataArr['pay_amt'] * 100;
                // 获取用户信息
                $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,openid,telphone');
                // 获取合同信息
                $contract_info = $this->contract_model->getOne(['contract_id' => $order['contract_id']]);
                $status = false;
                $exire_date = $contract_info['exire_date'];
                $this->db->begin(function () use ($order, &$status, $postDataArr, $post_data_str, $contract_info, &$exire_date, $user_info, $total_fee) {
                    $this->redis->hSet('rentRenewOrderLock', $postDataArr['order_no'], json_encode($post_data_str));//加锁
                    // pay_way 支付方式，1：微信支付，2：线下支付，6：聚合支付
                    $this->renew($order, $contract_info, $user_info, $total_fee, 6);
                    $status = true;
                });
                $this->redis->hDel('rentRenewOrderLock', $postDataArr['order_no']);//解锁
                if ($status) {
                    //开始结算和发送模板消息
                    $this->sendRenewMsg($order, $contract_info, $user_info, $exire_date);
                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $this->arrayToXml($return);
                    return $this->response->end($return);
                }
//            }
        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $this->arrayToXml($return);
        return $this->response->end($return);
    }

    //续费支付回调
    public function http_rentRenewOrder()
    {
        //验证微信签名
        $postStr = $this->http_input->getRawContent();
        file_put_contents('renew_order.txt', $postStr);
        //$postStr = file_get_contents('renew_order.txt');

        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($postObj->result_code == 'SUCCESS' && $postObj->return_code == 'SUCCESS') {
            //判断锁
            $rentRenewOrderLock = $this->redis->hGet('rentRenewOrderLock', $postObj->out_trade_no);
            if (!empty($rentRenewOrderLock)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            $order_sn = $postObj->out_trade_no;
            $join = [
                ['contract c', 'c.contract_id=rq_renew_order.contract_id', 'left']
            ];
            $order = $this->renewOrderModel->getOne(['renew_order_no' => $order_sn], 'rq_renew_order.*,c.company_id,c.contract_equipments_line', $join);
            if (empty($order)) {
                file_put_contents('pay_call_order_null.txt', date('Ymd His') . ':' . $order_sn . PHP_EOL, FILE_APPEND | LOCK_EX);
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_mode'] == 2) {
                $wx_config = $this->service_payment_config;
            } else {
                $company_config = CompanyService::getCompanyConfig($order['company_id']);
                $wx_config = $company_config['wx_config'];
            }
            $wx_config['payment_type'] = 'JSAPI';
            //$WxPay = new WxPay($wx_config);
            $WxPay = WxPay::getInstance($wx_config);
            if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);
            $signObj = [];
            foreach ($postObj as $key => $val) {
                if ($key != 'sign') {
                    $signObj[$key] = $val;
                }
            }

            //验证签名
            if ($WxPay->getSign($signObj) == $postObj->sign) {
                $order['wx_callback_num'] = $postObj->transaction_id;
                $total_fee = $postObj->total_fee;

                // 获取用户信息
                $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,openid,telphone');
                // 获取合同信息
                $contract_info = $this->contract_model->getOne(['contract_id' => $order['contract_id']]);
                $status = false;
                $exire_date = $contract_info['exire_date'];
                $this->db->begin(function () use ($order, &$status, $postObj, $postStr, $contract_info, &$exire_date, $user_info, $total_fee) {
                    $this->redis->hSet('rentRenewOrderLock', $postObj->out_trade_no, json_encode($postStr));//加锁
                    $this->renew($order, $contract_info, $user_info, $total_fee);
                    $status = true;
                });
                $this->redis->hDel('rentRenewOrderLock', $postObj->out_trade_no);//解锁
                if ($status) {
                    //开始结算和发送模板消息
                    $this->sendRenewMsg($order, $contract_info, $user_info, $exire_date);
                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $WxPay->arrayToXml($return);
                    return $this->response->end($return);
                }
            }
        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }

    /**
     * @desc   续费金额为0时回调--已废弃,移到model
     * @param order_sn string 必选 订单编号
     * @param order_money string 必选 金额
     * @param openid string 必选 工程openid
     * @date   2020-6-19
     * @return [type]     [description]
     * @author lcx
     */
    public function http_renewCallback()
    {
        //接收参数
        $param = json_decode($this->http_input->getRawContent(), true);
        $order_sn = $param['order_sn'];
        $total_fee = $param['order_money'];
        $openid = $param['openid'] ?? '';
        $join = [
            ['contract c', 'c.contract_id=rq_renew_order.contract_id', 'left']
        ];
        //判断锁
        $renewCallbackLock = $this->redis->hGet('renewCallbackLock', $order_sn);
        if (!empty($renewCallbackLock)) {
            return false;
        }
        //获取续费订单信息
        $order = $this->renewOrderModel->getOne(['renew_order_no' => $order_sn], 'rq_renew_order.*,c.company_id,c.contract_equipments_line', $join);

        //获取用户信息
        $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,openid,telphone');
        // 获取合同信息
        $contract_info = $this->contract_model->getOne(['contract_id' => $order['contract_id']]);
        $status = false;
        $exire_date = $contract_info['exire_date'];

        //  $this->db->begin(function () use ($order, &$status, $contract_info, &$exire_date, $user_info, $total_fee, $order_sn) {
        $this->redis->hSet('renewCallbackLock', $order_sn, json_encode($order));//加锁
        $this->renew($order, $contract_info, $user_info, $total_fee);
        $status = true;
        //   });
        $this->redis->hDel('renewCallbackLock', $order_sn);//解锁
        if ($status) {
            //开始续费结算和发送模板消息
            $this->sendRenewMsg($order, $contract_info, $user_info, $exire_date);
            return true;
        }
        return false;

    }

    //续费公共处理方法
    public function renew(array $order, array $contract_info, array $user_info, $total_fee, $pay_way = 1)
    {
        // 1-- 修改订单状态
        $this->renewOrderModel->edit(['renew_order_id' => $order['renew_order_id']],
            ['status' => 3, 'pay_status' => 2, 'pay_time' => time(), 'renew_time' => time(), 'wx_callback_num' => $order['wx_callback_num'],
                'pay_way' => $pay_way]); // 支付方式，1：微信支付，2：线下支付，6：聚合支付
        // 2-- 修改合同信息
        $exire_date = ($order['is_buy_out'] == 1) ? strtotime("+20 year", $contract_info['exire_date']) : strtotime("+1 year", $contract_info['exire_date']);  // 到期时间延后一年

        $contract['exire_date'] = $exire_date;
        $contract['real_exire_date'] = $exire_date;
        if ($exire_date > time()) {
            $contract['status'] = 4;
        }
        $this->contract_model->save(['contract_id' => $order['contract_id']], $contract);
        //添加合同日志
        $this->addContractLog($order['contract_id'], 3, "【用户端】用户【" . $user_info['username'] . "】【用户ID:" . $order['user_id'] . "】续费成功,修改合同到期时间为" . date('Y-m-d H:i:s', $exire_date), $order['user_id']);
        //查询合同设备信息
        $contract_ep_info = $this->contract_ep_model->getAll(array('contract_id' => $order['contract_id']), 'equipment_id');
        if (empty($contract_ep_info)) {
            //throw new SwooleException("该合同下无正常运行的设备,无法续费");
            return false;
        }
        //添加续费-设备关联表
        foreach ($contract_ep_info as $key => $value) {
            $eq_data = [];
            $eq_data['renew_order_id'] = $order['renew_order_id'];
            $eq_data['equipment_id'] = $value['equipment_id'];
            $eq_data['create_time'] = time();
            $this->renew_eq_model->add($eq_data);
        }
        //添加续费记录
        $record = [];
        $record['renew_order_id'] = $order['renew_order_id'];
        $record['user_id'] = $order['user_id'];
        $record['contract_id'] = $order['contract_id'];
        $record['remarks'] = '用户合同续费，微信支付';
        $record['renew_record_time'] = time();
        $this->renew_record_model->add($record);

        //优惠券
        if (!empty($order['coupon_id'])) {
            $coupon_id = explode(',', $order['coupon_id']);
            $update_coupon = [
                'use_time' => time(),
                'status' => 2,
                'order_total_sn' => $order['renew_order_no'],
                'use_type' => 2,
                'order_id' => $order['renew_order_id'],
            ];
            $this->Achievement->table = 'coupon';
            $this->Achievement->updateData(['coupon_id' => ['IN', $coupon_id]], $update_coupon);

            $tmp = [
                'coupon_id' => '',
                'use_type' => 1,
                'note' => '用户合同续费[编号：' . $contract_info['contract_no'] . ']',
                'add_time' => time(),
            ];
            $this->Achievement->table = 'coupon_log';
            foreach ($coupon_id as $key => $value) {
                $tmp['coupon_id'] = $value;
                $this->Achievement->insertData($tmp);
            }
        }
        //扣取账户余额
        if (!empty($order['deduction_account_balance']) && $order['deduction_account_balance'] > 0) {
            //扣取账户
            $this->Achievement->table = 'oa_workers';
            $workers = $this->Achievement->findData(['user_id' => $order['user_id']], 'cash_available,workers_id');
            $cash_available = $workers['cash_available'] - $order['deduction_account_balance'];
            $this->Achievement->updateData(['user_id' => $order['user_id']], ['cash_available' => $cash_available]);
            //写入资金变动记录
            $funds_record = [
                'user_id' => $workers['workers_id'],
                'user_type' => 1,
                'source' => 5,
                'account_type' => 1,
                'order_id' => $order['renew_order_id'],
                'money' => $order['deduction_account_balance'],
                'note' => '续费单号:' . $order['renew_order_no'],
                'add_time' => time(),
            ];
            $this->Achievement->table = 'funds_record';
            $this->Achievement->insertData($funds_record);
        }
        $payment_method = $order['is_paying_agent'] == 1 ? 4 : 1;
        $user_id = $order['user_id'];
        //代付账户抵扣-扣取账户余额
        if (!empty($order['paying_agent_deduction_account_balance']) && $order['paying_agent_deduction_account_balance'] > 0) {
            if ($order['paying_agent_role'] == 2) {
                //扣取账户
                $this->Achievement->table = 'oa_workers';
                $workers = $this->Achievement->findData(['user_id' => $order['paying_agent_user_id']], 'cash_available,workers_id');
                $cash_available = $workers['cash_available'] - $order['paying_agent_deduction_account_balance'];
                $this->Achievement->updateData(['user_id' => $order['paying_agent_user_id']], ['cash_available' => $cash_available]);
                //写入资金变动记录
                $funds_record = [
                    'user_id' => $workers['workers_id'],
                    'user_type' => 1,
                    'source' => 7,
                    'account_type' => 1,
                    'order_id' => $order['renew_order_id'],
                    'money' => $order['paying_agent_deduction_account_balance'],
                    'note' => '续费单号：' . $order['renew_order_no'] . ',代付发起人:' . $user_info['username'] . '(联系电话:' . $user_info['telphone'] . ')',
                    'add_time' => time(),
                ];
                $this->Achievement->table = 'funds_record';
                $this->Achievement->insertData($funds_record);
            }
            $user_id = $order['paying_agent_user_id'];

        }

//                    $payServiceFeeResult = HttpService::requestBossApi(['company_id' => $contract_info['company_id'], 'equipment_id' => array_column($contract_ep_info, 'equipment_id'), 'type' => 2, 'user_id' => $order['user_id'], 'contract_id' => $order['contract_id']], '/api/Equipment/multPayServiceFee');
//                    if ($payServiceFeeResult['code'] != 1000) {//扣费失败,直接抛出异常
//                        throw new SwooleException("扣费失败" . $payServiceFeeResult['msg']);
//                    }
        //续费设备--下发套餐--数据同步指令
        $renew_eq_map['renew_order_id'] = $order['renew_order_id'];
        $renew_eq_join = [
            ['equipment_lists as el', 'rq_renew_order_equipment.equipment_id = el.equipment_id', 'LEFT'],
            ['equipments as e', 'e.equipments_id = el.equipments_id', 'LEFT'],
        ];
        $renew_eq_data = $this->renew_eq_model->getAll($renew_eq_map, 'el.equipment_id,el.device_no,e.type,el.used_days,el.used_traffic,el.remaining_days,el.remaining_traffic', $renew_eq_join);
        if (!empty($renew_eq_data)) {
            foreach ($renew_eq_data as $k => $v) {
                //修改设备表租赁时间
                $this->equipmentListsModel->updateEquipmentLists(array('end_time' => $contract['exire_date']), array('equipment_id' => $v['equipment_id']));
                if ($v['type'] == 1) {//如果是智能设备
                    $exire_date = strtotime(getMonthLastDay($contract['exire_date']));   //获取签约月份最后一天
                    $data_sync_params['sn'] = $v['device_no'];
                    $data_sync_params['used_days'] = $v['used_days'];
                    $data_sync_params['used_traffic'] = $v['used_traffic'];
                    $data_sync_params['remaining_days'] = $v['remaining_days'];
                    $data_sync_params['remaining_traffic'] = $v['remaining_traffic'];
                    if ($order['package_mode'] == 2) {
                        $data_sync_params['remaining_traffic'] = $order['package_value'] + $v['remaining_traffic'];
                    } else if ($order['package_mode'] == 1) {
                        $data_sync_params['remaining_days'] = ceil(($exire_date - time()) / 86400);
                    }
                    $data_sync_path = '/House/Issue/data_sync';
                    HttpService::Thrash($data_sync_params, $data_sync_path);
                    sleepCoroutine(2000);
                    //请求心跳
                    $state_params['sn'] = $v['device_no'];
                    $state_path = '/House/Issue/heartbeat';
                    HttpService::Thrash($state_params, $state_path);
                } else {
                    continue;
                }
            }
        }
        //新增资金记录
        $finance_record['type'] = 3;
        $finance_record['renew_order_id'] = $order['renew_order_id'];
        $finance_record['user_id'] = $order['user_id'];
        $finance_record['money'] = $total_fee;
        $finance_record['total_money'] = formatMoney($order['order_total_money'], 1);
        $finance_record['coupon_money'] = formatMoney($order['coupon_total_money'], 1);
        $finance_record['deduction_account_balance'] = formatMoney($order['deduction_account_balance'], 1) + formatMoney($order['paying_agent_deduction_account_balance'], 1);
        $finance_record['create_time'] = time();
        $finance_record['is_online_pay'] = 1;
        $finance_record['pay_way'] = $pay_way; // 支付方式 1微信支付 2 支付宝支付 3现金支付 4对公转账 5百度支付 6聚合支付
        $finance_record['callback_num'] = $order['wx_callback_num'];
        $finance_record['payment_method'] = $payment_method;
        $finance_record['payment_uid'] = $user_id;
        $finance_record['o_id'] = $contract_info['operation_id'];
        $finance_record['a_id'] = $contract_info['administrative_id'];
        $finance_record['company_id'] = $contract_info['company_id'];
        // 按照%0.6 千分之6 来计算 手续费
        $finance_record['service_money'] = serviceMoneyFormat($finance_record['money']);
        $this->finance_record_model->add($finance_record);

        # TODO 续费买断 合同产生的优惠券划给总账户
        if($contract_info['is_buy_out'] == 1){
            (new WorkOrderService())->couponToAdmin($contract_info['contract_id'],'支付回调-续费买断');
        }

    }

    /**
     * 提现回调
     */
    public function http_adapayWithdrawCallback()
    {
        $postData = $this->http_input->getPost('data');
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'adapay_withdraw_callback_log', $postData . PHP_EOL, true);
        $postDataArr = json_decode($postData, true);
        $this->Achievement->table = 'withdraw';
        if ($postDataArr['status'] == 'succeeded') {
            $update['play_money_state'] = 3;
            $update['cash_order_state'] = 2;
            $update['complete_time'] = time();
            $update['examine_pass'] = time();
            $update['play_time'] = time();
            $update['error_msg'] = '';
        } else {
            $update['error_msg'] = $postDataArr['error_msg'];
            $update['cash_order_state'] = 1;
            $update['play_money_state'] = 1;
        }
        $this->Achievement->updateData(['cash_order_number' => $postDataArr['order_no']], $update);
    }

    //结算和发送模板消息
    public function sendRenewMsg(array $order, array $contract_info, array $user_info, $exire_date)
    {
        if ($order['contract_equipments_line'] == 1) {
            //开始续费结算
            $this->RenewalSettlement($contract_info['contract_no'], $order['renew_order_id']);
        }
        // 模板信息
        $exire_date = date('Y-m-d H:i:s', $exire_date);
        $now = date('Y-m-d H:i:s', $order['create_time']);
        $package_mode = '时长模式';
        $package = $package_mode . '(' . $order['package_value'] . '天)';
        if ($order['package_mode'] == 2) {
            $package_mode = '流量模式';
            $package = $package_mode . '(' . $order['package_value'] . 'ML)';
        }
        $user_notice_config = ConfigService::getTemplateConfig('renew_success', 'user', $order['company_id']);
        if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
            $template_id = $user_notice_config['template']['wx_template_id'];
            $mp_template_id = $user_notice_config['template']['mp_template_id'];
            // 模板内容
            $template_data = [
                'keyword1' => ['value' => $order['renew_order_no']], //订单号
                'keyword2' => ['value' => $order['package_cycle']], // 续费时长
                'keyword3' => ['value' => $package], // 续费类型
                'keyword4' => ['value' => $exire_date], //到期时间
                'keyword5' => ['value' => $now], //续费时间
                'keyword6' => ['value' => $order['order_actual_money']], // 金额
                'keyword7' => ['value' => '您的续费已完成。感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾,合同编号:' . $contract_info['contract_no']], // 备注
            ];
            $weapp_template_keyword = '';
            $mp_template_data = [
                'first' => ['value' => '恭喜!您已成功续费', 'color' => '#4e4747'],
                'keyword1' => ['value' => "合同续费(合同编号:" . $contract_info['contract_no'] . ")", 'color' => '#4e4747'],
                'keyword2' => ['value' => $exire_date, 'color' => '#4e4747'],
                'remark' => ['value' => '感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾!若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']), 'color' => '#173177'],
            ];
            $template_tel = $user_info['telphone'];  // 电话
            $template_content = '您的续费的' . $package . '已成功，续费时长为' . $order['package_cycle'] . '，续费金额为' . $order['order_actual_money'] . '。打开微信进入小程序可查看详情,如有疑问请联系客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']);
            NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, '', $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $order['company_id']);
        }

    }


    /**
     * 续费结算
     * @param string $contract_no
     * @date 2019/1/25 10:41
     * @author ligang
     */
    public function RenewalSettlement(string $contract_no, $renew_order_id)
    {
        if (!$this->config['system_auto_settle']) {
            return true;//已关闭自动结算直接返回
        }
        $data = [
            'contract_no' => $contract_no,
            'type' => 2,
            'from_renew_order_id' => $renew_order_id
        ];
        $json = json_encode($data);
        $response = $this->GetIPAddressHttpClient->httpClient
            ->setData($json)
            ->setMethod('post')
            ->coroutineExecute('/Achievement/Settlement/balance');
        $is_success = true;
        if ($response['statusCode'] == 200) {
            $body = json_decode($response['body'], 1);
            if ($body['code'] != 1000) {
                //结算失败
                $is_success = false;
            }
        } else {
            $is_success = false;
        }
        if (!$is_success) {
            //失败写入日志
            SLog::SL(SLog::CONTRACT_LOG, $contract_no)->info(__FUNCTION__, [$contract_no, $response]);
        }
    }


    /**
     * @desc   添加订单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLog($order_id, $status, $remark, $is_to_user = 0)
    {
        $data['operater_role'] = 1;
        $data['order_id'] = $order_id;
        $data['status'] = $status;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['is_to_user'] = $is_to_user;
        $res = $this->log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加设备日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addEquipmentLog($user_id, $equipment_id, $type, $remark)
    {
        $data['user_id'] = $user_id;
        $data['equipment_id'] = $equipment_id;
        $data['type'] = $type;
        $data['user_role'] = 1;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $res = $this->eq_log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加合同操作日志
     * @param $status 1增2删3改4解绑'
     * @date   2018-07-19
     * @author lcx
     * @return [type]     [description]
     */
    public function addContractLog($contract_id, $status = 1, $remark, $user_id)
    {
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $this->contract_log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加订单物流信息
     * @param $status
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLogistics($order_id, $status_code, $remark, $operator_uid)
    {
        $data['order_id'] = $order_id;
        $data['status_code'] = $status_code;
        $data['status_name'] = $this->config->get('order_logistics_code')[$status_code];
        $data['remark'] = $remark;
        $data['create_time'] = time();
        $data['operator_role'] = 3;
        $data['operator_uid'] = $operator_uid;
        $res = $this->order_logistics_model->add($data);
        //修改订单物流状态
        $this->order_model->save(array('order_id' => $order_id), array('logistics_status' => $status_code));
        return $res;
    }

    /**
     * 对象 转 数组
     * @param object $obj 对象
     * @return array
     */
    protected function object_to_array($obj)
    {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)object_to_array($v);
            }
        }

        return $obj;
    }

    function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";

        return $xml;
    }


}
