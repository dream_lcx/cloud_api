<?php

namespace app\Controllers\User;

use app\Library\SLog;
use app\Services\Common\VaildateService;
use app\Services\Company\CompanyService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use Server\CoreBase\Controller;
use Monolog\Logger;
use Server\SwooleMarco;
use app\Wechat\WxPay;
use app\Services\Common\ConfigService;
use Throwable;
use Exception;
use Server\CoreBase\SwooleRedirectException;
use Server\CoreBase\SwooleException;
use app\Services\Common\ReturnCodeService;

/*
 * 基础类
 */

class Base extends Controller
{

    protected $parm;
    protected $user_id;
    protected $user_info;
    protected $log_model;
    protected $addr_model;
    protected $equip_area_model;
    protected $area_model;
    protected $admin_info_model;
    protected $time_model;
    protected $contract_log_model;
    protected $work_log_model;
    protected $user_model;
    protected $user_log_model;
    protected $GetIPAddressHttpClient;
    protected $dev_mode;
    protected $app_debug;
    protected $wx_config;
    protected $pay_config;
    protected $alipay;
    protected $max_bind_number = 10;
    protected $engineer_wx_name;
    protected $user_wx_name;
    protected $BossApiClient;
    protected $baidu;
    protected $baidu_weapp_config;
    protected $host;
    protected $operation_id;
    protected $customer_administrative_center_model;

    public function initialization($controller_name, $method_name)
    {
        $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
        $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token,company");
        $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        parent::initialization($controller_name, $method_name);
        $this->user_id = $this->request->user_id;
        $this->operation_id = 0;//运营中心ID
        $this->company = $this->request->company;
//        $this->host = $this->http_input->getAllHeader()['host'];//主机域名 yh.cqthesis.cn
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        $this->parm = VaildateService::validateParam($this->parm);//参数验证
        $this->log_model = $this->loader->model('OrderLogModel', $this);
        $this->addr_model = $this->loader->model('UseraddressModel', $this);
        $this->equip_area_model = $this->loader->model('EquipmentsAdministrativeModel', $this);
        $this->admin_info_model = $this->loader->model('AdministrativeInfoModel', $this);
        $this->area_model = $this->loader->model('AreaModel', $this);
        $this->time_model = $this->loader->model('WorkOrderTimeModel', $this);
        $this->contract_log_model = $this->loader->model('ContractLogModel', $this);
        $this->work_log_model = $this->loader->model('WorkOrderLogModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->customer_administrative_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->user_log_model = $this->loader->model('CustomerLogModel', $this);
        //判断用户状态是否正常
        if (!empty($this->user_id)) {
            $userinfo = $this->user_model->getOne(array('user_id' => $this->user_id), 'is_delete,account_status,openid,user_belong_to_company');
            if (!empty($userinfo) && ($userinfo['is_delete'] == 1 || $userinfo['account_status'] == 2)) {
                return $this->jsonend(-1006, "该账号已被冻结,更多信息请联系客服");
            }
            if ($this->request->client == 1 && !empty($userinfo) && empty($userinfo['openid'])) {
                return $this->jsonend(-1004, "登录已失效,请重新登录");
            }
            $customer_administrative_center = $this->customer_administrative_center_model->getOne(['user_id' => $this->user_id], 'operation_id');
            $this->operation_id = empty($customer_administrative_center) ? 0 : $customer_administrative_center['operation_id'];
        }
        //开发者模式
        $this->dev_mode = ConfigService::getConfig('develop_mode', false, $this->company);
        //调试模式
        $this->app_debug = ConfigService::getConfig('app_debug', false, $this->company);
        $company_config = CompanyService::getCompanyConfig($this->company);
        $this->wx_config = $company_config['wx_config'];
        $this->pay_config = $company_config['pay_config'];//支付配置
        //获取微信配置
        $this->baidu_weapp_config = $this->config->get('baidu_weapp');
        if ($this->dev_mode == 1) {
            $this->baidu_weapp_config = $this->config->get('debug_config.baidu_weapp');
        }
        $this->alipay = $this->config->get('alipay');
        $this->baidu = $this->config->get('baidu');
        $this->engineer_wx_name = $company_config['engineer_wx_config']['app_wx_name'] ?? '优净云';
        $this->user_wx_name = $company_config['wx_config']['app_wx_name'] ?? '优净云';
    }

    public function __construct()
    {
        parent::__construct();
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
        $this->BossApiClient = get_instance()->getAsynPool('BossApi');
    }

    /**
     * 异常的回调(如果需要继承$autoSendAndDestroy传flase)
     * @param Throwable $e
     * @param null $handle
     * @throws Exception
     * @date 2019/5/28 10:09
     */

    public function onExceptionHandle(\Throwable $e, $handle = null)
    {
        $log = '--------------------------[报错信息]----------------------------' . PHP_EOL;
        $log .= 'client：User' . PHP_EOL;
        $log .= 'DATE：' . date("Y-m-d H:i:s") . PHP_EOL;
        $log .= 'getMessage：' . $e->getMessage() . PHP_EOL;
        $log .= 'getCode：' . $e->getCode() . PHP_EOL;
        $log .= 'getFile：' . $e->getFile() . PHP_EOL;
        $log .= 'getLine：' . $e->getLine();
        FileLogService::WriteLog(SLog::ERROR_LOG, 'error', $log);

        //必须的代码
        if ($e instanceof SwooleRedirectException) {
            $this->http_output->setStatusHeader($e->getCode());
            $this->http_output->setHeader('Location', $e->getMessage());
            $this->http_output->end('end');
            return;
        }
        if ($e instanceof SwooleException) {
            secho("EX", "--------------------------[报错指南]----------------------------" . date("Y-m-d h:i:s"));
            secho("EX", "异常消息：" . $e->getMessage());
            print_context($this->getContext());
            secho("EX", "--------------------------------------------------------------");
            $this->log($e->getMessage() . "\n" . $e->getTraceAsString(), Logger::ERROR);
        }
        //可以重写的代码
        if ($handle == null) {
            switch ($this->request_type) {
                case SwooleMarco::HTTP_REQUEST:
                    $e->request = $this->request;
                    $data = get_instance()->getWhoops()->handleException($e);
                    $this->http_output->setStatusHeader(500);
                    $this->http_output->end($data);
                    break;
                case SwooleMarco::TCP_REQUEST:
                    $this->send($e->getMessage());
                    break;
            }
        } else {
            \co::call_user_func($handle, $e);
        }
    }

    /**
     * 返回json 格式数据
    */
    public function show(int $code,string $message,array $data)
    {
        $returnArr = [
            'code'=>$code,
            'message'=>$message,
            'data'=>$data
        ];
        if (!$this->canEnd()) {
            return;
        }
        $this->response->end($returnArr);
        $this->endOver();
    }

    // 返回json格式
    public function jsonend($code = '', $message = '', $data = '', $gzip = true)
    {
        $output = array('code' => $code, 'message' => $message, 'data' => $data);

        if (!$this->canEnd()) {
            return;
        }
        if (!get_instance()->config->get('http.gzip_off', false)) {
            //低版本swoole的gzip方法存在效率问题
//            if ($gzip) {
//                $this->response->gzip(1);
//            }
            //压缩备用方案
            /* if ($gzip) {
              $this->response->header('Content-Encoding', 'gzip');
              $this->response->header('Vary', 'Accept-Encoding');
              $output = gzencode($output . " \n", 9);
              } */
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'application/json; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }
        $this->response->end($output);
        $this->endOver();
    }



    /**
     * @desc   判断选择地址是否在产品服务区内
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function isInService($address_id, $equipments_id)
    {
        $address_info = $this->addr_model->getOne(array('address_id' => $address_id), '*');
        if (empty($address_info)) {
            return false;
        }
        //是否所属全省/市
        $map['provice_code'] = ['IN', [-1, $address_info['provice_code']]];
        $map['city_code'] = ['IN', [-1, $address_info['city_code']]];
        $map['area_code'] = ['IN', [-1, $address_info['area_code']]];
        $map['equipments_id'] = $equipments_id;
        $info = $this->equip_area_model->getOne($map, 'city_code,area_code');
        if (!empty($info)) {
            return true;
        }
        return false;
    }


    /**
     * @desc   根据地址code获取详细信息
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function getAreaInfo($code)
    {
        $data = $this->area_model->getOne(array('id' => $code), '*');
        if (!empty($data)) {
            return $data;
        }
        return [];
    }

    /**
     * @desc   添加订单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLog($order_id, $status, $remark, $is_to_user = 0)
    {
        $data['operater_role'] = 1;
        $data['order_id'] = $order_id;
        $data['status'] = $status;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['is_to_user'] = $is_to_user;
        $res = $this->log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加工单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */

    public function addWorkOrderLog($work_order_id, $time, $status, $remark)
    {
        $log['work_order_id'] = $work_order_id;
        $log['create_work_time'] = $time;
        $log['operating_time'] = time();
        $log['do_id'] = $this->user_id;
        $log['operating_type'] = 2;
        $log['operating_status'] = $status;
        $log['remarks'] = $remark;
        $res = $this->work_log_model->add($log);
        return $res;
    }

    /**
     * @desc   添加合同操作日志
     * @param $status 1增2删3改4解绑'
     * @date   2018-07-19
     * @author lcx
     * @return [type]     [description]
     */
    public function addContractLog($contract_id, $status = 1, $remark)
    {
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $this->user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $this->contract_log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加用户登录日志
     * @param $user_id 用户信息
     * @param $client_info 客户端信息
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addUserLog($user_id, $client_info,$operator_type=2,$filename='',$old_content='',$new_content='')
    {
        $data['operator_type'] = $operator_type;
        $data['operator_id'] = $user_id;
        $data['filename'] = $filename;
        $data['new_content'] = $new_content;
        $data['old_content'] = $old_content;
        $user_info = $this->user_model->getOne(array('user_id' => $user_id), 'realname,username,account');
        if (!empty($user_info)) {
            $data['operator_name'] = $user_info['username'];
            $data['operator_account'] = $user_info['account'];
            $data['customer_id'] = $user_id;
            $data['add_time'] = time();
            $data['terminal_type'] = 3;
            $data['client_ip'] = $this->request->server['remote_addr'];
            if(!empty($client_info)){
                $data['client_device_brand'] = $client_info['client_device_brand']; //品牌
                $data['client_device_model'] = $client_info['client_device_model']; //型号
                $data['client_device_version'] = $client_info['clinet_device_wx_version'] . '-' . $client_info['clinet_device_system_version'] . '-' . $client_info['clinet_device_base_version'];
                $data['client_device_platform'] = $client_info['client_device_platform'];
            }
         
            return $this->user_log_model->add($data);
        }
        return false;
    }

    /**
     * @desc   获取时间范围详情
     * @param
     * @date   2018-07-24
     * @return [type]     [description]
     * @author lcx
     */
    public function getRangeInfo($time_id)
    {
        $info = $this->time_model->getOne(array('time_id' => $time_id));
        if (!empty($info)) {
            return $info;
        }
        return false;
    }


}
