<?php

namespace app\Controllers\User;

use app\Library\SLog;
use app\Services\Common\ConfigService;
use app\Services\Common\DataService;
use app\Services\Common\HttpService;
use app\Services\Company\CompanyService;
use app\Services\Contract\ContractService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use app\Services\Stock\StockService;
use app\Services\User\UserService;
use Server\CoreBase\SwooleException;

class Contract extends Base
{

    protected $contract_model;
    protected $auth_model;
    protected $user_model;
    protected $contract_ep_model;
    protected $parts_model;
    protected $ep_part_model;
    protected $bind_model;
    protected $company_model;
    protected $contractAdditionalModel;
    protected $equipment_list_model;
    protected $ContractPackageRecordModel;
    protected $equipmentWaterRecordModel;
    protected $Achievement;
    protected $contract_additional_model;
    protected $userService;
    protected $stockModel;
    protected $equipmentModel;
    protected $renewOrderModel;

    // 前置方法，加载模型
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->parts_model = $this->loader->model('PartsModel', $this);
        $this->ep_part_model = $this->loader->model('EquipmentsPartsModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->company_model = $this->loader->model('CompanyModel', $this);
        $this->contractAdditionalModel = $this->loader->model('ContractAdditionalModel', $this);
        $this->equipment_list_model = $this->loader->model('EquipmentListsModel', $this);
        $this->ContractPackageRecordModel = $this->loader->model('ContractPackageRecordModel', $this);
        $this->equipmentWaterRecordModel = $this->loader->model('EquipmentWaterRecordModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->contract_additional_model = $this->loader->model('ContractAdditionalModel', $this);
        $this->operation_info_model = $this->loader->model('OperationInfoModel', $this);
        $this->stockModel = $this->loader->model('StockModel', $this);
        $this->equipmentModel = $this->loader->model('EquipmentModel', $this);
        // 续费订单模型
        $this->renewOrderModel = $this->loader->model('RenewOrderModel', $this);
        $this->userService = new UserService();
    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 合同签署
     * @description
     * @method POST
     * @url User/Contract/sign
     * @param contract_id 必选 int 合同ID
     * @param contact_type 可选 int 合同类型1电子合同-确认同意 2电子合同-电子签名 3纸质合同，默认1
     * @return {"code": 1000, "message": "签署成功", "data": ""}
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_sign()
    {
        // 参数
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数');
        }
        $this->parm['contract_id'] = (int)$this->parm['contract_id'];
        $map['contract_id'] = $this->parm['contract_id'];
        $work_order_id = $this->parm['work_order_id'] ?? '';
        $contact_type = $this->parm['contact_type'] ?? 2;
        // 查询合同
        $data = $this->contract_model->getOne($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1000, "合同不存在");
        }
        //判断用户是否通过实名认证
        $user_info = $this->user_model->getOne(array('user_id' => $data['user_id']), 'is_auth,realname,gender,user_class');
        if (empty($user_info) || $user_info['is_auth'] != 2) {
            return $this->jsonend(-1201, "该客户还未通过实名认证，请先进行实名");
        }
        // 判断是否商用机合同
        $business_user_contract_sign_way = $this->config->get('business_user_contract_sign_way')[$user_info['user_class']];
        if ($data['contract_equipments_line'] == 2 && $business_user_contract_sign_way == 1) {
            return $this->jsonend(-1000, "抱歉,商用机暂不支持线上签署合同,后续师傅将会上传纸质合同到系统");
        }
        //2024-5-7 签署前查询设备如果有移机操作 查询当前合同关联的移机合同（因为设备存在于移机合同下）
        $this->parm['contract_id'] = ContractService::getMoveContractId((int)$this->parm['contract_id']);
        // 签署合同时判断是否已绑定设备
        $is_binding = ContractService::isBindingEquipments((int)$this->parm['contract_id']);
        if($is_binding){
            return $this->jsonend(-1000,'未绑定设备,合同无法签署');
        }
        // 签署合同日志
        $msg = ['contract_id' => $data['contract_id'], 'esign_filename' => $data['esign_filename'], 'esign_user_sign' => $data['esign_user_sign'], 'esign_contract_no' => $data['esign_contract_no'], 'exire_date' => $data['exire_date'], 'installed_time' => $data['installed_time']
        ];
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'contract_sign_before_log', json_encode($msg) . PHP_EOL, true);
        // 是否已签合同 是否续费
        if (!empty($this->parm['is_sign']) && $this->parm['is_sign'] == 2) {
            $this->db->begin(function () use ($data) {
                $edit_contract['esign_contract_no'] = 'XF' . date('YmdHis', time()) . rand(1000000000, 9999999999);
                $edit_contract['esign_filename'] = $this->parm['esign_filename'] ?? '';
                $edit_contract['esign_user_sign'] = $this->parm['esign_user_sign'] ?? '';
                $this->contract_model->save(array('contract_id' => $this->parm['contract_id']), $edit_contract);
                // 合同ID
                $add_data['contract_id'] = $this->parm['contract_id'];
                // 创建时间
                $add_data['create_time'] = time();
                // 签署时间
                $add_data['sign_time'] = time();
                // 补充类型1移机协议 2拆机合同 3续费合同
                $add_data['type'] = 3;
                // 状态1待签署 2已签署 3作废
                $add_data['status'] = 2;
                // 续费订单id
                $add_data['renew_order_id'] = $this->parm['renew_order_id'] ?? '';
                // 电子签名成功后保存签署后的文件名称
                $add_data['esign_filename'] = $this->parm['esign_filename'] ?? '';
                // 电子签名,用户签字图片
                $add_data['esign_user_sign'] = $this->parm['esign_user_sign'] ?? '';
                // 添加合同补充协议
                $this->contract_additional_model->add($add_data);
            }, function ($e) {
                return $this->jsonend(-1000, '签合同失败', $e->error);
            });
            return $this->jsonend(1000, "签约成功");
        } else {
            if ($data['status'] != 3) {
                return $this->jsonend(-1000, "抱歉,该合同无法进行签署!请确认合同是否有效或者已经签署过");
            }
            //------------------------------------------移机新增begin------------------------------------------
            //根据合同获取工单信息
            $where = [
                'contract_id' => $map['contract_id'],
                'rq_work_order_business.business_id' => ['IN', [5, 10]],
                'eq_type' => 1
            ];
            if (!empty($work_order_id) && $work_order_id != 'undefined') {
                $where['rq_work_order.work_order_id'] = $work_order_id;
            }
            $field = '
            rq_work_order.work_order_id,
            rq_work_order_business.business_id,
            rq_work_order.is_new_word_order,
            rq_work_equipment.equipment_id,
            rq_work_order.replacement_type,
            rq_work_order.move_change_equipments,
            rq_work_order.user_id,
            rq_work_order.equipments_id,
            rq_work_order.equipment_num
        ';
            $join = [
                ['work_order_business', 'rq_work_order.work_order_id = rq_work_order_business.work_order_id', 'LEFT'],
                ['work_equipment', 'rq_work_order.work_order_id = rq_work_equipment.work_order_id', 'LEFT'],
            ];
            $this->Achievement->table = 'work_order';
            $work_order = $this->Achievement->findJoinData($where, $field, $join);
            $is_replacement = false;
            if (!empty($work_order)) {
                if ($work_order['replacement_type'] == 2) {
                    return $this->jsonend(-1000, "移机拆不需要签署合同");
                }
                $is_replacement = true;
            }
            //------------------------------------------移机新增end------------------------------------------
            //查询合同设备信息--处理设备配件信息
            $c_join = [
                ['equipments e', 'e.equipments_id = rq_contract_equipment.equipments_id', 'left']
            ];
            $contract_info = $this->contract_ep_model->getAll(array('contract_id' => $this->parm['contract_id']), 'e.original_parts,e.filter_element,e.equipments_type,rq_contract_equipment.equipment_id', $c_join);
            $eq_part_lists = array();
            $eq_filter_element = array();
            $eq_filter_element_num = array();
            $original_parts = array();
            $db_eq_filter_element_num = [];
            if (!empty($contract_info) && !$is_replacement) {
                foreach ($contract_info as $k => $v) {
                    //原始配件
                    if (!empty($v['original_parts'])) {
                        $original_parts = json_decode($v['original_parts'], true);
                        if (!empty($original_parts)) {
                            foreach ($original_parts as $kk => $vv) {
                                $arr['contract_id'] = $this->parm['contract_id'];
                                $arr['equipment_id'] = $v['equipment_id'];
                                $arr['parts_id'] = $vv['parts_id'];
                                $arr['is_original'] = 1;
                                $arr['create_time'] = time();
                                $arr['cycle'] = $vv['cycle'] ?? 999;
                                $arr['last_replace_time'] = time();
                                $arr['expire_time'] = ContractService::getPartsExpireTime($arr['cycle'],$arr['last_replace_time']);
                                $arr['parts_number'] = 1;
                                $eq_part_lists[] = $arr;
                            }
                        }
                    }
                    // 新滤芯
                    if (!empty($data['package_id'])) {
                        // TODO 2024-08-09
                        $this->Achievement->table = 'equipments_filter';
                        $filterArr = $this->Achievement->selectData(['package_id'=>(int)$data['package_id']]);
                        // TODO 如果在新套餐滤芯关系表查询不到数据  读取产品保存的滤芯字段
                        if(empty($filterArr)){
                            if (!empty($v['filter_element'])) {
                                $filterArr = json_decode($v['filter_element'], true);
                            }
                        }
                        if (!empty($filterArr)) {
                            foreach ($filterArr as $kk => $vv) {
                                $arr2['contract_id'] = $this->parm['contract_id'];
                                $arr2['equipment_id'] = $v['equipment_id'];
                                $arr2['parts_id'] = $vv['parts_id'];
                                $arr2['is_filter'] = 1;
                                $arr2['create_time'] = time();
                                $arr2['cycle'] = getMonthByDay($vv['cycle']);// 月
                                $arr2['last_replace_time'] = time();
                                $arr2['expire_time'] = ContractService::getPartsExpireTime($vv['cycle'],$arr2['last_replace_time']);
                                $arr2['parts_number'] = 1;
                                $arr2['filter_level'] = $vv['level'] ?? $kk+1;
                                $eq_filter_element[] = $arr2;
                                array_push($eq_filter_element_num, $vv['cycle']);
                                $db_arr['residual_value'] = $vv['cycle'];
                                $db_arr['max'] = $vv['cycle'];
                                array_push($db_eq_filter_element_num, $db_arr);
                            }
                        }
                        //处理滤芯
                        $all_len = count($eq_filter_element_num);
                        if ($all_len < 5) {
                            $len = 5 - $all_len;
                            for ($i = 0; $i < $len; $i++) {
                                array_push($eq_filter_element_num, 1000);
                                $db_arr['residual_value'] = 1000;
                                $db_arr['max'] = 1000;
                                array_push($db_eq_filter_element_num, $db_arr);
                            }
                        } else if ($all_len > 5) {
                            $eq_filter_element_num = array_slice($eq_filter_element_num, 0, 5);
                            $db_eq_filter_element_num = array_slice($db_eq_filter_element_num, 0, 5);
                        }
                    }
                }
            }
            //仅首次签署更新这些字段
            if ($data['is_effect'] == 1) {
                $edit_data['is_effect'] = 2;
                $edit_data['effect_time'] = !empty($data['sign_date']) ? $data['sign_date'] : time(); //合同生效时间默认是当前时间,如果后台设置了签约时间，就是签约时间
                $edit_data['installed_time'] = $edit_data['effect_time']; //装机时间
                if ($data['business_id'] == 5) {
                    //移机合同装机时间同新装时间
                    $new_contract = $this->contract_model->getOne(['contract_id' => $data['pid']], 'installed_time');
                    if (!empty($new_contract)) {
                        $edit_data['installed_time'] = $new_contract['installed_time'];
                    }
                }
            } else {
                $edit_data['installed_time'] = $data['installed_time'];
            }
            $edit_data['now_date'] = time(); //记录合同生效当时时间
            $edit_data['sign_client'] = empty($this->parm['sign_client'] ?? '') ? 1 : $this->parm['sign_client'];
            $edit_data['esign_filename'] = $this->parm['esign_filename'] ?? '';
            $edit_data['esign_user_sign'] = $this->parm['esign_user_sign'] ?? '';
            $edit_data['contact_type'] = $contact_type;
            if ($contact_type == 2) {
                $edit_data['is_to_user'] = 1;
            }
            $add_status = false;
            $this->db->begin(function () use ($edit_data, &$add_status, $is_replacement, $work_order, $eq_part_lists, $eq_filter_element, $eq_filter_element_num, $data, $db_eq_filter_element_num, $contract_info) {
                try {
                    //移机||换套餐
                    if ($is_replacement || $data['business_id'] == 10) {
                        //合同生效时间，装机时间，到期时间应该与原合同一致
                        $old_contract_id = $this->contract_model->getOne(['contract_id' => $this->parm['contract_id']], 'pid');
                        $old_contract_info = [];
                        if (!empty($old_contract_id) && !empty($old_contract_id['pid'])) {
                            $old_contract_info = $this->contract_model->getOne(['contract_id' => $old_contract_id['pid']], 'effect_time,installed_time,exire_date,real_exire_date,province,city,area,address');
                        }
                        $edit_data['effect_time'] = $old_contract_info['effect_time'] ?? '';
                        $edit_data['installed_time'] = $old_contract_info['installed_time'] ?? '';
                        $edit_data['exire_date'] = $old_contract_info['exire_date'] ?? '';
                        $edit_data['real_exire_date'] = $old_contract_info['real_exire_date'] ?? '';

                        //设备配件信息--如果是移机将原始合同的设备配件信息同步到移机合同
                        $where = [
                            'contract_id' => $data['pid'],
                            'equipment_id' => $work_order['equipment_id'],
                            'is_delete' => 0,
                        ];
                        $this->Achievement->table = 'equipments_parts';
                        $parts = $this->Achievement->selectData($where, '*', [], []);
                        if (!empty($parts)) {
                            $this->ep_part_model->del(['contract_id' => $data['pid']]); //删除当前合同相关设备配件信息
                            foreach ($parts as $key => $value) {
                                unset($value['id']);
                                $value['contract_id'] = $this->parm['contract_id'];
                                $this->ep_part_model->add($value);
                            }
                        }
                        //用户绑定设备
                        if ($is_replacement) {
                            $bind_data['equipments_id'] = $work_order['equipments_id'];
                            $bind_data['work_order_id'] = $work_order['work_order_id'];
                            $bind_data['equipment_id'] = $work_order['equipment_id'];
                            $bind_data['user_id'] = $work_order['user_id'];
                            $bind_data['create_time'] = time();
                            $bind_data['is_owner'] = 2;
                            $this->bind_model->addCustomerBindEquipment($bind_data);
                            //判断该地区是否开通
                            $address_info = $this->userService->getOwnArea($data['user_id'], '', $data['province_code'], $data['city_code'], $data['area_code'], $this->company);
                            if (!$address_info) {
                                returnJsonAdmin(-1101, "该地址暂未开通服务");
                            }
                            //用户划拨到移机区域
                            $this->Achievement->table = 'customer_administrative_center';
                            $customer_administrative_center = $this->Achievement->findData(['user_id' => $work_order['user_id'], 'administrative_id' => $address_info['a_id']], 'id');
                            if (empty($customer_administrative_center)) {
                                $this->Achievement->insertData([
                                    'user_id' => $work_order['user_id'],
                                    'operation_id' => $address_info['operation'],
                                    'administrative_id' => $address_info['a_id'],
                                    'update_time' => time(),
                                ]);
                            }
                        }

                        //更新设备地址
                        $eq['province_code'] = $data['province_code'];
                        $eq['city_code'] = $data['city_code'];
                        $eq['area_code'] = $data['area_code'];
                        $eq['province'] = $data['province'];
                        $eq['city'] = $data['city'];
                        $eq['area'] = $data['area'];
                        $eq['address'] = $data['address'];
                        $eq['contract_id'] = $this->parm['contract_id']; //更新设备表合同信息
                        $this->equipment_list_model->updateEquipmentLists($eq, array('equipment_id' => $work_order['equipment_id']));

                        //更新合同设备关系表设备地址
                        $eq_contr_map['contract_id'] = $this->parm['contract_id'];
                        $eq_contr_map['equipment_id'] = $work_order['equipment_id'];
                        $eq_contr_info = $this->contract_ep_model->getOne($eq_contr_map, 'moving_machine_number');
                        if (empty($eq_contr_info)) {
                            throw new Exception("合同里无该设备信息");
                        }
                        $eq_contr_data['equipments_address'] = $data['province'] . $data['city'] . $data['area'] . $data['address'];
                        $eq_contr_data['uptime'] = time();
                        $eq_contr_data['moving_machine_number'] = $eq_contr_info['moving_machine_number'] + 1;
                        $this->contract_ep_model->save($eq_contr_map, $eq_contr_data);

                        //新增合同补充协议
                        $add_data['contract_id'] = $this->parm['contract_id'];
                        $add_data['equipment_id'] = $work_order['equipment_id'];
                        $add_data['type'] = 1;
                        $add_data['old_address'] = $old_contract_info['province'] . $old_contract_info['city'] . $old_contract_info['area'] . $old_contract_info['address'];
                        $add_data['move_address'] = $data['province'] . $data['city'] . $data['area'] . $data['address'];
                        $add_data['create_time'] = time();
                        $this->contract_additional_model->add($add_data);

                        $this->Achievement->table = 'customer';
                        $user = $this->Achievement->findData(['user_id' => $work_order['user_id']], 'replacement_number');
                        if (!empty($user) && $user['replacement_number'] > 0) {
                            $this->Achievement->table = 'customer';
                            $this->Achievement->setDec(['user_id' => $work_order['user_id']], 'replacement_number', 1);
                        }
                        //获取合同信息
                        $this->Achievement->table = 'contract';
                        $new_contract = $this->Achievement->findData(['contract_id' => $this->parm['contract_id']], 'contract_no,pid');
                        if (!empty($new_contract)) {
                            //修改原始合同状态
                            $this->Achievement->updateData(['contract_id' => $new_contract['pid']], ['status' => 6]);
                            $this->Achievement->table = 'work_order';
                            $join = [
                                ['contract', 'rq_work_order.contract_id=rq_contract.contract_id', 'left']
                            ];
                            $old_contract = $this->Achievement->findJoinData(['rq_work_order.contract_id' => $new_contract['pid']], 'contract_no,work_order_id', $join);

                            if (!empty($old_contract)) {
                                //更新旧合同设备状态
                                $this->Achievement->table = 'contract_equipment';
                                $this->Achievement->updateData(['contract_id' => $new_contract['pid'], 'equipment_id' => $work_order['equipment_id']], ['state' => 0]);
                                //解除设备绑定关系
                                $this->Achievement->table = 'customer_bind_equipment';
                                $this->Achievement->updateData(['work_order_id' => $old_contract['work_order_id'], 'is_owner' => 2, 'user_id' => $work_order['user_id']], ['status' => 3, 'unbinding_time' => time()]);
                                //修改结算关系合同编号
                                if ($is_replacement) {
                                    $this->Achievement->table = 'contract_rank';
                                    $this->Achievement->updateData(['contract_no' => $old_contract['contract_no']], ['contract_no' => $new_contract['contract_no']]);
                                }
                            }
                        }
                    } else {
                        // 是否生效 1 否  2 是  （租赁人是否签字）
                        if ($data['is_effect'] == 1) {
                            $edit_data['for_core_time'] = time();
                            $edit_data['recent_time'] = time();
                            if ($data['is_buy_out'] == 1) {
                                $edit_data['exire_date'] = $data['exire_date'] ?? '';
                                $edit_data['real_exire_date'] = $data['exire_date'];
                            } else {
                                //$edit_data['exire_date'] = $data['package_cycle'] == 365 ? strtotime('+1year') : strtotime('+' . $data['package_cycle'] . ' day'); //如果为365直接加1年
                                // 新计算合同到期时间方式
                                $edit_data['exire_date'] = ContractService::getPartsExpireTime($data['package_cycle'],time());
                                $edit_data['real_exire_date'] = $edit_data['exire_date'];
                                if (!empty($data['expect_exire_date'])) {
                                    $edit_data['exire_date'] = $data['expect_exire_date'];
                                    $edit_data['real_exire_date'] = $data['expect_exire_date'];
                                }
                            }
                            $edit_data['renew_date'] = $data['package_cycle'];
                            //设备配件设置
                            $this->ep_part_model->del(['contract_id' => $this->parm['contract_id']]); //删除当前合同相关设备配件信息
                            if (!empty($eq_part_lists)) {
                                foreach ($eq_part_lists as $k => $v) {
                                    $this->ep_part_model->add($v);
                                }
                            }
                            if (!empty($eq_filter_element)) {
                                foreach ($eq_filter_element as $k => $v) {
                                    $re = $this->ep_part_model->add($v);
                                }
                            }
                        }
                    }
                    if (!empty($edit_data['exire_date'])) {
                        if ($data['is_buy_out'] == 1) {
                            $edit_data['status'] = 9;
                        } else {
                            $edit_data['status'] = $edit_data['exire_date'] > time() ? 4 : 5;
                        }
                    }else{
                        // TODO 正常转机合同重签
                        $edit_data['exire_date'] = $data['exire_date'] ?? '';
                        $edit_data['status'] = $data['exire_date'] > time() ? 4 : 5;
                    }
                    $this->contract_model->save(array('contract_id' => $this->parm['contract_id']), $edit_data); //修改合同信息

                    //查询合同设备信息
                    $eq_join = [
                        ['equipment_lists as el', 'rq_contract_equipment.equipment_id = el.equipment_id', 'inner'],
                        ['equipments as e', 'rq_contract_equipment.equipments_id = e.equipments_id', 'inner']
                    ];
                    $contract_eq_info = $this->contract_ep_model->getAll(array('rq_contract_equipment.contract_id' => $this->parm['contract_id']), 'el.device_no,e.type,rq_contract_equipment.equipment_id', $eq_join);
                    if (!empty($contract_eq_info)) {
                        if ($data['package_mode'] == 1) {
                            $package_mode = 1;
                            $working_mode = 1;
                        } else {
                            $package_mode = 0;
                            $working_mode = 0;
                        }
                        foreach ($contract_eq_info as $k => $v) {
                            /*                     * ****************************修改设备租赁状态****************** */
                            $this->bind_model->save(array('equipment_id' => $v['equipment_id'], 'is_owner' => 2, 'status' => 0), array('status' => 1));
                            $device_no = $v['device_no'];
                            if ($is_replacement && isset($work_order['replacement_type']) && $work_order['replacement_type'] != 2) {
                                //移机
                                //下发-绑定套餐
                                $params = ['sn' => $device_no, 'working_mode' => $package_mode, 'filter_element_max' => $eq_filter_element_num];
                                $path = '/House/Issue/bindingPackage';
                                HttpService::Thrash($params, $path);
                                sleepCoroutine(400);
                                //获取设备信息
                                $this->Achievement->table = 'contract_equipment';
                                $old_device = $this->Achievement->findData(['contract_id' => $data['pid']], 'equipment_id');
                                $used_traffic = 0;
                                if (!empty($old_device)) {
                                    $this->Achievement->table = 'equipment_water_record';
                                    $old_water = $this->Achievement->findData(['equipment_id' => $old_device['equipment_id']], 'SUM(water) AS number');
                                    $used_traffic = $old_water['number'] ?? 0;
                                }
                                //设备在到期时间当月最后一天前都可以使用--2020-04-13修改
                                $exire_date = strtotime(getMonthLastDay($data['exire_date']));   //获取签约月份最后一天
                                $data_sync_params['sn'] = $device_no;
                                $data_sync_params['used_days'] = ceil((time() - $data['effect_time']) / 86400);
                                $data_sync_params['used_traffic'] = $used_traffic;
                                $data_sync_params['remaining_days'] = ceil(($exire_date - time()) / 86400);
                                $data_sync_params['remaining_traffic'] = 0;
                                $data_sync_path = '/House/Issue/data_sync';
                                HttpService::Thrash($data_sync_params, $data_sync_path);
                                $this->redis->del('device_heartbeat:' . $device_no); //删除redis心跳
                                //请求心跳
                                sleepCoroutine(400);
                                $state_params['sn'] = $device_no;
                                $state_path = '/House/Issue/heartbeat';
                                HttpService::Thrash($state_params, $state_path);
                                //保存设备数据
                                $updateEquipmentLists = [
                                    'remaining_traffic' => $data_sync_params['remaining_traffic'],
                                    'remaining_days' => $data_sync_params['remaining_days'],
                                    'used_traffic' => $data_sync_params['used_traffic'],
                                    'used_days' => 0,
                                    'start_time' => $data['installed_time'],
                                    'end_time' => $data['exire_date'],
                                    'working_mode' => $working_mode,
                                    'filter_element' => json_encode($db_eq_filter_element_num)
                                ];
                                $this->equipment_list_model->updateEquipmentLists($updateEquipmentLists, array('equipment_id' => $v['equipment_id']));
                                //保存套餐信息
                                $package_record['contract_id'] = $this->parm['contract_id'];
                                $package_record['equipment_id'] = $v['equipment_id'];
                                $package_record['used_days'] = $data_sync_params['used_days'];
                                $package_record['remaining_days'] = $data_sync_params['remaining_days'];
                                $package_record['used_traffic'] = $data_sync_params['used_traffic'];
                                $package_record['remaining_traffic'] = $data_sync_params['remaining_traffic'];
                                $package_record['working_mode'] = $package_mode;
                                $package_record['filter_element_max'] = json_encode($eq_filter_element_num);
                                $package_record['hand_time'] = time();
                                $package_record['last_hand_time'] = time();
                                $package_record['status'] = 1;
                                $this->ContractPackageRecordModel->add($package_record);

                                //将套餐信息放入Redis中，实时监测套餐是否下发成功
                                $PlanToMonitor = $this->redis->get('PlanToMonitor');
                                if ($PlanToMonitor) {
                                    $PlanToMonitor = json_decode($PlanToMonitor, 1);
                                } else {
                                    $PlanToMonitor = [];
                                }
                                array_push($PlanToMonitor, $package_record);
                                $this->redis->set('PlanToMonitor', json_encode($PlanToMonitor));
                            } else {
                                //修改设备表租赁时间
                                if (!empty($edit_data['exire_date'])) {
                                    $this->equipment_list_model->updateEquipmentLists(array('start_time' => $edit_data['installed_time'], 'end_time' => $edit_data['exire_date'], 'working_mode' => $working_mode, 'filter_element' => json_encode($db_eq_filter_element_num)), array('equipment_id' => $v['equipment_id']));
                                }
                                /*                     * *********************给智能设备发送指令************************ */

                                if ($v['type'] == 1) { //智能下发套餐

                                    //下发-绑定套餐
                                    $params = ['sn' => $device_no, 'working_mode' => $package_mode, 'filter_element_max' => $eq_filter_element_num];
                                    $path = '/House/Issue/bindingPackage';

                                    HttpService::Thrash($params, $path);
                                    sleepCoroutine(2000);
                                   // sleep(10);
                                    //下发--数据同步
                                    $data_sync_params['sn'] = $device_no;
                                    $data_sync_params['used_days'] = 0;
                                    $data_sync_params['used_traffic'] = 0;
                                    $data_sync_params['remaining_days'] = 0;
                                    $data_sync_params['remaining_traffic'] = 0;
                                    if ($package_mode == 0) {
                                        // 新计算方式
                                        $data_sync_params['remaining_days'] = ContractService::getContractCycleDays($data['now_time'],$data['package_cycle']);
                                        $data_sync_params['remaining_traffic'] = $data['package_value'];
                                        //$data_sync_params['remaining_days'] = $data['package_cycle'];
                                    } else if ($package_mode == 1) {
                                        //$data_sync_params['remaining_days'] = $data['package_value'];
                                        $data_sync_params['remaining_days'] = ContractService::getContractCycleDays($data['now_time'],$data['package_cycle']);
                                        $data_sync_params['remaining_traffic'] = 999999999;
                                        //统计设备已用流量
                                        $equipment_water_record_sum = $this->equipmentWaterRecordModel->getOne(['equipment_id' => $v['equipment_id']], 'sum(water) total_water');  // 总用水量
                                        $data_sync_params['used_traffic'] = $equipment_water_record_sum['total_water'] ?? 0; //已用流量
                                    }
                                    //$exire_date = time() + $data['package_cycle'] * 86400;
                                    // TODO 统一计算方式 不再分别写入
                                    $exire_date = ContractService::getPartsExpireTime($data['package_cycle'],time());
                                    //如果后台设置了固定到期时间,取固定到期时间
                                    $sign_date = time();
                                    if (!empty($data['expect_exire_date'])) {
                                        $exire_date = $data['expect_exire_date'];
                                    }
                                    //设备在到期时间当月最后一天前都可以使用--2020-04-13修改
                                    $exire_date = strtotime(getMonthLastDay($exire_date));   //获取签约月份最后一天
                                    //如果后台设置了签约时间，固定时间减去签约时间----------新增
                                    if (!empty($data['sign_date'])) {
                                        $sign_date = $data['sign_date'];
                                    }

                                    $data_sync_params['sn'] = $device_no;
                                    $data_sync_params['used_days'] = 0;
                                    $data_sync_params['used_traffic'] = 0;
                                    $data_sync_params['remaining_days'] = ceil(($exire_date - time()) / 86400);
                                    $data_sync_params['remaining_traffic'] = 0;
//                                  $data_sync_params['remaining_days'] = ($exire_date - $sign_date) / 86400;

                                    $data_sync_path = '/House/Issue/data_sync';
                                    HttpService::Thrash($data_sync_params, $data_sync_path);
                                    AsyncFile::write('issue_package', date('Y-m-d H:i:s', time()) . '-' . $device_no . ':' . json_encode(['package' => $params, 'data_sync_params' => $data_sync_params]));
                                    $this->redis->del('device_heartbeat:' . $device_no); //删除redis心跳
                                    //请求心跳
                                    sleepCoroutine(2000);
                                    $state_params['sn'] = $device_no;
                                    $state_path = '/House/Issue/heartbeat';
                                    HttpService::Thrash($state_params, $state_path);
                                    //保存设备数据
                                    $this->equipment_list_model->updateEquipmentLists(array('remaining_traffic' => $data_sync_params['remaining_traffic'], 'remaining_days' => $data_sync_params['remaining_days'], 'used_traffic' => $data_sync_params['used_traffic'], 'used_days' => 0), array('equipment_id' => $v['equipment_id']));
                                    //保存套餐信息
                                    $package_record['contract_id'] = $this->parm['contract_id'];
                                    $package_record['equipment_id'] = $v['equipment_id'];
                                    $package_record['used_days'] = $data_sync_params['used_days'];
                                    $package_record['remaining_days'] = $data_sync_params['remaining_days'];
                                    $package_record['used_traffic'] = $data_sync_params['used_traffic'];
                                    $package_record['remaining_traffic'] = $data_sync_params['remaining_traffic'];
                                    $package_record['working_mode'] = $package_mode;
                                    $package_record['filter_element_max'] = json_encode($eq_filter_element_num);
                                    $package_record['hand_time'] = time();
                                    $package_record['last_hand_time'] = time();
                                    $package_record['status'] = 1;
                                    $this->ContractPackageRecordModel->add($package_record);

                                    //将套餐信息放入Redis中，实时监测套餐是否下发成功
                                    $PlanToMonitor = $this->redis->get('PlanToMonitor');
                                    if ($PlanToMonitor) {
                                        $PlanToMonitor = json_decode($PlanToMonitor, 1);
                                    } else {
                                        $PlanToMonitor = [];
                                    }
                                    array_push($PlanToMonitor, $package_record);
                                    $this->redis->set('PlanToMonitor', json_encode($PlanToMonitor));
                                }
                            }
                        }
                    }
                    //合同日志
                    $this->addContractLog($this->parm['contract_id'], 4, "合同签署成功，正式生效。由用户端发起签署");

                    //当合同签约成功且在华信模式下判断租赁下单根据推荐人减去经销商或者城市投资人的库存
                    $joins = [
                        ['work_order_business', 'rq_work_order.work_order_id=rq_work_order_business.work_order_id', 'left']
                    ];
                    $this->Achievement->table = 'work_order';
                    $work_order_info = $this->Achievement->findJoinData(['rq_work_order.work_order_id' => $work_order['work_order_id']], 'rq_work_order_business.business_id,rq_work_order.user_id,rq_work_order.replacement_type,rq_work_order.move_change_equipments,rq_work_order.equipments_id,rq_work_order.equipment_num,rq_work_order.move_old_equipments', $joins);
                    $equipment_info = $this->equipmentModel->getOne(['equipments_id' => $work_order_info['equipments_id']], 'equipments_id,equipments_type');

                    if (in_array($this->company, $this->config->get('logic_mode.huaxin'))) {
                        if ($equipment_info['equipments_type'] == 1 && ($work_order_info['business_id'] == 3 || ($work_order_info['business_id'] == 5 && in_array($work_order_info['replacement_type'], [1, 3]) && $work_order_info['move_change_equipments'] == 1))) { //租赁产品时订单的库存在签合同时扣除
                            //先判断用户是是否有经销商，没得则减去所属投资人的库存
                            $res = $this->userService->getUserBelongTo($work_order_info['user_id'], $this->company);
                            if (empty($res)) {
                                throw new SwooleException("用户推荐人信息错误");
                            }
                            //查询库存信息
                            $stock_where['role'] = $res['role'];
                            $stock_where['uid'] = $res['uid'];
                            $stock_where['equipments_id'] = $equipment_info['equipments_id']; //产品id
                            $stock_where['company_id'] = $this->company;
                            $stock_info = $this->stockModel->getOne($stock_where, 'stock_id,surplus');
                            $surplus = !empty($stock_info) ? $stock_info['surplus'] : 0;

                            //减去产品库存，并写入记录
                            StockService::changeStock($res['role'], $res['uid'], $work_order_info['equipments_id'], $work_order_info['equipment_num'], $this->company, 2);
                            StockService::addStockRecord($res['role'], $res['uid'], $work_order_info['equipments_id'], $surplus, $work_order_info['equipment_num'], 4, 2, $res['role'], $res['uid'], $this->company, '用户下单租赁产品，减少产品库存');

                            //移机拆/装和移机装工单且更换了新产品情况下增加库存
                            if ($work_order_info['business_id'] == 5 && in_array($work_order_info['replacement_type'], [1, 3]) && $work_order_info['move_change_equipments'] == 1) {
                                //查询旧产品剩余数量
                                $stock_where['equipments_id'] = $work_order_info['move_old_equipments'];
                                $old_stock_info = $this->stockModel->getOne($stock_where, 'surplus');
                                $surplus = !empty($old_stock_info) ? $old_stock_info['surplus'] : 0;
                                //增加旧产品库存
                                StockService::changeStock($res['role'], $res['uid'], $work_order_info['move_old_equipments'], $work_order_info['equipment_num'], $this->company, 1);
                                //写入库存记录
                                StockService::addStockRecord($res['role'], $res['uid'], $work_order_info['move_old_equipments'], $surplus, $work_order_info['equipment_num'], 1, 1, $res['role'], $res['uid'], $this->company, '用户移机下单更换租赁产品，增加旧产品库存');
                            }
                        }
                    }
                    $add_status = true;
                } catch (\Exception $exception) {
                    AsyncFile::write('contract_sign', date('Y-m-d H:i:s', time()) . '-' . $this->parm['contract_id'] . ':工程端签约失败,' . $exception->getMessage() . '///' . $exception->getLine());
                }
            }, function ($e) {
                AsyncFile::write('contract_sign', date('Y-m-d H:i:s', time()) . '-' . $this->parm['contract_id'] . ':工程端签约失败,' . $e->error);
                return $this->jsonend(-1000, "签约失败,请重试!" . $e->error);
            });
            if ($add_status) {
                //向大数据发送通知
                $big_data_notice_config = ConfigService::getTemplateConfig('contract_sign_success', 'big_data', $this->company);
                if (!empty($big_data_notice_config) && !empty($big_data_notice_config['websocket']) && $big_data_notice_config['websocket']['switch']) {
                    mb_internal_encoding('UTF-8');
                    $gender = $user_info['gender'] == 1 ? '先生' : '女士';
                    $user_named = mb_substr($user_info['realname'], 0, 1) . $gender;
                    DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 7, 'user' => $user_named]]);
                }
                return $this->jsonend(1000, "签约成功");
            }
            AsyncFile::write('contract_sign', date('Y-m-d H:i:s', time()) . '-' . $this->parm['contract_id'] . ':工程端签约失败');
            return $this->jsonend(-1000, "签约失败");
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 合同详情
     * @description
     * @method POST
     * @url User/Contract/getContractInfo
     * @param contract_id 必选 int 合同ID
     * @return {"code":1000,"message":"获取成功","data":{"contract_info":{"status":"4","contract_no":"CNA16575948802157","contract_money":"1000.00","contract_deposit":"400.00","is_effect":"2","effect_time":"2018-10-16","contract_cycle":"55","contract_class":"1","esign_path":"https:\\/\\/\\/jscloud.youheone.com\\/p\\/pdf\\/PDF_User\\/20181016_CNA16575948802157_57_1.pdf","c","contract_time_start_format":"2018-10-16","contract_time_end_format":"2018-12-10","contract_time_start":["2018","10","16"],"contract_time_end":["2018","12","10"],"province":"重庆市","city":"重庆市","area":"渝北区","address":"觅凰庭","renew_money":"600.00","esign_user_sign":"https:\\/\\/\\/jscloud.youheone.com\\/p\\/pdf\\/img_yz\\/20181016110359CNA165759488021571539659039_39_8207.png"},""},"rental_user_info":{"realname":"Liu","auth_type":"1","id_number":"500235199201303340","business_license_pic":null,"telphone":"18883880448"},"company_info":{"company_id":"1","company_name":"重庆优合一众网络科技有限公司","business_license_pic":"J9300300403043","company_addr":"重庆市渝北区山顶道国宾城12栋13-13","telphone":"023-86838055","contacts":"王**","contacts_tel":"18549302304","company_seal":"https:\\/\\/\\/jscloud.youheone.com\\/p\\/pdf\\/img_yz\\/\\/yinzhang.png","s","status":"1"},"equipments_info":{"list":[{"equipments_number":"00000001","equipments_name":"DR75-C5型直饮水机","model":"Product 7"}],"total_num":1},"original_parts":[{"parts_id":"6","parts_name":"减压阀","parts_pic":"https:\\/\\/\\/qn.youheone.com\\/f\\/\\/fad4520180911132244480.png","c","cycle":"720"}],"package_info":{"package_mode":"1","package_value":"55","package_money":"600.00","package_cycle":"55"},"contract_additional":[]}}
     * @return_param contract_info array 合同基本信息
     * @return_param rental_user_info array 租赁人信息
     * @return_param company_info array 公司信息
     * @return_param equipments_info array 设备信息
     * @return_param original_parts array 原始配件
     * @return_param package_info array 套餐信息
     * @return_param contract_additional array 补充协议
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getContractInfo()
    {
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数');
        }
        $map['contract_id'] = $this->parm['contract_id'];
        $data = $this->contract_model->getOne($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1000, "合同不存在");
        }
        /*         * **********************合同信息********************* */
        $contract_info = array();
        $contract_info['attach'] = $data['attach']; //合同界面布局样式1润泉版 2济南铭沁版
        $contract_info['contract_equipments_line'] = $data['contract_equipments_line']; //合同界面布局样式1润泉版 2济南铭沁版
        $contract_info['view_layout'] = $data['view_layout']; //合同界面布局样式1润泉版 2济南铭沁版
        $contract_info['status'] = $data['status'];
        $contract_info['operation_id'] = $data['operation_id'];
        $contract_info['company_id'] = $data['company_id']; // 入驻商ID
        $contract_info['contract_no'] = $data['contract_no']; //合同编号
        $contract_info['contract_money'] = $data['contract_money']; //合同金额
        $contract_info['contract_deposit'] = $data['contract_deposit']; //押金
        $contract_info['is_effect'] = $data['is_effect'];
        $contract_info['effect_time'] = empty($data['effect_time']) ? '' : date('Y-m-d', $data['effect_time']);
        $contract_info['contract_cycle'] = $data['package_cycle'];
        $contract_info['contract_class'] = $data['contract_class'];
        $contract_info['business_id'] = $data['business_id'];
        $contract_info['esign_contract_no'] = $data['esign_contract_no'];
        $contract_info['pid'] = $data['pid'];
        $contract_info['is_buy_out'] = $data['is_buy_out'];
        $contract_info['is_dl'] = $data['is_dl'];
        if ($this->dev_mode == 1) {
            //$contract_info['esign_path'] = empty($data['esign_filename']) ? '' : $this->config->get('debug_config.static_resource_host') .'src/www/'. $data['esign_filename'];
            $contract_info['esign_path'] = empty($data['esign_filename']) ? '' : $this->config->get('debug_config.static_resource_host') . $data['esign_filename'];
        } else {
            $contract_info['esign_path'] = empty($data['esign_filename']) ? '' : $this->config->get('static_resource_host') . $data['esign_filename'];
        }
        if ($data['is_effect'] == 1) {
            $contract_info['contract_time_start_format'] = date('Y-m-d', time()); //合同开始时间
            $package_cycle = getMonthByDay($data['package_cycle']);
            $contract_info['contract_time_end_format'] = date('Y-m-d', strtotime('+ ' . $package_cycle . ' month',time()));
        } else {
            if ($data['status'] == 9) {
                // 买断
                $buy_exire_date = $this->config->get('is_buy_out.exire_year');
                $contract_info['contract_time_start_format'] = date('Y-m-d', strtotime('-' . $buy_exire_date . ' year', $data['exire_date'])); //合同开始时间
            } else {
                $contract_info['contract_time_start_format'] = date('Y-m-d', strtotime('-1 year', $data['exire_date'])); //合同开始时间
            }
            $contract_info['contract_time_end_format'] = date('Y-m-d', $data['exire_date']); //合同到期时间
        }
        $contract_info['exitTime'] = 1;
        if (!empty($data['is_buy_out'])) {
            if ($data['is_buy_out'] == 1) {
                $buy_exire_date = $this->config->get('is_buy_out.exire_year');
                $contract_info['contract_time_start_format'] = date('Y-m-d', strtotime('-' . $buy_exire_date . ' year', $data['exire_date'])); //合同开始时间
                $contract_info['contract_time_end_format'] = date('Y-m-d', $data['exire_date']); //合同到期时间
                $contract_info['exitTime'] = '/';
            }
        }

        $contract_info['AY'] = date('Y');
        $contract_info['AM'] = date('m');
        $contract_info['AD'] = date('d');
        $contract_info['contract_time_start'] = explode('-', $contract_info['contract_time_start_format']);
        $contract_info['contract_time_end'] = ($data['is_buy_out'] == 2) ? explode('-', $contract_info['contract_time_end_format']) : array('/', '/', '/');
        $contract_info['province'] = $data['province'];
        $contract_info['city'] = $data['city'];
        $contract_info['area'] = $data['area'];
        $contract_info['address'] = $data['address'];
        $contract_info['renew_money'] = $data['renew_money'];
        $contract_info['installProperty'] = '家用机';
        $contract_info['waterPressure'] = ''; //水压
        $contract_info['capContractMoney'] = num_to_rmb($contract_info['contract_money']);
        $contract_info['cycle'] = ''; //换芯周期
        $contract_info['equipmentType'] = 'RO反渗透直饮机'; //设备型号
        $contract_info['signRepresent'] = ''; //签约代表
        if ($this->dev_mode == 1) {
            $static_resource_host = $this->config->get('debug_config.static_resource_host');
        } else {
            $static_resource_host = $this->config->get('qiniu.qiniu_url');
        }
        // TODO 2024-09-06 合同详情里的滤芯周期 因新老客户有差异
        if($data['is_old_contract'] == 1){
            $contract_info['filter_cycle'] = [6, 6, 12, 12, 12];
        }else{
            $contract_info['filter_cycle'] = [6, 6, 6, 12, 12];
        }
        if ($contract_info['contract_equipments_line'] == 2) {
            $contract_info['filter_cycle'] = [3, 3, 6, 12, 6];
        }
        $contract_info['house_type'] = $data['house_type'];
        $contract_info['attach'] = empty($contract_info['attach']) ? '' : $contract_info['attach'];

        /**         * *********************乙方信息---租赁人********************* */
        $rental_user_info = array();
        //认证信息
        $auth_info = $this->auth_model->getAll(array('user_id' => $data['user_id'], 'auth_role' => 1), '*');
        if (($data['contract_equipments_line'] != 2 || ($data['contract_equipments_line'] == 2 && $this->config->get('business_machines')['is_need_auth'] == true)) && (empty($auth_info) || $auth_info['status'] != 2)) {
            return $this->jsonend(-1101, "您还未通过认证,请先认证");
        }
        $user_info = $this->user_model->getOne(array('user_id' => $data['user_id']), 'user_class,telphone,emergency_contact,realname,id_card');
        $rental_user_info['realname'] = $auth_info['realname'] ?? $user_info['realname'];
        $rental_user_info['auth_type'] = $auth_info['auth_type'] ?? 1;
        $rental_user_info['id_number'] = $auth_info['id_number'] ?? $user_info['id_card'];
        $rental_user_info['user_id'] = $data['user_id'];
        if (in_array($rental_user_info['auth_type'], [2, 3])) {
            $rental_user_info['realname'] = $auth_info['company_name'];
            $rental_user_info['id_number'] = $auth_info['business_license_number'];
        }
        $rental_user_info['business_license_pic'] = $auth_info['business_license_pic'] ?? '';
        //基本信息

        $user_info['emergency_contact'] = empty($user_info['emergency_contact']) ? '' : json_decode($user_info['emergency_contact'], true);
        $rental_user_info['telphone'] = $user_info['telphone'];
        $rental_user_info['nailEmail'] = ''; //邮箱
        $rental_user_info['user_class'] = $user_info['user_class']; //邮箱
        $rental_user_info['nailUrgentTel'] = empty($user_info['emergency_contact']) ? '' : ($user_info['emergency_contact']['tel'] ?? ''); //紧急联系电话
        $arr['rental_user_info'] = $rental_user_info;
        if ($rental_user_info['user_class'] == 2) {
            $static_resource_host = $this->config->get('qiniu.qiniu_url');
        }
        $contract_info['esign_user_sign'] = empty($data['esign_user_sign']) ? '' : $static_resource_host . $data['esign_user_sign'];
        //$contract_info['esign_user_sign'] = empty($data['esign_user_sign']) ? '' : $static_resource_host .'src/www/'. $data['esign_user_sign'];

        $arr['contract_info'] = $contract_info;

        /**         * *********************甲方信息---公司********************* */
        //$arr['company_info'] = $this->company_model->getOne(array('company_id' => $data['company_id']), '*');
        $company_info = CompanyService::getCompanyInfo($data['company_id']);
        //获取运营中心的印章
        $operation_info = $this->operation_info_model->getOne(['o_id' => $contract_info['operation_id']], 'contract_company_seal,contract_company_name');
        if (!empty($operation_info) && !empty($operation_info['contract_company_seal'])) {
            $company_info['company_name'] = $operation_info['contract_company_name'];
            $company_info['company_seal'] = $this->config->get('qiniu.qiniu_url') . $operation_info['contract_company_seal'];
        }
        $arr['company_info'] = $company_info;

        /*         * **********************设备信息********************* */
        $equipment_info = array();
        $join = [
            ['equipments as e', 'e.equipments_id = rq_contract_equipment.equipments_id', 'inner'],
            ['equipment_lists as el', 'el.equipment_id = rq_contract_equipment.equipment_id', 'inner']
        ];
        $contract_ep_info = $this->contract_ep_model->getAll(array('rq_contract_equipment.contract_id' => $this->parm['contract_id']),
            'rq_contract_equipment.*,e.equipments_name,e.model,e.original_parts,el.device_no,equipments_orgin_money,el.province, el.city, el.area, el.address', $join);
        if (!empty($contract_ep_info)) {
            foreach ($contract_ep_info as $k => $v) {
                $equipment['equipments_number'] = $v['device_no'];
                $equipment['equipments_name'] = $v['equipments_name'];
                $equipment['model'] = $v['model'];
                $equipment['province'] = $v['province'];
                $equipment['city'] = $v['city'];
                $equipment['area'] = $v['area'];
                $equipment['address'] = $v['address'];
                $equipment['equipments_orgin_money'] = $v['equipments_orgin_money'];
                $equipment['cap_equipments_orgin_money'] = num_to_rmb($v['equipments_orgin_money']);
                $equipment_info[] = $equipment;
            }
        }
        $arr['equipments_info']['list'] = $equipment_info;
        $arr['equipments_info']['total_num'] = count($equipment_info);
        /*         * **********************原始配件信息********************* */
        $parts = array();
        if (!empty($contract_ep_info)) {
            $original_parts = json_decode($contract_ep_info[0]['original_parts'], true);
            if (!empty($original_parts)) {
                foreach ($original_parts as $k => $v) {
                    $part_info = $this->parts_model->getOne(array('parts_id' => $v['parts_id']), 'parts_name,parts_pic');
                    $part['parts_id'] = $v['parts_id'];
                    $part['parts_name'] = $part_info['parts_name'];
                    $part['parts_pic'] = $this->config->get('qiniu.qiniu_url') . $part_info['parts_pic'];
                    $part['cycle'] = $v['cycle'];
                    $parts[] = $part;
                }
            }
        }

        $arr['original_parts'] = $parts;

        /*         * **********************合同套餐信息********************* */
        $package_info['package_mode'] = $data['package_mode'];
        $package_info['package_value'] = $data['package_cycle'];
        $package_info['package_money'] = $data['package_money'];
        $package_info['package_cycle'] = $data['package_cycle'];
        $arr['package_info'] = $package_info;

        // 获取合同补充协议信息
        $where['rq_contract_additional.contract_id'] = $this->parm['contract_id'];
        $join = [
            ['equipment_lists el', 'el.equipment_id = rq_contract_additional.equipment_id', 'left join']
        ];
        $field = 'rq_contract_additional.*,el.device_no';
        $order = ['create_time' => 'DESC'];
        $contract_additional = $this->contractAdditionalModel->getAll($where, $field, $join, $order);
        if ($contract_additional) {
            $type = ['1' => '移机协议', '2' => '拆机协议', '3' => '续费合同'];
            $status = ['1' => '待签署', '2' => '已签署', '3' => '作废'];
            foreach ($contract_additional as $k => $v) {
                $contract_additional[$k]['type'] = $type[$v['type']];
                $contract_additional[$k]['status'] = $status[$v['status']];
                if (strlen($v['create_time']) > 5) {
                    $contract_additional[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                } else {
                    $contract_additional[$k]['create_time'] = '';
                }
                if (strlen($v['sign_time']) > 5) {
                    $contract_additional[$k]['sign_time'] = date('Y-m-d H:i:s', $v['sign_time']);
                } else {
                    $contract_additional[$k]['sign_time'] = '';
                }
            }
        }
        $arr['contract_additional'] = $contract_additional;

        //获取续费优惠券金额
        $this->Achievement->table = 'coupon_cate';
        $coupon = $this->Achievement->findData(['put_type' => 5, 'status' => 1], 'money');
        $arr['coupon'] = ($coupon['money'] ?? 0) / 100;

        if (!empty($this->parm['coupon'])) {
            $is_array = is_array($this->parm['coupon']);
            if ($is_array) {
                $money = 0;
                foreach ($this->parm['coupon'] as $item) {
                    $this->Achievement->table = 'coupon';
                    $coupon = $this->Achievement->findData(['coupon_id' => $item], 'cid');
                    $arr['coupons'][] = $coupon;
                    $this->Achievement->table = 'coupon_cate';
                    $coupon_money = $this->Achievement->findData(['put_type' => 5, 'status' => 1, 'id' => $coupon['cid']], 'money');
                    $money = $money + $coupon_money['money'];
                }
                $arr['money'] = $money / 100;
            } else {
                $arr['money'] = 0;
            }
        } else {
            $arr['money'] = 0;
        }

        $renew_order = $this->renewOrderModel->getOne(['contract_id' => $this->parm['contract_id'], 'pay_status' => 2, 'pay_time' => ['>', '1703088000']], 'renew_order_id, status', [], ['renew_order_id' => 'desc']);
        if (!empty($renew_order)) {
            if ($renew_order['status'] == 4) {
                // 没有续过费
                $arr['is_sign'] = 1;
            } else {
                $contract_additional = $this->contract_additional_model->getOne(['renew_order_id' => $renew_order['renew_order_id']]);
                if (empty($contract_additional)) {
                    $arr['status_name'] = $this->config->get('contract_status')[3];
                    if ($contract_info['status'] == 6 || $contract_info['status'] == 7 || $contract_info['status'] == 9) {
                        $arr['is_sign'] = 3;
                    } else {
                        // 续费了没有记录
                        $arr['is_sign'] = 2;
                    }
                } else {
                    // 续费了有记录
                    $arr['is_sign'] = 3;
                }
            }
            $arr['renew_order_id'] = $renew_order['renew_order_id'];
        } else {
            if ($contract_info['status'] == 6 || $contract_info['status'] == 7 || $contract_info['status'] == 9) {
                $arr['is_sign'] = 3;
            } else {
                // 没有续过费
                $arr['is_sign'] = 1;
            }
            $arr['renew_order_id'] = '';
//            if (!empty($this->parm['renew_flag']) && $this->parm['renew_flag'] == 1) {
//                $arr['contract_info']['renew_money'] = $contract_info['renew_money'];
//            } else {
//                $arr['contract_info']['renew_money'] = $contract_info['contract_money'];
//            }
        }
        // 获取合同显示模版
        $contractService = new ContractService();
        $arr['web_template'] = $contractService->getWebContractTemplate($arr);
        return $this->jsonend(1000, "获取成功", $arr);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 合同列表
     * @description
     * @method POST
     * @url User/Contract/getMyContractList
     * @param page 可选 int 页数，默认1
     * @param pagesize 可选 int 每页条数，默认10
     * @return { "code": 1000,"message": "获取列表成功","data": [{"contract_no": "CNA16700500823908","renew_date": "55","province": "重庆市","city": "重庆市", "area": "渝北区","address": "觅凰庭","contract_id": "3","status": "4","exire_date": "2018-12-10","contract_class": "1","status_name": "生效中" }]}
     * @return_param contract_no string 合同编号
     * @return_param renew_date string 合同续费金额，单位元
     * @return_param province string 合同省份
     * @return_param city string 合同城市
     * @return_param area string 合同区域
     * @return_param contract_id string 合同ID
     * @return_param status string 合同状态
     * @return_param exire_date string 到期时间
     * @return_param status_name string 状态名称
     * @return_param contract_class int 合同分类 1租赁合同 2购买合同
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getMyContractList()
    {
        $page = empty($this->parm['page'] ?? '') ? 1 : $this->parm['page'];
        $pagesize = empty($this->parm['pagesize'] ?? '') ? 1 : $this->parm['pagesize'];
        $map['user_id'] = $this->user_id;
        $map['is_to_user'] = 1;
        $map['status'] = ['IN', [3, 4, 5, 6, 7]];
        $map['contract_class'] = 1; //目前只需展示租赁合同，购买合同对用户不可见仅用作逻辑处理
        $field = 'contract_no,renew_date,province,city,area,address,contract_id,status,exire_date,contract_class,expect_exire_date, is_buy_out';
        $data = $this->contract_model->getAll($map, $field, ['status' => 'ASC', 'add_time' => 'DESC'], $page, $pagesize);
        if (empty($data)) {
            return $this->jsonend(-1003, "暂无相关数据");
        }
        foreach ($data as $k => $v) {
            $data[$k]['status_name'] = $this->config->get('contract_status')[$v['status']];
            $renew_order = $this->renewOrderModel->getOne(['contract_id' => $v['contract_id'], 'pay_status' => 2, 'pay_time' => ['>', '1703088000']], 'renew_order_id', [], ['renew_order_id' => 'desc']);
            if (!empty($renew_order)) {
                $contract_additional = $this->contract_additional_model->getOne(['renew_order_id' => $renew_order['renew_order_id']]);
                if (empty($contract_additional)) {
                    $data[$k]['status_name'] = $this->config->get('contract_status')[3];
                    // 续费了没有记录
                    $data[$k]['is_sign'] = 2;
                    $data[$k]['renew_order_id'] = $renew_order['renew_order_id'];
                } else {
                    // 续费了有记录
                    $data[$k]['is_sign'] = 3;
                    $data[$k]['renew_order_id'] = $renew_order['renew_order_id'];
                }
            } else {
                // 没有续过费
                if ($data['status' == 6] || $data['status' == 7]) {
                    $data[$k]['is_sign'] = 3;
                } else {
                    $data[$k]['is_sign'] = 1;
                }
                $data[$k]['renew_order_id'] = '';
            }
            if ($v['is_buy_out'] == 1) {
                $data[$k]['exire_date'] = '/';
            } else {
                $data[$k]['exire_date'] = empty($v['exire_date']) ? '' : date('Y-m-d', $v['exire_date']);
            }
            //如果签约前如果设置了预期到期时间则显示
            if (empty($v['exire_date']) && !empty($v['expect_exire_date'])) {
                $data[$k]['exire_date'] = date('Y-m-d', $v['expect_exire_date']);
            }
        }
        return $this->jsonend(1000, "获取列表成功", $data);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 重新下发套餐
     * @description
     * @method POST
     * @url /User/Contract/againPackage
     * @param contract_id 必选 int 合同ID
     * @param equipment_id 必选 int 设备ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_againPackage()
    {
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1002, "缺少参数合同ID");
        }
        if (empty($this->parm['equipment_id'] ?? '')) {
            return $this->jsonend(-1002, "缺少参数设备ID");
        }
        $info = $this->ContractPackageRecordModel->getOne(['contract_id' => $this->parm['contract_id'], 'equipment_id' => $this->parm['equipment_id'], 'status' => 1]);
        if (empty($info)) {
            return $this->jsonend(-1000, "不存在未下发的套餐");
        }
        //查询设备编号
        $eq = $this->equipment_list_model->findEquipmentLists(['equipment_id' => $this->parm['equipment_id']], 'device_no,switch_machine,status');
        if (empty($eq)) {
            return $this->jsonend(-1000, "设备不存在");
        }
        if ($eq['status'] == 1) {
            return $this->jsonend(-1000, "设备已离线，请联网后再下发");
        }
        if ($eq['switch_machine'] == 0) {
            return $this->jsonend(-1000, "设备已关机，请开机后再下发");
        }
        //下发-绑定套餐
        $params = ['sn' => $eq['device_no'], 'working_mode' => $info['working_mode'], 'filter_element_max' => json_decode($info['filter_element_max'], true)];
        $path = '/House/Issue/bindingPackage';
        HttpService::Thrash($params, $path);
        sleepCoroutine(2000);
        //下发--数据同步
        $data_sync_params['sn'] = $eq['device_no'];
        $data_sync_params['used_days'] = $info['used_days'];
        $data_sync_params['used_traffic'] = $info['used_traffic'];
        $data_sync_params['remaining_days'] = $info['remaining_days'];
        $data_sync_params['remaining_traffic'] = $info['remaining_traffic'];
        $data_sync_path = '/House/Issue/data_sync';
        HttpService::Thrash($data_sync_params, $data_sync_path);
        $this->redis->del('device_heartbeat:' . $eq['device_no']); //删除redis心跳
        //请求心跳
        sleepCoroutine(2000);
        $state_params['sn'] = $eq['device_no'];
        $state_path = '/House/Issue/heartbeat';
        HttpService::Thrash($state_params, $state_path);
        //修改下发记录
        $res = $this->ContractPackageRecordModel->save(['id' => $info['id']], ['last_hand_time' => time()]);
        if ($res) {
            return $this->jsonend(1000, "请求发送成功,正在下发中");
        }
        return $this->jsonend(1000, "请求发送失败");
    }
}
