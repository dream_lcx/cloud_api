<?php


namespace app\Controllers\User;

use app\Services\Common\ConfigService;
use app\Services\Common\EmailService;
use app\Services\Common\HttpService;
use app\Services\Log\AsyncFile;
use app\Services\User\UserService;
use Firebase\JWT\JWT;
use Server\CoreBase\SwooleException;

/**
 * 报单
 * Class Reportorder
 * @package app\Controllers\User
 */
class Reportorder extends Base
{
    protected $user_model;
    protected $invite_model;
    protected $report_order_model;
    protected $userService;
    protected $admin_user_model;
    protected $op_user_model;
    protected $administrative_user_model;

    // 前置方式，加载模型
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->invite_model = $this->loader->model('InvitationModel', $this);
        $this->report_order_model = $this->loader->model('ReportOrderModel', $this);
        $this->userService = new UserService();
        $this->admin_user_model = $this->loader->model('AdminUserModel', $this);
        $this->op_user_model = $this->loader->model('OperationUserModel', $this);
        $this->administrative_user_model = $this->loader->model('AdministrativeUserModel', $this);
    }

    /**
     * showdoc
     * @catalog API文档/报单相关
     * @title 新增报单
     * @description
     * @url User/Reportorder/add
     * @method POST
     * @param
     * @return
     * @return_param
     * @remark
     * @author xg
     * @date 2021/7/6 14:44
     */
    public function http_add()
    {
        //检验参数合法性
        $check_result = $this->checkParam($this->parm);
        if ($check_result['code'] != 1000) {
            return $this->jsonend($check_result['code'], $check_result['msg']);
        }
        $pid_info = $this->user_model->getOne(['user_id' => $this->user_id], 'is_dealer,telphone');
        //自己给自己报单
        if($pid_info['telphone']==$this->parm['telphone']){
            return $this->jsonend(-1000, "不能给本人报单,请直接从商城进行下单");
        }
        //被报单客户是否已经存在,不存在新增客户
        $user_info = $this->user_model->getOne(['telphone' => $this->parm['telphone'], 'is_delete' => 0], 'user_id');
        $add_status = false;
        $add_order = [];
        $this->db->begin(function () use (&$add_status, $user_info,$pid_info,&$add_order) {
            if (!empty($user_info)) {
                $user_id = $user_info['user_id'];
            } else {
                $user_id = $this->addUser($this->parm);   //新增客户
                $add_param['user_id'] = $user_id;
                $add_param['pid'] = $this->user_id;
                $add_param['identity'] = $pid_info['is_dealer'] == 1 ? 1 : 0;
                $this->addInvitationRecord($add_param);//新增邀请记录
            }
            //新增报单记录
            $add_order['sn'] = 'RO' . date('YmdHis') . rand(10000, 99999);
            $add_order['source_user_id'] = $this->user_id;
            $add_order['user_id'] = $user_id;
            $add_order['username'] = $this->parm['username'] ?? '';
            $add_order['telphone'] = $this->parm['telphone'];
            $add_order['province_code'] = $this->parm['province_code'] ?? '';
            $add_order['city_code'] = $this->parm['city_code'] ?? '';
            $add_order['area_code'] = $this->parm['area_code'] ?? '';
            $add_order['province'] = $this->parm['province'] ?? '';
            $add_order['city'] = $this->parm['city'] ?? '';
            $add_order['area'] = $this->parm['area'] ?? '';
            $add_order['address'] = $this->parm['address'] ?? '';
            $add_order['remark'] = $this->parm['remark'] ?? '';
            $add_order['status'] = 1;
            $add_order['create_time'] = time();
            if(!empty($this->parm['province_code'])){
                $admin_info = $this->userService->getOwnArea($user_id, '', $this->parm['province_code'], $this->parm['city_code'], $this->parm['area_code'], $this->company);
                if (!empty($admin_info)) {
                    $add_order['a_id'] = $admin_info['a_id'];
                    $add_order['o_id'] = $admin_info['operation'];
                }
            }
            $this->report_order_model->add($add_order);
            $add_status = true;
        }, function ($e) {
            return $this->jsonend(-1000, "报单失败,请重试!" . $e->error);
        });
        if ($add_status) {
            //通知到后台
            $admin_notice_config = ConfigService::getTemplateConfig('user_appoint_success', 'admin', $this->company);
            if (!empty($admin_notice_config)) {
                //发邮件通知
                if (!empty($admin_notice_config['email']) && $admin_notice_config['email']['switch']) {
                    $content = '<h2>线下报单通知</h2><br/><div>报单编号:' . $add_order['sn'] . '</div><div>客户电话:' . $add_order['telphone'] . '</div><div>地址:' . $add_order['province'] . $add_order['city'] . $add_order['area'] . $add_order['address'] . '</div><div style="color:#808080">'.$add_order['remark'].'请尽快进入管理后台订单管理-线下报单进行派单处理!</div>';
                    //总后台
                    $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                    if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                        EmailService::sendEmailAsync($admin_email['notice_email'], $content, "线下报单通知", '', '', '', $this->company);
                    }
                    $userService = new UserService();
                    $a_info = $userService->getOwnArea($user_info['user_id'],'', $add_order['province_code'], $add_order['city_code'], $add_order['area_code'], 1);
                    if (!empty($a_info)) {
                        //对应运营中心
                        $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                        if (!empty($op_email) && !empty($op_email['notice_email'])) {
                            EmailService::sendEmailAsync($op_email['notice_email'], $content, "线下报单通知", '', '', '', $this->company);
                        }
                        //对应合伙人
                        $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                        if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                            EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "线下报单通知", '', '', '', $this->company);
                        }
                    }
                }
            }
            return $this->jsonend(1000, "报单成功");
        }
        return $this->jsonend(-1000, "报单失败");
    }

    /**
     * showdoc
     * @catalog API文档/报单相关
     * @title 获取报单记录
     * @description
     * @url User/Reportorder/record
     * @method POST
     * @param
     * @return
     * @return_param
     * @remark
     * @author xg
     * @date 2021/7/6 16:02
     */
    public function http_record()
    {
        $page = $this->parm['page'] ?? 1;
        $row = $this->parm['row'] ?? 10;
        $keywords = $this->parm['keywords'] ?? '';
        $where['source_user_id'] = $this->user_id;
        if (!empty($keywords)) {
            $where['username|telphone|address|sn'] = ['like', '%' . $keywords . '%'];
        }
        $result = $this->report_order_model->getAll($where, '*');
        $status = [1=>'待派单',2=>'已派单',3=>'已作废'];
        if(!empty($result)){
            foreach ($result as $k=>$v){
                $result[$k]['create_time'] = date('Y年m月d日 H:i',$v['create_time']);
                $result[$k]['status_desc'] = $status[$v['status']]??'--';
            }
        }
        $count = $this->report_order_model->count($where);
        $data = [
            'data' => $result,
            'page' => [
                'current_page' => $page,
                'row' => $row,
                'total' => $count,
                'total_page' => ceil($count / $row)
            ]
        ];
        return $this->jsonend(1000, '获取成功', $data);
    }

    /*
     * 新增客户
     */
    public function addUser($param)
    {
        //在boss系统用户中心注册
        $bossResult = HttpService::requestBossApi(['company_id' => $this->company, 'telphone' => $this->parm['telphone'], 'app_sn' => $this->config->get('app_sn'), 'client' => 1], '/api/User/register');
        if ($bossResult['code'] != 0) {//注册失败,直接抛出异常
            throw new SwooleException("用户中心注册失败" . $bossResult['msg']);
        }
        $bossTokenData = JWT::decode($bossResult['data']['token'], $this->config->get('token_key'), array('HS256'));
        $user_data['account'] = $bossTokenData->account;
        $user_data['uuid'] = $bossTokenData->uuid;
        $user_data['telphone'] = $this->parm['telphone'];
        $user_data['source'] = $this->user_id;
        $user_data['source_is_valid'] = 2;
        $user_data['source_is_default'] = 1;
        $user_data['username'] = $param['username'];
        $user_data['province_code'] = $param['province_code'] ?? '';
        $user_data['city_code'] = $param['city_code'] ?? '';
        $user_data['area_code'] = $param['area_code'] ?? '';
        $user_data['province'] = $param['province'] ?? '';
        $user_data['city'] = $param['city'] ?? '';
        $user_data['area'] = $param['area'] ?? '';
        $user_data['user_address'] = $param['address'] ?? '';
        return $this->user_model->add($user_data);
    }

    /*
     * 新增邀请记录
     */
    public function addInvitationRecord($param)
    {
        $add_data['user_id'] = $param['user_id'];
        $add_data['type'] = 1;
        $add_data['pid'] = $param['pid'];
        $add_data['identity'] = $param['identity'];
        $add_data['create_time'] = time();
        $add_data['status'] = 2;
        return $this->invite_model->add($add_data);
    }

    public function checkParam($param)
    {
        if (empty($param['telphone'])) {
            return returnResult(-1000, "请填写联系电话");
        }
        if (!isMobile($param['telphone'])) {
            return returnResult(-1000, "请填写正确的联系电话");
        }
        return returnResult(1000, "检验成功");
    }
}