<?php

namespace app\Controllers\User;

use app\Library\Alipay\aop\AopClient;
use app\Library\Alipay\aop\request\AlipayTradeCreateRequest;
use app\Services\Common\AdaPayService;
use app\Services\Common\DataService;
use app\Services\Common\EmailService;
use app\Services\Common\WxPayService;
use app\Services\Company\CompanyService;
use app\Services\Device\DeviceService;
use app\Services\Common\ConfigService;
use app\Services\Common\NotifiyService;
use app\Services\Common\CommonService;
use app\Services\User\UserService;
use app\Services\Stock\StockService;
use app\Wechat\WxPay;


/**
 * 订单模块API
 */
class Order extends Base
{

    protected $user_model;
    protected $order_model;
    protected $equip_model;
    protected $rent_package_model;
    protected $addr_model;
    protected $evaluate_model;
    protected $evaluate_label_model;
    protected $evaluate_to_label_model;
    protected $work_model;
    protected $order_invoice_model;
    protected $user_invoice_model;
    protected $order_logistics_model;
    protected $coupon_model;
    protected $equipment_lists_model;
    protected $bind_model;
    protected $finance_record_model;
    protected $equipment_model;
    protected $admin_user_model;
    protected $op_user_model;
    protected $administrative_user_model;
    private $user_center_model;
    protected $userService;
    protected $stockModel;
    protected $oaWorkersModel;
    protected $Achievement;
    protected $equipmentsPackageModel;


    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->order_model = $this->loader->model('OrderModel', $this);
        $this->equip_model = $this->loader->model('EquipmentModel', $this);
        $this->rent_package_model = $this->loader->model('RentalPackageModel', $this);
        $this->addr_model = $this->loader->model('UseraddressModel', $this);
        $this->evaluate_model = $this->loader->model('EvaluateModel', $this);
        $this->evaluate_label_model = $this->loader->model('EvaluateLabelModel', $this);
        $this->evaluate_to_label_model = $this->loader->model('EvaluateToLabelModel', $this);
        $this->work_model = $this->loader->model('WorkOrderModel', $this);
        $this->order_invoice_model = $this->loader->model('OrderInvoiceModel', $this);
        $this->user_invoice_model = $this->loader->model('UserinvoiceModel', $this);
        $this->order_logistics_model = $this->loader->model('OrderLogisticsModel', $this);
        $this->coupon_model = $this->loader->model('CouponModel', $this);
        $this->equipment_lists_model = $this->loader->model('EquipmentListsModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->finance_record_model = $this->loader->model('FinanceRecordModel', $this);
        $this->equipment_model = $this->loader->model('EquipmentModel', $this);
        $this->admin_user_model = $this->loader->model('AdminUserModel', $this);
        $this->op_user_model = $this->loader->model('OperationUserModel', $this);
        $this->administrative_user_model = $this->loader->model('AdministrativeUserModel', $this);
        $this->user_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->stockModel = $this->loader->model('StockModel', $this);
        $this->oaWorkersModel = $this->loader->model('OaWorkersModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->equipmentsPackageModel = $this->loader->model('EquipmentsPackageModel',$this);
        $this->userService = new UserService();
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 下单
     * @description 租赁或者购买下单
     * @method POST
     * @url User/Order/addOrder
     * @param equipment_num 可选 int 设备数量，默认1
     * @param order_type 可选 int 订单类型1租赁订单 2购买订单 3积分订单，默认1
     * @param is_invoice 可选 int 是否开发票 1否 2是，默认1
     * @param pay_way 可选 int 支付方式1微信，默认1
     * @param address_id 必选 int 地址ID
     * @param equipments_id 必选 int 产品ID
     * @param equipments_rental_package_id 可选 int 套餐ID,如果order_type=1必传
     * @param remarks 可选 string 订单备注
     * @param invoice_type 可选 int 发票类型1纸质发票 2电子发票
     * @param invoice_id 可选 int 发票ID
     * @param invoice_recevice_tel 可选 string 收票人电话
     * @param invoice_recevice_email 可选 string 收票人邮箱
     * @param invoice_recevice_name 可选 string 收票人姓名
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_addOrder()
    {
        /**
         * 扎帐 20230901
         * 1.开启扎帐
         * 2.支付金额大于0
         * 3.当前在扎帐日期期间
         */
        $date = date('d', time());
        if ($this->config['system_clearing_swtich'] == true && $this->config['system_clearing_date'] >= $date) {
            return $this->jsonend(-1000, $this->config['system_clearing_text']);
        }
        $this->parm['equipment_num'] = empty($this->parm['equipment_num'] ?? '') ? 1 : $this->parm['equipment_num'];
        $this->parm['order_type'] = empty($this->parm['order_type'] ?? '') ? 1 : $this->parm['order_type'];
        $this->parm['is_invoice'] = empty($this->parm['is_invoice'] ?? '') ? 1 : $this->parm['is_invoice'];
        $this->parm['pay_way'] = empty($this->parm['pay_way'] ?? '') ? 1 : $this->parm['pay_way'];
        //查询用户信息
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_belong_to_promoter,user_belong_to_channel,user_id,source');
        if (empty($user_info)) {
            return $this->jsonend(-1001, "用户信息不存在");
        }
        if (empty($this->parm['equipments_id'] ?? '')) {
            return $this->jsonend(-1001, "你还未选择任何产品");
        }
        //非购买产品必须选择套餐
        if ($this->parm['order_type'] != 2 && empty($this->parm['equipments_rental_package_id'] ?? '')) {
            return $this->jsonend(-1001, "你还未选择任何套餐");
        }
        if (empty($this->parm['address_id'] ?? '')) {
            return $this->jsonend(-1001, "你还未选择任何地址,请先选择地址");
        }
        //查询设备分类信息--产品
        $product_info = $this->equip_model->getDetail(array('equipments_id' => $this->parm['equipments_id']), '*');
        if (empty($product_info)) {
            return $this->jsonend(-1101, '该产品不存在');
        }
        if ($product_info['is_shelves'] == 2) {
            return $this->jsonend(-1102, '该产品已下架,请选择其他产品');
        }
        if ($this->parm['order_type'] == 2 && $product_info['equipments_type'] != 2) {
            return $this->jsonend(-1102, '该产品非购买类产品');
        }
//        if ($product_info['storage'] < $this->parm['equipment_num']) {
//            return $this->jsonend(-1103, '该产品库存不足,仅剩余' . $product_info['storage'] . '个,您可以选择其他商品');
//        }
        //判断地址是否在服务区内
        if (!$this->isInService($this->parm['address_id'], $this->parm['equipments_id'])) {
            return $this->jsonend(-1107, "您选择的地址超出服务范围,请重新选择");
        }

        // 判断是否使用优惠券
        $is_coupon = 0;
        $coupon_money = 0;
        $coupon_id = '';
        if (!empty($this->parm['coupon_id'])) {
            // 判断优惠券是否可用
            $join = [
                ['coupon_cate cc', 'cc.id = c.cid', 'left'], // 优惠券分类表--优惠券表
            ];
            $field = 'c.status,c.send_time,c.code,cc.money,cc.use_start_time,cc.use_end_time,cc.time_type,cc.use_days';
            $where['c.coupon_id'] = $this->parm['coupon_id'];
            $where['c.status'] = 1;
            $where['c.uid'] = $this->user_id;
            $where['cc.status'] = 1;
            $coupon_info = $this->coupon_model->getJoinOne($where, $join, $field);
            unset($where);
            if ($coupon_info) {
                if ($coupon_info['time_type'] == 2) {  // 固定时间段
                    if ($coupon_info['use_end_time'] >= time()) {
                        $is_coupon = 1;
                        $coupon_id = $this->parm['coupon_id'];
                        // 优惠券金额
                        $coupon_money = formatMoney($coupon_info['money']);
                        if($product_info['equipments_line'] == 3){
                            if($coupon_info['money'] >= 4800){
                                $coupon_money = $product_info['equipments_purchase_money'];
                            }
                            if($coupon_info['money'] < 4800){
                                $unit = 48 - $coupon_money;
                                $coupon_money = $product_info['equipments_purchase_money'] - $unit;
                                dump($coupon_money);
                            }
                        }

                        $order_data['coupon_id'] = $this->parm['coupon_id'];
                        $order_data['coupon_money'] = $coupon_money;
                    }
                } elseif ($coupon_info['time_type'] == 1) {  // 领取后多少天
                    if ($coupon_info['send_time'] + 86400 * $coupon_info['use_days'] >= time()) {
                        $is_coupon = 1;
                        $coupon_id = $this->parm['coupon_id'];
                        // 优惠券金额
                        $coupon_money = formatMoney($coupon_info['money']);
                        if($product_info['equipments_line'] == 3){
                            if($coupon_info['money'] >= 4800){
                                $coupon_money = $product_info['equipments_purchase_money'];
                            }
                            if($coupon_info['money'] < 4800){
                                $unit = 48 - $coupon_money;
                                $coupon_money = $product_info['equipments_purchase_money'] - $unit;
                                dump($coupon_money);
                            }
                        }
                        $order_data['coupon_id'] = $this->parm['coupon_id'];
                        $order_data['coupon_money'] = $coupon_money;
                    }
                }
            }
        }

        //查询套餐信息
        if ($this->parm['order_type'] != 2) {

            $package_info = $this->rent_package_model->getOne(array('package_id' => $this->parm['equipments_rental_package_id'], 'status' => 1), '*');

            if (empty($package_info)) {
                return $this->jsonend(-1104, "你选择的套餐不存在");
            }
            $order_data['package_id'] = $package_info['package_id'];
            $order_data['package_name'] = $package_info['package_name'];
            $order_data['package_mode'] = $package_info['mode'];
            $order_data['package_value'] = $package_info['num'] ?:$package_info['cycle'];// 值为空 取周期的值
            $order_data['package_money'] = $package_info['total_money'];
            $order_data['package_cycle'] = $package_info['cycle'];
            $order_data['package_renew_money'] = $package_info['renew_money'];
            $order_data['equipment_money'] = $package_info['total_money'];
        } else {
            // 购买套餐默认 20年？
            $order_data['package_id'] = 0;
            $order_data['package_name'] = '购买套餐';
            $order_data['package_mode'] = 1;
            $order_data['package_value'] = 12 * 2;
            $order_data['package_money'] = $product_info['equipments_purchase_money'];
            $order_data['package_cycle'] = 12 * 2;
            $order_data['equipment_money'] = $product_info['equipments_purchase_money'];
        }
        $order_data['company_id'] = $product_info['company_id'];
        //查询地址信息
        $addr_info = $this->addr_model->getOne(array('address_id' => $this->parm['address_id']), '*');
        $order_data['order_no'] = CommonService::createSn('cloud_order_no');
        $order_data['user_id'] = $this->user_id;
        $order_data['order_type'] = $this->parm['order_type'];
        $order_data['business_type_id'] = 3;
        $order_data['equipments_id'] = $this->parm['equipments_id'];
        $order_data['house_type'] = $addr_info['house_type'];
        $order_data['province'] = $addr_info['provice'];
        $order_data['city'] = $addr_info['city'];
        $order_data['area'] = $addr_info['area'];
        $order_data['province_code'] = $addr_info['provice_code'];
        $order_data['city_code'] = $addr_info['city_code'];
        $order_data['area_code'] = $addr_info['area_code'];
        $order_data['address'] = $addr_info['address'];
        //行政中心
        $admin = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $addr_info['provice_code'], $addr_info['city_code'], $addr_info['area_code'], $this->company);
        if (empty($admin)) {
            return $this->jsonend(-1107, "您选择的地址暂未提供服务,请重新选择");
        }
        $order_data['administrative_id'] = $admin['a_id'];
        $order_data['operation_id'] = $admin['operation'];
        $order_data['contact'] = $addr_info['contact'];
        $order_data['contact_tel'] = $addr_info['contact_tel'];
        $order_data['lng'] = $addr_info['lng'];
        $order_data['lat'] = $addr_info['lat'];
        $order_data['order_status'] = 1;
        $order_data['create_time'] = time();
        $order_data['equipment_num'] = $this->parm['equipment_num'];
        $order_data['equipment_total_money'] = $order_data['equipment_money'] * $order_data['equipment_num'];
        $order_data['single_deposit'] = $product_info['deposit'];
        $order_data['total_deposit'] = $product_info['deposit'] * $this->parm['equipment_num'];
        $order_data['install_cost'] = $product_info['install_cost'] ? $product_info['install_cost'] * $this->parm['equipment_num'] : 0;
        $order_data['order_total_money'] = $order_data['equipment_total_money'] + $order_data['total_deposit'] + $order_data['install_cost'];
        $order_data['order_actual_money'] = $order_data['equipment_total_money'] + $order_data['total_deposit'] + $order_data['install_cost'] - $coupon_money;
        $order_data['is_invoice'] = $this->parm['is_invoice'];
        $order_data['remarks'] = empty($this->parm['remarks'] ?? '') ? '' : $this->parm['remarks'];
//        if(empty($order_data['remarks']) && $product_info['equipments_line'] == 3 && !empty($this->parm['coupon_id'])){
//
//            $order_data['remarks'] = '使用优惠券:'.$coupon_info['code'].' 抵扣了一次';
//            if($coupon_money == $product_info['equipments_purchase_money']){
//                $order_data['remarks'] = '使用优惠券:'.$coupon_info['code'].' 兑换了一次';
//            }
//        }
        $order_data['order_belong_to_client'] = $this->request->client;
        $order_data['order_belong_to_promoter'] = $user_info['user_belong_to_promoter'];
        $order_data['order_belong_to_channel'] = $user_info['user_belong_to_channel'];
        $invoice_data = array();
        if ($this->parm['is_invoice'] == 2) {
            if (empty($this->parm['invoice_type'] ?? '')) {
                return $this->jsonend(-1105, "请选择发票类型");
            }
            if (empty($this->parm['invoice_id'] ?? '')) {
                return $this->jsonend(-1001, "请选择填写发票内容");
            }
            if (empty($this->parm['invoice_recevice_tel'] ?? '')) {
                return $this->jsonend(-1001, "请选择填写收票人电话");
            }
            if (!isMobile($this->parm['invoice_recevice_tel'])) {
                return $this->jsonend(-1001, "收票人电话不合法");
            }
            //发票抬头信息
            $invoice_info = $this->user_invoice_model->getOne(array('invoice_id' => $this->parm['invoice_id']), '*');
            if (empty($invoice_info)) {
                return $this->jsonend(-1001, "发票信息不正确");
            }
            $invoice_data['invoice_type'] = $this->parm['invoice_type'];
            $invoice_data['name'] = $invoice_info['name'];
            $invoice_data['tel'] = $invoice_info['tel'];
            $invoice_data['tax_no'] = $invoice_info['tax_no'];
            $invoice_data['address'] = $invoice_info['address'];
            $invoice_data['bank'] = $invoice_info['bank'];
            $invoice_data['bank_account'] = $invoice_info['bank_account'];
            $invoice_data['invoice_title_type'] = $invoice_info['type'];
            if (!empty($this->parm['invoice_recevice_name'] ?? '')) {
                $invoice_data['invoice_recevice_name'] = $this->parm['invoice_recevice_name'];
            }
            if (!empty($this->parm['invoice_recevice_email'] ?? '')) {
                if (!isEmail($this->parm['invoice_recevice_email'])) {
                    return $this->jsonend(-1002, "收票人邮箱不合法");
                }
                $invoice_data['invoice_recevice_email'] = $this->parm['invoice_recevice_email'];
            }
            $invoice_data['invoice_recevice_tel'] = $this->parm['invoice_recevice_tel'];
            $invoice_data['create_time'] = time();
            $invoice_data['money'] = intval($order_data['order_actual_money'] * 100);
            $invoice_data['user_id'] = $this->user_id;
        }

        $add_status = false;
        $order_id = '';
        $this->db->begin(function () use ($order_data, &$add_status, &$order_id, &$jsapi, &$invoice_data, &$is_coupon, &$coupon_id, &$product_info) {
            $order_id = $this->order_model->add($order_data);
            $this->addOrderLog($order_id, $order_data['order_status'], "用户【用户ID:" . $this->user_id . "】已下单,请尽快支付");
            if ($this->parm['is_invoice'] == 2) {
                $invoice_data['order_id'] = $order_id;
                $this->order_invoice_model->add($invoice_data);
            }
            // 判断是否使用优惠券
            if ($is_coupon) {
                $where['coupon_id'] = $coupon_id;
                $data['status'] = 2;
                $data['order_id'] = $order_id;
                $data['use_type'] = 2;
                $data['use_time'] = time();
                $this->coupon_model->edit($where, $data);
                unset($where);
            }

            //华信模式下判断购买下单根据推荐人减去经销商或者城市合伙人的库存
            if (in_array($this->company, $this->config->get('logic_mode.huaxin'))) {
                if ($product_info['equipments_type'] == 2) {//(除了已取消和已作废订单)并满足是购买产品
                    //先判断用户是是否有经销商，没得则减去所属合伙人的库存
                    $res = $this->userService->getUserBelongTo($this->user_id, $this->company);
                    if (empty($res)) {
                        $this->jsonend(-1000, '用户推荐人信息错误');
                    }
                    //先判断是否有库存
                    $where['role'] = $res['role'];
                    $where['uid'] = $res['uid'];
                    $where['equipments_id'] = $product_info['equipments_id'];
                    $where['company_id'] = $this->company;
                    $stock_info = $this->stockModel->getOne($where, 'stock_id,surplus');

                    /* if (empty($stock_info) || $stock_info['surplus'] < $this->parm['equipment_num']) {
                         $this->jsonend(-1000, '暂无库存或库存不足，请先添加库存');
                     }*/
                    $surplus = !empty($stock_info) ? $stock_info['surplus'] : 0;
                    //减去库存
                    StockService::changeStock($res['role'], $res['uid'], $product_info['equipments_id'], $this->parm['equipment_num'], $this->company);
                    //新增库存记录
                    StockService::addStockRecord($res['role'], $res['uid'], $product_info['equipments_id'], $surplus, $this->parm['equipment_num'], 4, 1, $res['role'], $res['uid'], $this->company, '用户下单购买');
                }
            }

            $add_status = true;
        });
        if ($add_status) {
            $order_data['order_id'] = $order_id;
            $product_data['equipments_name'] = $product_info['equipments_name'];
            $product_data['equipments_money'] = $product_info['equipments_money'];
            $product_data['equipments_renew_money'] = $product_info['equipments_renew_money'];
            $product_data['equipments_purchase_money'] = $product_info['equipments_purchase_money'];
            $product_data['brief_desc'] = $product_info['brief_desc'];
            $product_data['equipments_type'] = $product_info['equipments_type'];
            $product_data['main_pic'] = UploadImgPath($product_info['main_pic']);
            $order_data['product_data'] = $product_data;
            $this->jsonend(1000, '下单成功', $order_data);
        }
        $this->jsonend(-1000, '下单失败');
    }


    //4对公转账回调
    public function transferOrder($order_id)
    {
        if (empty($order_id)) {
            return false;
        }

        //查询订单
        $order = $this->order_model->getOne(['order_id' => $order_id], '*');

        if (empty($order)) {
            return false;
        }
        $status = false;
        $this->db->begin(function () use ($order, &$status) {
            //修改订单状态,如果走发货流程 order_status=2 待发货 反之 order_status=3 待安装
            $order_status = 3;//待安装
            $deliver_way = 0;//未发货
            if ($order['order_type'] == 2) {//购买订单
                $order_status = 2;//待发货
                $deliver_way = 2;//不需物流
            }
            //如果已经是已安装不修改订单状态
            if ($order['order_status'] == 4) {
                $order_status = 4;
            }
            $this->order_model->save(['order_id' => $order['order_id']], ['order_status' => $order_status, 'pay_status' => 2, 'pay_time' => time(), 'wx_callback_num' => $order['wx_callback_num'], 'deliver_way' => $deliver_way]);
            //添加订单日志
            $this->addOrderLog($order['order_id'], $order_status, "您的订单已支付成功,我们将尽快进行处理,订单编号:" . $order['order_no'], 1);
            //添加用户与行政中心关系
            $this->userService->addUserCenter($order['user_id'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
            //添加物流信息
            $this->addOrderLogistics($order['order_id'], 1, "您的订单开始处理", $order['user_id']);
            //新增资金记录
            $finance_record['type'] = 1;
            $finance_record['order_id'] = $order['order_id'];
            $finance_record['user_id'] = $order['user_id'];
            $finance_record['money'] = formatMoney($order['order_actual_money'], 1);
            $finance_record['create_time'] = time();
            $finance_record['is_online_pay'] = 2;//线下支付
            $finance_record['pay_way'] = 4;
            $finance_record['callback_num'] = '';
            $finance_record['payment_method'] = 1;
            $finance_record['payment_uid'] = $order['user_id'];
            $finance_record['o_id'] = $order['operation_id'];
            $finance_record['a_id'] = $order['administrative_id'];
            $this->finance_record_model->add($finance_record);

            $status = true;
        });

        if ($status) {
            $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'openid,telphone,user_id,source');
            $eq_info = $this->equipment_model->getDetail(array('equipments_id' => $order['equipments_id']), 'equipments_name,model');
            if (empty($user_info) || empty($eq_info)) {
                return $this->response->end('fail');
            }
            $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order['province_code'], $order['city_code'], $order['area_code'], $this->company);
            //发邮件通知
            if (1 == 0) {//暂时关闭此功能
                $order_time = date('Y-m-d H:i:s', $order['create_time']);
                $opertaor = $this->config->get('order_opertaor')[$order['opertaor']];//订单来源
                $content = '<h2>新订单通知</h2><br/><div>订单编号:' . $order['order_no'] . '</div><div>订单来源:' . $opertaor . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>商品名称:' . $eq_info['equipments_name'] . '(数量:' . $order['equipment_num'] . ')</div><div>订单金额:' . $order['order_actual_money'] . '元</div><div>下单时间:' . $order_time . '</div><div style="color:#808080">客户已完成支付,请进入管理后台查看详情!</div>';
                //总后台
                $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                    EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                }
                if (!empty($a_info)) {
                    //对应运营中心
                    $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                    if (!empty($op_email) && !empty($op_email['notice_email'])) {
                        EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                    }
                    //对应合伙人
                    $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                    if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                        EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                    }
                }
            }

            return true;
        }
        return false;

    }


    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 订单支付
     * @description
     * @method POST
     * @url User/Order/pay
     * @param order_id 必选 int 订单ID
     * @param is_paying_agent 可选 int 是否代付,1:是2:否
     * @param paying_agent_deduction_account_balance 可选 int 代付账户抵扣金额
     * @param paying_agent_role 可选 int 代付人角色0:否,1:用户,2:经销商,3:工程
     * @param pay_way 可选 int 支付方式1:微信支付,默认1, 2:支付宝支付
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_pay()
    {
        /**
         * 扎帐 20230901
         * 1.开启扎帐
         * 2.支付金额大于0
         * 3.当前在扎帐日期期间
         */
        $date = date('d', time());
        if ($this->config['system_clearing_swtich'] == true && $this->config['system_clearing_date'] >= $date) {
            return $this->jsonend(-1000, $this->config['system_clearing_text']);
        }
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "请选择订单");
        }
        if (empty($this->parm['pay_way'] ?? '')) {
            return $this->jsonend(-1001, "请选择支付方式");
        }
        if (!in_array($this->parm['pay_way'], [1, 2, 3, 4, 5])) {
            return $this->jsonend(-1103, "支付方式错误");
        }

        $user_order_id = $this->parm['order_id'];
        $redis_lock = $this->redis->get('user_order_pay' . $user_order_id);
        if ($redis_lock) {
            return $this->jsonend(-1000, '请勿频繁操作,10s后重试');
        }
        $this->redis->set('user_order_pay' . $user_order_id, $user_order_id, 10);
        //查询订单
        $order_info = $this->order_model->getOne(array('order_id' => $this->parm['order_id']), '*');
        if (empty($order_info)) {
            return $this->jsonend(-1101, "订单不存在");
        }
        if ($order_info['order_status'] != 1 && $order_info['pay_status'] == 2) {
            return $this->jsonend(-1102, "该订单已经支付过,不能重复支付");
        }

        //用户信息
        $paying_agent_user_openid = $this->parm['paying_agent_user_openid'] ?? '';//代付人用户OPENID
        $user_info = $this->user_model->getOne(array('user_id' => $this->user_id), 'openid,ali_uid');
        if ($this->parm['pay_way'] == 1) {//微信
            if (empty($user_info) || empty($user_info['openid'])) {
                return $this->jsonend(-1103, "用户信息错误");
            }
            $paying_agent_user_openid = $this->parm['paying_agent_user_openid'] ?? $user_info['openid'];//代付人用户OPENID
        } else if ($this->parm['pay_way'] == 2) {//支付宝
            if (empty($user_info) || empty($user_info['ali_uid'])) {
                return $this->jsonend(-1103, "用户信息错误");
            }
            $paying_agent_user_openid = $this->parm['paying_agent_user_openid'] ?? $user_info['ali_uid'];//代付人用户OPENID
            $order_info['buyer_id'] = $user_info['ali_uid'];
        } else if ($this->parm['pay_way'] == 4) {//对公转账
            if (empty($user_info)) {
                return $this->jsonend(-1103, "用户信息错误");
            }
        }
        $is_paying_agent = $this->parm['is_paying_agent'] ?? 2;//是否代付 1:是 2:否
        $paying_agent_deduction_account_balance = $this->parm['paying_agent_deduction_account_balance'] ?? 0;//代付账户余额抵扣金额
        $paying_agent_user_id = $this->user_id;//代付人用户ID
        $paying_agent_role = $this->parm['paying_agent_role'] ?? 0;//代付人角色 1:用户 2:经销商 3:工程,目前只处理经销商代付
        //代付-判断余额
        if ($is_paying_agent == 1 && $paying_agent_deduction_account_balance > 0) {
            if ($paying_agent_role == 2) {//经销商代付
                $worker_info = $this->oaWorkersModel->getOne(['user_id' => $paying_agent_user_id, 'is_delete' => 0], 'cash_available');
                if ($worker_info['cash_available'] < $paying_agent_deduction_account_balance) {
                    return $this->jsonend(-1103, "账户余额不足");
                }
            } else {
                return $this->jsonend(-1103, "暂不支持其他角色代付");
            }
            if ($paying_agent_deduction_account_balance > $order_info['order_actual_money']) {
                return $this->jsonend(-1102, "最多可抵扣" . $order_info['order_actual_money'] . '元');
            }

        }
        $add_status = false;
        $jsapi = '';
        $order_actual_money = $order_info['order_actual_money'];
        $this->db->begin(function () use (&$add_status, &$jsapi, &$order_actual_money, &$order_info, $user_info, $is_paying_agent, $paying_agent_deduction_account_balance, $paying_agent_user_id, $paying_agent_user_openid, $paying_agent_role) {
            $order_data['pay_way'] = $this->parm['pay_way'];
            $order_data['pay_mode'] = $this->pay_config['mode'] ?? 1;
            $order_data['pay_mchid'] = $this->pay_config['mode'] == 2 ? $this->pay_config['sub_mch_id'] : $this->wx_config['mchid'];
            $order_data['is_paying_agent'] = $is_paying_agent;
            $order_data['paying_agent_role'] = $paying_agent_role;
            $order_data['paying_agent_user_id'] = $paying_agent_user_id;
            $order_data['paying_agent_deduction_account_balance'] = $paying_agent_deduction_account_balance;
            $order_data['paying_agent_user_openid'] = $paying_agent_user_openid;
            if ($paying_agent_deduction_account_balance > 0) {
                $order_data['order_actual_money'] = $order_info['order_actual_money'] - $paying_agent_deduction_account_balance;
                $order_actual_money = $order_data['order_actual_money'];
            }
            $this->order_model->save(array('order_id' => $this->parm['order_id']), $order_data);
            if ($order_actual_money == 0) {
                $order_info['pay_way'] = $this->parm['pay_way'];
                $order_info['is_paying_agent'] = $order_data['is_paying_agent'];
                $order_info['paying_agent_role'] = $order_data['paying_agent_role'];
                $order_info['paying_agent_user_id'] = $order_data['paying_agent_user_id'];
                $order_info['paying_agent_deduction_account_balance'] = $order_data['paying_agent_deduction_account_balance'];
                $jsapi = $this->rentOrderCallback($order_info);
            } else {
                //调起微信支付
                if ($this->parm['pay_way'] == 1) {
                    $pay_data['order_sn'] = $order_info['order_no'];
                    $pay_data['money'] = $order_info['order_actual_money'];
                    $wx_pay_data['money'] = $pay_data['money'];
                    $pay_data['app_id'] = $this->wx_config['adapay_app_id']; // 聚合支付ID
                    //三方微信支付
                    // 0-100分的随机减扣，避免产生大量相同金额的支付订单
                    $deduction = $this->redis->get('user_order_deduction:' . $order_info['order_id']);
                    if (!$deduction) {
                        // 0-100分的随机减扣，避免产生大量相同金额的支付订单
                        $deduction = mt_rand(1, 99);
                        $this->redis->set('user_order_deduction:' . $order_info['order_id'], $deduction, 3600 * 24);
                    }
                    $pay_data['money'] = ($pay_data['money'] * 100 - $deduction) / 100.0;
                    $notify_url = $this->config->get('callback_domain_name') . '/User/Callback/rentAdaPayOrder';
                    //$notify_url = 'http://runquan.jiuque.cc/User/Callback/rentAdaPayOrder';
                    if ($this->dev_mode == 1) {
                        $pay_data['money'] = '0.02';
                        $notify_url = $this->config->get('debug_config.callback_domain_name') . '/User/Callback/rentAdaPayOrder';
                    }

                    $result = AdaPayService::pay($pay_data, $notify_url, $user_info);
                    if ($result['code'] == -1000) {
                        $pay_data['money'] = $wx_pay_data['money'];
                        dump('调用微信支付');
                        // 聚合支付失败时，调用微信支付
                        $notify_url = $this->config->get('callback_domain_name') . '/User/Callback/rentOrder';
                        if ($this->dev_mode == 1) {
                            $pay_data['money'] = 1;
                            $notify_url = $this->config->get('debug_config.callback_domain_name') . '/User/Callback/rentOrder';
                        }
                        $jsapi = WxPayService::pay($this->wx_config, $user_info['openid'], $pay_data, $notify_url, $this->pay_config['sub_mch_id'] ?? '', $this->pay_config['mode'] ?? 1);
                    } else {
                        dump('聚合支付');
                        $jsapi = $result['data'];
                    }
                    $code = 1000;

                } else if ($this->parm['pay_way'] == 2) {//支付宝支付
                    $response = $this->aliPay($order_info, 'User/Callback/ali_rentOrder');
                    $resultCode = $response->code;
                    if (!empty($resultCode) && $resultCode == 10000) {
                        echo "成功";
                        $responseData = [];
                        $responseData['order_id'] = $this->parm['order_id'];
                        $responseData['out_trade_no'] = $response->out_trade_no;
                        $responseData['trade_no'] = $response->trade_no;

                        return $this->jsonend(1000, '操作成功', $responseData);
                    } else {
                        echo "失败";
                        return $this->jsonend(-1000, '支付失败');
                    }
                } else if ($this->parm['pay_way'] == 3) {//百度小程序支付
                    $jsapi = $this->baiduPay($order_info);

                } else if ($this->parm['pay_way'] == 4 || $this->parm['pay_way'] == 5) {//对公转账或者现金支付
                    $order_info['pay_way'] = $this->parm['pay_way'];
                    $order_info['is_paying_agent'] = $order_data['is_paying_agent'];
                    $order_info['paying_agent_role'] = $order_data['paying_agent_role'];
                    $order_info['paying_agent_user_id'] = $order_data['paying_agent_user_id'];
                    $order_info['paying_agent_deduction_account_balance'] = $order_data['paying_agent_deduction_account_balance'];
                    $order_info['order_actual_money'] = $order_data['order_actual_money'];
                    $result = $this->rentOrderCallback($order_info);
                    //$result = $this->transferOrder($order_info['order_id']);
                    if ($result) {
                        $add_status = true;
                    }
                }
            }

            $add_status = true;
        });
        if ($add_status) {
            $order_data['money'] = $order_actual_money;
            $order_data['order_id'] = $this->parm['order_id'];
            $order_data['jsapi'] = $jsapi;
            return $this->jsonend(1000, '操作成功', $order_data);
        }
        return $this->jsonend(-1000, '操作失败');
    }

    //支付金额为0时回调
    public function rentOrderCallback($order)
    {

        if (empty($order)) {
            return false;
        }
        if ($order['pay_status'] == 2) {
            return false;
        }
        $order['wx_callback_num'] = '';
        $total_fee = formatMoney($order['order_actual_money'], 1);
        //修改订单状态,如果走发货流程 order_status=2 待发货 反之 order_status=3 待安装
        $order_status = 3;
        $deliver_way = 0;
        if ($order['order_type'] == 2) {
            $order_status = 2;
            $deliver_way = 2;
        }
        //如果已经是已安装不修改订单状态
        if ($order['order_status'] == 4) {
            $order_status = 4;
        }
        $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,realname,openid,telphone,user_id,source');
        if (!empty($user_info['realname'])) {
            $user_info['username'] = $user_info['realname'];
        }
        $this->order_model->save(['order_id' => $order['order_id']], ['order_status' => $order_status, 'pay_status' => 2, 'pay_time' => time(), 'wx_callback_num' => $order['wx_callback_num'], 'deliver_way' => $deliver_way]);
        //添加订单日志
        $this->addOrderLog($order['order_id'], $order_status, "您的订单已支付成功,我们将尽快进行处理,订单编号:" . $order['order_no'], 1);
        //添加用户与行政中心关系
        $this->userService->addUserCenter($order['user_id'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
        //添加物流信息
        $this->addOrderLogistics($order['order_id'], 1, "您的订单开始处理", $order['user_id']);
        $user_id = $order['user_id'];
        $payment_method = $order['is_paying_agent'] == 1 ? 4 : 1;
        //如果是代付且金额大于0,更新代付人账户余额
        if (!empty($order['paying_agent_deduction_account_balance']) && $order['paying_agent_deduction_account_balance'] > 0) {
            //扣取账户
            $this->Achievement->table = 'oa_workers';
            $workers = $this->Achievement->findData(['user_id' => $order['paying_agent_user_id']], 'cash_available,workers_id');
            $cash_available = $workers['cash_available'] - $order['paying_agent_deduction_account_balance'];
            $this->Achievement->updateData(['user_id' => $order['paying_agent_user_id']], ['cash_available' => $cash_available]);
            //写入资金变动记录
            $funds_record = [
                'user_id' => $workers['workers_id'],
                'user_type' => 1,
                'source' => 6,
                'account_type' => 1,
                'order_id' => $order['order_id'],
                'money' => $order['paying_agent_deduction_account_balance'],
                'note' => '订单单号:' . $order['order_no'] . ',代付发起人:' . $user_info['username'] . '(联系电话:' . $user_info['telphone'] . ')',
                'add_time' => time(),
            ];
            $this->Achievement->table = 'funds_record';
            $this->Achievement->insertData($funds_record);
            $user_id = $order['paying_agent_user_id'];
        }
        //新增资金记录
        $finance_record['type'] = 1;
        $finance_record['order_id'] = $order['order_id'];
        $finance_record['user_id'] = $order['user_id'];
        $finance_record['money'] = $total_fee;
        $finance_record['total_money'] = formatMoney($order['order_total_money'], 1);
        $finance_record['coupon_money'] = formatMoney($order['coupon_money'], 1);
        $finance_record['deduction_account_balance'] = formatMoney($order['paying_agent_deduction_account_balance'], 1);
        $finance_record['create_time'] = time();
        $finance_record['is_online_pay'] = 1;
        //订单表支付方式定义与资金表不一致,需要处理
        $pay_way = $order['pay_way'];
        if ($order['pay_way'] == 3) {
            $pay_way = 5;
        } else if ($order['pay_way'] == 5) {
            $pay_way = 3;
        }
        $finance_record['pay_way'] = $pay_way;
        $finance_record['callback_num'] = $order['wx_callback_num'];
        $finance_record['payment_method'] = $payment_method;
        $finance_record['payment_uid'] = $user_id;
        $finance_record['o_id'] = $order['operation_id'];
        $finance_record['a_id'] = $order['administrative_id'];
        $finance_record['company_id'] = $order['company_id'];
        $this->finance_record_model->add($finance_record);
        $status = true;
        if ($status) {
            $order_time = date('Y-m-d H:i:s', $order['create_time']);
            $eq_info = $this->equipment_model->getDetail(array('equipments_id' => $order['equipments_id']), 'equipments_name,model');
            //通知到用户
            $user_notice_config = ConfigService::getTemplateConfig('user_new_order', 'user', $order['company_id']);
            if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                $template_id = $user_notice_config['template']['wx_template_id'];
                $mp_template_id = $user_notice_config['template']['mp_template_id'];
                //数据组装
                $pay_way = '微信支付';

                $package_mode = '时长模式';
                $package = $package_mode . '(' . $order['package_value'] . '天)';
                if ($order['package_mode'] == 2) {
                    $package_mode = '流量模式';
                    $package = $package_mode . '(' . $order['package_value'] . 'ML)';
                }
                //通知数据组装
                $template_data = [
                    'keyword1' => ['value' => $order['order_no']],
                    'keyword2' => ['value' => $eq_info['equipments_name']],
                    'keyword3' => ['value' => $order['equipment_num']],
                    'keyword4' => ['value' => $package],
                    'keyword5' => ['value' => $order['order_actual_money']],
                    'keyword6' => ['value' => $pay_way],
                    'keyword7' => ['value' => $order_time],
                    'keyword8' => ['value' => '您的订单已经支付成功，感谢您对我们的的支持。若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id'])]
                ];

                $template_tel = $user_info['telphone'];
                $template_content = '您的订单(订单编号:' . $order['order_no'] . ',商品名称:' . $eq_info['equipments_name'] . ',数量:' . $order['equipment_num'] . ')已完成支付，金额为' . $order['order_actual_money'] . '元。打开微信进入' . $this->wx_config['app_wx_name'] . '可查看支付方式、下单时间等支付详情';
                $template_url = 'pages/user/myOrderDetail/myOrderDetail?order_id=' . $order['order_id'];
                $weapp_template_keyword = '';
                $mp_template_data = [
                    'first' => ['value' => '您的订单已经支付成功，感谢您对我们的支持', 'color' => '#4e4747'],
                    'keyword1' => ['value' => $eq_info['equipments_name'], 'color' => '#4e4747'],
                    'keyword2' => ['value' => $order['order_no'], 'color' => '#4e4747'],
                    'keyword3' => ['value' => $order['order_actual_money'], 'color' => '#4e4747'],
                    'remark' => ['value' => '若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']), 'color' => '#173177'],
                ];
                NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $order['company_id']);
            }

            //通知到后台
            $admin_notice_config = ConfigService::getTemplateConfig('user_new_order', 'admin', $order['company_id']);
            if (!empty($admin_notice_config)) {
                $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
                //站内
                if (!empty($admin_notice_config['websocket']) && $admin_notice_config['websocket']['switch']) {
                    $msg['type'] = 1;
                    $msg['msg'] = '有新订单拉';
                    $this->sendToUid($this->uid, $msg);
                    //通知行政中心
                    $msg['type'] = 1;
                    $msg['msg'] = '有新订单拉';
                    if (!empty($a_info)) {
                        $this->sendToUid($this->uid, $msg);
                        $this->sendToUid($this->uid, $msg);
                    }
                }
                //仅用户下单且支付的才邮件通知后台
                if (!empty($admin_notice_config['email']) && $admin_notice_config['email']['switch'] && $order['opertaor'] == 1) {
                    //发邮件通知
                    $opertaor = $this->config->get('order_opertaor')[$order['opertaor']];//订单来源
                    $content = '<h2>新订单通知</h2><br/><div>订单编号:' . $order['order_no'] . '</div><div>订单来源:' . $opertaor . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>商品名称:' . $eq_info['equipments_name'] . '(数量:' . $order['equipment_num'] . ')</div><div>订单金额:' . $order['order_actual_money'] . '元</div><div>下单时间:' . $order_time . '</div><div style="color:#808080">客户已完成支付,请进入管理后台查看详情!</div>';
                    //总后台
                    $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                    if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                        EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                    }
                    if (!empty($a_info)) {
                        //对应运营中心
                        $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                        if (!empty($op_email) && !empty($op_email['notice_email'])) {
                            EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                        }
                        //对应合伙人
                        $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                        if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                            EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                        }
                    }
                }

            }
            //通知到大数据平台
            $big_data_notice_config = ConfigService::getTemplateConfig('user_new_order', 'big_data', $order['company_id']);
            if (!empty($big_data_notice_config) && !empty($big_data_notice_config['websocket']) && $big_data_notice_config['websocket']['switch']) {
                $tel = substr($user_info['telphone'], 0, 3) . '****' . substr($user_info['telphone'], 7, 4);
                DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 1, 'product' => $eq_info['equipments_name'], 'user_tel' => $tel]]);
            }

            return true;

        }
        return false;
    }

    /**
     * 支付宝支付
     * @param type $order
     */
    public function aliPay($order, $url)
    {
        $aop = new AopClient ();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new AlipayTradeCreateRequest();
        if ($this->dev_mode) {
            $notifyUrl = $this->config->get('debug_config.callback_domain_name');
        } else {
            $notifyUrl = $this->config->get('callback_domain_name');
        }
        $request->setNotifyUrl($notifyUrl . '/' . $url);
        $payData = [];
        $payData['out_trade_no'] = $order['order_no'];
        $payData['total_amount'] = $order['order_actual_money'];
        if ($this->app_debug == 1) {
            $payData['total_amount'] = 0.01;
        }
        $payData['subject'] = '购买商品';
        $payData['buyer_id'] = $order['buyer_id'];
        $payData = json_encode($payData);
        $request->setBizContent($payData);
        $result = $aop->execute($request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        return $result->$responseNode;
    }

    //百度支付
    public function baiduPay($order_info)
    {
        $this->http_output->setHeader('Content-type: text/html', 'charset=utf-8');
        require_once APP_DIR . '/Library/baiduWeapp/nuomi-openplatform-php-sdk-1.0.3/Autoloader.php';
        if (empty($order_info)) {
            return false;
        }
        /**
         * 第一部分：从公私钥文件路径中读取出公私钥文件内容
         */
        $rsaPrivateKeyFilePath = APP_DIR . '/Library/baiduWeapp/rsa/rsa_private_key.pem';
        $rsaPublicKeyFilePath = APP_DIR . '/Library/baiduWeapp/rsa/rsa_public_key.pem';

        if (!file_exists($rsaPrivateKeyFilePath) || !is_readable($rsaPrivateKeyFilePath) ||
            !file_exists($rsaPublicKeyFilePath) || !is_readable($rsaPublicKeyFilePath)) {
            return false;
        }
        $rsaPrivateKeyStr = file_get_contents($rsaPrivateKeyFilePath);
        $rsaPublicKeyStr = file_get_contents($rsaPublicKeyFilePath);

        $tpOrderId = $order_info['order_no'];
        $totalAmount = 1;//订单金额
        if ($this->app_debug != 1) {
            $totalAmount = intval(round($order_info['order_actual_money'] * 100));
        }

        /**
         * 第二部分：生成签名 DEMO
         */
        $requestParamsArr = array(
            'appKey' => $this->baidu_weapp_config['appKey'],
            'dealId' => $this->baidu_weapp_config['dealId'],
            'tpOrderId' => $tpOrderId,
            'totalAmount' => $totalAmount
        );
        $rsaSign = \NuomiRsaSign::genSignWithRsa($requestParamsArr, $rsaPrivateKeyStr);
        $cashierTopBlock = [];
        $tpData['displayData'] = ['cashierTopBlock' => $cashierTopBlock];
        $tpData['appKey'] = $this->baidu_weapp_config['appKey'];
        $tpData['dealId'] = $this->baidu_weapp_config['dealId'];
        $tpData['tpOrderId'] = $tpOrderId;
        $tpData['rsaSign'] = $rsaSign;
        $tpData['totalAmount'] = $totalAmount;
        $tpData['returnData'] = [];

        $data["dealId"] = $this->baidu_weapp_config['dealId'];
        $data["appKey"] = $this->baidu_weapp_config['appKey'];
        $data["totalAmount"] = $totalAmount;
        $data["tpOrderId"] = $tpOrderId;
        $data["dealTitle"] = "订单支付";
        $data["signFieldsRange"] = "1";
        $data["rsaSign"] = $rsaSign;
        $data["bizInfo"] = ['tpData' => $tpData];

        return $data;
    }


    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取订单详情
     * @description
     * @method POST
     * @url User/Order/getOrderDetail
     * @param order_id 必选 int 订单ID
     * @return "data":{"order_id":"3","order_no":"CQR917731088221618","user_id":"3","order_type":"1","business_type_id":"3","express_name":null,"express_number":null,"warranty_start_time":null,"warranty_end_time":null,"equipments_id":"1","administrative_id":"23","operation_id":"1","package_mode":"1","package_value":"55","package_money":"600.00","package_cycle":"180","province":"重庆市","city":"重庆市","area":"渝北区","province_code":"23","city_code":"271","area_code":"2503","address":"加州城市花园12栋11-6","contact":"傅丽宇","contact_tel":"18324191816","lat":"29.59344","lng":"106.51857","order_status":"3","pay_status":"2","logistics_status":null,"is_show":"1","wx_callback_num":"4200000164201809173918037104","pay_way":"1","create_time":"2018-09-17 16:31:48","pay_time":"2018-09-17 16:32:16","cancel_time":null,"equipment_num":"1","equipment_money":"600.00","equipment_total_money":"600.00","coupon_id":null,"coupon_money":null,"single_deposit":"400.00","total_deposit":"400.00","order_total_money":"1000.00","order_actual_money":"1000.00","remarks":"","is_invoice":"1","opertaor":"1","delivergoods_time":"","collectgoods_time":"","main_pic":"https:\/\/qn.youheone.com\/a037f201809171621201333.jpg","equipments_name":"史密斯牌DR75-C5型直饮水机","model":"Product 7","scale":"台","book_status":1,"book_work_order_id":"2","order_status_name":"待安装"}
     * @return_param order_id int 订单ID
     * @return_param order_type int 订单类型1租赁订单2购买订单3积分订单
     * @return_param order_no string 订单编号
     * @return_param equipment_num string 产品数量i
     * @return_param order_status_name string 订单状态
     * @return_param order_total_money float 订单总价
     * @return_param package_mode int 套餐模式 1时长 2流量
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getOrderDetail()
    {
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数订单ID");
        }
        $map['order_id'] = $this->parm['order_id'];
        $join = [
            ['equipments as e', 'e.equipments_id = rq_orders.equipments_id', 'inner']   // 设备分类表对应订单表
        ];
        $data = $this->order_model->getOne($map, 'rq_orders.*,e.main_pic,e.equipments_name,e.model,e.scale', $join);
        if (empty($data)) {
            return $this->jsonend(-1003, "没有相关订单信息");
        }
        $data['delivergoods_time'] = strlen($data['delivergoods_time']) > 5 ? date('Y-m-d H:i:s', $data['delivergoods_time']) : '';
        $data['collectgoods_time'] = strlen($data['collectgoods_time']) > 5 ? date('Y-m-d H:i:s', $data['collectgoods_time']) : '';
        $data['create_time'] = date('Y-m-d H:i:s', $data['create_time']);
        $data['pay_time'] = empty($data['pay_time']) ? '' : date('Y-m-d H:i:s', $data['pay_time']);
        if ($data['package_mode'] == 2) {
            $data['package_value'] = empty($data['package_value']) ? '' : number_format($data['package_value'] / 1000, 3);
        }
        //是否预约安装
        $work_order = $this->work_model->getWorkDetail(array('wb.business_id' => 3, 'order_id' => $data['order_id']), [['work_order_business wb', 'rq_work_order.work_order_id = wb.work_order_id', 'LEFT']], 'rq_work_order.work_order_id');
        $data['book_status'] = 0;
        $data['book_work_order_id'] = 0;
        if (!empty($work_order)) {
            $data['book_status'] = 1;
            $data['book_work_order_id'] = $work_order['work_order_id'];
        }
        //发票信息
        if ($data['is_invoice'] == 2) {
            $data['invoice_info'] = $this->order_invoice_model->getOne(array('order_id' => $this->parm['order_id']), '*');
        }
        $data['main_pic'] = $this->config->get('qiniu.qiniu_url') . $data['main_pic'];
        $data['order_status_name'] = $this->config->get('order_status')[$data['order_status']];
        if ($data['order_status'] == 4 && $data['is_show'] == 1) {
            $data['order_status_name'] = '待评价';
        } else if ($data['order_status'] == 4 && $data['is_show'] == 2) {
            $data['order_status_name'] = '已评价';
        }
        return $this->jsonend(1000, "获取数据成功", $data);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取用户订单列表
     * @description 支持分页，状态查询
     * @method POST
     * @url User/Order/getOrderList
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @param order_status 可选 int 状态ID,默认全部 1待付款 2待发货 3待安装 4已安装5已取消 6待收货
     * @return {"code":1000,"message":"获取订单列表成功","data":[{"order_id":"83","order_type":"2","order_no":"CQRA10477995674933","equipment_num":"1","order_total_money":"1598.00","order_status":"3","user_id":"6","package_mode":"1","package_value":"36500","package_money":"1598.00","package_cycle":"36500","main_pic":"https:\/\/qn.youheone.com\/d8c61201810081031598328.jpg","equipments_name":"测试产品202","scale":"台","model":"product2","is_show":"1","book_status":1,"book_work_order_id":"104","order_status_name":"待安装"}]}
     * @return_param order_id int 订单ID
     * @return_param order_type int 订单类型1租赁订单 2购买订单 3积分订单
     * @return_param order_no string 订单编号
     * @return_param equipment_num string 产品数量
     * @return_param order_status_name string 订单状态
     * @return_param order_total_money float 订单总价
     * @return_param package_mode int 套餐模式 1时长 2流量
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getOrderList()
    {
        // 接收参数
        $user_id = $this->user_id;
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $order_status = $this->parm['order_status'] ?? '';

        // 连表
        $join = [
            ['equipments as e', 'e.equipments_id = rq_orders.equipments_id', 'inner']   // 设备分类表对应订单表
        ];
        // 查询条件
        $where['rq_orders.user_id'] = $user_id;
        // 判断是否传订单状态
        if (!empty($order_status)) {
            $where['rq_orders.order_status'] = $order_status;
        }
        //是否评价 1否 2是
        if (!empty($this->parm['is_show'] ?? '')) {
            $where['is_show'] = $this->parm['is_show'];
            if (empty($order_status)) {
                $where['order_status'] = array('not in', [1]);
            }
        }
        // 查询字段
        $field = 'rq_orders.order_id,order_type,rq_orders.order_no,rq_orders.equipment_num,rq_orders.order_total_money,rq_orders.order_status,rq_orders.user_id,rq_orders.package_mode,rq_orders.package_value,rq_orders.package_money,rq_orders.package_cycle,
                  e.main_pic,e.equipments_name,e.scale,e.model,is_show';
        // 排序方式
        $order = ['rq_orders.create_time' => 'DESC'];
        // 调用模型获取订单列表
        $orderData = $this->order_model->getAll($where, $page, $pageSize, $field, $join, $order);

        // 判断结果--返回
        if ($orderData) {
            foreach ($orderData as $k => $v) {
                $orderData[$k]['main_pic'] = $this->config->get('qiniu.qiniu_url') . $v['main_pic'];
                //是否预约安装
                $work_order = $this->work_model->getWorkDetail(array('wb.business_id' => 3, 'order_id' => $v['order_id']), [['work_order_business wb', 'rq_work_order.work_order_id = wb.work_order_id', 'LEFT'], ['contract', 'rq_work_order.contract_id = rq_contract.contract_id', 'LEFT']], 'rq_work_order.work_order_id,rq_contract.contract_id,rq_contract.status AS contract_status');
                $orderData[$k]['contract_status'] = $work_order['contract_status'] ?? 0;
                $orderData[$k]['contract_id'] = $work_order['contract_id'] ?? '';
                $orderData[$k]['book_status'] = 0;
                $orderData[$k]['book_work_order_id'] = 0;
                if (!empty($work_order)) {
                    $orderData[$k]['book_status'] = 1;
                    $orderData[$k]['book_work_order_id'] = $work_order['work_order_id'];
                }
                //订单状态
                $orderData[$k]['order_status_name'] = $this->config->get('order_status')[$v['order_status']];
                if ($v['order_status'] == 4 && $v['is_show'] == 1) {
                    $orderData[$k]['order_status_name'] = '待评价';
                } else if ($v['order_status'] == 4 && $v['is_show'] == 2) {
                    $orderData[$k]['order_status_name'] = '已评价';
                }
            }

            return $this->jsonend(1000, '获取订单列表成功', $orderData);
        } else {
            return $this->jsonend(-1000, '暂无订单', $orderData);
        }
    }

    /**
     * sgg
     * 获取新装未支付的订单
     */
    public function http_getOrderOne()
    {
        // 接收参数
        $user_id = $this->user_id;
        $where['user_id'] = $user_id;
        $where['business_type_id'] = 3;
        $where['pay_status'] = 1;
        $where['order_status'] = 1;
        $orderData = $this->order_model->getOne($where);
        if (empty($orderData)) {
            return $this->jsonend(-1000, '暂无订单');
        }
        return $this->jsonend(1000, '获取订单列表成功', $orderData);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 根据状态统计订单
     * @description
     * @method POST
     * @url User/Order/getOrderStatus
     * @return {"code":1000,"message":"获取订单状态数量成功","data":{"n_pay":0,"substituting":1,"n_install":2,"y_install":1,"collection":0,"n_evaluate":1}}
     * @return_param n_pay int 待支付订单数量
     * @return_param substituting int 待发货订单数量
     * @return_param n_install int 待安装订单数量
     * @return_param y_install int 已安装订单数量
     * @return_param collection int 待收货订单数量
     * @return_param n_evaluate int 待评价订单数量
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getOrderStatus()
    {
        // 接收参数
        $user_id = $this->user_id;

        // 获取用户四种状态
        // 待付款--条件
        $where = ['order_status' => 1, 'user_id' => $user_id];
        $orderStatusNum['n_pay'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 待发货--条件
        $where = ['order_status' => 2, 'user_id' => $user_id];
        $orderStatusNum['substituting'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 待安装--条件
        $where = ['order_status' => 3, 'user_id' => $user_id];
        $orderStatusNum['n_install'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 已安装--条件
        $where = ['user_id' => $user_id, 'order_status' => 4];
        $orderStatusNum['y_install'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 待收货--条件
        $where = ['user_id' => $user_id, 'order_status' => 6];
        $orderStatusNum['collection'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 待评价--条件
        $where = ['rq_orders.user_id' => $user_id, 'order_status' => 4, 'is_show' => 1];
        // 连表
        $join = [
            ['evaluate e', 'e.order_id = rq_orders.order_id', 'left']
        ];
        $orderStatusNum['n_evaluate'] = $this->order_model->count($where, $join);

        // 判断结果--返回
        if ($orderStatusNum) {
            return $this->jsonend(1000, '获取订单状态数量成功', $orderStatusNum);
        } else {
            return $this->jsonend(-1000, '获取订单状态数量失败', $orderStatusNum);
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取评价标签
     * @description
     * @method POST
     * @url User/Order/getEvaluateLabel
     * @param type 可选  int 类型 1对订单评价表桥 2对工程人员评价标签,默认1
     * @return {"code":1000,"message":"获取数据成功","data":[{"label_id":"1","label_name":"好评","label_type":"1","create_time":"1537179443","status":"1","sort":"0","is_del":"0"}]}
     * @return_param label_id int 标签ID
     * @return_param label_name int 标签名称
     * @return_param label_type int 标签类型 1对订单评价表桥 2对工程人员评价标签
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getEvaluateLabel()
    {
        //$map['label_type'] = empty($this->parm['type'] ?? '') ? 1 : $this->parm['type'];
        $map['status'] = 1;
        $data = $this->evaluate_label_model->getAll($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1003, "暂无相关数据");
        }
        return $this->jsonend(1000, "获取数据成功", $data);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 提交评价
     * @description
     * @method POST
     * @url User/Order/addUserEvaluation
     * @param type 必选 int 1对订单评价表桥 2对工程人员评价标签
     * @param engineer_id 可选 int 工程人员ID,若type=2必传
     * @param work_order_id 可选 int 工单ID,若type=2必传
     * @param order_id 可选 int 订单ID,若type=1必传
     * @param evaluate_content 必选 string 内容
     * @param label 必选 array 评价标签
     * @param evaluate_stars 可选 int 评轮等级1-5，默认0
     * @param evaluate_image 可选 array 图片
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_addUserEvaluation()
    {
        // 接收参数
        $data['user_id'] = $this->user_id;
        // 判断 评价类型
        if (!empty($this->parm['type'] ?? '') && $this->parm['type'] == 2) {
            $data['type'] = 2;
            if (empty($this->parm['engineer_id'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数,工程人员ID");
            }
            if (empty($this->parm['work_order_id'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数,工单ID");
            }
            $data['engineer_id'] = $this->parm['engineer_id'];
            $data['work_order_id'] = $this->parm['work_order_id'];
        } else {
            $data['type'] = 1;
            if (empty($this->parm['equipments_id'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数equipments_id");
            }
            if (empty($this->parm['order_id'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数order_id");
            }
            $data['equipments_id'] = $this->parm['equipments_id'];
            $data['order_id'] = $this->parm['order_id'];
        }
        if (empty($this->parm['evaluate_content'] ?? '')) {
            return $this->jsonend(-1001, "请输入评价内容");
        }
        if (empty($this->parm['label'] ?? '')) {
            return $this->jsonend(-1001, "至少选择一项标签");
        }
        $evaluate_stars = $this->parm['evaluate_stars'] ?? 0;
        $evaluate_image = empty($this->parm['evaluate_image'] ?? '') ? '' : json_encode($this->parm['evaluate_image']);
        // 数据组装
        $data['evaluate_stars'] = $evaluate_stars;
        $data['evaluate_content'] = $this->parm['evaluate_content'];
        $data['evaluate_image'] = $evaluate_image;
        $data['evaluate_time'] = time();
        $add_status = false;
        $this->db->begin(function () use ($data, &$add_status) {
            $evaluate_id = $this->evaluate_model->add($data);
            if (!empty($this->parm['label'])) {
                foreach ($this->parm['label'] as $k => $v) {
                    $arr['evaluate_id'] = $evaluate_id;
                    $arr['label_id'] = $v;
                    $arr['create_time'] = time();
                    $this->evaluate_to_label_model->add($arr);
                }
            }
            //修改订单/工单状态
            if (!empty($this->parm['type'] ?? '') && $this->parm['type'] == 2) {
                $work_order_data['work_order_status'] = 12;
                $this->work_model->editWork(array('work_order_id' => $this->parm['work_order_id']), $work_order_data);
                //添加工单日志
                $this->addWorkOrderLog($this->parm['work_order_id'], time(), 12, "感谢您的评价");
            } else {
                $order['is_show'] = 2;
                $this->order_model->save(array('order_id' => $this->parm['order_id']), $order);
            }
            $add_status = true;
        });
        // 判断结果--返回
        if ($add_status) {
            return $this->jsonend(1000, '评论成功');
        } else {
            return $this->jsonend(-1000, '评论失败');
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取商品评论列表
     * @description 支持分页
     * @method POST
     * @url User/Order/getEvaluateList
     * @param equipments_id 必选 int 设备ID
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @param type 可选 int 评价类型 1对订单评价 2对工程人员评价,默认1
     * @return {"code":1000,"message":"获取评价成功","data":[{"evaluate_id":"5","user_id":"3","type":"1","engineer_id":null,"work_order_id":null,"equipments_id":"1","order_id":"2","evaluate_stars":"5","evaluate_content":"还行吧","evaluate_image":[],"status":"1","evaluate_time":"2018-09-25 14:22:47","username":"fly傅丽宇","avatar_img":"https:\/\/wx.qlogo.cn\/mmopen\/vi_32\/4oenloc5C5pJWHfxAEVMzuDtz69ILQxrdOdbWgyvTj6qQfAtmnfnC5tEy0yoILAZws9cial5eG1UC48RgDzoy8A\/132","create_time":"2018-09-17 16:26:28"}]}
     * @return_param evaluate_id int 评论ID
     * @return_param evaluate_stars int 评论星级
     * @return_param evaluate_content string 评论内容
     * @return_param evaluate_image array 评论图片
     * @return_param evaluate_time string 评论时间
     * @return_param username string 用户名称
     * @return_param avatar_img string 用户头像
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getEvaluateList()
    {
        // 接收参数
        $equipments_id = $this->parm['equipments_id'] ?? '';
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $type = empty($this->parm['type'] ?? '') ? 1 : $this->parm['type'];
        // 判断参数
        if (empty($equipments_id)) {
            return $this->jsonend(-1001, '设备分类ID不能为空');
        }

        // 主表 -- 评价表
        $dbName = 'evaluate a';
        // 条件
        $where['a.equipments_id'] = $equipments_id;
        $where['a.type'] = $type;
        // 连表
        $join = [
            ['customer c', 'c.user_id = a.user_id', 'left'], // 用户表对应评论表
            ['orders o', 'o.order_id = a.order_id', 'left']
        ];
        // 查询字段
        $field = 'a.*,c.username,c.avatar_img,o.create_time';
        // 排序方式
        $order = ['evaluate_time' => 'DESC'];
        // 调用模型获取商品对应评价
        $evaluateData = $this->evaluate_model->getAll($where, $join, $page, $pageSize, $field, $order, $dbName);
        // 判断结果--返回
        if ($evaluateData) {
            foreach ($evaluateData as $k => $v) {
                $evaluateData[$k]['avatar_img'] = UploadImgPath($v['avatar_img'], 1);
                $evaluateData[$k]['username'] = empty($v['username']) ? '匿名用户' : passUsername($v['username']);
                $evaluateData[$k]['evaluate_time'] = date('Y-m-d H:i:s', $v['evaluate_time']);
                $evaluateData[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                $pic = array();
                if (!empty($v['evaluate_image'])) {
                    $evaluate_image = json_decode($v['evaluate_image'], true);
                    if (!empty($evaluate_image)) {
                        foreach ($evaluate_image as $kk => $vv) {
                            $pic[] = $this->config->get('qiniu.qiniu_url') . $vv;
                        }
                    }
                }
                $evaluateData[$k]['evaluate_image'] = $pic;
            }
            return $this->jsonend(1000, '获取评价成功', $evaluateData);
        } else {
            return $this->jsonend(-1000, '获取评价失败', $evaluateData);
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取订单动态
     * @description
     * @method POST
     * @url User/Order/userOrderDynamic
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @return {"code":1000,"message":"获取用户订单动态成功","data":[{"operating_status":"10","order_number":"CQWA10492827921095","order_id":"104","main_pic":"https:\/\/qn.youheone.com\/d8c61201810081031598328.jpg","status":"服务中","create_time":"2018-10-10 13:29","remark":"工程人员已上门，正在服务中！工单编号:CQWA10492827921095","dynamic_type":2}]}
     * @return_param operating_status int 状态码
     * @return_param order_number string 订单编号
     * @return_param status string 状态
     * @return_param remark string 备注
     * @return_param create_time date 时间
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_userOrderDynamic()
    {
        // 接收参数
        $user_id = $this->user_id;
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        // 主表
        $dbName = 'orders_log a';
        // 条件
        $where['o.user_id'] = $user_id;
        $where['is_to_user'] = 1;
        // 连表
        $join = [
            ['orders o', 'o.order_id = a.order_id', 'left'], // 订单表对应订单日志表
            ['equipments e', 'e.equipments_id = o.equipments_id', 'left']  // 设备分类表对应订单表
        ];
        // 查询字段
        $field = 'e.equipments_name,e.main_pic,a.*,o.order_no as order_number';
        // 排序方式
        $order = ['a.create_time' => 'DESC'];
        // 调用模型查询用户动态状态
        $orderLogData = $this->log_model->getAll($where, 1, -1, $join, $field, $order, $dbName);
        // 判断结果-- 返回
        if ($orderLogData) {
            foreach ($orderLogData as $k => $v) {
                $orderLogData[$k]['status'] = $this->config->get('order_status')[$v['status']];
                $orderLogData[$k]['dynamic_type'] = 1;

//                $orderLogData[$k]['main_pic'] = $this->config['qiniu']['qiniu_url'] . $v['main_pic'];
//                $orderLogData[$k]['create_time'] = empty($v['create_time'])?'':date('Y-m-d H:i', $v['create_time']);
            }
        }
        //查询工单动态
        $work_map['wo.user_id'] = $this->user_id;
        $work_map['wl.is_to_user'] = 1;
        $work_join = $join = [
            ['work_order wo', 'wo.work_order_id = wl.work_order_id', 'left'],
            ['equipments e', 'e.equipments_id = wo.equipments_id', 'left']  // 设备分类表对应订单表
        ];
        $work_order_log = $this->work_log_model->getAll($work_map, 1, -1, $work_join, 'wl.operating_status,wo.order_number,wo.work_order_id as order_id,e.main_pic,wo.work_order_status as status,operating_time as create_time,wl.remarks as remark', ['wl.operating_time' => 'DESC'], 'work_order_log wl');
        if (!empty($work_order_log)) {
            foreach ($work_order_log as $k => $v) {
                $work_order_log[$k]['status'] = empty($v['operating_status']) ? '' : $this->config->get('user_work_order_status')[$v['operating_status']];
                $work_order_log[$k]['dynamic_type'] = 2;
                $work_order_log[$k]['remark'] = empty($v['operating_status']) ? '' : $this->config->get('user_work_order_status_desc')[$v['operating_status']] . '！工单编号:' . $v['order_number'];
            }
        }
        //数组合并
        $new_arr = array_merge($orderLogData, $work_order_log);
        //数组排序
        $new_arr2 = mymArrsort($new_arr, 'create_time');
        //数组分页
        $final_arr = arrPage($new_arr2, $page, $pageSize);
        if (!empty($final_arr)) {
            foreach ($final_arr as $k => $v) {
                $final_arr[$k]['create_time'] = empty($v['create_time']) ? '' : date('Y-m-d H:i', $v['create_time']);
                $final_arr[$k]['main_pic'] = $this->config['qiniu']['qiniu_url'] . $v['main_pic'];
            }
            return $this->jsonend(1000, '获取用户订单动态成功', $final_arr);
        }
        return $this->jsonend(-1003, '暂无用户订单动态');
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 取消订单
     * @description
     * @method POST
     * @url User/Order/cancelOrder
     * @param order_id 必选 int 订单ID
     * @return {"code":1000,"message":"取消成功","data":true}
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_cancelOrder()
    {
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数订单ID");
        }
        //查询订单信息
        $map['order_id'] = $this->parm['order_id'];
        $map['user_id'] = $this->user_id;
        $field = 'pay_status,order_status';
        $order_info = $this->order_model->getOne($map, $field);
        if (empty($order_info)) {
            return $this->jsonend(-1100, "订单不存在");
        }
        if ($order_info['pay_status'] == 2) {
            return $this->jsonend(-1101, "该订单已支付,不能进行取消");
        }
        if ($order_info['order_status'] == 5) {
            return $this->jsonend(-1102, "该订单已取消,不能重复进行取消");
        }
        //修改订单状态
        $edit_data['order_status'] = 5;
        $edit_data['cancel_time'] = time();
        $add_status = false;
        $this->db->begin(function () use ($edit_data, &$add_status, $map) {
            $this->order_model->save($map, $edit_data);
            $this->addOrderLog($this->parm['order_id'], $edit_data['order_status'], "用户【用户ID:" . $this->user_id . "】已取消订单");
            $add_status = true;
        });
        if ($add_status) {
            return $this->jsonend(1000, "取消成功");
        }
        return $this->jsonend(-1000, "取消失败");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 确认收货
     * @description
     * @method POST
     * @url User/Order/collectGoods
     * @param order_id 必选 int 订单ID
     * @return {"code":1000,"message":"确认收货成功","data":true}
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_collectGoods()
    {
        // 接收参数--验证参数
        $order_id = $this->parm['order_id'] ?? '';
        $user_id = $this->user_id;
        if (empty($order_id)) {
            return $this->jsonend(-1001, '订单ID不能为空');
        }
        // 获取保修时间
        $join = [
            ['equipments e', 'e.equipments_id = rq_orders.equipments_id', 'left'],
        ];
        $field = 'e.warranty_time,equipments_line';
        $orderInfo = $this->order_model->getOne(['order_id' => $order_id], $field, $join);
        $warranty_end_time = 0;
        $warranty_start_time = time();
        if ($orderInfo) {
            $warranty_end_time = date('Y-m-d H:i:s', strtotime('+' . $orderInfo['warranty_time'] . 'day'));
        }

        // 开启事务
        $add_status = false;
        $equipments_line = $orderInfo['equipments_line'];
        $this->db->begin(function () use (&$add_status, &$order_id, $user_id, $warranty_start_time, $warranty_end_time,$equipments_line) {
            // 1-- 修改订单状态
            $where['order_id'] = $order_id;
            $data['order_status'] = 3; // 待安装
            if( $equipments_line == 3){ // 产品线为 礼品
                $data['order_status'] = 4; // 待安装
            }
            $data['warranty_start_time'] = $warranty_start_time; // 保修开始时间
            $data['warranty_end_time'] = strtotime($warranty_end_time);  // 保修结束时间
            $this->order_model->save($where, $data);
            // 2-- 新增物流信息
            //$this->addOrderLogistics($order_id, 7, '确认收货', $user_id); //找不到对应方法
            // 3-- 新增订单日志
            $this->addOrderLog($order_id, 3, '用户【用户ID:' . $user_id . '】已收货');
            $add_status = true;
        });
        if ($add_status) {
            return $this->jsonend(1000, '确认收货成功', $add_status);
        }
        return $this->jsonend(-1000, '确认收货失败,请稍后再试', $add_status);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 查询物流信息
     * @description
     * @method POST
     * @url User/Order/orderLogistics
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @param order_id 必选 int 订单ID
     * @return {"code":1000,"message":"获取数据成功","data":{"base_info":{"logistics_status":"7","express_name":"暂无物流信息","express_number":"暂无物流信息","equipments_name":"购买产品1","main_pic":"https:\/\/qn.youheone.com\/20d01201809201045299934.png","company_hotline":"18883880448","logistics_status_name":"已签收"},"logistics":[{"logistics_id":"5","order_id":"18","status_code":"7","status_name":"已签收","remark":"确认收货","create_time":"09-20 11:34"}]}}
     * @return_param base_info object 基本信息（logistics_status 物流状态express_name快递名称 express_number快递编号 equipments_name产品名称 main_pic产品主图）
     * @return_param logistics array 物流信息（status_name 状态名称）
     * @remark {"order_id":18}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_orderLogistics()
    {
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数order_id");
        }
        //订单信息
        $join = [
            ['equipments as e', 'e.equipments_id = rq_orders.equipments_id', 'inner']   // 设备分类表对应订单表
        ];
        $order = $this->order_model->getOne(array('order_id' => $this->parm['order_id']), 'logistics_status,express_name,express_number,e.equipments_name,e.main_pic', $join);
        if (empty($order)) {
            return $this->jsonend(-1000, "订单信息错误");
        }
        $page = empty($this->parm['page'] ?? '') ? 1 : $this->parm['page'];
        $pageSize = empty($this->parm['pageSize'] ?? '') ? 10 : $this->parm['pageSize'];
        $map['order_id'] = $this->parm['order_id'];
        $data = $this->order_logistics_model->getAll($map, 'logistics_id,order_id,status_code,status_name,remark,create_time', $page, $pageSize);
        if (empty($data)) {
            return $this->jsonend(-1003, "暂无相关数据");
        }

        //处理订单基本信息
        $order['main_pic'] = empty($order['main_pic']) ? '' : $this->config->get('qiniu.qiniu_url') . $order['main_pic'];
        $order['company_hotline'] = ConfigService::getConfig('company_hotline', false, $this->company);
        $order['logistics_status_name'] = $this->config->get('order_logistics_code')[$order['logistics_status']];
        $order['express_name'] = empty($order['express_name']) ? '暂无物流信息' : $order['express_name'];
        $order['express_number'] = empty($order['express_number']) ? '暂无物流信息' : $order['express_number'];
        $arr['base_info'] = $order;
        //处理物流信息
        foreach ($data as $k => $v) {
            $data[$k]['create_time'] = empty($v['create_time']) ? '' : date('m-d H:i', $v['create_time']);
            $data[$k]['status_name'] = $this->config->get('order_logistics_code')[$v['status_code']];
        }
        $arr['logistics'] = $data;
        return $this->jsonend(1000, "获取数据成功", $arr);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/设备相关
     * @title 绑定设备
     * @description
     * @method POST
     * @url User/Order/activeBuyDevice
     * @param order_id 必选 sting 订单ID
     * @param device_no 必选 sting 设备编号
     * @return {"code": 1000,"message": "绑定成功"}
     * @return_param
     * @remark {"work_order_id":"12","device_no":"a5465"}
     * @number 0
     * @author lcx
     * @date 2018-10-19
     */
    public function http_activeBuyDevice()
    {
        if (empty($this->parm['device_no'] ?? '')) {
            return $this->jsonend(-1001, "请输入设备编号");
        }
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数订单ID");
        }
        //查询订单信息
        $order_info = $this->order_model->getOne(['order_id' => $this->parm['order_id']], '*');
        if (empty($order_info)) {
            return $this->jsonend(-1101, "订单信息错误");
        }

        //判断设备是否存在于设备库
        $li_map['device_no'] = $this->parm['device_no'];
        $libary = $this->equipment_lists_model->findEquipmentLibrary($li_map, '*');

        if (empty($libary)) {
            return $this->jsonend(-1101, "请输入正确的设备号");
        }
        //查询产品信息
        $eq_info = $this->equip_model->getDetail(array('equipments_id' => $order_info['equipments_id']), 'equipments_name,model,equipments_type,warranty_time,mainboard_id');
        if ($eq_info['equipments_type'] != 2) {
            return $this->jsonend(-1101, "仅支持激活售卖的产品");
        }
        /* if ($eq_info['mainboard_id'] != $libary['model_id']) {
             //设备和产品的主板型号不一致
             return $this->jsonend(-1101, "设备和产品的主板型号不一致");
         }*/

        $warranty_time['start'] = $order_info['warranty_start_time'];
        $warranty_time['end'] = $order_info['warranty_end_time'];
        //判断设备是否已经存在
        $equipment_info = $this->equipment_lists_model->findEquipmentLists(array('device_no' => $this->parm['device_no']), 'equipment_id,status');
        if ($this->dev_mode == 0) {//正式环境才进行联网状态验证
            if (!empty($equipment_info)) {
                //判断用户是否重复绑定设备
                $bd_map['equipment_id'] = $equipment_info['equipment_id'];
                $bd_map['is_owner'] = 2;
                $bind_info = $this->bind_model->getOne($bd_map, '*');
                if (!empty($bind_info)) {
                    return $this->jsonend(-1104, "该设备已被绑定过，不能重复绑定");
                }
            } else {
                return $this->jsonend(-1104, "未获取到设备信息,请等待设备联网成功后再试");
            }
        }
        //判断用户是否已经绑定完
        $bind_map['equipments_id'] = $order_info['equipments_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['user_id'] = $order_info['user_id'];
        $bind_list = $this->bind_model->getAll($bind_map, '*');
        if (!empty($bind_list) && count($bind_list) >= $order_info['equipment_num']) {
            return $this->jsonend(-1105, '该用户租赁该类型设备' . $order_info['equipment_num'] . '台，已绑定' . count($bind_list) . '台');
        }
        // 获取用户信息
        $user_info = $this->user_model->getOne(['user_id' => $order_info['user_id']], 'openid,telphone');
        $equipment_id = '';
        $add_status = false;
        $this->db->begin(function () use (&$add_status, $libary, $order_info, $user_info, $equipment_info, $eq_info, &$equipment_id, $warranty_time) {
            $eq_data['province'] = $order_info['province'];
            $eq_data['city'] = $order_info['city'];
            $eq_data['area'] = $order_info['area'];
            $eq_data['province_code'] = $order_info['province_code'];
            $eq_data['city_code'] = $order_info['city_code'];
            $eq_data['area_code'] = $order_info['area_code'];
            $eq_data['address'] = $order_info['address'];
            $eq_data['lng'] = $order_info['lng'];
            $eq_data['lat'] = $order_info['lat'];
            $eq_data['contact_number'] = $order_info['contact_tel'];
            $eq_data['contact'] = $order_info['contact'];
            $eq_data['alias_name'] = $eq_info['equipments_name'];
            $eq_data['equipment_type'] = $eq_info['equipments_type'];
            if ($eq_info['equipments_type'] == 2) {
                $eq_data['start_time'] = $warranty_time['start'];
                $eq_data['end_time'] = $warranty_time['end'];
            }
            $eq_data['company_id'] = $order_info['company_id'];
            if (empty($equipment_info)) {
                //新增设备记录
                $eq_data['equipments_id'] = $order_info['equipments_id'];
                $eq_data['device_no'] = $this->parm['device_no'];
                $eq_data['create_time'] = time();
                $eq_data['status'] = 1;
                $eq_data['contract_id'] = '';
                $equipment_id = $this->equipment_lists_model->insertEquipmentLists($eq_data);
            } else {
                //修改设备信息
                $eq_data['contract_id'] = '';
                $equipment_id = $equipment_info['equipment_id'];
                $eq_data['equipments_id'] = $order_info['equipments_id'];
                $this->equipment_lists_model->updateEquipmentLists($eq_data, array('equipment_id' => $equipment_id));
            }
            //添加绑定记录
            $bind_data['equipments_id'] = $order_info['equipments_id'];
            $bind_data['order_id'] = $this->parm['order_id'];
            $bind_data['equipment_id'] = $equipment_id;
            $bind_data['user_id'] = $order_info['user_id'];
            $bind_data['create_time'] = time();
            $bind_data['is_owner'] = 2;
            $this->bind_model->addCustomerBindEquipment($bind_data);
            //已判定完修改相应状态
            $bind_map['equipments_id'] = $order_info['equipments_id'];
            $bind_map['user_id'] = $order_info['user_id'];
            $bind_map['is_owner'] = 2;
            $bind_map['order_id'] = $this->parm['order_id'];
            $bind_list = $this->bind_model->getAll($bind_map, '*');
            if (count($bind_list) >= $order_info['equipment_num']) {
                //如果绑定完设备修改订单状态
                $this->order_model->save(['order_id' => $this->parm['order_id']], ['order_status' => 4]);
            }
            //如果是购买产品直接下发套餐以及数据同步
            if ($eq_info['equipments_type'] == 2) {
                DeviceService::activeBuyDevice($this->parm['device_no'], $order_info['equipments_id']);
            }
            $add_status = true;
        }, function ($e) {
            secho('[ERROR] 用户激活设备错误', $e->error);
            return $this->jsonend(-1000, "用户激活设备失败,请重试!" . $e->error);
        });

        if ($add_status) {

            // 发送模板消息
            //通知到用户
            $user_notice_config = ConfigService::getTemplateConfig('user_bind_device_success', 'user', $this->company);
            if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                $template_id = $user_notice_config['template']['wx_template_id'];
                $mp_template_id = $user_notice_config['template']['mp_template_id'];
                $template_data = [
                    'keyword1' => ['value' => $this->parm['device_no']], // 设备编号
                    'keyword2' => ['value' => $eq_info['model']], // 品牌型号
                    'keyword3' => ['value' => date('Y-m-d H:i:s')], // 绑定时间
                    'keyword4' => ['value' => 1], // 绑定数量
                    'keyword5' => ['value' => '恭喜!您已经成功绑定了一台设备'], // 绑定提醒
                ];
                $weapp_template_keyword = '';
                $mp_template_data = [
                    'first' => ['value' => '恭喜!您已经成功绑定了一台设备', 'color' => '#4e4747'],
                    'keyword1' => ['value' => 1, 'color' => '#4e4747'],
                    'keyword2' => ['value' => 0, 'color' => '#4e4747'],
                    'keyword3' => ['value' => $eq_info['equipments_name'] . "(设备编号:" . $this->parm['device_no'] . ")", 'color' => '#4e4747'],
                    'remark' => ['value' => '感谢您一直以来对' . $this->user_wx_name . '的支持，' . $this->user_wx_name . '永远都是维护您健康的坚实后盾!若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company), 'color' => '#173177'],
                ];
                $template_tel = $user_info['telphone'];  // 电话
                $template_content = '您已经成功绑定了一台设备。设备编号为' . $this->parm['device_no'] . '，绑定时间' . date('Y-m-d H:i:s') . '。打开微信进入' . $this->user_wx_name . '小程序可查看设备详情';
                // 调用模板消息发送
                $template_url = '';
                NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $this->company);
            }


            return $this->jsonend(1000, "激活成功");
        }
        return $this->jsonend(-1000, "激活失败,请重试");
    }


    /**
     * @desc   添加订单物流信息
     * @param $status
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLogistics($order_id, $status_code, $remark, $operator_uid)
    {
        $data['order_id'] = $order_id;
        $data['status_code'] = $status_code;
        $data['status_name'] = $this->config->get('order_logistics_code')[$status_code];
        $data['remark'] = $remark;
        $data['create_time'] = time();
        $data['operator_role'] = 3;
        $data['operator_uid'] = $operator_uid;
        $res = $this->order_logistics_model->add($data);
        //修改订单物流状态
        $this->order_model->save(array('order_id' => $order_id), array('logistics_status' => $status_code));
        return $res;
    }


}
