<?php

namespace app\Process;

use Server\Components\Process\Process;
use Server\Components\CatCache\CatCacheProcess;
use Server\Asyn\HttpClient\HttpClientPool;
use Server\Asyn\Mysql\MysqlAsynPool;

/**
 * 异步任务
 */
class SimProcess extends Process {

    protected $mysqlPool;
    protected $db;
    protected $tick_id;
    protected $SimState2HttpClient;
    protected $conf;
    protected $count;
    protected $pageSize = 2;

    public function start($process) {
      
        $this->conf = get_instance()->config->get('shumi');
        get_instance()->addAsynPool('SimState2', new HttpClientPool(get_instance()->config, get_instance()->config->get('shumi.testUrl')));
        $this->SimState2HttpClient = get_instance()->getAsynPool('SimState2');
        $this->mysqlPool = new MysqlAsynPool(get_instance()->config, get_instance()->config->get('mysql.active'));
        $this->db = $this->mysqlPool->installDbBuilder();

        $this->getIms();
    }

    /**
     * 查询ism流量卡状态
     */
    public function getIms() {
        $this->tick_id = get_instance()->tick(20000, function() {
            $count = $this->db->from('equipment_library')
                    ->TPWhere(['device_status' => ['<>', 3], 'sim_sn' => ['<>', 'null']])
                    ->select('*')
                    ->query()
                    ->num_rows();
            $this->count = $count;
            if ($count <= 0) {
                return true;
            }
            $status_success = $this->setImsStatus(1);
            if($status_success){
                $this->setImsInfo(1);
            }
            
        });
        return true;
    }
    /**
     * 查询流量卡状态 并修改数据库状态
     * @param type $page
     * @return boolean
     */
    public function setImsStatus($page) {
        $cards = $this->db->from('equipment_library')
                ->TPWhere(['device_status' => ['<>', 3], 'sim_sn' => ['<>', 'null']])
                ->select('*')
                ->page($this->pageSize, $page)
                ->query()
                ->result_array();
        if (empty($cards)) {
            return true;
        }
        $card_arr = [];
        foreach ($cards as $k => &$v) {
            if (empty($v['sim_sn'])) {
                unset($cards[$k]);
                continue;
            }
            array_push($card_arr, trim($v['sim_sn']));
        }
        $card_str = implode(',', $card_arr);
        //$this->getStatus(['iccid'=>'89860429161891321670','date'=>'20181228','duration'=>'2'],'/card/daily/usage/');
        $data = $this->getStatus(['iccidList' => $card_str], '/card/batch/status/');
        foreach ($data['data'] as $kk => $vv) {
            
            if(isset($vv['errorMsg'])){
                var_dump($vv['errorMsg']);
                continue;
            }
            if (!isset($vv['accountStatus']) || empty($vv['accountStatus'])) {
                continue;
            }
            $status = 0;
            switch ($vv['accountStatus']) {
                case 'UNKNOWN':
                    $status = -1;break;
                case 'READY':
                    $status = 0;break;
                case 'ACTIVATED':
                    $status = 1;break;
                case 'SUSPENDED':
                    $status = 2;break;
                case 'TERMINATED':
                    $status = 3;break;
                default :
                    $status = 0;break;
            }
            try {
                $falg = $this->db->update('equipment_library')
                        ->TPWhere(['sim_sn' => $vv['iccid']])
                        ->set(['sim_status' => $status])
                        ->query()
                        ->affected_rows();
                var_dump($falg);
            } catch (\Throwable $e) {
                echo '------流量卡状态修改失败--------' . $e->getMessage();
                continue;
            }
        }
        $total_page = ceil($this->count/$this->pageSize);
        $page++;
        if($page<=$total_page){
            $this->setImsStatus($page);
        }
        return true;
    }
    
    /**
     * 查询流量卡详情 并修改数据库状态
     * @param type $page
     * @return boolean
     */
    public function setImsInfo($page) {
        $cards = $this->db->from('equipment_library')
                ->TPWhere(['device_status' => ['<>', 3], 'sim_sn' => ['<>', 'null']])
                ->select('*')
                ->page($this->pageSize, $page)
                ->query()
                ->result_array();
        if (empty($cards)) {
            return true;
        }
        $card_arr = [];
        foreach ($cards as $k => &$v) {
            if (empty($v['sim_sn'])) {
                unset($cards[$k]);
                continue;
            }
            array_push($card_arr, trim($v['sim_sn']));
        }
        $card_str = implode(',', $card_arr);
        $data = $this->getStatus(['iccidList' => $card_str], '/device/batch/info/');
        foreach ($data['data'] as $kk => $vv) {
            if(isset($vv['errorMsg'])){
                var_dump($vv['errorMsg']);
                continue;
            }
            try {
                $falg = $this->db->update('equipment_library')
                        ->TPWhere(['sim_sn' => $vv['iccid']])
                        ->set(['activa_time' => $vv['activeTime'],'expire_time'=>$vv['agreementEndTime'],'seet_meal'=>$vv['rateplan']])
                        ->query()
                        ->affected_rows();
                var_dump($falg);
            } catch (\Throwable $e) {
                echo '------流量卡状态修改失败--------' . $e->getMessage();
                continue;
            }
        }
        $total_page = ceil($this->count/$this->pageSize);
        $page++;
        if($page<=$total_page){
            $this->setImsInfo($page);
        }
        return true;
    }

    protected function onShutDown() {
        get_instance()->clearTimer($this->tick_id);
        // TODO: Implement onShutDown() method.
    }

    /**
     * 调用API 查询流量卡状态
     * @param type $params 参数 ['sn'=>$equipment_number,'osn'=>$order_id,'money'=>$money]
     * @param type $path 路径 '/Water/Water/recharge_payment'
     */
//    public function getStatus333($params = [],$path = '/card/batch/status'){
    public function getStatus($params = [], $path = '/device/info/') {
        if (count($params) <= 1) {
            $keys = $this->is_assoc($params);
            if ($keys === true) {
                $path = '/v1/openapi' . $path . $params[0];
                $path2 = $path;
            } else {
                $path = rtrim($path, '/');
                $path2 = '/v1/openapi' . $path . $keys[0] . $params[$keys[0]];
                $path = $path . '?';
                $path = '/v1/openapi' . $path . $keys[0] . '=' . $params[$keys[0]];
            }
        } else {
            $path = '/v1/openapi' . $path;
            $path = rtrim($path, '/');
            $url = $this->getSign($params);
            $path2 = $path . $url;
            $path = $path . '?' . $url;
        }

        $header = [];
        $header['timestamp'] = time();
        $header['key'] = $this->conf['apiKey'];
        $str = 'GET' . $path2 . $header['timestamp'] . $header['key'] . $this->conf['apiSecret'];
        //var_dump($str);
        $st1 = 'GET/v1/openapi/card/status/89860429161891321670154699741913wmEmNeAyDmx1Df0CIHuYfZrWhneNJMEOOlodNP32C3L7RRr0G0';
        $sha = sha1($str, true);
        $sign = base64_encode($sha);
        //var_dump($sign);
        $header['signature'] = $sign;
        $response = $this->SimState2HttpClient->httpClient
                ->setHeaders($header)
                //->setData('89860429161891321670')
                ->setMethod('GET')
                ->coroutineExecute($path);
        //var_dump($response);
        $result = json_decode($response['body'], true);
        if ($response['statusCode'] == 200 && $result['code'] == 0) {
            echo '成功';
            return $result;
        } else {
            echo '========请求失败!=========='.$result['message'];
        }
    }

    /**
     * 判断是否是索引数组
     * @param type $array
     * @return boolean
     */
    function is_assoc($array) {
        if (is_array($array)) {
            $keys = array_keys($array);
            if ($keys !== array_keys($keys)) {
                return $keys;
            }
            return true;
        }
        return true;
    }

    
    //组装签名数据
    public function getSign($Obj) {
        foreach ($Obj as $k => $v) {
            $Parameters[$k] = $v;
        }
        ksort($Parameters);
        $String = $this->formatBizQueryParaMap($Parameters, false);

        // $String = md5($String);
        $result_ = $String;
        //$result_ = strtoupper($String);

        return $result_;
    }
    //组装签名数据
    function formatBizQueryParaMap($paraMap, $urlencode) {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }

            $buff .= $k . $v;
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }
}
