<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    用户地址  Model
 *    Date: 2018/7/19
 * Class UseraddressModel
 * @package app\Models
 */
class UseraddressModel extends Model
{
    // 表名
    protected $dbName = 'customer_address';

    /**   YSF
     *    查询列表数据
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @param int $page      当前页
     * @param int $pageSize  每页条数
     * @param array $order   排序方式
     * @return mixed
     */
    public function getAll(array $where, string $field = '*', int $page = 1, int $pageSize = 10000, array $order = ['is_default' => 'DESC'])
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**   YSF
     *    地址修改
     * @param array $where 修改条件
     * @param array $data  修改数据
     * @return mixed
     */
    public function editAddress(array $where, array $data)
    {
        $result = $this->db->update($this->dbName)
                    ->set($data)
                    ->TPWhere($where)
                    ->query()
                    ->affected_rows();
        return $result;
    }

    /**   YSF
     *    添加地址
     * @param array $data 添加数据
     * @return mixed
     */
    public function addAddress(array $data)
    {
        $result = $this->db->insert($this->dbName)
                    ->set($data)
                    ->query()
                    ->insert_id();
        return $result;
    }

    /**   YSF
     *    查询单条数据
     * @param array $where  查询条件
     * @param string $field 查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->query()
                    ->row();
        return $result;
    }

}