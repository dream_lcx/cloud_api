<?php
/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: xg
 * @Date: 2022-08-01 09:22:40
 * @LastEditors: xg
 * @LastEditTime: 2022-08-01 09:23:26
 */
namespace app\Models;
use Server\CoreBase\Model;
/**
 *
 */
class WorkOrderReturnVisitModel extends Model
{
    protected $table = 'work_order_return_visit';
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-20
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();   
        return $id;
    }

    /**   YSF
     *    查询列表
     * @param array $where    查询条件
     * @param int $page       当前页
     * @param int $pageSize   每页条数
     * @param array $join     连表
     * @param string $field   查询字段
     * @param array $order    排序方式
     * @param string $dbName  主表
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, array $join, string $field, array $order, string $dbName)
    {
        if($pageSize<0){
             $result = $this->db->select($field)
                    ->from($dbName)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->order($order)
                    ->query()
                    ->result_array();
        }else{
             $result = $this->db->select($field)
                    ->from($dbName)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
        }
       
        return $result;
    }
    /**   YSF
     *    查询列表--不分页
     * @param array $where    查询条件
     * @param array $join     连表
     * @param string $field   查询字段
     * @param array $order    排序方式
     * @param string $dbName  主表
     * @return mixed
     */
    public function getList(array $where,string $field, array $order)
    {
        $result = $this->db->select($field)
                    ->from($this->table)
                    ->TPWhere($where)
                    ->order($order)
                    ->query()
                    ->result_array();
        return $result;
    }
 
 
}