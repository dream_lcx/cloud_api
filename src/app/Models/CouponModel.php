<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   优惠券 模型
 *   Date: 2018/12/4
 * Class CouponModel
 * @package app\Models
 */
class CouponModel extends Model
{
    // 表名
    protected $table = 'coupon';
    protected $alias = 'coupon c';

    // 连表列表查询
    public function getJoinAll(array $where, array $join, int $page, int $pageSize, string $field, array $order)
    {
        $result = $this->db->select($field)
                ->from($this->alias)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();

        return $result;
    }

    // 连表单条查询
    public function getJoinOne(array $where, array $join, string $field)
    {
        $result = $this->db->select($field)
                ->from($this->alias)
                ->TPWhere($where)
                ->TPJoin($join)
                ->query()
                ->row();
        return $result;
    }

    // 单表单条查询
    public function getOne(array $where, string $field='*')
    {
        $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->query()
                ->row();
        return $result;
    }

    //添加优惠券
    public function add(array $data)
    {
        $result = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }

    // 优惠券修改
    public function edit(array $where, array $data)
    {
        $result = $this->db->update($this->table)
                ->set($data)
                ->TPWhere($where)
                ->query()
                ->affected_rows();
        return $result;
    }

    // 单表列表查询
    public function getAll(array $where, string $field='*')
    {
        $result = $this->db->select($field)
                    ->from($this->table)
                    ->TPWhere($where)
                    ->query()
                    ->result_array();
        return $result;
    }

    /**
     * @desc   统计数量
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function count($map,$join=[]){
        $result = $this->db->select('*')
            ->from($this->alias)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }



}