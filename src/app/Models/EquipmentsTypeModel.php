<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   产品分类 模型
 *   Date: 2019/4/22
 * Class EquipmentsTypeModel
 * @package app\Models
 */
class EquipmentsTypeModel extends Model
{
    // 表名
    protected $dbName = 'equipments_type';

    // 列表查询
    public function getAll(array $where, string $field = '*', int $page=1, int $pageSize=-1, array $order = ['sort' => 'DESC', 'id' => 'DESC'])
    {
        if ($pageSize == -1) {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPWhere($where)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }

        return $result;
    }

    // 获取总数量
    public function getCount(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPWhere($where)
                ->query()
                ->num_rows();
        return $result;
    }









}