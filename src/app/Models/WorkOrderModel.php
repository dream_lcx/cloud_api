<?php

namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    工单  Model
 *    Date: 2018/7/18
 * Class WorkOrderModel
 * @package app\Models
 */
class WorkOrderModel extends Model
{
    // 表名
    protected $dbName = 'work_order';

    /**   YSF
     *    获取维修人员我的工单
     * @param array $where 查询条件
     * @param array $join 连表
     * @param int $page 当前页
     * @param int $pageSize 每页条数
     * @param string $field 查询字段
     * @param $order          排序方式
     * @return mixed
     */
    public function getAll(array $where, array $join, int $page, int $pageSize, string $field, $order, $group = '')
    {
        if ($pageSize < 0) {

            if (!empty($group)) {
                $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPJoin($join)
                    ->TPWhere($where)
                    ->groupBy($group)
                    ->order($order)
                    ->query()
                    ->result_array();
            } else {
                $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPJoin($join)
                    ->TPWhere($where)
                    ->order($order)
                    ->query()
                    ->result_array();
            }
        } else {
            if (!empty($group)) {
                $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPJoin($join)
                    ->TPWhere($where)
                    ->groupBy($group)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
            } else {
                $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPJoin($join)
                    ->TPWhere($where)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
            }

        }


        return $result;
    }

    public function getAllByRaw(string $sql)
    {
        return $this->db->query($sql)->result_array();
    }

    public function searchList(array $where, array $join, int $page, int $pageSize, string $field, $order, $con = 'AND')
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPJoin($join)
            ->TPWhere($where, $con)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**   YSF
     *    获取工单详情
     * @param array $where 查询条件
     * @param array $join 连表操作
     * @param string $field 查询字段
     * @return null
     */
    public function getWorkDetail(array $where, array $join = array(), string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPJoin($join)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    /**   YSF
     *    修改工单
     * @param array $where 条件
     * @param array $data 数据
     * @return mixed
     */
    public function editWork(array $where, array $data)
    {
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPWhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param 无
     * @date   2018-07-18
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data)
    {
        $id = $this->db->insert($this->dbName)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**   YSF
     *    统计数量
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return int
     */
    public function getCount(array $where, string $field = '*', array $join = [])
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    /**   YSF
     *    获取订单
     * @param array $where 查询条件
     * @param int $page 当前页
     * @param int $pageSize 每页条数
     * @param array $join 连表
     * @param string $field 查询字段
     * @param array $order 排序方式
     * @param string $dbName 主表
     * @return mixed
     */
    public function workAll(array $where, int $page, int $pageSize, array $join, string $field, array $order, string $dbName = 'work_order a', $group = 'a.work_order_id')
    {
        $result = $this->db->select($field)
            ->from($dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize, $page)
            ->order($order)
            ->groupBy($group)
            ->query()
            ->result_array();

        return $result;
    }

    //查询所有工单，不分页
    public function getAllLists(array $where, array $join, string $field, array $order)
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**   YSF
     *    获取单条数据
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*', array $join = array())
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPJoin($join)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    // 查询一条数据
    public function getSettlementOne($where, $order, $field,  $join)
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPJoin($join)
            ->TPWhere($where,'AND')
            ->order($order)
            ->limit(1)
            ->query()
            ->row();
        return $result;
    }




    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }


}