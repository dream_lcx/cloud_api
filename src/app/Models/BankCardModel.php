<?php

namespace app\Models;

use app\Services\Common\ReturnCodeService;
use Server\CoreBase\Model;


class BankCardModel extends Model
{
    // 表名
    protected $dbName = 'bank_card';

    /**
     *    列表
     * @param array $where 查询条件
     * @param array $join 连表
     * @param int $page 当前页
     * @param int $pageSize 每页条数
     * @param string $field 查询字段
     * @param $order          排序方式
     * @return mixed
     */
    public function getAll(array $where, array $join, int $page, int $pageSize, string $field, $order)
    {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }


        return $result;
    }

    /**
     *    查询单条数据
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*', array $join = [])
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }


    /**
     * @desc   添加信息
     * @param 无
     * @date   2018-07-24
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data)
    {
        $id = $this->db->insert($this->dbName)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * @desc   统计数量
     * @param 无
     * @date   2018-12-6
     * @param array $data [description]
     * @author lcx
     */
    public function count($map, $join = [])
    {
        $result = $this->db->select('*')
            ->from($this->dbName)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-24
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    //添加银行卡
    public function addBankCard($param,$uid,$role)
    {
        if(empty($param) || empty($uid) || empty($role)){
            return returnResult(ReturnCodeService::ILLEGAL_PARAM,'参数错误');
        }
        $checkParam = $this->checkParam($param);
        if ($checkParam['code'] != ReturnCodeService::SUCCESS) {
            return $checkParam;
        }
        //判断银行卡卡号是否重复
        $where = [
            'bank_number' => $param['bank_number'],
            'is_delete' => 0,
            'role' => $role,
            'uid' => $uid
        ];
        $bankNumberInfo = $this->getOne($where, 'bank_id');
        unset($where);
        if (!empty($bankNumberInfo)) {
            return returnResult(ReturnCodeService::FAIL, '银行卡已添加过,请勿重复添加');
        }
        $data = [
            'uid' => $uid,
            'role' => $role,
            'bank_type' => $param['bank_type'] ?? 0,
            'bank_number' => $param['bank_number'],
            'bank_name' => $param['bank_name'],
            'bank_code' => $param['bank_code'],
            'name' => $param['name'],
            'id_card' => $param['id_card'],
            'bank_phone' => $param['bank_phone'],
            'address' => $param['address'],
            'default' => $param['default'] ?? 1,
            'add_time' => time(),
        ];
        $check = false;
        $this->db->begin(function () use (&$check, $data, $role, $uid) {
            //设置默认银行卡
            if ($data['default'] == 1) {
                $this->save(['role' => $role, 'uid' => $uid, 'is_delete' => 0], ['default' => 2]);
            }
            $this->add($data);
            unset($data);
            $check = true;
        }, function ($e) {
            return returnResult(ReturnCodeService::FAIL, '添加失败!' . $e->error);
        });
        if ($check) {
            return returnResult(ReturnCodeService::SUCCESS, '添加成功');
        }
        return returnResult(ReturnCodeService::FAIL, '添加失败');
    }

    //编辑银行卡
    public function editBankCard($param)
    {
        if (empty($param['bank_id'])) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '请选择银行卡');
        }
        $checkParam = $this->checkParam($param);
        if ($checkParam['code'] != ReturnCodeService::SUCCESS) {
            return $checkParam;
        }
        //银行卡信息是否存在
        $bankInfo = $this->getOne(['bank_id' => $param['bank_id']], 'bank_id,role,uid');
        if (empty($bankInfo)) {
            return returnResult(ReturnCodeService::FAIL, '编辑失败!银行卡信息错误');
        }
        //银行卡卡号是否重复
        $where = [
            'bank_number' => $param['bank_number'],
            'is_delete' => 0,
            'role' => $bankInfo['role'],
            'uid' => $bankInfo['uid']
        ];
        $bankNumberInfo = $this->getOne($where, 'bank_id');
        unset($where);
        if (!empty($bankNumberInfo) && $bankNumberInfo['bank_id'] != $param['bank_id']) {
            return returnResult(ReturnCodeService::FAIL, '银行卡已添加过,请勿重复添加');
        }
        $data = [
            'bank_type' => $param['bank_type'] ?? 0,
            'bank_number' => $param['bank_number'] ?? '',
            'bank_name' => $param['bank_name'] ?? '',
            'bank_code' => $param['bank_code'] ?? '',
            'name' => $param['name'] ?? '',
            'id_card' => $param['id_card'] ?? '',
            'bank_phone' => $param['bank_phone'] ?? '',
            'address' => $param['address'] ?? '',
            'default' => $param['default'] ?? '',
            'update_time' => time(),
        ];
        $update_data = [];
        foreach ($data as $k => $v) {
            if (empty($v)) {
                continue;
            }
            $update_data[$k] = $v;
        }
        unset($data);
        if (empty($update_data)) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, "请选择修改内容");
        }
        $check = false;
        $this->db->begin(function () use (&$check, $update_data, $bankInfo) {
            //设置默认银行卡
            if (!empty($update_data['default']) && $update_data['default'] == 1) {
                $this->save(['role' => $bankInfo['role'], 'uid' => $bankInfo['uid'], 'is_delete' => 0], ['default' => 2]);
            }
            $this->save(['bank_id' => $bankInfo['bank_id']], $update_data);
            unset($update_data);
            $check = true;
        }, function ($e) {
            return returnResult(ReturnCodeService::FAIL, '编辑失败!' . $e->error);
        });
        if ($check) {
            return returnResult(ReturnCodeService::SUCCESS, '编辑成功');
        }
        return returnResult(ReturnCodeService::FAIL, '编辑失败');
    }

    //删除银行卡
    public function delBankCard($where)
    {
        if (empty($where)) {
            return returnResult(ReturnCodeService::FAIL, '请选择银行卡');
        }
        $field = 'bank_id';
        $result = $this->getOne($where, $field);
        if (empty($result)) {
            return returnResult(ReturnCodeService::FAIL, '银行卡信息不存在');
        }
        $result = $this->save(['bank_id' => $result['bank_id']], ['is_delete' => 1]);
        if ($result) {
            return returnResult(ReturnCodeService::SUCCESS, '删除成功');
        }
        return returnResult(ReturnCodeService::FAIL, '删除失败');
    }

    //获取银行卡列表
    public function getBankCardList($where, $field = '', $join = [], $page = 1, $pageSize = 10)
    {
        $field = empty($field) ? 'bank_id,bank_type,bank_number, bank_name,bank_code,`name`,id_card,bank_phone,address,`default`,is_delete' : $field;
        $order = [
            'add_time' => 'DESC',
            'update_time' => 'DESC',
        ];
        $result = $this->getAll($where, $join, $page, $pageSize,$field,$order);
        if (empty($result)) {
            return returnResult(ReturnCodeService::NO_DATA, '暂无数据');
        }
        $count = $this->count($where, $join);
        $allPage = ceil($count / $pageSize);
        return returnResult(ReturnCodeService::SUCCESS, '获取成功', $result, $count, $allPage, $page, $pageSize);
    }

    //获取银行卡信息
    public function getBankCardDetail($where, $field = '*')
    {
        if (empty($where)) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '缺少搜索参数');
        }
        $field = empty($field) ? 'bank_id,bank_type,bank_number,bank_name,bank_code,`name`,id_card,bank_phone,address,is_delete, `default`' : $field;
        $result = $this->getOne($where, $field);
        if (empty($result)) {
            return returnResult(ReturnCodeService::FAIL, '银行卡信息错误');
        }
        return returnResult(ReturnCodeService::SUCCESS, '获取成功', $result);
    }

    public function checkParam($param)
    {
        if (empty($param)) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '缺少参数');
        }
        if (empty($param['name'])) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '请填写姓名');
        }
        if (empty($param['bank_name'])) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '请选择开户行');
        }
        if (empty($param['bank_code'])) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '缺少参数bank_code');
        }
        if (empty($param['bank_number']) || !checkBankNo($param['bank_number'])) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '请填写正确的银行卡号');
        }
        if (empty($param['bank_phone']) || !isMobile($param['bank_phone'])) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '请填写正确的手机号码');
        }
        if (empty($param['id_card']) || !checkIdCard($param['id_card'])) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '请输入正确的身份证号码');
        }
        if (empty($param['address'])) {
            return returnResult(ReturnCodeService::ILLEGAL_PARAM, '请填写开户地');
        }
        return returnResult(ReturnCodeService::SUCCESS, '校验通过');
    }


}