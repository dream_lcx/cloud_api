<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   文章分类模型
 *   Date: 2019/1/23
 * Class ArticleCateModel
 * @package app\Models
 */
class ArticleCateModel extends Model
{
    // 表名
    protected $name = 'article_cate';

}