<?php
namespace app\Models;
use Noodlehaus\Exception;
use Server\CoreBase\Model;
use Server\CoreBase\SwooleException;
use Throwable;

class EquipmentsPackageModel extends Model
{
    protected $table = 'equipments_package';

    /**
     * @desc  查询一条数据
     * @param array $where [description]
     * @param string $field [description]
     * @return null [type]            [description]
     * @throws SwooleException
     * @throws Throwable
     * @date   2018-07-18
     * @author lcx
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    /**   YSF
     *    设备分类列表--分页
     * @param array $where 查询条件
     * @param int $page 当前页码
     * @param int $pageSize 每页数量
     * @param string $field 查询字段
     * @param array $order 排序方式
     * @param array $join
     * @return mixed
     * @throws SwooleException
     * @throws Throwable
     */
    public function getAll(array $where, int $page, int $pageSize, string $field,  array $order,array $join)
    {
        if(empty($field)){
            $field = '*';
        }
        $data =  $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize, $page);
        if(!empty($order)){
            $data->order($order);
        }
        $data->query()->result_array();
        return $data;
    }

    public function selectData(array $where = [],string $field = '*',array $join = [],array $order = [],string $group='')
    {
        if(!empty($group)){
            $result = $this->db
                ->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->order($order)
                ->groupBy($group)
                ->query()
                ->result_array();
            return $result;
        }else{
            $result = $this->db
                ->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->order($order)
                ->query()
                ->result_array();
            return $result;
        }

    }

    public function findData(array $where,string $field ='*',array $order = [])
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->order($order)
            ->query()
            ->row();
        return $result;
    }


}