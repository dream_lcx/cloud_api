<?php

namespace app\Models;

use Server\CoreBase\Model;

class MessageLogModel extends Model
{
    // 短信发送日志
    protected $table = 'message_log';

    // 获取一条信息
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    // 统计
    public function count($map, $join=[]) {
        $result = $this->db->select('*')
            ->from($this->table)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }


    // 添加短信
    public function add(array $data){
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

}