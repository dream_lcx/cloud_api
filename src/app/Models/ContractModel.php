<?php
/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: xg
 * @Date: 2021-03-15 17:33:35
 * @LastEditors: xg
 * @LastEditTime: 2022-05-09 17:25:56
 */
namespace app\Models;
use Noodlehaus\Exception;
use Server\CoreBase\Model;
use Server\CoreBase\SwooleException;
/**
 * 合同
 */
class ContractModel extends Model
{
    protected $table = 'contract';
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();       
        return $id;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }
     /**    lcx
     *     分页查询
     * @param array $where
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, string $field = '*',array $order = ['add_time' => 'DESC'],int $page=1, int $pageSize=-1, array $join = [])
    {

        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }
        return $result;

    }
   
     //修改合同状态
     public function editContractStatus($user_id,$status,$remark){
        $contract =  $this->getAll(['user_id'=>$user_id,'status'=>['IN',[4,5]]],'contract_id');
        if(empty($contract)){
            return true;
        }
        $contractLogModel =  $this->loader->model('ContractLogModel', $this);;
        foreach($contract as $key=>$val){
            $data['contract_id'] = $val['contract_id'];
            $data['do_id'] = 0;
            $data['do_type'] = 3;
            $data['terminal_type'] = 0;
            $data['do_time'] = time();
            $data['remark'] = $remark;
            $contractLogModel->add($data);
            $this->save(['contract_id'=>$val['contract_id']],['status'=>$status]);
        }
        return true;
    }
    /**
     * @desc   统计数量
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function count($map,$join=[]){
        $result = $this->db->select('*')
            ->from($this->table)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }


 
}