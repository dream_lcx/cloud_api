<?php
namespace app\Models;

use app\Library\SLog;
use app\Services\Common\ConfigService;
use app\Services\Common\HttpService;
use app\Services\Common\NotifiyService;
use Server\CoreBase\Model;

/**   YSF
 *    续费订单 Model
 *    Date: 2018/8/3
 * Class RenewOrderModel
 * @package app\Models
 */
class RenewOrderModel extends Model
{
    // 表名
    protected $dbName = 'renew_order';
    protected $contract_model;
    protected $contract_log_model;
    protected $contract_ep_model;
    protected $renew_eq_model;
    protected $Achievement;
    protected $finance_record_model;
    protected $renew_record_model;
    protected $equipmentListsModel;
    protected $user_model;
    protected $GetIPAddressHttpClient;
    public function __construct()
    {
        parent::__construct();
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->contract_log_model = $this->loader->model('ContractLogModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->renew_eq_model = $this->loader->model('RenewOrderEquipmentModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->finance_record_model = $this->loader->model('FinanceRecordModel', $this);
        $this->renew_record_model = $this->loader->model('RenewRecordModel', $this);
        $this->equipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
    }
    /**   YSF
     *    添加一条订单
     * @param array $data 数据
     * @return mixed
     */
    public function add(array $data)
    {
        $result = $this->db->insert($this->dbName)
                    ->set($data)
                    ->query()
                    ->insert_id();
        return $result;
    }

    /**   YSF
     *    单条查询
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*',$join=[], $order = [])
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->order($order)
                    ->query()
                    ->row();
        return $result;
    }

    /**
     * @param array $where 修改条件
     * @param array $data  数据
     * @return mixed
     */
    public function edit(array $where, array $data)
    {

        $result = $this->db->update($this->dbName)
                    ->set($data)
                    ->TPWhere($where)
                    ->query()
                    ->affected_rows();
        return $result;
    }


    public function getAll(array $where, int $page=1, int $pageSize=10, string $field = '*', array $join=[],array $order = ['renew_time' => 'DESC'])
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize,$page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }
    /**
     * @desc   续费金额为0时回调
     * @param order_sn string 必选 订单编号
     * @param order_money string 必选 金额
     * @param openid string 必选 工程openid
     * @date   2020-6-19
     * @return [type]     [description]
     * @author lcx
     */
    public function renewCallback($param)
    {
        $order_sn = $param['order_sn'];
        $total_fee = $param['order_money'];
        $join = [
            ['contract c', 'c.contract_id=rq_renew_order.contract_id', 'left']
        ];
        //判断锁
        $renewCallbackLock = $this->redis->hGet('renewCallbackLock', $order_sn);
        if (!empty($renewCallbackLock)) {
            return false;
        }
        //获取续费订单信息
        $order = $this->getOne(['renew_order_no' => $order_sn], 'rq_renew_order.*,c.company_id', $join);

        //获取用户信息
        $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,openid,telphone');
        // 获取合同信息
        $contract_info = $this->contract_model->getOne(['contract_id' => $order['contract_id']]);
        $status = false;
        $exire_date = $contract_info['exire_date'];

       // $this->db->begin(function () use ($order, &$status, $contract_info, &$exire_date, $user_info, $total_fee, $order_sn) {
            $this->redis->hSet('renewCallbackLock', $order_sn, json_encode($order));//加锁
            $this->renew($order, $contract_info, $user_info, $total_fee);
            $status = true;
        //});
        $this->redis->hDel('renewCallbackLock', $order_sn);//解锁
        if ($status) {
            //开始续费结算和发送模板消息
            $this->sendRenewMsg($order, $contract_info, $user_info, $exire_date);
            return true;
        }
        return false;

    }

    //续费公共处理方法
    public function renew(array $order, array $contract_info, array $user_info, $total_fee)
    {
        // 1-- 修改订单状态
        $this->edit(['renew_order_id' => $order['renew_order_id']], ['status' => 3, 'pay_status' => 2, 'pay_time' => time(), 'renew_time' => time(), 'wx_callback_num' => $order['wx_callback_num']]);
        // 2-- 修改合同信息
        $exire_date = $contract_info['exire_date'] + ($order['package_cycle'] * 60 * 60 * 24);  // 到期时间延后
        $contract['exire_date'] = $exire_date;
        $contract['real_exire_date'] = $exire_date;
        if ($exire_date > time()) {
            $contract['status'] = 4;
        }
        $this->contract_model->save(['contract_id' => $order['contract_id']], $contract);
        //添加合同日志
        $this->addContractLog($order['contract_id'], 3, "【用户端】用户【" . $user_info['username'] . "】【用户ID:" . $order['user_id'] . "】续费成功,修改合同到期时间为" . date('Y-m-d H:i:s', $exire_date), $order['user_id']);
        //查询合同设备信息
        $contract_ep_info = $this->contract_ep_model->getAll(array('contract_id' => $order['contract_id']), 'equipment_id');
        if (empty($contract_ep_info)) {
            //throw new SwooleException("该合同下无正常运行的设备,无法续费");
            return false;
        }
        //添加续费-设备关联表
        foreach ($contract_ep_info as $key => $value) {
            $eq_data = [];
            $eq_data['renew_order_id'] = $order['renew_order_id'];
            $eq_data['equipment_id'] = $value['equipment_id'];
            $eq_data['create_time'] = time();
            $this->renew_eq_model->add($eq_data);
        }
        //添加续费记录
        $record = [];
        $record['renew_order_id'] = $order['renew_order_id'];
        $record['user_id'] = $order['user_id'];
        $record['contract_id'] = $order['contract_id'];
        $record['remarks'] = '用户合同续费，微信支付';
        $record['renew_record_time'] = time();
        $this->renew_record_model->add($record);

        //优惠券
        if (!empty($order['coupon_id'])) {
            $coupon_id = explode(',', $order['coupon_id']);
            $update_coupon = [
                'use_time' => time(),
                'status' => 2,
                'order_total_sn' => $order['renew_order_no'],
                'use_type' => 2,
                'order_id' => $order['renew_order_id'],
            ];
            $this->Achievement->table = 'coupon';
            $this->Achievement->updateData(['coupon_id' => ['IN', $coupon_id]], $update_coupon);

            $tmp = [
                'coupon_id' => '',
                'use_type' => 1,
                'note' => '用户合同续费[编号：' . $contract_info['contract_no'] . ']',
                'add_time' => time(),
            ];
            $this->Achievement->table = 'coupon_log';
            foreach ($coupon_id as $key => $value) {
                $tmp['coupon_id'] = $value;
                $this->Achievement->insertData($tmp);
            }
        }
        //账户抵扣-扣取账户余额
        if(!empty($order['deduction_account_balance']) && $order['deduction_account_balance']>0){
            //扣取账户
            $this->Achievement->table = 'oa_workers';
            $workers = $this->Achievement->findData(['user_id'=>$order['user_id']],'cash_available,workers_id');
            $cash_available = $workers['cash_available']-$order['deduction_account_balance'];
            $this->Achievement->updateData(['user_id'=>$order['user_id']],['cash_available'=>$cash_available]);
            //写入资金变动记录
            $funds_record = [
                'user_id' => $workers['workers_id'],
                'user_type' => 1,
                'source' => 5,
                'account_type' => 1,
                'order_id' => $order['renew_order_id'],
                'money' => $order['deduction_account_balance'],
                'note' => '续费单号:'.$order['renew_order_no'],
                'add_time' => time(),
            ];
            $this->Achievement->table = 'funds_record';
            $this->Achievement->insertData($funds_record);
        }
        //代付账户抵扣-扣取账户余额
        if(!empty($order['paying_agent_deduction_account_balance']) && $order['paying_agent_deduction_account_balance']>0){
            if($order['paying_agent_role']==2){
                //扣取账户
                $this->Achievement->table = 'oa_workers';
                $workers = $this->Achievement->findData(['user_id'=>$order['paying_agent_user_id']],'cash_available,workers_id');
                $cash_available = $workers['cash_available']-$order['paying_agent_deduction_account_balance'];
                $this->Achievement->updateData(['user_id'=>$order['paying_agent_user_id']],['cash_available'=>$cash_available]);
                //写入资金变动记录
                $funds_record = [
                    'user_id' => $workers['workers_id'],
                    'user_type' => 1,
                    'source' => 7,
                    'account_type' => 1,
                    'order_id' => $order['renew_order_id'],
                    'money' => $order['paying_agent_deduction_account_balance'],
                    'note' => '续费单号：'.$order['renew_order_no'].',代付发起人:'.$user_info['username'].'(联系电话:'.$user_info['telphone'].')',
                    'add_time' => time(),
                ];
                $this->Achievement->table = 'funds_record';
                $this->Achievement->insertData($funds_record);
            }

        }

//                    $payServiceFeeResult = HttpService::requestBossApi(['company_id' => $contract_info['company_id'], 'equipment_id' => array_column($contract_ep_info, 'equipment_id'), 'type' => 2, 'user_id' => $order['user_id'], 'contract_id' => $order['contract_id']], '/api/Equipment/multPayServiceFee');
//                    if ($payServiceFeeResult['code'] != 1000) {//扣费失败,直接抛出异常
//                        throw new SwooleException("扣费失败" . $payServiceFeeResult['msg']);
//                    }
        //续费设备--下发套餐--数据同步指令
        $renew_eq_map['renew_order_id'] = $order['renew_order_id'];
        $renew_eq_join = [
            ['equipment_lists as el', 'rq_renew_order_equipment.equipment_id = el.equipment_id', 'LEFT'],
            ['equipments as e', 'e.equipments_id = el.equipments_id', 'LEFT'],
        ];
        $renew_eq_data = $this->renew_eq_model->getAll($renew_eq_map, 'el.equipment_id,el.device_no,e.type,el.used_days,el.used_traffic,el.remaining_days,el.remaining_traffic', $renew_eq_join);
        if (!empty($renew_eq_data)) {
            foreach ($renew_eq_data as $k => $v) {
                //修改设备表租赁时间
                $this->equipmentListsModel->updateEquipmentLists(array('end_time' => $contract['exire_date']), array('equipment_id' => $v['equipment_id']));
                if ($v['type'] == 1) {//如果是智能设备
                    $exire_date = strtotime(getMonthLastDay($contract['exire_date']));   //获取签约月份最后一天
                    $data_sync_params['sn'] = $v['device_no'];
                    $data_sync_params['used_days'] = $v['used_days'];
                    $data_sync_params['used_traffic'] = $v['used_traffic'];
                    $data_sync_params['remaining_days'] = $v['remaining_days'];
                    $data_sync_params['remaining_traffic'] = $v['remaining_traffic'];
                    if ($order['package_mode'] == 2) {
                        $data_sync_params['remaining_traffic'] = $order['package_value'] + $v['remaining_traffic'];
                    } else if ($order['package_mode'] == 1) {
                        $data_sync_params['remaining_days'] = ceil(($exire_date - time()) / 86400);
                    }
                    $data_sync_path = '/House/Issue/data_sync';
                    HttpService::Thrash($data_sync_params, $data_sync_path);
                    sleepCoroutine(2000);
                    //请求心跳
                    $state_params['sn'] = $v['device_no'];
                    $state_path = '/House/Issue/heartbeat';
                    HttpService::Thrash($state_params, $state_path);
                } else {
                    continue;
                }
            }
        }
        //新增资金记录
        $finance_record['type'] = 3;
        $finance_record['renew_order_id'] = $order['renew_order_id'];
        $finance_record['user_id'] = $order['user_id'];
        $finance_record['money'] = $total_fee;
        $finance_record['total_money'] = formatMoney($order['order_total_money'],1);
        $finance_record['coupon_money'] = formatMoney($order['coupon_total_money'],1);
        $finance_record['deduction_account_balance'] = formatMoney($order['deduction_account_balance'],1)+formatMoney($order['paying_agent_deduction_account_balance'],1);
        $finance_record['create_time'] = time();
        $finance_record['is_online_pay'] = 1;
        $finance_record['pay_way'] = 1;
        $finance_record['callback_num'] = $order['wx_callback_num'];
        $finance_record['payment_method'] = 1;
        $finance_record['payment_uid'] = $order['user_id'];
        $finance_record['o_id'] = $contract_info['operation_id'];
        $finance_record['a_id'] = $contract_info['administrative_id'];
        $finance_record['company_id'] = $contract_info['company_id'];
        $this->finance_record_model->add($finance_record);

    }
    /**
     * @desc   添加合同操作日志
     * @param $status 1增2删3改4解绑'
     * @date   2018-07-19
     * @author lcx
     * @return [type]     [description]
     */
    public function addContractLog($contract_id, $status = 1, $remark, $user_id)
    {
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $this->contract_log_model->add($data);
        return $res;
    }

    //结算和发送模板消息
    public function sendRenewMsg(array $order, array $contract_info, array $user_info, $exire_date)
    {
        //开始续费结算
        $this->RenewalSettlement($contract_info['contract_no'], $order['renew_order_id']);

        // 模板信息
        $exire_date = date('Y-m-d H:i:s', $exire_date);
        $now = date('Y-m-d H:i:s', $order['create_time']);
        $package_mode = '时长模式';
        $package = $package_mode . '(' . $order['package_value'] . '天)';
        if ($order['package_mode'] == 2) {
            $package_mode = '流量模式';
            $package = $package_mode . '(' . $order['package_value'] . 'ML)';
        }
        $user_notice_config = ConfigService::getTemplateConfig('renew_success', 'user', $order['company_id']);
        if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
            $template_id = $user_notice_config['template']['wx_template_id'];
            $mp_template_id = $user_notice_config['template']['mp_template_id'];
            // 模板内容
            $template_data = [
                'keyword1' => ['value' => $order['renew_order_no']], //订单号
                'keyword2' => ['value' => $order['package_cycle']], // 续费时长
                'keyword3' => ['value' => $package], // 续费类型
                'keyword4' => ['value' => $exire_date], //到期时间
                'keyword5' => ['value' => $now], //续费时间
                'keyword6' => ['value' => $order['order_actual_money']], // 金额
                'keyword7' => ['value' => '您的续费已完成。感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾,合同编号:' . $contract_info['contract_no']], // 备注
            ];
            $weapp_template_keyword = '';
            $mp_template_data = [
                'first' => ['value' => '恭喜!您已成功续费', 'color' => '#4e4747'],
                'keyword1' => ['value' => "合同续费(合同编号:" . $contract_info['contract_no'] . ")", 'color' => '#4e4747'],
                'keyword2' => ['value' => $exire_date, 'color' => '#4e4747'],
                'remark' => ['value' => '感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾!若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']), 'color' => '#173177'],
            ];
            $template_tel = $user_info['telphone'];  // 电话
            $template_content = '您的续费的' . $package . '已成功，续费时长为' . $order['package_cycle'] . '，续费金额为' . $order['order_actual_money'] . '。打开微信进入小程序可查看详情';
            NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, '', $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $order['company_id']);
        }

    }


    /**
     * 续费结算
     * @param string $contract_no
     * @date 2019/1/25 10:41
     * @author ligang
     */
    public function RenewalSettlement(string $contract_no, $renew_order_id)
    {
        if(!$this->config['system_auto_settle']){
            return true;//已关闭自动结算直接返回
        }
        $data = [
            'contract_no' => $contract_no,
            'type' => 2,
            'from_renew_order_id' => $renew_order_id
        ];
        $json = json_encode($data);
        $response = $this->GetIPAddressHttpClient->httpClient
            ->setData($json)
            ->setMethod('post')
            ->coroutineExecute('/Achievement/Settlement/balance');
        $is_success = true;
        if ($response['statusCode'] == 200) {
            $body = json_decode($response['body'], 1);
            if ($body['code'] != 1000) {
                //结算失败
                $is_success = false;
            }
        } else {
            $is_success = false;
        }
        if (!$is_success) {
            //失败写入日志
            SLog::SL(SLog::CONTRACT_LOG, $contract_no)->info(__FUNCTION__, [$contract_no, $response]);
        }
    }


}
