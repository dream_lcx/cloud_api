<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    订单评价 Model
 *    Date: 2018/7/23
 * Class EvaluateModel
 * @package app\Models
 */
class EvaluateModel extends Model
{
    // 表名
    protected $dbName = 'evaluate';

    /**   YSF
     *    添加评论
     * @param array $data  数据
     * @return mixed
     */
    public function add(array $data)
    {
        $result = $this->db->insert($this->dbName)
                    ->set($data)
                    ->query()
                    ->insert_id();
        return $result;
    }

    /**    YSF
     *     获取评价列表
     * @param array $where     查询条件
     * @param array $join      连表
     * @param int $page        当前页
     * @param int $pageSize    每页条数
     * @param string $field    查询字段
     * @param array $order     排序方式
     * @param string $dbName   主表名
     * @return mixed
     */
    public function getAll(array $where, array $join, int $page, int $pageSize, string $field, array $order, string $dbName)
    {
        $result = $this->db->select($field)
                    ->from($dbName)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->order($order)
                    ->page($pageSize, $page)
                    ->query()
                    ->result_array();
        return $result;
    }

    /**   YSF
     *    获取评论总数
     * @param array $where    查询条件
     * @param array $join     连表
     * @param string $field   查询字段
     * @param string $dbName  主表
     * @return int
     */
    public function getCount(array $where, array $join=array(), string $field='*', string $dbName='evaluate')
    {
        $result = $this->db->select($field)
                    ->from($dbName)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->query()
                    ->num_rows();
        return $result;
    }

    // 不连表获取多条
    public function getOne(array $where, string $field='*')
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->query()
                    ->row();
        return $result;
    }

    // 连表查询多条
    public function getJoinAll(array $where, string $field='*', array $join, $dbName)
    {
        $result = $this->db->select($field)
                    ->from($dbName)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->query()
                    ->result_array();
        return $result;
    }

}