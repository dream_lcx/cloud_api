<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 2020/2/21
 * Time: 14:30
 */
namespace app\Models;

use Server\CoreBase\Model;

/**
 * 版本模型
 */
class VersionModel extends Model
{
    protected $table = 'version';

    /**
     * @desc  查询一条用户数据
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param string $field [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function getOne(array $where, string $field = "*")
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    /**    lcx
     *     分页查询
     * @param array $where
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, string $field = '*', array $order = ['user_id' => 'ASC'], int $page = 1, int $pageSize = -1)
    {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->page($pageSize, $page)
                ->query()
                ->result_array();
        }

        return $result;
    }

    /**
     *    数量查询
     * @param array $where
     * @param string $field
     * @return int
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     */
    public function getCount(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->num_rows();
        return $result;
    }


}