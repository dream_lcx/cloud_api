<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    动态通知 Model
 *    Date: 2018/7/24
 * Class DynamicModel
 * @package app\Models
 */
class DynamicModel extends Model
{
    // 表名
    protected $dbName = 'dynamic';

    /**   YSF
     *    列表查询
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @param array $order   排序方式
     * @return mixed
     */
    public function getAll(array $where, string $field = '*', array $order = ['create_time' => 'DESC'])
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->order($order)
                    ->query()
                    ->result_array();
        return $result;
    }

    /**   YSF
     *    单条查询
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
                        ->from($this->dbName)
                        ->TPWhere($where)
                        ->query()
                        ->row();
        return $result;
    }

}