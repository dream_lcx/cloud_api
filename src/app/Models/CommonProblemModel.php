<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   常见问题 模型
 *   Date: 2019/3/6
 * Class CommonProblemModel
 * @package app\Models
 */
class CommonProblemModel extends Model
{
    // 表名
    protected $name = 'common_problem';
    protected $join_name = 'common_problem as a';

    // 连表列表查询
    public function getJoinAll(array $where=[],array $join=[],string $field='*',int $page=1,int $pageSize=99999,array $order=[])
    {
        $value = $this->db->select($field)
                    ->from($this->join_name)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
        return $value;
    }

    // 单表列表查询
    public function getAll(array $where=[],string $field='*',int $page=1,int $pageSize=99999,array $order=[])
    {
        $value = $this->db->select($field)
                    ->from($this->name)
                    ->TPWhere($where)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
        return $value;
    }

    // 单表单条查询
    public function getOne(array $where=[],string $field='*')
    {
        $value = $this->db->select($field)
                    ->from($this->name)
                    ->TPWhere($where)
                    ->query()
                    ->row();
        return $value;
    }








}