<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 用户模型
 */
class ContractPackageRecordModel extends Model
{
    protected $table = 'contract_package_record';
    /**
     * @desc  查询一条用户数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }
    /**
     * @desc   添加用户信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();       
        return $id;
    }
    /**
     * @desc  更新用户信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }

    /**
     *
     * @author ligang
     * @param array $where
     * @param string $field
     * @return null
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/29 10:53
     */
    public function selectContractPackageRecord(array $where,string $field = '*',string $table = '')
    {
        $result = $this->db
            ->select($field)
            ->from(!empty($table)?$table:$this->table)
            ->TPWhere($where)
            ->query()
            ->result_array();
        return $result;
    }
 
}