<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    意见标签（类型） Model
 *    Date: 2018/8/2
 * Class OpinionTypeModel
 * @package app\Models
 */
class OpinionTypeModel extends Model
{
    // 表名
    protected $dbName = 'opinion_type';

    /**   YSF
     *    列表查询
     * @param array $order  排序方式
     * @param string $field 查询字段
     * @return mixed
     */
    public function getAll(array $order, string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

}