<?php

namespace app\Models;

use Server\CoreBase\Model;

class EngineersAdministrativeCenterModel extends Model
{
    // 表名
    protected $table = 'engineers_administrative_center';

    public function getOne(array $where, string $field = "*")
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    public function getAll(array $where, string $field = '*', array $join = [], int $page = 1, int $pageSize = -1, array $order = [])
    {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPJoin($join)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPJoin($join)
                ->TPWhere($where)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }

        return $result;
    }
}