<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    意见反馈  Model
 *    Date: 2018/8/2
 * Class OpinionFeedback
 * @package app\Models
 */
class OpinionFeedback extends Model
{
    // 表名
    protected $dbName = 'opinion_feedback';

    /**   YSF
     *    添加操作
     * @param array $data 数据
     * @return mixed
     */
    public function add(array $data)
    {
        $result = $this->db->insert($this->dbName)
                    ->set($data)
                    ->query()
                    ->insert_id();
        return $result;
    }

}