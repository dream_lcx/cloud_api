<?php

namespace app\Models;

use Server\CoreBase\Model;
use Server\CoreBase\SwooleException;
use Throwable;

class BasicModel extends Model
{

    /**
     * 查询一条数据
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return null
     * @throws SwooleException
     * @throws Throwable
     */
    public function find(array $where, string $field="*"){
        return $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
    }


}