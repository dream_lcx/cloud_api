<?php

/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2018/7/20
 * Time: 17:31
 */

namespace app\Models;

use Server\CoreBase\Model;

class EquipmentListsModel extends Model
{

    protected $equipment_lists = 'equipment_lists';
    protected $equipment_water_record = 'equipment_water_record';
    protected $equipment_library = 'equipment_library';

    //计数
    public function count(array $map, array $join = [])
    {
        $result = $this->db->select('*')
            ->from($this->equipment_lists)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    /**
     * 查询设备表明
     * @param array $where
     * @param string $field
     * @param array $order
     * @return mixed
     * @throws \Throwable
     * @date 2018/8/22 10:52
     * @author ligang
     */
    public function selectEquipmentLists(array $where, string $field = '*', array $order = [], string $group = "", array $join = [])
    {

        $result = $this->db
            ->from($this->equipment_lists)
            ->select($field)
            ->TPWhere($where)
            ->TPJoin($join)
            ->order($order)->groupBy($group)
            ->query()
            ->result_array();
        return $result;
    }


    public function selectRawEquipmentLists(array $where, string $field = '*', array $order = [], string $group = "", array $join = [],$whereRaw = '')
    {

        $result = $this->db
            ->from($this->equipment_lists)
            ->select($field)
            ->TPWhere($where)
            ->TPWhere($whereRaw)
            ->TPJoin($join)
            ->order($order)->groupBy($group)
            ->query()
            ->result_array();
        return $result;
    }

    /**
     * 关联查询设备表
     * @param array $where
     * @param array $join
     * @param string $field
     * @param array $order
     * @return mixed
     * @throws \Exception
     * @date 2018/7/26 13:48
     * @author ligang
     */
    public function selectJoinEquipmentLists(array $where, array $join, string $field = '*', array $order, int $page, int $pageSize)
    {

        $result = $this->db
            ->from($this->equipment_lists)
            ->select($field)
            ->TPWhere($where)
            ->order($order)
            ->TPJoin($join)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**
     * 查询单条设备信息
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @param string $co 查询类型
     * @return null
     * @throws \Exception
     * @date 2018/7/20 17:34
     * @author ligang
     */
    public function findEquipmentLists(array $where, string $field = '*', string $co = 'AND', array $join = array())
    {
        $result = $this->db
            ->select($field)
            ->from($this->equipment_lists)
            ->TPWhere($where, $co)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }

    /**
     * 查询单条设备库信息
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @param string $co 查询类型
     * @return null
     * @throws \Exception
     * @date 2018/7/20 17:34
     * @author ligang
     */
    public function findEquipmentLibrary(array $where, string $field = '*', string $co = 'AND')
    {

        $result = $this->db
            ->select($field)
            ->from($this->equipment_library)
            ->TPWhere($where, $co)
            ->query()
            ->row();
        return $result;
    }

    /**
     * 更新
     * @param array $data
     * @param array $where
     * @return mixed
     * @throws \Exception
     * @date 2018/7/23 9:47
     * @author ligang
     */
    public function updateEquipmentLists(array $data, array $where)
    {
        $result = $this->db
            ->update($this->equipment_lists)
            ->set($data)
            ->TPWhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * 更新设备库
     * @param array $data
     * @param array $where
     * @return mixed
     * @throws \Exception
     * @date 2018/7/23 9:47
     * @author ligang
     */
    public function updateEquipmentLibrary(array $data, array $where)
    {
        $result = $this->db
            ->update($this->equipment_library)
            ->set($data)
            ->TPWhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * 插入用水量
     * @param array $data
     * @return mixed
     * @date 2018/7/23 17:22
     * @author ligang
     */
    public function insertEquipmentWaterRecord(array $data)
    {
        $result = $this->db
            ->insert($this->equipment_water_record)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }

    /**
     * 插入设备
     * @param array $data
     * @return mixed
     * @date 2018/7/23 17:22
     * @author ligang
     */
    public function insertEquipmentLists(array $data)
    {
        $result = $this->db
            ->insert($this->equipment_lists)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }

    /**   YSF
     *    获取设备动态
     * @param array $where 查询条件
     * @param int $page 当前页
     * @param int $pageSize 每页条数
     * @param array $join 连表
     * @param string $field 查询字段
     * @param array $order 排序方式
     * @param string $dbName 主表
     * @return mixed
     */
    public function equipmentListsDynamic(array $where, int $page, int $pageSize, array $join, string $field, array $order, string $dbName)
    {
        $result = $this->db->select($field)
            ->from($dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**   YSF
     *    连表获取单条设备信息
     * @param array $where 查询字段
     * @param array $join 连表
     * @param string $field 查询字段
     * @param string $dbName 主表
     * @return null
     */
    public function getOneJoin(array $where, array $join, string $field, string $dbName)
    {
        $result = $this->db->select($field)
            ->from($dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }

    // 获取单条设备
    public function getOne(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->equipment_lists)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    /**
     * 确认设备保修时间--如果未做任何修改直接返回TRUE
     * @param array $equipment_id 设备ID
     * @param int $start 保修开始时间
     * @param int $end 保修结束时间
     */
    public function confirmWarrantyTime(int $equipment_id, int $start, int $end)
    {
        //查询设备信息
        $info = $this->db->select('start_time,end_time')
            ->from($this->equipment_lists)
            ->TPWhere(array('equipment_id' => $equipment_id))
            ->query()
            ->row();
        if (empty($info)) {
            return false;
        }
        if ($info['start_time'] != $start) {
            $arr['start_time'] = $start;
        }
        if ($info['end_time'] != $end) {
            $arr['end_time'] = $end;
        }
        if (empty($arr)) {
            return false;
        }
        return $this->db->update($this->equipment_lists)
            ->set($arr)
            ->TPWhere(array('equipment_id' => $equipment_id))
            ->query()
            ->affected_rows();
    }

    /**
     * 物理删除数据
     * @param array $where
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/4/19 11:30
     * @author ligang
     */
    public function delete(array $where)
    {
        $result = $this->db
            ->from($this->equipment_lists)
            ->TPWhere($where)
            ->delete()
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->equipment_lists)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    public function getDeviceNo($sn)
    {
        //查询设备码信息 新增支持设备码扫描
        $qr_info = $this->db->select('id,device_no')
            ->from('equipment_qrcode')
            ->TPWhere(['qrcode_no' => $sn, 'status' => 1])
            ->query()
            ->row();
        if (!empty($qr_info) && !empty($qr_info['device_no'])) {
            $sn = $qr_info['device_no'];
        }
        return $sn;
    }


    /**
     * 处理TDS值 2020-10-12处理
     * @param $tds 设备上报的tds值
     * @return int
     */
    public function handleTds($tds,$redis_tds=0)
    {
        $is_rand = false;
        //若tds值小于200时取5-10随机数;大于200时取40-45随机数
        if ($tds>30) {
            $tds = empty($redis_tds)?rand(15,25):$redis_tds;
            $is_rand = empty($redis_tds)?true:false;;
        } else if ($tds == 0) {
            $tds = empty($redis_tds)?rand(6,15):$redis_tds;
            $is_rand = empty($redis_tds)?true:false;;
        }
        return ['tds'=>$tds,'is_rand'=>$is_rand];
    }

}
