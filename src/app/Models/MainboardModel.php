<?php

namespace app\Models;

use Server\CoreBase\Model;

class MainboardModel extends Model
{
    protected $table = 'mainboard';

    public function getAll(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->result_array();
        return $result;
    }
}