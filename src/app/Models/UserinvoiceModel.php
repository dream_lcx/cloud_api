<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    用户发票  Model
 *    Date: 2018/7/20
 * Class UserinvoiceModel
 * @package app\Models
 */
class UserinvoiceModel extends Model
{
    // 表名
    protected $dbName = 'customer_invoice';

    /**   YSF
     *    修改发票
     * @param array $where
     * @param array $data
     * @return mixed
     */
    public function editInvoice(array $where, array $data)
    {
        $result = $this->db->update($this->dbName)
                    ->set($data)
                    ->TPWhere($where)
                    ->query()
                    ->affected_rows();
        return $result;
    }

    /**   YSF
     *    查询单个发票
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @return null
     */
    public function getOne(array $where, string $field)
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->query()
                    ->row();
        return $result;
    }

    /**   YSF
     *    发票列表
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @param int $page      当前页
     * @param int $pageSize  每页条数
     * @param array $order   排序方式
     * @return mixed
     */
    public function getAll(array $where, string $field = '*', int $page = 1, int $pageSize = 10, array $order = ['create_time' => 'DESC'])
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
        return $result;
    }

    /**   YSF
     *    添加发票
     * @param $data  添加数据
     * @return mixed
     */
    public function addInvoice($data)
    {
        $result = $this->db->insert($this->dbName)
                        ->set($data)
                        ->query()
                        ->insert_id();
        return $result;
    }

}