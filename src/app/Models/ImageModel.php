<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   图片库 模型
 *   Date: 2018/12/3
 * Class ImageModel
 * @package app\Models
 */
class ImageModel extends Model
{
    // 表名
    protected $table = 'image';

    // 列表查询
    public function getAll(array $where, int $page, int $pageSize, string $field = '*', array $order = ['sort' => 'DESC'])
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->page($pageSize,$page)
            ->order($order)
            ->query()
            ->result_array();

        return $result;
    }

    // 删除操作
    public function del(array $where)
    {
        $result = $this->db->delete()
                ->from($this->table)
                ->TPWhere($where)
                ->query()
                ->affected_rows();
        return $result;
    }

    // 添加操作
    public function add(array $data)
    {
        $result = $this->db->insert($this->table)
                ->set($data)
                ->query()
                ->insert_id();
        return $result;
    }



}