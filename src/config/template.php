<?php
/*************************************重庆润泉公司**************************/
$config['company_1'] = [
    'user_new_order' => [
        'desc' => '用户线上下单支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => '2-6SS51P8QvDVgUbc3GAhgsAD786Sx_jwWAfcYWkptU', 'mp_template_id' => 'DhdmLze-8fnwd6ojQD2bekDSXtW1WcNvIfDdnN08y-I', 'title' => '订单支付成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => true],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'user_appoint_success' => [
        'desc' => '用户线上预约安装成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => 'KNOyQaUk9fg5HA-j7jZYtE1pAeE00hMTM44YargH-Cg', 'mp_template_id' => 'g5u_WF8UxjOLHRAFg4jYn-Fxg2sg2G8fpvTcidns0fM', 'title' => '预约成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => true],
                'email' => ['switch' => true],
            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'engineer_go_home' => [
        'desc' => '工程师傅上门',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'engineer_confirm_time' => [
        'desc' => '工程师傅确认预约时间',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => 'Re4HThLgxfFAfv1Bus9uXUewdTIEr-WkFaRbWDzP6PU', 'mp_template_id' => 'LRSVLs-GWLjk20DQR9NtqJaThHS_Tt26tW1ux84CWns', 'title' => '确认预约时间']
            ],

        ]
    ],
    'bind_device_success' => [
        'desc' => '工程师傅给客户绑定设备成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => 'pseePIxMOAkCamHsNNJzmvu4R37r6KHXrNHLVhcNeX8', 'mp_template_id' => '9biiEbaXur0iaJR5ryNUVz2Kz0N03e_m-EWCYHXu5ik', 'title' => '绑定设备成功']
            ],
        ]
    ],
    'user_bind_device_success' => [
        'desc' => '用户绑定其他设备',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => 'pseePIxMOAkCamHsNNJzmvu4R37r6KHXrNHLVhcNeX8', 'mp_template_id' => '9biiEbaXur0iaJR5ryNUVz2Kz0N03e_m-EWCYHXu5ik', 'title' => '绑定设备成功']
            ],
        ]
    ],
    'contract_reminding' => [
        'desc' => '绑定设备成功后通知客户签署合同',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => 'O5kxw8V4KvszkM6DscDYMTU42lxE7GnJgJD9zkO9ewE', 'mp_template_id' => 'BFvFp70cWoYd0T-SfefNjasWkXasB8mK3SCjmow5ltw', 'title' => '合同签署提醒']
            ],

        ]
    ],
    'contract_sign_success' => [
        'desc' => '合同签署成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'renew_success' => [
        'desc' => '续费成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => 'IyYxpkG1O0pCxm3rvaGSo6QTxJxSmYKewT_iRjWt0CA', 'title' => '续费成功']
            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => 'IyYxpkG1O0pCxm3rvaGSo6QTxJxSmYKewT_iRjWt0CA', 'title' => '续费成功']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'renew_warn' => [
        'desc' => '合同到期定时发送缴费提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => 'g5MRrhBFUNGPy34OImV9uu6v2Qcrb1kvGBSbyFvx-x0', 'title' => '续费提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'reminder' => [
        'desc' => '客户催单',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => 'FtlyMLdaAGImwM9-8yl68peGJ4qZZwsuwqf3II1sH5E', 'mp_template_id' => 'WJgmPH8SY-lMh7z3SC-Rz14mUxwe2JYe3heD_QFuNMo', 'title' => '催单通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'WorkOrderProcessingNotice' => [
        'desc' => '工单处理通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '8IvAmVzzH72vntpSNtOKiVjCLEIPq8czP3gSX9xA524', 'mp_template_id' => '8IvAmVzzH72vntpSNtOKiVjCLEIPq8czP3gSX9xA524', 'title' => '工单处理通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'TransactionReminders' => [
        'desc' => '工程师傅回单时支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => false, 'wx_template_id' => 'ioOGCIzUYJSbZ6gHI7Meo8O1BPTRWffXc1zmjnAZ5G0', 'mp_template_id' => 'ioOGCIzUYJSbZ6gHI7Meo8O1BPTRWffXc1zmjnAZ5G0', 'title' => '交易提醒']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'core_expire' => [
        'desc' => '滤芯到期提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => 'skFyuFrx1ormhbJwr-LO2J_QDaR5xg2egfiJ_AELtoU', 'mp_template_id' => 'WJgmPH8SY-lMh7z3SC-Rz14mUxwe2JYe3heD_QFuNMo', 'title' => '滤芯到期提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],


];

/*************************************重庆优净云**************************/
$config['company_2'] = [
    'user_new_order' => [
        'desc' => '用户线上下单支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '2-6SS51P8QvDVgUbc3GAhgsAD786Sx_jwWAfcYWkptU', 'mp_template_id' => 'DhdmLze-8fnwd6ojQD2bekDSXtW1WcNvIfDdnN08y-I', 'title' => '订单支付成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => true],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'user_appoint_success' => [
        'desc' => '用户线上预约安装成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'KNOyQaUk9fg5HA-j7jZYtE1pAeE00hMTM44YargH-Cg', 'mp_template_id' => 'g5u_WF8UxjOLHRAFg4jYn-Fxg2sg2G8fpvTcidns0fM', 'title' => '预约成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => true],
                'email' => ['switch' => true],
            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'engineer_go_home' => [
        'desc' => '工程师傅上门',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'engineer_confirm_time' => [
        'desc' => '工程师傅确认预约时间',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'Re4HThLgxfFAfv1Bus9uXUewdTIEr-WkFaRbWDzP6PU', 'mp_template_id' => 'LRSVLs-GWLjk20DQR9NtqJaThHS_Tt26tW1ux84CWns', 'title' => '确认预约时间']
            ],

        ]
    ],
    'bind_device_success' => [
        'desc' => '工程师傅给客户绑定设备成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'pseePIxMOAkCamHsNNJzmvu4R37r6KHXrNHLVhcNeX8', 'mp_template_id' => '9biiEbaXur0iaJR5ryNUVz2Kz0N03e_m-EWCYHXu5ik', 'title' => '绑定设备成功']
            ],
        ]
    ],
    'user_bind_device_success' => [
        'desc' => '用户绑定其他设备',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'pseePIxMOAkCamHsNNJzmvu4R37r6KHXrNHLVhcNeX8', 'mp_template_id' => '9biiEbaXur0iaJR5ryNUVz2Kz0N03e_m-EWCYHXu5ik', 'title' => '绑定设备成功']
            ],
        ]
    ],
    'contract_reminding' => [
        'desc' => '绑定设备成功后通知客户签署合同',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'O5kxw8V4KvszkM6DscDYMTU42lxE7GnJgJD9zkO9ewE', 'mp_template_id' => 'BFvFp70cWoYd0T-SfefNjasWkXasB8mK3SCjmow5ltw', 'title' => '合同签署提醒']
            ],

        ]
    ],
    'contract_sign_success' => [
        'desc' => '合同签署成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'renew_success' => [
        'desc' => '续费成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => 'IyYxpkG1O0pCxm3rvaGSo6QTxJxSmYKewT_iRjWt0CA', 'title' => '续费成功']
            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => 'IyYxpkG1O0pCxm3rvaGSo6QTxJxSmYKewT_iRjWt0CA', 'title' => '续费成功']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'renew_warn' => [
        'desc' => '合同到期定时发送缴费提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => 'g5MRrhBFUNGPy34OImV9uu6v2Qcrb1kvGBSbyFvx-x0', 'title' => '续费提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'reminder' => [
        'desc' => '客户催单',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => 'FtlyMLdaAGImwM9-8yl68peGJ4qZZwsuwqf3II1sH5E', 'mp_template_id' => 'WJgmPH8SY-lMh7z3SC-Rz14mUxwe2JYe3heD_QFuNMo', 'title' => '催单通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'WorkOrderProcessingNotice' => [
        'desc' => '工单处理通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '8IvAmVzzH72vntpSNtOKiVjCLEIPq8czP3gSX9xA524', 'mp_template_id' => '8IvAmVzzH72vntpSNtOKiVjCLEIPq8czP3gSX9xA524', 'title' => '工单处理通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'TransactionReminders' => [
        'desc' => '工程师傅回单时支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => 'ioOGCIzUYJSbZ6gHI7Meo8O1BPTRWffXc1zmjnAZ5G0', 'mp_template_id' => 'ioOGCIzUYJSbZ6gHI7Meo8O1BPTRWffXc1zmjnAZ5G0', 'title' => '交易提醒']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'core_expire' => [
        'desc' => '滤芯到期提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'skFyuFrx1ormhbJwr-LO2J_QDaR5xg2egfiJ_AELtoU', 'mp_template_id' => 'WJgmPH8SY-lMh7z3SC-Rz14mUxwe2JYe3heD_QFuNMo', 'title' => '滤芯到期提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],


];
/*************************************青州一泽**************************/
$config['company_3'] = [
    'user_new_order' => [
        'desc' => '用户线上下单支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'iSM7i5wWqArwmQBbwKI5s-SwYKcKyjMvnr5pjoIvxmE', 'mp_template_id' => 'iSM7i5wWqArwmQBbwKI5s-SwYKcKyjMvnr5pjoIvxmE', 'title' => '订单支付成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => true],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'user_appoint_success' => [
        'desc' => '用户线上预约安装成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'KNOyQaUk9fg5HA-j7jZYtE1pAeE00hMTM44YargH-Cg', 'mp_template_id' => 'HQr6vRAvh9QCMmdzrwzNEqNAIQuZNViUTSTn_MAf3ow', 'title' => '预约成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => true],
                'email' => ['switch' => true],
            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'engineer_go_home' => [
        'desc' => '工程师傅上门',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'engineer_confirm_time' => [
        'desc' => '工程师傅确认预约时间',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'hg4_GNcH0JJr44Avw6oFfoegt8lish5H7ZQJ6qeHW3M', 'mp_template_id' => 'hg4_GNcH0JJr44Avw6oFfoegt8lish5H7ZQJ6qeHW3M', 'title' => '确认预约时间']
            ],

        ]
    ],
    'bind_device_success' => [
        'desc' => '工程师傅给客户绑定设备成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'Fdp-SWRfYsAgXF1YoTqIW_XV_asDKgFFm0Qk3OV7yJg', 'mp_template_id' => 'Fdp-SWRfYsAgXF1YoTqIW_XV_asDKgFFm0Qk3OV7yJg', 'title' => '设备绑定通知']
            ],
        ]
    ],
    'user_bind_device_success' => [
        'desc' => '用户绑定其他设备',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'Fdp-SWRfYsAgXF1YoTqIW_XV_asDKgFFm0Qk3OV7yJg', 'mp_template_id' => 'Fdp-SWRfYsAgXF1YoTqIW_XV_asDKgFFm0Qk3OV7yJg', 'title' => '设备绑定通知']
            ],
        ]
    ],
    'contract_reminding' => [
        'desc' => '绑定设备成功后通知客户签署合同',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'rVXcoPw9s-0R9aJ5tLjjGAinAvWlKgDMO2sQo0-YWQA', 'mp_template_id' => 'rVXcoPw9s-0R9aJ5tLjjGAinAvWlKgDMO2sQo0-YWQA', 'title' => '合同签署通知']
            ],

        ]
    ],
    'contract_sign_success' => [
        'desc' => '合同签署成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'renew_success' => [
        'desc' => '续费成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => 'mRCUIdIBj5OxG9MSO4ruCOlRlYh0rQYqTIw4-ksr95o', 'title' => '续费成功通知']
            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => 'mRCUIdIBj5OxG9MSO4ruCOlRlYh0rQYqTIw4-ksr95o', 'title' => '续费成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'renew_warn' => [
        'desc' => '合同到期定时发送缴费提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '_TuY4P3e38mViXMDdwAS_RRN_u7GV8ESncn79rSgldc', 'mp_template_id' => '_TuY4P3e38mViXMDdwAS_RRN_u7GV8ESncn79rSgldc', 'title' => '缴费提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'reminder' => [
        'desc' => '客户催单',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => 'r8ueXpsFQpnDD1NXsfszpta_fOfZgtidE8ZDlxk2SBM', 'mp_template_id' => 'r8ueXpsFQpnDD1NXsfszpta_fOfZgtidE8ZDlxk2SBM', 'title' => '催单通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'WorkOrderProcessingNotice' => [
        'desc' => '工单处理通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '8IvAmVzzH72vntpSNtOKiVjCLEIPq8czP3gSX9xA524', 'mp_template_id' => '8IvAmVzzH72vntpSNtOKiVjCLEIPq8czP3gSX9xA524', 'title' => '工单处理通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'TransactionReminders' => [
        'desc' => '工程师傅回单时支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => 'ioOGCIzUYJSbZ6gHI7Meo8O1BPTRWffXc1zmjnAZ5G0', 'mp_template_id' => 'ioOGCIzUYJSbZ6gHI7Meo8O1BPTRWffXc1zmjnAZ5G0', 'title' => '交易提醒']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'core_expire' => [
        'desc' => '滤芯到期提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'skFyuFrx1ormhbJwr-LO2J_QDaR5xg2egfiJ_AELtoU', 'mp_template_id' => 'WJgmPH8SY-lMh7z3SC-Rz14mUxwe2JYe3heD_QFuNMo', 'title' => '滤芯到期提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],


];
/*************************************水行家**************************/
$config['company_4'] = [
    'user_new_order' => [
        'desc' => '用户线上下单支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'UxX3IkTpVQEhzFNJogNSZE0qO-qS9239SSDJ09vnues', 'title' => '订单支付成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'user_appoint_success' => [
        'desc' => '用户线上预约安装成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => '	WbpAyZugPN7HgZADx5KjfwKWQgWIG3hTpSrqoLL_57g', 'title' => '预约成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => true],
                'email' => ['switch' => true],
            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'engineer_go_home' => [
        'desc' => '工程师傅上门',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'engineer_confirm_time' => [
        'desc' => '工程师傅确认预约时间',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'u90zQIho9QwXTH74ssLapJqZAHLiN7YZQF3L_EmewxE', 'title' => '预约时间确认通知']
            ],

        ]
    ],
    'bind_device_success' => [
        'desc' => '工程师傅给客户绑定设备成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => '2_0DumuB6Z9VZCnoU8BxaloLZLBlFhC5ZurckGda0Yw', 'title' => '设备绑定通知']
            ],
        ]
    ],
    'user_bind_device_success' => [
        'desc' => '用户绑定其他设备',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => '2_0DumuB6Z9VZCnoU8BxaloLZLBlFhC5ZurckGda0Yw', 'title' => '设备绑定通知']
            ],
        ]
    ],
    'contract_reminding' => [
        'desc' => '绑定设备成功后通知客户签署合同',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => '18QyrTc87pzZyOHDnyoK_U6kj1qbSpw5nv13L9hd6Gc', 'title' => '合同签署通知']
            ],

        ]
    ],
    'contract_sign_success' => [
        'desc' => '合同签署成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'renew_success' => [
        'desc' => '续费成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'WYZmji8fCV-fTLQZF6s2L8gvDFrSxuQA2oFTIJLQN3E', 'title' => '续费成功通知']
            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'WYZmji8fCV-fTLQZF6s2L8gvDFrSxuQA2oFTIJLQN3E', 'title' => '续费成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'renew_warn' => [
        'desc' => '合同到期定时发送缴费提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'rarWCalb8CBzxDnK-0IK13dbYrm8Bmyr9ctIXykf648', 'title' => '续费提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'reminder' => [
        'desc' => '客户催单',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => '666STkyMDNETMryY-b3bHxl9B666d-aRETrGGPz2n50', 'title' => '催单通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'WorkOrderProcessingNotice' => [
        'desc' => '工单处理通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => '8IvAmVzzH72vntpSNtOKiVjCLEIPq8czP3gSX9xA524', 'title' => '工单处理通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'TransactionReminders' => [
        'desc' => '工程师傅回单时支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'ioOGCIzUYJSbZ6gHI7Meo8O1BPTRWffXc1zmjnAZ5G0', 'title' => '交易提醒']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'core_expire' => [
        'desc' => '滤芯到期提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'WJgmPH8SY-lMh7z3SC-Rz14mUxwe2JYe3heD_QFuNMo', 'title' => '滤芯到期提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],


];
return $config;