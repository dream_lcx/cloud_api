<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 银行卡类别
 */

$config['banks'] = [
    '0'=>[
        'bank_name'=>'工商银行',
        'bank_code'=>'1002',
    ],
    '1'=>[
        'bank_name'=>'农业银行',
        'bank_code'=>'1005',
    ],
    '2'=>[
        'bank_name'=>'中国银行',
        'bank_code'=>'1026',
    ],
    '3'=>[
        'bank_name'=>'建设银行',
        'bank_code'=>'1003',
    ],
    '4'=>[
        'bank_name'=>'招商银行',
        'bank_code'=>'1001',
    ],
    '5'=>[
        'bank_name'=>'邮储银行',
        'bank_code'=>'1066',
    ],
    '6'=>[
        'bank_name'=>'交通银行',
        'bank_code'=>'1020',
    ],
    '7'=>[
        'bank_name'=>'浦发银行',
        'bank_code'=>'1004',
    ],
    '8'=>[
        'bank_name'=>'民生银行',
        'bank_code'=>'1006',
    ],
    '9'=>[
        'bank_name'=>'兴业银行',
        'bank_code'=>'1009',
    ],
    '10'=>[
        'bank_name'=>'平安银行',
        'bank_code'=>'1010',
    ],
    '11'=>[
        'bank_name'=>'中信银行',
        'bank_code'=>'1021',
    ],
    '12'=>[
        'bank_name'=>'华夏银行',
        'bank_code'=>'1025',
    ],
    '13'=>[
        'bank_name'=>'广发银行',
        'bank_code'=>'1027',
    ],    
    '14'=>[
        'bank_name'=>'光大银行',
        'bank_code'=>'1022',
    ],
    '15'=>[
        'bank_name'=>'北京银行',
        'bank_code'=>'1032',
    ],
    '16'=>[
        'bank_name'=>'宁波银行',
        'bank_code'=>'1056',
    ],
];
return $config;