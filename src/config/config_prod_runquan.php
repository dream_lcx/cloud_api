<?php

/*
 * 线上环境配置--线上
 */
//域名配置
$config['callback_domain_name'] = 'https://rqcrm.youheone.com';
$config['achievement_domain_name'] = 'https://rqcrm.youheone.com';
$config['static_resource_host'] = 'https://rqcrm.youheone.com/'; //静态资源路径.图片,视频等
$config['boss_api_url'] = 'https://boss.youheone.com/';
$config['base_domain'] = 'https://rqcrm.youheone.com';
//百度小程序
$config['baidu_weapp']= [
    'appKey'=>'MMMuHF',
    'dealId'=>'386317915'
];

return $config;
