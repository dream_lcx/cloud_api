<?php
/*
 * 公共配置文件--适用于线上线下
 */
$config['system_auto_settle'] = true;//是否开启系统结算 20230906关闭系统自动结算
$config['system_clearing_swtich'] = false;//是否开启系统清算 true:开启 false:关闭,开启后每月扎帐截止日期前不能进行支付(用户、工程)
$config['system_clearing_date'] = 5;//扎帐截止日期
$config['system_clearing_text'] = '每月1-5日为系统扎帐时间,此期间不能进行线上缴费,请于每月6日后进行操作!';//扎帐提示文案
$config['token_key'] = 'youheone_user_key';
$config['app_sn'] = 'cloud2019';//应用编号,请根据boss系统应用表
$config['default_company_id'] = 1;//默认公司ID
$config['debug_log'] = true;//是否开启调试和错误日志,自行编码内记录的日志信息
$config['business_user_contract_sign_way'] = [1 => 2, 2 => 1, 3 => 1];//商用机合同签署方式 1线下纸质合同并上传 2线上签署合同
//逻辑模式runquan：润泉的模式 huaxin:华信的模式
$config['logic_mode'] = [
    'runquan' => [1],
    'huaxin' => [2, 3, 4]
];
//商用机 is_need_auth 是否需要实名 is_need_sign_contract是否需要签合同
$config['business_machines'] = [
    'is_need_auth' => true,
    'is_need_sign_contract' => false
];
//拥有润泉独有逻辑的公司ID
$config['runquan_logic_mode'] = [1];
//七牛云配置
$config['qiniu'] = [
    'accessKey' => '5oCby8LtJSHJngK-f0Zwg05-ETA3JrIwQUP9IQ2E',
    'secrectKey' => 'AZA9PZYttVpIxsZ8653o_hwZPZh3zzlB2nVZLmnV',
    'bucket' => 'cloud',
    'qiniu_url' => 'https://qn.youheone.com/',
    'picture_style_one' => 'imageView2/0/q/75|watermark/2/text/6YeN5bqG5ram5rOJ5YeA5rC0/font/5b6u6L2v6ZuF6buR/fontsize/360/fill/I0ZGRkZGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10|imageslim',//图片样式
    'rotate_style' => 'imageMogr2/rotate/-90',//旋转90度
];
//百度地图配置
$config['baidu_map'] = [
    'ak' => 'en4lPqxSMjtVjdNWweAtlr48WugLx2sL'
];
//短信配置--八米
//$config['sms'] = [
//    'uid' => '1053',
//    'passwd' => '123456',
//    'content' => '润泉净水'
//];

//人脸识别--腾讯云API秘钥
$config['face'] = [
    'appid' => '1252911620',
    'secretId' => 'AKIDISUP9jN8cx9D5lBXeYGm3DC2g8QY9MyO',
    'secretKey' => 'LUbbWGnAOQhtxpVSAuUs44sTs681lQxM',
    'bucket' => ''
];

//人脸识别--百度API秘钥
//$config['baidu_face'] = [
//    'appId' => '15380948',
//    'apiKey' => 'Ge22B1Qz18VSBphyE2C1KEsb',
//    'secretKey' => '4rAkKhYlt1IGMrbVP76UMAKBzChsHPzD',
//];
//百度账号：王飞
$config['baidu_face'] = [
    'appId' => '23856500',
    'apiKey' => 'pKUfAyPpi5o54EqPZywhyTit',
    'secretKey' => 'tIabNFGKiiWfe72C21MGtWg1rpMgRZgk',
];
//树米 SIM API密匙
$config['shumi'] = [
    'companyNo' => '562056598505484288',
    'apiKey' => '13wmEmNeAyDmx1Df0CIHuYfZrWhneNJM',
    'apiSecret' => 'EOOlodNP32C3L7RRr0G0',
    'testUrl' => 'https://api-dev.showmac.cn/',
    'proUrl' => 'https://api.showmac.cn/',
];
$config['alipay'] = [
    'appId' => '2019010462846036',
    'alipayrsaPublicKey' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyTiRa6bNwBjsainVhQRjhx/VW4Dr9tjHbMKaVBytCmmEtIEhbeLq9ix02WJoYyD4BOPtUGI1Jlq/eA4kscC5dSJSuL7pJytNfnSgkUq5MpboF/f6aC7AYZtEmb9OooaLA9ZXav/RqMTGt1Q5KbY4YZDZs/d28cb516Rl8x6XOMNklDYDWhzOA0oMadXN74lp5OMNo7z/ISbEMGAOczz2BIxHnbYUSA5rPuGVFwApNERc5MeAVYqojXjJpDztp24LLGjvjC3WJmPpDrwJMvcA2Z92V9uGHNPzAF+KoKzxHRzRaFhVwRohBFSmP1EupIud4RLxbzRenQGWzfOGgOQHEwIDAQAB',
    'rsaPrivateKey' => 'MIIEpQIBAAKCAQEAyKKYxRvLr8+ySV2rl2b5Yk/1tR/2iyFpG5uAJoW/O16WhH1fBur9GdOlR46qrGLoskXnEZ1dvnRy8HNO0J1M1PoGLSC/DRvclvGgZLbO/0h37gyY3EUTQ3213lm8DxrPMYOiA0GfIRlY18WUtKgAhg/heKwvLSqGl+X+hp07JZXEO3rJlyoVRr+ulapiAyhJ+GlYPK/inQBDjE67LQ3uGeD2+/MNxDM8OlIbjLDzo/XL9gxbPUns62O+jcGSNqu+R/nTqXRSnMyCejvI7WfQuJ+1cjiw/8C7e+XtoWiZ/eRs9tMCsSNj+dB/PyLW0DsOn9b0Wrc0axnxeuCrZpQ8QQIDAQABAoIBAGy0S/lfmfhjm8FMUw/w5uVxVTsRWk7/hydCFSjKBlZwZTTbiIUDBKbdmQswLSDBZjpQzWUp0gTCqxPEJcu5rksmO/pvZ5P+qIHP4Isw0O5XoUME6D1P36QNlGAoVlAIwoGdREZnY4RXa19c2bkX3VDFgfATumvKQqpCHqlovwcZLGWBSRgRoYShHZ0b6kJmXXSpPXHns2cfIkOMfnRz0tFuBvddB/Gy7eFoWkGIPBUZDSlWgIjZ7iOXw9Cuq2DSW+ZvSMzf7hQL5OXq36KRyLHMXn2qX0j6vkMDIYen89o9qJKLtEiLiLYAEmxreerXxOooWuve15S0R9kCZFDOXAECgYEA5GcH+NLXxX45KExzD/yiW49HqQssyrkYhxJDLbUpJIFevJK3lmzt1EjhLn9fiZPmsd6fKPXEPQdJrA7DoUHvBHBXHpIwU5hzgbtoLPijmwuILTui/l258utw3K/u9tjlpTKl2ZLr/SGA3VJ14bDvQLlqDkkUkoKseKZXb7AUtTECgYEA4OCp9wahbumkWzbG+ErAL63rxRaKLZ07gJapdUt3Q4uGdu7mKqsViXreoV2ztp6ZkYYNwY3XewTZRrgwlNIIKZYIoN+3nhc5A53AGmchRuyzv6BUXx7/jOnYyTsAuH+CNM2w7rfXPrDL7Hf7Jp8RUki6F38fZF6jT0O4sSY6dBECgYEAzCgcRBmSrQFKbe8H2n5jUZwc3ckTXJFejz2Ptsd74xmQaNZsK5VqcJpETZ+k8gC18L+hXKdxpWtiOt1nmDirQTh5ZuYdNFvMw2WUlL4a33fLXUM1SlDU6YHx/BF2dEcj3ioLfROKxVSxLyOKIxZYhW42klZYgS00pRNqjHbDy8ECgYEAiSK4w7o5bEfzQOTQxc3gEbOAWjp6nYKLLMm0IaGZ44kMwS7DEjYu9h4F8mYQGEfz4rjbXrO/OngkgGoGxlqnO6URMfn8Id6LmFb1NaS6uGPU4gFGMpPWx/o7h8wBjoHDx6vAQHPAQazTTyuD73OFvqY3RejyHeVQth/YW7+yWeECgYEAxs6kJ4l286AfKvA6/Ozot9G4LEkYPezYxmuv/eH8MtVAxDEIhwXAacJ1/f4dBZZVyenImQ3Fu8tzD9RZCN+G6NhASzOJlZ4Kziw7iJULBoEimZlElLnJDKzZyVYRJhq/FGBMXSqqX3M4aVCdNlfp8cXXdm+kUSnVLDt95jwh938=',
    'AES' => 'MPgr/oHm9KU6lTejqCll3g==',
    'gatewayUrl' => 'https://openapi.alipay.com/gateway.do'
];

$config['baidu'] = [
    'AppKey' => 'moMsFyVTFMU8qt75q4K8EKgb41hK0hF1',
    'AppSecret' => 'RQnhICN4k8DjrWPwMHhzWpviDKspkh26'
];
$config['wx_pay'] = [
    'app_wx_name' => '滳滳打水',
    'appId' => '',
    'appSecret' => '',
    'pay_key' => 'chongqingyouheyizhongwangluokeji',
    'mchid' => '1500484712'
];
//聚合支付应用ID，应用网址：https://console.adapay.tech/merconsole/manage/application
$config['adapay_app_id'] = [
    'engineer' => 'app_030a467c-69b4-40ec-8f8a-00c210bb05ee',
    'user' => 'app_c44dee9c-9047-487e-954b-742a01d7f1cd'
];

// 汇付支付提现固定单笔所需手续费1元
$config['fee_money'] = 1;

//报备有效天数
$config['report_day'] = 7;

//新装抵用券，有效期，单位天
$config['coupon_valid_time'] = 365;

//存在返券，抵扣金额
$config['ticket_money'] = 50;

//账号配置，该账号为用户表account字段，也是员工编号，以这里的为准
$config['account'] = [
    'total_account_number' => '855182', //总账号
    //'new_total_account_number' => '2', // 周建华 账号
    'gm_account_number' => '855182', //总经理账号
    'gma_account_number' => '312170', //总经理助理账号
];
//申请经销商默认/默认推荐人
$config['default_inviter'] = [
    'user_id' => '32',
    'account' => '350938',
    'telphone' => '18523190308',
    'id_card' => '51303019870405841X',
    'name' => '王',
];

//主板硬件协议版本
$config['protocol_version'] = [
//    ['id' => 1, 'name' => '2G主板', 'desc' => '优合研发的2G和少量4G主板'],
    ['id' => 2, 'name' => '刷卡版', 'desc' => '刷卡板'],
    ['id' => 3, 'name' => '4G主板', 'desc' => '物联网主板']
];

// 买断到期时间，单位 年
$config['is_buy_out'] = [
    'exire_year' => 2
];

//工单状态
$config['work_order_status'] = [
    1 => '待审核',
    2 => '无效工单',
    3 => '前台已处理',
    4 => '审核通过,待指派工程部',
    5 => '审核通过,待派单',
    6 => '工单已进入大厅',
    7 => '已指派工程人员,待接单',
    8 => '工程人员已接单,待预约',
    9 => '工程人员已预约,待上门',
    10 => '工程人员已上门,服务中',
    11 => '工程人员已完成,待评价',
    12 => '已评价',
    13 => '工程人员已退单,待审核',
    14 => '工程人员退单成功,回到大厅',
    15 => '前台未回访',
    16 => '前台已回访，未完成',
    17 => '前台已回访，已完成',
    18 => '退款中',
    19 => '已退款'
];
//维修端端工单状态,仅用于状态转换
$config['engineer_order_status'] = [
    1 => '待审核',
    2 => '无效工单',
    3 => '前台已处理',
    4 => '待指派工程部',
    5 => '待派单',
    6 => '工单已进入大厅',
    7 => '待接单',
    8 => '待预约',
    9 => '待上门',
    10 => '处理中',
    11 => '待评价',
    12 => '已评价',
    13 => '已退单,待审核',
    14 => '退单成功',
    15 => '前台未回访',
    16 => '前台已回访，未完成',
    17 => '前台已回访，已完成',
    18 => '退款中',
    19 => '已退款'
];
//用户端端工单状态,仅用于状态转换
$config['user_work_order_status'] = [
    1 => '待审核',
    2 => '无效工单',
    3 => '前台已处理',
    4 => '待指派工程部',
    5 => '待派单',
    6 => '工单已进入大厅',
    7 => '待接单',
    8 => '待预约',
    9 => '待上门',
    10 => '服务中',
    11 => '待评价',
    12 => '已评价',
    13 => '已退单,待审核',
    14 => '待派单',
    15 => '前台未回访',
    16 => '前台已回访，未完成',
    17 => '前台已回访，已完成',
    18 => '退款中',
    19 => '已退款'
];
//用户端端工单状态说明
$config['user_work_order_status_desc'] = [
    1 => '工单提交成功，等待客服处理',
    2 => '经过审核，该工单视为无效工单',
    3 => '客服通过电话沟通已处理该工单',
    4 => '审核通过,待指派工程部中',
    5 => '已指派工程部,待派单中',
    6 => '已指派工程人员,待接单',
    7 => '已指派工程人员,待接单',
    8 => '已派单，工程师傅为您服务',
    9 => '已确定上门时间，待上门服务',
    10 => '师傅已上门，感谢您的等待',
    11 => '服务完成，请您对本次服务进行评价',
    12 => '感谢您的使用，欢迎加入润泉大家庭',
    13 => '工程人员已申请退单,我们将会重新为您指派工程人员',
    14 => '工程人员已申请退单,我们将会重新为您指派工程人员',
    15 => '前台未回访',
    16 => '前台已回访，未完成',
    17 => '前台已回访，已完成',
    18 => '退款中',
    19 => '已退款'
];
//工单退单原因
$config['return_reason'] = [
    1 => '时间问题',
    2 => '无法完成',
    3 => '距离太远',
    4 => '其他',
    5 => '不安装了'
];
//订单物流状态码
$config['order_logistics_code'] = [1 => '已下单', 2 => '已发货', 3 => '已揽件', 4 => '运输中', 5 => '派送中', 6 => '待取件', 7 => '已签收'];
//设备状态
$config['device_status'] = ['0' => '正常', '1' => '待激活', '2' => '未注册', '3' => '欠费', '4' => '滤芯待复位', '5' => '硬件调试'];
//订单状态
$config['order_status'] = [1 => '待付款', 2 => '待发货', 3 => '待安装', 4 => '已安装', 5 => '已取消', 6 => '待收货', 7 => '已作废', 8 => '退款中', 9 => '已退款'];
//订单来源0总后台 1客户 2 行政中心 3运营中心'
$config['order_opertaor'] = [0 => '总后台', 1 => '客户', 2 => '城市合伙人', 3 => '运营中心'];
//支付方式
$config['pay_way'] = [1 => '微信支付', 2 => '支付宝支付', 3 => '现金支付', 4 => '对公转账'];
//设备动态类型操作类型 1服务到期 2滤芯到期 3停水警告 4续费 5欠费 6检修 7漏水 8缺水 9更换设备
$config['device_log_status'] = [1 => '服务到期', 2 => '滤芯到期', 3 => '停水警告', 4 => '续费', 5 => '欠费', 6 => '检修', 7 => '漏水', 8 => '缺水', 9 => '更换设备'];
//合同状态 1生成中  2待公司(乙方)签字 3待租赁人(甲方)签字 4.生效中 5.已过期
$config['contract_status'] = [
    1 => '生成中',
    2 => '待公司签字',
    3 => '待签',
    4 => '生效中',
    5 => '已过期',
    6 => '已拆机,合同终止',
    7 => '已解约,合同终止'
];
//实名认证状态-1待提交姓名身份证号 -2待上传身份证正反面 -3待活体检测  1待审核2审核通过3审核失败
$config['realname_auth_status'] = [-1 => '待提交姓名身份证号', -2 => '待上传身份证正反面', -3 => '待活体检测', -4 => '待上传营业执照/单位附件', 1 => '待审核', 2 => '认证通过', 3 => '认证未通过'];

//认证类型
$config['auth_type'] = [0 => '个人认证', 1 => '企业认证', 2 => '政事认证'];

//业绩结算
//职位对应的金额
$config['grade_money'] = [
    1 => 60, //业务员
//        2=>30,  //直推人
    2 => 0, //直推人               2019年1月2日 更改结算金额
    3 => 100, //经销商
    4 => 10, //星级经销商
    5 => 5, //五星级经销商
    6 => 5, //十星级经销商
    7 => 5, //二十星级经销商
    8 => 10, //二星级经销商
    9 => 5, //总经理助理
    10 => 10, //总经理
    11 => 20, //讲解
];
// TODO 2024-06-01 新结算佣金配置
$config['settlement_commission'] = [
    'is_recommend'          => 10, // 推荐人佣金
    'agent_level_first'     => 10, //一级代理经销商佣金
    'agent_level_two'       => 20, //二级代理经销商佣金
    'agent_level_three'     => 30, //三级代理经销商佣金
    'agent_level_four'      => 40, //四级代理经销商佣金
    'dealer_commission'     => 100, //经销商佣金
    'total_commission'      => 150, //总佣金
];

// TODO 2024-06-01 新结算佣金配置
$config['newBalanceWayTime'] = '2024-04-01';


/**
 * 回单选择得机型，请与CRM一致
 */
//$config['device_model'] = [
//    '润泉有泵净水器',
//    '润泉无泵净水器',
//    '直饮江山箱式有泵机',
//    '直饮江山箱式无泵机',
//    '其他',
//];
/*$config['device_model'] = [
    '物联网润泉有泵机',
    '物联网润泉无泵机',
    '直饮江山箱式有泵刷卡机',
    '直饮江山箱式无泵刷卡机',
    '其他',
    '不带电',
    '带电',
    '605箱式机',
    '大胖中央净水器',
    '软水处理器'
];*/

//邀请状态
$config['invitation_user_status'] = [1 => '待激活', 2 => '待签约', 3 => '已解绑'];
// 工程人员账单状态
$config['engineer_bill'] = [
    'pay_type' => [1 => '支出', 2 => '收入', 3 => '代付'],
    'pay_way' => [1 => '微信支付', 2 => '支付宝支付', 3 => '现金支付', 4 => '对公转账'],
    'status' => [1 => '已支付'],
    'pay_client' => [1 => '扫码收款', 2 => '线下收款', 3 => '现金收款', 4 => '对公转账']
];
// 用户账单状态
$config['user_bill'] = [
    'pay_type' => [1 => '支出', 2 => '收入'],
    'pay_way' => [1 => '微信支付', 2 => '支付宝支付', 3 => '现金支付', 4 => '对公转账'],
    'status' => [1 => '已支付'],
    'pay_client' => [1 => '扫码支付', 2 => '线下支付', 3 => '现金支付', 4 => '对公转账']
];
//开票状态
$config['invoice_status'] = [1 => '待审核', 2 => '待发送', 3 => '开票成功', 4 => '开票失败'];
//系统默认配置
$config['system_defalut'] = [
    'username' => '未知'
];

//距离筛选，必须含有KM，范围使用-分割
$config['distance'] = [
    [
        'id' => 1,
        'name' => '1-5KM'
    ],
    [
        'id' => 2,
        'name' => '5-10KM'
    ],
    [
        'id' => 3,
        'name' => '10-15KM'
    ],
    [
        'id' => 4,
        'name' => '15-20KM'
    ],
    [
        'id' => 5,
        'name' => '20KM以上'
    ],
];
//sim_status状态
$config['sim_status'] = [-1 => '未知状态', 0 => '待激活', 1 => '已激活', 2 => '已停机', 3 => '拆机'];
//资金记录来源
$config['funds_record_source'] = [1 => '提现', 2 => '结算', 3 => '其他', 4 => '特殊情况扣费', 5 => '续费抵扣', 6 => '新装代付', 7 => '续费代付', '8' => '撤销结算扣除'];

//用户账户账单 变动类型
$config['user_account_bill_source'] = [1 => '渠道业绩结算', 2 => '渠道推广员业绩结算', 3 => '分成给渠道推广员', 4 => '提现'];

// 客户住房性质
$config['house_type'] = [0 => '未知', 1 => '购买', 2 => '公租房', 3 => '租赁'];
//移机类型，1：移机拆/装，2：移机拆，3：移机装。移机专用
$config['replacement_type'] = [1 => '拆/装', 2 => '拆', 3 => '装'];

//新增
//设备类型
$config['equipment_type'] = [0 => '未绑定', 1 => '租赁', 2 => '购买'];
//设备在线离线状态
$config['equipmentlist_status'] = [1 => '离线', 2 => '在线'];
//设备状态-上报
$config['device_status'] = [0 => '正常', 1 => '待激活', 2 => '未注册', 3 => '欠费', 4 => '滤芯待复位', 5 => '硬件调试'];

// //库存管理角色
$config['stock_role'] = [1 => '总后台', 2 => '运营端', 3 => '行政端', 4 => '经销商'];

//服务商支付
$config['service_payment_config'] = [
    'appId' => 'wxc076a5234f51597e',
    'appSecret' => 'f18097a91abd2b608b857412b95c6982',
    'pay_key' => 'chongqingyouheyizhongwangluokeji',
    'mchid' => '1503946831',
];

//新增 订单状态，1：待审核，2：通过，3：拒绝
$config['cash_order_state'] = [
    1 => '待审核',
    2 => '通过',
    3 => '拒绝',
];

//打款类型
$config['play_money_type'] = [
    0 => '线下打款',
    1 => '线上打款',
];

//打款状态，1：未打款，2：打款中，3：已完成
$config['play_money_state'] = [
    1 => '未打款',
    2 => '打款中',
    3 => '已完成'
];

//银行 bank_type
$config['bank_type'] = [
    1 => '银行卡',
    2 => '微信',
    3 => '支付宝',
];

//新增设备二位码地址
$config['qrcode_url'] = [
    'cloud_qrcode' => 'https://qrcode.youheone.com/',
];
//星级讲解人讲解费
$config['star_explainer_money'] = 5;

return $config;
