<?php
/*
 * @Descripttion:
 * @version: 1.0.0
 * @Author: xg
 * @Date: 2022-03-01 09:40:12
 * @LastEditors: xg
 * @LastEditTime: 2022-10-18 15:16:20
 */
/**
 * 拦截器  拦截器里的方法不需要token认证
 */
$config['custom']['interceptor'] = [
    'Common' => [
        'Device' => ['editFilterAll', 'editFilterCycle']
    ],
    'User' => [
        'User' => ['register', 'bind', 'updateToken','alipayRegister','test','aliPhone','login','getBaiduOpenid','binds'],
        'Equipment' => ['index', 'equipmentsDetail', 'equipmentRentalPackage'],
        'Order' => ['getEvaluateList'],
        'Callback' => ['rentOrder', 'rentRenewOrder','ali_rentOrder','test','baiduRentOrder','renewCallback','rentAdaPayOrder',
            'rentAdaPayRenewOrder', 'adapayWithdrawCallback', 'rentAdaPayRenewOrderApifox'],
        'Wx' => ['aesPhoneDecrypt'],
        'Relation' => ['levelSettlement','test','again'],
        'Relationhcp' => ['levelSettlement','test','again'],
        'Spread' => ['dealerApply','agentDealerApply'],
        'Parts' => ['editEquipmentsParts','resetEquipmentsParts','delEquipmentsParts'],
        'Test'=>['test'],
        'Share' => ['shareLog'],
        'Browse' => ['browseLog'],
        'Equipmentstype' => ['getEquipmentstypeList']
    ],
    'Engineer' => [
        'Engineers' => ['login', 'updateToken', 'forgetPassword', 'member', 'requestAdapay', 'settleAccount', 'drawcash'],
        'Wx' => ['register'],
        'Pay' => ['test'],
        'Callback' => ['codePay', 'refund', 'scanPayCallback', 'payCallback','rentRenewOrder','payCallbackAdd','rentRenewOrderAdd','scanPayAddOrder', 'adapayWithdrawCallback',
            'renewCallback', 'payAdaPayCallback','payAdaPayCallbackAgain', 'rentAdaPayRenewOrder'],
        'Order' => ['abTest'],
        'Contract'=>['getContractInfo'],
        'Workorder'=>['setPartApi'],

    ],
    'House' => [
        'Issue' => [
            'bindingPackage',
            'screen_status',
            'power_off',
            'mandatory_rinse',
            'charge_as',
            'as_negative',
            'time_synchronization',
            'heartbeat',
            'filter_element_reset_modification',
            'mode_switch',
            'factory_data_reset',
            'remote_firmware',
            'version',
            'modify_mandatory_flushing_times',
            'modified_timed_washing',
            'modify_maintenance_parameters',
            'modify_control_parameter_one',
            'modify_control_parameter_two',
            'switch_test_mode',
            'computer_board_time_sync',
            'water_sync',
            'data_sync',
            'syncRedisToDevice',
            'delDevice',
            'readFile',
            'getDevice'
        ]
    ],
    'Boss'=>[
        'Company'=>[
            'getlists'
        ]
    ],
    'Achievement'=>[
        'Login'=>[
            'login'
        ],
        'Dealer'=>[
            'canceltransfer',
            'exportDeviceExcel'
        ],
        'Settlement'=>[
            'balance',
            'channelSettlement',
            'removeSettlement',
            'balanceRepair',
            'exportAchievement',
            'sysnAchievementChecking',
            'newInstallNoSettleContract',
            'againSettlement',
            'examineOldAchievement',
            'adminExamineAchievementDetail',
            'staticsAcivement',
            'newEquipmentAutoSettlement'
        ]
    ],
    'External'=>[
        'User'=>['checkUserByPhone'],
    ]
];
return $config;
