<?php
/*
 * @Descripttion:
 * @version: 1.0.0
 * @Author: xg
 * @Date: 2022-03-01 09:40:12
 * @LastEditors: xg
 * @LastEditTime: 2022-06-21 09:24:57
 */
/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-7-14
 * Time: 下午1:58
 */

/**
 * 选择数据库环境
 */
$config['redis']['enable'] = true;
$config['redis']['active'] = 'dev';
/**
 * 开发环境
 */
$config['redis']['dev']['ip'] = '127.0.0.1';
$config['redis']['dev']['port'] = 6379;
$config['redis']['dev']['select'] = 5;
$config['redis']['dev']['password'] = 'PBGV5kXAnMyyU';


/**
 * 线上环境
 */
$config['redis']['prod']['ip'] = '127.0.0.1';
$config['redis']['prod']['port'] = 6379;
$config['redis']['prod']['select'] = 5;
$config['redis']['prod']['password'] = 'yh_redis';


/**
 * 最终的返回，固定写这里
 */
return $config;

