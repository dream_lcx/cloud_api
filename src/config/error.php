<?php
//错误收集上报系统
$config['error']['enable'] = false;
//是否显示在http上
$config['error']['http_show'] = false;
//访问地址，需自己设置ip：port
$config['error']['url'] = "Http://114.116.67.89:9092/Error";
$config['error']['redis_prefix'] = '@sd-error_';
$config['error']['redis_timeOut'] = '36000';

$config['error']['dingding_enable'] = false;

$config['error']['dingding_url'] = 'https://oapi.dingtalk.com';
//钉钉机器人，需自己申请
$config['error']['dingding_robot'] = '/robot/send?access_token=6b12c9928f414b6dfa25867d63e556a503997bdfb3c052a7f2c3f256c64493f0';
return $config;